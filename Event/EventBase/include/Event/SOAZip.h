/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <boost/mp11/algorithm.hpp>
#include <boost/mp11/bind.hpp>
#include <boost/type_index/ctti_type_index.hpp>
#include <tuple>

#include "GaudiKernel/GaudiException.h"
#include "LHCbMath/SIMDWrapper.h"
#include "Proxy.h"
// #include "GaudiKernel/detected.h"
#include "ZipUtils.h"

#if defined( __clang__ ) && ( __clang_major__ < 11 ) || defined( __APPLE__ ) && ( __clang_major__ < 12 )
#  define SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_BEGIN                                                                    \
    _Pragma( "clang diagnostic push" ) _Pragma( "clang diagnostic ignored \"-Wunused-lambda-capture\"" )
#  define SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_END _Pragma( "clang diagnostic pop" )
#else
#  define SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_BEGIN
#  define SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_END
#endif

/**
 * Zip of SOACollections
 *
 * A zip is a thin wrapper arround one or more SOACollection based structures. It
 * is specialized using one of the SIMDWrapper backend and provides ways to iterate,
 * filter or copy a set of collections synchronously.
 *
 * The iterator returns a ZipProxy object (see Proxy.h)
 *
 * To construct a zip of multiple containers, use:
 * `LHCb::Event:make_zip<SIMDWrapper::InstructionSet::Best>( containerA, containerB, ... )`
 * (Best is the default SIMD and can be omitted)
 *
 * To construct a zip with a single container, prefer the shorter versions:
 * `container.scalar()` or `container.simd<SIMDWrapper::InstructionSet::Best>()`
 * (Best is the default SIMD and can be omitted)
 *
 * Note: a Zip only keep pointers to existing containers and does not own any memory
 * thus it should always live on the stack where it can be optimized away by the
 * compiler.
 */
namespace LHCb::Event {
  /** Helper to map Best onto its current meaning, leaving other values alone.
   */
  template <SIMDWrapper::InstructionSet s>
  inline constexpr SIMDWrapper::InstructionSet resolve_instruction_set_v = SIMDWrapper::type_map<s>::instructionSet();

  template <SIMDWrapper::InstructionSet Simd, typename... ContainerTypes>
  struct Zip {
    using container_ptr_tuple          = std::tuple<ContainerTypes*...>;
    static constexpr auto default_simd = Simd;
    using default_simd_t               = typename SIMDWrapper::type_map_t<Simd>;
    using mask_v                       = typename default_simd_t::mask_v;

    struct Iterator {
      using value_type        = ZipProxy<typename ContainerTypes::template proxy_type<
          resolve_instruction_set_v<Simd>, LHCb::Pr::ProxyBehaviour::Contiguous, ContainerTypes>...>;
      using pointer           = value_type const*;
      using reference         = value_type&;
      using const_reference   = value_type const&;
      using difference_type   = int;
      using iterator_category = std::random_access_iterator_tag;
      using simd_t            = default_simd_t;
      container_ptr_tuple m_containers;
      int                 m_offset{ 0 };

      Iterator() = default;
      Iterator( container_ptr_tuple containers, int offset ) : m_containers{ containers }, m_offset{ offset } {}

      auto operator->() const {
        return std::apply( [&]( auto&&... args ) { return value_type{ { args, m_offset }... }; }, m_containers );
      }
      auto operator*() const {
        return std::apply( [&]( auto&&... args ) { return value_type{ { args, m_offset }... }; }, m_containers );
      }

      Iterator& operator++() {
        m_offset += simd_t::size;
        return *this;
      }
      Iterator& operator--() {
        m_offset -= simd_t::size;
        return *this;
      }
      Iterator operator++( int ) {
        Iterator retval = *this;
        m_offset += simd_t::size;
        return retval;
      }
      Iterator operator--( int ) {
        Iterator retval = *this;
        m_offset -= simd_t::size;
        return retval;
      }
      Iterator& operator+=( difference_type n ) {
        m_offset += n * simd_t::size;
        return *this;
      }
      auto operator[]( difference_type n ) const {
        return std::apply(
            [offset = m_offset + n * simd_t::size]( auto&&... args ) {
              return value_type{ { args, offset }... };
            },
            m_containers );
      }
      friend Iterator operator+( Iterator lhs, difference_type rhs ) { return lhs += rhs; }
      friend Iterator operator+( difference_type lhs, Iterator rhs ) { return rhs += lhs; }
      friend bool     operator==( Iterator const& lhs, Iterator const& rhs ) {
        assert( lhs.m_containers == rhs.m_containers );
        return lhs.m_offset == rhs.m_offset;
      }
      friend bool            operator!=( Iterator const& lhs, Iterator const& rhs ) { return !( lhs == rhs ); }
      friend difference_type operator-( Iterator const& lhs, Iterator const& rhs ) {
        assert( ( lhs.m_offset - rhs.m_offset ) % simd_t::size == 0 );
        return ( lhs.m_offset - rhs.m_offset ) / simd_t::size;
      }
    };

    using value_type        = typename Iterator::value_type;
    using gather_value_type = ZipProxy<typename ContainerTypes::template proxy_type<
        resolve_instruction_set_v<Simd>, LHCb::Pr::ProxyBehaviour::ScatterGather, ContainerTypes>...>;
    using broadcast_value_type =
        ZipProxy<typename ContainerTypes::template proxy_type<resolve_instruction_set_v<Simd>,
                                                              LHCb::Pr::ProxyBehaviour::ScalarFill, ContainerTypes>...>;

    Zip( ContainerTypes*... containers ) : m_containers( containers... ) {
      // We assume that size() and zipIdentifier() can just be taken from the
      // 0th container, now's the time to check that assumption...!
      if ( !Zipping::areSemanticallyCompatible( *containers... ) ) {
        auto info = ( std::string{ System::typeinfoName( typeid( ContainerTypes ) ) } + ", " + ... );
        throw GaudiException{ "Asked to zip containers that are not semantically compatible: " + info,
                              "LHCb::Event::Zip", StatusCode::FAILURE };
      }
      if ( !Zipping::areSameSize( *containers... ) ) {
        auto info = ( std::string{ System::typeinfoName( typeid( ContainerTypes ) ) } + ", " + ... );
        throw GaudiException{ "Asked to zip containers that are not the same size: " + info, "LHCb::Event::Zip",
                              StatusCode::FAILURE };
      }
    }

    template <SIMDWrapper::InstructionSet OtherDefaultSimd>
    Zip( Zip<OtherDefaultSimd, ContainerTypes...> old ) : m_containers{ std::move( old.m_containers ) } {}

    auto begin() const { return Iterator{ m_containers, 0 }; }
    auto end() const {
      // m_offset is incremented by dType::size each time, so repeatedly
      // incrementing begin() generally misses {m_tracks, m_tracks->size()}
      int num_chunks = ( size() + default_simd_t::size - 1 ) / default_simd_t::size;
      int max_offset = num_chunks * default_simd_t::size;
      return Iterator{ m_containers, max_offset };
    }

    auto operator[]( int i ) const { return *Iterator{ m_containers, i }; }

    /** Get a component of the zip.
     */
    template <typename T>
    auto& get() const {
      // If T is in ContainerTypes use that, otherwise try std::add_const_t<T>
      return *std::get<std::conditional_t<boost::mp11::mp_contains<std::tuple<ContainerTypes...>, T>::value, T,
                                          std::add_const_t<T>>*>( m_containers );
    }

    template <LHCb::Pr::ProxyBehaviour Behaviour>
    using zip_proxy_type = ZipProxy<
        typename ContainerTypes::template proxy_type<resolve_instruction_set_v<Simd>, Behaviour, ContainerTypes>...>;
    template <LHCb::Pr::ProxyBehaviour Behaviour>
    using const_zip_proxy_type =
        ZipProxy<typename ContainerTypes::template proxy_type<resolve_instruction_set_v<Simd>, Behaviour,
                                                              ContainerTypes const>...>;

    auto gather( typename default_simd_t::int_v const& indices, typename default_simd_t::mask_v const& mask ) {
      return std::apply(
          [&]( auto&&... args ) {
            return zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather>{
                { args,
                  typename zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather>::OffsetType{ indices, mask } }... };
          },
          m_containers );
    }
    auto gather( typename default_simd_t::int_v const& indices, typename default_simd_t::mask_v const& mask ) const {
      return std::apply(
          [&]( auto&&... args ) {
            return const_zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather>{
                { args,
                  typename zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather>::OffsetType{ indices, mask } }... };
          },
          m_containers );
    }

    auto gather2D( typename default_simd_t::int_v const& indices, typename default_simd_t::mask_v const& mask ) {
      using int_v = typename default_simd_t::int_v;
      return std::apply(
          [&]( ContainerTypes*... args ) {
            return zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather2D>{
                { std::array<ContainerTypes*, default_simd_t::size>{ args },
                  typename zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather2D>::OffsetType{ int_v{ 0 }, indices,
                                                                                                  mask } }... };
          },
          m_containers );
    }
    auto gather2D( typename default_simd_t::int_v const& indices, typename default_simd_t::mask_v const& mask ) const {
      using int_v = typename default_simd_t::int_v;
      return std::apply(
          [&]( ContainerTypes*... args ) {
            return const_zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather2D>{
                { std::array<ContainerTypes*, default_simd_t::size>{ args },
                  typename const_zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather2D>::OffsetType{
                      int_v{ 0 }, indices, mask } }... };
          },
          m_containers );
    }

    auto scalar_fill( typename zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScalarFill>::OffsetType const& offset ) {
      return std::apply(
          [&]( auto&&... args ) {
            return zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScalarFill>{ { args, offset }... };
          },
          m_containers );
    }
    auto
    scalar_fill( typename const_zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScalarFill>::OffsetType const& offset ) const {
      return std::apply(
          [&]( auto&&... args ) {
            return const_zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScalarFill>{ { args, offset }... };
          },
          m_containers );
    }

    auto size() const { return std::get<0>( m_containers )->size(); }
    auto empty() const { return std::get<0>( m_containers )->empty(); }
    auto zipIdentifier() const { return std::get<0>( m_containers )->zipIdentifier(); }

    void reserve( std::size_t size ) const {
      std::apply( [&]( auto&&... container ) { ( container->reserve( size ), ... ); }, m_containers );
    }

    void resize( std::size_t size ) {
      std::apply( [&]( auto&&... container ) { ( container->resize( size ), ... ); }, m_containers );
    }

    template <typename... ContainerTypes2>
    inline void copy_back( Zip<Simd, ContainerTypes2...> const& old, int offset, mask_v mask ) const {
      SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_BEGIN
      std::apply(
          [&]( auto&... new_container ) {
            std::apply(
                // FIXME: new_container... is added to the lambda capture as a workarround for clang 8
                [&, new_container...]( const auto&... old_container ) {
                  ( new_container->template copy_back<default_simd_t>( *old_container, offset, mask ), ... );
                },
                old.m_containers );
          },
          m_containers );
      SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_END
    }

    std::tuple<std::remove_const_t<ContainerTypes>...>
    clone( Zipping::ZipFamilyNumber zf = Zipping::generateZipIdentifier() ) const {
      return std::apply(
          [&]( auto&&... args ) {
            return std::make_tuple<std::remove_const_t<ContainerTypes>...>( { zf, *args }... );
          },
          m_containers );
    }

    /** @brief Return itself if backend is already scalar, otherwise return clone with scalar backend.
     */
    auto scalar() const {
      if constexpr ( SIMDWrapper::InstructionSet::Scalar == default_simd ) {
        return *this;
      } else {
        return with<SIMDWrapper::InstructionSet::Scalar>();
      }
    }

    /** @brief Return itself if backend is already best, otherwise return clone with chosen backend.
     *  @tparam InstrSet Chosen backend.
     */
    template <SIMDWrapper::InstructionSet InstrSet = SIMDWrapper::InstructionSet::Best>
    auto simd() const {
      if constexpr ( InstrSet == default_simd ) {
        return *this;
      } else {
        return with<InstrSet>();
      }
    }

    template <typename F>
    [[nodiscard]] auto filter( F&& filt ) const {
      auto out      = this->clone();
      using zip_t   = Zip<Simd, std::remove_const_t<ContainerTypes>...>;
      zip_t out_zip = std::apply( [&]( auto&&... args ) { return zip_t( &args... ); }, out );
      out_zip.reserve( this->size() );
      for ( auto const& chunk : *this ) {
        auto filt_mask = std::invoke( filt, chunk );
        out_zip.copy_back( *this, chunk.offset(), chunk.loop_mask() && filt_mask );
      }
      if constexpr ( sizeof...( ContainerTypes ) > 1 ) {
        return out;
      } else {
        return std::move( std::get<0>( out ) );
      }
    }

    template <typename F>
    void filterInPlace( F&& filt ) {
      auto new_size = 0;
      for ( auto const& chunk : *this ) {
        mask_v filt_mask = std::invoke( filt, chunk ) && chunk.loop_mask();

        std::apply(
            [&]( auto&&... container ) {
              ( container->dataTree()->template copy_back<default_simd_t>( new_size, *container->dataTree(),
                                                                           chunk.offset(), filt_mask ),
                ... );
            },
            m_containers );
        new_size += popcount( filt_mask );
      }
      resize( new_size );
    }

    /** @brief Get a clone of this zip with a different default SIMD backend.
     *  @tparam NewDefaultSimd New default SIMD backend.
     */
    template <SIMDWrapper::InstructionSet NewDefaultSimd>
    auto with() const {
      return Zip<NewDefaultSimd, ContainerTypes...>{ *this };
    }

    /** @brief Check if two zips refer to the same containers.
     */
    friend bool operator==( Zip const& lhs, Zip const& rhs ) { return lhs.m_containers == rhs.m_containers; }

    /** @brief Check if two zips refer to different containers.
     */
    friend bool operator!=( Zip const& lhs, Zip const& rhs ) { return !( lhs == rhs ); }

    template <SIMDWrapper::InstructionSet, typename...>
    friend struct Zip;

    container_ptr_tuple m_containers;
  };

  namespace detail {
    template <typename T>
    struct merged_object_helper {
      using tuple_t = std::tuple<T>;
      static constexpr auto decompose( T& x ) { return std::tie( x ); }
    };

    template <typename... T>
    struct merged_object_helper<std::tuple<T...>> {
      using tuple_t = std::tuple<T...>;
      static constexpr auto decompose( std::tuple<T...>& x ) { return std::tie( std::get<T>( x )... ); }
    };

    template <typename... T>
    struct merged_object_helper<std::tuple<T...> const> {
      using tuple_t = std::tuple<T const...>;
      static constexpr std::tuple<T const&...> decompose( std::tuple<T...> const& x ) {
        return std::tie( std::get<T>( x )... );
      }
    };

    template <SIMDWrapper::InstructionSet def_simd, typename... T>
    struct merged_object_helper<::LHCb::Event::Zip<def_simd, T...>> {
      using tuple_t = std::tuple<T...>;
      // Convert std::tuple<A const*, ...> to std::tuple<A const&, ...>
      static constexpr auto decompose( ::LHCb::Event::Zip<def_simd, T...>& x ) {
        return std::tie( *std::get<T*>( x.m_containers )... );
      }
    };

    template <SIMDWrapper::InstructionSet def_simd, typename... T>
    struct merged_object_helper<::LHCb::Event::Zip<def_simd, T...> const> {
      using tuple_t = std::tuple<T...>;
      // Convert std::tuple<A const*, ...> to std::tuple<A const&, ...>
      static constexpr auto decompose( ::LHCb::Event::Zip<def_simd, T...> const& x ) {
        return std::tie( *std::get<T*>( x.m_containers )... );
      }
    };

    /** Helper to sort a std::tuple of const references according to a
     *  different tuple type's ordering. e.g. given an instance of
     *    std::tuple<A const&, B const&>
     *  and the template parameter
     *    std::tuple<B, A>
     *  return an instance of std::tuple<B const&, A const&> populated from the
     *  given instance. This only works if the types A, B, ... are unique in
     *  each tuple.
     */
    template <typename>
    struct tuple_sort {};

    template <typename... SArgs>
    struct tuple_sort<std::tuple<SArgs...>> {
      template <typename... Args>
      constexpr static auto apply( std::tuple<Args...>&& in ) {
        return std::tuple<SArgs*...>{ &std::get<SArgs&>( in )... };
      }
    };

    /** Comparison of two types using Boost CTTI
     */
    template <typename T1, typename T2>
    struct ctti_sort {
      using tidx                  = boost::typeindex::ctti_type_index;
      constexpr static bool value = tidx::type_id<T1>() < tidx::type_id<T2>();
    };

    /** Do some template magic to get from the parameter pack (Args...)
     *  corresponding to the arguments of make_zip() to the sorted list of
     *  unpacked types that will be passed to Zip as template parameters.
     *  This involves applying unpacking rules for std::tuple and Zip arguments
     *  and then sorting the result using Boost CTTI.
     *
     *  e.g.
     *    sorted_t<std::tuple <B, C>, A>
     *  might yield
     *    std::tuple<A, B, C>
     *  (although the actual ordering is unspecified, we rely on Boost to
     *  ensure that it is stable)
     */
    template <typename... Args>
    using sorted_t =
        boost::mp11::mp_sort<boost::mp11::mp_append<typename detail::merged_object_helper<Args>::tuple_t...>,
                             ctti_sort>;

    /** Helper for full_zip_t
     */
    template <typename>
    struct full_zip {};

    template <typename... Ts>
    struct full_zip<std::tuple<Ts...>> {
      template <SIMDWrapper::InstructionSet def_simd>
      using type = Zip<def_simd, Ts...>;
    };

    /** Deduce the return type of make_zip from a parameter pack representing
     *  its arguments. This is a relatively trivial wrapper around sorted_t.
     *  The "full" in the name is because the user is required to explicitly
     *  specify the extra template parameter def_simd.
     *
     *  e.g.
     *    full_zip_t<def_simd, std::tuple<B, C>, A>
     *  might yield
     *    Zip<def_simd, A, B, C>
     */
    template <SIMDWrapper::InstructionSet def_simd, typename... Args>
    using full_zip_t = typename full_zip<sorted_t<Args...>>::template type<def_simd>;
  } // namespace detail

  template <SIMDWrapper::InstructionSet def_simd = SIMDWrapper::InstructionSet::Best, typename... ContainerTypes>
  auto make_zip( ContainerTypes&... containers ) {
    // If some of ContainerTypes... are std::tuple<T...> then try to unpack, e.g.
    // ContainerTypes = {A, std::tuple<B, C>} (types)
    //         tracks = {a, bc}              (const&)
    // Should unpack to
    // {A, B, C}                                                     (types)
    // {a, static_cast<B const&>( bc ), static_cast<C const&>( bc )} (const&)

    // Get something like std::tuple<B, A, C> that has been unpacked following
    // the rules above and sorted using CTTI. Reordering the arguments passed
    // to this function (i.e. "tracks") would not affect sorted_t.
    using sorted_t = detail::sorted_t<ContainerTypes...>;

    // Get the full Zip<def_simd, B, A, C> type that we'll return
    using ret_t = typename detail::full_zip<sorted_t>::template type<def_simd>;

    // Unpack the arguments, as specified above, into a tuple of const&. This
    // will generally not be ordered correctly (i.e. it will be something like
    // std::tuple<A const&, B const&, C const&> that doesn't match sorted_t).
    auto expanded_containers =
        std::tuple_cat( detail::merged_object_helper<ContainerTypes>::decompose( containers )... );

    // Reorder to make a tuple of references in the order demanded by sorted_t
    auto sorted_containers = detail::tuple_sort<sorted_t>::apply( std::move( expanded_containers ) );

    // Finally, construct the zip object using this sorted list of references
    return std::make_from_tuple<ret_t>( std::move( sorted_containers ) );
  }

  // Helper to get the type of a zip of the types T...
  template <typename... T>
  using zip_t = detail::full_zip_t<SIMDWrapper::InstructionSet::Best, T const...>;

  template <SIMDWrapper::InstructionSet simd, typename... T>
  using simd_zip_t = detail::full_zip_t<simd, T const...>;

  /** @brief Represent adaptable zip and proxy types
   *
   * This helper structured type allows to redefine the zip and proxy
   * types for a given instruction set and proxy behaviours. This
   * design was inspired from the necessity of implementing functions
   * to be selected by ADL, where the instruction set and behaviours
   * can be different at different points of the code but we don't want
   * to define the same function for every combination of the two. An
   * example is:
   *
   * @code{.cpp}
   *
   * using Container = zip_t<T ...>;
   *
   * // this is implicitly using the Best instruction set
   * template <LHCb::Pr::ProxyBehaviour Behaviour>
   * auto some_function( Container::zip_proxy_type<Behaviour> const& ) { ... }
   *
   * @endcode
   *
   * If we attempt to call "some_function" with a container using the Scalar
   * instruction set it will fail. Instead of specializing "some_function" for
   * every possible instruction set, these adapters allow to write
   *
   * @code{.cpp}
   *
   * using Container = zip_t<T ...>;
   *
   * template <SIMDWrapper::InstructionSet instruction_set, LHCb::Pr::ProxyBehaviour Behaviour>
   * auto some_function( typename zip_proxy_types_adapter<Container>::const_proxy_type<instruction_set, behaviour>
   * const& ) { ... }
   *
   * @endcode
   *
   * Note that we can not use "const_zip_proxy_type_adapter" because the C++ template
   * deduction rules will not be able to determine the values of "instruction_type"
   * and "behaviour".
   */
  template <class>
  struct zip_proxy_types_adapter;

  template <SIMDWrapper::InstructionSet original_instruction_set, class... T>
  struct zip_proxy_types_adapter<Zip<original_instruction_set, T...>> {

    template <SIMDWrapper::InstructionSet instruction_set, Pr::ProxyBehaviour behaviour>
    using proxy_type = ZipProxy<typename T::template proxy_type<instruction_set, behaviour, T>...>;

    template <SIMDWrapper::InstructionSet instruction_set, Pr::ProxyBehaviour behaviour>
    using const_proxy_type = ZipProxy<typename T::template proxy_type<instruction_set, behaviour, T const>...>;
  };

  template <class ZipType, SIMDWrapper::InstructionSet instruction_set, Pr::ProxyBehaviour behaviour>
  struct zip_proxy_type_adapter {
    using type = typename zip_proxy_types_adapter<ZipType>::template proxy_type<instruction_set, behaviour>;
  };

  template <class ZipType, SIMDWrapper::InstructionSet instruction_set, Pr::ProxyBehaviour behaviour>
  using zip_proxy_type_adapter_t = typename zip_proxy_type_adapter<ZipType, instruction_set, behaviour>::type;

  template <class ZipType, SIMDWrapper::InstructionSet instruction_set, Pr::ProxyBehaviour behaviour>
  struct const_zip_proxy_type_adapter {
    using type = typename zip_proxy_types_adapter<ZipType>::template const_proxy_type<instruction_set, behaviour>;
  };

  template <class ZipType, SIMDWrapper::InstructionSet instruction_set, Pr::ProxyBehaviour behaviour>
  using const_zip_proxy_type_adapter_t =
      typename const_zip_proxy_type_adapter<ZipType, instruction_set, behaviour>::type;

  template <typename>
  struct is_zip : std::false_type {};

  template <SIMDWrapper::InstructionSet def_simd, typename... Ts>
  struct is_zip<Zip<def_simd, Ts...>> : std::true_type {};

  // Helper to determine if a given type is a Zip<...> type.
  template <typename T>
  inline constexpr bool is_zip_v = is_zip<T>::value;

  namespace detail {
    template <typename, typename = void>
    struct is_zippable : std::false_type {};

    template <typename... Ts>
    struct is_zippable<std::tuple<Ts...>,
                       std::void_t<typename Ts::template proxy_type<resolve_instruction_set_v<SIMDWrapper::Best>,
                                                                    LHCb::Pr::ProxyBehaviour::Contiguous, Ts>...,
                                   decltype( make_zip( std::declval<std::add_lvalue_reference_t<Ts>>()... ) )>>
        : std::true_type {};
  } // namespace detail

  // Helper to determine if the given types can be zipped together.
  template <typename... Ts>
  inline constexpr bool is_zippable_v = detail::is_zippable<std::tuple<std::remove_cv_t<Ts>...>>::value;
} // namespace LHCb::Event
