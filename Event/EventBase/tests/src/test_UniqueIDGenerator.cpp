/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestrecobjectid
#include "Event/UniqueIDGenerator.h"
#include "LHCbMath/SIMDWrapper.h"
#include <boost/test/unit_test.hpp>
#include <type_traits>

static_assert( std::is_default_constructible_v<LHCb::UniqueIDGenerator> );
static_assert( std::is_move_constructible_v<LHCb::UniqueIDGenerator> );
static_assert( std::is_move_assignable_v<LHCb::UniqueIDGenerator> );

static_assert( !std::is_copy_constructible_v<LHCb::UniqueIDGenerator> );
static_assert( !std::is_copy_assignable_v<LHCb::UniqueIDGenerator> );
static_assert( !std::is_convertible_v<LHCb::UniqueIDGenerator::ID<int>, int> );
static_assert( !std::is_convertible_v<LHCb::UniqueIDGenerator::ID<unsigned int>, unsigned int> );
static_assert(
    !std::is_convertible_v<LHCb::UniqueIDGenerator::ID<int>, LHCb::UniqueIDGenerator::ID<int>::underlying_type> );

// So we can use the same interface for simd integral types and "int"
constexpr bool all( bool b ) { return b; }

template <class IntType>
void test_function() {

  LHCb::UniqueIDGenerator       rid;
  LHCb::UniqueIDGenerator const crid;

  assert( all( rid.generate<IntType>() != rid.generate<IntType>() ) );
  assert( all( crid.generate<IntType>() != crid.generate<IntType>() ) );

  auto f = rid.generate<IntType>();
  auto s = crid.generate<IntType>();

  try {
    (void)( f == s );
    // must have raised std::runtime_error
    assert( false );
  } catch ( std::runtime_error& e ) {}

  assert( all( f.generator_tag() != s.generator_tag() ) );
  assert( all( f.value() == s.value() ) );

  try {
    for ( int i = 0; i < std::numeric_limits<int>::max(); ++i ) rid.generate<IntType>();
    // must have raised std::overflow_error
    assert( false );
  } catch ( std::overflow_error& e ) {}
}

BOOST_AUTO_TEST_CASE( test_recobjectid_int ) { test_function<int>(); }
BOOST_AUTO_TEST_CASE( test_recobjectid_scalar ) {
  test_function<typename SIMDWrapper::type_map_t<SIMDWrapper::InstructionSet::Scalar>::int_v>();
}
BOOST_AUTO_TEST_CASE( test_recobjectid_best ) {
  test_function<typename SIMDWrapper::type_map_t<SIMDWrapper::InstructionSet::Best>::int_v>();
}
