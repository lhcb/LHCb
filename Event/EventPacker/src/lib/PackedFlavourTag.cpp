/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedFlavourTag.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void FlavourTagPacker::pack( const Data& ft, PackedData& pft, PackedDataVector& pfts ) const {
  const auto ver = pfts.packingVersion();
  if ( !isSupportedVer( ver ) ) return;

  // fill ppart key from part
  pft.key = ft.key();

  // Decision
  pft.decision = to_underlying( ft.decision() );
  pft.omega    = StandardPacker::fraction( ft.omega() );

  // OS decision
  pft.decisionOS = to_underlying( ft.decisionOS() );
  pft.omegaOS    = StandardPacker::fraction( ft.omegaOS() );

  // tagging particle
  if ( ft.taggedB() ) { pft.taggedB = StandardPacker::reference64( &pfts, ft.taggedB() ); }

  // Taggers
  pft.firstTagger = pfts.taggers().size();
  pfts.taggers().reserve( pfts.taggers().size() + ft.taggers().size() );
  for ( const auto& T : ft.taggers() ) {
    // make a new packed tagger object
    auto& ptagger = pfts.taggers().emplace_back();

    // save data members
    ptagger.type     = T.type();
    ptagger.decision = to_underlying( T.decision() );
    ptagger.omega    = StandardPacker::fraction( T.omega() );

    // tagging particles
    ptagger.firstTagP = pfts.taggeringPs().size();
    if ( m_pack_tagparticles_refs ) {
      pfts.taggeringPs().reserve( pfts.taggeringPs().size() + T.taggerParts().size() );
      for ( const auto& TP : T.taggerParts() ) {
        if ( TP.target() ) { pfts.taggeringPs().push_back( StandardPacker::reference64( &pfts, TP ) ); }
      }
    }
    ptagger.lastTagP = pfts.taggeringPs().size();

    // for packing versions 1 and above add mva and charge to PackedTagger
    if ( ver > 0 ) {
      ptagger.mvaValue = StandardPacker::mva( T.mvaValue() );
      ptagger.charge   = StandardPacker::fraction( T.charge() );
    }
  }

  pft.lastTagger = pfts.taggers().size();
}

StatusCode FlavourTagPacker::unpack( const PackedData& pft, Data& ft, const PackedDataVector& pfts,
                                     DataVector& fts ) const {

  bool ok = true;

  const auto ver = pfts.packingVersion();
  if ( !isSupportedVer( ver ) )
    return StatusCode::FAILURE; // TODO: move to component, and define a dedicated error code

  auto unpack_ref = StandardPacker::UnpackRef( &pfts, &fts );
  // Decision
  ft.setDecision( FlavourTag::TagResult{ pft.decision } );
  ft.setOmega( StandardPacker::fraction( pft.omega ) );

  // OS Decision
  ft.setDecisionOS( FlavourTag::TagResult{ pft.decisionOS } );
  ft.setOmegaOS( StandardPacker::fraction( pft.omegaOS ) );

  // Tagging B
  if ( -1 != pft.taggedB ) {
    if ( auto b = unpack_ref( pft.taggedB ); b ) {
      ft.setTaggedB( b );
    } else {
      parent().error() << "Corrupt FlavourTag Particle SmartRef found" << endmsg;
      ok = false;
    }
  }

  // Taggers
  auto&      taggers  = ft.taggers();
  const auto taggersR = Packer::subrange( pfts.taggers(), pft.firstTagger, pft.lastTagger );
  taggers.reserve( taggersR.size() );
  for ( const auto& ptagger : taggersR ) {

    // Make a new tagger
    auto& tagger = taggers.emplace_back();

    // set the tagger members
    tagger.setType( ptagger.type );
    tagger.setDecision( Tagger::TagResult{ ptagger.decision } );
    tagger.setOmega( StandardPacker::fraction( ptagger.omega ) );

    // tagging particles
    for ( const auto& iP : Packer::subrange( pfts.taggeringPs(), ptagger.firstTagP, ptagger.lastTagP ) ) {
      if ( auto p = unpack_ref( iP ); p ) {
        tagger.addToTaggerParts( p );
      } else {
        parent().error() << "Corrupt FlavourTag Tagging Particle SmartRef found" << endmsg;
        ok = false;
      }
    }

    // for packing versions 1 and above retrieve mva and charge to PackedTagger
    if ( ver > 0 ) {
      tagger.setMvaValue( StandardPacker::mva( ptagger.mvaValue ) );
      tagger.setCharge( StandardPacker::fraction( ptagger.charge ) );
    }
  }
  return StatusCode{ ok };
}

StatusCode FlavourTagPacker::unpack( const PackedDataVector& pfts, DataVector& fts ) const {

  const auto ver = pfts.packingVersion();
  if ( !isSupportedVer( ver ) ) return StatusCode::FAILURE; // TODO: define dedicated error code

  fts.reserve( pfts.data().size() );

  bool sc = true;
  for ( const auto& pft : pfts.data() ) {
    // make and save new pid in container
    auto* ft = new Data();
    fts.insert( ft, pft.key );
    // Fill data from packed object
    sc &= unpack( pft, *ft, pfts, fts ).isSuccess();
  }
  if ( !sc ) { fts.clear(); }

  return StatusCode{ sc };
}

StatusCode FlavourTagPacker::check( const Data* dataA, const Data* dataB ) const {
  // assume OK from the start
  bool ok = true;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Checks

  // key
  ok &= ch.compareInts( "Key", dataA->key(), dataB->key() );

  // decision
  ok &= ch.compareInts( "Decision", dataA->decision(), dataB->decision() );
  // omega
  ok &= ch.compareFloats( "Omega", dataA->omega(), dataB->omega(), 1e-4 );

  // decisionOS
  ok &= ch.compareInts( "DecisionOS", dataA->decisionOS(), dataB->decisionOS() );
  // omegaOS
  ok &= ch.compareFloats( "OmegaOS", dataA->omegaOS(), dataB->omegaOS(), 1e-4 );

  // tagging B
  ok &= ch.comparePointers( "TaggedB", dataA->taggedB(), dataB->taggedB() );

  // Taggers
  const bool sizeOK = ch.compareInts( "#Taggers", dataA->taggers().size(), dataB->taggers().size() );
  ok &= sizeOK;
  if ( sizeOK ) {
    auto iA( dataA->taggers().begin() ), iB( dataB->taggers().begin() );
    for ( ; iA != dataA->taggers().end() && iB != dataB->taggers().end(); ++iA, ++iB ) {
      ok &= ch.compareInts( "TaggerType", iA->type(), iB->type() );
      ok &= ch.compareInts( "TaggerDecision", iA->decision(), iB->decision() );
      ok &= ch.compareFloats( "TaggerOmega", iA->omega(), iB->omega() );
      ok &= ch.compareFloats( "TaggerMVAValue", iA->mvaValue(), iB->mvaValue() );
      ok &= ch.compareFloats( "TaggerCharge", iA->charge(), iB->charge() );

      const bool pSizeOK = ch.compareInts( "TaggerPSize", iA->taggerParts().size(), iB->taggerParts().size() );
      ok &= pSizeOK;
      if ( pSizeOK ) {
        auto iPA( iA->taggerParts().begin() ), iPB( iB->taggerParts().begin() );
        for ( ; iPA != iA->taggerParts().end() && iPB != iB->taggerParts().end(); ++iPA, ++iPB ) {
          ok &= ch.comparePointers( "TaggerParts", &**iPA, &**iPB );
        }
      }
    }
  }

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA->parent() && dataA->parent()->registry() ? dataA->parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with FlavourTag data packing :-" << endmsg
                       << "  Original FlavourTag key=" << dataA->key() << " in '" << loc << "'" << endmsg << dataA
                       << endmsg << "  Unpacked FlavourTag" << endmsg << dataB << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

namespace LHCb {
  StatusCode unpack( Gaudi::Algorithm const* parent, const FlavourTagPacker::PackedDataVector& in,
                     FlavourTagPacker::DataVector& out ) {
    return FlavourTagPacker{ parent }.unpack( in, out );
  }

} // namespace LHCb
