/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedMCRichHit.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void MCRichHitPacker::pack( const DataVector& hits, PackedDataVector& phits ) const {
  const auto ver = phits.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  if ( ver == 0 ) throw GaudiException( "Unsupported packing version", __PRETTY_FUNCTION__, StatusCode::FAILURE );
  phits.data().reserve( hits.size() );
  for ( const auto* hit : hits ) {
    auto& phit     = phits.data().emplace_back();
    phit.x         = StandardPacker::position( hit->entry().x() );
    phit.y         = StandardPacker::position( hit->entry().y() );
    phit.z         = StandardPacker::position( hit->entry().z() );
    phit.energy    = StandardPacker::energy( hit->energy() );
    phit.tof       = StandardPacker::time( hit->timeOfFlight() );
    phit.sensDetID = hit->sensDetID().key();
    phit.history   = hit->historyCode();
    if ( hit->mcParticle() ) phit.mcParticle = StandardPacker::reference64( &phits, hit->mcParticle() );
  }
}

StatusCode MCRichHitPacker::unpack( const PackedDataVector& phits, DataVector& hits ) const {
  bool       ok  = true;
  const auto ver = phits.packingVersion();
  if ( !isSupportedVer( ver ) ) return StatusCode::FAILURE; // TODO: define dedicate error code
  hits.reserve( phits.data().size() );
  auto unpack_ref = StandardPacker::UnpackRef{ &phits, &hits, StandardPacker::UnpackRef::Use32{ ver == 0 } };
  for ( const auto& phit : phits.data() ) {
    // make and save new hit in container
    auto* hit = new Data();
    hits.add( hit );
    // Fill data from packed object
    hit->setEntry( Gaudi::XYZPoint( StandardPacker::position( phit.x ), StandardPacker::position( phit.y ),
                                    StandardPacker::position( phit.z ) ) );
    hit->setEnergy( StandardPacker::energy( phit.energy ) );
    hit->setTimeOfFlight( StandardPacker::time( phit.tof ) );
    hit->setSensDetID( LHCb::RichSmartID( phit.sensDetID ) );
    hit->setHistoryCode( phit.history );
    if ( -1 != phit.mcParticle ) {
      if ( auto p = unpack_ref( phit.mcParticle ); p ) {
        hit->setMCParticle( p );
      } else {
        parent().error() << "Corrupt MCRichHit MCParticle SmartRef detected." << endmsg;
        ok = false;
      }
    }
  }
  return StatusCode{ ok };
}

StatusCode MCRichHitPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Loop over data containers together and compare
  auto iA( dataA.begin() ), iB( dataB.begin() );
  for ( ; iA != dataA.end() && iB != dataB.end(); ++iA, ++iB ) {
    // assume OK from the start
    bool ok = true;
    // Hit position
    ok &= ch.comparePoints( "Entry Point", ( *iA )->entry(), ( *iB )->entry() );
    // energy
    ok &= ch.compareEnergies( "Energy", ( *iA )->energy(), ( *iB )->energy() );
    // tof
    ok &= ch.compareDoubles( "TOF", ( *iA )->timeOfFlight(), ( *iB )->timeOfFlight() );
    // Detector ID
    ok &= ch.compareInts( "SensDetID", ( *iA )->sensDetID(), ( *iB )->sensDetID() );
    // History code
    ok &= ch.compareInts( "HistoryCode", ( *iA )->historyCode(), ( *iB )->historyCode() );
    // MCParticle reference
    ok &= ch.comparePointers( "MCParticle", ( *iA )->mcParticle(), ( *iB )->mcParticle() );

    // force printout for tests
    // ok = false;
    // If comparison not OK, print full information
    if ( !ok ) {
      parent().warning() << "Problem with MCRichHit data packing :-" << endmsg << "  Original Hit : " << **iA << endmsg
                         << "  Unpacked Hit : " << **iB << endmsg;
      sc = StatusCode::FAILURE;
    }
  }

  // Return final status
  return sc;
}
