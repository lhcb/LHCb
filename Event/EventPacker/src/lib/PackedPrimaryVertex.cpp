/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedPrimaryVertex.h"
#include "Event/PackedEventChecks.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"
#include <string>

namespace {
  template <typename TYPE>
  static auto sqrt_or_0( const TYPE x ) {
    return ( x > TYPE( 0 ) ? std::sqrt( x ) : TYPE( 0 ) );
  }
} // namespace

using namespace LHCb;

void PrimaryVertexPacker::pack( const Data& vert, PackedData& pvert, PackedDataVector& pverts ) const {
  pverts.setVersion( 1 );

  const auto ver = pverts.version();
  if ( !isSupportedVer( ver ) ) return;

  // Key
  pvert.key = vert.key();

  pvert.chi2 = StandardPacker::fltPacked( vert.chi2() );
  pvert.nDoF = vert.nDoF();
  pvert.x    = StandardPacker::position( vert.position().x() );
  pvert.y    = StandardPacker::position( vert.position().y() );
  pvert.z    = StandardPacker::position( vert.position().z() );

  // convariance Matrix
  const auto err0 = sqrt_or_0( vert.covMatrix()( 0, 0 ) );
  const auto err1 = sqrt_or_0( vert.covMatrix()( 1, 1 ) );
  const auto err2 = sqrt_or_0( vert.covMatrix()( 2, 2 ) );
  pvert.cov00     = StandardPacker::position( err0 );
  pvert.cov11     = StandardPacker::position( err1 );
  pvert.cov22     = StandardPacker::position( err2 );
  pvert.cov10     = StandardPacker::fraction( vert.covMatrix()( 1, 0 ), err1 * err0 );
  pvert.cov20     = StandardPacker::fraction( vert.covMatrix()( 2, 0 ), err2 * err0 );
  pvert.cov21     = StandardPacker::fraction( vert.covMatrix()( 2, 1 ), err2 * err1 );
}

StatusCode PrimaryVertexPacker::unpack( const PackedData& pvert, Data& vert, const PackedDataVector& pverts,
                                        DataVector& /*verts*/ ) const {
  const auto ver = pverts.version();
  if ( !isSupportedVer( ver ) ) return StatusCode::SUCCESS; // TODO: defined dedicated error code

  vert.setChi2AndDoF( StandardPacker::fltPacked( pvert.chi2 ), pvert.nDoF );
  vert.setPosition( Gaudi::XYZPoint( StandardPacker::position( pvert.x ), StandardPacker::position( pvert.y ),
                                     StandardPacker::position( pvert.z ) ) );

  // convariance Matrix
  const auto err0 = StandardPacker::position( pvert.cov00 );
  const auto err1 = StandardPacker::position( pvert.cov11 );
  const auto err2 = StandardPacker::position( pvert.cov22 );
  auto&      cov  = *( const_cast<Gaudi::SymMatrix3x3*>( &vert.covMatrix() ) );
  cov( 0, 0 )     = err0 * err0;
  cov( 1, 0 )     = err1 * err0 * StandardPacker::fraction( pvert.cov10 );
  cov( 1, 1 )     = err1 * err1;
  cov( 2, 0 )     = err2 * err0 * StandardPacker::fraction( pvert.cov20 );
  cov( 2, 1 )     = err2 * err1 * StandardPacker::fraction( pvert.cov21 );
  cov( 2, 2 )     = err2 * err2;

  return StatusCode::SUCCESS;
}

StatusCode PrimaryVertexPacker::unpack( const PackedDataVector& pverts, DataVector& verts ) const {
  verts.reserve( pverts.data().size() );
  bool sc = true;
  for ( const auto& pvert : pverts.data() ) {
    // make and save new pid in container
    auto* vert = new Data();
    verts.insert( vert, pvert.key );
    // Fill data from packed object
    sc &= unpack( pvert, *vert, pverts, verts ).isSuccess();
  }
  if ( !sc ) { verts.clear(); }
  return StatusCode{ sc };
}

StatusCode PrimaryVertexPacker::check( const Data& dataA, const Data& dataB ) const {
  // checker
  const DataPacking::DataChecks ch( parent() );

  bool isOK = true;

  // key
  isOK &= ch.compareInts( "Key", dataA.key(), dataB.key() );
  isOK &= ch.compareFloats( "Chi2", dataA.chi2(), dataB.chi2() );
  isOK &= ch.compareInts( "nDOF", dataA.nDoF(), dataB.nDoF() );

  // reference position
  isOK &= ch.comparePoints( "ReferencePoint", dataA.position(), dataB.position() );

  // covariance matrix
  const std::array<double, 3> tolDiagPosCov = { { Packer::POSITION_TOL, Packer::POSITION_TOL, Packer::POSITION_TOL } };
  isOK &= ch.compareCovMatrices<Gaudi::SymMatrix3x3, 3>( "PosCov", dataA.covMatrix(), dataB.covMatrix(), tolDiagPosCov,
                                                         Packer::FRACTION_TOL );
  return ( isOK ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

namespace LHCb {
  StatusCode unpack( Gaudi::Algorithm const* parent, const PackedPrimaryVertices& in,
                     LHCb::PrimaryVertex::Container& out ) {
    return PrimaryVertexPacker{ parent }.unpack( in, out );
  }
} // namespace LHCb
