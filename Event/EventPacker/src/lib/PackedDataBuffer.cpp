/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "ZSTDCompression.h"

#include "Event/PackedDataBuffer.h"

#include "RZip.h"

namespace LHCb::Hlt::PackedData {

  bool ByteBuffer::compress( Compression compression, int level, buffer_type& output ) const {
    int output_size = 0;
    int input_size  = m_buffer.size();
    int buffer_size = input_size;
    output.clear();
    output.resize( buffer_size ); // TODO get rid of unneeded initialization

    zipMultipleAlgorithm( level, &input_size, (char*)m_buffer.data(), &buffer_size, (char*)output.data(), &output_size,
                          compression );

    if ( output_size == 0 || output_size > input_size ) {
      // the input cannot be compressed or size would be greater than original
      output_size = 0;
    }
    output.resize( output_size );
    return output_size > 0;
  }

  bool ByteBuffer::init( buffer_view data, bool compressed ) {
    m_pos = 0;
    if ( !compressed ) {
      assert( m_buffer.data() != data.data() );
      m_buffer.assign( data.begin(), data.end() );
      return true;
    }
    if ( data.size() < 9 ) {
      // unzip_header _will_ access the first 9 bytes (unconditionally)...
      return false;
    }
    int  data_size   = 0;
    int  output_size = 0;
    auto hdrVersion  = unzip_header( data.size(), &data_size, (unsigned char*)data.data(), &output_size );
    if ( hdrVersion == 0 ) {
      // invalid header...
      return false;
    }
    if ( static_cast<size_t>( data_size ) != data.size() ) {
      // recorded size is not the size we have been given
      return false;
    }
    m_buffer.resize( output_size );
    int buffer_size = m_buffer.size();
    unzip( hdrVersion, &data_size, (unsigned char*)data.data(), &buffer_size, (unsigned char*)m_buffer.data(),
           &output_size );
    return static_cast<size_t>( output_size ) ==
           m_buffer.size(); // did we find exactly the amount of (uncompressed) data expected?
  }

} // namespace LHCb::Hlt::PackedData
