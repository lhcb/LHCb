/*****************************************************************************\
* (c) Copyright 2021, Matthew Rodusek (bitwizeshift)
*
* based on https://bitwizeshift.github.io/posts/2021/03/09/getting-an-unmangled-type-name-at-compile-time
*
\*****************************************************************************/
#pragma once
#include <array> // std::array
#include <string>
#include <string_view>
#include <utility> // std::index_sequence

namespace details {
  template <std::size_t... Is>
  constexpr auto as_array( std::string_view str, std::index_sequence<Is...> ) {
    return std::array{ str[Is]... };
  }

  template <typename T>
  constexpr auto type_name_array() {
#if defined( __clang__ )
    constexpr auto prefix   = std::string_view{ "[T = " };
    constexpr auto suffix   = std::string_view{ "]" };
    constexpr auto function = std::string_view{ __PRETTY_FUNCTION__ };
#elif defined( __GNUC__ )
    constexpr auto prefix   = std::string_view{ "with T = " };
    constexpr auto suffix   = std::string_view{ "]" };
    constexpr auto function = std::string_view{ __PRETTY_FUNCTION__ };
#elif defined( _MSC_VER )
    constexpr auto prefix   = std::string_view{ "type_name_array<" };
    constexpr auto suffix   = std::string_view{ ">(void)" };
    constexpr auto function = std::string_view{ __FUNCSIG__ };
#else
#  error Unsupported compiler
#endif

    constexpr auto start = function.find( prefix ) + prefix.size();
    constexpr auto end   = function.rfind( suffix );
    static_assert( start < end );

    constexpr auto name = function.substr( start, ( end - start ) );
    return as_array( name, std::make_index_sequence<name.size()>{} );
  }

  template <typename T>
  struct type_name_holder {
    static inline constexpr auto value = type_name_array<T>();
  };
} // namespace details

template <typename T>
constexpr std::string_view type_name() {
  constexpr auto& value = details::type_name_holder<T>::value;
  return std::string_view{ value.data(), value.size() };
}
