/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "BufferSOAPackerBaseAlg.h"
#include "Event/HltDecReports.h"
#include "Event/PackedData.h"
#include "Event/PackedDataBuffer.h"
#include "Event/PackedDataChecksum.h"
#include "Event/PackedEventChecks.h"
#include "Event/TrackEnums.h"
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/DataObjID.h"
#include "Kernel/IIndexedANNSvc.h"
#include "Kernel/TaggedBool.h"
#include "RegistryWrapper.h"
#include "Traits.h"
#include <LHCbAlgs/MergingTransformer.h>
#include <algorithm>
#include <cassert>
#include <map>
#include <numeric>
#include <type_traits>

namespace {
  template <class F>
  constexpr auto is_callable( F&& ) {
    return []( auto&&... args ) constexpr { return std::is_invocable<F&&, decltype( args )...>{}; };
  }

  template <typename F, typename Value, typename... C>
  void enumerate( F f, Value i, C&&... c ) {
    ( f( i++, std::forward<C>( c ) ), ... );
  }

  template <typename C>
  auto size( C const& c ) -> decltype( c.size() ) {
    return c.size();
  }

  using DataObjIDMapper_t = std::map<DataObjID, DataObjID>;

  template <typename Set>
  static const std::set<typename Set::value_type, typename Set::value_compare, typename Set::allocator_type> empty_set =
      {};

  namespace details_ {
    template <typename ValueRange, typename S>
    class ContainerRange {
      struct Sentinel {};
      S const*   m_parent;
      ValueRange m_current;

    public:
      ContainerRange( S const& parent ) : m_parent{ &parent }, m_current{ m_parent, m_parent->begin() } {}

      auto     begin() const { return *this; }
      Sentinel end() const { return {}; }
      bool     operator!=( Sentinel ) const { return m_current.begin() != m_parent->end(); }

      ContainerRange& operator++() {
        m_current = ValueRange{ m_parent, m_current.end() };
        return *this;
      }
      ValueRange const& operator*() const { return m_current; }
    };

    template <typename ValueRange, typename C>
    class EmptyContainerRange {
      struct Sentinel                             final {};
      std::optional<ValueRange>                   m_range;
      typename std::set<C const*>::const_iterator m_current;
      typename std::set<C const*>::const_iterator m_end;

    public:
      EmptyContainerRange( std::set<C const*> const& c ) : m_current{ c.begin() }, m_end{ c.end() } {
        if ( m_current != m_end ) m_range.emplace( *m_current );
      }
      auto                 begin() const { return *this; }
      Sentinel             end() const { return {}; }
      bool                 operator!=( Sentinel ) const { return m_current != m_end; }
      EmptyContainerRange& operator++() {
        ++m_current;
        if ( m_current != m_end ) m_range.emplace( *m_current );
        return *this;
      }
      ValueRange const& operator*() const { return m_range.value(); }
    };
  } // namespace details_

  template <typename Range1, typename Range2>
  struct concat_t {
    Range1 m_r1;
    Range2 m_r2;

    struct Sentinel final {};
    struct Iterator {
      decltype( m_r1.begin() ) m_i1;
      decltype( m_r1.end() )   m_s1;
      decltype( m_r2.begin() ) m_i2;
      decltype( m_r2.end() )   m_s2;
      static_assert( std::is_same_v<decltype( *m_i1 ), decltype( *m_i2 )> );
      bool      operator!=( Sentinel ) const { return m_i1 != m_s1 || m_i2 != m_s2; }
      Iterator& operator++() {
        if ( m_i1 != m_s1 ) {
          ++m_i1;
        } else {
          ++m_i2;
        }
        return *this;
      }
      decltype( auto ) operator*() const { return m_i1 != m_s1 ? *m_i1 : *m_i2; }
    };

    concat_t( Range1&& r1, Range2&& r2 ) : m_r1{ std::move( r1 ) }, m_r2{ std::move( r2 ) } {}
    concat_t( const concat_t& )            = delete;
    concat_t& operator=( const concat_t& ) = delete;
    concat_t( concat_t&& )                 = delete;
    concat_t& operator=( concat_t&& )      = delete;

    Iterator begin() const { return { m_r1.begin(), m_r1.end(), m_r2.begin(), m_r2.end() }; }
    Sentinel end() const { return {}; }
  };
  template <typename R1, typename R2>
  concat_t( R1&&, R2&& ) -> concat_t<R1, R2>;

} // namespace

namespace Gaudi::Parsers {
  StatusCode parse( std::map<std::string, DataObjIDColl>& result, const std::string& s ) {
    std::map<std::string, std::vector<std::string>> t;
    return parse( t, s ).andThen( [&] {
      result.clear();
      for ( const auto& [k, v] : t ) {
        DataObjIDColl col;
        std::transform( v.begin(), v.end(), std::inserter( col, col.end() ),
                        []( const auto& i ) { return DataObjID{ i }; } );
        result.emplace( k, col );
      }
    } );
  }

  StatusCode parse( std::pair<DataObjID, DataObjID>& result, const std::pair<std::string, std::string>& kv ) {
    DataObjID k;
    DataObjID v;
    return parse( k, kv.first ).andThen( [&] { return parse( v, kv.second ); } ).andThen( [&] { result = { k, v }; } );
  }

  StatusCode parse( DataObjIDMapper_t& result, const std::string& s ) {
    std::map<std::string, std::string> t;
    return parse( t, s ).andThen( [&] {
      result.clear();
      return std::accumulate( t.begin(), t.end(), StatusCode{ StatusCode::SUCCESS },
                              [&]( StatusCode sc, const auto& kv ) {
                                return sc.andThen( [&] {
                                  std::pair<DataObjID, DataObjID> p;
                                  return parse( p, kv ).andThen( [&] { result.emplace( p ); } );
                                } );
                              } );
    } );
  }
} // namespace Gaudi::Parsers

namespace LHCb {

  namespace {
    namespace Traits = Packers::Traits;

    const Gaudi::StringKey PackedObjectLocations{ "PackedObjectLocations" };

    using Buffer = Hlt::PackedData::PackedDataOutBuffer;
    template <typename T>
    using VOC = Gaudi::Functional::vector_of_const_<T>;

    struct Counters {
      Gaudi::Accumulators::SummingCounter<unsigned int> nbPackedData;
      Gaudi::Accumulators::SummingCounter<unsigned int> nbBufferData;
      Gaudi::Accumulators::SummingCounter<unsigned int> nContainersPacked;

      Counters( Gaudi::Algorithm const* parent, std::string_view name )
          : nbPackedData{ parent, fmt::format( "{} # of packed entries", name ) }
          , nbBufferData{ parent, fmt::format( "{} buffer size", name ) }
          , nContainersPacked{ parent, fmt::format( "{} # containers packed", name ) } {}
    };

    template <typename... Containers>
    class CountersFor {
      std::array<Counters, sizeof...( Containers )> m_counters;

    public:
      CountersFor( Gaudi::Algorithm const* parent )
          : m_counters{ Counters{ parent, Traits::containerName<Containers>() }... } {}

      template <typename C>
      constexpr Counters& get() {
        if constexpr ( std::disjunction_v<std::is_same<C, Containers>...> ) {
          return m_counters[index_of_v<C, std::tuple<Containers...>>];
        } else {
          return m_counters[index_of_v<Traits::container<C>, std::tuple<Containers...>>];
        }
      }
    };

    /// Copy data object version
    void saveVersion( Gaudi::Algorithm const& parent, int i_ver, std::string_view identifier, DataObject& out ) {
      const int o_ver = out.version();
      // sanity check
      if ( o_ver != 0 && o_ver != i_ver ) {
        parent.warning() << identifier << "  input version " << i_ver << " != current packed version " << o_ver
                         << endmsg;
      }
      out.setVersion( i_ver );
    }

    std::string_view remove_prefix( std::string_view s, std::string_view prefix ) {
      if ( s.substr( 0, prefix.size() ) == prefix ) s.remove_prefix( prefix.size() );
      return s;
    }

    std::string_view remove_suffix( std::string_view s, std::string_view suffix ) {
      if ( s.substr( s.size() - suffix.size() ) == suffix ) s.remove_suffix( suffix.size() );
      return s;
    }

    class Encoder {
      DataObjIDColl const*                          m_requested;
      DataObjIDMapper_t const*                      m_external;
      IIndexedANNSvc::inv_map_t const*              m_map;
      std::unordered_map<std::string, unsigned int> m_deps;
      unsigned                                      m_key = 0;
      std::string                                   m_prefix;
      bool                                          m_anonymize = false;
      unsigned int                                  id( std::string const& l ) {
        auto id = m_map->find( l ); // TODO: support transparent lookup
        if ( id == m_map->end() ) {
          throw GaudiException(
              fmt::format( "could not locate packedobjectlocation {} (configured prefix={}) in table for key 0x{:08x}",
                                                            l, m_prefix, m_key ),
              __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }
        return id->second;
      }

    public:
      struct Anonymize_tag;
      using Anonymize = LHCb::tagged_bool<Anonymize_tag>;

      Encoder( DataObjIDColl const* requested, DataObjIDMapper_t const* external, IIndexedANNSvc const& annsvc,
               unsigned key, std::string_view stream, Anonymize anonymize )
          : m_requested{ requested }
          , m_external{ external }
          , m_map{ &annsvc.s2i( key, PackedObjectLocations ) }
          , m_key{ key }
          , m_prefix{ fmt::format( "/Event/{}/", remove_suffix( remove_prefix( stream, "/Event/" ), "/" ) ) }
          , m_anonymize{ anonymize } {}
      unsigned         key() const { return m_key; }
      std::string_view stream() const { return remove_suffix( remove_prefix( m_prefix, "/Event/" ), "/" ); }
      auto             packedLocation( std::string_view loc ) const {
        if ( loc.empty() )
          throw GaudiException( "empty PackedObjectLocation", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        if ( loc.front() != '/' ) return std::string{ m_prefix }.append( loc );
        if ( loc.substr( 0, m_prefix.size() ) == m_prefix ) return std::string{ loc };
        return std::string{ m_prefix }.append( remove_prefix( loc, "/Event/" ) );
      }
      auto operator()( std::string_view loc ) {
        // if external, just do the minimal thing as instructed, and no more...
        // also: external locations can (by construction!) not be anonymized!
        if ( auto iext = m_external->find( DataObjID{ std::string{ loc } } ); iext != m_external->end() ) {
          return id( iext->second.key() );
        }
        auto packed = packedLocation( loc );
        if ( m_anonymize && m_requested->find( DataObjID{ std::string{ loc } } ) == m_requested->end() ) {
          // implicit dependency -- generate location ID for this location if not already present (for this event!)
          auto [dep, _] = m_deps.emplace( packed, ( -1u ) - m_deps.size() );
          return dep->second;
        } else {
          return id( packed );
        }
      }
    };

    template <typename PackedDataVector>
    auto create_buffer( Gaudi::Algorithm const& parent, std::string_view location, Encoder& encoder,
                        PackedDataVector const& pdata ) {

      // reserve some space for data, this should be tuned
      LHCb::Hlt::PackedData::PackedDataOutBuffer buffer( encoder.key() );
      buffer.reserve( pdata.data().size() );

      buffer.save<uint32_t>( pdata.clID() );
      auto locationID = encoder( location );
      buffer.save<int32_t>( locationID );
      auto*        linkMgr = pdata.linkMgr();
      unsigned int nlinks  = linkMgr->size();

      if ( parent.msgLevel( MSG::DEBUG ) ) {
        parent.debug() << "packed version " << (unsigned int)pdata.version() << endmsg;
        parent.debug() << "packed data type " << System::typeinfoName( typeid( pdata ) ) << endmsg;
        parent.debug() << "packed data entries " << pdata.data().size() << endmsg;
        parent.debug() << "classID " << pdata.clID() << " locationID " << locationID << " nlinks " << nlinks << endmsg;
      }

      buffer.saveSize( nlinks );
      for ( auto const& link : *linkMgr ) buffer.save<int32_t>( encoder( link.path() ) );

      // Reserve bytes for the size of the object
      auto posObjectSize = buffer.saveSize( 0 ).first;

      // Save the object actual object and see how many bytes were written
      auto objectSize = buffer.save( pdata ).second;

      // Save the object's size in the correct position
      buffer.saveAt<uint32_t>( objectSize, posObjectSize );

      return buffer;
    }

    template <typename Packer, typename Item>
    auto create_packeddata( Packer const& packer, Item const& item ) {
      auto pdata = DataPacking::Buffer::RegistryWrapper<typename Packer::PackedDataVector>(
          fmt::format( "{}Packed", identifier( item ) ) );
      saveVersion( packer.parent(), item.version(), identifier( item ), *pdata );
      auto reserve = []( auto& _0, auto const& _1 ) -> decltype( _0->data().reserve( _1.size() ) ) {
        return _0->data().reserve( _1.size() );
      };
      if constexpr ( std::is_invocable_v<decltype( reserve ), decltype( pdata ), Item const&> ) reserve( pdata, item );
      packer.pack( item, *pdata );
      return pdata;
    }

    template <typename Packer, typename ValueRange>
    StatusCode check_packeddata( Packer const& packer, ValueRange range,
                                 typename Packer::PackedDataVector const& pdata ) {
      auto unpacked = DataPacking::Buffer::RegistryWrapper<typename Packer::DataVector>(
          fmt::format( "{}_PackingCheck", identifier( range ) ) );
      unpacked.setDataSvc( packer.parent().evtSvc() );
      unpacked->setVersion( pdata.version() );
      if ( auto sc = unpack( &packer.parent(), pdata, *unpacked ); sc.isFailure() ) {
        packer.parent().error() << "Error unpacking " << identifier( range ) << " during checking: " << sc << endmsg;
        return sc;
      }
      auto sc = DataPacking::DataChecks{ packer.parent() }.check( packer, range, *unpacked );
      if ( sc.isFailure() ) {
        packer.parent().error() << "Packing check failed for " << identifier( range ) << ": " << sc << endmsg;
      } else {
        packer.parent().info() << "Packing check successful for " << identifier( range ) << endmsg;
      }
      return sc;
    }

    template <typename Packer, typename Ranges>
    void append_ranges_to( Buffer& buffer, Encoder& encoder, bool check,
                           LHCb::Hlt::PackedData::PackedDataChecksum* checksum, Counters& counters,
                           Packer const& packer, Ranges const& ranges ) {
      int nRanges = 0;
      for ( const auto& range : ranges ) {
        ++nRanges;

        counters.nbPackedData += range.size();

        if ( packer.parent().msgLevel( MSG::DEBUG ) ) {
          packer.parent().debug() << "Location " << identifier( range ) << " (" << encoder( identifier( range ) ) << ")"
                                  << " size " << range.size() << " version " << static_cast<int>( range.version() )
                                  << " type " << System::typeinfoName( typeid( *range.container() ) ) << endmsg;
        }

        const auto pdata = create_packeddata( packer, range );
        if ( check ) check_packeddata( packer, range, *pdata ).ignore();
        if ( checksum ) checksum->processObject( *pdata, encoder.packedLocation( ( identifier( range ) ) ) + "Packed" );

        const auto buf = create_buffer( packer.parent(), identifier( range ), encoder, *pdata );
        if ( packer.parent().msgLevel( MSG::DEBUG ) ) {
          packer.parent().debug() << "buffer size " << buffer.size() << endmsg;
        }
        counters.nbBufferData += buf.size();
        buffer.addBuffer( buf );
      }
      counters.nContainersPacked += nRanges;
    }

    namespace dump {

      MsgStream& buffer( MsgStream& msg, Buffer const& buffer ) {
        auto m          = std::map<int, LHCb::Hlt::PackedData::ObjectHeader>{};
        auto readBuffer = LHCb::Hlt::PackedData::PackedDataInBuffer( buffer.key() );
        readBuffer.init( buffer.buffer() );
        while ( !readBuffer.eof() ) {
          LHCb::Hlt::PackedData::ObjectHeader header{ readBuffer };
          readBuffer.skip( header.storedSize );
          m.emplace( header.locationID, std::move( header ) );
        }
        for ( const auto& [_, header] : m ) {
          msg << "Found CLID=" << header.classID << " locationID=" << header.locationID << " size " << header.storedSize
              << " with " << header.linkLocationIDs.size() << " links; internal: ";
          const auto&   links = header.linkLocationIDs;
          std::set<int> internal, external;
          std::partition_copy( links.begin(), links.end(), std::inserter( internal, internal.end() ),
                               std::inserter( external, external.end() ),
                               [&m]( const auto& i ) { return m.find( i ) != m.end(); } );
          for ( auto id : internal ) msg << id << " ";
          if ( !external.empty() ) {
            msg << " external: ";
            for ( auto id : external ) {
              msg << id << " ";
              if ( id & 0x80000000 ) msg << "(anonymized: this should never happen!) ";
            }
          }
          msg << endmsg;
        }
        return msg;
      }

      MsgStream& request( MsgStream& msg, HltDecReports const& dec, const DataObjIDColl& containers ) {
        msg << "got positive decisions: ";
        for ( auto const& [k, dr] : dec )
          if ( dr.decision() ) msg << k << " ";
        msg << endmsg;
        msg << "explicitly requested containers for this event: "; // TODO: for each container, report which
                                                                   // lines asked for it...
        for ( const auto& i : containers ) msg << i << " ";
        msg << endmsg;
        return msg;
      }

      MsgStream& request_verification( MsgStream& msg, DataObjIDMapper_t const& externalLocations,
                                       DataObjIDColl const& containers, HltDecReports const& dec,
                                       std::map<std::string, DataObjIDColl> const& map ) {
        msg << "assuming the following containers are persisted through other means: ";
        for ( const auto& i : externalLocations ) msg << i << " ";
        msg << endmsg;
        for ( auto const& i : containers ) {
          msg << "requested container " << i << " not found -- requested by: ";
          for ( auto const& [k, dr] : dec ) {
            if ( !dr.decision() ) continue;
            auto j = map.find( k );
            if ( j != map.end() && j->second.find( i ) != j->second.end() ) msg << k << " ";
          }
          msg << endmsg;
        }
        return msg;
      }
    } // namespace dump

    namespace {
      template <typename C>
      class Item {
        std::set<C const*> m_set;
        using Packer = Traits::packer<C>;
        Packer    m_packer;
        Counters* m_counters = nullptr;

      public:
        Item( Gaudi::Algorithm const& parent, Counters& counters ) : m_packer{ &parent }, m_counters{ &counters } {}

        const auto& parent() { return m_packer.parent(); }
        Packer&     packer() { return m_packer; }

        bool emplace( C const* c ) { return m_set.emplace( c ).second; }

        void append_to( Buffer& buffer, Encoder& encoder, bool check,
                        LHCb::Hlt::PackedData::PackedDataChecksum* checksum ) const {
          for ( auto i : m_set ) {
            if ( m_packer.parent().msgLevel( MSG::DEBUG ) ) {
              m_packer.parent().debug() << fmt::format( "Location {} ( -> 0x{:08x} ) size {} version {:x} type {}",
                                                        identifier( *i ), encoder( identifier( *i ) ), size( *i ),
                                                        static_cast<int>( i->version() ),
                                                        System::typeinfoName( typeid( *i ) ) )
                                        << endmsg;
            }
            const auto pdata = create_packeddata( m_packer, *i );
            m_counters->nbPackedData += pdata->data().size();
            if ( checksum )
              checksum->processObject( *pdata, encoder.packedLocation( ( identifier( *i ) ) ) + "Packed" );
            if ( check ) check_packeddata( m_packer, *i, *pdata ).ignore();

            const auto buf = create_buffer( m_packer.parent(), identifier( *i ), encoder, *pdata );
            m_counters->nbBufferData += buf.size();
            buffer.addBuffer( buf );
          }
          m_counters->nContainersPacked += m_set.size();
        }
      };

      template <>
      class Item<LHCb::Event::PV::PrimaryVertexContainer> {
        using Container = LHCb::Event::PV::PrimaryVertexContainer;
        std::set<Container const*> m_containers;
        Gaudi::Algorithm const*    m_parent{ nullptr };
        Counters*                  m_counters = nullptr;

      public:
        Item( Gaudi::Algorithm const& parent, Counters& counters ) : m_parent{ &parent }, m_counters{ &counters } {}

        const auto& parent() const { return *m_parent; }
        bool        emplace( Container const* c ) { return m_containers.emplace( c ).second; }

        void append_to( Buffer& parentbuffer, Encoder& encoder, bool,
                        LHCb::Hlt::PackedData::PackedDataChecksum* ) const {
          for ( auto i : m_containers ) {
            LHCb::Hlt::PackedData::PackedDataOutBuffer buffer( encoder.key() );
            const auto&                                data = *i;
            buffer.save<uint32_t>( data.clID() );
            const auto& location   = identifier( data );
            auto        locationID = encoder( location );
            buffer.save<int32_t>( locationID );
            auto*        linkMgr = data.linkMgr();
            unsigned int nlinks  = linkMgr->size();
            if ( parent().msgLevel( MSG::DEBUG ) ) {
              parent().debug() << "packed version " << (unsigned int)data.version() << endmsg;
              parent().debug() << "packed data type " << System::typeinfoName( typeid( data ) ) << endmsg;
              parent().debug() << "packed data entries " << data.size() << endmsg;
              parent().debug() << "classID " << data.clID() << " locationID " << locationID << " nlinks " << nlinks
                               << endmsg;
            }

            buffer.saveSize( nlinks );
            for ( auto const& link : *linkMgr ) buffer.save<int32_t>( encoder( link.path() ) );

            // Reserve bytes for the size of the object
            auto posObjectSize = buffer.saveSize( 0 ).first;

            // Save the object actual object and see how many bytes were written
            auto objectSize =
                parent().msgLevel( MSG::DEBUG ) ? buffer.save<true>( data ).second : buffer.save<false>( data ).second;

            // Save the object's size in the correct position
            buffer.saveAt<uint32_t>( objectSize, posObjectSize );

            m_counters->nbBufferData += buffer.size();
            parentbuffer.addBuffer( buffer );
          }
          m_counters->nContainersPacked += m_containers.size();
        }
      };

      template <typename ValueType>
      class Item<::SharedObjectsContainer<ValueType>> {
        // note: by construction, entries in a shared object container can _never_
        //       appear as dependency. So we only support packing _entire_ containers.
        using C = ::SharedObjectsContainer<ValueType>;
        std::set<C const*> m_containers;

        using Packer = LHCb::Packers::SharedObjectsContainer<ValueType>;
        Packer    m_packer;
        Counters* m_counters = nullptr;

      public:
        Item( Gaudi::Algorithm const& parent, Counters& counters ) : m_packer{ &parent }, m_counters{ &counters } {}

        const auto& parent() { return m_packer.parent(); }
        Packer&     packer() { return m_packer; }

        bool emplace( C const* c ) { return m_containers.emplace( c ).second; }
        void append_to( Buffer& buffer, Encoder& encoder, bool, LHCb::Hlt::PackedData::PackedDataChecksum* ) const {
          for ( auto i : m_containers ) {
            const auto pdata = create_packeddata( m_packer, *i );
            m_counters->nbPackedData += pdata->data().size();
            const auto buf = create_buffer( m_packer.parent(), identifier( *i ), encoder, *pdata );
            m_counters->nbBufferData += buf.size();
            buffer.addBuffer( buf );
            // TODO: add checking + checksum
          }
          m_counters->nContainersPacked += m_containers.size();
        }
      };

      template <typename ValueType, typename Mapping>
      class Item<KeyedContainer<ValueType, Mapping>> {
        using C = KeyedContainer<ValueType, Mapping>;
        using K = typename C::key_type;
        struct Less {
          using is_transparent = void;
          bool operator()( ValueType const* lhs, ValueType const* rhs ) const {
            return std::pair{ lhs->parent(), lhs->key() } < std::pair{ rhs->parent(), rhs->key() };
          }
          bool operator()( ObjectContainerBase const* c, ValueType const* rhs ) const { return c < rhs->parent(); }
          bool operator()( ValueType const* lhs, ObjectContainerBase const* c ) const { return lhs->parent() < c; }
        };

        using S = std::set<ValueType const*, Less>;
        S                  m_set;
        std::set<C const*> m_empty_containers; // empty containers need explicit treatment

        bool m_locked = false;
        class ValueRange {
          using Iterator                         = typename S::const_iterator;
          ObjectContainerBase const* m_container = nullptr;
          Iterator                   m_current   = empty_set<S>.begin();
          Iterator                   m_end       = empty_set<S>.end();

        public:
          // create a non-empty range in some S, starting at 'current'
          ValueRange( S const* p, Iterator current )
              : m_container{ current != p->end() ? ( *current )->parent() : nullptr }
              , m_current{ current }
              , m_end{ m_container ? p->upper_bound( m_container ) : current } {}
          // create an empty range (which still has acess to a valid parent container!)
          ValueRange( C const* p ) : m_container{ p } {}

          auto        begin() const { return m_current; }
          auto        end() const { return m_end; }
          ValueRange& operator++() {
            ++m_current;
            return *this;
          }

          size_t             size() const { return std::distance( begin(), end() ); }
          const std::string& identifier() const {
            using LHCb::identifier;
            return identifier( *m_container );
          }
          ObjectContainerBase const* container() const { return m_container; }
          int                        version() const { return m_container->version(); }
        };

        using ContainerRange      = details_::ContainerRange<ValueRange, S>;
        using EmptyContainerRange = details_::EmptyContainerRange<ValueRange, C>;

        using Packer = Traits::packer<ValueType>;
        Packer    m_packer;
        Counters* m_counters = nullptr;

      public:
        Item( Gaudi::Algorithm const& parent, Counters& counters ) : m_packer{ &parent }, m_counters{ &counters } {}

        const auto& parent() { return m_packer.parent(); }
        Packer&     packer() { return m_packer; }

        bool emplace( C const* c ) {
          // while non-empty containers are transitively selected by their content
          // empty containers need explicit action
          return c->empty() ? m_empty_containers.emplace( c ).second : true;
        }
        bool emplace( ValueType const* v ) {
          if ( m_locked )
            throw GaudiException( "attempt to add to locked item (potential circular dependency?)", __PRETTY_FUNCTION__,
                                  StatusCode::FAILURE );
          return m_set.emplace( v ).second;
        }

        bool contains( ValueType const* v ) const { return m_set.find( v ) != m_set.end(); }

        void lock() { m_locked = true; }

        void append_to( Buffer& buffer, Encoder& encoder, bool check,
                        LHCb::Hlt::PackedData::PackedDataChecksum* checksum ) const {
          // should we check that there are no duplicates between m_set and m_empty_containers?
          append_ranges_to( buffer, encoder, check, checksum, *m_counters, m_packer,
                            concat_t{ ContainerRange{ m_set }, EmptyContainerRange{ m_empty_containers } } );
        }
      };

      template <typename From, typename To, typename Weight>
      class Item<RelationWeighted1D<From, To, Weight>> {
        using Container = RelationWeighted1D<From, To, Weight>;

      public:
        class Entry {
        public:
          using Container      = RelationWeighted1D<From, To, Weight>;
          using contained_type = typename Container::Entry;
          using DataVector     = Container; // this is the thing we want to mimic

          Entry( Container const* p, contained_type const* entry ) : m_parent{ p }, m_entry{ entry } {}

          auto             parent() const { return m_parent; }
          decltype( auto ) from() const { return m_entry->from(); }
          decltype( auto ) to() const { return m_entry->to(); }
          decltype( auto ) weight() const { return m_entry->weight(); }

        private:
          Container const*      m_parent;
          contained_type const* m_entry;
        };

      private:
        struct Less {
          using is_transparent = void;
          bool operator()( Entry const& lhs, Entry const& rhs ) const {
            if ( lhs.parent() < rhs.parent() ) return true;
            if ( rhs.parent() < lhs.parent() ) return false;
            using LessF = typename Entry::contained_type::LessF;
            using LessW = typename Entry::contained_type::LessW;
            using LessT = typename Entry::contained_type::LessT;
            if ( LessF()( lhs.from(), rhs.from() ) ) return true;
            if ( LessF()( rhs.from(), lhs.from() ) ) return false;
            if ( LessW()( lhs.weight(), rhs.weight() ) ) return true;
            if ( LessW()( rhs.weight(), lhs.weight() ) ) return false;
            return LessT()( lhs.to(), rhs.to() );
          }
          bool operator()( Container const* c, Entry const& rhs ) const { return c < rhs.parent(); }
          bool operator()( Entry const& lhs, Container const* c ) const { return lhs.parent() < c; }
        };
        using S = std::set<Entry, Less>;
        S                          m_set;
        std::set<Container const*> m_empty_relations; // empty containers need explicit treatment

        class ValueRange {
          using Iterator               = typename S::const_iterator;
          Iterator         m_current   = empty_set<S>.begin();
          Container const* m_container = nullptr;
          Iterator         m_end       = empty_set<S>.end();

        public:
          ValueRange( S const* parent, Iterator current )
              : m_current{ current }
              , m_container{ current != parent->end() ? current->parent() : nullptr }
              , m_end{ m_container ? parent->upper_bound( m_container ) : current } {}

          ValueRange( Container const* p ) : m_container{ p } {}

          ValueRange const& relations() const { return *this; } // mimic the original RelationWeighted class...

          auto        begin() const { return m_current; }
          auto        end() const { return m_end; }
          ValueRange& operator++() {
            ++m_current;
            return *this;
          }

          size_t             size() const { return std::distance( begin(), end() ); }
          const std::string& identifier() const {
            using LHCb::identifier;
            return identifier( *m_container );
          }
          Container const* container() const { return m_container; }
          int              version() const { return m_container->version(); }
        };

        using ContainerRange      = details_::ContainerRange<ValueRange, S>;
        using EmptyContainerRange = details_::EmptyContainerRange<ValueRange, Container>;
        using Packer              = Traits::packer<RelationWeighted1D<From, To, Weight>>;
        Packer    m_packer;
        Counters* m_counters = nullptr;

      public:
        Item( Gaudi::Algorithm const& parent, Counters& counters ) : m_packer{ &parent }, m_counters{ &counters } {}

        const auto& parent() { return m_packer.parent(); }
        Packer&     packer() { return m_packer; }

        bool emplace( Entry const* v ) {
          assert( v != nullptr );
          return m_set.emplace( *v ).second;
        }

        bool emplace( Container const* c ) {
          // give that relations tables may be pruned,
          // checking for 'empty' is not the right thing to do.
          // For now, we rely on the _caller_ to deal with this
          // (but this could be skipped in the caller, add
          //  'every' relations container here, and then when packing
          //  remove the ones which already have entries in the
          //  'entries' set -- i.e. just always add, and figure out
          //  at the end what is duplicate wrt. the set of entries))
          return m_empty_relations.emplace( c ).second;
        }

        void append_to( Buffer& buffer, Encoder& encoder, bool check,
                        LHCb::Hlt::PackedData::PackedDataChecksum* checksum ) const {
          // should we check that there are no duplicates between m_set and m_empty_relations?
          append_ranges_to( buffer, encoder, check, checksum, *m_counters, m_packer,
                            concat_t{ ContainerRange{ m_set }, EmptyContainerRange{ m_empty_relations } } );
        }
      };
    } // namespace

    class ShoppingList {

      template <typename... Cs>
      class Maps {
        std::tuple<Item<Cs>...> m_items;

      public:
        template <typename Counters>
        Maps( const Gaudi::Algorithm& parent, Counters& counters )
            : m_items{ { parent, counters.template get<Cs>() }... } {}

        template <typename C>
        const auto& get() const {
          if constexpr ( std::disjunction_v<std::is_same<C, Cs>...> ) {
            return std::get<Item<C>>( m_items );
          } else if constexpr ( Gaudi::cpp17::is_detected_v<Traits::details::Container_t, C> ) {
            return std::get<Item<Traits::details::Container_t<C>>>( m_items );
          }
          // if we ever get here: see if there is a type in Cs... which has an embedded type
          // Cs::contained_type (as is the case in eg. KeyedContainer) and then use that Cs
        }

        template <typename C>
        auto& get() {
          if constexpr ( std::disjunction_v<std::is_same<C, Cs>...> ) {
            return std::get<Item<C>>( m_items );
          } else if constexpr ( Gaudi::cpp17::is_detected_v<Traits::details::Container_t, C> ) {
            return std::get<Item<Traits::details::Container_t<C>>>( m_items );
          }
          // if we ever get here: see if there is a type in Cs... which has an embedded type
          // Cs::contained_type (as is the case in eg. KeyedContainer) and then use that Cs
        }

        template <typename I>
        bool add( I const& v ) {
          return this->template get<I>().emplace( &v );
        }

        template <typename Fun>
        void for_each( Fun f ) const {
          ( f( get<Cs>() ), ... );
        }
      };

      template <typename V>
      void add( const SmartRef<V>& v ) {
        add( v.target() );
      }

      template <typename V>
      void add( const SmartRefVector<V>& v ) {
        for ( const auto& i : v ) add( i );
      }

      void add( const Particle& p ) {
        if ( m_lists.add( p ) ) {
          add( p.endVertex() );
          add( p.proto() );
          add( p.daughters() );
          add( p.pv() );
        }
      }

      void add( const Vertex& v ) {
        if ( m_lists.add( v ) ) add( v.outgoingParticles() );
      }

      void add( const VertexBase& v ) {
        if ( auto rv = dynamic_cast<const RecVertex*>( &v ) ) {
          add( *rv );
        } else if ( auto rv = dynamic_cast<const PrimaryVertex*>( &v ) ) {
          add( *rv );
        } else {
          std::cerr << __PRETTY_FUNCTION__ << " do not know how to add this type  yet.... " << std::endl;
        }
      }

      void add( const ProtoParticle& p ) {
        if ( m_lists.add( p ) ) {
          add( p.track() );
          add( p.richPID() );
          add( p.caloChargedPID() );
          add( p.bremInfo() );
          add( p.muonPID() );
          add( p.globalChargedPID() );
          add( p.neutralPID() );
          add( p.calo() );
        }
      }

      void add( const Track& t ) {
        if ( m_lists.add( t ) ) {
          if ( m_options.add_track_ancestors ) add( t.ancestors() );
        }
      }

      void add( const RichPID& r ) {
        if ( m_lists.add( r ) ) add( r.track() );
      }

      void add( const Event::Calo::v1::CaloChargedPID& n ) { m_lists.add( n ); }

      void add( const Event::Calo::v1::BremInfo& n ) { m_lists.add( n ); }

      void add( const MuonPID& m ) {
        if ( m_lists.add( m ) ) {
          add( m.idTrack() );
          add( m.muonTrack() );
        }
      }

      void add( const GlobalChargedPID& g ) { m_lists.add( g ); }

      void add( const NeutralPID& n ) { m_lists.add( n ); }

      void add( const CaloHypo& h ) {
        if ( m_lists.add( h ) ) {
          if ( m_options.add_calo_digits ) add( h.digits() );
          if ( m_options.add_calo_clusters ) add( h.clusters() );
          add( h.hypos() );
        }
      }

      void add( const CaloCluster& c ) {
        if ( m_lists.add( c ) && m_options.add_calo_digits ) {
          for ( const auto& entry : c.entries() ) add( entry.digit() );
        }
      }

      // no references in PackedCaloDigit
      void add( const CaloDigit& h ) { m_lists.add( h ); }

      // no references in PackedCaloAdc
      void add( const CaloAdc& a ) { m_lists.add( a ); }

      void add( const FlavourTag& ft ) {
        if ( m_lists.add( ft ) ) {
          add( ft.taggedB() );
          // taggers are kept 'by value' inside FlavourTag, so no seperate 'add' for taggers type, i.e. Tagger
          // instead, go transitive here...
          if ( m_options.add_tagger_particles )
            for ( const auto& t : ft.taggers() ) add( t.taggerParts() );
        }
      }

      void add( const RecVertex& rv ) {
        if ( !rv.isPrimary() )
          throw GaudiException( fmt::format( "got RecVertex which is not primary from {}", identifier( rv ) ),
                                __PRETTY_FUNCTION__, StatusCode::FAILURE );
        if ( m_lists.add( rv ) ) {
          if ( m_options.add_pv_tracks ) add( rv.tracks() );
        }
      }

      void add( const PrimaryVertex& rv ) { m_lists.add( rv ); }

      void add( const TwoProngVertex& rv ) {
        if ( m_lists.add( rv ) ) add( rv.tracks() );
      }

      template <typename From, typename To>
      void add( const Relation1D<From, To>& r ) {
        // TODO: can go in two directions: either add anything that is in the table, or (once all keys are known) prune
        // all unused entries from the table. Since both may be needed, this should be configured externally
        // regardless, the 'to' side needs to go recursive (which may create loops?)
        // Probably best to prune the 'from' hand side (when explicitly requested!), and go recursive on the 'to' side
        // TODO: figure out what the packers need...
        if ( m_lists.add( r ) ) {
          if ( false /*  prune( r.registry() ) */ ) {
            for ( const auto& i : r.relations() ) {
              //     if ( !m_lists.contains( i.from() ) ) continue;
              // add( i );
              if constexpr ( std::is_base_of_v<DataObject, To> ) add( i.to() );
            }
          } else {
            for ( const auto& i : r.relations() ) {
              // add( i );
              add( i.from() );
              if constexpr ( std::is_base_of_v<DataObject, To> || std::is_base_of_v<ContainedObject, To> )
                add( i.to() );
            }
          }
        }
      }

      template <typename From, typename To, typename Weight>
      void add( const RelationWeighted1D<From, To, Weight>& r ) {
        bool  empty_relation = true;
        auto* item           = ( m_options.prune_relations /* ( r.registry() )*/ ? &m_lists.get<From>() : nullptr );
        // if we prune, have to make sure that, from now on, adding new `From` entries is considered an error
        if ( item ) item->lock();
        for ( const auto& i : r.relations() ) {
          if ( item ) {
            if ( !item->contains( i.from() ) ) continue;
          } else {
            add( i.from() );
          }
          if ( m_lists.add( typename Item<RelationWeighted1D<From, To, Weight>>::Entry{ &r, &i } ) ) {
            empty_relation = false;
            if constexpr ( std::is_base_of_v<DataObject, To> || std::is_base_of_v<ContainedObject, To> ) add( i.to() );
          }
        }
        if ( empty_relation )
          m_lists.add( r ); // no elements were selected, so the container itself is not
                            // transitively selected -- so must be added _explictly_.
      }

      // this is the initial entry point
      template <typename Container>
      void add( VOC<Container*> const& c ) {
        const auto& p = m_lists.template get<Container>().parent();
        for ( auto const* i : c ) {
          if ( !i ) continue;
          auto const& id = identifier( *i );
          if ( is_external( id ) ) {
            if ( p.msgLevel( MSG::DEBUG ) )
              p.debug() << "skipping " << id << " -- should be externally persisted" << endmsg;
            continue;
          }
          if ( !is_explicitly_requested( id ) ) {
            if ( p.msgLevel( MSG::DEBUG ) )
              p.debug() << " skipping " << id << " -- not requested for this event" << endmsg;
            continue;
          }
          add( *i );
        }
      }

      template <typename V>
      void add( const V* v ) {
        if ( v ) add( *v );
      }

      template <typename V, typename M>
      void add( const KeyedContainer<V, M>& kc ) {
        if ( kc.empty() )
          m_lists.add( kc ); // make sure empty lists are explicitly forwarded, as those are not transitively
                             // selected by their content (duh!)
        for ( const auto& i : kc ) add( i );
      }

      template <typename V>
      void add( const SharedObjectsContainer<V>& c ) {
        if ( m_lists.add( c ) ) {
          // don't forget to add the dependencies of this container!
          for ( const auto& i : c ) add( i );
        }
      }

      void add( const RecSummary& rs ) { m_lists.add( rs ); }

      void add( const LHCb::Event::PV::PrimaryVertexContainer& pvs ) { m_lists.add( pvs ); }

      void add( const MCParticle& p ) {
        // for now, MCParticles should be packed externally...
        if ( is_external( identifier( p ) ) ) return;
        throw GaudiException( "Configuration error: reference to MCParticle in " + identifier( p ) + " will not work",
                              __PRETTY_FUNCTION__, StatusCode::FAILURE );
        // perhaps we should just pack it one day... just needs a trivial packer  -- the code already exists in
        // PackMCParticle
#if 0
        if ( m_lists.add( p ) ) {
          add( p.originVertex() );
          add( p.endVertices() );
        }
#endif
      }

      void add( const MCVertex& v ) {
        // for now, MCVertices should be packed externally...
        if ( is_external( identifier( v ) ) ) return;
        throw GaudiException( "Configuration error: reference to MCVertex in " + identifier( v ) + " will not work",
                              __PRETTY_FUNCTION__, StatusCode::FAILURE );
        // perhaps we should just pack it one day... just needs a trivial packer  -- the code already exists in
        // PackMCVertex
#if 0
        if ( m_lists.add( v ) ) {
          add( v.mother() );
          add( v.products() );
        }
#endif
      }

      void add( const WeightsVector& v ) { m_lists.add( v ); }

    public:
      struct Options {
        // TODO: make these 'per container' instead of global
        bool                                                                         add_track_ancestors     = false;
        bool                                                                         add_calo_digits         = false;
        bool                                                                         add_calo_clusters       = false;
        bool                                                                         add_tagger_particles    = false;
        bool                                                                         add_pv_tracks           = false;
        std::vector<LHCb::State::Location>                                           persisted_track_states  = {};
        std::map<LHCb::Event::Enum::Track::Type, std::vector<LHCb::State::Location>> track_states_exceptions = {};
        bool                                                                         prune_relations         = true;
      };

      template <typename... Items>
      ShoppingList( Gaudi::Algorithm const& parent, DataObjIDColl containers, DataObjIDMapper_t external,
                    CountersFor<Items...>& counters, Options options, VOC<Items*> const&... items )
          : m_requested{ std::move( containers ) }
          , m_external{ std::move( external ) }
          , m_options{ std::move( options ) }
          , m_lists{ parent, counters } {
        m_lists.get<CaloHypos>()
            .packer()
            .packDigitRefs( m_options.add_calo_digits )
            .packClusterRefs( m_options.add_calo_clusters );
        m_lists.get<CaloClusters>().packer().packDigitRefs( m_options.add_calo_digits );
        m_lists.get<FlavourTags>().packer().packTagParticleRefs( m_options.add_tagger_particles );
        m_lists.get<RecVertices>().packer().packTrackRefs( m_options.add_pv_tracks );
        m_lists.get<Tracks>().packer().setStateExceptions( m_options.track_states_exceptions );
        m_lists.get<Tracks>().packer().setDefaultPersistedStates( m_options.persisted_track_states );
        // do the actual work!
        ( add( items ), ... );
        // TODO: verify that anything in containers is actually added in the end....
      }

      bool is_external( DataObjID const& id ) { return m_external.find( id ) != m_external.end(); }
      bool is_explicitly_requested( DataObjID const& id ) { return m_requested.find( id ) != m_requested.end(); }

      template <typename Fun>
      void for_each( Fun f ) const {
        m_lists.for_each( std::forward<Fun>( f ) );
      }

    private:
      DataObjIDColl     m_requested = {}; // explicitly requested containers to be packed for this specific event
      DataObjIDMapper_t m_external  = {}; // persisted 'out-of-bound' -- skip, but allow non-anynomous references into
                                          // these
      Options m_options = {};
      // list of all supported types
      Traits::expand_t<Maps> m_lists;
    };
    template <typename EventData>
    using input_t = Gaudi::Functional::vector_of_const_<EventData*> const&;
    template <typename... EventData>
    using baseclass_t = LHCb::Algorithm::MergingTransformer<Buffer( input_t<EventData>... )>;

  } // namespace

  template <typename... Containers>
  class SelectivePacker final : public baseclass_t<Containers...> {
    DataObjectReadHandle<HltDecReports> m_decrep{ this, "DecReports", HltDecReportsLocation::Default };
    Gaudi::Property<std::map<std::string, DataObjIDColl>> m_map{ this, "LineToLocations", {} };
    Gaudi::Property<DataObjIDColl>                        m_alwaysPack{ this, "AlwaysPack", {} };
    Gaudi::Property<DataObjIDMapper_t>                    m_externalLocations{
        this, "ExternalLocations", {} }; // externally packed locations may get 'renamed' when the external packer packs
                                                            // them.
    Gaudi::Property<unsigned int>             m_encodingKey{ this, "EncodingKey", 0u };
    Gaudi::Property<bool>                     m_enableCheck{ this, "EnableCheck", false };
    Gaudi::Property<bool>                     m_enableChecksum{ this, "EnableChecksum", false };
    Gaudi::Property<bool>                     m_anonymizeDependencies{ this, "AnonymizeDependencies", false };
    Gaudi::Property<bool>                     m_awol_allowed{ this, "AbsentRequestedContainerAllowed", false };
    Gaudi::Property<std::vector<std::string>> m_addTrackAncestors{ this, "AddTrackAncestors", {} };
    Gaudi::Property<std::vector<std::string>> m_addCaloDigits{ this, "AddCaloDigits", {} };
    Gaudi::Property<std::vector<std::string>> m_addCaloClusters{ this, "AddCaloClusters", {} };
    Gaudi::Property<std::vector<std::string>> m_addTagParticles{ this, "AddTagParticles", {} };
    Gaudi::Property<std::vector<std::string>> m_addPVTracks{ this, "AddPVTracks", {} };
    Gaudi::Property<std::string>              m_stream{ this, "OutputPrefix", {} };
    ServiceHandle<IIndexedANNSvc> m_annsvc{ this, "ANNSvc", "HltANNSvc", "Service to retrieve PackedObjectLocations" };

    Gaudi::Property<std::map<LHCb::Event::Enum::Track::Type, std::vector<LHCb::State::Location>>>
                                                        m_trackStateExceptions{ this, "TrackStateExceptions", {} };
    Gaudi::Property<std::vector<LHCb::State::Location>> m_trackStates{ this,
                                                                       "PersistedTrackStates",
                                                                       { LHCb::State::Location::ClosestToBeam,
                                                                         LHCb::State::Location::FirstMeasurement,
                                                                         LHCb::State::Location::LastMeasurement } };

    mutable CountersFor<Containers...> m_counters{ this };

  public:
    SelectivePacker( std::string const& name, ISvcLocator* pSvcLocator )
        : baseclass_t<Containers...>{
              name, pSvcLocator, { { Traits::containerName<Containers>(), {} }... }, { "outputLocation", {} } } {}

    Buffer operator()( input_t<Containers>... items ) const override {

      // figure out the subset of containers to actually persist
      const auto&   dec        = m_decrep.get();
      DataObjIDColl containers = m_alwaysPack;
      for ( auto const& [k, dr] : *dec ) {
        if ( !dr.decision() ) continue;
        auto i = m_map.find( k );
        if ( i != m_map.end() ) containers.insert( i->second.begin(), i->second.end() );
      }

      if ( this->msgLevel( MSG::DEBUG ) ) dump::request( this->debug(), *dec, containers );

      // verify that the requested subset of containers actually exist
      DataObjIDColl requested;
      enumerate(
          [&]( int i, const auto& voc ) {
            for ( auto const& [j, c] : LHCb::range::enumerate( voc ) ) {
              if ( !c ) continue;
              if ( auto in = containers.find( this->inputLocation( i, j ) ); in != containers.end() ) {
                requested.emplace( identifier( *c ) ); // some TES implementations do not have the common prefix in
                                                       // their registry identifier...
                containers.erase( in );
              }
            }
          },
          0, items... );

      if ( !containers.empty() ) {
        auto& log = ( m_awol_allowed ? this->warning() : this->error() );
        for ( auto const& i : containers ) {
          log << "requested container " << i << " not amongst configured inputs" << endmsg;
        }
        if ( !m_awol_allowed ) {
          throw GaudiException( "Configuration error: requested container not amongst configured inputs",
                                __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }
      }

      if ( this->msgLevel( MSG::DEBUG ) )
        dump::request_verification( this->debug(), m_externalLocations, containers, *dec, m_map );

      // decide how 'deep' to go depending on which lines fired
      auto any_fired = [&]( std::vector<std::string> const& lines ) {
        // perhaps figure this out in loop above instead, by checking whether for any positive decision
        // their name is mentioned in one of the 'addXYZ' ?
        return std::any_of( lines.begin(), lines.end(), [&]( const auto& line ) -> bool {
          if ( auto i = dec->find( line ); i != dec->end() ) return i->second.decision();
          throw GaudiException( "requested line not present", __PRETTY_FUNCTION__, StatusCode::FAILURE );
          __builtin_unreachable();
        } );
      };

// FIXME: C++20: stop ignoring "-Wpedantic" or "-Wc++20-extensions"
#pragma GCC diagnostic push
#if __GNUC__ >= 12
#  pragma GCC diagnostic ignored "-Wc++20-extensions"
#else
#  pragma GCC diagnostic ignored "-Wpedantic"
#endif
      ShoppingList req{ *this,
                        requested,
                        m_externalLocations,
                        m_counters,
                        ShoppingList::Options{ .add_track_ancestors     = any_fired( m_addTrackAncestors ),
                                               .add_calo_digits         = any_fired( m_addCaloDigits ),
                                               .add_calo_clusters       = any_fired( m_addCaloClusters ),
                                               .add_tagger_particles    = any_fired( m_addTagParticles ),
                                               .add_pv_tracks           = any_fired( m_addPVTracks ),
                                               .persisted_track_states  = m_trackStates.value(),
                                               .track_states_exceptions = m_trackStateExceptions.value() },
                        items... };
#pragma GCC diagnostic pop

      Buffer buffer( m_encodingKey );

      std::optional<LHCb::Hlt::PackedData::PackedDataChecksum> checksum;
      if ( m_enableChecksum ) checksum.emplace();

      auto encoder = Encoder{ &requested, &m_externalLocations.value(),
                              *m_annsvc,  m_encodingKey,
                              m_stream,   Encoder::Anonymize{ m_anonymizeDependencies.value() } };
      req.for_each( [&, cksum = LHCb::get_pointer( checksum )]( const auto& c ) {
        c.append_to( buffer, encoder, m_enableCheck, cksum );
      } );
      if ( checksum ) print( this->info(), *checksum );
      if ( this->msgLevel( MSG::DEBUG ) ) dump::buffer( this->debug(), buffer );

      return buffer;
    }
  };

  using SelectivePackerInstance = Traits::expand_t<SelectivePacker>;
  DECLARE_COMPONENT_WITH_ID( SelectivePackerInstance, "LHCb__SelectivePacker" )

  // SOA packers
  DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::SOA::Pack<LHCb::Event::v3::Tracks>, "SOATrackPacker" )
  DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::SOA::Pack<LHCb::Event::Calo::v2::Clusters>, "SOACaloClusterPacker" )
  DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::SOA::Pack<LHCb::Event::Calo::v2::Hypotheses>, "SOACaloHypoPacker" )
  DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::SOA::Pack<LHCb::Event::PV::PrimaryVertexContainer>, "SOAPVPacker" )

} // namespace LHCb
