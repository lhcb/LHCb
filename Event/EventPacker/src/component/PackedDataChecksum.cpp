/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedDataChecksum.h"
#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloChargedInfo_v1.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedCaloHypo.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedGlobalChargedPID.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedNeutralPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedVertex.h"

namespace LHCb::Hlt::PackedData {

// We need to make sure the checksum is properly implemented even after
// structures evolve. The following is an ungly way to force us to make sure
// that is the case. Not sure it catches all possible cases...
#ifdef __GNUC__
  static_assert( sizeof( PackedTrack ) == 80, "PackedTrack has changed!" ); // padded!
  static_assert( sizeof( PackedState ) == 68, "PackedState has changed!" );
  static_assert( sizeof( PackedRichPID ) == 56, "PackedRichPID has changed!" ); // padded!
  static_assert( sizeof( PackedCaloChargedPID ) == 64, "PackedCaloChargedPID has changed!" );
  static_assert( sizeof( PackedBremInfo ) == 48, "PackedBremInfo has changed!" );
  static_assert( sizeof( PackedMuonPID ) == 64, "PackedMuonPID has changed!" );                   // padded!
  static_assert( sizeof( PackedGlobalChargedPID ) == 56, "PackedGlobalChargedPID has changed!" ); // padded!
  static_assert( sizeof( PackedNeutralPID ) == 64, "PackedNeutralPID has changed!" );             // padded!
  static_assert( sizeof( PackedCaloCluster ) == 72, "PackedCaloCluster has changed!" );           // padded!
  static_assert( sizeof( PackedCaloClusterEntry ) == 16, "PackedCaloClusterEntry has changed!" ); // padded!
  static_assert( sizeof( PackedCaloHypo ) == 76, "PackedCaloHypo has changed!" );
  static_assert( sizeof( PackedProtoParticle ) == 72, "PackedProtoParticle has changed!" );
  static_assert( sizeof( PackedRecVertex ) == 60, "PackedRecVertex has changed!" ); // padded!
  static_assert( sizeof( PackedFlavourTag ) == 32, "PackedFlavourTag has changed!" );
  static_assert( sizeof( PackedTagger ) == 24, "PackedTagger has changed!" ); // padded!
  static_assert( sizeof( PackedRelation ) == 16, "PackedRelation has changed!" );
  static_assert( sizeof( PackedRelatedInfoMap ) == 16, "PackedRelatedInfoMap has changed!" );
  static_assert( sizeof( PackedVertex ) == 72, "PackedVertex has changed!" );      // padded!
  static_assert( sizeof( PackedParticle ) == 184, "PackedParticle has changed!" ); // padded!
  static_assert( sizeof( PackedCaloDigit ) == 8, "PackedCaloDigit has changed!" );
  static_assert( sizeof( PackedCaloAdc ) == 8, "PackedCaloAdc has changed!" );
#endif

} // namespace LHCb::Hlt::PackedData
