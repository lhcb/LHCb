/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "BufferSOAUnpackerBaseAlg.h"
#include "BufferUnpackerBaseAlg.h"
#include "RelationPackers.h"

#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloChargedInfo_v1.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedCaloHypo.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedGlobalChargedPID.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedNeutralPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecSummary.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedTwoProngVertex.h"
#include "Event/PackedVertex.h"
#include "Event/PackedWeightsVector.h"

#include "Event/CaloClusters_v2.h"
#include "Event/CaloHypos_v2.h"
#include "Event/PrimaryVertices.h"
#include "Event/Track_v3.h"

namespace DataPacking::Buffer {
  // These packers take one data buffer location and produce unpacked object of the specified type
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::RecVertex::Container>, "RecVertexUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::PrimaryVertex::Container>, "PrimaryVertexUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::Vertex::Container>, "VertexUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::TwoProngVertex::Container>, "TwoProngVertexUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::RichPID::Container>, "RichPIDUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MuonPID::Container>, "MuonPIDUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::NeutralPID::Container>, "NeutralPIDUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::GlobalChargedPID::Container>, "GlobalChargedPIDUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::Event::Calo::v1::CaloChargedPID::Container>, "CaloChargedPIDUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::Event::Calo::v1::BremInfo::Container>, "BremInfoUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::Particle::Container>, "ParticleUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::Particle::Selection>, "ParticleSelectionUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::Track::Container>, "TrackUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::Track::Selection>, "TrackSelectionUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::FlavourTag::Container>, "FlavourTagUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::FlavourTag::Selection>, "FlavourTagSelectionUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::CaloHypo::Container>, "CaloHypoUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::CaloCluster::Container>, "CaloClusterUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::CaloDigit::Container>, "CaloDigitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::CaloAdc::Container>, "CaloAdcUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::WeightsVector::Container>, "WeightsVectorUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::RecSummary>, "RecSummaryUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::ProtoParticle::Container>, "ProtoParticleUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::ProtoParticle::Selection>, "ProtoParticleSelectionUnpacker" )

  // SOA unpackers
  DECLARE_COMPONENT_WITH_ID( SOA::Unpack<LHCb::Event::v3::Tracks>, "SOATrackUnpacker" )
  DECLARE_COMPONENT_WITH_ID( SOA::Unpack<LHCb::Event::Calo::v2::Clusters>, "SOACaloClusterUnpacker" )
  DECLARE_COMPONENT_WITH_ID( SOA::Unpack<LHCb::Event::Calo::v2::Hypotheses>, "SOACaloHypoUnpacker" )
  DECLARE_COMPONENT_WITH_ID( SOA::Unpack<LHCb::Event::PV::PrimaryVertexContainer>, "SOAPVUnpacker" )

  // Relation unpackers
  namespace { // cannot have types with a comma in a macro, so define some aliases for them
    using P2V   = LHCb::Relation1D<LHCb::Particle, LHCb::VertexBase>;
    using P2MC  = LHCb::Relation1D<LHCb::Particle, LHCb::MCParticle>;
    using P2i   = LHCb::Relation1D<LHCb::Particle, int>;
    using P2RIM = LHCb::Relation1D<LHCb::Particle, LHCb::RelatedInfoMap>;
    using PP2MC = LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double>;
  } // namespace
  DECLARE_COMPONENT_WITH_ID( Unpack<P2V>, "P2VRelationUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<P2MC>, "P2MCPRelationUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<P2i>, "P2IntRelationUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<P2RIM>, "P2InfoRelationUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<PP2MC>, "PP2MCPRelationUnpacker" )

  // unpacker which just unpacks everything it can find -- usefull for debugging, interactive (GaudiPython) browsing
  // of the TES
  struct UnpackDstDataBank final : UnpackBase {
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_empty_buffer{
        this, "Empty Buffers -- presumably DstData bank has no payload" };
    using UnpackBase::UnpackBase;
    void operator()( LHCb::Hlt::PackedData::MappedInBuffers const& buffers ) const override {
      if ( buffers.empty() ) {
        ++m_empty_buffer;
        return;
      }
      auto loader = loader_for( buffers );
      for ( const auto& [id, _] : buffers ) { // TODO: add MappedInBuffers.keys() to return a (lazy) range of keys...
        auto r = loader.load( id );
        if ( !r ) {
          warning() << r.error() << endmsg;
        } else if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "unpacked " << r->first << endmsg;
        }
      }
    }
  };
  DECLARE_COMPONENT_WITH_ID( UnpackDstDataBank, "UnpackDstDataBank" )
} // namespace DataPacking::Buffer
