/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "RelationPackers.h"

namespace LHCb::Packers {

  void P2InfoRelation::pack( const Relation1D<LHCb::Particle, LHCb::RelatedInfoMap>& rels,
                             PackedRelatedInfoRelations&                             prels ) const {

    // Make a entry in the containers vector, for this TES location
    prels.containers().emplace_back();
    auto& pcont = prels.containers().back();

    // reference to original container and key
    pcont.reference = StandardPacker::reference64( &prels, &rels, 0 );

    // First entry in the relations vector
    pcont.first = prels.data().size();

    // Loop over the relations and fill
    prels.data().reserve( prels.data().size() + rels.relations().size() );

    // Use the packer to pack this location ...
    m_rInfoPacker.pack( rels, prels );

    // last entry in the relations vector
    pcont.last = prels.data().size();
  }

  StatusCode P2InfoRelation::unpack( const PackedRelatedInfoRelations&     prels,
                                     Relation1D<Particle, RelatedInfoMap>& rels ) const {
    auto resolver = details::Resolver{ prels.linkMgr() };
    for ( const auto& prel : prels.containers() ) {
      for ( const auto& rel : LHCb::Packer::subrange( prels.relations(), prel.first, prel.last ) ) {

        auto f = resolver.object<details::FromContainer<DataVector>>( rel.reference );
        if ( !f ) {
          if ( f.error() == ErrorCode::WRONG_CONTAINER_TYPE )
            return f.error(); // data is there, but different type -- notify layer above to retry
          parent().debug() << f.error() << " while retrieving object with key " << resolver.key( rel.reference )
                           << " from " << resolver.path( rel.reference ) << endmsg;
          continue;
        }

        LHCb::RelatedInfoMap t;
        t.reserve( rel.last - rel.first );
        for ( const auto& jj : LHCb::Packer::subrange( prels.info(), rel.first, rel.last ) ) { t.insert( jj ); }

        StatusCode sc = rels.relate( f.value(), t );
        if ( !sc )
          parent().warning() << "Something went wrong with relation unpacking "
                             << "sourceKey " << resolver.key( rel.reference ) << " sourceLink "
                             << resolver.path( rel.reference ) << endmsg;
      }
    }
    if ( resolver.hasErrors() ) resolver.dump( parent().warning() );
    rels.i_sort();
    return StatusCode::SUCCESS; // TODO: return error if resolver.hasError()
  }

  void P2IntRelation::pack( const Relation1D<LHCb::Particle, int>& rels, PackedRelations& prels ) const {

    // Make a new packed data object and save
    auto& prel = prels.data().emplace_back();

    // reference to original container
    prel.container = StandardPacker::reference64( &prels, &rels, 0 );

    // First object
    prel.start = prels.sources().size();

    // reserve size
    const auto newSize = prels.sources().size() + rels.relations().size();
    prels.sources().reserve( newSize );
    prels.dests().reserve( newSize );

    // Loop over relations
    for ( const auto& R : rels.relations() ) {
      prels.sources().emplace_back( StandardPacker::reference64( &prels, R.from() ) );
      prels.dests().emplace_back( R.to() );
    }

    // last object
    prel.end = prels.sources().size();
  }

  StatusCode P2IntRelation::unpack( const PackedRelations& prels, Relation1D<LHCb::Particle, int>& rels ) const {
    auto resolver = details::Resolver{ prels.linkMgr() };
    for ( const auto& prel : prels.data() ) {
      for ( int kk = prel.start; kk < prel.end; ++kk ) {
        const auto& src = prels.sources()[kk];
        const auto& dst = prels.dests()[kk];

        const auto f = resolver.object<details::FromContainer<DataVector>>( src );
        if ( !f ) {
          if ( f.error() == ErrorCode::WRONG_CONTAINER_TYPE )
            return f.error(); // data is there, but different type -- notify layer above to retry
          if ( parent().msgLevel( MSG::DEBUG ) )
            parent().debug() << f.error() << " while retrieving object with key " << resolver.key( src ) << " from "
                             << resolver.path( src ) << endmsg;
          continue;
        }

        StatusCode sc = rels.relate( f.value(), static_cast<int>( dst ) );
        if ( !sc )
          parent().warning() << "Something went wrong with relation unpacking "
                             << "sourceKey " << resolver.key( src ) << " sourceLink " << resolver.path( src ) << endmsg;
      }
    }
    if ( resolver.hasErrors() ) resolver.dump( parent().warning() );
    rels.i_sort();
    return StatusCode::SUCCESS; // TODO: check resolver.hasErrors()
  }

} // namespace LHCb::Packers

namespace LHCb {

  // known unpacking transformations
  StatusCode unpack( Gaudi::Algorithm const* parent, PackedWeightedRelations const& in,
                     RelationWeighted1D<ProtoParticle, MCParticle, double>& out ) {
    return Packers::WeightedRelation<ProtoParticle, MCParticle>{ parent }.unpack( in, out );
  }
  StatusCode unpack( Gaudi::Algorithm const* parent, PackedRelatedInfoRelations const& in,
                     Relation1D<Particle, RelatedInfoMap>& out ) {
    return Packers::P2InfoRelation{ parent }.unpack( in, out );
  }
  StatusCode unpack( Gaudi::Algorithm const* parent, PackedRelations const& in, Relation1D<Particle, RecVertex>& out ) {
    return Packers::ParticleRelation<RecVertex>{ parent }.unpack( in, out );
  }
  StatusCode unpack( Gaudi::Algorithm const* parent, PackedRelations const& in,
                     Relation1D<Particle, VertexBase>& out ) {
    return Packers::ParticleRelation<VertexBase>{ parent }.unpack( in, out );
  }
  StatusCode unpack( Gaudi::Algorithm const* parent, PackedRelations const& in,
                     Relation1D<Particle, MCParticle>& out ) {
    return Packers::ParticleRelation<MCParticle>{ parent }.unpack( in, out );
  }
  StatusCode unpack( Gaudi::Algorithm const* parent, PackedRelations const& in, Relation1D<Particle, int>& out ) {
    return Packers::P2IntRelation{ parent }.unpack( in, out );
  }

} // namespace LHCb
