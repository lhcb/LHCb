/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "BufferUnpackerBaseAlg.h"
#include "type_name.h"

using namespace LHCb::Packers::Traits;
using namespace LHCb::Hlt::PackedData;

namespace {
#if 0
  class PTMap {
    // TODO: keep track of the names of all transient types for this packed type
    struct Item {
      std::string_view              packed;
      std::vector<std::string_view> transient;
      Item( std::string_view p, std::string_view t ) : packed{p}, transient{t} {}
    };
    std::map<CLID, Item> m;

  public:
    PTMap() = default;
    template <typename P, typename T>
    void append() {
      constexpr auto t = type_name<T>();
      constexpr auto p = type_name<P>();
      auto [i, ok]     = m.try_emplace( P::classID(), p, t );
      if ( !ok ) i->second.transient.push_back( t );
    }
    auto begin() const { return m.begin(); }
    auto end() const { return m.end(); }
  };
#endif

  template <typename DataVector>
  Expected<std::pair<std::string, DataObject const*>> resolveObject( PackedDataInBuffer& buffer,
                                                                     ObjectHeader const& header, Loader& loader ) {
    auto loc = loader.decoder()(
        loader.decoder().remap_v2( header.locationID ).value_or( header.locationID ) ); // FIXME: have decoder 'rewrite'
                                                                                        // the tables if v2 instead...
    if ( !loc ) return Unexpected{ ErrorCode::UNKNOWN_LOCATIONID };
    if ( auto obj = loader.get( *loc ); obj ) return std::pair{ *loc, *obj };
    auto obj = restoreObject<DataVector>( buffer, header, loader );
    if ( !obj ) return Unexpected{ obj.error() };
    auto ptr = obj->get();
    if ( auto sc = loader.put( *loc, std::move( *obj ) ); sc.isFailure() ) return Unexpected{ sc };
    return std::pair{ *loc, ptr };
  }

  template <typename... DataVector>
  struct Create_t {
    auto operator()() const {
      std::multimap<CLID, LoaderFn_t> loaders;
      ( loaders.emplace( packed_representation<DataVector>::classID(), &resolveObject<DataVector> ), ... );
      if ( loaders.size() != sizeof...( DataVector ) ) throw std::logic_error( "Failure filling unpacking map" );
      return loaders;
    }
  };
} // namespace

namespace LHCb::Hlt::PackedData {
  const std::multimap<CLID, LoaderFn_t> Loader::s_map = expand_t<Create_t>{}();
}
