/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/IRegistry.h"
#include <string>
#include <type_traits>

namespace DataPacking::Buffer {

  template <typename Data>
  class RegistryWrapper : public IRegistry {
    std::string       m_path             = {};
    IDataProviderSvc* m_pDataProviderSvc = nullptr;
    Data              m_data;

  public:
    template <typename... Args, typename = std::enable_if_t<std::is_constructible_v<Data, Args...>>>
    RegistryWrapper( std::string location, Args&&... args )
        : m_path{ std::move( location ) }, m_data{ std::forward<Args>( args )... } {
      m_data.setRegistry( this );
    }
    ~RegistryWrapper() { m_data.setRegistry( nullptr ); }

    RegistryWrapper( RegistryWrapper&& )            = default;
    RegistryWrapper& operator=( RegistryWrapper&& ) = default;

    Data*       operator->() { return &m_data; }
    Data const* operator->() const { return &m_data; }
    Data&       operator*() { return m_data; }
    Data const& operator*() const { return m_data; }

    void setDataSvc( IDataProviderSvc* s ) { m_pDataProviderSvc = s; }

    Data&       value() & { return m_data; }
    Data const& value() const& { return m_data; }

    Data&&       value() && { return m_data; }
    Data const&& value() const&& { return m_data; }

    // IRegistry interface
    unsigned long     addRef() override { return 1; }
    unsigned long     release() override { return 1; }
    const name_type&  name() const override { return m_path; }
    const id_type&    identifier() const override { return m_path; }
    IDataProviderSvc* dataSvc() const override { return m_pDataProviderSvc; }
    DataObject*       object() const override { return const_cast<Data*>( &m_data ); } // YUCK!
    IOpaqueAddress*   address() const override { return nullptr; }
    void              setAddress( IOpaqueAddress* ) override {}
  };
} // namespace DataPacking::Buffer
