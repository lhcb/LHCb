
/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/CaloChargedInfo_v1.h"
#include "Event/StandardPacker.h"
#include "Event/Track_v1.h"
#include <vector>

// Include files
namespace LHCb {
  class CaloCluster;
  class CaloHypo;
  class RecVertex;
  class TwoProngVertex;
  class PrimaryVertex;
  class Particle;
  class ProtoParticle;
  class Vertex;
  class FlavourTag;
  class RichPID;
  class MuonPID;
  class GlobalChargedPID;
  class NeutralPID;

  namespace shared_objects_container_clids {
    namespace details {
      template <typename T>
      struct clid_;
      template <>
      struct clid_<CaloCluster> : std::integral_constant<CLID, 1841> {};
      template <>
      struct clid_<Event::v1::Track> : std::integral_constant<CLID, 1850> {};
      template <>
      struct clid_<CaloHypo> : std::integral_constant<CLID, 1851> {};
      template <>
      struct clid_<RecVertex> : std::integral_constant<CLID, 1853> {};
      template <>
      struct clid_<TwoProngVertex> : std::integral_constant<CLID, 1854> {};
      template <>
      struct clid_<PrimaryVertex> : std::integral_constant<CLID, 1855> {};
      template <>
      struct clid_<Particle> : std::integral_constant<CLID, 1881> {};
      template <>
      struct clid_<ProtoParticle> : std::integral_constant<CLID, 1852> {};
      template <>
      struct clid_<Vertex> : std::integral_constant<CLID, 1882> {};
      template <>
      struct clid_<FlavourTag> : std::integral_constant<CLID, 1883> {};
      template <>
      struct clid_<RichPID> : std::integral_constant<CLID, 1861> {};
      template <>
      struct clid_<MuonPID> : std::integral_constant<CLID, 1871> {};
      template <>
      struct clid_<NeutralPID> : std::integral_constant<CLID, 1891> {};
      template <>
      struct clid_<Event::Calo::v1::CaloChargedPID> : std::integral_constant<CLID, 1892> {};
      template <>
      struct clid_<Event::Calo::v1::BremInfo> : std::integral_constant<CLID, 1893> {};
      template <>
      struct clid_<GlobalChargedPID> : std::integral_constant<CLID, 1894> {};
    } // namespace details

    template <typename T>
    constexpr CLID clid_for = details::clid_<T>::value;

  } // namespace shared_objects_container_clids
  /** @class PackedSharedObjectsContainer PackedSharedObjectsContainer.h Event/PackedSharedObjectsContainer.h
   *
   *  Packed SharedObjectsContainer
   *
   */

  template <typename ValueType>
  class PackedSharedObjectsContainer : public DataObject {

  public:
    /// Class ID
    static CLID classID() { return shared_objects_container_clids::clid_for<ValueType>; }

    /// Class ID
    const CLID& clID() const override {
      static const CLID id = classID();
      return id;
    }
    /// packing version
    static char packingVersion() { return 0; }

  public:
    auto begin() const { return m_source.begin(); }
    auto end() const { return m_source.end(); }
    auto size() const { return m_source.size(); }
    void reserve( size_t s ) { m_source.reserve( s ); }

    auto& emplace_back( const ValueType* obj ) {
      return m_source.emplace_back( StandardPacker::reference64( this, obj ) );
    }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      // the object does not define a packing version, so save 0
      buf.save( static_cast<uint8_t>( packingVersion() ) );
      buf.save( m_source );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      // the object does not define a packing version, but we save one
      auto version = buf.template load<uint8_t>();
      // setVersion( version );
      if ( packingVersion() != version ) {
        throw GaudiException( "packing version is not supported: " + std::to_string( version ), __PRETTY_FUNCTION__,
                              StatusCode::FAILURE );
      }
      buf.load( m_source );
    }

    std::vector<std::int64_t> const& data() const { return m_source; }

  private:
    std::vector<std::int64_t> m_source;
  };

} // namespace LHCb
