/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "Event/PrimaryVertex.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"

#include "fmt/format.h"

#include <string>
#include <vector>

namespace LHCb {

  /**
   *  Structure to describe a reconstructed vertex
   *
   *  @author Olivier Callot
   *  @date   2008-11-14
   */
  struct PackedPrimaryVertex {
    std::int32_t key{ 0 };
    std::int32_t chi2{ 0 };
    std::int32_t nDoF{ 0 };
    std::int32_t x{ 0 };
    std::int32_t y{ 0 };
    std::int32_t z{ 0 };
    std::int32_t cov00{ 0 };
    std::int32_t cov11{ 0 };
    std::int32_t cov22{ 0 };
    std::int16_t cov10{ 0 };
    std::int16_t cov20{ 0 };
    std::int16_t cov21{ 0 };
    std::int32_t container{ 0 };

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this );
    }
#endif
  };

  constexpr CLID CLID_PackedPrimaryVertices = 1812;

  // Namespace for locations in TDS
  namespace PackedPrimaryVertexLocation {
    inline const std::string Primary  = "pRec/Vertex/PrimaryVerices";
    inline const std::string InStream = "/pPhys/PrimaryVertices";
  } // namespace PackedPrimaryVertexLocation
  using PrimaryVertices = KeyedContainer<LHCb::PrimaryVertex, Containers::HashMap>;

  /**
   *  Container of packed PrimaryVertex objects
   *
   *  @author Olivier Callot
   *  @date   2008-11-14
   */
  class PackedPrimaryVertices : public DataObject {

  public:
    /// Default Packing Version
    static char defaultPackingVersion() { return 2; }

    /// Vector of packed objects
    typedef std::vector<LHCb::PackedPrimaryVertex> Vector;

    /// Standard constructor
    PackedPrimaryVertices() = default;

    const CLID&        clID() const override { return PackedPrimaryVertices::classID(); }
    static const CLID& classID() { return CLID_PackedPrimaryVertices; }

    std::vector<PackedPrimaryVertex>&       data() { return m_vect; }
    const std::vector<PackedPrimaryVertex>& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 1 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PrimaryVertices packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
    }

    // perform unpacking
    friend StatusCode unpack( Gaudi::Algorithm const*, const PackedPrimaryVertices&, PrimaryVertices& );

  private:
    std::vector<PackedPrimaryVertex> m_vect;

    /// Data packing version
    char m_packingVersion{ defaultPackingVersion() };
  };

  /**
   *  Utility class to handle the packing and unpacking of the PrimaryVertices
   *  This only packes the minimal info: position, covariance matrix and chi2
   *
   */

  class PrimaryVertexPacker : public PackerBase {

  public:
    // These are required by the templated algorithms
    typedef LHCb::PrimaryVertex         Data;
    typedef LHCb::PackedPrimaryVertex   PackedData;
    typedef LHCb::PrimaryVertices       DataVector;
    typedef LHCb::PackedPrimaryVertices PackedDataVector;
    // using Data = LHCb::Event::PV::PrimaryVertex ;
    // using PackedData = LHCb::PackedPrimaryVertex ;
    // using DataVector = LHCb::Event::PV::PrimaryVertex::Container ;
    // using PackedDataVector = LHCb::PackedPrimaryVertices ;
    constexpr inline static auto propertyName() { return "LightPVs"; }

    using PackerBase::PackerBase;

    /// Pack Vertices
    template <typename Range>
    void pack( const Range& verts, PackedDataVector& pverts ) const {
      pverts.data().reserve( verts.size() );
      for ( const auto* vert : verts ) pack( *vert, pverts.data().emplace_back(), pverts );
    }

    /// Unpack a Vertex
    StatusCode unpack( const PackedData& pvert, Data& vert, const PackedDataVector& pverts, DataVector& verts ) const;

    /// Unpack Vertices
    StatusCode unpack( const PackedDataVector& pverts, DataVector& verts ) const;

    /// Compare two ProtoParticles to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;
    StatusCode check( const Data* dataA, const Data* dataB ) const { return check( *dataA, *dataB ); }

  private:
    /// Pack a Vertex
    void pack( const Data& vert, PackedData& pvert, PackedDataVector& pverts ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 2 == ver || 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "PrimaryVertexPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
