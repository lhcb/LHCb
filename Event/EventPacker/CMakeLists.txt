###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/EventPacker
-----------------
#]=======================================================================]

gaudi_add_library(EventPackerLib
    SOURCES
        src/lib/PackedCaloAdc.cpp
        src/lib/PackedCaloCluster.cpp
        src/lib/PackedCaloDigit.cpp
        src/lib/PackedCaloHypo.cpp
        src/lib/PackedCaloChargedInfo_v1.cpp
        src/lib/PackedCluster.cpp
        src/lib/PackedEventChecks.cpp
        src/lib/PackedFlavourTag.cpp
        src/lib/PackedGlobalChargedPID.cpp
        src/lib/PackedMCCaloHit.cpp
        src/lib/PackedMCHit.cpp
        src/lib/PackedMCRichDigitSummary.cpp
        src/lib/PackedMCRichHit.cpp
        src/lib/PackedMCRichOpticalPhoton.cpp
        src/lib/PackedMCRichSegment.cpp
        src/lib/PackedMCRichTrack.cpp
        src/lib/PackedMuonPID.cpp
        src/lib/PackedNeutralPID.cpp
        src/lib/PackedPartToRelatedInfoRelation.cpp
        src/lib/PackedParticle.cpp
        src/lib/PackedPrimaryVertex.cpp
        src/lib/PackedProtoParticle.cpp
        src/lib/PackedRecVertex.cpp
        src/lib/PackedRichPID.cpp
        src/lib/PackedTrack.cpp
        src/lib/PackedVertex.cpp
        src/lib/PackedTwoProngVertex.cpp
        src/lib/PackedWeightsVector.cpp
        src/lib/PackedRecSummary.cpp
        src/lib/StandardPacker.cpp
        src/lib/PackedDataBuffer.cpp
    LINK
        PUBLIC
            Rangev3::rangev3
            Boost::headers
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::DigiEvent
            LHCb::LHCbAlgsLib
            LHCb::MCEvent
            LHCb::PhysEvent
            LHCb::RecEvent
            LHCb::RelationsLib
            LHCb::TrackEvent
            LHCb::DAQEventLib
            LHCb::HltEvent
            LHCb::HltInterfaces
            ROOT::Core
)

gaudi_add_module(EventPacker
    SOURCES
        src/component/ErrorCategory.cpp
        src/component/SelectivePacker.cpp
        src/component/RelationPackers.cpp
        src/component/BufferUnpackerBaseAlg.cpp
        src/component/DumpTracks.cpp
        src/component/PackMCParticle.cpp
        src/component/PackMCVertex.cpp
        src/component/UnpackMCParticle.cpp
        src/component/UnpackMCVertex.cpp
        src/component/UnpackProtoParticle.cpp
        src/component/UnpackRecVertex.cpp
        src/component/MCPackers.cpp
        src/component/Unpackers.cpp
        src/component/BufferUnpackers.cpp
        src/component/PackedDataChecksum.cpp
    LINK
        Boost::headers
        fmt::fmt
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::DAQEventLib
        LHCb::DetDescLib
        LHCb::DigiEvent
        LHCb::EventPackerLib
        LHCb::HltEvent
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::MCEvent
        LHCb::MDFLib
        LHCb::MicroDstLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::RelationsLib
        LHCb::TrackEvent
)

gaudi_add_dictionary(EventPackerDict
    HEADERFILES dict/PackedEventDict.h
    SELECTION dict/PackedEventDict.xml
    LINK LHCb::EventPackerLib
  )

gaudi_add_executable(test_compression
  SOURCES tests/src/test_compression.cpp
  LINK
      Boost::unit_test_framework
      LHCb::EventPackerLib
  TEST
  )

gaudi_add_executable(test_checksums
  SOURCES tests/src/test_checksums.cpp
  LINK
      Boost::unit_test_framework
      LHCb::EventPackerLib
  TEST
  )
