/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/NamedRange.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Point4DTypes.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/SmartRef.h"
#include "GaudiKernel/SmartRefVector.h"
#include "Kernel/RelationObjectTypeTraits.h"
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations
  class MCParticle;

  // Class ID definition
  static const CLID CLID_MCVertex = 220;

  // Namespace for locations in TDS
  namespace MCVertexLocation {
    inline const std::string Default = "MC/Vertices";
  }

  /** @class MCVertex MCVertex.h
   *
   * Monte Carlo vertex information
   *
   * @author Gloria Corti, revised by P. Koppenburg
   *
   */

  class MCVertex final : public KeyedObject<int> {
  public:
    /// typedef for std::vector of MCVertex
    using Vector      = std::vector<MCVertex*>;
    using ConstVector = std::vector<const MCVertex*>;

    /// typedef for KeyedContainer of MCVertex
    using Container = KeyedContainer<MCVertex, Containers::HashMap>;
    /// The container type for shared MCVertices (without ownership)
    using Selection = SharedObjectsContainer<MCVertex>;
    /// For uniform access to containers in TES (KeyedContainer,SharedContainer)
    using Range = Gaudi::NamedRange_<ConstVector>;

    /// Describe the physics process related to the vertex
    enum MCVertexType {
      Unknown = 0,
      ppCollision,
      DecayVertex,
      OscillatedAndDecay,
      StringFragmentation,
      HadronicInteraction = 100,
      Bremsstrahlung,
      PairProduction,
      Compton,
      DeltaRay,
      PhotoElectric,
      Annihilation,
      RICHPhotoElectric,
      Cerenkov,
      RichHpdBackScat,
      GenericInteraction,
      LHCHalo        = 200,
      MuonBackground = 300,
      MuonBackgroundFlat,
      MuonBackgroundSpillover,
      WorldLimit = 400,
      KinematicLimit
    };
    friend std::ostream& operator<<( std::ostream&, MCVertexType );

    // Copy Constructor
    MCVertex( const LHCb::MCVertex& right )
        : KeyedObject<int>() // note: key is _not_ copied...
        , m_position( right.position() )
        , m_time( right.time() )
        , m_type( right.type() )
        , m_mother( right.mother() )
        , m_products( right.products() ) {}

    /// Default Constructor
    MCVertex() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::MCVertex::classID(); }
    static const CLID& classID() { return CLID_MCVertex; }

    /// Clone this MCVertex. Returns a pointer to a new MCVertex (user must take ownership)
    [[nodiscard]] MCVertex* clone() const { return new LHCb::MCVertex( *this ); }

    /// Clone this MCVertex including key. Returns a pointer to a new MCVertex (user must take ownership)
    [[nodiscard]] MCVertex* cloneWithKey() const {
      auto* clone = new LHCb::MCVertex( *this );
      clone->setKey( key() );
      return clone;
    }

    /// position time 4-vector
    [[nodiscard]] Gaudi::XYZTPoint position4vector() const {
      return { m_position.X(), m_position.Y(), m_position.Z(), m_time };
    }

    /// Returns true if this vertex is a primary vertex
    [[nodiscard]] bool isPrimary() const { return m_type == MCVertex::MCVertexType::ppCollision; }

    /// Returns true if this vertex is a decay vertex
    [[nodiscard]] bool isDecay() const {
      return m_type == MCVertex::MCVertexType::DecayVertex || m_type == MCVertex::MCVertexType::OscillatedAndDecay;
    }

    /// Get primary vertex up the decay tree
    [[nodiscard]] const LHCb::MCVertex* primaryVertex() const;

    /// Print this MCVertex in a human readable way
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  Position in LHCb reference system
    [[nodiscard]] const Gaudi::XYZPoint& position() const { return m_position; }

    /// Update  Position in LHCb reference system
    MCVertex& setPosition( Gaudi::XYZPoint value ) {
      m_position = std::move( value );
      return *this;
    }

    /// Retrieve const  Time since pp interaction
    [[nodiscard]] double time() const { return m_time; }

    /// Update  Time since pp interaction
    MCVertex& setTime( double value ) {
      m_time = value;
      return *this;
    }

    /// Retrieve const  How the vertex was made
    [[nodiscard]] const MCVertexType& type() const { return m_type; }

    /// Update  How the vertex was made
    MCVertex& setType( MCVertexType value ) {
      m_type = std::move( value );
      return *this;
    }

    /// Retrieve (const)  Pointer to parent particle that decay or otherwise end in this vertex
    [[nodiscard]] const LHCb::MCParticle* mother() const { return m_mother; }

    /// Update  Pointer to parent particle that decay or otherwise end in this vertex
    MCVertex& setMother( SmartRef<LHCb::MCParticle> value ) {
      m_mother = std::move( value );
      return *this;
    }

    /// Retrieve (const)  Pointer to daughter particles
    [[nodiscard]] const SmartRefVector<LHCb::MCParticle>& products() const { return m_products; }

    /// Update  Pointer to daughter particles
    MCVertex& setProducts( SmartRefVector<LHCb::MCParticle> value ) {
      m_products = std::move( value );
      return *this;
    }

    /// Add to  Pointer to daughter particles
    MCVertex& addToProducts( SmartRef<LHCb::MCParticle> value ) {
      m_products.push_back( std::move( value ) );
      return *this;
    }

    /// Remove from  Pointer to daughter particles
    MCVertex& removeFromProducts( const SmartRef<LHCb::MCParticle>& value ) {
      auto i = std::remove( m_products.begin(), m_products.end(), value );
      m_products.erase( i, m_products.end() );
      return *this;
    }

    /// Clear  Pointer to daughter particles
    MCVertex& clearProducts() {
      m_products.clear();
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const MCVertex& obj ) { return obj.fillStream( str ); }

  private:
    Gaudi::XYZPoint            m_position{ 0.0, 0.0, 0.0 };               ///< Position in LHCb reference system
    double                     m_time{ 0.0 };                             ///< Time since pp interaction
    MCVertexType               m_type{ MCVertex::MCVertexType::Unknown }; ///< How the vertex was made
    SmartRef<LHCb::MCParticle> m_mother; ///< Pointer to parent particle that decay or otherwise end in this vertex
    SmartRefVector<LHCb::MCParticle> m_products; ///< Pointer to daughter particles

  }; // class MCVertex

  /// Definition of Keyed Container for MCVertex
  using MCVertices = MCVertex::Container;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations
#include "MCParticle.h"

inline const LHCb::MCVertex* LHCb::MCVertex::primaryVertex() const {
  return isPrimary() ? this : m_mother ? m_mother->primaryVertex() : nullptr;
}

namespace Relations {
  template <typename>
  struct ObjectTypeTraits;

  template <>
  struct ObjectTypeTraits<LHCb::MCVertex> : KeyedObjectTypeTraits<LHCb::MCVertex> {};
} // namespace Relations
