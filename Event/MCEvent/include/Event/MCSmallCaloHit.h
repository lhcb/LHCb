/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MCCaloHit.h"

namespace LHCb {
  static const CLID CLID_MCSmallCaloHit = 2008;

  /** @class MCSmallCaloHit MCSmallCaloHit.h
   *
   * Special extension of MCCaloHit to support
   * calorimeters with higher granularity
   *
   * TODO: this is is just a testing class for now
   *
   * @author Michał Mazurek
   * @date 16/02/22
   *
   */

  class MCSmallCaloHit : public LHCb::MCCaloHit {
  public:
    using Dim = unsigned short;

    using MCCaloHit::MCCaloHit;

    const CLID&        clID() const override;
    static const CLID& classID();

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& ) const override;

    void setCol( Dim col ) { m_col = col; };
    Dim  col() { return m_col; };
    void setRow( Dim row ) { m_row = row; };
    Dim  row() { return m_row; };
    void setNCols( Dim ncols ) { m_ncols = ncols; };
    Dim  ncols() { return m_ncols; };
    void setNRows( Dim nrows ) { m_nrows = nrows; };
    Dim  nrows() { return m_nrows; };

  private:
    Dim m_row   = 0;
    Dim m_col   = 0;
    Dim m_nrows = 1;
    Dim m_ncols = 1;
  };
} // namespace LHCb

inline const CLID& LHCb::MCSmallCaloHit::clID() const { return LHCb::MCSmallCaloHit::classID(); }

inline const CLID& LHCb::MCSmallCaloHit::classID() { return CLID_MCSmallCaloHit; }

inline std::ostream& LHCb::MCSmallCaloHit::fillStream( std::ostream& str ) const {
  LHCb::MCCaloHit::fillStream( str );
  str << "{ "
      << "super cell col:	" << m_col << std::endl
      << "super cell row:	" << m_row << std::endl
      << " }";
  return str;
}
