/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENEVENT_HEPMCUTILS_H
#define GENEVENT_HEPMCUTILS_H 1

// Include files
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

#include "Event/HepMCEvent.h"

/** @namespace HepMCUtils HepMCUtils.h GenEvent/HepMCUtils.h
 *
 *  Utility functions to use HepMC Events
 *
 *  @author Patrick Robbe
 *  @date   2006-02-14
 */

namespace HepMCUtils {
  /// Returns true if trees of vertices V1 and V2 belong to the same tree
  bool commonTrees( HepMC::GenVertex* V1, const HepMC::GenVertex* V2 );

  /// Compare 2 HepMC GenParticle according to their barcode
  bool compareHepMCParticles( const HepMC::GenParticle* part1, const HepMC::GenParticle* part2 );

  /** Check if a particle is before or after oscillation.
   *  In HepMC description, the mixing is seen as a decay B0 -> B0bar. In this
   *  case, the B0 is said to be the BAtProduction contrary to the B0bar.
   *  @param[in] thePart  Particle to check.
   *  @return true if the particle is the particle before osillation.
   */
  bool IsBAtProduction( const HepMC::GenParticle* thePart );

  /// Remove all daughters of a particle
  void RemoveDaughters( HepMC::GenParticle* thePart );

  /// Comparison function as structure
  struct particleOrder {
    bool operator()( const HepMC::GenParticle* part1, const HepMC::GenParticle* part2 ) const {
      return part1->barcode() < part2->barcode();
    }
  };

  /// Type of HepMC particles container ordered with barcodes
  typedef std::set<HepMC::GenParticle*, particleOrder> ParticleSet;
} // namespace HepMCUtils

//=============================================================================
// Inline functions
//=============================================================================

//=============================================================================
// Function to test if vertices are in the same decay family
//=============================================================================
inline bool HepMCUtils::commonTrees( HepMC::GenVertex* V1, const HepMC::GenVertex* V2 ) {
  if ( !V2 || !V1 ) return false;
  return V1 == V2 ||
         std::find( V1->vertices_begin( HepMC::descendants ), V1->vertices_end( HepMC::descendants ), V2 ) !=
             V1->vertices_end( HepMC::descendants ) ||
         std::find( V1->vertices_begin( HepMC::ancestors ), V1->vertices_end( HepMC::ancestors ), V2 ) !=
             V1->vertices_end( HepMC::ancestors );
}

//=============================================================================
// Function to sort HepMC::GenParticles according to their barcode
//=============================================================================
inline bool HepMCUtils::compareHepMCParticles( const HepMC::GenParticle* part1, const HepMC::GenParticle* part2 ) {
  return ( part1->barcode() < part2->barcode() );
}

//=============================================================================
// Returns true if B is first B (removing oscillation B) and false
// if the B is the B after oscillation
//=============================================================================
inline bool HepMCUtils::IsBAtProduction( const HepMC::GenParticle* thePart ) {
  if ( ( abs( thePart->pdg_id() ) != 511 ) && ( abs( thePart->pdg_id() ) != 531 ) ) return true;
  if ( !thePart->production_vertex() ) return true;
  HepMC::GenVertex* theVertex = thePart->production_vertex();
  if ( 1 != theVertex->particles_in_size() ) return true;
  HepMC::GenParticle* theMother = ( *theVertex->particles_in_const_begin() );
  return ( theMother->pdg_id() != -thePart->pdg_id() );
}

//=============================================================================
// Erase the daughters of one particle
//=============================================================================
inline void HepMCUtils::RemoveDaughters( HepMC::GenParticle* theParticle ) {
  if ( !theParticle ) return;

  HepMC::GenVertex* EV = theParticle->end_vertex();
  if ( !EV ) return;

  theParticle->set_status( LHCb::HepMCEvent::statusType::StableInProdGen );
  HepMC::GenEvent* theEvent = theParticle->parent_event();

  std::vector<HepMC::GenVertex*> tempList;
  tempList.push_back( EV );

  for ( auto iterDes = EV->particles_begin( HepMC::descendants ); iterDes != EV->particles_end( HepMC::descendants );
        ++iterDes ) {
    if ( ( *iterDes )->end_vertex() ) tempList.push_back( ( *iterDes )->end_vertex() );
  }

  for ( auto iter = tempList.begin(); iter != tempList.end(); ++iter ) {
    theEvent->remove_vertex( *iter );
    delete *iter;
  }
}

#endif // GENEVENT_HEPMCUTILS_H
