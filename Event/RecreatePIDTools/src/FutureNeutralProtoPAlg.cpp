/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/TrackUtils.h"
#include "Core/FloatComparison.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "DetDesc/IDetectorElement.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloHypo.h"
#include "Event/NeutralPID.h"
#include "Event/ProtoParticle.h"
#include "LHCbAlgs/Transformer.h"
#include "fmt/format.h"

namespace LHCb {
  /**
   *  Creator of the neutral ProtoParticles from CaloHypos
   *
   *  The current version fills the following estimators for ProtoParticle
   *
   *  <ul>
   *  <li>  <i>CaloTrMatch</i>     as <b>minimal</b> of this estimator for all
   *        linked <i>CaloHypo</i> objects. The value is extracted from
   *        the relation table/associator as a relation weigth between
   *        <i>CaloCluster</i> and <i>TrStoredTrack</i> objects </li>
   *  <li>  <i>CaloDepositID</i>   as <b>maximal</b> of this estimator for all
   *        linked <i>CaloHypo</i> objects using Spd/Prs estimator tool
   *        written by Frederic Machefert </li>
   *  <li>  <i>CaloShowerShape</i> as <b>maximal</b> of the estimator for
   *        all linked <i>CaloHypo</i> objects. Estimator is equal to the
   *        sum of diagonal elements of cluster spread matrix (2nd order
   *        moments of the cluster) </li>
   *  <li>  <i>ClusterMass</i>     as <b>maximal</b> of the estimator of
   *        cluster mass using smart algorithm by Olivier Deschamp </li>
   *  <li>  <i>PhotonID</i>        as the estimator of PhotonID
   *        using nice identifiaction tool
   *        CaloPhotonEstimatorTool by Frederic Machefert *
   *  </ul>
   *
   *
   *  @author Olivier Deschamps
   *  @date   2006-06-09
   *  Adapted from NeutralPPsFromCPsAlg class (Vanya Belyaev Ivan.Belyaev@itep.ru)
   */

  using TrackMatchTable = LHCb::Calo::TrackUtils::Clusters2BestTrackMatch;

  class FutureNeutralProtoPAlg final
      : public LHCb::Algorithm::MultiTransformer<std::tuple<ProtoParticles, NeutralPIDs>(
                                                     const CaloHypos&, const CaloHypos&, const CaloHypos&,
                                                     const TrackMatchTable&, const DeCalorimeter&,
                                                     const DeCalorimeter& ),
                                                 Algorithm::Traits::usesConditions<DeCalorimeter, DeCalorimeter>> {

  public:
    /// Standard constructor
    FutureNeutralProtoPAlg( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            { KeyValue{ "MergedPi0s", CaloHypoLocation::MergedPi0s },
                              KeyValue{ "Photons", CaloHypoLocation::Photons },
                              KeyValue{ "SplitPhotons", CaloHypoLocation::SplitPhotons },
                              KeyValue{ "TrackMatchTable", "" },
                              KeyValue{ "DeEcal", Calo::Utilities::DeCaloFutureLocation( "Ecal" ) },
                              KeyValue{ "DeHcal", Calo::Utilities::DeCaloFutureLocation( "Hcal" ) } },
                            { KeyValue{ "ProtoParticleLocation", ProtoParticleLocation::Neutrals },
                              KeyValue{ "NeutralPID", "" } } ){};

    StatusCode initialize() override; ///< Algorithm initialization

    std::tuple<ProtoParticles, NeutralPIDs> operator()( const CaloHypos&, const CaloHypos&, const CaloHypos&,
                                                        const TrackMatchTable&, const DeCalorimeter&,
                                                        const DeCalorimeter& ) const override;

  private: // data
    Gaudi::Property<bool> m_light_mode{
        this, "LightMode", false, "Use 'light' mode and do not collect all information. Useful for Calibration." };

    ToolHandle<Calo::Interfaces::IHypoEstimator> m_estimator{ this, "CaloHypoEstimator", "CaloFutureHypoEstimator" };
    mutable Gaudi::Accumulators::StatCounter<>   m_countMergedPi0s{ this, "Neutral Protos from MergedPi0s" };
    mutable Gaudi::Accumulators::StatCounter<>   m_countPhotons{ this, "Neutral Protos from Photons" };
    mutable Gaudi::Accumulators::StatCounter<>   m_countSplitPhotons{ this, "Neutral Protos from SplitPhotons" };
    mutable Gaudi::Accumulators::StatCounter<>   m_countProtos{ this, "Neutral Protos" };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( FutureNeutralProtoPAlg, "FutureNeutralProtoPAlg" )
} // namespace LHCb

StatusCode LHCb::FutureNeutralProtoPAlg::initialize() {
  return MultiTransformer::initialize().andThen( [&] {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

    if ( m_light_mode.value() ) info() << "FutureNeutral protoparticles will be created in 'Light' Mode" << endmsg;

    // TODO: Move to configuration
    auto& h2c = dynamic_cast<IProperty&>( *m_estimator->hypo2Calo() );
    h2c.setProperty( "Seed", "false" ).ignore();
    h2c.setProperty( "PhotonLine", "true" ).ignore();
    h2c.setProperty( "AddNeighbors", "false" ).ignore();
  } );
}

std::tuple<LHCb::ProtoParticles, LHCb::NeutralPIDs>                                       //
LHCb::FutureNeutralProtoPAlg::operator()( const LHCb::CaloHypos& hypos_from_mergedPi0s,   //
                                          const LHCb::CaloHypos& hypos_from_Photons,      //
                                          const LHCb::CaloHypos& hypos_from_SplitPhotons, //
                                          const TrackMatchTable& tracktable,              //
                                          const DeCalorimeter&   ecal,                    //
                                          const DeCalorimeter&   hcal ) const {

  auto ret = std::tuple{ LHCb::ProtoParticles{}, NeutralPIDs{} };

  auto& [protos, container_NeutralPIDs] = ret;

  // prepare cluster access, both to find clusters (indexed by cellids) and relation tabled viewed from clusters
  auto clusters  = tracktable.from()->index();
  auto tableview = tracktable.buildView();

  // wrapper around data map getter, to include track match table the estimator doesn't have access to
  auto getData = [&]( auto const* hypo ) {
    // hypo data
    auto data = m_estimator->get_data( ecal, hcal, *hypo );
    // track relation
    using namespace LHCb::Calo::Enum;
    auto it      = data.find( DataType::CellID );
    auto cellid  = ( it != data.end() ) ? static_cast<unsigned>( it->second ) : 0u;
    auto cluster = clusters.find( LHCb::Detector::Calo::CellID( cellid ) );
    if ( cluster != clusters.end() ) {
      auto clsview = tableview.scalar()[cluster->indices().cast()];
      if ( clsview.hasRelation() )
        data[DataType::ClusterMatch] = clsview.relation().template get<LHCb::Calo::TrackUtils::ClusterMatch>().cast();
    }
    return data;
  };

  // -- reset mass storage
  std::map<const int, double> mass_per_cell = { {} };
  // Get masses
  for ( const auto* hypo : hypos_from_mergedPi0s ) {
    if ( !hypo ) continue;
    using namespace LHCb::Calo::Enum;
    // The call to process is happening twice for mergedPi0s, should be made smarter.
    auto      data          = getData( hypo );
    const int cellCode      = data.at( DataType::CellID );
    auto      it            = data.find( DataType::HypoM );
    mass_per_cell[cellCode] = ( it != data.end() ? it->second : Default );
  }

  //------ loop over all caloHypo containers
  auto append_protos_from_location = [&]( const auto& hypos, auto count_protos ) {
    // this needs to be re-declared because of the bug in clang 12 that reference to local binding fails in enclosing
    // function
    auto& [protos, container_NeutralPIDs] = ret;

    int count = 0;

    // == Loop over CaloHypos
    for ( const auto* hypo : hypos ) {
      if ( !hypo ) { continue; }
      count++;

      // == create and store the corresponding ProtoParticle
      auto* proto = new LHCb::ProtoParticle();
      protos.insert( proto );

      // == link CaloHypo to ProtoP
      using namespace LHCb::Calo::Enum;
      proto->addToCalo( hypo );
      if ( m_light_mode.value() ) continue;

      const auto hypothesis = hypo->hypothesis();

      auto data = getData( hypo );

      // create NeutralPID object
      auto pid = std::make_unique<LHCb::NeutralPID>();

      auto cellidcode = data.find( DataType::CellID ) != data.end() ? data.find( DataType::CellID )->second : Default;
      pid->setCaloNeutralID( LHCb::Detector::Calo::CellID( (unsigned)cellidcode ) );

      auto set = [&]( auto fn, const DataType t ) {
        auto i = data.find( t );
        if ( i != data.end() ) { ( *pid.*fn )( i->second ); }
      };

      // add data to NeutralPID object
      if ( hypothesis == LHCb::CaloHypo::Hypothesis::Photon || hypothesis == LHCb::CaloHypo::Hypothesis::Pi0Merged ) {
        const auto it = mass_per_cell.find( cellidcode );
        if ( it != mass_per_cell.end() ) pid->setClusterMass( it->second );
      }
      set( &LHCb::NeutralPID::setCaloNeutralEcal, DataType::ClusterE );
      set( &LHCb::NeutralPID::setCaloTrMatch, DataType::ClusterMatch );
      set( &LHCb::NeutralPID::setShowerShape, DataType::Spread );
      set( &LHCb::NeutralPID::setCaloNeutralHcal2Ecal, DataType::Hcal2Ecal );
      set( &LHCb::NeutralPID::setCaloNeutralE19, DataType::E19 );
      set( &LHCb::NeutralPID::setCaloNeutralE49, DataType::E49 );
      set( &LHCb::NeutralPID::setCaloClusterCode, DataType::ClusterCode );
      set( &LHCb::NeutralPID::setCaloClusterFrac, DataType::ClusterFrac );
      set( &LHCb::NeutralPID::setSaturation, DataType::Saturation );

      if ( hypothesis != LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 ) {
        set( &LHCb::NeutralPID::setIsNotH, DataType::isNotH );
        set( &LHCb::NeutralPID::setIsPhoton, DataType::isPhoton );
      }

      proto->setNeutralPID( pid.get() );
      container_NeutralPIDs.insert( pid.release() );

    } // loop over CaloHypos
    count_protos += count;
  };

  append_protos_from_location( hypos_from_mergedPi0s, m_countMergedPi0s.buffer() );
  append_protos_from_location( hypos_from_Photons, m_countPhotons.buffer() );
  append_protos_from_location( hypos_from_SplitPhotons, m_countSplitPhotons.buffer() );

  m_countProtos += protos.size();

  return ret;
}
