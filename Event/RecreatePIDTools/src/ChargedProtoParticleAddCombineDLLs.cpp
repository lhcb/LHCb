/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file ChargedProtoAddCombineDLLs.cpp
 *
 * Implementation file for tool ChargedProtoAddCombineDLLs
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 15/11/2006
 */
//-----------------------------------------------------------------------------

#include "Event/ProtoParticle.h"
#include "Event/RichPID.h"
#include "GaudiAlg/GaudiTool.h"
#include "Interfaces/IProtoParticleTool.h"

namespace LHCb::Rec::ProtoParticle::Charged {
  namespace {
    //-----------------------------------------------------------------------------
    /** @class CombinedLL ChargedProtoPAlg.h
     *
     *  Utility class holding the combined LL values for a ProtoParticle
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date 29/03/2006
     */
    //-----------------------------------------------------------------------------
    class CombinedLL {
    public:
      /// Standard constructor with initialisation value
      CombinedLL( double init = 0 )
          : elDLL( init ), muDLL( init ), piDLL( init ), kaDLL( init ), prDLL( init ), deDLL( init ){};
      double elDLL{ 0 }; ///< Electron Log Likelihood
      double muDLL{ 0 }; ///< Muon Log Likelihood
      double piDLL{ 0 }; ///< Pion Log Likelihood
      double kaDLL{ 0 }; ///< Kaon Log Likelihood
      double prDLL{ 0 }; ///< Proton Log Likelihood
      double deDLL{ 0 }; ///< Deuteron Log Likelihood
    public:
      /// Implement ostream << method
      friend std::ostream& operator<<( std::ostream& s, const CombinedLL& dlls ) {
        return s << "[ " << dlls.elDLL << " " << dlls.muDLL << " " << dlls.piDLL << " " << dlls.kaDLL << " "
                 << dlls.prDLL << " " << dlls.deDLL << " ]";
      }
    };

    struct Cmp {
      using is_transparent = void;
      bool operator()( std::string_view lhs, std::string_view rhs ) const {
        return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(),
                                             []( char l, char r ) { return std::toupper( l ) < std::toupper( r ); } );
      }
    };
    using namespace std::string_view_literals;
    const auto m_maskTechnique = std::map<std::string_view, int, Cmp>{ { { "RICH"sv, 0x1 },
                                                                         { "MUON"sv, 0x2 },
                                                                         { "ECAL"sv, 0x4 },
                                                                         { "HCAL"sv, 0x8 },
                                                                         { "PRS"sv, 0x10 },
                                                                         { "SPD"sv, 0x20 },
                                                                         { "BREM"sv, 0x40 },
                                                                         { "CALO"sv, 0x7C } },
                                                                       Cmp{} };
  } // namespace

  class AddCombineDLLs final : public extends<GaudiTool, Interfaces::IProtoParticles> {

  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;                                              ///< Algorithm initialization
    StatusCode operator()( ProtoParticles&, IGeometryInfo const& ) const override; ///< Algorithm execution

  private: // methods
    /// Add the Rich DLL information to the combined DLL
    bool addRich( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const;

    /// Add the Muon DLL information to the combined DLL
    bool addMuon( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const;

    /// Add the Calo DLL information to the combined DLL
    bool addCalo( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const;

  private: // data
    Gaudi::Property<std::vector<std::string>> m_elDisable{ this, "ElectronDllDisable", {} };
    Gaudi::Property<std::vector<std::string>> m_muDisable{ this, "MuonDllDisable", {} };
    Gaudi::Property<std::vector<std::string>> m_kaDisable{ this, "KaonDllDisable", {} };
    Gaudi::Property<std::vector<std::string>> m_piDisable{ this, "ProtonDllDisable", {} };
    Gaudi::Property<std::vector<std::string>> m_prDisable{ this, "PionllDisable", {} };
    Gaudi::Property<std::vector<std::string>> m_deDisable{ this, "DeuteronDllDisable", {} };

    Gaudi::Property<bool> m_removeOldInfo{ this, "RemoveOldInfo", true,
                                           "Only to be set to false during unpacking old files!" };

    int m_elCombDll{ 0xFFFF };
    int m_muCombDll{ 0xFFFF };
    int m_prCombDll{ 0xFFFF };
    int m_piCombDll{ 0xFFFF };
    int m_kaCombDll{ 0xFFFF };
    int m_deCombDll{ 0xFFFF };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( AddCombineDLLs, "ChargedProtoParticleAddCombineDLLs" )

  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode AddCombineDLLs::initialize() {
    StatusCode scc = extends::initialize();
    if ( scc.isFailure() ) return scc;

    for ( const auto& itech : m_elDisable ) {
      auto i = m_maskTechnique.find( itech );
      if ( i == m_maskTechnique.end() ) {
        error() << "Electron PID technique " << itech << " unknown" << endmsg;
        scc = StatusCode::FAILURE;
      } else {
        m_elCombDll &= ~i->second;
      }
    }
    for ( const auto& itech : m_muDisable ) {
      auto i = m_maskTechnique.find( itech );
      if ( i == m_maskTechnique.end() ) {
        error() << "Muon PID technique " << itech << " unknown" << endmsg;
        scc = StatusCode::FAILURE;
      } else {
        m_muCombDll &= ~i->second;
      }
    }
    for ( const auto& itech : m_prDisable ) {
      auto i = m_maskTechnique.find( itech );
      if ( i == m_maskTechnique.end() ) {
        error() << "Proton PID technique " << itech << " unknown" << endmsg;
        scc = StatusCode::FAILURE;
      } else {
        m_prCombDll &= ~i->second;
      }
    }
    for ( const auto& itech : m_piDisable ) {
      auto i = m_maskTechnique.find( itech );
      if ( i == m_maskTechnique.end() ) {
        error() << "Pion PID technique " << itech << " unknown" << endmsg;
        scc = StatusCode::FAILURE;
      } else {
        m_piCombDll &= ~i->second;
      }
    }
    for ( const auto& itech : m_kaDisable ) {
      auto i = m_maskTechnique.find( itech );
      if ( i == m_maskTechnique.end() ) {
        error() << "Kaon PID technique " << itech << " unknown" << endmsg;
        scc = StatusCode::FAILURE;
      } else {
        m_kaCombDll &= ~i->second;
      }
    }

    info() << "Using retuned RICH el and mu DLL values in combined DLLs" << endmsg;
    if ( 0 == m_elCombDll )
      Warning( "Not creating Combined DLL for electron hypothesis", StatusCode::SUCCESS ).ignore();
    if ( 0 == m_muCombDll ) Warning( "Not creating Combined DLL for muon hypothesis", StatusCode::SUCCESS ).ignore();
    if ( 0 == m_piCombDll ) Warning( "Not creating Combined DLL for pion hypothesis", StatusCode::SUCCESS ).ignore();
    if ( 0 == m_kaCombDll ) Warning( "Not creating Combined DLL for kaon hypothesis", StatusCode::SUCCESS ).ignore();
    if ( 0 == m_prCombDll ) Warning( "Not creating Combined DLL for proton hypothesis", StatusCode::SUCCESS ).ignore();
    if ( 0 == m_deCombDll )
      Warning( "Not creating Combined DLL for deuteron hypothesis", StatusCode::SUCCESS ).ignore();

    return scc;
  }

  //=============================================================================
  // Main execution
  //=============================================================================
  StatusCode AddCombineDLLs::operator()( ProtoParticles& protos, IGeometryInfo const& ) const ///< Algorithm execution
  {

    // Loop over the protos
    for ( auto* proto : protos ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "Creating Combined DLLs for ProtoParticle " << proto->key() << endmsg;

      // Remove any current Combined DLL information
      if ( m_removeOldInfo ) proto->removeCombinedInfo();

      bool hasPIDinfo( false );

      // Combined DLL data object for this proto
      CombinedLL combDLL( 0 );

      // Add any RICH info
      hasPIDinfo |= addRich( proto, combDLL );

      // Add any MUON info
      hasPIDinfo |= addMuon( proto, combDLL );

      // Add any CALO info
      hasPIDinfo |= addCalo( proto, combDLL );

      if ( !hasPIDinfo ) continue;

      // Store the final combined DLL values into the ProtoParticle
      bool                    add_pid = !proto->globalChargedPID();
      auto                    new_pid = std::make_unique<LHCb::GlobalChargedPID>();
      LHCb::GlobalChargedPID* pid     = add_pid ? new_pid.get() : proto->globalChargedPID();
      pid->setCombDLLe( combDLL.elDLL - combDLL.piDLL );
      pid->setCombDLLmu( combDLL.muDLL - combDLL.piDLL );
      pid->setCombDLLk( combDLL.kaDLL - combDLL.piDLL );
      pid->setCombDLLp( combDLL.prDLL - combDLL.piDLL );
      pid->setCombDLLd( combDLL.deDLL - combDLL.piDLL );
      if ( add_pid ) proto->setGlobalChargedPID( new_pid.release() );
    } // loop over protos

    return StatusCode::SUCCESS;
  }

  bool AddCombineDLLs::addRich( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const {
    // Add RICH Dll information.
    bool ok  = false;
    auto pid = proto->richPID();
    if ( pid ) {
      ok = true;
      // Apply renormalisation of RICH el and mu DLL values
      // Eventually, should make these tunable job options ....
      const int rTechnique = m_maskTechnique.at( "RICH" );
      if ( 0 != ( m_elCombDll & rTechnique ) ) combDLL.elDLL += pid->scaledDLLForCombDLL( Rich::Electron );
      if ( 0 != ( m_muCombDll & rTechnique ) ) combDLL.muDLL += pid->scaledDLLForCombDLL( Rich::Muon );
      if ( 0 != ( m_piCombDll & rTechnique ) ) combDLL.piDLL += pid->scaledDLLForCombDLL( Rich::Pion );
      if ( 0 != ( m_kaCombDll & rTechnique ) ) combDLL.kaDLL += pid->scaledDLLForCombDLL( Rich::Kaon );
      if ( 0 != ( m_prCombDll & rTechnique ) ) combDLL.prDLL += pid->scaledDLLForCombDLL( Rich::Proton );
      if ( 0 != ( m_deCombDll & rTechnique ) ) combDLL.deDLL += pid->scaledDLLForCombDLL( Rich::Deuteron );
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Adding RICH info " << combDLL << endmsg;
    }
    return ok;
  }

  bool AddCombineDLLs::addMuon( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const {

    bool ok = false;
    if ( auto pid = proto->muonPID() ) {
      ok                   = true;
      const int mTechnique = m_maskTechnique.at( "MUON" );
      if ( 0 != ( m_elCombDll & mTechnique ) ) combDLL.elDLL += pid->IsMuon() ? pid->MuonLLBg() : 0.;
      if ( 0 != ( m_muCombDll & mTechnique ) ) combDLL.muDLL += pid->IsMuon() ? pid->MuonLLMu() : 0.;
      if ( 0 != ( m_piCombDll & mTechnique ) ) combDLL.piDLL += pid->IsMuon() ? pid->MuonLLBg() : 0.;
      if ( 0 != ( m_kaCombDll & mTechnique ) ) combDLL.kaDLL += pid->IsMuon() ? pid->MuonLLBg() : 0.;
      if ( 0 != ( m_prCombDll & mTechnique ) ) combDLL.prDLL += pid->IsMuon() ? pid->MuonLLBg() : 0.;
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Adding MUON info " << combDLL << endmsg;
    }
    return ok;
  }

  bool AddCombineDLLs::addCalo( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const {
    bool ok  = false;
    auto pid = proto->caloChargedPID();
    if ( pid && pid->InEcal() ) {
      const int eTechnique = m_maskTechnique.at( "ECAL" );
      if ( 0 != ( m_elCombDll & eTechnique ) ) combDLL.elDLL += pid->EcalPIDe();
      if ( 0 != ( m_muCombDll & eTechnique ) ) combDLL.muDLL += pid->EcalPIDmu();
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Adding ECAL info " << combDLL << endmsg;
      ok = true;
    }

    if ( pid && pid->InHcal() ) {
      const int hTechnique = m_maskTechnique.at( "HCAL" );
      if ( 0 != ( m_elCombDll & hTechnique ) ) combDLL.elDLL += pid->HcalPIDe();
      if ( 0 != ( m_muCombDll & hTechnique ) ) combDLL.muDLL += pid->HcalPIDmu();
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Adding HCAL info " << combDLL << endmsg;
      ok = true;
    }

    auto brem = proto->bremInfo();
    if ( brem && brem->InBrem() ) {
      const int bTechnique = m_maskTechnique.at( "BREM" );
      if ( 0 != ( m_elCombDll & bTechnique ) ) combDLL.elDLL += brem->BremPIDe();
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Adding BREM info " << combDLL << endmsg;
      ok = true;
    }
    return ok;
  }

} // namespace LHCb::Rec::ProtoParticle::Charged
//=============================================================================
