###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/DAQEvent
--------------
#]=======================================================================]

gaudi_add_library(DAQEventLib
    SOURCES
        src/ODIN.cpp
        src/ODIN_standalone.cpp
        src/RawBank.cpp
        src/RawEvent.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::LHCbKernel
)

gaudi_add_dictionary(DAQEventDict
    HEADERFILES xml/lcgdict/lcg_headers.h
    SELECTION xml/lcgdict/lcg_selection.xml
    LINK LHCb::DAQEventLib
)

foreach(test_name IN ITEMS TestRawEvent test_ODIN_no_Gaudi test_ODIN_codecs)
    gaudi_add_executable(${test_name}
        SOURCES tests/src/${test_name}.cpp
        LINK
            Boost::unit_test_framework
            LHCb::DAQEventLib
        TEST
    )
endforeach()
target_link_libraries(TestRawEvent PRIVATE ROOT::Tree)
