/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#pragma once

#ifndef ODIN_WITHOUT_GAUDI
#  include <GaudiKernel/DataObject.h>
#  include <GaudiKernel/Time.h>
#  include <Kernel/STLExtensions.h>
#elif defined( __CUDACC__ ) || defined( __HIP__ )
// FIXME: we assume that if we build for CUDA or HIP we are in Allen
//        so that we can use Allen::device::span (as gsl::span does not work on HIP)
#  include <BackendCommon.h>
namespace LHCb {
  using Allen::device::span;
}
#else
#  if defined( __GNUC__ ) || defined( __clang__ )
#    define UNLIKELY( x ) __builtin_expect( ( x ), 0 )
#  else
#    define UNLIKELY( x ) x
#  endif
#  include <gsl/span>
#  include <iostream>
namespace LHCb {
  using gsl::span;
}
#endif
#include <array>
#include <bit>
#include <cassert>
#include <cstdint>
#include <cstring>

// define macros to target host and device build for accelerators
#if defined( __CUDACC__ ) || defined( __HIP__ )
#  define ACCEL_TARGET_SPEC __host__ __device__
#else
#  define ACCEL_TARGET_SPEC
#endif

namespace LHCb {

#ifndef ODIN_WITHOUT_GAUDI
  // Class ID definition
  static const CLID CLID_ODIN = 1005;

  // Namespace for locations in TDS
  namespace ODINLocation {
    inline const std::string Default = "DAQ/ODIN";
  }
#endif

  namespace ODINImplementation::details {
    /// Helper to extract COUNT bits starting from OFFSET in a buffer.
    template <unsigned int COUNT, unsigned int OFFSET>
    constexpr ACCEL_TARGET_SPEC auto get_bits( LHCb::span<const std::uint32_t> data ) {
      static_assert( COUNT != 0 && COUNT <= 64, "invalid COUNT parameter" );
      if constexpr ( COUNT == 1 ) {
        return ( data[OFFSET / 32] & ( 1 << OFFSET % 32 ) ) ? true : false;
        // } else if constexpr ( COUNT <= 8 ) { // this version returns strings in Python
        //   static_assert( ( OFFSET % 32 + COUNT ) <= 32 );
        //   const auto mask = static_cast<std::uint32_t>( -1 ) >> ( 32 - COUNT );
        //   return static_cast<std::uint8_t>( ( data[OFFSET / 32] >> OFFSET % 32 ) & mask );
      } else if constexpr ( COUNT <= 16 ) {
        static_assert( ( OFFSET % 32 + COUNT ) <= 32, "invalid COUNT or OFFSET parameter" );
        const auto mask = static_cast<std::uint32_t>( -1 ) >> ( 32 - COUNT );
        return static_cast<std::uint16_t>( ( data[OFFSET / 32] >> OFFSET % 32 ) & mask );
      } else if constexpr ( COUNT < 32 ) {
        static_assert( ( OFFSET % 32 + COUNT ) <= 32, "invalid COUNT or OFFSET parameter" );
        const auto mask = static_cast<std::uint32_t>( -1 ) >> ( 32 - COUNT );
        return static_cast<std::uint32_t>( ( data[OFFSET / 32] >> OFFSET % 32 ) & mask );
      } else if constexpr ( COUNT == 32 ) {
        static_assert( OFFSET % 32 == 0, "invalid COUNT or OFFSET parameter" );
        return data[OFFSET / 32];
      } else if constexpr ( COUNT == 64 ) {
        static_assert( OFFSET % 32 == 0, "invalid COUNT or OFFSET parameter" );
        return static_cast<std::uint64_t>( data[OFFSET / 32] ) |
               ( static_cast<std::uint64_t>( data[OFFSET / 32 + 1] ) << 32 );
      }
    }
    /// Helper to set COUNT bits starting from OFFSET in a buffer using the passed value.
    template <unsigned int COUNT, unsigned int OFFSET, typename VALUE>
    constexpr ACCEL_TARGET_SPEC void set_bits( LHCb::span<std::uint32_t> data, VALUE value ) {
      static_assert( COUNT != 0 && COUNT <= 64, "invalid COUNT parameter" );
      if constexpr ( COUNT == 1 ) {
        if ( value ) {
          data[OFFSET / 32] |= ( 1 << OFFSET % 32 );
        } else {
          data[OFFSET / 32] &= ~( 1 << OFFSET % 32 );
        }
      } else if constexpr ( COUNT < 32 ) {
        static_assert( ( OFFSET % 32 + COUNT ) <= 32, "invalid COUNT or OFFSET parameter" );
        const auto mask   = static_cast<std::uint32_t>( -1 ) >> ( 32 - COUNT );
        data[OFFSET / 32] = ( data[OFFSET / 32] & ~( mask << OFFSET % 32 ) ) | ( ( value & mask ) << OFFSET % 32 );
      } else if constexpr ( COUNT == 32 ) {
        static_assert( OFFSET % 32 == 0, "invalid COUNT or OFFSET parameter" );
        data[OFFSET / 32] = value;
      } else if constexpr ( COUNT == 64 ) {
        static_assert( OFFSET % 32 == 0, "invalid COUNT or OFFSET parameter" );
        data[OFFSET / 32]     = static_cast<std::uint32_t>( value & 0xFFFFFFFF );
        data[OFFSET / 32 + 1] = static_cast<std::uint32_t>( ( value >> 32 ) & 0xFFFFFFFF );
      }
    }
  } // namespace ODINImplementation::details

  namespace
#ifndef ODIN_WITHOUT_GAUDI
      ODINImplementation::v7
#else
      ODINImplementation::v7_standalone
#endif
  {

    struct ODIN final
#ifndef ODIN_WITHOUT_GAUDI
        : DataObject
#endif
    {

      /// Meaning of the EventType bits
      enum class EventTypes : std::uint16_t {
        VeloOpen  = 0x0001,
        et_bit_02 = 0x0002,
        NoBias    = 0x0004,
        Lumi      = 0x0008,
        et_bit_04 = 0x0010,
        et_bit_05 = 0x0020,
        et_bit_06 = 0x0040,
        et_bit_07 = 0x0080,
        et_bit_08 = 0x0100,
        et_bit_09 = 0x0200,
        et_bit_10 = 0x0400,
        et_bit_11 = 0x0800,
        et_bit_12 = 0x1000,
        et_bit_13 = 0x2000,
        et_bit_14 = 0x4000,
        et_bit_15 = 0x8000
      };

      /// Calibration types
      enum class CalibrationTypes : std::uint8_t { //
        A = 0x1,
        B = 0x2,
        C = 0x4,
        D = 0x8
      };

      /// BX Types
      enum class BXTypes : std::uint8_t { //
        NoBeam       = 0,
        Beam1        = 1,
        Beam2        = 2,
        BeamCrossing = 3
      };

      /// Types of trigger broadcasted by ODIN
      enum class TriggerTypes : std::uint8_t { //
        LumiTrigger        = 2,
        CalibrationTrigger = 10
      };

      static constexpr int BANK_VERSION = 7;
      static constexpr int BANK_SIZE    = 40;

      std::array<std::uint32_t, BANK_SIZE / sizeof( std::uint32_t )> data{ 0 };

      inline
#ifdef ODIN_WITHOUT_GAUDI
          constexpr
#endif
          // Note that this cannot be `= default` to support accelerator builds
          ACCEL_TARGET_SPEC
          ODIN() {
#ifndef ODIN_WITHOUT_GAUDI
        setVersion( BANK_VERSION );
#endif
      }

      inline ACCEL_TARGET_SPEC ODIN( LHCb::span<const std::uint32_t> buffer ) {
        assert( buffer.size() == data.size() );
#if defined( __CUDACC__ ) || defined( __HIP__ )
        // FIXME: we assume that if we build for CUDA or HIP we are in Allen, so we can use memcpy()
        //        which will be inherited from either HIPBackend.h or CUDABackend.h via BackendCommon.h
        memcpy( data.data(), buffer.data(), sizeof( data ) );
#else
        std::memcpy( data.data(), buffer.data(), sizeof( data ) );
#endif
#ifndef ODIN_WITHOUT_GAUDI
        setVersion( BANK_VERSION );
#endif
      }

      template <int VERSION = BANK_VERSION, typename = std::enable_if_t<VERSION == BANK_VERSION || VERSION == 6>>
      [[nodiscard]] static ODIN from_version( LHCb::span<const std::uint32_t> buffer );

      template <int VERSION = BANK_VERSION, typename = std::enable_if_t<VERSION == BANK_VERSION || VERSION == 6>>
      void to_version( LHCb::span<std::uint32_t> output_buffer ) const;

#ifndef ODIN_WITHOUT_GAUDI
      // Retrieve pointer to class definition structure
      [[nodiscard]] const CLID&        clID() const override { return this->classID(); }
      [[nodiscard]] static const CLID& classID() { return CLID_ODIN; }
#endif

      enum Fields {
        RunNumberSize                   = 32,
        RunNumberOffset                 = 0,
        EventTypeSize                   = 16,
        EventTypeOffset                 = 1 * 32 + 0,
        CalibrationStepSize             = 16,
        CalibrationStepOffset           = 1 * 32 + 16,
        GpsTimeSize                     = 64,
        GpsTimeOffset                   = 2 * 32,
        TriggerConfigurationKeySize     = 32,
        TriggerConfigurationKeyOffset   = 4 * 32,
        PartitionIDSize                 = 32,
        PartitionIDOffset               = 5 * 32,
        BunchIdSize                     = 12,
        BunchIdOffset                   = 6 * 32 + 0,
        BunchCrossingTypeSize           = 2,
        BunchCrossingTypeOffset         = 6 * 32 + 12,
        NonZeroSuppressionModeSize      = 1,
        NonZeroSuppressionModeOffset    = 6 * 32 + 14,
        TimeAlignmentEventCentralSize   = 1,
        TimeAlignmentEventCentralOffset = 6 * 32 + 15,
        TimeAlignmentEventIndexSize     = 6,
        TimeAlignmentEventIndexOffset   = 6 * 32 + 16,
        StepRunEnableSize               = 1,
        StepRunEnableOffset             = 6 * 32 + 22,
        TriggerTypeSize                 = 4,
        TriggerTypeOffset               = 6 * 32 + 23,
        TimeAlignmentEventFirstSize     = 1,
        TimeAlignmentEventFirstOffset   = 6 * 32 + 27,
        CalibrationTypeSize             = 4,
        CalibrationTypeOffset           = 6 * 32 + 28,
        OrbitNumberSize                 = 32,
        OrbitNumberOffset               = 7 * 32,
        EventNumberSize                 = 64,
        EventNumberOffset               = 8 * 32
      };

      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto runNumber() const {
        return details::get_bits<RunNumberSize, RunNumberOffset>( data );
      }
      constexpr void setRunNumber( std::uint32_t value ) {
        details::set_bits<RunNumberSize, RunNumberOffset>( data, value );
      }

      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto eventType() const {
        return details::get_bits<EventTypeSize, EventTypeOffset>( data );
      }
      [[nodiscard]] constexpr bool eventTypeBit( EventTypes mask ) const {
        assert( std::popcount( static_cast<std::uint16_t>( mask ) ) == 1 );
        return eventType() & static_cast<std::uint16_t>( mask );
      }
      constexpr void setEventType( std::uint16_t value ) {
        details::set_bits<EventTypeSize, EventTypeOffset>( data, value );
      }
      constexpr void setEventTypeBit( EventTypes mask, bool value ) {
        assert( std::popcount( static_cast<std::uint16_t>( mask ) ) == 1 );
        std::uint16_t newEventType = eventType();
        if ( value ) {
          newEventType |= static_cast<std::uint16_t>( mask );
        } else {
          newEventType &= ~static_cast<std::uint16_t>( mask );
        }
        setEventType( newEventType );
      }
      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto calibrationStep() const {
        return details::get_bits<CalibrationStepSize, CalibrationStepOffset>( data );
      }
      constexpr void setCalibrationStep( std::uint16_t value ) {
        details::set_bits<CalibrationStepSize, CalibrationStepOffset>( data, value );
      }

      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto gpsTime() const {
        return details::get_bits<GpsTimeSize, GpsTimeOffset>( data );
      }
      constexpr void setGpsTime( std::uint64_t value ) { details::set_bits<GpsTimeSize, GpsTimeOffset>( data, value ); }

      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto triggerConfigurationKey() const {
        return details::get_bits<TriggerConfigurationKeySize, TriggerConfigurationKeyOffset>( data );
      }
      constexpr void setTriggerConfigurationKey( std::uint32_t value ) {
        details::set_bits<TriggerConfigurationKeySize, TriggerConfigurationKeyOffset>( data, value );
      }

      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto partitionID() const {
        return details::get_bits<PartitionIDSize, PartitionIDOffset>( data );
      }
      constexpr void setPartitionID( std::uint32_t value ) {
        details::set_bits<PartitionIDSize, PartitionIDOffset>( data, value );
      }

      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto bunchId() const {
        return details::get_bits<BunchIdSize, BunchIdOffset>( data );
      }
      constexpr void setBunchId( std::uint16_t value ) { details::set_bits<BunchIdSize, BunchIdOffset>( data, value ); }
      [[nodiscard]] constexpr ACCEL_TARGET_SPEC BXTypes bunchCrossingType() const {
        return static_cast<BXTypes>( details::get_bits<BunchCrossingTypeSize, BunchCrossingTypeOffset>( data ) );
      }
      constexpr void setBunchCrossingType( BXTypes value ) {
        details::set_bits<BunchCrossingTypeSize, BunchCrossingTypeOffset>( data, static_cast<std::uint8_t>( value ) );
      }
      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto nonZeroSuppressionMode() const {
        return details::get_bits<NonZeroSuppressionModeSize, NonZeroSuppressionModeOffset>( data );
      }
      constexpr void setNonZeroSuppressionMode( bool value ) {
        details::set_bits<NonZeroSuppressionModeSize, NonZeroSuppressionModeOffset>( data, value );
      }
      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto timeAlignmentEventCentral() const {
        return details::get_bits<TimeAlignmentEventCentralSize, TimeAlignmentEventCentralOffset>( data );
      }
      constexpr void setTimeAlignmentEventCentral( bool value ) {
        details::set_bits<TimeAlignmentEventCentralSize, TimeAlignmentEventCentralOffset>( data, value );
      }
      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto timeAlignmentEventIndex() const {
        return details::get_bits<TimeAlignmentEventIndexSize, TimeAlignmentEventIndexOffset>( data );
      }
      constexpr void setTimeAlignmentEventIndex( std::uint8_t value ) {
        details::set_bits<TimeAlignmentEventIndexSize, TimeAlignmentEventIndexOffset>( data, value );
      }
      [[nodiscard]] constexpr ACCEL_TARGET_SPEC bool isTAE() const { return timeAlignmentEventIndex() != 0; }
      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto stepRunEnable() const {
        return details::get_bits<StepRunEnableSize, StepRunEnableOffset>( data );
      }
      constexpr void setStepRunEnable( bool value ) {
        details::set_bits<StepRunEnableSize, StepRunEnableOffset>( data, value );
      }
      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto triggerType() const {
        return details::get_bits<TriggerTypeSize, TriggerTypeOffset>( data );
      }
      constexpr void setTriggerType( std::uint8_t value ) {
        details::set_bits<TriggerTypeSize, TriggerTypeOffset>( data, value );
      }
      constexpr void setTriggerType( TriggerTypes value ) { setTriggerType( static_cast<std::uint8_t>( value ) ); }
      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto timeAlignmentEventFirst() const {
        return details::get_bits<TimeAlignmentEventFirstSize, TimeAlignmentEventFirstOffset>( data );
      }
      constexpr void setTimeAlignmentEventFirst( bool value ) {
        details::set_bits<TimeAlignmentEventFirstSize, TimeAlignmentEventFirstOffset>( data, value );
      }
      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto calibrationType() const {
        return details::get_bits<CalibrationTypeSize, CalibrationTypeOffset>( data );
      }
      [[nodiscard]] constexpr bool calibrationTypeBit( CalibrationTypes mask ) const {
        assert( std::popcount( static_cast<std::uint8_t>( mask ) ) == 1 );
        return calibrationType() & static_cast<std::uint8_t>( mask );
      }
      constexpr void setCalibrationType( std::uint8_t value ) {
        details::set_bits<CalibrationTypeSize, CalibrationTypeOffset>( data, value );
      }
      constexpr void setCalibrationTypeBit( CalibrationTypes mask, bool value ) {
        assert( std::popcount( static_cast<std::uint8_t>( mask ) ) == 1 );
        std::uint8_t newCalibrationType = calibrationType();
        if ( value ) {
          newCalibrationType |= static_cast<std::uint8_t>( mask );
        } else {
          newCalibrationType &= ~static_cast<std::uint8_t>( mask );
        }
        setCalibrationType( newCalibrationType );
      }

      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto orbitNumber() const {
        return details::get_bits<OrbitNumberSize, OrbitNumberOffset>( data );
      }
      constexpr void setOrbitNumber( std::uint32_t value ) {
        details::set_bits<OrbitNumberSize, OrbitNumberOffset>( data, value );
      }

      [[nodiscard]] constexpr ACCEL_TARGET_SPEC auto eventNumber() const {
        return details::get_bits<EventNumberSize, EventNumberOffset>( data );
      }
      constexpr void setEventNumber( std::uint64_t value ) {
        details::set_bits<EventNumberSize, EventNumberOffset>( data, value );
      }

#ifndef ODIN_WITHOUT_GAUDI
      [[nodiscard]] Gaudi::Time eventTime() const {
        const auto gps = gpsTime();
        return ( gps == 0xD0DD0D0000000000ULL ? 0 : gps * 1000 );
      }
      void setEventTime( const Gaudi::Time& time ) { setGpsTime( time.ns() / 1000 ); }
#endif
    };

    template <>
    [[nodiscard]] inline ODIN ODIN::from_version<ODIN::BANK_VERSION>( LHCb::span<const std::uint32_t> buffer ) {
      return buffer;
    }
    template <>
    inline void ODIN::to_version<ODIN::BANK_VERSION>( LHCb::span<std::uint32_t> buffer ) const {
      assert( buffer.size() == data.size() );
      std::memcpy( buffer.data(), data.data(), sizeof( data ) );
    }

    std::ostream& operator<<( std::ostream& s, ODIN::CalibrationTypes e );
    std::ostream& operator<<( std::ostream& s, ODIN::BXTypes e );
    std::ostream& operator<<( std::ostream& s, ODIN::TriggerTypes e );
  } // namespace ODINImplementation::v7

#ifndef ODIN_WITHOUT_GAUDI
  using ODIN = ODINImplementation::v7::ODIN;
#else
  using ODIN = ODINImplementation::v7_standalone::ODIN;
#endif
} // namespace LHCb
