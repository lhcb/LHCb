/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/RawBank.h"
#include "GaudiKernel/DataObject.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/STLExtensions.h"
#include <map>
#include <optional>
#include <string>
#include <type_traits>
#include <vector>
namespace LHCb {

  // Class ID definition
  constexpr CLID CLID_RawEvent = 1002;

  // Namespace for locations in TES
  namespace RawEventLocation {
    inline const std::string Default  = "DAQ/RawEvent"; ///< Original FULL Raw Event
    inline const std::string Emulated = "Emu/RawEvent";
    inline const std::string Copied   = "pRec/RawEvent";
    inline const std::string Calo     = "Calo/RawEvent";    ///< Copy of Calo banks
    inline const std::string Muon     = "Muon/RawEvent";    ///< Copy of Muon banks, for MDST
    inline const std::string Rich     = "Rich/RawEvent";    ///< Copy of Rich banks
    inline const std::string Trigger  = "Trigger/RawEvent"; ///< Copy of Trigger banks for Stripping and MDST
    inline const std::string PersistReco =
        "PersistReco/RawEvent"; ///< Copy of PersistReco banks for MC Stripping and MDST
    inline const std::string Other =
        "Other/RawEvent"; ///< Copy of all banks except Calo, Muon, Rich and Trigger (now Obsolete)
    inline const std::string Velo    = "Velo/RawEvent";    ///< Copy of Velo banks
    inline const std::string Tracker = "Tracker/RawEvent"; ///< Copy of IT, OT and TT banks
    inline const std::string HC      = "HC/RawEvent";      ///< Copy of Herschel banks
    inline const std::string Unstripped =
        "Unstripped/RawEvent"; ///< Miscellaneous banks not required by stripping lines.
    inline const std::string VeloCluster = "VeloCluster/RawEvent"; ///< Copy of Velo Cluster banks
    inline const std::string VeloSPmixed = "VeloSPmixed/RawEvent"; ///< Copy of Velo SPmixed banks
    inline const std::string VeloError   = "VeloError/RawEvent";   ///< Copy of Velo Error banks
  }                                                                // namespace RawEventLocation

  /** @class LHCb::RawEvent RawEvent.h
   *
   * Raw event
   *
   * @author Helder Lopes
   * @author Markus Frank
   * created Tue Oct 04 14:45:29 2005
   *
   */

  class RawEvent : public DataObject {
  public:
    /** @class LHCb::RawEvent::Bank RawEvent.h Event/RawEvent.h
     *
     * Shadow class used to deal with persistency.
     * This class is entirely internal. Do not change.
     * In particular the field comments are hints to ROOT
     * to support the storage of variable size C-arrays in order
     * to avoid a copy of the data.
     *
     * Banks can be removed using the removeBank(RawBank*) member
     * function. The bank to be removed must be identified by its
     * pointer to ensure unambiguous bank identification also in the
     * event where multiple banks if the same bank type are present.
     * If no other bank of the category of the bank (Banktype)to
     * be removed is anymore present in the raw event, also the
     * category is removed.
     *
     * Note:
     * - The length passed to the RawEvent::createBank should NOT
     *   contain the size of the header !
     *
     * @author  M.Frank
     * @version 1.0
     */
    struct Bank final {
      int                 m_len  = 0;       // Bank length
      char                m_owns = 1;       //! transient data member: ownership flag
      const unsigned int* m_buff = nullptr; //[m_len]
      /// Default constructor
      Bank() = default;
      /// Initializing constructor
      Bank( int len, char owns, const unsigned int* b ) : m_len( len ), m_owns( owns ), m_buff( b ) {}
      /// Copy constructor
      [[deprecated( "copy ctor should not exist..." )]] Bank( const Bank& ) = default;
      Bank( Bank&& rhs ) {
        m_len  = std::exchange( rhs.m_len, 0 );
        m_owns = std::exchange( rhs.m_owns, 0 );
        m_buff = std::exchange( rhs.m_buff, nullptr );
      }
      /// Move Assignment operator
      Bank& operator=( Bank&& rhs ) {
        m_len  = std::exchange( rhs.m_len, 0 );
        m_owns = std::exchange( rhs.m_owns, 0 );
        m_buff = std::exchange( rhs.m_buff, nullptr );
        return *this;
      }
      /// Access to memory buffer
      [[nodiscard]] const unsigned int* buffer() const { return m_buff; }
      /// Access to ownership flag.
      [[nodiscard]] bool ownsMemory() const { return m_owns == 1; }
    };

    using allocator_type = Allocators::EventLocal<Bank>;

    class MapView final {
      /** @class LHCb::RawEvent::MapView
       *
       * Access a 'span' of RawBanks of a given type
       *
       * Note; this structure is a 'view' i.e. it explicitly relies
       * on some underlying RawEvent instance which must have a lifetime
       * which extends beyond the lifetime the corresponding view instance.
       *
       * @author Gerhard Raven
       * created 2021/02/12
       *
       */
      using allocator_t = std::allocator_traits<RawEvent::allocator_type>::rebind_alloc<const RawBank*>;
      std::vector<const RawBank*, allocator_t>              m_banks;
      std::array<uint16_t, RawBank::BankType::LastType + 1> m_indices;

    public:
      MapView( RawEvent const& );

      /// raison d'etre for this class:
      /// accessor method to the Rawbank*s for a given bank type
      [[nodiscard]] RawBank::View banks( RawBank::BankType bankType ) const noexcept {
        assert( static_cast<size_t>( bankType + 1 ) < m_indices.size() );
        const auto f = m_indices[bankType];
        const auto l = m_indices[bankType + 1];
        return make_span( m_banks ).subspan( f, l - f );
      }
    }; // class MapView

    friend class MapView;

    /// Default Constructor
    RawEvent( allocator_type alloc = {} ) : m_banks{ alloc } {}

    /// Default Destructor
    ~RawEvent() override;

    /// Move constructor
    RawEvent( RawEvent&& ) = default;

    // Move assignemnt
    RawEvent& operator=( RawEvent&& ) = default;

    /// Retrieve class identifier (static)
    static const CLID& classID() { return CLID_RawEvent; }
    /// Retrieve class identifier (virtual overload)
    const CLID& clID() const override { return RawEvent::classID(); }

    /// accessor method to the vector of Raw banks for a given bank type
    [[nodiscard]] RawBank::View banks( RawBank::BankType bankType ) const {
      // The optimizer should be able to deal with this...
      if ( !m_map ) m_map.emplace( *this );
      return m_map->banks( bankType );
    }

    /// allows to reserve space for future banks
    void reserve( unsigned int n ) { m_banks.reserve( n ); }

    /// returns size of the RawEvent, aka number of banks it contains
    [[nodiscard]] unsigned int size() const { return m_banks.size(); }

    /// returns the allocator of the bank vector
    [[nodiscard]] allocator_type get_allocator() const { return m_banks.get_allocator(); }

    /// For offline use only: copy data into a set of banks, adding bank header internally.
    void addBank( int sourceID, RawBank::BankType bankType, int version, span<const std::byte> data ) {
      adoptBank( createBank( sourceID, bankType, version, data ), true );
    }

    template <typename ValueType, typename = std::enable_if_t<!std::is_convertible_v<ValueType, std::byte>>>
    void addBank( int sourceID, RawBank::BankType bankType, int version, span<ValueType> data ) {
      addBank( sourceID, bankType, version, as_bytes( data ) );
    }

    template <typename ValueType>
    void addBank( int sourceID, RawBank::BankType bankType, int version, const std::vector<ValueType>& data ) {
      addBank( sourceID, bankType, version, make_span( data ) );
    }

    template <typename ValueType, auto N>
    void addBank( int sourceID, RawBank::BankType bankType, int version, const std::array<ValueType, N>& data ) {
      addBank( sourceID, bankType, version, make_span( data ) );
    }

    /// For offline use only: copy data into a bank, adding bank header internally.
    void addBank( const RawBank* data ); // Pointer to data block (payload) of bank

    /// Take ownership of a bank, including the header
    void adoptBank( const RawBank* bank, // Pointer to beginning of bank (i.e. bank header)
                    bool           adopt_memory ); // Flag to adopt memory

    /// Remove bank identified by its pointer
    /** Remove raw data bank from bankset and update bank map.
     * The bank removal can fail if the specified bank was not found.
     *
     *  @param bank          [IN]      Pointer to raw bank structure to be removed.
     *
     *  @return Boolean value indicating success (=true) or failure(=false)
     */
    bool removeBank( const RawBank* bank );

    /// Rawbank creator
    /** Create raw bank and fill values
     *  @param srcID          [IN]     Source identifier
     *  @param typ            [IN]     Bank type (from RawBank::BankType enum)
     *  @param vsn            [IN]     Bank version
     *  @param len            [IN]     Length of data segment in bytes
     *  @param data           [IN]     Data buffer (if NULL, no data are copied)
     *
     *  @return Initialized Pointer to RawBank structure
     */
    static RawBank* createBank( int srcID, RawBank::BankType typ, int vsn, size_t len, const void* data = nullptr );

    template <typename ValueType>
    static RawBank* createBank( int srcID, RawBank::BankType typ, int vsn, span<ValueType> data ) {
      return createBank( srcID, typ, vsn, data.size_bytes(), data.data() );
    }

    /// Access the full length 32 bit aligned length of a bank in bytes
    /** Access full bank length
     * @param  len           [IN]     Raw unaligned bank length
     *
     * @return padded bank size in bytes
     */
    static size_t paddedBankLength( size_t len );

  private:
    std::vector<Bank, allocator_type> m_banks; // Vector with persistent bank structure
    mutable std::optional<MapView>    m_map;   //! transient
  };                                           // class RawEvent
} // namespace LHCb
