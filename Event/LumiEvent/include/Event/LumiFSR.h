/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/VectorMap.h"
#include <ostream>
#include <vector>

namespace LHCb {

  // Class ID definition
  static const CLID CLID_LumiFSR = 13501;

  // Namespace for locations in TDS
  namespace LumiFSRLocation {
    inline const std::string Default = "/FileRecords/LumiFSR";
  }

  /** @class LumiFSR LumiFSR.h
   *
   * Accounting class for Lumi in FSR
   *
   * @author Jaap Panman
   *
   */

  class LumiFSR final : public KeyedObject<int> {
  public:
    /// typedef for std::vector of LumiFSR
    typedef std::vector<LumiFSR*>       Vector;
    typedef std::vector<const LumiFSR*> ConstVector;

    /// typedef for KeyedContainer of LumiFSR
    typedef KeyedContainer<LumiFSR, Containers::HashMap> Container;

    /// For User information
    typedef std::pair<int, long long> ValuePair;
    /// User information
    typedef GaudiUtils::VectorMap<int, ValuePair> ExtraInfo;
    /// Set of runNumbers
    typedef std::vector<unsigned int> RunNumbers;
    /// Set of file IDs
    typedef std::vector<std::string> FileIDs;

    /// Default Constructor
    LumiFSR() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::LumiFSR::classID(); }
    static const CLID& classID() { return CLID_LumiFSR; }

    /// Assignment operator
    LumiFSR& operator=( const LHCb::LumiFSR& rhs );

    /// add a RunNumber to the set
    LumiFSR& addRunNumber( unsigned int number ) {
      m_runNumbers.push_back( number );
      return *this;
    }

    /// has information for specified runNumber
    [[nodiscard]] bool hasRunNumber( unsigned int number ) const {
      return m_runNumbers.end() != find( m_runNumbers.begin(), m_runNumbers.end(), number );
    }

    /// Addition operator
    const RunNumbers& mergeRuns( const RunNumbers& rhs );

    /// add a fileID to the set
    LumiFSR& addFileID( std::string& idString ) {
      m_fileIDs.push_back( idString );
      return *this;
    }

    /// has information for specified fileID
    [[nodiscard]] bool hasFileID( const std::string& idString ) const {
      return m_fileIDs.end() != find( m_fileIDs.begin(), m_fileIDs.end(), idString );
    }

    /// Addition operator
    const FileIDs& mergeFileIDs( const FileIDs& rhs );

    /// ExtraInformation. Don't use directly, use hasInfo, info, addInfo...
    [[nodiscard]] const ExtraInfo& extraInfo() const { return m_extraInfo; }

    /// has information for specified key
    [[nodiscard]] bool hasInfo( int key ) const { return m_extraInfo.end() != m_extraInfo.find( key ); }

    ///  Add new information associated with the specified key. This method cannot be used
    ///  to modify information for a pre-existing key.
    bool addInfo( const int key, const int incr, const long long count ) {
      return m_extraInfo.insert( key, { incr, count } ).second;
    }

    /// extract the information associated with the given key. If there is no such
    /// information the default value will be returned.
    [[nodiscard]] ValuePair info( const int key, const ValuePair& def ) const {
      auto i = m_extraInfo.find( key );
      return m_extraInfo.end() == i ? def : i->second;
    }

    /// erase the information associated with the given key
    int eraseInfo( int key ) { return m_extraInfo.erase( key ); }

    /// sums the values for existing keys and inserts a new key if needed
    ExtraInfo const& mergeInfo( const ExtraInfo& rhs );

    /// increments the values for existing keys and inserts a new key if needed
    ExtraInfo const& incrementInfo( const int key, const long long count );

    /// Addition operator
    LumiFSR& operator+=( const LHCb::LumiFSR& rhs ) {
      if ( this != &rhs ) {
        mergeRuns( rhs.m_runNumbers );
        mergeFileIDs( rhs.m_fileIDs );
        mergeInfo( rhs.m_extraInfo );
      }
      return *this;
    }

    /// intelligent printout
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  Set of run numbers summed up in this job
    [[nodiscard]] const RunNumbers& runNumbers() const { return m_runNumbers; }

    /// Update  Set of run numbers summed up in this job
    LumiFSR& setRunNumbers( const RunNumbers& value ) {
      m_runNumbers = value;
      return *this;
    }

    /// Retrieve const  Set of file IDs summed up in this job
    [[nodiscard]] const FileIDs& fileIDs() const { return m_fileIDs; }

    /// Update  Set of file IDs summed up in this job
    LumiFSR& setFileIDs( const FileIDs& value ) {
      m_fileIDs = value;
      return *this;
    }

    /// Update  Some additional user information. Don't use directly. Use *Info() methods.
    LumiFSR& setExtraInfo( const ExtraInfo& value ) {
      m_extraInfo = value;
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const LumiFSR& obj ) { return obj.fillStream( str ); }

  private:
    RunNumbers m_runNumbers; ///< Set of run numbers summed up in this job
    FileIDs    m_fileIDs;    ///< Set of file IDs summed up in this job
    ExtraInfo  m_extraInfo;  ///< Some additional user information. Don't use directly. Use
                             ///< *Info() methods.

  }; // class LumiFSR

  /// Definition of Keyed Container for LumiFSR
  typedef KeyedContainer<LumiFSR, Containers::HashMap> LumiFSRs;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

inline LHCb::LumiFSR& LHCb::LumiFSR::operator=( const LHCb::LumiFSR& rhs ) {
  if ( this != &rhs ) {
    // FIXME: this implements _addition_, not _assignment_!
    mergeRuns( rhs.m_runNumbers );
    mergeFileIDs( rhs.m_fileIDs );
    mergeInfo( rhs.m_extraInfo );
  }
  return *this;
}

inline const LHCb::LumiFSR::RunNumbers& LHCb::LumiFSR::mergeRuns( const RunNumbers& rhs ) {
  // concatenate run number set (cannot use "set" - have to do it by hand!)
  for ( auto rn : rhs ) {
    if ( !hasRunNumber( rn ) ) { m_runNumbers.push_back( rn ); }
  }
  return m_runNumbers;
}

inline const LHCb::LumiFSR::FileIDs& LHCb::LumiFSR::mergeFileIDs( const FileIDs& rhs ) {
  // concatenate fileID set (cannot use "set" - have to do it by hand!)
  for ( const auto& id : rhs ) {
    if ( !hasFileID( id ) ) { m_fileIDs.push_back( id ); }
  }
  return m_fileIDs;
}

inline LHCb::LumiFSR::ExtraInfo const& LHCb::LumiFSR::mergeInfo( const ExtraInfo& rhs ) {
  // sum info or add new key
  for ( auto const& [key, valueB] : rhs ) {
    ValuePair valueA = info( key, ValuePair( 0, 0 ) );
    if ( hasInfo( key ) ) { eraseInfo( key ); }
    addInfo( key, valueA.first + valueB.first, valueA.second + valueB.second );
  }
  return m_extraInfo;
}

inline LHCb::LumiFSR::ExtraInfo const& LHCb::LumiFSR::incrementInfo( const int key, const long long count ) {
  // increment info or add new key, only if data exists
  if ( count != -1 ) {
    ValuePair value = info( key, ValuePair( 0, 0 ) );
    if ( hasInfo( key ) ) { eraseInfo( key ); }
    addInfo( key, ++value.first, value.second + count );
  }
  return m_extraInfo;
}

inline std::ostream& LHCb::LumiFSR::fillStream( std::ostream& s ) const {
  s << "{  runs : ";
  for ( auto const& run : m_runNumbers ) s << run << " ";
  s << " files : ";
  for ( auto const& file : m_fileIDs ) s << file << " ";
  // overall sum info
  s << " info (key/incr/integral) : ";
  for ( const auto& [key, value] : m_extraInfo ) { s << key << " " << value.first << " " << value.second << " / "; }
  return s << " }";
}
