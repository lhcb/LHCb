/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Detector/VP/VPChannelID.h"
#include "Kernel/VPConstants.h"

#include <cstdint>
#include <ostream>
#include <vector>

// This compile guard is present for gcc12 + lcg 104/105
// support for ROOT.
// See https://gitlab.cern.ch/lhcb/LHCb/-/issues/342
#ifndef __CLING__
#  include "Kernel/MultiIndexedContainer.h"
#endif

/**
 * VPMicroClusters are the bare minimal representation of the information
 * needed to build a 'VPLightCluster', the type of cluster needed to fit
 * tracks. These clusters priarily exists for reasons of persistency.
 *
 * @author Laurent Dufour <laruent.dufour@cern.ch>
 **/
namespace LHCb {

  // Namespace for locations in TDS
  namespace VPClusterLocation {
    inline const std::string Micro = "Raw/VP/MicroClusters";
  }

  class VPMicroCluster final {

  public:
    using FracType = std::uint8_t;

  public:
    /// Default Constructor, used by packing/unpacking
    VPMicroCluster() = default;

    /// Constructor
    VPMicroCluster( const FracType xfraction, const FracType yfraction, const Detector::VPChannelID vpID )
        : m_fx( xfraction ), m_fy( yfraction ), m_vpID( vpID ) {}

    /// Return the cluster channelID
    [[nodiscard]] auto channelID() const noexcept { return m_vpID; }

    /// Retrieve const  inter-pixel fraction
    [[nodiscard]] auto xfraction() const noexcept { return m_fx; }
    [[nodiscard]] auto yfraction() const noexcept { return m_fy; }

    /// Print the cluster information
    std::ostream& fillStream( std::ostream& s ) const {
      s << "{VPCluster channelID: "
        << channelID()
        // Note, cast these from 8 to 16 bit types as otherwise they do not
        // appear as numerical values in the output.
        << ", fx: " << static_cast<std::uint16_t>( xfraction() ) //
        << ", fy: " << static_cast<std::uint16_t>( yfraction() ) << "}";
      return s;
    }

    // setters
    void setXfraction( const FracType fx ) noexcept { m_fx = fx; }
    void setYfraction( const FracType fy ) noexcept { m_fy = fy; }
    void setChannelID( const Detector::VPChannelID channelID ) noexcept { m_vpID = channelID; }

    bool operator==( const VPMicroCluster& other ) const noexcept {
      return ( xfraction() == other.xfraction() && //
               yfraction() == other.yfraction() && //
               channelID() == other.channelID() );
    }

  private:
    FracType              m_fx{ 0 }; ///< inter-pixel fraction in x coordinate
    FracType              m_fy{ 0 }; ///< inter-pixel fraction in y coordinate
    Detector::VPChannelID m_vpID;    ///< channelID of cluster

  public:
    /** Ordering required by the IndexedHitContainer (UTHitClusters) **/
    struct Order {
      bool operator()( const VPMicroCluster& lhs, const VPMicroCluster& rhs ) const {
        // primary sort done on the indexing parameter (module)
        if ( lhs.channelID().module() != rhs.channelID().module() )
          return lhs.channelID().module() < rhs.channelID().module();

        return lhs.channelID() < rhs.channelID();
      }
    };
  }; // class VPMicroCluster

  using VPMicroClusterVector = std::vector<VPMicroCluster>;

#ifndef __CLING__
  /// indexed container of clusters
  using VPMicroClusters = Container::MultiIndexedContainer<VPMicroCluster, VP::NModules>;

  inline std::optional<VPMicroCluster> findCluster( const Detector::VPChannelID channelID,
                                                    const VPMicroClusters&      clusters ) {
    const auto moduleNumber = channelID.module();
    for ( const auto& cluster : clusters.range( moduleNumber ) ) {
      if ( cluster.channelID() == channelID ) { return cluster; }
    }
    return std::nullopt;
  }

  inline auto sortClusterContainer( VPMicroClusters& clusters ) { return clusters.forceSort(); }

#endif

  inline std::ostream& operator<<( std::ostream& str, const VPMicroCluster& obj ) { return obj.fillStream( str ); }
} // namespace LHCb
