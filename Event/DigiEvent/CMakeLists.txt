###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/DigiEvent
---------------
#]=======================================================================]

gaudi_add_library(DigiEvent
    SOURCES
        src/PlumeAdc.cpp
        src/UTDigit.cpp
        src/UTHitCluster.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::LHCbKernel
            LHCb::EventBase
            LHCb::LHCbMathLib
            LHCb::UTDetLib
)

gaudi_add_dictionary(DigiEventDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK
        LHCb::DigiEvent
)

gaudi_add_executable(test_calodigits
    SOURCES
        tests/src/test_calodigits_v2.cpp
    LINK
        Boost::unit_test_framework
        Gaudi::GaudiKernel
        LHCb::DigiEvent
    TEST
)
