###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/HltEvent
--------------
#]=======================================================================]

gaudi_add_library(HltEvent
    SOURCES
        src/HltDecReport.cpp
        src/HltDecReports.cpp
        src/HltObjectSummary.cpp
        src/HltSelReports.cpp
        src/HltVertexReports.cpp
    LINK
        PUBLIC
            Boost::headers
            Gaudi::GaudiKernel
            LHCb::LHCbKernel
            LHCb::RecEvent
)

gaudi_add_dictionary(HltEventDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK LHCb::HltEvent
)
