/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/HltDecReport.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/VectorMap.h"
#include <algorithm>
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_HltDecReports = 7521;

  // Namespace for locations in TDS
  namespace HltDecReportsLocation {
    inline const std::string Default     = "Hlt/DecReports";
    inline const std::string Hlt1Default = "Hlt1/DecReports";
    inline const std::string Hlt2Default = "Hlt2/DecReports";
    inline const std::string Emulated    = "Hlt/Emu/DecReports";
  } // namespace HltDecReportsLocation

  /** @class HltDecReports HltDecReports.h
   *
   * managed container of Hlt Trigger Decision Reports
   *
   * @author Tomasz Skwarnicki
   *
   */

  class HltDecReports : public DataObject {
  public:
    /// container of HltDecReport-s keyed by trigger decision name
    typedef GaudiUtils::VectorMap<std::string, LHCb::HltDecReport> Container;
    /// const iterator over of HltDecReport
    typedef Container::const_iterator const_iterator;
    /// const reference to contained object
    typedef Container::const_reference const_reference;
    /// type of contained object
    typedef Container::value_type value_type;

    /// Default Constructor
    HltDecReports() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::HltDecReports::classID(); }
    static const CLID& classID() { return CLID_HltDecReports; }

    /// return pointer to Hlt Decision Report for given trigger decision name (==0 if not found)
    const HltDecReport* decReport( const std::string& decisionName ) const {
      auto rep = m_decReports.find( decisionName );
      return rep != m_decReports.end() ? &( rep->second ) : nullptr;
    }

    /// check if the trigger decision name is present in the container
    bool hasDecisionName( const std::string& decisionName ) const {
      return m_decReports.find( decisionName ) != m_decReports.end();
    }

    /// return names of the decisions stored in the container
    std::vector<std::string> decisionNames() const {
      std::vector<std::string> names;
      names.reserve( m_decReports.size() );
      std::transform( begin(), end(), std::back_inserter( names ), []( const auto& i ) { return i.first; } );
      return names;
    }

    /// begin iterator
    Container::const_iterator begin() const { return m_decReports.begin(); }

    /// end iterator
    Container::const_iterator end() const { return m_decReports.end(); }

    /// find HltDecReport in the container given its trigger decision name
    Container::const_iterator find( const std::string& decisionName ) const {
      return m_decReports.find( decisionName );
    }

    /// size of the HltDecReports container (i.e. number of decision reports stored)
    Container::size_type size() const { return m_decReports.size(); }

    /// reserve space for (at least) num elements
    void reserve( const Container::size_type& num ) { m_decReports.reserve( num ); }

    /// insert HltDecReport for given decisionName, returns StatusCode::FAILURE if duplicate decision name
    StatusCode insert( const std::string& decisionName, const LHCb::HltDecReport& decReport ) {
      return m_decReports.insert( decisionName, decReport ).second ? StatusCode::SUCCESS : StatusCode::FAILURE;
    }

    /// insert HltDecReport for given decisionName
    std::pair<Container::const_iterator, bool> insert( const Container::value_type& value ) {
      return m_decReports.insert( value );
    }

    /// insert HltDecReport for given decisionName
    std::pair<Container::const_iterator, bool> insert( const Container::const_iterator& hint,
                                                       const Container::value_type&     value ) {
      return m_decReports.insert( hint, value );
    }

    /// insert HltDecReport for given decisionName
    std::pair<Container::const_iterator, bool> insert( const Container::const_iterator& hint,
                                                       const Container::key_type&       key,
                                                       const Container::mapped_type&    mapped ) {
      return m_decReports.insert( hint, key, mapped );
    }

    /// intelligent printout
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  Trigger Configuration Key used for Configuration
    [[nodiscard]] unsigned int configuredTCK() const { return m_configuredTCK; }

    /// Update  Trigger Configuration Key used for Configuration
    HltDecReports& setConfiguredTCK( unsigned int value ) {
      m_configuredTCK = value;
      return *this;
    }

    /// Retrieve const  Reserved for online Task / Node ID
    [[nodiscard]] unsigned int taskID() const { return m_taskID; }

    /// Update  Reserved for online Task / Node ID
    HltDecReports& setTaskID( unsigned int value ) {
      m_taskID = value;
      return *this;
    }

    /// Retrieve const  container of HltDecReport-s keyed by trigger decision name
    const Container& decReports() const { return m_decReports; }

    /// Update  container of HltDecReport-s keyed by trigger decision name
    HltDecReports setDecReports( const LHCb::HltDecReports::Container& value ) {
      m_decReports = value;
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const HltDecReports& obj ) { return obj.fillStream( str ); }

  private:
    unsigned int m_configuredTCK = 0;  ///< Trigger Configuration Key used for Configuration
    unsigned int m_taskID        = 0;  ///< Reserved for online Task / Node ID
    Container    m_decReports    = {}; ///< container of HltDecReport-s keyed by trigger decision name

  }; // class HltDecReports

} // namespace LHCb
