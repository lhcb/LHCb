/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "DetDesc/DetectorElement.h"
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloDigitStatus.h"
#include "Event/CaloDigits_v2.h"
#include "Event/CaloPosition.h"
#include "Event/SIMDEventTypes.h"
#include "GaudiKernel/ClassID.h"
#include "GaudiKernel/Point3DTypes.h"
#include "Kernel/EventLocalUnique.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/meta_enum.h"
#include <cstdint>
#include <utility>
#include <vector>

namespace LHCb::Event::Calo {
  using DigitStatus = LHCb::Calo::DigitStatus::Status;

  constexpr CLID CLID_ClustersV2 = 2023;

  inline namespace v2 {

    // Namespace for locations in TDS
    namespace ClusterLocation {
      inline const std::string Default      = "Rec/Calo/Clusters";
      inline const std::string EcalRaw      = "Rec/Calo/EcalClustersRaw";
      inline const std::string EcalOverlap  = "Rec/Calo/EcalClustersOverlap";
      inline const std::string Ecal         = "Rec/Calo/EcalClusters";
      inline const std::string Hcal         = "Rec/Calo/HcalClusters";
      inline const std::string EcalSplit    = "Rec/Calo/EcalSplitClusters";
      inline const std::string DefaultHlt   = "Hlt/Calo/Clusters";
      inline const std::string EcalHlt      = "Hlt/Calo/EcalClusters";
      inline const std::string HcalHlt      = "Hlt/Calo/HcalClusters";
      inline const std::string EcalSplitHlt = "Hlt/Calo/EcalSplitClusters";
      inline const std::string EcalHlt1     = "Hlt1/Calo/EcalClusters";
    } // namespace ClusterLocation

    namespace ClusterEntryTag {
      struct Id : Event::int_field {}; // TODO: make CellID field
      struct Energy : Event::float_field {};
      struct Fraction : Event::float_field {};
      struct Status : Event::enum_field<DigitStatus> {};
      struct Entry : Event::struct_field<Id, Energy, Fraction, Status> {
        template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
                  typename Path>
        struct EntryProxy : StructProxy<Simd, Behaviour, ContainerTypeBase, Path> {

          using base_t = StructProxy<Simd, Behaviour, ContainerTypeBase, Path>;
          using base_t::base_t;

          [[nodiscard, gnu::always_inline]] auto cellID() const {
            if constexpr ( Simd == SIMDWrapper::InstructionSet::Scalar ) {
              return LHCb::Detector::Calo::CellID{ static_cast<unsigned>(
                  this->template get<Id>().cast() ) }; // hack to ease transition to soacollections
            } else {
              return this->template get<Id>();
            }
          }
          [[nodiscard, gnu::always_inline]] auto energy() const {
            if constexpr ( Simd == SIMDWrapper::InstructionSet::Scalar ) {
              return this->template get<Energy>().cast(); // hack to ease transition to soacollections
            } else {
              return this->template get<Energy>();
            }
          }
          [[nodiscard, gnu::always_inline]] auto fraction() const {
            if constexpr ( Simd == SIMDWrapper::InstructionSet::Scalar ) {
              return this->template get<Fraction>().cast(); // hack to ease transition to soacollections
            } else {
              return this->template get<Fraction>();
            }
          }
          [[nodiscard, gnu::always_inline]] auto status() const {
            if constexpr ( Simd == SIMDWrapper::InstructionSet::Scalar ) {
              return this->template get<Status>().cast(); // hack to ease transition to soacollections
            } else {
              return this->template get<Status>();
            }
          }

          // modifiers
          auto& setCellID( LHCb::Detector::Calo::CellID cellID ) {
            this->template field<Id>().set( cellID.all() );
            return *this;
          }
          auto& setEnergy( float e ) {
            this->template field<Energy>().set( e );
            return *this;
          }
          auto& setFraction( float f ) {
            this->template field<Fraction>().set( f );
            return *this;
          }
          auto& setStatus( DigitStatus st ) {
            this->template field<Status>().set( st );
            return *this;
          }
          auto& addStatus( DigitStatus s ) {
            setStatus( status().set( s ) );
            return *this;
          }
          auto& removeStatus( DigitStatus s ) {
            setStatus( status().reset( s ) );
            return *this;
          }
          auto& resetStatus() {
            setStatus( CaloDigitStatus::Mask::Undefined );
            return *this;
          }
        };

        template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
                  typename Path>
        using proxy_type = EntryProxy<Simd, Behaviour, ContainerType, Path>;
      };
    } // namespace ClusterEntryTag

    enum class ClusterType : int { Undefined = 0, Invalid, CellularAutomaton, Area3x3, Area2x2 };

    namespace ClusterTag {
      struct Seed : Event::int_field {}; // TODO: make CellID field
      struct Entries : Event::vector_field<ClusterEntryTag::Entry> {};
      struct Energy : Event::float_field {};
      struct Position : Event::Vec_field<3> {};
      struct Type : Event::enum_field<ClusterType> {};

      /// 3x3 Covariance matrix (E,X,Y)
      struct Covariance : Event::MatSym_field<3> {};
      /// 2x2 spread matrix (X,Y)
      struct Spread : Event::MatSym_field<2> {};
      /// center of gravity
      struct Center : Event::Vec_field<2> {};

      template <typename T>
      using Clusters_t = Event::SOACollection<T, Seed, Entries, Energy, Position, Type, Covariance, Spread, Center>;
    } // namespace ClusterTag

    struct Clusters : ClusterTag::Clusters_t<Clusters> {
      using base_t = typename ClusterTag::Clusters_t<Clusters>;
      using Type   = ClusterType;

      using base_t::allocator_type;
      using base_t::base_t;

      const CLID&        clID() const { return Clusters::classID(); }
      static const CLID& classID() { return CLID_ClustersV2; }

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      struct ClusterProxy : Event::Proxy<simd, behaviour, ContainerType> {
        using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
        using base_t::loop_mask;
        using base_t::Proxy;
        using base_t::width;

        [[gnu::always_inline]] auto operator->() const { return this; }
        [[gnu::always_inline]] auto operator->() { return this; }

        // observers, and, in case if T is non-const, implicitly(!) setters
        [[nodiscard, gnu::always_inline]] auto energy() const {
          if constexpr ( simd == SIMDWrapper::InstructionSet::Scalar ) {
            return this->template get<ClusterTag::Energy>().cast(); // hack to ease transition to soacollections
          } else {
            return this->template get<ClusterTag::Energy>();
          }
        }
        [[nodiscard, gnu::always_inline]] auto e() const { return energy(); }
        [[nodiscard, gnu::always_inline]] auto position() const {
          if constexpr ( simd == SIMDWrapper::InstructionSet::Scalar ) {
            auto in = this->template get<ClusterTag::Position>();
            return Gaudi::XYZPointF{ in.x().cast(), in.y().cast(), in.z().cast() };
          } else {
            return this->template get<ClusterTag::Position>();
          }
        }
        [[nodiscard, gnu::always_inline]] auto covariance() const {
          if constexpr ( simd == SIMDWrapper::InstructionSet::Scalar ) {
            auto                in = this->template get<ClusterTag::Covariance>();
            Gaudi::SymMatrix3x3 out;
            for ( int i = 0; i < 3; i++ ) {
              for ( int j = 0; j <= i; j++ ) { out( i, j ) = in( i, j ).cast(); }
            }
            return out;
          } else {
            return this->template get<ClusterTag::Covariance>();
          }
        }
        [[nodiscard, gnu::always_inline]] auto spread() const {
          if constexpr ( simd == SIMDWrapper::InstructionSet::Scalar ) {
            auto                in = this->template get<ClusterTag::Spread>();
            Gaudi::SymMatrix2x2 out;
            for ( int i = 0; i < 2; i++ ) {
              for ( int j = 0; j <= i; j++ ) { out( i, j ) = in( i, j ).cast(); }
            }
            return out;
          } else {
            return this->template get<ClusterTag::Spread>();
          }
        }
        [[nodiscard, gnu::always_inline]] auto center() const {
          if constexpr ( simd == SIMDWrapper::InstructionSet::Scalar ) {
            auto in = this->template get<ClusterTag::Center>();
            return Gaudi::Vector2{ in.x().cast(), in.y().cast() };
          } else {
            return this->template get<ClusterTag::Center>();
          }
        }

        [[gnu::always_inline]] void setEnergy( float e ) {
          static_assert( simd == SIMDWrapper::InstructionSet::Scalar, "setters only for scalar" );
          this->template field<ClusterTag::Energy>().set( e );
        }

        [[gnu::always_inline]] void setPosition( Gaudi::XYZPointF pos ) {
          static_assert( simd == SIMDWrapper::InstructionSet::Scalar, "setters only for scalar" );
          this->template field<ClusterTag::Position>().set(
              LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{ pos.x(), pos.y(), pos.z() } );
        }

        [[gnu::always_inline]] void setCovariance( LHCb::LinAlg::MatSym<SIMDWrapper::scalar::float_v, 3> covariance ) {
          static_assert( simd == SIMDWrapper::InstructionSet::Scalar, "setters only for scalar" );
          this->template field<ClusterTag::Covariance>().set( covariance );
        }

        [[gnu::always_inline]] void setSpread( Gaudi::SymMatrix2x2 spread ) {
          static_assert( simd == SIMDWrapper::InstructionSet::Scalar, "setters only for scalar" );
          LHCb::LinAlg::MatSym<SIMDWrapper::scalar::float_v, 2> cov;
          for ( int i = 0; i < 2; i++ ) {
            for ( int j = 0; j <= i; j++ ) { cov( i, j ) = spread( i, j ); }
          }
          this->template field<ClusterTag::Spread>().set( cov );
        }

        [[gnu::always_inline]] void setCenter( Gaudi::Vector2 pos ) {
          static_assert( simd == SIMDWrapper::InstructionSet::Scalar, "setters only for scalar" );
          this->template field<ClusterTag::Center>().set(
              LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 2>{ pos[0], pos[1] } );
        }

        [[gnu::always_inline]] void setType( Type t ) {
          static_assert( simd == SIMDWrapper::InstructionSet::Scalar, "setters only for scalar" );
          this->template field<ClusterTag::Type>().set( t );
        }

        [[gnu::always_inline]] void setCellID( LHCb::Detector::Calo::CellID cellID ) {
          static_assert( simd == SIMDWrapper::InstructionSet::Scalar, "setters only for scalar" );
          this->template field<ClusterTag::Seed>().set( cellID.all() );
        }

        // the following really cannot be modified...
        [[nodiscard, gnu::always_inline]] auto size() const {
          if constexpr ( simd == SIMDWrapper::InstructionSet::Scalar ) {
            return this->template field<ClusterTag::Entries>().size().cast(); // hack to ease transition to
                                                                              // soacollections
          } else {
            return this->template field<ClusterTag::Entries>().size();
          }
        }
        /// the type of cluster
        [[nodiscard, gnu::always_inline]] auto type() const {
          if constexpr ( simd == SIMDWrapper::InstructionSet::Scalar ) {
            return this->template get<ClusterTag::Type>().cast(); // hack to ease transition to soacollections
          } else {
            return this->template get<ClusterTag::Type>();
          }
        }
        /// The cellID of the seed digit for the cluster
        [[nodiscard, gnu::always_inline]] auto cellID() const {
          if constexpr ( simd == SIMDWrapper::InstructionSet::Scalar ) { // hack to ease transition to soacollections
            return LHCb::Detector::Calo::CellID{
                static_cast<unsigned>( this->template get<ClusterTag::Seed>().cast() ) };
          } else {
            return this->template get<ClusterTag::Seed>();
          }
        }
        /// The cluster contents itself - entries : digits with their status
        [[nodiscard, gnu::always_inline]] auto entries() const { return this->template field<ClusterTag::Entries>(); }
      };

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      using proxy_type = ClusterProxy<simd, behaviour, ContainerType>;

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
      using reference = LHCb::Event::ZipProxy<ClusterProxy<simd, behaviour, Clusters>>;
      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
      using const_reference = const LHCb::Event::ZipProxy<ClusterProxy<simd, behaviour, const Clusters>>;

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
      using Entries = ClusterTag::Entries::proxy_type<simd, behaviour, Clusters, SOAPath<ClusterTag::Entries>>;
      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
      using const_Entries =
          ClusterTag::Entries::proxy_type<simd, behaviour, const Clusters, SOAPath<ClusterTag::Entries>>;
      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
      using value_type    = ClusterProxy<simd, behaviour, Clusters>;
      using pointer       = void;
      using const_pointer = void;

      template <typename T>
      class IndexView {
        T*                                                      m_parent = nullptr;
        std::array<int16_t, LHCb::Detector::Calo::Index::max()> m_index;

      public:
        explicit IndexView( T* parent ) : m_parent{ parent } {
          if ( !m_parent ) return;
          m_index.fill( -1 );
          for ( auto cluster : m_parent->scalar() ) {
            auto& idx = m_index[LHCb::Detector::Calo::Index{ cluster.cellID() }];
            if ( idx != -1 ) {
              // oops -- not unique, so flag, and just give up...
              m_parent = nullptr;
              return;
            }
            idx = cluster.offset();
          }
        }
        [[nodiscard]] explicit operator bool() const { return m_parent != nullptr; }
        [[nodiscard]] auto find( LHCb::Detector::Calo::CellID id ) const {
          if ( !*this ) throw;
          if ( !LHCb::Detector::Calo::isValid( id ) ) return m_parent->scalar().end();
          auto i = m_index[LHCb::Detector::Calo::Index{ id }];
          return i < 0 ? m_parent->scalar().end() : ( m_parent->scalar().begin() + i );
        }
        [[nodiscard]] auto begin() const { return m_parent->scalar().begin(); }
        [[nodiscard]] auto end() const { return m_parent->scalar().end(); }
      };

      [[nodiscard]] auto index() const { return IndexView{ this }; }
    };
  } // namespace v2
} // namespace LHCb::Event::Calo
