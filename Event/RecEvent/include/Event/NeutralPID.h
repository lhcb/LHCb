/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/Track.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/SmartRef.h"
#include <cstdint>
#include <ostream>
#include <vector>

namespace LHCb {

  // Class ID definition TODO
  static const CLID CLID_NeutralPID = 12252;

  // Namespace for locations in TDS
  namespace NeutralPIDLocation {
    inline const std::string Default = "";
  } // namespace NeutralPIDLocation

  /** @class NeutralPID NeutralPID.h
   *
   * Stores the neutral variables from Calo info
   *
   */

  class NeutralPID final : public KeyedObject<int> {

  public:
    using CellID = LHCb::Detector::Calo::CellID;

  public:
    using Vector      = std::vector<NeutralPID*>;
    using ConstVector = std::vector<const NeutralPID*>;

    typedef KeyedContainer<NeutralPID, Containers::HashMap> Container;

    using Selection = SharedObjectsContainer<NeutralPID>;
    using Range     = Gaudi::Range_<ConstVector>;

    /// Copy constructor. Creates a new NeutralPID object with the same pid information
    NeutralPID( const LHCb::NeutralPID& lhs )
        : KeyedObject<int>()
        , m_CaloNeutralID( lhs.m_CaloNeutralID )
        , m_ClusterMass( lhs.m_ClusterMass )
        , m_CaloNeutralEcal( lhs.m_CaloNeutralEcal )
        , m_CaloTrMatch( lhs.m_CaloTrMatch )
        , m_ShowerShape( lhs.m_ShowerShape )
        , m_CaloNeutralHcal2Ecal( lhs.m_CaloNeutralHcal2Ecal )
        , m_CaloNeutralE19( lhs.m_CaloNeutralE19 )
        , m_CaloNeutralE49( lhs.m_CaloNeutralE49 )
        , m_CaloClusterCode( lhs.m_CaloClusterCode )
        , m_CaloClusterFrac( lhs.m_CaloClusterFrac )
        , m_Saturation( lhs.m_Saturation )
        , m_IsNotH( lhs.m_IsNotH )
        , m_IsPhoton( lhs.m_IsPhoton ) {}

    /// Default Constructor
    NeutralPID() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::NeutralPID::classID(); }
    static const CLID& classID() { return CLID_NeutralPID; }

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    auto CaloNeutralID() const noexcept { return m_CaloNeutralID; }

    void setCaloNeutralID( CellID value ) noexcept { m_CaloNeutralID = value; }

    auto ClusterMass() const noexcept { return m_ClusterMass; }

    void setClusterMass( float value ) noexcept { m_ClusterMass = value; }

    auto CaloNeutralEcal() const noexcept { return m_CaloNeutralEcal; }

    void setCaloNeutralEcal( float value ) noexcept { m_CaloNeutralEcal = value; }

    auto CaloTrMatch() const noexcept { return m_CaloTrMatch; }

    void setCaloTrMatch( float value ) noexcept { m_CaloTrMatch = value; }

    auto ShowerShape() const noexcept { return m_ShowerShape; }

    void setShowerShape( float value ) noexcept { m_ShowerShape = value; }

    auto CaloNeutralHcal2Ecal() const noexcept { return m_CaloNeutralHcal2Ecal; }

    void setCaloNeutralHcal2Ecal( float value ) noexcept { m_CaloNeutralHcal2Ecal = value; }

    auto CaloNeutralE19() const noexcept { return m_CaloNeutralE19; }

    void setCaloNeutralE19( float value ) noexcept { m_CaloNeutralE19 = value; }

    auto CaloNeutralE49() const noexcept { return m_CaloNeutralE49; }

    void setCaloNeutralE49( float value ) noexcept { m_CaloNeutralE49 = value; }

    auto CaloClusterCode() const noexcept { return m_CaloClusterCode; }

    void setCaloClusterCode( int value ) noexcept { m_CaloClusterCode = value; }

    auto CaloClusterFrac() const noexcept { return m_CaloClusterFrac; }

    void setCaloClusterFrac( float value ) noexcept { m_CaloClusterFrac = value; }

    auto Saturation() const noexcept { return m_Saturation; }

    void setSaturation( int value ) noexcept { m_Saturation = value; }

    auto IsNotH() const noexcept { return m_IsNotH; }

    void setIsNotH( float value ) noexcept { m_IsNotH = value; }

    auto IsPhoton() const noexcept { return m_IsPhoton; }

    void setIsPhoton( float value ) noexcept { m_IsPhoton = value; }

    friend std::ostream& operator<<( std::ostream& str, const NeutralPID& obj ) { return obj.fillStream( str ); }

  private:
    CellID m_CaloNeutralID{};
    float  m_ClusterMass{ -1000. };
    float  m_CaloNeutralEcal{ -999.0 };
    float  m_CaloTrMatch{ +1.e+06 };
    float  m_ShowerShape{ -999.0 };
    float  m_CaloNeutralHcal2Ecal{ -999.0 };
    float  m_CaloNeutralE19{ -999.0 };
    float  m_CaloNeutralE49{ -999.0 };
    int    m_CaloClusterCode{ -999 };
    float  m_CaloClusterFrac{ 1 };
    int    m_Saturation{ -1000 };
    float  m_IsNotH{ -1 };
    float  m_IsPhoton{ +1 };

  }; // class NeutralPID

  /// Definition of Keyed Container for NeutralPID
  using NeutralPIDs = NeutralPID::Container;

} // namespace LHCb

inline std::ostream& LHCb::NeutralPID::fillStream( std::ostream& s ) const {
  return s << "{"
           << " CaloNeutralID :	" << m_CaloNeutralID                //
           << " ClusterMass :	" << m_ClusterMass                  //
           << " CaloNeutralEcal :	" << m_CaloNeutralEcal      //
           << " CaloTrMatch :	" << m_CaloTrMatch                  //
           << " ShowerShape :	" << m_ShowerShape                  //
           << " CaloNeutralHcal2Ecal :	" << m_CaloNeutralHcal2Ecal //
           << " CaloNeutralE19 :	" << m_CaloNeutralE19       //
           << " CaloNeutralE49 :	" << m_CaloNeutralE49       //
           << " CaloClusterCode :	" << m_CaloClusterCode      //
           << " CaloClusterFrac :	" << m_CaloClusterFrac      //
           << " Saturation :	" << m_Saturation                   //
           << " IsNotH :	" << m_IsNotH                       //
           << " IsPhoton :	" << m_IsPhoton                     //
           << " }";
}
