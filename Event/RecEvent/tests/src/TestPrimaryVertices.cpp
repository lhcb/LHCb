/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#undef NDEBUG
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestPrimaryVertices

#include <boost/test/unit_test.hpp>

#include "Event/PrimaryVertices.h"

using namespace LHCb::Event::PV;

BOOST_AUTO_TEST_CASE( CanCopyAndAssignPV ) {
  // create a single PV and test move, copy, assignment of key
  ExtendedPrimaryVertex pv{ Gaudi::XYZPoint( 0, 0, 1 ) };
  pv.setRange( 5, 10 );

  // test copy constructor
  {
    ExtendedPrimaryVertex pvcopy{ pv };
    BOOST_CHECK_EQUAL( pvcopy.position(), pv.position() );
    BOOST_CHECK_EQUAL( pvcopy.end(), pv.end() );
  }

  // test assignment operator
  {
    ExtendedPrimaryVertex pvcopy{};
    pvcopy = pv;
    BOOST_CHECK_EQUAL( pvcopy.position(), pv.position() );
    BOOST_CHECK_EQUAL( pvcopy.end(), pv.end() );
  }

  // test move constructor
  {
    ExtendedPrimaryVertex tmppv{ pv };
    ExtendedPrimaryVertex pvcopy{ std::move( tmppv ) };
    BOOST_CHECK_EQUAL( pvcopy.position(), pv.position() );
    BOOST_CHECK_EQUAL( pvcopy.end(), pv.end() );
  }

  // test move assignment
  {
    ExtendedPrimaryVertex tmppv{ pv };
    ExtendedPrimaryVertex pvcopy{ Gaudi::XYZPoint( 0, 0, 1 ) };
    pvcopy = std::move( tmppv );
    BOOST_CHECK_EQUAL( pvcopy.position(), pv.position() );
    BOOST_CHECK_EQUAL( pvcopy.end(), pv.end() );
  }
}

template <typename Vertices>
bool comparePVContainers( const Vertices& lhs, const Vertices& rhs ) {
  using namespace std;
  auto lhspvtracks = lhs.tracks.scalar();
  auto rhspvtracks = rhs.tracks.scalar();
  return std::equal( begin( lhs.vertices ), end( lhs.vertices ), begin( rhs.vertices ), end( rhs.vertices ),
                     []( const auto& w1, const auto& w2 ) {
                       return w1.key() == w2.key() && w1.position() == w2.position();
                     } ) &&
         std::equal( begin( lhspvtracks ), end( lhspvtracks ), begin( rhspvtracks ), end( rhspvtracks ),
                     []( const auto& w1, const auto& w2 ) { return w1.x().cast() == w2.x().cast(); } ) &&
         lhs.velotrackmap == rhs.velotrackmap;
}

BOOST_AUTO_TEST_CASE( CanCopyPVContainer ) {

  // create a PV container with two different PVs
  PrimaryVertexContainer pvcontainer;
  pvcontainer.vertices.push_back( ExtendedPrimaryVertex{ Gaudi::XYZPoint( 0, 0, 0 ) } );
  pvcontainer.vertices.push_back( ExtendedPrimaryVertex{ Gaudi::XYZPoint( 0, 0, 1 ) } );
  pvcontainer.setIndices();

  // TODO: add some tracks with non-zero x position
  pvcontainer.tracks.resize( 10 );
  auto pvtracks_scalar = pvcontainer.tracks.scalar();
  for ( size_t i = 0; i < pvcontainer.tracks.size(); ++i )
    pvtracks_scalar[i].field<PVTrackTag::x>().set( float( i * 0.1 ) );
  pvcontainer.velotrackmap.resize( 10 );
  std::iota( pvcontainer.velotrackmap.begin(), pvcontainer.velotrackmap.end(), 0 );

  BOOST_CHECK_EQUAL( pvcontainer.vertices.front().key(), 0 );
  BOOST_CHECK_EQUAL( pvcontainer.vertices.back().key(), pvcontainer.vertices.size() - 1 );

  // Test copy constructor
  {
    auto pvcontainercopy{ pvcontainer };
    BOOST_CHECK( pvcontainer.checkEqual( pvcontainercopy ) );
  }
  // Test move constructor
  {
    std::cout << "Testing move constructor" << std::endl;
    auto tmppvs = new PrimaryVertexContainer{ pvcontainer };
    std::cout << "Now calling the move constructor" << std::endl;
    auto pvcontainercopy = new PrimaryVertexContainer{ std::move( *tmppvs ) };
    std::cout << "Now calling the check" << std::endl;
    BOOST_CHECK( pvcontainer.checkEqual( *pvcontainercopy ) );

    std::cout << "Deleting the copied container: " << std::endl;
    delete pvcontainercopy;
    std::cout << "Deleting the moved container: " << std::endl;
    delete tmppvs;
  }
}
