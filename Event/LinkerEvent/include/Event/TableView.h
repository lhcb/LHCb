/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/FunctionalDetails.h"

#include <stdexcept>
#include <string>

#include <type_traits>
#include <vector>

namespace LHCb::Phys::RelationTables {

  template <typename Column, typename Label>
  class TableView {
    Gaudi::Functional::details::vector_of_const_<Column> const* m_cols;
    LHCb::span<Label const>                                     m_labels;
    struct Cursor;

  public:
    class Row {
      friend Cursor;
      Gaudi::Functional::details::vector_of_const_<Column> const* cols;
      LHCb::span<Label const>                                     labels;
      size_t                                                      row;

    public:
      Row( Gaudi::Functional::details::vector_of_const_<Column> const* c, LHCb::span<Label const> l, size_t r )
          : cols{ c }, labels{ l }, row{ r } {}
      template <typename T>
      auto operator()( T const& label ) const {
        auto ic = std::find( labels.begin(), labels.end(), label );
        if ( ic == labels.end() ) throw std::out_of_range{ "Unknown label." };
        return ( *cols )[ic - labels.begin()].relations()[row].to();
      }
      auto from() const {
        auto const& tbl = cols->front().relations();
        auto const& rr  = std::next( tbl.begin(), row );
        auto        frm = rr->from();
        assert( std::all_of( cols->begin(), cols->end(), [frm, this]( auto const& c ) {
          return std::next( c.relations().begin(), row )->from() == frm;
        } ) );
        return frm;
      }
    };

  private:
    struct Sentinel {};
    struct Cursor {
      Row   current;
      bool  operator!=( Sentinel ) const { return current.row != current.cols->front().relations().size(); }
      auto& operator++() {
        ++current.row;
        return *this;
      }
      Row const& operator*() const { return current; }
    };

  public:
    TableView( Gaudi::Functional::details::vector_of_const_<Column>&&, LHCb::span<Label const> ) = delete;
    TableView( Gaudi::Functional::details::vector_of_const_<Column> const& cols, LHCb::span<Label const> labels )
        : m_cols{ &cols }, m_labels{ labels } {
      if ( m_cols->size() == 0 ) { throw std::invalid_argument( "Input vector of relation tables empty." ); }
      if ( m_cols->size() != m_labels.size() ) {
        throw std::invalid_argument( "Number of columns and column names don't match." );
      }
      auto sz = m_cols->front().size();
      if ( std::all_of( m_cols->begin(), m_cols->end(), [sz]( const auto& c ) { return c.size() != sz; } ) ) {
        throw std::invalid_argument( "Not all relation tables have same length." );
      }
    }
    Cursor   begin() { return { { m_cols, m_labels, 0 } }; }
    Sentinel end() { return {}; }
  };

  template <typename Column, typename Label>
  TableView( Gaudi::Functional::details::vector_of_const_<Column> const&, std::vector<Label> const& )
      -> TableView<Column, Label>;

} // namespace LHCb::Phys::RelationTables
