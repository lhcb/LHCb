/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/Particle.h"
#include <algorithm>
#include <boost/container/small_vector.hpp>
#include <cmath>
#include <iterator>
#include <optional>

#ifdef __cpp_lib_ranges
#  include <ranges>

namespace LHCb::Utils::Particle::details {
  using std::ranges::any_of;
  using std::views::filter;
  template <typename I, typename S>
  constexpr auto subrange( I&& begin, S&& end ) {
    return std::ranges::subrange{ std::forward<I>( begin ), std::forward<S>( end ) };
  }
} // namespace LHCb::Utils::Particle::details

#else

namespace LHCb::Utils::Particle::details {

  template <typename I, typename S>
  class subrange {
    I b;
    S e;

  public:
    subrange( I b, S e ) : b{ std::move( b ) }, e{ std::move( e ) } {}
    auto begin() const { return b; }
    auto end() const { return e; }
  };

  template <typename Iterator, typename Sentinel, typename Pred>
  class filter {
    class FilteredIterator {
      Iterator current;
      Sentinel end;
      Pred     pred;
      void     findValid() {
        while ( current != end && !pred( *current ) ) ++current;
      }

    public:
      using difference_type [[maybe_unused]]   = typename Iterator::difference_type;
      using value_type [[maybe_unused]]        = typename Iterator::value_type;
      using reference                          = typename Iterator::reference;
      using iterator_category [[maybe_unused]] = std::input_iterator_tag;

      FilteredIterator( Iterator begin, Sentinel end, Pred pred )
          : current{ std::move( begin ) }, end{ std::move( end ) }, pred{ std::move( pred ) } {
        findValid();
      }
      bool              operator!=( Sentinel s ) const { return current != s; }
      FilteredIterator& operator++() {
        ++current;
        findValid();
        return *this;
      }
      reference operator*() const { return *current; }
    };
    FilteredIterator b;
    Sentinel         e;

  public:
    filter( Iterator i, Sentinel e, Pred p ) : b{ std::move( i ), e, std::move( p ) }, e{ e } {}
    template <typename Range>
    filter( Range r, Pred p ) : filter{ r.begin(), r.end(), std::move( p ) } {}
    auto begin() const { return b; }
    auto end() const { return e; }
  };
  template <typename Range, typename Pred>
  filter( Range r, Pred ) -> filter<decltype( r.begin() ), decltype( r.end() ), Pred>;

  template <typename Range, typename Predicate>
  bool any_of( Range&& r, Predicate const& p ) {
    for ( auto const& i : r )
      if ( p( i ) ) return true;
    return false;
  }
} // namespace LHCb::Utils::Particle::details

#endif

namespace LHCb::Utils::Particle {

  inline auto walk( const LHCb::Particle& p ) {
    struct Sentinel {};
    class Iterator {
      boost::container::small_vector<
          std::pair<LHCb::Particle const*, std::optional<SmartRefVector<LHCb::Particle>::const_iterator>>, 8>
          m_stack;

    public:
      using difference_type [[maybe_unused]]   = ptrdiff_t;
      using value_type [[maybe_unused]]        = LHCb::Particle const*;
      using reference                          = value_type const&;
      using iterator_category [[maybe_unused]] = std::input_iterator_tag;

      Iterator( const LHCb::Particle* p ) {
        if ( !p ) return;
        m_stack.emplace_back( p, std::nullopt );
      }

      bool operator==( Sentinel ) const { return m_stack.empty(); }
#ifndef __cpp_impl_three_way_comparison
      bool operator!=( Sentinel s ) const { return !( *this == s ); }
#endif

      Iterator& operator++() {
        while ( true ) {
          if ( m_stack.empty() ) return *this;
          auto& [p, d] = m_stack.back();
          if ( !d ) {
            d = p->daughters().begin(); // first daughter
          } else if ( *d != p->daughters().end() ) {
            ++*d; // next daughter
          }
          if ( *d == p->daughters().end() ) { // no more daughters
            m_stack.pop_back();               // done with this particle, go back to parent
          } else {
            m_stack.emplace_back( **d, std::nullopt );
            return *this;
          }
        }
      }
      Iterator operator++( int ) {
        Iterator r = *this;
        ++*this;
        return r;
      }

      reference operator*() const {
        assert( !m_stack.empty() );
        return m_stack.back().first;
      }
    };

    return details::subrange( Iterator{ &p }, Sentinel{} );
  }

  inline auto getBasics( const LHCb::Particle& p ) {
    return details::filter( walk( p ), []( LHCb::Particle const* i ) { return i->isBasicParticle() && i->proto(); } );
  }

  using details::any_of;

} // namespace LHCb::Utils::Particle
