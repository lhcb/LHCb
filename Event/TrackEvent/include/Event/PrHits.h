/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "MuonDAQ/MuonHitContainer.h"
#include "PrSciFiHits.h"
#include "PrUTHits.h"
#include "PrVeloHits.h"

#include <tuple>

namespace LHCb::Pr {

  /**
   * @brief Defines basic HitTypes that specialise the `Hits` wrapper around hit containers
   */
  enum struct HitType {
    VP,
    UT,
    FT,
    Muon,
  };

  template <HitType T>
  struct Hits;

  /**
   * @brief Specialisations of Hits according to HitType
   *
   * @tparam HitType
   *
   *  * @details These template specialisations of `Hits` give a uniform-looking interface to the
   * SOA hit classes (mainly) used by pattern recognition algorithms. This extra layer makes it
   * a bit easier to get the right hit container because the caller has to only know which HitType
   * is desired without worrying about finding the right container/handler (e.g. there has been some
   * confusion about whether to use PrUTHitHandler, UTHitHandler or UT::Hits, same for FTHitHandler
   * and FT::Hits) which should be a detail. The default answer now is LHCb::Pr::Hits from PrHits.h.
   * However, for the time being, this barely hides the fact that the underlying hit containers have
   * very different interfaces. In particular the SciFi hit container does not use (TODO:) a
   * SOACollection. Once this is implemented the main difference between the containers
   * will be the HitTags reflecting the different underyling detector designs.
   */
  template <>
  struct Hits<HitType::VP> : VP::details::Hits {
    // inherit constructors
    using VP::details::Hits::Hits;
  };

  template <>
  struct Hits<HitType::UT> : UT::details::Hits {
    // inherit constructors
    using UT::details::Hits::Hits;
  };
  template <>
  struct Hits<HitType::FT> : FT::details::Hits {
    // inherit constructors
    using FT::details::Hits::Hits;
  };
  template <>
  struct Hits<HitType::Muon> : LHCb::Muon::DAQ::details::MuonHitContainer {
    // inherit constructors
    using LHCb::Muon::DAQ::details::MuonHitContainer::MuonHitContainer;
  };

  // simply a convenient wrapper around std::get
  template <HitType Type, HitType... Types>
  constexpr auto& get( const std::tuple<const Hits<Types>&...>& c ) {
    return std::get<const Hits<Type>&>( c );
  }

  // check HitType euqality via templates, useful in variadic expression
  template <HitType Type, HitType Type2>
  constexpr auto is_equal = ( Type == Type2 );

  // aliases for backward compatibility
  namespace UT {
    using Hits = LHCb::Pr::Hits<LHCb::Pr::HitType::UT>;
  }

  namespace VP {
    using Hits = LHCb::Pr::Hits<LHCb::Pr::HitType::VP>;
  }

  namespace FT {
    using Hits = LHCb::Pr::Hits<LHCb::Pr::HitType::FT>;
  }

} // namespace LHCb::Pr

using MuonHitContainer = LHCb::Pr::Hits<LHCb::Pr::HitType::Muon>;
