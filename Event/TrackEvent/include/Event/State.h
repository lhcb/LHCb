/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/StateVector.h"
#include "Event/TrackEnums.h"
#include "Event/TrackTypes.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/VectorMap.h"
#include "Kernel/meta_enum.h"
#include <algorithm>
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_State = 10011;

  // Namespace for locations in TDS
  namespace StateLocation {
    inline const std::string Default = "Rec/Track/States";
  }

  /** @class State State.h
   *
   * State is the base class of offline and online track states.
   *
   * @author Jose Hernando, Eduardo Rodrigues
   *
   */

  class State final {
  public:
    /// typedef for std::vector of State
    using Vector      = std::vector<State*>;
    using ConstVector = std::vector<const State*>;
    using Location    = LHCb::Event::Enum::State::Location;

    /// typedef for KeyedContainer of State
    typedef KeyedContainer<State, Containers::HashMap> Container;

    /// Default constructor
    State() { setLocation( State::Location::LocationUnknown ); }

    /// Constructor with arguments
    State( const Gaudi::TrackVector& stateVec, const Gaudi::TrackSymMatrix& cov, double z,
           const LHCb::State::Location& location );

    /// Constructor from a StateVector
    State( const LHCb::StateVector& stateVec ) : m_stateVector( stateVec.parameters() ), m_z( stateVec.z() ) {}

    /// Constructor without matrix
    State( const Gaudi::TrackVector& stateVec, double z, const LHCb::State::Location& location );

    /// Constructor with location
    State( const LHCb::State::Location& location ) { setLocation( location ); }

    // Retrieve pointer to class definition structure
    [[nodiscard]] const CLID& clID() const { return LHCb::State::classID(); }
    static const CLID&        classID() { return CLID_State; }

    /// conversion of string to enum for type Location
    [[deprecated]] static LHCb::State::Location LocationToType( const std::string& aName );

    /// conversion to string for enum type Location
    [[deprecated]] static std::string LocationToString( int aEnum ) {
      return toString( LHCb::State::Location( aEnum ) );
    }

    /// Retrieve the number of state parameters
    [[nodiscard]] unsigned int nParameters() const { return (unsigned int)m_stateVector.Dim(); }

    /// Retrieve the position and momentum vectors and the corresponding 6D covariance matrix (pos:0->2,mom:3-5) of the
    /// state
    void positionAndMomentum( Gaudi::XYZPoint& pos, Gaudi::XYZVector& mom, Gaudi::SymMatrix6x6& cov6D ) const {
      pos   = position();
      mom   = momentum();
      cov6D = posMomCovariance();
    }

    /// Retrieve the position and momentum vectors of the state
    void positionAndMomentum( Gaudi::XYZPoint& pos, Gaudi::XYZVector& mom ) const {
      pos = position();
      mom = momentum();
    }

    /// Retrieve the 3D-position vector (x,y,z) of the state
    [[nodiscard]] Gaudi::XYZPoint position() const {
      return Gaudi::XYZPoint( m_stateVector[0], m_stateVector[1], m_z );
    }

    /// Retrieve the x-position of the state
    [[nodiscard]] double x() const { return m_stateVector[0]; };

    /// Retrieve the y-position of the state
    [[nodiscard]] double y() const { return m_stateVector[1]; };

    /// Retrieve the z-position of the state
    [[nodiscard]] double z() const { return m_z; };

    /// Retrieve the slopes (Tx=dx/dz,Ty=dy/dz,1.) of the state
    [[nodiscard]] Gaudi::XYZVector slopes() const { return Gaudi::XYZVector( m_stateVector[2], m_stateVector[3], 1. ); }

    /// Retrieve the Tx=dx/dz slope of the state
    [[nodiscard]] double tx() const { return m_stateVector[2]; }

    /// Retrieve the Ty=dy/dz slope of the state
    [[nodiscard]] double ty() const { return m_stateVector[3]; };

    /// Retrieve the charge-over-momentum Q/P of the state
    [[nodiscard]] double qOverP() const { return m_stateVector[4]; };

    /// Retrieve the momentum of the state
    [[nodiscard]] double p() const;

    /// Retrieve the transverse momentum of the state
    [[nodiscard]] double pt() const;

    /// Retrieve the momentum vector (px,py,pz) of the state
    [[nodiscard]] Gaudi::XYZVector momentum() const {
      Gaudi::XYZVector mom = slopes();
      return mom *= ( p() / mom.R() );
    }

    /// Retrieve the Q/Pperp (ratio of the charge to the component of the momentum transverse to the magnetic field) of
    /// the state
    [[nodiscard]] double qOverPperp() const {
      const double tx2 = tx() * tx();
      return ( qOverP() * sqrt( ( 1. + tx2 + ty() * ty() ) / ( 1. + tx2 ) ) );
    }

    /// Retrieve the 6D (x,y,z,px,py,pz) covariance matrix of the state
    [[nodiscard]] Gaudi::SymMatrix6x6 posMomCovariance() const;

    /// Retrieve the errors on the 3D-position vector (x,y,z) of the state
    [[nodiscard]] Gaudi::SymMatrix3x3 errPosition() const {
      return posMomCovariance().Sub<Gaudi::SymMatrix3x3>( 0, 0 );
    };

    /// Retrieve the squared error on the x-position of the state
    [[nodiscard]] double errX2() const { return m_covariance( 0, 0 ); }

    /// Retrieve the squared error on the y-position of the state
    [[nodiscard]] double errY2() const { return m_covariance( 1, 1 ); };

    /// Retrieve the errors on the slopes (Tx=dx/dz,Ty=dy/dz,1.) of the state
    [[nodiscard]] Gaudi::SymMatrix3x3 errSlopes() const;

    /// Retrieve the squared error on the x-slope Tx=dx/dz of the state
    [[nodiscard]] double errTx2() const { return m_covariance( 2, 2 ); }

    /// Retrieve the squared error on the y-slope Ty=dy/dz of the state
    [[nodiscard]] double errTy2() const { return m_covariance( 3, 3 ); }

    /// Retrieve the squared error on the charge-over-momentum Q/P of the state
    [[nodiscard]] double errQOverP2() const { return m_covariance( 4, 4 ); }

    /// Retrieve the squared error on the momentum (px,py,pz) of the state
    [[nodiscard]] double errP2() const;

    /// Retrieve the errors on the momentum vector (px,py,pz) of the state
    [[nodiscard]] Gaudi::SymMatrix3x3 errMomentum() const {
      return posMomCovariance().Sub<Gaudi::SymMatrix3x3>( 3, 3 );
    };

    /// Retrieve the squared error on the Q/Pperp of the state
    [[nodiscard]] double errQOverPperp2() const;

    /// Update the state vector
    void setState( const Gaudi::TrackVector& state );

    /// Update the state vector
    void setState( const LHCb::StateVector& state );

    /// Update the state vector
    void setState( double x, double y, double z, double tx, double ty, double qOverP );

    /// Update the state covariance matrix
    void setCovariance( const Gaudi::TrackSymMatrix& value );

    /// Update the x-position of the state
    void setX( double value );

    /// Update the y-position of the state
    void setY( double value );

    /// Update the z-position of the state
    void setZ( double value );

    /// Update the x-slope Tx=dx/dz slope of the state
    void setTx( double value );

    /// Update the y-slope Ty=dy/dz slope of the state
    void setTy( double value );

    /// Update the charge-over-momentum Q/P value of the state
    void setQOverP( double value );

    /// Update the squared error on the charge-over-momentum Q/P of the state
    void setErrQOverP2( double value );

    /// Check if the state is at a predefined location
    [[nodiscard]] bool checkLocation( const LHCb::State::Location& value ) const { return location() == value; };

    /// transport this state to a new z-position
    void linearTransportTo( double z );

    /// printOut method to Gaudi message stream
    std::ostream& fillStream( std::ostream& os ) const;

    /// Retrieve const  the variety of State flags
    [[nodiscard]] unsigned int flags() const { return m_flags; }

    /// Update  the variety of State flags
    void setFlags( unsigned int value );

    /// Retrieve state location
    [[nodiscard]] LHCb::State::Location location() const {
      return ( LHCb::State::Location )( ( m_flags & locationMask ) >> locationBits );
    }

    /// Update state location
    void setLocation( const LHCb::State::Location& value );

    /// Retrieve const  the state vector
    [[nodiscard]] const Gaudi::TrackVector& stateVector() const { return m_stateVector; }

    /// Retrieve  the state vector
    Gaudi::TrackVector& stateVector() { return m_stateVector; }

    /// Retrieve const  the state covariance matrix (indexes 0,...,4 for x, y, tx, ty, Q/p)
    [[nodiscard]] const Gaudi::TrackSymMatrix& covariance() const { return m_covariance; }

    /// Retrieve  the state covariance matrix (indexes 0,...,4 for x, y, tx, ty, Q/p)
    Gaudi::TrackSymMatrix& covariance() { return m_covariance; }

    friend std::ostream& operator<<( std::ostream& str, const State& obj ) { return obj.fillStream( str ); }

  protected:
    /// Offsets of bitfield flags
    enum flagsBits { locationBits = 0 };

    /// Bitmasks for bitfield flags
    enum flagsMasks { locationMask = 0xffffL };

    unsigned int          m_flags{ 0 };  ///< the variety of State flags
    Gaudi::TrackVector    m_stateVector; ///< the state vector
    Gaudi::TrackSymMatrix m_covariance;  ///< the state covariance matrix (indexes 0,...,4 for x, y, tx, ty, Q/p)
    double                m_z{ 0.0 };    ///< the z-position of the state

  }; // class State

  /// Definition of Keyed Container for State
  using States = State::Container;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::State::State( const Gaudi::TrackVector& stateVec, const Gaudi::TrackSymMatrix& cov, double z,
                           const LHCb::State::Location& location )
    : m_stateVector( stateVec ), m_covariance( cov ), m_z( z ) {
  setLocation( location );
}

inline LHCb::State::State( const Gaudi::TrackVector& stateVec, double z, const LHCb::State::Location& location )
    : m_stateVector( stateVec ), m_covariance(), m_z( z ) {
  setLocation( location );
}

inline LHCb::State::Location LHCb::State::LocationToType( const std::string& aName ) {
  LHCb::State::Location l{ LHCb::State::Location::LocationUnknown };
  return parse( l, aName ).isSuccess() ? l : LHCb::State::Location::LocationUnknown;
}

inline void LHCb::State::setFlags( unsigned int value ) { m_flags = value; }

inline void LHCb::State::setLocation( const LHCb::State::Location& value ) {
  auto val = (unsigned int)value;
  m_flags &= ~locationMask;
  m_flags |= ( ( ( (unsigned int)val ) << locationBits ) & locationMask );
}

inline void LHCb::State::setState( const Gaudi::TrackVector& state ) { m_stateVector = state; }

inline void LHCb::State::setState( const LHCb::StateVector& state ) {
  m_stateVector = state.parameters();
  m_z           = state.z();
}

inline void LHCb::State::setCovariance( const Gaudi::TrackSymMatrix& value ) { m_covariance = value; }

inline void LHCb::State::setX( double value ) { m_stateVector[0] = value; }

inline void LHCb::State::setY( double value ) { m_stateVector[1] = value; }

inline void LHCb::State::setZ( double value ) { m_z = value; }

inline void LHCb::State::setTx( double value ) { m_stateVector[2] = value; }

inline void LHCb::State::setTy( double value ) { m_stateVector[3] = value; }

inline void LHCb::State::setQOverP( double value ) { m_stateVector[4] = value; }

inline void LHCb::State::setErrQOverP2( double value ) { m_covariance( 4, 4 ) = value; }
