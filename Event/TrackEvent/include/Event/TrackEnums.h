/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Kernel/meta_enum.h"

#include <map>
#include <stdexcept>
#include <string>
#include <vector>

namespace LHCb::Event::Enum::Track {

  meta_enum_class( History, int,
                   // numbers are (almost!) consistent with LHCb::Event::v1::Track::History
                   Unknown              = 0,  // unknown history (i.e. history not set)
                   TrackIdealPR         = 1,  // track produced with the ideal pattern recognition
                   MuonID               = 10, // track produced with the Muon pattern recognition
                   PrForward            = 30, // track produced with the PrForward pattern recognition
                   PrSeeding            = 31, // track produced with the PrSeeding pattern recognition
                   PrMatch              = 32, // track produced with the PrMatch pattern recognition
                   PrDownstream         = 33, // track produced with the PrDownstream pattern recognition
                   PrVeloUT             = 34, // track produced with the PrVeloUT pattern recognition
                   SciFiTrackForwarding = 35, // track produced with the SciFiTrackForwarding pattern recognition
                   PrPixel              = 36, // track produced with the VeloClusterTrackingSIMD pattern recognition
                   PrVeloHeavyFlavour   = 37, // track produced with the VeloHeavyFlavourTrackFinder pattern recognition
                   Last )

      /// Track fit history enumerations
      meta_enum_class( FitHistory, int, Unknown = 0, // track probably not fitted
                       PrKalmanFilter,               // track fitted by PrKalmanFilter
                       TrackMasterFitter,            // track fitted by TrackMasterFitter
                       VeloKalman,                   // track fitted VeloKalman (CPU or GPU version)
                       Last )

      /// Track type enumerations, note that these are not consecutive numbers (to not break downstream code)
      meta_enum_class( Type, int,
                       Unknown      = 0,  // track of undefined type
                       Velo         = 1,  // VELO track
                       VeloBackward = 2,  // VELO backward track
                       Long         = 3,  // forward track
                       Upstream     = 4,  // upstream track
                       Downstream   = 5,  // downstream track
                       Ttrack       = 6,  // seed track
                       Muon         = 7,  // muon track
                       UT           = 10, // UT track
                       SeedMuon     = 11, // SeedMuon track
                       VeloMuon     = 12, // VeloMuon track
                       MuonUT       = 13, // MuonUT track
                       LongMuon     = 14, // LongMuon track
                       Last )

      /// Track pattern recognition status flag enumerations: The flag specifies in which state of the pattern
      /// recognition phase the track is. The status flag is set by the relevant algorithms
      meta_enum_class( PatRecStatus, int,
                       Unknown = 0,       // track in an undefined PR status
                       PatRecIDs,         // pattern recognition track with LHCbIDs
                       PatRecMeas, Last ) // pattern recognition track with Measurements added

      /// Track fitting status flag enumerations: The flag specifies in which state of the fitting phase the track
      /// is. The status flag is set by the relevant algorithms
      meta_enum_class( FitStatus, int,
                       Unknown = 0,      // track in an undefined fitting status
                       Fitted,           // fitted track
                       FitFailed, Last ) // track for which the track fit failed

      /// Track general flag enumerations
      meta_enum_class( Flag, int,
                       Unknown     = 0,      //
                       Invalid     = 2,      // invalid track for physics
                       Clone       = 4,      // clone track (of a corresponding unique track)
                       Used        = 8,      //
                       IPSelected  = 16,     //
                       PIDSelected = 32,     //
                       Selected    = 64, Last ) //

      /// Additional information assigned to this Track by pattern recognition
      meta_enum_class( AdditionalInfo, int,
                       Unknown         = 0,  //
                       PatQuality      = 2,  // Quality variable from PrForward Tracking
                       Cand1stQPat     = 3,  //  Quality of the first candidate
                       Cand2ndQPat     = 4,  //  Quality of the second candidate
                       NCandCommonHits = 5,  //  NCand with common hits
                       Cand1stChi2Mat  = 6,  //  Chi2 of the first candidate
                       Cand2ndChi2Mat  = 7,  //  Chi2 of the second candidate
                       MatchChi2       = 16, // Chi2 from the velo-seed matching (TrackMatching)
                       FitVeloChi2     = 17, // Chi2 of the velo segment (from TrackFitResult)
                       FitVeloNDoF     = 18, // NDoF of the velo segment chisq
                       FitTChi2        = 19, // Chi2 of the T station segment (from TrackFitResult)
                       FitTNDoF        = 20, // NDoF of the T station segment chisq
                       FitMatchChi2    = 21, // Chi2 of the breakpoint between T and TT (from TrackFitResult)
                       FitMuonChi2     = 22,
                       NUTOutliers     = 25, // Number of UT outlier hits (from TrackFitResult)
                       TsaLikelihood   = 32, // Likelihood from tsa seeding
                       CloneDist = 101, // Track is flagged as being a (rejected) clone of another track. Value is the
                                        // KL clone distance
                       nPRVeloRZExpect    = 103, // Number of expected Velo clusters from VELO RZ pattern recognition
                       nPRVelo3DExpect    = 104, // Number of expected Velo clusters from VELO 3D pattern recognition
                       MuonChi2perDoF     = 300, // Chi2/nDoF of muon track fit
                       MuonMomentumPreSel = 301, // 1 if pass Momentum pre-selection, 0 if not
                       MuonInAcceptance   = 302, // 1 if in Muon system InAcceptance, 0 if not
                       IsMuonLoose        = 303, // 1 if pass IsMuonLoose criteria, 0 if not
                       IsMuon             = 304, // 1 if pass IsMuon criteria, 0 if not
                       MuonDist2          = 305, // Squared distance of the closest muon hit to the extrapolated track
                       MuonDLL            = 306, // DLL (from muon system only)
                       MuonNShared = 307,   // NShared (number of additional IsMuon tracks with at least one shared hit
                                            // with the current track and a smaller Dist value)
                       MuonCLQuality = 308, // CLQuality
                       MuonCLArrival = 309, // CLArrival
                       IsMuonTight   = 310, // 1 if pass IsMuonTight criteria, 0 if not
                       Last )

      /**
       * @brief Checks if track type is expected to have hits in T stations.
       *
       * @param type The track type.
       * @return true if track can have SciFi hits.
       * @return false if track cannot have SciFi hits.
       */
      constexpr bool hasT( Type type ) {
    switch ( type ) {
    case Type::LongMuon:
    case Type::Long:
    case Type::Downstream:
    case Type::Ttrack:
    case Type::SeedMuon:
      return true;
    case Type::Unknown:
    case Type::MuonUT:
    case Type::Velo:
    case Type::VeloBackward:
    case Type::Upstream:
    case Type::Muon:
    case Type::UT:
    case Type::VeloMuon:
    case Type::Last:
      return false;
    }
    throw std::invalid_argument( "Track type not handled." );
  }

  /**
   * @brief Checks if track type is expected to have hits in Velo stations.
   *
   * @param type The track type.
   * @return true if track can have Velo hits.
   * @return false if track cannot have Velo hits.
   */
  constexpr bool hasVelo( Type type ) {
    switch ( type ) {
    case Type::Velo:
    case Type::VeloBackward:
    case Type::Upstream:
    case Type::Long:
    case Type::VeloMuon:
    case Type::LongMuon:
      return true;
    case Type::Unknown:
    case Type::Downstream:
    case Type::MuonUT:
    case Type::Muon:
    case Type::UT:
    case Type::Ttrack:
    case Type::SeedMuon:
    case Type::Last:
      return false;
    }
    throw std::invalid_argument( "Track type not handled." );
  }

  /**
   * @brief Checks if track type is expected to have hits in UT stations.
   *
   * @param type The track type.
   * @return true if track can have UT hits.
   * @return false if track cannot have UT hits.
   */
  constexpr bool hasUT( Type type ) {
    switch ( type ) {
    case Type::Downstream:
    case Type::Upstream:
    case Type::Long:
    case Type::MuonUT:
    case Type::LongMuon:
    case Type::UT:
      return true;
    case Type::Velo:
    case Type::VeloBackward:
    case Type::VeloMuon:
    case Type::Unknown:
    case Type::Muon:
    case Type::Ttrack:
    case Type::SeedMuon:
    case Type::Last:
      return false;
    }
    throw std::invalid_argument( "Track type not handled." );
  }

  /**
   * @brief Checks if track type is expected to have hits in the Muon stations.
   *
   * @param type The track type.
   * @return true if track can have Muon hits.
   * @return false if track cannot have Muon hits.
   */
  constexpr bool hasMuon( Type type ) {
    switch ( type ) {
    case Type::MuonUT:
    case Type::LongMuon:
    case Type::VeloMuon:
    case Type::SeedMuon:
    case Type::Muon:
      return true;
    case Type::Downstream:
    case Type::Upstream:
    case Type::Long:
    case Type::UT:
    case Type::Velo:
    case Type::VeloBackward:
    case Type::Unknown:
    case Type::Ttrack:
    case Type::Last:
      return false;
    }
    throw std::invalid_argument( "Track type not handled." );
  }

} // namespace LHCb::Event::Enum::Track

namespace LHCb::Event::Enum::State {
  /// State location enumerations -- NEVER change an existing numerical value, as they appear in persisted data!
  meta_enum_class_with_unknown( Location, int, LocationUnknown, LocationUnknown = 0, ClosestToBeam = 1,
                                FirstMeasurement = 2, EndVelo = 3, AtUT = 4, BegT = 5, BegRich1 = 6, EndRich1 = 7,
                                BegRich2 = 8, EndRich2 = 9, BegECal = 12, ECalShowerMax = 13, MidECal = 14,
                                EndECal = 15, BegHCal = 16, MidHCal = 17, EndHCal = 18, Muon = 19, LastMeasurement = 20,
                                Vertex = 21, V0Vertex = 22, EndUT = 23, EndT = 24, LastFTHit = 25 )

      [[nodiscard]] constexpr auto locations() {
    return members_of<Location>();
  }
} // namespace LHCb::Event::Enum::State

namespace Gaudi::Parsers {
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::History>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::FitHistory>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::Type>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::PatRecStatus>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::FitStatus>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::Flag>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::AdditionalInfo>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::State::Location>&, const std::string& );
  StatusCode parse( std::map<LHCb::Event::Enum::Track::Type, std::vector<LHCb::Event::Enum::State::Location>>&,
                    const std::string& );
} // namespace Gaudi::Parsers
