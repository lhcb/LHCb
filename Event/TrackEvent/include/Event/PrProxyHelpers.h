/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/MatVec.h"
namespace LHCb::Pr::detail {
  /** @todo Deprecate this and follow the model of
   *        LHCb::Pr::Fitted::Forward::Tracks.
   */
  template <typename PosType, typename SlopeType>
  struct VeloState {
    PosType   m_position;
    SlopeType m_slopes;
    VeloState( PosType position, SlopeType slopes )
        : m_position{ std::move( position ) }, m_slopes{ std::move( slopes ) } {}
    PosType const&                                position() const { return m_position; }
    SlopeType const&                              slopes() const { return m_slopes; }
    [[nodiscard, gnu::always_inline]] auto        x() const { return position().X(); }
    [[nodiscard, gnu::always_inline]] auto        y() const { return position().Y(); }
    [[nodiscard, gnu::always_inline]] auto        z() const { return position().Z(); }
    [[nodiscard, gnu::always_inline]] auto        tx() const { return slopes().X(); }
    [[nodiscard, gnu::always_inline]] auto        ty() const { return slopes().Y(); }
    [[nodiscard, gnu::always_inline]] friend auto referencePoint( const VeloState& vs ) {
      return LHCb::LinAlg::Vec{ vs.x(), vs.y(), vs.z() };
    }
    [[nodiscard, gnu::always_inline]] friend auto slopes( const VeloState& vs ) {
      return LHCb::LinAlg::Vec{ vs.tx(), vs.ty(), 1.f };
    }
  };
} // namespace LHCb::Pr::detail
