/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/SOACollection.h"
#include "Event/States.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "LHCbMath/MatVec.h"

namespace LHCb::Event::PosDirParameters {

  enum struct PosDirVector { x, y, z, tx, ty };

  constexpr unsigned int operator+( unsigned int i, PosDirVector s ) { return i + static_cast<unsigned>( s ); }

} // namespace LHCb::Event::PosDirParameters

namespace LHCb::Event {

  /**
   * Tag and Proxy baseclasses for representing a position (x,y,z) and a direction (tx, ty), no momentum
   */
  struct pos_dir_field : floats_field<5> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct NDStateProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using type          = typename simd_t::float_v;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type =
          NumericProxy<Simd, Behaviour, ContainerTypeBase, typename Path::template extend_t<std::size_t>, float>;

      NDStateProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      template <PosDirParameters::PosDirVector i>
      [[nodiscard, gnu::always_inline]] constexpr auto proxy() const {
        return proxy_type{ m_container, m_offset, m_path.append( static_cast<std::size_t>( i ) ) };
      }

      [[nodiscard, gnu::always_inline]] constexpr auto x() const {
        return proxy<PosDirParameters::PosDirVector::x>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto y() const {
        return proxy<PosDirParameters::PosDirVector::y>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto tx() const {
        return proxy<PosDirParameters::PosDirVector::tx>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto ty() const {
        return proxy<PosDirParameters::PosDirVector::ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto z() const {
        return proxy<PosDirParameters::PosDirVector::z>().get();
      }

      [[gnu::always_inline, gnu::flatten]] constexpr decltype( auto ) setPosition( type const& x, type const& y,
                                                                                   type const& z ) {
        proxy<PosDirParameters::PosDirVector::x>().set( x );
        proxy<PosDirParameters::PosDirVector::y>().set( y );
        proxy<PosDirParameters::PosDirVector::z>().set( z );
        return *this;
      }

      template <typename F>
      [[gnu::always_inline, gnu::flatten]] constexpr decltype( auto )
      setPosition( LHCb::LinAlg::Vec<F, 3> const& pos ) {
        proxy<PosDirParameters::PosDirVector::x>().set( pos.x() );
        proxy<PosDirParameters::PosDirVector::y>().set( pos.y() );
        proxy<PosDirParameters::PosDirVector::z>().set( pos.z() );
        return *this;
      }

      [[gnu::always_inline, gnu::flatten]] constexpr decltype( auto ) setDirection( type const& tx, type const& ty ) {
        proxy<PosDirParameters::PosDirVector::tx>().set( tx );
        proxy<PosDirParameters::PosDirVector::ty>().set( ty );
        return *this;
      }

      template <typename F>
      [[gnu::always_inline, gnu::flatten]] constexpr decltype( auto )
      setDirection( LHCb::LinAlg::Vec<F, 3> const& dir ) {
        proxy<PosDirParameters::PosDirVector::tx>().set( dir.x() );
        proxy<PosDirParameters::PosDirVector::ty>().set( dir.y() );
        return *this;
      }

      [[gnu::always_inline, gnu::flatten]] constexpr decltype( auto ) set( type const& x, type const& y, type const& z,
                                                                           type const& tx, type const& ty ) {
        setPosition( x, y, z );
        setDirection( tx, ty );
        return *this;
      }

      [[nodiscard, gnu::always_inline]] constexpr auto get() const { return *this; }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = NDStateProxy<Simd, Behaviour, ContainerType, Path>;
  };
  template <std::size_t... N>
  using pos_dirs_field = ndarray_field<pos_dir_field, N...>;

} // namespace LHCb::Event

/*
 * Define maximum numbers of hits and states for Pr::Tracks which are detector specific
 */
namespace LHCb::Pr::TracksInfo {

  inline constexpr std::size_t NumPosDirPars = 5; // statePosVec vector(5D) (x,y,z,dx,dy)
  // The maximum number of hits for PrLongTracks, depends on the pattern recognition
  inline constexpr std::size_t MaxVPHits   = 52;
  inline constexpr std::size_t MaxUTHits   = 8;
  inline constexpr std::size_t MaxFTHits   = 12;
  inline constexpr std::size_t MaxMuonHits = 8;
} // namespace LHCb::Pr::TracksInfo

// define a helper function to obtain the type of the Tag given the type of the tracks container
// e.g LHCb::Pr::Velo::Tracks --> LHCb::Pr::Velo::Tag
namespace LHCb::Pr {
  template <typename container>
  using tag_type_t = typename container::tag_t;

  /**
   * @brief Map a state location to the array index of the TrackTypes available_states.
   *
   * @tparam T TrackType
   * @param loc State::Location
   * @return constexpr std::size_t index
   */
  template <Event::Enum::Track::Type T>
  constexpr auto stateIndex( Event::Enum::State::Location loc ) {

    constexpr auto state_locs = Event::v3::get_state_locations<Event::v3::available_states_t<T>>{}();
    for ( std::size_t i = 0; i < state_locs.size(); ++i ) {
      if ( state_locs[i] == loc ) return i;
    }
    throw std::invalid_argument( "State " + toString( loc ) + " not available" );
  }

} // namespace LHCb::Pr
