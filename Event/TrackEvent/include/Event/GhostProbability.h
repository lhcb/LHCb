/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Event/SOACollection.h"

/** @class GhostProbabilities GhostProbabilities.h
 *  SoA implementation of ghost probability, to be zipped with the v3::Tracks
 */

namespace LHCb::Event::v3 {

  namespace GhostProbabilityTag {

    struct GhostProbability : Event::float_field {};

    template <typename T>
    using ghostprob_t = Event::SOACollection<T, GhostProbability>;
  } // namespace GhostProbabilityTag

  struct GhostProbabilities : GhostProbabilityTag::ghostprob_t<GhostProbabilities> {
    using base_t = typename GhostProbabilityTag::ghostprob_t<GhostProbabilities>;
    using base_t::base_t;
  };

} // namespace LHCb::Event::v3
