###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Tools/GitEntityResolver
-----------------------
#]=======================================================================]

gaudi_add_header_only_library(GitEntityResolverLib
    LINK
        Boost::filesystem
        LHCb::LHCbKernel
        PkgConfig::git2
)

gaudi_add_module(GitEntityResolver
    SOURCES
        src/GitEntityResolver.cpp
    LINK
        Boost::filesystem
        Boost::headers
        Gaudi::GaudiKernel
        LHCb::GitEntityResolverLib
        LHCb::LHCbKernel
        LHCb::XmlToolsLib
        XercesC::XercesC
)

gaudi_install(PYTHON)

gaudi_install(SCRIPTS)

gaudi_add_pytest(tests/pytest
    PROPERTIES ENVIRONMENT "GIT_TEST_REPOSITORY=${CMAKE_CURRENT_BINARY_DIR}/test_repo/DDDB"
)
gaudi_add_pytest(python OPTIONS --doctest-modules)
gaudi_add_pytest(tests/test_scripts OPTIONS --doctest-modules)
