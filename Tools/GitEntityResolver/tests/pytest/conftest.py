###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import re
from subprocess import check_output
from textwrap import dedent

import pytest
from LHCbTesting import LHCbExeTest


class GERTestHelper(LHCbExeTest):
    condition_values = []
    init_template = dedent("""\
        ToolSvc.GitDDDB     DEBUG Initializing...
        ToolSvc.GitDDDB      INFO opening Git repository '{repository}'
        ToolSvc.GitDDDB      INFO using commit '{tag}' corresponding to {commit_id}
        ToolSvc.GitDDDB   VERBOSE ServiceLocatorHelper::service: found service IncidentSvc
        ToolSvc.GitDDDB     DEBUG registering to IncidentSvc
        ToolSvc.GitDDDB     DEBUG Successfully initialized.
        """)
    bare = False

    @pytest.fixture(scope="class")
    def repo_info(self):
        repository = os.environ["GIT_TEST_REPOSITORY"] + (
            "-bare.git" if self.bare else ""
        )
        if self.tag == "<files>":
            commit_id = "None"
        else:
            commit_id = (
                check_output(["git", "rev-parse", self.tag], cwd=repository)
                .decode("utf-8")
                .strip()
            )
        info = dict(
            repository=repository,
            tag=self.tag,
            commit_id=commit_id,
            short_id=commit_id[:8],
        )
        return info

    def assert_block(self, block: str, stdout: bytes):
        stdout = stdout.decode()
        assert block in stdout

    def test_initialization(self, stdout: bytes, repo_info: dict[str, str]):
        expected = self.init_template.format(**repo_info)
        print(expected)
        self.assert_block(expected, stdout)

    def test_reported_tag(self, stdout: bytes, repo_info: dict[str, str]):
        expected = (
            "LoadDDDB             INFO Database {repository} tag {tag}[{short_id}]".format(
                **repo_info
            )
            if repo_info["tag"] != "<files>"
            else "LoadDDDB             INFO Database {repository} tag {tag}".format(
                **repo_info
            )
        )
        self.assert_block(expected, stdout)

    def test_condition_value(self, stdout: bytes):
        if not self.condition_values:
            pytest.skip("No condition values to check")
        stripped_output = "\n".join(
            l
            for l in stdout.decode().splitlines()
            if re.match(r"^---|^Validity|^\(int\) parameter", l)
        )
        expected = "\n".join(
            f"--- /dd/{name}\nValidity: {validity}\n{value}"
            for name, validity, value in self.condition_values
        )

        assert stripped_output == expected
