###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from conftest import GERTestHelper
from GaudiTesting import NO_ERROR_MESSAGES


@pytest.mark.ctest_fixture_required("gitentityresolver.prepare")
@pytest.mark.shared_cwd("GitEntityResolver")
class Test(GERTestHelper):
    command = ["gaudirun.py", "-v"]

    def options(self):
        from DetDescChecks.Options import LoadDDDBTest

        LoadDDDBTest("2016")

        from GitCondDB.TestOptions import setup

        setup(tag="v1")

    reference = {"messages_count": NO_ERROR_MESSAGES}
    tag = "v1"
    condition_values = [
        ("TestCondition", "0.0 -> 9223372036.854775807", "(int) parameter = 2016"),
    ]
