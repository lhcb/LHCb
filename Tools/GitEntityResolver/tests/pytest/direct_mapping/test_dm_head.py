###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from conftest import GERTestHelper
from GaudiTesting import NO_ERROR_MESSAGES


@pytest.mark.ctest_fixture_required("gitentityresolver.prepare")
@pytest.mark.shared_cwd("GitEntityResolver")
class Test(GERTestHelper):
    command = ["gaudirun.py", "-v"]

    def options(self):
        from DetDescChecks.Options import LoadDDDBTest

        LoadDDDBTest("2016")

        from GitCondDB.TestOptions import setup

        setup(
            tag="HEAD",
            conditions=[
                "/dd/Direct/Cond1",
                "/dd/Direct/Cond2",
                "/dd/Direct/Nested/Cond3",
            ],
        )

    reference = {"messages_count": NO_ERROR_MESSAGES}
    tag = "HEAD"
    condition_values = [
        ("Direct/Cond1", "123456.0 -> 9223372036.854775807", "(int) parameter = 1"),
        ("Direct/Cond2", "0.0 -> 9223372036.854775807", "(int) parameter = 0"),
        ("Direct/Nested/Cond3", "0.0 -> 9223372036.854775807", "(int) parameter = 0"),
    ]
