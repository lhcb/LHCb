###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v"]

    def options(self):
        from Configurables import ApplicationMgr
        from Configurables import LHCbTests__GenericTool__Algorithm as Alg

        ApplicationMgr(TopAlg=[Alg("Alg")], EvtSel="NONE", EvtMax=1)

    def test_stdout(self, stdout: bytes):
        expected_block = b"""
Alg                  INFO executing...
Alg.Generic          INFO MyGenericTool::execute()
Alg.GenericTS        INFO MyGenericTSTool::execute() const
Alg.GenericTS        INFO MyGenericTSTool::execute() const
Alg                  INFO from const method...
Alg.GenericTS        INFO MyGenericTSTool::execute() const
Alg                  INFO ...done
"""

        assert expected_block in stdout, (
            f"Expected block missing in standard output: {expected_block}"
        )
