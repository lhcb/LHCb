###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["python", "../options/odinemulator.py"]
    reference = {"messages_count": {"FATAL": 0, "ERROR": 0, "WARNING": 0}}

    def test_stdout(self, stdout: bytes):
        # Being determinisitic, the emulator should always accept the same events and
        # hence emulate the same accept fractions
        expected_block = b"""
ODINEmulator                           INFO #accept(4,0): #=0       Sum=0           Eff=|(-100.0000 +- -100.000)%|
ODINEmulator                           INFO #accept(4,1): #=0       Sum=0           Eff=|(-100.0000 +- -100.000)%|
ODINEmulator                           INFO #accept(4,2): #=0       Sum=0           Eff=|(-100.0000 +- -100.000)%|
ODINEmulator                           INFO #accept(4,3): #=1000    Sum=523         Eff=|( 52.30000 +- 1.57947 )%|
ODINEmulator                           INFO #accept(8,0): #=0       Sum=0           Eff=|(-100.0000 +- -100.000)%|
ODINEmulator                           INFO #accept(8,1): #=0       Sum=0           Eff=|(-100.0000 +- -100.000)%|
ODINEmulator                           INFO #accept(8,2): #=0       Sum=0           Eff=|(-100.0000 +- -100.000)%|
ODINEmulator                           INFO #accept(8,3): #=1000    Sum=247         Eff=|( 24.70000 +- 1.36379 )%|
"""

        assert expected_block in stdout, (
            f"Expected block missing in standard output: {expected_block}"
        )
