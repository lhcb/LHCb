###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "../options/errorstream.py"]
    reference = {"messages_count": {"FATAL": 0, "ERROR": 0, "WARNING": 33}}

    def test_stderr(self, stderr: bytes):
        # Varying number of error lines.
        pytest.skip()

    def test_stdout(self, stdout: bytes):
        # Being determinisitic, the emulator should always accept the same events and
        # hence emulate the same accept fractions
        expected_block = b"""
NONLAZY_AND: top                                        #=50      Sum=0           Eff=|( 0.000000 +- 0.00000 )%|
 LAZY_AND: algs                                         #=50      Sum=17          Eff=|( 34.00000 +- 6.69925 )%|
  ConfigurableDummy/ConfigurableDummy_b0635f1e          #=50      Sum=17          Eff=|( 34.00000 +- 6.69925 )%|
 LAZY_AND: writer                                       #=50      Sum=33          Eff=|( 66.00000 +- 6.69925 )%|
  MessageSvcEventFilter/MessageSvcEventFilter_56e8af36  #=50      Sum=33          Eff=|( 66.00000 +- 6.69925 )%|
"""
        assert expected_block in stdout, (
            f"Missing expected block in standard output: {expected_block}"
        )
