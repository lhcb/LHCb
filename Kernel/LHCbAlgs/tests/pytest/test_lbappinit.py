###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import inspect

from LHCbTesting import LHCbExeTest


def config():
    from Configurables import ApplicationMgr
    from DDDB.CheckDD4Hep import UseDD4Hep

    from PyConf.Algorithms import LHCbTests__LbAppInitTester
    from PyConf.application import ApplicationOptions, configure, configure_input
    from PyConf.control_flow import CompositeNode

    options = ApplicationOptions(_enabled=False)
    options.input_type = "NONE"
    if not UseDD4Hep:
        options.dddb_tag = "upgrade/dddb-20220705"
        options.conddb_tag = "upgrade/sim-20220705-vc-mu100"
    else:
        options.geometry_version = "run3/trunk"
        options.conditions_version = "master"
    options.simulation = True
    options.evt_max = 1
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.EvtSel = "NONE"  # ignore input configuration

    cf = CompositeNode(
        "test",
        [
            LHCbTests__LbAppInitTester(name="LbAppInitTester"),
        ],
    )
    app.OutStream.clear()
    config.update(configure(options, cf))


class Test(LHCbExeTest):
    currentFile = inspect.getfile(inspect.currentframe())
    command = ["gaudirun.py", f"{currentFile}:config"]

    def test_stdout(self, stdout: bytes):
        import re

        from DDDB.CheckDD4Hep import UseDD4Hep

        stdout = stdout.decode()
        if not UseDD4Hep:
            stdout = re.sub(r"  /[^ ]*(DDDB|SIMCOND)", r"\1", stdout)
            expected = """LbAppInitTester                        INFO CondDB tags:
DDDB.git -> upgrade/dddb-20220705[c4e7d0e8]
SIMCOND.git -> upgrade/sim-20220705-vc-mu100[5d1f40ae]"""

        else:
            stdout = re.sub(
                r"git:[^ ]*lhcb-conditions-database\.git",
                "git:/.../lhcb-conditions-database.git",
                stdout,
            )
            expected = """LbAppInitTester                        INFO CondDB tags:
  GeometryLocation -> ${DETECTOR_PROJECT_ROOT}/compact
  GeometryVersion -> run3/trunk
  GeometryMain -> LHCb.xml
  ConditionLocation -> git:/.../lhcb-conditions-database.git
  ConditionVersion -> master"""

        assert expected in "\n".join(stdout.splitlines()), (
            f"Expected block missing: {expected}"
        )
