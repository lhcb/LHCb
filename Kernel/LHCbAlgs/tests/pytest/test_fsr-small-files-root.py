###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from pathlib import Path

from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    """
    Author: rlambert
    Purpose: Check that running over small files does create an openable output file with FSRs
    Prerequisites: None
    Common failure modes, severities and cures:
                  . SEVERE: Segfault or raised exception, stderr, nonzero return code
                  . MAJOR: No ERROR messages should ever be printed when running this test.
                  . MAJOR: An unopenable output file is a major problem here
    """

    command = ["gaudirun.py", "../options/fsr-small-file.py"]

    def test_tfile(self, cwd: Path):
        import ROOT

        tf = ROOT.TFile.Open(str(cwd / "tryRoot.dst"))
        assert tf.GetListOfKeys().GetSize() != 0, (
            "No keys present, output file (tryRoot.dst) is corrupted."
        )

        t1 = tf.Get("Event")
        assert (t1 is not None and t1) and (t1.GetEntries() >= 10), (
            "Not enough events copied, output file (tryRoot.dst) is corrupted."
        )
