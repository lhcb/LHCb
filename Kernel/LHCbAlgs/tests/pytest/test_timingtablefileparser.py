###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from pathlib import Path

#    """
#    Author: rlambert, refactored by pkopciew
#    Purpose: unit test of the Timing Table csv/dat parser.
#    Prerequisites: None
#    Common failure modes, severities and cures:
#                  . MAJOR: Any failure indicates a logic error in the parsing code.
#    """


def resolve_ref_path(relative_path: str) -> str:
    """Resolve a relative file path to an absolute path."""
    return str((Path(__file__).resolve().parent / relative_path).resolve())


def test_timingref():
    from LHCbAlgs import TTParser

    comp = TTParser.compare(
        resolve_ref_path("../refs/timing.dat"), resolve_ref_path("../refs/timing.csv")
    )
    assert comp == (["TestAlg4"], ["TestAlg2", "TestAlg3"], ["TestAlg5"]), (
        f"Found: {comp} while expected: {(['TestAlg4'], ['TestAlg2', 'TestAlg3'], ['TestAlg5'])}."
    )
