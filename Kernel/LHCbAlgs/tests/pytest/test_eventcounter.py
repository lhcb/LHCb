###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTesting import NO_ERROR_MESSAGES
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v"]
    reference = {"messages_count": NO_ERROR_MESSAGES}

    def options(self):
        from Configurables import EvtCounter
        from Configurables import LHCbAlgsTests__ForceEventCounter as FC
        from Configurables import LHCbAlgsTests__TestEventCounter as EC
        from Gaudi.Configuration import ApplicationMgr

        app = ApplicationMgr(EvtSel="NONE", EvtMax=5)

        counters = [
            EvtCounter("NotFromOne", InitialCount=123),
            EvtCounter("NotIncreasing", UseIncident=False),
            EvtCounter("Forcing", UseIncident=False),
        ]

        default = EC("Test-Default")
        algs = [FC("ForceCounter"), default]
        algs += [
            EC("Test-%s" % cnt.name().replace("ToolSvc.", ""), EvtCounter=cnt)
            for cnt in counters
        ]

        app.TopAlg = algs

    def test_stdout(self, stdout: bytes):
        expected_block = b"""
ApplicationMgr       INFO Application Manager Started successfully
Test-Default         INFO Event count = 1
Test-NotFromOne      INFO Event count = 123
Test-NotIncreasing   INFO Event count = 1
Test-Forcing         INFO Event count = 0
Test-Default         INFO Event count = 2
Test-NotFromOne      INFO Event count = 124
Test-NotIncreasing   INFO Event count = 1
Test-Forcing         INFO Event count = 10
Test-Default         INFO Event count = 3
Test-NotFromOne      INFO Event count = 125
Test-NotIncreasing   INFO Event count = 1
Test-Forcing         INFO Event count = 20
Test-Default         INFO Event count = 4
Test-NotFromOne      INFO Event count = 126
Test-NotIncreasing   INFO Event count = 1
Test-Forcing         INFO Event count = 30
Test-Default         INFO Event count = 5
Test-NotFromOne      INFO Event count = 127
Test-NotIncreasing   INFO Event count = 1
Test-Forcing         INFO Event count = 40
"""

        assert expected_block in stdout, (
            f"Missing expected block in standard output: {expected_block}"
        )
