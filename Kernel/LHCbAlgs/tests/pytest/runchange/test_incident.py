###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v"]

    def options(self):
        from Configurables import LHCbAlgsTests__RunChangeIncidentHandler as IncidentAlg
        from Configurables import LHCbAlgsTests__RunChangeTest as TestAlg
        from Gaudi.Configuration import ApplicationMgr, EventDataSvc

        app = ApplicationMgr()
        app.TopAlg = [TestAlg("RunChangeTest"), IncidentAlg("IncidentHandler")]
        app.EvtSel = "NONE"
        app.EvtMax = 5
        EventDataSvc(ForceLeaves=True)

    def test_stdout(self, stdout: bytes):
        reference_block = b"""RunChangeTest        INFO Define inital condition
IncidentHandler      INFO RunChange incident received from RunChangeTest.OdinTimeDecoder
IncidentHandler      INFO Run 1
RunChangeTest        INFO Test: new run
IncidentHandler      INFO RunChange incident received from RunChangeTest.OdinTimeDecoder
IncidentHandler      INFO Run 2
RunChangeTest        INFO Test: same run (stable, no trigger)
RunChangeTest        INFO Test: same run (stable, no trigger)
RunChangeTest        INFO Test: same run (stable, no trigger)
"""
        assert reference_block in stdout, (
            f"Missing expected block in standard output: {reference_block}"
        )
