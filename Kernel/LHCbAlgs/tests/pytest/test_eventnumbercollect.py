###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from pathlib import Path

from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    """Author: sponce
    Purpose: Check that EventNumberCollectAlg is working properly
    """

    command = ["gaudirun.py", "../options/EventNumbersCollect.py"]
    reference = "../refs/eventnumbercollect.yaml"

    def test_json_output(self, cwd: Path):
        import json

        ref = [247644, 247701, 247725]
        try:
            f = open(str(cwd / "numbers.json"))
            data = json.load(f)
            assert data == ref, (
                f"Extracted numbers: {data} do not match the reference: {ref}"
            )
        except IOError:
            raise IOError("Could not find data file numbers.json")
        except json.JSONDecodeError as e:
            raise json.JSONDecodeError(e.msg)
