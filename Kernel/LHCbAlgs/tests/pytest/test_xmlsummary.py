###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from pathlib import Path

from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    """
    Author: M. Cattaneo
    Purpose: Check the XmlSummary is produced when xml_summary_file is defined
    Prerequisites: None
    """

    command = ["gaudirun.py", "../options/XmlSummary.py"]

    def test_stdout(self, cwd: Path):
        import os.path

        assert os.path.isfile(cwd / "testSummary.xml"), "XML Summary was not created"
