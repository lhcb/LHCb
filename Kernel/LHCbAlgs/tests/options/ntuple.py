###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test that PyConf can be used to write out ntuples (ROOT files)."""

from Gaudi.Configuration import ERROR

from PyConf.Algorithms import TupleAlg
from PyConf.application import ApplicationOptions, configure, configure_input
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.data_type = "Upgrade"
options.evt_max = 100
options.simulation = True
options.ntuple_file = "ntuple.root"
options.input_type = "None"
options.dddb_tag = "upgrade/master"  # unused
options.conddb_tag = "upgrade/master"  # unused
options.geometry_version = "run3/trunk"  # unused
options.conditions_version = "master"  # unused
config = configure_input(options)

# Suppress warnings of the type "Tuple 'Types Test Column Wise' 'long' has
# different sizes on 32/64 bit systems."
algs = [TupleAlg(name="Tuple", OutputLevel=ERROR)]

config.update(configure(options, CompositeNode("TopSeq", algs)))
