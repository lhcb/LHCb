###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    input_from_root_file,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("2010_justFSR_EW")
options.xml_summary_file = "testSummary.xml"

config = configure_input(options)

cf_node = CompositeNode(
    "Seq",
    children=[
        input_from_root_file("/Event/DAQ/RawEvent", forced_type="LHCb::RawEvent")
    ],
)
config.update(configure(options, cf_node))
