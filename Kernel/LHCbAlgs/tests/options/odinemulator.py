###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Workaround for ROOT-10769
import warnings

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import cppyy

from PyConf.Algorithms import ODINEmulator
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    default_raw_banks,
    default_raw_event,
    make_odin,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb(
    "2017HLTCommissioning_BnoC_MC2015_Bs2PhiPhi_L0Processed"
)
options.evt_max = 1000
options.gaudipython_mode = True
config = configure_input(options)

LUMI_BIT = cppyy.gbl.LHCb.ODIN.EventTypes.Lumi
LUMI_FRACTION = 0.25
NOBIAS_BIT = cppyy.gbl.LHCb.ODIN.EventTypes.NoBias
NOBIAS_FRACTION = 0.5

odinAlg = make_odin()
emulator = ODINEmulator(
    name="ODINEmulator",
    InputODINLocation=odinAlg,
    # Set our accept fractions for p-p crossings
    AcceptFractions={
        "Lumi": [0, 0, 0, LUMI_FRACTION],
        "NoBias": [0, 0, 0, NOBIAS_FRACTION],
    },
)

# in GaudiPython mode, we need to explicitely give the whole sequence as HLTControlFlowMgr is not used
algs = [
    default_raw_event("ODIN"),
    default_raw_banks("ODIN"),
    odinAlg,
    emulator,
]
config.update(configure(options, CompositeNode("muon_decoding", algs)))

import GaudiPython as GP
from GaudiKernel.DataHandle import DataHandle
from PRConfig import TestFileDB

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()

event = 0
nlumi = nnobias = 0
while event < options.evt_max:
    odin = TES[odinAlg.location]
    if not odin:
        event += 1
        appMgr.run(1)
        continue

    # In MC, the Lumi and NoBias bits are never set, and all events are p-p
    # crossings
    assert odin.eventType() & LUMI_BIT == 0, "Unexpected Lumi bit"
    assert odin.eventType() & NOBIAS_BIT == 0, "Unexpected NoBias bit"
    assert odin.bunchCrossingType() == cppyy.gbl.LHCb.ODIN.BXTypes.BeamCrossing, (
        "Unexpected non beam-beam crossing"
    )

    emulated = TES[emulator.OutputODINLocation.location]
    assert emulated, "Emulated ODIN not found"
    if emulated.eventType() & LUMI_BIT:
        nlumi += 1
    if emulated.eventType() & NOBIAS_BIT:
        nnobias += 1

    event += 1
    appMgr.run(1)

assert nlumi > 0, "No emulated Lumi events found"
assert nnobias > 0, "No emulated NoBias events found"
