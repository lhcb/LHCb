#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from __future__ import print_function

import sys
import time
from builtins import range
from random import randrange

rawEventLoc = "DAQ/RawEvent"
srcOdinLoc = "source/ODIN"
dstOdinLoc = "destination/ODIN"


## Configuation function.
def configure():
    import Gaudi.Configuration
    from Configurables import ODINDecodeTool, ODINEncodeTool

    encode = ODINEncodeTool()
    encode.ODINLocation = srcOdinLoc
    encode.RawEventLocation = rawEventLoc
    encode.BankVersion = 7
    decode = ODINDecodeTool()
    decode.ODINLocation = dstOdinLoc
    decode.RawEventLocations = [rawEventLoc]


## Main function
configure()

import GaudiPython
from GaudiPython import setOwnership

# prepare the application
app = GaudiPython.AppMgr()
obj = [GaudiPython.gbl.DataObject() for i in range(4)]
for o in obj:
    setOwnership(o, 0)

app.evtSvc().setRoot("/Event", obj[0])
app.evtSvc().registerObject("source", obj[1])
app.evtSvc().registerObject("destination", obj[2])
app.evtSvc().registerObject("DAQ", obj[3])

fields = [  # (name, bits)
    (
        name[0].lower() + name[1:],
        getattr(GaudiPython.gbl.LHCb.ODIN.Fields, name + "Size"),
    )
    for name in [
        "RunNumber",
        "EventType",
        "CalibrationStep",
        "GpsTime",
        "TriggerConfigurationKey",
        "PartitionID",
        "BunchId",
        "BunchCrossingType",
        "NonZeroSuppressionMode",
        "TimeAlignmentEventCentral",
        "TimeAlignmentEventIndex",
        "StepRunEnable",
        "TriggerType",
        "TimeAlignmentEventFirst",
        "CalibrationType",
        "OrbitNumber",
        "EventNumber",
    ]
]

# check we have all bits
assert sum(bits for _, bits in fields) == (10 * 32), "some fields are missing"

# Fill the ODIN object
odin = GaudiPython.gbl.LHCb.ODIN()

# Skip the version number
# odin.setVersion(5)

inputs = {}
for name, bits in fields:
    inputs[name] = randrange(1 << bits)
    getattr(odin, "set" + name[0].upper() + name[1:])(inputs[name])

setOwnership(odin, 0)
app.evtSvc().registerObject(srcOdinLoc, odin)

# The raw event must be created before calling the encoder
raw = GaudiPython.gbl.LHCb.RawEvent()
setOwnership(raw, 0)
app.evtSvc().registerObject(rawEventLoc, raw)

encoder = app.toolsvc().create("ODINEncodeTool", interface="IGenericTool")
decoder = app.toolsvc().create("ODINDecodeTool", interface="IGenericTool")

# Encode
encoder.execute()
# Decode
decoder.execute()

new_odin = app.evtSvc()[dstOdinLoc]

error = False
print("")
print(": ===================================================")
# `DataObject::version` returns `unsigned char` so we have to convert to int
print(": Decoded ODIN version %d" % ord(new_odin.version()))
print(": === Comparing original and decoded ODIN objects ===")
for name, _ in fields:
    o = getattr(odin, name)()
    n = getattr(new_odin, name)()
    if o != n or o != inputs[name]:
        error = True
        print(
            ": Mismatch in %-30r: orig = 0x%X, new = 0x%X, set = 0x%X"
            % (name, o, n, inputs[name])
        )
    else:
        print(": OK          %-30r: 0x%X" % (name, n))
print(": ===================================================")
print("")

if error:
    sys.exit(1)
