/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Gaudi/Algorithm.h"
#include "GaudiKernel/DataObjectHandle.h"

namespace Gaudi {

  /**
   *  Trivial algorithm to remove data in the TES
   *  @author Chris Jones Christopher.Rob.Jones@cern.ch
   *  @date 2012-11-08
   */
  class DataRemove final : public Algorithm {
  public:
    using Algorithm::Algorithm;

    StatusCode execute( const EventContext& ) const override {
      if ( !m_dataLocation.objKey().empty() ) {
        if ( DataObject* data = m_dataLocation.getIfExists(); data ) {
          return evtSvc()->unregisterObject( data ).andThen( [&] { delete data; } );
        }
      }
      return StatusCode::SUCCESS;
    }

  private:
    DataObjectReadHandle<DataObject> m_dataLocation{ this, "DataLocation", "" };
  };

  DECLARE_COMPONENT( DataRemove )

} // namespace Gaudi
