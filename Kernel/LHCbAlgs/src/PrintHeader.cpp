/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "LHCbAlgs/Consumer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrintHeader
//
// 2003-03-16 : Gloria Corti
//-----------------------------------------------------------------------------

/** @class PrintHeader PrintHeader.h cmt/PrintHeader.h
 *
 *  Print event and run number in debug mode
 *
 *  @author Gloria Corti
 *  @date   2003-03-16
 */

class PrintHeader final : public LHCb::Algorithm::Consumer<void( const LHCb::ODIN& )> {
  mutable Gaudi::Accumulators::Counter<> m_nEvents{ this, "EventCount" }; ///< Counter of events processed
  Gaudi::Property<unsigned int>          m_frequency{
      this, "Frequency", 1,
      "Frequency of event printing. Only every <Frequency> event will be printed. "
               "Default to 1. Note that in mutltithreaded mode, this is not reliable when not equal to 1" };

public:
  /// Standard constructor
  PrintHeader( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{ name, pSvcLocator, { "ODINLocation", LHCb::ODINLocation::Default } } {}

  void operator()( const LHCb::ODIN& odin ) const override {
    ++m_nEvents;
    auto n = m_nEvents.nEntries();
    if ( m_frequency.value() <= 1 || n % m_frequency.value() == 1 ) {
      info() << "# " << n << " Run " << odin.runNumber() << ", Event " << odin.eventNumber() << endmsg;
    }
  }
};

// Declaration of the Algorithm Factory

DECLARE_COMPONENT( PrintHeader )
