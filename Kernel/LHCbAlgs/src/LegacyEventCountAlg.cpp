/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "LHCbAlgs/Consumer.h"

namespace LHCb {

  /**
   * Super simple algo counting events - Legacy version, DO NOT USE
   * FIXME : only used to test XMLSummarySvc, should be dropped when this goes
   */
  struct LegacyEventCountAlg final
      : public LHCb::Algorithm::Consumer<void(), Gaudi::Functional::Traits::useLegacyGaudiAlgorithm> {
    using Consumer::Consumer;
    void operator()() const override { ++counter( "efficiency" ); }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( LegacyEventCountAlg, "LegacyEventCountAlg" )

} // namespace LHCb
