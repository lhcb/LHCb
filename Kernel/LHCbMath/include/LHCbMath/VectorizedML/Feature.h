/*****************************************************************************\
* (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <LHCbMath/Utils.h>
#include <tuple>

namespace LHCb::VectorizedML {

  struct Feature {};

  template <typename... Feats>
  struct Features {
    constexpr static size_t Size = sizeof...( Feats );
    constexpr size_t        size() const { return Size; }

    template <int idx>
    auto constexpr get() const {
      return std::get<idx>( m_features );
    }

    const char* name( int idx ) const {
      const char* name = nullptr;
      Utils::unwind<0, sizeof...( Feats )>( [&]( auto j ) {
        if ( idx == j ) name = std::get<j>( m_features ).name();
      } );
      return name;
    }

  private:
    std::tuple<Feats...> m_features;
  };

} // namespace LHCb::VectorizedML
