/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @file LHCbKernel/DeterministicMixer.h
 *
 *  Quasi-random generator for DeterministicPrescaler
 */
#pragma once
#include "Kernel/STLExtensions.h"
#include "LHCbMath/bit_cast.h"
#include <algorithm>
#include <cstdint>
#include <ostream>
#include <string_view>

namespace LHCb {

  struct DeterministicMixer {

    uint32_t state;

    constexpr explicit DeterministicMixer( uint32_t state = 0 ) noexcept : state{ state } {}
    constexpr explicit DeterministicMixer( std::string_view s ) noexcept : state{ static_cast<uint32_t>( s.size() ) } {
      ( *this )( s );
    }

    friend std::ostream& operator<<( std::ostream& os, const DeterministicMixer& m ) { return os << m.state; }

    // mix some 'extra' entropy into 'state' and return result
    constexpr DeterministicMixer& operator()( uint32_t extra ) noexcept {
      // note: the constants below are _not_ arbitrary, but are picked
      //       carefully such that the bit shuffling has a large 'avalanche' effect...
      //       See https://web.archive.org/web/20130918055434/http://bretm.home.comcast.net/~bretm/hash/
      //
      // note: as a result, you might call this a quasi-random (not to be confused
      //       with psuedo-random!) number generator, in that it generates an output
      //       which satisfies a requirement on the uniformity of its output distribution.
      //       (and not on the predictability of the next number in the sequence,
      //       based on knowledge of the preceding numbers)
      //
      // note: another way to look at this is is as an (approximation of an) evaporating
      //       black hole: whatever you dump in to it, you get something uniformly
      //       distributed back ;-)
      //
      state += extra;
      state += ( state << 16 );
      state ^= ( state >> 13 );
      state += ( state << 4 );
      state ^= ( state >> 7 );
      state += ( state << 10 );
      state ^= ( state >> 5 );
      state += ( state << 8 );
      state ^= ( state >> 16 );
      return *this;
    }

    // mix some 'extra' entropy into 'state' and return result
    constexpr DeterministicMixer& operator()( uint64_t extra ) noexcept {
      constexpr auto mask = ( ( ~uint64_t{ 0 } ) >> 32 );
      return ( *this )( uint32_t( extra & mask ) )( uint32_t( ( extra >> 32 ) & mask ) );
    }

    // mix some 'extra' entropy into 'state' and return result
    constexpr DeterministicMixer& operator()( std::string_view extra ) noexcept {
      // FIXME: this _might_ do something different on big endian vs. small endian machines...
      auto to_uint32 = []( span<const char, 4> a ) {
        return uint32_t( a[0] ) | uint32_t( a[1] ) << 8 | uint32_t( a[2] ) << 16 | uint32_t( a[3] ) << 24;
      };
      // prefix extra with ' ' until the size is a multiple of 4.
      if ( auto rem = extra.size() % 4; rem != 0 ) {
        // prefix name with ' ' until the size is a multiple of 4.
        auto prefix = std::array{ ' ', ' ', ' ', ' ' };
        std::copy_n( extra.data(), rem, std::next( prefix.data(), 4 - rem ) );
        ( *this )( to_uint32( prefix ) );
        extra.remove_prefix( rem );
      }
      for ( ; !extra.empty(); extra.remove_prefix( 4 ) ) {
        ( *this )( to_uint32( span<const char>{ extra }.first<4>() ) );
      }
      return *this;
    }

    // be paranoid: forbid _any_ implict conversions...
    // ... unless it is specifically blessed
    template <typename T>
    constexpr DeterministicMixer& operator()( const T& t ) {
      if constexpr ( std::is_constructible_v<std::string_view, T> ) {
        return ( *this )( std::string_view{ t } );
      } else if constexpr ( std::is_integral_v<T> && sizeof( T ) == sizeof( uint32_t ) ) {
        return ( *this )( bit_cast<uint32_t>( t ) );
      } else if constexpr ( std::is_integral_v<T> && sizeof( T ) == sizeof( uint64_t ) ) {
        return ( *this )( bit_cast<uint64_t>( t ) );
      } else {
        // empty on purpose -- so that this doesn't compile if we get here ;-)
      }
    }

    template <typename... Ts, typename = std::enable_if_t<( sizeof...( Ts ) > 1 )>>
    constexpr DeterministicMixer& operator()( Ts&&... ts ) {
      return ( ( *this )( std::forward<Ts>( ts ) ), ... );
    }
  };

} // namespace LHCb
