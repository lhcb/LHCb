/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <cmath>
#include <cstdint>
#include <limits>
#include <ratio>
#include <type_traits>

// local
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/SIMDTypes.h"
#include "LHCbMath/bit_cast.h"

namespace LHCb::Math {

  /// Precision type
  enum class PrecisionMode {
    Relative,       ///< Relative precision, based on masking mantissa bits
    Absolute,       ///< Absolute precision, based on number of decimal places or std::ratio
    AbsoluteRounded ///< Same as Absolute, but in addition rounded to nearest value
  };

  namespace details {

    /// int type
    template <typename TYPE>
    using INT = std::conditional_t<std::is_same_v<TYPE, float>, std::int32_t, std::int64_t>;
    /// unsigned int type
    template <typename TYPE>
    using UINT = std::conditional_t<std::is_same_v<TYPE, float>, std::uint32_t, std::uint64_t>;

    /// clang does not have constexpr std::pow so roll our own for now... :(
    template <typename TYPE>
    inline constexpr std::enable_if_t<std::is_integral_v<TYPE>, TYPE> //
    pow_of_ten( const TYPE i ) {
      return ( 0 == i ? TYPE( 1u ) : TYPE( 10u ) * pow_of_ten( i - 1 ) );
    }

    /// Implementation of relative precision
    template <typename FP, std::uint32_t PRECISION, typename TYPE>
    inline constexpr TYPE relative( const TYPE x ) {

      // int type
      using UINT = details::UINT<FP>;

      // number of mantissa bits for given type
      constexpr UINT mantissa_bits = std::numeric_limits<FP>::digits;

      // sanity check on required precision level
      static_assert( mantissa_bits >= PRECISION );
      if constexpr ( mantissa_bits == PRECISION ) {
        // no truncation...
        return x;
      } else {
        // mantissa mask
        constexpr UINT mask = ( ~UINT( 0x0u ) << ( mantissa_bits - PRECISION ) );
        // return, based on scalar or SIMD type
        if constexpr ( std::is_floating_point_v<TYPE> ) {
          return bit_cast<TYPE>( bit_cast<UINT>( x ) & mask );
        } else if constexpr ( LHCb::SIMD::is_SIMD_v<TYPE> ) {
          using namespace LHCb::Math::impl;
          using I = LHCb::SIMD::INT<UINT>;
          return union_cast<TYPE>( union_cast<I>( x ) & I( mask ) );
        }
      }
    }

    /// Implementation of abs truncation
    template <PrecisionMode MODE, ///< Precision mode
              typename FP,        ///< scalar floating point type
              typename TYPE       ///< value type
              >
    inline constexpr TYPE absolute_impl( const TYPE x, const FP dp, const FP inv_dp ) {
      // run the truncation
      if constexpr ( std::is_floating_point_v<TYPE> ) {
        using I = details::INT<TYPE>;
        if constexpr ( PrecisionMode::Absolute == MODE ) {
          return static_cast<TYPE>( static_cast<I>( x * dp ) ) * inv_dp;
        } else if constexpr ( PrecisionMode::AbsoluteRounded == MODE ) {
          return std::nearbyint( x * dp ) * inv_dp;
        }
      } else if constexpr ( LHCb::SIMD::is_SIMD_v<TYPE> ) {
        using namespace LHCb::SIMD;
        using I = LHCb::SIMD::INT<details::INT<FP>>;
        if constexpr ( PrecisionMode::Absolute == MODE ) {
          return lb_cast<TYPE>( lb_cast<I>( x * TYPE( dp ) ) ) * TYPE( inv_dp );
        } else if constexpr ( PrecisionMode::AbsoluteRounded == MODE ) {
          return lb_cast<TYPE>( lb_cast<I>( std::round( x * TYPE( dp ) ) ) ) * TYPE( inv_dp );
        }
      }
    }

    /// Implementation of absolute precision truncation
    template <typename FP,             ///< scalar floating point type
              PrecisionMode MODE,      ///< Precision mode
              std::uint32_t PRECISION, ///< Precision, as number of d.p. to keep
              typename TYPE            ///< value type
              >
    inline constexpr TYPE absolute( const TYPE x ) {

      // sanity check on required precision level
      static_assert( PRECISION <= std::numeric_limits<details::UINT<TYPE>>::digits10 );

      // scale factors for the given number of DP
      constexpr FP dp     = details::pow_of_ten( details::UINT<TYPE>( PRECISION ) );
      constexpr FP inv_dp = 1.0f / dp;

      // return the truncation
      return absolute_impl<MODE>( x, dp, inv_dp );
    }

    /// Implementation of absolute precision truncation
    template <typename FP,        ///< scalar floating point type
              PrecisionMode MODE, ///< Precision mode
              class PRECISION,    ///< precision expressed as an std::ratio
              typename TYPE,      ///< variable type
              class = typename std::enable_if_t<PRECISION::den != 0>>
    inline constexpr TYPE absolute( const TYPE x ) {

      // scale factors for the given number of DP
      constexpr FP dp     = FP( PRECISION::den ) / FP( PRECISION::num );
      constexpr FP inv_dp = 1.0f / dp;

      // run the truncation
      return absolute_impl<MODE>( x, dp, inv_dp );
    }

  } // namespace details

  /** Truncate to a given floating point value precision
   *  If relative precision is used, then PRECISION gives the number of mantissa bits to retain
   *  If absolute precision is used, then PRECISION gives the number of decimal places to retain
   */
  template <PrecisionMode MODE,      ///< Precision mode (relative or absolute)
            std::uint32_t PRECISION, ///< The truncation precision
            typename TYPE            ///< variable type
            >
  inline std::enable_if_t<std::is_floating_point_v<TYPE> || LHCb::SIMD::is_SIMD_v<TYPE>, TYPE>
  truncate( const TYPE x ) noexcept {

    if constexpr ( PrecisionMode::Absolute == MODE || PrecisionMode::AbsoluteRounded == MODE ) {

      // absolute precision truncation based on decimal places
      if constexpr ( std::is_floating_point_v<TYPE> ) {
        return details::absolute<TYPE, MODE, PRECISION>( x );
      } else if constexpr ( LHCb::SIMD::is_SIMD_v<TYPE> ) {
        //  Get the (scalar) float type
        using FP = typename TYPE::value_type;
        return details::absolute<FP, MODE, PRECISION>( x );
      }

    } else if constexpr ( PrecisionMode::Relative == MODE ) {

      // relative precision truncation based on mantissa bits
      if constexpr ( std::is_floating_point_v<TYPE> ) {
        return details::relative<TYPE, PRECISION>( x );
      } else if constexpr ( LHCb::SIMD::is_SIMD_v<TYPE> ) {
        // Get the (scalar) float type
        using FP = typename TYPE::value_type;
        return details::relative<FP, PRECISION>( x );
      }
    }
  }

  /** Truncate to a given floating point value precision expressed as a std::ratio
   *  e.g. to truncate to 0.2 use
   *  truncate< PrecisionMode::Absolute, std::ratio<2,10> >(x);
   */
  template <PrecisionMode MODE, ///< Precision mode
            class PRECISION,    ///< Precision as ratio
            typename TYPE,      ///< variable type
            class = typename std::enable_if_t<PRECISION::den != 0>>
  inline std::enable_if_t<std::is_floating_point_v<TYPE> || LHCb::SIMD::is_SIMD_v<TYPE>, TYPE>
  truncate( const TYPE x ) noexcept {

    // Absolute precision only for std::ratio version
    static_assert( PrecisionMode::Absolute == MODE || PrecisionMode::AbsoluteRounded == MODE );

    // check type
    if constexpr ( std::is_floating_point_v<TYPE> ) {
      return details::absolute<TYPE, MODE, PRECISION>( x );
    } else if constexpr ( LHCb::SIMD::is_SIMD_v<TYPE> ) {
      // Get the (scalar) float type
      using FP = typename TYPE::value_type;
      return details::absolute<FP, MODE, PRECISION>( x );
    }
  }

} // namespace LHCb::Math
