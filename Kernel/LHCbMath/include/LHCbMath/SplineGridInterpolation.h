/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// based upon the Hermite splines as derived in
//
// "Implementation of high order spline interpolations for tracking test particles
// in discretized fields"
//
// C.C.Lalescu, B.Teaca, D.Caratia
//
// Journal of Computational Physics, Volume 229, Issue 17, 20 August 2010, Pages 5862-5869
// https://doi.org/10.1016/j.jcp.2009.10.046
//
#pragma once

#include <array>
#include <cmath>
#include <functional>
#include <numeric>
#include <stdexcept>
#include <type_traits>

namespace LHCb::Math {
  namespace details {

    template <typename>
    struct PopFront_t;
    template <size_t I0, size_t... I, template <size_t...> class T>
    struct PopFront_t<T<I0, I...>> {
      using type = T<I...>;
    };

    template <typename E>
    using PopFront = typename PopFront_t<E>::type;

    template <size_t... E>
    struct Ends {
      static constexpr auto end( size_t i = 0 ) { return std::array{ E... }[i]; }
    };

    template <size_t... E>
    struct Strides {
      using defaultEnd = Ends<E...>;

      static constexpr auto dim = sizeof...( E );

      static constexpr auto stride( size_t I ) {
        if ( I >= sizeof...( E ) ) throw "arg larger than # of dimensions!";
        int s = 1;
        for ( int k = I + 1; k != sizeof...( E ); ++k ) { s *= std::array{ E... }[k]; }
        return s;
      }

      static constexpr auto index( std::array<size_t, dim> is ) {
        size_t ix = 0;
        int    i  = 0;
        for ( auto s : is ) ix += s * stride( i++ );
        return ix;
      }
    };

    template <typename Stride_t, typename End_t, typename ValueType>
    class md_slice {
      ValueType* const m_data{ nullptr };

      struct Sentinel {};

      class Iterator {
        ValueType* const m_data{ nullptr };
        size_t           m_pos{ 0 };

      public:
        constexpr explicit Iterator( ValueType* m_data, size_t pos = 0 ) : m_data{ m_data }, m_pos{ pos } {}
        Iterator& operator++() {
          constexpr auto s = Stride_t::stride( 0 );
          m_pos += s;
          return *this;
        }
        constexpr bool operator!=( Sentinel ) const {
          constexpr auto end = Stride_t::index( { End_t::end() } );
          return m_pos != end;
        }
        constexpr decltype( auto ) operator*() const {
          if constexpr ( Stride_t::dim == 1 ) {
            return m_data[m_pos];
          } else {
            return md_slice<PopFront<Stride_t>, PopFront<End_t>, ValueType>{ m_data + m_pos };
          }
        }
      };

    public:
      constexpr explicit md_slice( ValueType* ptr ) : m_data{ ptr } {} // FIXME -- ony for Iterator (and slice)...

      template <typename Container>
      constexpr explicit md_slice( Container& c ) : md_slice{ c.data() } {
        if ( c.size() < Stride_t::index( { End_t::end() } ) ) {
          throw std::out_of_range( "attempt to make a md_slice which exceeds the data size" );
        }
      }

      constexpr auto begin() const { return Iterator{ m_data }; }
      constexpr auto end() const { return Sentinel{}; }

      template <size_t... J>
      constexpr auto slice( std::array<size_t, Stride_t::dim> idx ) const {
        return md_slice<Stride_t, Ends<J...>, ValueType>( m_data + Stride_t::index( idx ) );
      }
    };

  } // namespace details

  template <size_t... I, typename Container>
  auto make_md_view( Container& c ) {
    using ValueType = std::conditional_t<std::is_const_v<Container>, std::add_const_t<typename Container::value_type>,
                                         typename Container::value_type>;
    return details::md_slice<details::Strides<I...>, details::Ends<I...>, ValueType>{ c };
  }

  template <size_t... I, typename Container>
  auto make_md_view( Container&& c ) = delete;

} // namespace LHCb::Math

namespace LHCb::Math::Spline {

  namespace details {

    template <typename T, typename... Ts>
    constexpr bool are_same = std::conjunction_v<std::is_same<T, Ts>...>;

    template <typename Array, size_t... I, size_t... J>
    auto transpose_helper( Array a, std::index_sequence<I...>, std::index_sequence<J...> ) {
      return std::array{ [&]( auto j ) { return std::array{ a[I][j]... }; }( J )... };
    }

    template <typename T, auto N, auto M>
    auto transpose( std::array<std::array<T, N>, M> a ) {
      return transpose_helper( a, std::make_index_sequence<M>{}, std::make_index_sequence<N>{} );
    }

    template <typename Array, size_t... I>
    auto permute( Array&& a, std::index_sequence<I...> ) {
      return std::array{ a[I]... };
    }

    template <size_t N, size_t... I>
    auto generate_transpose( std::index_sequence<I...> ) {
      constexpr auto M = sizeof...( I );
      return std::index_sequence<( N * I ) / M + ( N * I ) % M...>{};
    }

    template <size_t Stride, typename Array>
    auto permute( Array&& a ) {
      return permute( std::forward<Array>( a ),
                      generate_transpose<Stride>( std::make_index_sequence<std::tuple_size<Array>::value>{} ) );
    }

    template <typename Arg, size_t... I>
    constexpr auto mircat( Arg&& lhs, Arg&& rhs, std::index_sequence<I...> ) {
      constexpr auto N = sizeof...( I ) - 1;
      return std::array{ lhs[N - I]..., rhs[I]... };
    }

    template <typename Float, auto N>
    constexpr auto mircat( std::array<Float, N> lhs, std::array<Float, N> rhs ) {
      return mircat( lhs, rhs, std::make_index_sequence<N>{} );
    }

    /* clang-format off */

    // spline polynomials of the second kind
    template <int Order, int nPoints> struct Beta_n;

    template <>
    struct Beta_n<3, 4> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        return std::array{std::array{
          ( x * ( ( 4 - 3 * x ) * x + 1 ) ) / 2,
          ( ( x - 1 ) * x * x ) / 2,
        }...};
      }
    };

    template <>
    struct Beta_n<3, 6> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        static_assert( are_same<Float...> );
        return std::array{std::array{
          ( x * ( ( 5 - 4 * x ) * x + 2 ) ) / 3,
          ( x * ( x * ( 7 * x - 6 ) - 1 ) ) / 12,
          ( ( 1 - x ) * x * x ) / 12
        }...};
      }
    };

    template <>
    struct Beta_n<3, 8> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        static_assert( are_same<Float...> );
        return std::array{std::array{
          ( x * ( ( 6 - 5 * x ) * x + 3 ) ) / 4,
          ( x * ( x * ( 12 * x - 9 ) - 3 ) ) / 20,
          ( x * ( ( 7 - 8 * x ) * x + 1 ) ) / 60,
          ( ( x - 1 ) * x * x ) / 60
        }...};
      }
    };

    template <>
    struct Beta_n<5, 4> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        static_assert( are_same<Float...> );
        return std::array{std::array{
          ( x * ( x * ( x * ( x * ( 6 * x - 15 ) + 9 ) + 1 ) + 1 ) ) / 2,
          ( x * x * x * ( ( 5 - 2 * x ) * x - 3 ) ) / 2
        }...};
      }
    };

    template <>
    struct Beta_n<5, 6> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        static_assert( are_same<Float...> );
        return std::array{std::array{
          ( x * ( x * ( x * ( x * ( 25 * x - 62 ) + 33 ) + 8 ) + 8 ) ) / 12,
          ( x * ( x * ( x * ( ( 61 - 25 * x ) * x - 33 ) - 1 ) - 2 ) ) / 24,
          ( x * x * x * ( x * ( 5 * x - 12 ) + 7 ) ) / 24,
        }...};
      }
    };

    template <>
    struct Beta_n<5, 8> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        static_assert( are_same<Float...> );
        return std::array{std::array{
          ( x * ( x * ( x * ( x * ( 59 * x - 145 ) + 68 ) + 27 ) + 27 ) ) / 36,
          ( x * ( x * ( x * ( ( 93 - 39 * x ) * x - 45 ) - 3 ) - 6 ) ) / 40,
          ( x * ( x * ( x * ( x * ( 115 * x - 270 ) + 147 ) + 2 ) + 6 ) ) / 360,
          ( x * x * x * ( ( 19 - 8 * x ) * x - 11 ) ) / 180,
        }...};
      }
    };

    template <>
    struct Beta_n<7, 6> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        static_assert( are_same<Float...> );
        return std::array{ std::array{
          ( x * ( x * ( x * ( x * ( x * ( ( 245 - 70 * x ) * x - 290 ) + 113 ) - 2 ) + 8 ) + 8 ) ) / 12,
          ( x * ( x * ( x * ( x * ( x * ( x * ( 70 * x - 245 ) + 290 ) - 114 ) + 2 ) - 1 ) - 2 ) ) / 24,
          ( x * x * x * x * ( x * ( ( 49 - 14 * x ) * x - 58 ) + 23 ) ) / 24
        } ...};
      }
    };

    template <>
    struct Beta_n<7, 8> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        static_assert( are_same<Float...> );
        return std::array{std::array{
          ( x * ( x * ( x * ( x * ( x * ( ( 2180 - 623 * x ) * x - 2566 ) + 976 ) - 39 ) + 108 ) + 108 ) ) / 144,
          ( x * ( x * ( x * ( x * ( x * ( x * ( 623 * x - 2179 ) + 2565 ) - 995 ) + 40 ) - 18 ) - 36 ) ) / 240,
          ( x * ( x * ( x * ( x * ( x * ( ( 2178 - 623 * x ) * x - 2566 ) + 1010 ) - 15 ) + 4 ) + 12 ) ) / 720,
          ( x * x * x * x * ( x * ( x * ( 89 * x - 311 ) + 367 ) - 145 ) ) / 720
        }...};
      }
    };

    template <>
    struct Beta_n<9, 6> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        static_assert( are_same<Float...> );
        return std::array{std::array{
          ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( 230 * x - 1035 ) + 1770 ) - 1365 ) + 400 ) -
            2 ) - 2 ) + 8 ) + 8 ) ) / 12,
          ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( ( 1035 - 230 * x ) * x - 1770 ) + 1365 ) - 400 ) +
            1 ) + 2 ) - 1 ) - 2 ) ) / 24,
          ( x * x * x * x * x * ( x * ( x * ( x * ( 46 * x - 207 ) + 354 ) - 273 ) + 80 ) ) / 24
        }...};
      }
    };

    template <>
    struct Beta_n<9, 8> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        static_assert( are_same<Float...> );
        return std::array{std::array{
          ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( 2030 * x - 9135 ) + 15617 ) - 12030 ) + 3524 ) -
            39 ) - 39 ) + 108 ) + 108 ) ) / 144,
          ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( ( 9135 - 2030 * x ) * x - 15617 ) + 12031 ) - 3525 ) +
            20 ) + 40 ) - 18 ) - 36 ) ) / 240,
          ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( 2030 * x - 9135 ) + 15617 ) - 12032 ) + 3524 ) -
            5 ) - 15 ) + 4 ) + 12 ) ) / 720,
          ( x * x * x * x * x * ( x * ( x * ( ( 1305 - 290 * x ) * x - 2231 ) + 1719 ) - 503 ) ) / 720
        }...};
      }
    };

    template <>
    struct Beta_n<11, 8> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        static_assert( are_same<Float...> );
        return std::array{std::array{
          ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( ( 38731 - 7042 * x ) * x - 85995 ) + 96495 ) -
            54803 ) + 12617 ) + 3 ) - 39 ) - 39 ) + 108 ) + 108 ) ) / 144,
          ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( 7042 * x - 38731 ) + 85995 ) - 96495 ) +
            54803 ) - 12616 ) - 4 ) + 20 ) + 40 ) - 18 ) - 36 ) ) / 240,
          ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( ( 38731 - 7042 * x ) * x - 85995 ) + 96495 ) -
            54803 ) + 12615 ) + 3 ) - 5 ) - 15 ) + 4 ) + 12 ) ) / 720,
          ( x * x * x * x * x * x * ( x * ( x * ( x * ( x * ( 1006 * x - 5533 ) + 12285 ) - 13785 ) + 7829 ) -
            1802 ) ) / 720
        }...};
      }
    };

    template <>
    struct Beta_n<13, 8> {
      template <typename... Float>
      constexpr auto operator()( Float... x ) const {
        static_assert( are_same<Float...> );
        return std::array{std::array{
          ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( 25228 * x - 163982 ) + 447062 ) -
            655039 ) + 544705 ) - 244083 ) + 46109 ) + 3 ) + 3 ) - 39 ) - 39 ) + 108 ) + 108 ) ) / 144,
          ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( ( 163982 - 25228 * x ) * x - 447062 ) +
            655039 ) - 544705 ) + 244083 ) - 46109 ) - 2 ) - 4 ) + 20 ) + 40 ) - 18 ) - 36 ) ) / 240,
          ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( x * ( 25228 * x - 163982 ) + 447062 ) -
            655039 ) + 544705 ) - 244083 ) + 46109 ) + 1 ) + 3 ) - 5 ) - 15 ) + 4 ) + 12 ) ) / 720,
          ( x * x * x * x * x * x * x * ( x * ( x * ( x * ( x * ( ( 23426 - 3604 * x ) * x - 63866 ) + 93577 ) -
            77815 ) + 34869 ) - 6587 ) ) / 720
        }...};
      }
    };

    /* clang-format on */

    template <int Order, int NPoints>
    struct Beta {
      template <typename Float>
      constexpr auto operator()( Float x ) const {
        const auto b = Beta_n<Order, NPoints>{}( 1 - x, x );
        return mircat( b[0], b[1] );
      }

      template <typename Float>
      constexpr auto operator()( Float x, Float y ) const {
        const auto b = Beta_n<Order, NPoints>{}( 1 - x, x, 1 - y, y );
        return std::array{ mircat( b[0], b[1] ), mircat( b[2], b[3] ) };
      }

      template <typename Float>
      constexpr auto operator()( Float x, Float y, Float z ) const {
        const auto b = Beta_n<Order, NPoints>{}( 1 - x, x, 1 - y, y, 1 - z, z );
        return std::array{ mircat( b[0], b[1] ), mircat( b[2], b[3] ), mircat( b[4], b[5] ) };
      }
    };

    // std::inner_product requires Sentinel1 == InputIt1... sigh...
    template <typename InputIt1, typename Sentinel1, typename InputIt2, typename T,
              typename BinaryOperation1 = std::plus<>, typename BinaryOperation2 = std::multiplies<>>
    constexpr T inner_product( InputIt1 first1, Sentinel1 last1, InputIt2 first2, T init, BinaryOperation1 op1 = {},
                               BinaryOperation2 op2 = {} ) {
      for ( ; first1 != last1; ++first1, ++first2 ) init = op1( std::move( init ), op2( *first1, *first2 ) );
      return init;
    }

    using namespace LHCb::Math::details;

    template <size_t N, size_t... S, size_t... E, typename ValueType>
    constexpr auto localGrid( md_slice<Strides<S...>, Ends<E...>, ValueType> grid,
                              std::array<size_t, sizeof...( S )>             x ) {
      static_assert( sizeof...( S ) == sizeof...( E ), "ends and stride must have same dimension" );
      static_assert( N == 4 || N == 6 || N == 8, "invalid number of gridpoints..." );
      return grid.template slice<N, N, N>(
          std::apply( [offset = ( N - 2 ) / 2]( auto... i ) { return std::array{ i - offset... }; }, x ) );
    }
  } // namespace details

  template <int SplineOrder, int NPoints, typename Grid, typename Float>
  constexpr auto interpolate( Grid&& grid, Float x ) {
    constexpr auto beta = details::Beta<SplineOrder, NPoints>{};
    const size_t   xg   = floor( x );
    const auto     bx   = beta( x - xg );
    const auto     f    = details::localGrid<NPoints>( std::forward<Grid>( grid ), { xg } );
    using ValueType     = decltype( *f.begin() );
    return details::inner_product( bx.begin(), bx.end(), f.begin(), ValueType{} );
  }

  template <int SplineOrder, int NPoints, typename Grid, typename Float>
  constexpr auto interpolate( Grid&& grid, Float x, Float y ) {
    constexpr auto beta = details::Beta<SplineOrder, NPoints>{};
    const size_t   xg = floor( x ), yg = floor( y );
    const auto [bx, by] = beta( x - xg, y - yg );
    const auto f        = details::localGrid<NPoints>( std::forward<Grid>( grid ), { xg, yg } );
    // f(i,j)*bx(i)*by(j) -- vectorize me!
    using ValueType = decltype( *( *f.begin() ).begin() );
    return details::inner_product( bx.begin(), bx.end(), f.begin(), ValueType{}, std::plus<>{},
                                   [by = std::cref( by )]( const auto& bi, const auto& fi ) {
                                     return bi * details::inner_product( by.get().begin(), by.get().end(), fi.begin(),
                                                                         ValueType{} );
                                   } );
  }

  template <int SplineOrder, int NPoints, typename Grid, typename Float>
  constexpr auto interpolate( Grid&& grid, Float x, Float y, Float z ) {
    constexpr auto beta = details::Beta<SplineOrder, NPoints>{};
    const size_t   xg = floor( x ), yg = floor( y ), zg = floor( z );
    const auto [bx, by, bz] = beta( x - xg, y - yg, z - zg );
    const auto f            = details::localGrid<NPoints>( std::forward<Grid>( grid ), { xg, yg, zg } );
    // f(ijk)*bx(i)*by(j)*bz(k) -- vectorize me!
    using ValueType = decltype( *( *( *f.begin() ).begin() ).begin() );
    return details::inner_product(
        bx.begin(), bx.end(), f.begin(), ValueType{}, std::plus<>{},
        [by = std::cref( by ), bz = std::cref( bz )]( const auto& bi, const auto& fi ) {
          return bi * details::inner_product( by.get().begin(), by.get().end(), fi.begin(), ValueType{}, std::plus<>{},
                                              [&bz]( const auto& bj, const auto& fij ) {
                                                return bj * details::inner_product( bz.get().begin(), bz.get().end(),
                                                                                    fij.begin(), ValueType{} );
                                              } );
        } );
  }
} // namespace LHCb::Math::Spline
