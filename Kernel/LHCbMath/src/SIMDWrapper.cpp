/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "LHCbMath/SIMDWrapper.h"
namespace {
  /** Helper to check at compile time that various "POD like" traits are
   *  satisfied.
   *
   *  @todo It would be nice to add std::is_trivially_default_constructible_v
   *        and std::is_trivial_v to this list, but right now this is not
   *        satisfied because there are user-defined constructors.
   */
  template <typename pod_t>
  inline constexpr bool is_pod_like_enough_v =
      std::is_trivially_destructible_v<pod_t> && std::is_trivially_copy_constructible_v<pod_t> &&
      std::is_trivially_move_constructible_v<pod_t> && std::is_trivially_copy_assignable_v<pod_t> &&
      std::is_trivially_move_assignable_v<pod_t> && std::is_nothrow_swappable_v<pod_t>;

  /** Helper to check at compile time that the various conversion operators
   *  are defined appropriately. This should be valid both for plain scalar
   *  types and for SIMD types.
   *
   *  @todo Could also check {bool_t, float_t} -> float_v
   */
  template <typename bool_t, typename int_t, typename float_t>
  inline constexpr bool obeys_promotion_rules_v =
      std::is_same_v<std::common_type_t<bool_t, int_t>, int_t> &&
      std::is_same_v<std::common_type_t<bool_t, bool_t>, bool_t> &&
      std::is_same_v<std::common_type_t<int_t, int_t>, int_t> &&
      std::is_same_v<std::common_type_t<int_t, float_t>, float_t> &&
      std::is_same_v<std::common_type_t<float_t, float_t>, float_t> &&
      std::is_same_v<std::invoke_result_t<std::plus<>, bool_t, bool_t>, int_t> && is_pod_like_enough_v<bool_t> &&
      is_pod_like_enough_v<int_t> && is_pod_like_enough_v<float_t>;
  static_assert( obeys_promotion_rules_v<bool, int, float> );

  /** Helper to check, at compile time, the behaviour of the SIMD types. This
   *  adds some extra checks that are not valid for plain scalar types but
   *  which should be valid for the SIMD types.
   */
  template <typename bool_t, typename int_t, typename float_t>
  inline constexpr bool check_simd_traits_v =
      obeys_promotion_rules_v<bool_t, int_t, float_t> && !std::is_convertible_v<bool_t, bool> &&
      !std::is_convertible_v<int_t, int> && !std::is_convertible_v<float_t, float>;
} // namespace

namespace SIMDWrapper {
  static_assert( check_simd_traits_v<scalar::mask_v, scalar::int_v, scalar::float_v> );
  static_assert( check_simd_traits_v<sse::mask_v, sse::int_v, sse::float_v> );
  static_assert( check_simd_traits_v<avx2::mask_v, avx2::int_v, avx2::float_v> );
  static_assert( check_simd_traits_v<avx256::mask_v, avx256::int_v, avx256::float_v> );
  static_assert( check_simd_traits_v<avx512::mask_v, avx512::int_v, avx512::float_v> );
  static_assert( check_simd_traits_v<neon::mask_v, neon::int_v, neon::float_v> );
  InstructionSet type_map<InstructionSet::Scalar>::stackInstructionSet() { return scalar::instructionSet(); }
  InstructionSet type_map<InstructionSet::SSE>::stackInstructionSet() { return sse::instructionSet(); }
  InstructionSet type_map<InstructionSet::AVX2>::stackInstructionSet() { return avx2::instructionSet(); }
  InstructionSet type_map<InstructionSet::AVX256>::stackInstructionSet() { return avx256::instructionSet(); }
  InstructionSet type_map<InstructionSet::AVX512>::stackInstructionSet() { return avx512::instructionSet(); }
  InstructionSet type_map<InstructionSet::Neon>::stackInstructionSet() { return neon::instructionSet(); }
  InstructionSet type_map<InstructionSet::Best>::stackInstructionSet() { return best::instructionSet(); }
} // namespace SIMDWrapper
