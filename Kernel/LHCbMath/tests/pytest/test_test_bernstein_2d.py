###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["../test_B2D.py"]

    def test_stderr(self, stderr: bytes):
        # Varying number of error lines.
        pytest.skip()

    def test_stdout(self, stdout: bytes):
        expected_block = b"""Check for constant 2D-polynomial      is OK
Check for symmetric 2D-polynomial(x2) is OK
Check for integrals[0]                is OK
Check for integrals[1]                is OK
Check for integrals[2]                is OK
Check for integrals[3]                is OK
Check for integrals[4]                is OK
Check for integrals[5]                is OK
Check for integrals[6]                is OK
"""

        assert expected_block in stdout, (
            f"Expected block missing in standard output: {expected_block}"
        )
