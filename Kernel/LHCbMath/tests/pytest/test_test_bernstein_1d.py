###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["../test_B1D.py"]

    def test_stdout(self, stdout: bytes):
        expected_block = b"""Check for constant polynomial         is OK
Check for random polynomial           is OK
Check for Bernstein interpolant       is OK
Check for Bernstein roots             is OK
Check for Bernstein integrals         is OK
Check for Bernstein elevate/reduce    is OK
Check for Bernstein quotient/reminder is OK
Check for BernsteinEven               is OK
Check for Positive constant           is OK
Check for Positive                    is OK
Check for PositiveEven                is OK
Check for Monothonic(x2)              is OK
Check for Convex(x4)                  is OK
Check for ConvexOnly(x2)              is OK
"""

        assert expected_block in stdout, (
            f"Expected block missing in standard output: {expected_block}"
        )
