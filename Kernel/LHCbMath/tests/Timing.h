/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <time.h>

decltype( auto ) time_diff( struct timespec* start, struct timespec* stop ) {
  constexpr auto scale     = 1000000000ULL;
  const auto     diff_nsec = stop->tv_nsec - start->tv_nsec;
  const auto     diff_sec  = stop->tv_sec - start->tv_sec;
  return ( diff_nsec < 0 //
               ? ( scale * ( diff_sec - 1 ) ) + diff_nsec + scale
               : ( scale * diff_sec ) + diff_nsec );
}
