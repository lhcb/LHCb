/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <RootCnv/RootAddress.h>
#include <RootCnv/RootRefs.h>

#include <Kernel/IOHandler.h>

#include <Gaudi/Parsers/Factory.h>
#include <GaudiKernel/DataIncident.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/IDataManagerSvc.h>
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/IService.h>
#include <GaudiKernel/LinkManager.h>
#include <GaudiKernel/SmartIF.h>
#include <GaudiUtils/IIODataManager.h>
#include <RootCnv/RootAddress.h>
#include <RootCnv/RootDataConnection.h>

#include <TFile.h>
#include <TLeaf.h>
#include <TTree.h>

#include <fmt/format.h>

#include <array>
#include <assert.h>
#include <mutex>
#include <stdexcept>
#include <string>
#include <tuple>
#include <type_traits>
#include <vector>

// Functions imported from GaudiKernel
namespace GaudiRoot {
  void popCurrentDataObject();
  void pushCurrentDataObject( DataObject** pobjAddr );
} // namespace GaudiRoot

namespace LHCb::IO {

  namespace details {

    /// reset a vector of pointers to only null pointers
    template <typename V>
    void reset( V& v ) {
      for ( auto& e : v ) e = nullptr;
    }

    /// dedicated exception for RootFile constructor
    struct RootFileFailed : std::runtime_error {
      using runtime_error::runtime_error;
    };

    /**
     * Small wrapper around Root TFile, storing also common references used by all events
     * On creation, the ROOT file is opened and the Refs tree if parsed, filling dbs, links, conts and params.
     */
    struct RootFile {
      RootFile( IInterface const&, std::string const&, IMessageSvc&, IIncidentSvc&, Gaudi::IIODataManager& );
      RootFile( RootFile const& )            = delete;
      RootFile( RootFile&& )                 = delete;
      RootFile& operator=( RootFile const& ) = delete;
      RootFile& operator=( RootFile&& )      = delete;
      ~RootFile();
      /// Access to underlying TFile
      TFile& file() { return *m_curConnection->file(); }
      /// connection to current input, only valid if connected, ownership is not ours, but IODataMgr
      Gaudi::RootDataConnection* m_curConnection{};
      /// IOManager to be used for disconnecting
      Gaudi::IIODataManager& m_ioMgr;
      /// Map containing internal links names for current file
      std::vector<std::string> m_links;
      /// whether we've read all data from this file
      bool m_fullyRead{ false };
    };

    /**
     * Buffer of vector of objects owned by ROOT, each vector corresponding to one event
     * The buffer only deals with pointers to the objects and with the thread safe
     * dispatching of objects to workers via the get method
     */
    class Buffer {
    public:
      using EventPtrVector    = std::vector<DataObject*>;
      using EventRefPtrVector = std::vector<Gaudi::RootObjectRefs*>;
      using EventType         = std::pair<EventPtrVector, unsigned int>;
      using TESEventType      = EventType;

      Buffer( std::vector<EventPtrVector>&& eventData, std::vector<EventRefPtrVector>&& eventRefs,
              std::shared_ptr<RootFile> file, IIncidentSvc* incidentSvc )
          : m_eventData( std::move( eventData ) )
          , m_eventRefs( std::move( eventRefs ) )
          , m_file( file )
          , m_nbAvailableEvents( m_eventData.size() )
          , m_incidentSvc( incidentSvc ) {}
      Buffer( Buffer const& other ) = delete;
      Buffer( Buffer&& other )      = delete;

      /// returns number of events in buffer
      unsigned int size() { return m_eventData.size(); }
      /**
       * get the next event in the Buffer. This method is thread safe
       * and guarantees to return every single tuple exactly once
       * @returns a tuple of objects associated to a given event or nothing when the buffer is empty
       */
      std::optional<EventType> get( EventContext const& ) {
        /// Atomically returns a unique eventID or <= 0 number if no events remain
        int evtId = m_nbAvailableEvents--;
        /// no event remains
        if ( evtId <= 0 ) return {};
        /// fill references for all objects of this event
        auto evtNb = size() - evtId;
        fillAllLinks( evtNb );
        // Make the rest of system aware of which file this event uses
        // This is in particular needed by the XMLSummarySvc
        m_incidentSvc->fireIncident( Incident( m_file->m_curConnection->name(), "EventFromFile" ) );
        /// get the event we've picked
        return std::pair{ std::move( m_eventData[evtNb] ), (unsigned int)evtNb };
      }

    protected:
      /**
       * fill all references for event evtNb. May throw in case of error
       * the implementation if the standard recursive implementation on number of template arguments
       */
      void fillAllLinks( unsigned int evtNb );

      /// accessor to Root file links
      std::string getLink( int which ) const {
        return ( ( which >= 0 ) && ( size_t( which ) < m_file->m_links.size() ) ) ? m_file->m_links[which]
                                                                                  : std::string{};
      }

      /// vector of tuple of pointers to Objects associated to each event residing in the ROOT File
      const std::vector<EventPtrVector> m_eventData;
      /// vector of tuple of pointers to cross References for Objects associated to each event residing in the ROOT File
      const std::vector<EventRefPtrVector> m_eventRefs;
      /// shared pointer to the owning ROOT File object
      const std::shared_ptr<RootFile> m_file;
      /// number of events still available in current buffer
      std::atomic<int> m_nbAvailableEvents;
      /// incident service to be used
      IIncidentSvc* m_incidentSvc{ nullptr };
    };

    /**
     * Input Handler to be used in an LHCb::IO::IOHandler and dealing with access to a set
     * of ROOT files.
     */
    struct InputHandler {
      struct SeekError {};
      struct ReadError {};

      template <typename Owner>
      InputHandler( Owner& owner, std::vector<std::string> const& input ) : m_input( input ) {
        // Retrieve conversion service handling event iteration
        SmartIF<IService> service = owner.service( "Gaudi::IODataManager/IODataManager" );
        m_ioMgr                   = service.as<Gaudi::IIODataManager>();
        if ( !m_ioMgr ) {
          throw GaudiException{ "IOHandlerFileRead",
                                "Unable to localize interface IID_IIODataManager from service IODataManager",
                                StatusCode::FAILURE };
        }
      }

      virtual ~InputHandler() { m_ioMgr.reset(); }
      InputHandler( InputHandler const& )            = delete;
      InputHandler( InputHandler&& )                 = delete;
      InputHandler& operator=( InputHandler const& ) = delete;
      InputHandler& operator=( InputHandler&& )      = delete;

      /**
       * Connects to first input already and try to prefetch data
       * Catched the EndOfInput in case no data ia available in order
       * to not fail at initialize. The failure will be reported on first
       * attempt to read data
       */
      template <typename Owner>
      void initialize( Owner& owner, IIncidentSvc* incidentSvc, std::string& eventTreeName,
                       std::vector<std::string> const& eventBranches, bool allowMissingInput, bool storeOldFSRs,
                       int evtPrintFrequency ) {
        m_incidentSvc       = incidentSvc;
        m_eventTreeName     = eventTreeName;
        m_eventBranches     = eventBranches;
        m_allowMissingInput = allowMissingInput;
        // setup internal vectors dealing with branches and outputs
        m_curBranch.resize( m_eventBranches.size(), nullptr );
        m_curRefBranch.resize( m_eventBranches.size(), nullptr );
        m_eventPlaceHolder.resize( m_eventBranches.size(), nullptr );
        m_eventRefPlaceHolder.resize( m_eventBranches.size(), nullptr );
        m_storeOldFSRs      = storeOldFSRs;
        m_evtPrintFrequency = evtPrintFrequency;
        // connect to ROOT file
        connectToCurrentInput( owner );
      }

      void finalize() {
        // close currently opened file if any
        m_curFile.reset();
      }

      /**
       * rewinds to the begining of input
       */
      template <typename Owner>
      void rewind( Owner& owner ) {
        m_curInput     = 0;
        m_curTreeEntry = 0;
        m_curTree      = nullptr;
        m_curFile.reset();
        connectToCurrentInput( owner );
      }

      /**
       * skips next n events without reading them (only headers will be actually read)
       * @throws SeekError
       * @throws IOHandler::EndOfInput
       */
      template <typename Owner>
      void skip( Owner& owner, unsigned int );

      /**
       * prefetches a number of events from input, allocating a Buffer object to store them and returning it
       * It will try to fill the buffer unless input is empty
       * @param bufferSize the size of the buffer to allocate
       */
      template <typename Owner>
      std::shared_ptr<Buffer> prefetchEvents( Owner const&, unsigned int nbEvents );

      /**
       * connects to current input
       * @throws IOHandler::EndOfInput
       */
      template <typename Owner>
      void connectToCurrentInput( Owner& owner );

      /**
       * set all branches addresses
       * throws a std::logic_error in case on of the branches is not found
       */
      virtual void setAllBranchAddresses();

      /**
       * reads (old) FSRs from ROOT File and store them to TES
       * Should be removed once new FSRs are in place
       */
      template <typename Owner>
      void storeOldFSRsToTES( Owner& );

      /// Incident service. FIXME : drop whenever FSRSink is not depending on this anymore
      IIncidentSvc* m_incidentSvc;
      /// Reference to file manager service
      SmartIF<Gaudi::IIODataManager> m_ioMgr;
      /// vector of input files
      std::vector<std::string> const m_input;
      /// currently used input as an index in m_input
      unsigned int m_curInput{ 0 };
      /// Current ROOT file
      std::shared_ptr<RootFile> m_curFile{ nullptr };
      /// TTree associated to m_curFile
      TTree* m_curTree{ nullptr };
      /// position of next entry to read in current tree
      unsigned int m_curTreeEntry{ 0 };
      /// TBranchs associated to m_curTree for event data
      std::vector<TBranch*> m_curBranch{};
      /// TBranchs associated to m_curTree for refs in event data
      std::vector<TBranch*> m_curRefBranch{};
      /// Placeholder for event data retrieval, needed by ROOT
      typename Buffer::EventPtrVector m_eventPlaceHolder{};
      /// Placeholder for event refs retrieval, needed by ROOT
      typename Buffer::EventRefPtrVector m_eventRefPlaceHolder{};
      /// whether old FSRs should be stored. FIXME : to be dropped when old FSRs are gone
      bool m_storeOldFSRs{};
      // Frequency for printing info on events processed
      int m_evtPrintFrequency{ 1000 };

      /// Name of the tree containing Events in the Root files
      std::string m_eventTreeName;
      /// Name(s) of the branch(es) containing Events in the Root files
      std::vector<std::string> m_eventBranches;
      /// allows to ignore missing inputs. Should never be used and should even be dropped,
      /// but was needed for the transition period form FetchDataFromFile to IOAlg
      bool m_allowMissingInput{ false };
    };

    // forward declaration of InputHandlerExt
    struct InputHandlerExt;

    /**
     * Extended version of Buffer, allowing to read all branches from
     * the input file and fill the TES with them. See InputHandlerExt for details.
     * This should only be used in conjunction with CopyInputStream and should be
     * dropped when CopyInputStream is replaced. FIXME
     */
    struct BufferExt : Buffer {
      /// vector of tuple of pointers to Objects associated to each event residing in the ROOT File, extra branches
      const std::vector<EventPtrVector> m_eventDataExt;
      /// vector of tuple of pointers to cross References for Objects associated to each event residing in the ROOT
      /// File, extra branches
      const std::vector<EventRefPtrVector> m_eventRefsExt;
      /// back pointer to the inputHandler
      InputHandlerExt const& m_inputHandler;

      BufferExt( std::vector<EventPtrVector>&& eventData, std::vector<EventRefPtrVector>&& eventRefs,
                 std::vector<EventPtrVector>&& eventDataExt, std::vector<EventRefPtrVector>&& eventRefsExt,
                 std::shared_ptr<RootFile> file, InputHandlerExt const& inputHandler, IIncidentSvc* incidentSvc )
          : Buffer( std::move( eventData ), std::move( eventRefs ), file, incidentSvc )
          , m_eventDataExt( std::move( eventDataExt ) )
          , m_eventRefsExt( std::move( eventRefsExt ) )
          , m_inputHandler( inputHandler ) {}

      /**
       * get the next event in the Buffer. This method is thread safe
       * and guarantees to return every single tuple exactly once
       * @returns a tuple of objects associated to a given event or nothing when the buffer is empty
       */
      std::optional<EventType> get( EventContext const& ) {
        /// Atomically returns a unique eventID or <= 0 number if no events remain
        int evtId = m_nbAvailableEvents--;
        /// no event remains
        if ( evtId <= 0 ) return {};
        /// fill references for all objects of this event
        auto evtNb = size() - evtId;
        fillAllLinks( evtNb );
        fillAllExtraLinks( evtNb );
        // Make the rest of system aware of which file this event uses
        // This is in particular needed by the XMLSummarySvc
        m_incidentSvc->fireIncident( Incident( m_file->m_curConnection->name(), "EventFromFile" ) );
        /// get the event we've picked
        return std::pair{ std::move( m_eventData[evtNb] ), evtNb };
      }
      /**
       * fill all extra references for event evtNb. May throw in case of error
       * the implementation if the standard recursive implementation on number of template arguments
       */
      void fillAllExtraLinks( unsigned int evtNb );
      /**
       * stores extra data in the transient store as RootIOAlg will only deal with
       * "official" outputs, not the rest of it.
       * This should only be used in conjunction with CopyInputStream and should be
       * dropped when CopyInputStream is replaced. FIXME
       */
      void fillExtraData( IDataProviderSvc&, unsigned int, std::vector<DataObject*>&, std::vector<std::string> const& );
    };

    /**
     * Extended version of InputHandler, allowing to read all branches from
     * the input file and fill the TES with them. However only branches present
     * in m_eventBranches will be outputs for the RootIOAlg and have DataHandles
     * associated.
     * This should only be used in conjunction with CopyInputStream and should be
     * dropped when CopyInputStream is replaced. FIXME
     */
    struct InputHandlerExt : InputHandler {
      using InputHandler::InputHandler;

      /**
       * prefetches a number of events from input, allocating a Buffer object to store them and returning it
       * It will try to fill the buffer unless input is empty
       * @param bufferSize the size of the buffer to allocate
       */
      template <typename Owner>
      std::shared_ptr<BufferExt> prefetchEvents( Owner const&, unsigned int nbEvents );

      /**
       * set all branches addresses
       * throws a std::logic_error in case on of the branches is not found
       */
      void setAllBranchAddresses() override;

      /// Name(s) of extra branch(es) containing Events in the Root files
      std::vector<std::string> m_eventBranchesExt;
      /// Paths in the TEST for extra branch(es) containing Events in the Root files
      std::vector<std::string> m_eventPathsExt;
      /// TBranchs associated to m_curTree for event data, extra branches
      std::vector<TBranch*> m_curBranchExt{};
      /// TBranchs associated to m_curTree for refs in event data, extra branches
      std::vector<TBranch*> m_curRefBranchExt{};
      /// Placeholder for event data retrieval, needed by ROOT, extra branches
      typename Buffer::EventPtrVector m_eventPlaceHolderExt{};
      /// Placeholder for event refs retrieval, needed by ROOT, extra branches
      typename Buffer::EventRefPtrVector m_eventRefPlaceHolderExt{};
    };

  } // namespace details
} // namespace LHCb::IO

namespace LHCb::IO {
  /**
   * base class for IOHandlers working with ROOT files
   * templated by the set of EventData to be delivered
   *
   * FIXME : the template argument should be dropped once CopyInputStream
   * is fixed. See comment of InputHandlerExt
   */
  template <bool ReadAllBranches>
  using RootIOHandler =
      typename std::conditional_t<ReadAllBranches, LHCb::IO::IOHandler<details::BufferExt, details::InputHandlerExt>,
                                  LHCb::IO::IOHandler<details::Buffer, details::InputHandler>>;

} // namespace LHCb::IO

template <typename Owner>
std::shared_ptr<LHCb::IO::details::Buffer>
LHCb::IO::details::InputHandler::prefetchEvents( Owner const& owner, unsigned int nbEventsToFetch ) {
  // Check whether we need to open the current file
  if ( !m_curFile ) connectToCurrentInput( owner );
  // create events and refs vector plus reserve space
  std::vector<typename Buffer::EventPtrVector> events;
  events.reserve( nbEventsToFetch );
  std::vector<typename Buffer::EventRefPtrVector> refs;
  refs.reserve( nbEventsToFetch );
  // Fill buffer with events from ROOT
  for ( unsigned int n = 0; n < nbEventsToFetch && m_curTreeEntry < m_curTree->GetEntriesFast();
        n++, m_curTreeEntry++ ) {
    // log event number if matching the printFrequency
    if ( m_evtPrintFrequency > 0 && ( m_curTreeEntry % m_evtPrintFrequency == 0 ) ) {
      owner.always() << "Reading Event record " << m_curTreeEntry + 1 << " within "
                     << m_curFile->m_curConnection->name() << endmsg;
    }
    // Note that we need to get branches manually one by one in order to play with pushCurrentDataObject
    // and ensure the reading of SmartRefs and ContainedObject is done properly
    // These are indeed using a special streamer in the back relying on the global environment
    // to know their parent (see RootIOStreamer.h and IOHandler in RootCnv/RootIOHandler.cpp)
    int nbBytes = 0;
    for ( unsigned int n = 0; n < m_eventBranches.size(); n++ ) {
      // This if looks weird, as we should have all branches requested. Practically it's not the case
      // and I copy here the explanation given in the setAllBranchAddresses method :
      //   Silently ignore missing branch, hoping nobody will ever read that
      //   This was default behavior in FetchDataFromFile and was abused for
      //   handling some backward compatibility issues, basically pretending
      //   that we would read both versions and never accessing he wrong one
      if ( m_curBranch[n] ) {
        GaudiRoot::pushCurrentDataObject( &m_eventPlaceHolder[n] );
        nbBytes += m_curBranch[n]->GetEntry( m_curTreeEntry );
        nbBytes += m_curRefBranch[n]->GetEntry( m_curTreeEntry );
        GaudiRoot::popCurrentDataObject();
      }
    }
    if ( nbBytes < 0 ) {
      // Entry does not exist or I/O error
      owner.error() << "I/O error while prefetching " << nbEventsToFetch << " events, giving up after " << n << endmsg;
      break;
    }
    events.push_back( m_eventPlaceHolder );
    refs.push_back( m_eventRefPlaceHolder );
    reset( m_eventPlaceHolder );    // reset all pointers to nullptr, important for ROOT !
    reset( m_eventRefPlaceHolder ); // reset all pointers to nullptr, important for ROOT !
    // Not reseting to nullptr here would lead to ROOT overwriting our event data
    // behind our back when reading the next event
  }
  auto retBuffer = std::make_shared<Buffer>( std::move( events ), std::move( refs ), m_curFile, m_incidentSvc );
  // the check for m_curTree tackles the case of the bootstraping after a rewind of the IOHandler
  if ( m_curTree && m_curTreeEntry >= m_curTree->GetEntriesFast() ) {
    // we've exhausted the current file here. The buffer may thus not be as full
    // as expected. Not a problem, we do not mix data from multiple file in a given buffer
    // Anyway move to next file
    m_curFile->m_fullyRead = true;
    m_curInput++;
    m_curFile.reset();
  }
  return retBuffer;
}

template <typename Owner>
std::shared_ptr<LHCb::IO::details::BufferExt>
LHCb::IO::details::InputHandlerExt::prefetchEvents( Owner const& owner, unsigned int nbEventsToFetch ) {
  // Check whether we need to open the current file
  if ( !m_curFile ) connectToCurrentInput( owner );
  // create events and refs vector plus reserve space
  std::vector<typename Buffer::EventPtrVector> events;
  events.reserve( nbEventsToFetch );
  std::vector<typename Buffer::EventRefPtrVector> refs;
  refs.reserve( nbEventsToFetch );
  std::vector<typename Buffer::EventPtrVector> eventsExt;
  eventsExt.reserve( nbEventsToFetch );
  std::vector<typename Buffer::EventRefPtrVector> refsExt;
  refsExt.reserve( nbEventsToFetch );
  // Fill buffer with events from ROOT
  for ( unsigned int n = 0; n < nbEventsToFetch && m_curTreeEntry < m_curTree->GetEntriesFast();
        n++, m_curTreeEntry++ ) {
    // log event number if matching the printFrequency
    if ( m_evtPrintFrequency > 0 && ( m_curTreeEntry % m_evtPrintFrequency == 0 ) ) {
      owner.always() << "Reading Event record " << m_curTreeEntry + 1 << " within "
                     << m_curFile->m_curConnection->name() << endmsg;
    }
    // Note that we need to get branches manually one by one in order to play with pushCurrentDataObject
    // and ensure the reading of SmartRefs and ContainedObject is done properly
    // These are indeed using a special streamer in the back relying on the global environment
    // to know their parent (see RootIOStreamer.h and IOHandler in RootCnv/RootIOHandler.cpp)
    int nbBytes = 0;
    for ( unsigned int n = 0; n < m_eventBranches.size(); n++ ) {
      // This if looks weird, as we should have all branches requested. Practically it's not the case
      // and I copy here the explanation given in the setAllBranchAddresses method :
      //   Silently ignore missing branch, hoping nobody will ever read that
      //   This was default behavior in FetchDataFromFile and was abused for
      //   handling some backward compatibility issues, basically pretending
      //   that we would read both versions and never accessing he wrong one
      if ( m_curBranch[n] ) {
        GaudiRoot::pushCurrentDataObject( &m_eventPlaceHolder[n] );
        nbBytes += m_curBranch[n]->GetEntry( m_curTreeEntry );
        nbBytes += m_curRefBranch[n]->GetEntry( m_curTreeEntry );
        GaudiRoot::popCurrentDataObject();
      }
    }
    for ( unsigned int n = 0; n < m_eventBranchesExt.size(); n++ ) {
      // This if looks weird, as we should have all branches initially listed.
      // Practically some may be empty directories and will be listed by root
      // but no really map to a branch. We thus ignore them
      if ( m_curBranchExt[n] ) {
        GaudiRoot::pushCurrentDataObject( &m_eventPlaceHolderExt[n] );
        nbBytes += m_curBranchExt[n]->GetEntry( m_curTreeEntry );
        nbBytes += m_curRefBranchExt[n]->GetEntry( m_curTreeEntry );
        GaudiRoot::popCurrentDataObject();
      }
    }
    if ( nbBytes <= 0 ) {
      // Entry does not exist or I/O error
      owner.error() << "I/O error while prefetching " << nbEventsToFetch << " events, giving up after " << n << endmsg;
      break;
    }
    events.push_back( m_eventPlaceHolder );
    refs.push_back( m_eventRefPlaceHolder );
    eventsExt.push_back( m_eventPlaceHolderExt );
    refsExt.push_back( m_eventRefPlaceHolderExt );
    reset( m_eventPlaceHolder );       // reset all pointers to nullptr, important for ROOT !
    reset( m_eventRefPlaceHolder );    // reset all pointers to nullptr, important for ROOT !
    reset( m_eventPlaceHolderExt );    // reset all pointers to nullptr, important for ROOT !
    reset( m_eventRefPlaceHolderExt ); // reset all pointers to nullptr, important for ROOT !
    // Not reseting to nullptr here would lead to ROOT overwriting our event data
    // behind our back when reading the next event
  }
  auto retBuffer = std::make_shared<BufferExt>( std::move( events ), std::move( refs ), std::move( eventsExt ),
                                                std::move( refsExt ), m_curFile, *this, m_incidentSvc );
  // the check for m_curTree tackles the case of the bootstraping after a rewind of the IOHandler
  if ( m_curTree && m_curTreeEntry >= m_curTree->GetEntriesFast() ) {
    // we've exhausted the current file here. The buffer may thus not be as full
    // as expected. Not a problem, we do not mix data from multiple file in a given buffer
    // Anyway move to next file
    m_curInput++;
    m_curFile.reset();
  }
  return retBuffer;
}

template <typename Owner>
void LHCb::IO::details::InputHandler::connectToCurrentInput( Owner& owner ) {
  while ( m_curInput < m_input.size() ) {
    // open next file
    try {
      owner.info() << "DATAFILE='" << m_input[m_curInput] << "'" << endmsg;
      m_curFile = std::make_shared<RootFile>( owner, m_input[m_curInput], *owner.msgSvc(), *m_incidentSvc, *m_ioMgr );
    } catch ( RootFileFailed const& e ) {
      owner.error() << e.what() << endmsg;
      m_curInput++;
      continue;
    }
    // find tree containing event data
    m_curTree = m_curFile->file().Get<TTree>( m_eventTreeName.c_str() );
    if ( !m_curTree ) {
      owner.info() << "Could not find TTree '" << m_eventTreeName << "' in file '" << m_input[m_curInput]
                   << "', file has no events" << endmsg;
      // Deal with FSRs
      if ( m_storeOldFSRs ) { storeOldFSRsToTES( owner ); }
      // and go to next file
      m_curInput++;
      continue;
    }
    // and all event data + event reference branches
    try {
      setAllBranchAddresses();
    } catch ( std::logic_error const& e ) {
      owner.error() << "could not find branch '" << e.what() << "' in file '" << m_input[m_curInput]
                    << "', skipping this file" << endmsg;
      m_curInput++;
      continue;
    }
    m_curTreeEntry = 0;
    // Deal with FSRs
    if ( m_storeOldFSRs ) { storeOldFSRsToTES( owner ); }
    return;
  }
  throw LHCb::IO::EndOfInput();
}

template <typename Owner>
void LHCb::IO::details::InputHandler::storeOldFSRsToTES( Owner& owner ) {
  /*
   * Code essentially extracted from RootCnvSvc with hardcoded "FileRecords"
   * FIXME Should be dropped when old FSRs are gone
   */
  auto*       pc         = m_curFile->m_curConnection;
  std::string fid        = pc->fid();
  std::string recordName = "/FileRecords";
  TBranch*    b          = pc->getBranch( "FileRecords", recordName );
  if ( b ) {
    for ( int i = 0; i < b->GetEntries(); ++i ) {
      if ( !pc->mergeFIDs().empty() ) fid = pc->mergeFIDs()[i];
      IOpaqueAddress* pAddr =
          new Gaudi::RootAddress( ROOT_StorageType, CLID_DataObject, fid, recordName, (unsigned long)( pc ), i );
      m_incidentSvc->fireIncident( ContextIncident<IOpaqueAddress*>( fid, "FILE_OPEN_READ", pAddr ) );
    }
  } else {
    if ( owner.msgLevel( MSG::VERBOSE ) ) {
      owner.verbose() << "No valid Records " << recordName << " present in:" << pc->fid() << endmsg;
    }
  }
}

template <typename Owner>
void LHCb::IO::details::InputHandler::skip( Owner& owner, unsigned int numEvt ) {
  unsigned int nbEvtToSkip = numEvt;
  while ( nbEvtToSkip > 0 ) {
    // open current file if needed
    if ( !m_curFile ) connectToCurrentInput( owner );
    // skip what we can in current file
    m_curTreeEntry += nbEvtToSkip;
    if ( m_curTreeEntry < m_curTree->GetEntriesFast() ) {
      // that was enough, done
      return;
    }
    // not enough events in current file, go to next and update nbEvtToSkip
    nbEvtToSkip = m_curTreeEntry - m_curTree->GetEntriesFast();
    m_curInput++;
    m_curFile.reset();
  }
}
