###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["../scripts/newparts.py"]

    def test_stdout(self, stdout: bytes):
        expected_block = b"""
ApplicationMgr       INFO Application Manager Initialized successfully
LHCb::ParticleP...   INFO Property triggers the update of internal Particle Property Data :  'Particles':[ 'chi_c1(1P) 0 20443 0.0 3.51067         7.835857e-22 chi_c1 20443 0 ' ]
LHCb::ParticleP...SUCCESS  New/updated particles (from "Particles" property)
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 | #    |        Name       |     PdgID    |   Q  |        Mass       |    (c*)Tau/Gamma  |  MaxWidth  |        EvtGen        |  PythiaID  |     Antiparticle     |
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 | 582  | chi_c1(1P)        |        20443 |   0  |       3.51067 GeV |     839.99989 keV |      0     |        chi_c1        |    20443   |        self-cc       |
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------

LHCb::ParticleP...   INFO Property triggers the update of internal Particle Property Data :  'Particles':[ 'chi_c1(1P) 0 20443 0.0 3.51067   -0.000839999886587 chi_c1 20443 0 ' ]
LHCb::ParticleP...SUCCESS  New/updated particles (from "Particles" property)
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 | #    |        Name       |     PdgID    |   Q  |        Mass       |    (c*)Tau/Gamma  |  MaxWidth  |        EvtGen        |  PythiaID  |     Antiparticle     |
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 | 582  | chi_c1(1P)        |        20443 |   0  |       3.51067 GeV |     839.99989 keV |      0     |        chi_c1        |    20443   |        self-cc       |
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
"""

        assert expected_block in stdout, (
            f"Expected block missing in standard output: {expected_block}"
        )
