###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import RegexpReplacer


class Test(LHCbExeTest):
    command = ["../scripts/pids.py"]
    reference = "../refs/partprop_pids.yaml"
    # Ignore 'L' suffix on numbers; these are `long` types in Python 3, which are just `int` types in Python 3
    preprocessor = LHCbExeTest.preprocessor + RegexpReplacer(
        orig=r"([0-9])L", repl=r"\1"
    )
