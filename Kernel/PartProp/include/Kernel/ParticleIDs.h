/*****************************************************************************\
* (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LHCbKernel_ParticleIDs_H
#define LHCbKernel_ParticleIDs_H 1
// ============================================================================
// Include files
// ============================================================================
#include "Kernel/ParticleID.h"
/**
 * @file ParticleIDs.h
 * @brief Particle IDs as consexpr values for common particles from PDG
 *
 */
namespace LHCb {
  namespace ParticleIDs {
    // Leptons
    constexpr auto electron              = ParticleID{ 11 };
    constexpr auto positron              = ParticleID{ -11 };
    constexpr auto muon_minus            = ParticleID{ 13 };
    constexpr auto muon_plus             = ParticleID{ -13 };
    constexpr auto tau_minus             = ParticleID{ 15 };
    constexpr auto tau_plus              = ParticleID{ -15 };
    constexpr auto electron_neutrino     = ParticleID{ 12 };
    constexpr auto electron_antineutrino = ParticleID{ -12 };
    constexpr auto muon_neutrino         = ParticleID{ 14 };
    constexpr auto muon_antineutrino     = ParticleID{ -14 };
    constexpr auto tau_neutrino          = ParticleID{ 16 };
    constexpr auto tau_antineutrino      = ParticleID{ -16 };

    // Mesons
    constexpr auto pion_plus    = ParticleID{ 211 };
    constexpr auto pion_minus   = ParticleID{ -211 };
    constexpr auto pion_neutral = ParticleID{ 111 };
    constexpr auto kaon_plus    = ParticleID{ 321 };
    constexpr auto kaon_minus   = ParticleID{ -321 };
    constexpr auto kaon_short   = ParticleID{ 310 };
    constexpr auto kaon_long    = ParticleID{ 130 };
    constexpr auto kaon_star    = ParticleID{ 323 };
    constexpr auto phi          = ParticleID{ 333 };
    constexpr auto rho          = ParticleID{ 113 };
    constexpr auto eta          = ParticleID{ 221 };
    constexpr auto eta_prime    = ParticleID{ 331 };

    // Charm Mesons
    constexpr auto d_zero      = ParticleID{ 421 };
    constexpr auto anti_d_zero = ParticleID{ -421 };
    constexpr auto d_plus      = ParticleID{ 411 };
    constexpr auto d_minus     = ParticleID{ -411 };
    constexpr auto d_s_plus    = ParticleID{ 431 };
    constexpr auto d_s_minus   = ParticleID{ -431 };

    // Bottom Mesons
    constexpr auto b_zero        = ParticleID{ 511 };
    constexpr auto anti_b_zero   = ParticleID{ -511 };
    constexpr auto b_plus        = ParticleID{ 521 };
    constexpr auto b_minus       = ParticleID{ -521 };
    constexpr auto b_s_zero      = ParticleID{ 531 };
    constexpr auto anti_b_s_zero = ParticleID{ -531 };

    // Baryons
    constexpr auto proton        = ParticleID{ 2212 };
    constexpr auto antiproton    = ParticleID{ -2212 };
    constexpr auto neutron       = ParticleID{ 2112 };
    constexpr auto antineutron   = ParticleID{ -2112 };
    constexpr auto lambda        = ParticleID{ 3122 };
    constexpr auto anti_lambda   = ParticleID{ -3122 };
    constexpr auto lambda_b      = ParticleID{ 5122 };
    constexpr auto anti_lambda_b = ParticleID{ -5122 };
    constexpr auto sigma_plus    = ParticleID{ 3222 };
    constexpr auto sigma_zero    = ParticleID{ 3212 };
    constexpr auto sigma_minus   = ParticleID{ 3112 };
    constexpr auto xi_zero       = ParticleID{ 3322 };
    constexpr auto xi_minus      = ParticleID{ 3312 };
    constexpr auto omega_minus   = ParticleID{ 3334 };

    // Photons and Bosons
    constexpr auto photon      = ParticleID{ 22 };
    constexpr auto z_boson     = ParticleID{ 23 };
    constexpr auto w_plus      = ParticleID{ 24 };
    constexpr auto w_minus     = ParticleID{ -24 };
    constexpr auto higgs_boson = ParticleID{ 25 };

    // ==========================================================================
  } // namespace ParticleIDs
  // ============================================================================

  // ==========================================================================
} // namespace LHCb
// ============================================================================
#endif /// LHCbKernel_ParticleIDs_H
