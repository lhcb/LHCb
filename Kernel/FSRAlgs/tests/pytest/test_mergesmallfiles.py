###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from pathlib import Path

import pytest
from GaudiTesting import platform_matches
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import KeepLines, LineSkipper


@pytest.mark.skipif(platform_matches([".*-dbg"]), reason="Unsupported platform")
class Test(LHCbExeTest):
    """
    Author: rlambert
    Purpose: Merge small files and check the FSRs which result from that
    Prerequisites: None
    Common failure modes, severities and cures:
                  . SEVERE: Segfault or raised exception, stderr, nonzero return code. Segfaults may indicate a massive VMEM usage from merging FSRs.
                  . MAJOR: no ERROR messages should ever be printed when running this test.
                  . MAJOR: File size explosion, if merging and cleaning of the input file FSRs fails to happen, the file size can explode.
                  . MAJOR: Memory usage explosion, if merging and cleaning of the input file FSRs fails to happen, the VMEM can explode in finalize.
    """

    command = ["gaudirun.py", "../../options/merge-small-files-test.py"]
    reference = "../refs/merge-small-files.yaml"
    timeout = 3600
    preprocessor = (
        LHCbExeTest.preprocessor
        + KeepLines(["FSRCleaner", "MergeFSR"])
        + LineSkipper(["ToolSvc."])
    )

    myname = "fsralgs.mergesmallfiles"
    retstr = ""

    def test_filesize(self, cwd: Path):
        try:
            size = os.path.getsize(cwd / "merged.dst")
        except OSError:
            raise OSError("Could not locate merged DST")

        assert (size / 1024 / 1024) < 3.0, (
            f"Merged DST large: {size / 1024 / 1024}, serious problem encountered."
        )

    def test_vmem(self, cwd: Path):
        memory = -1
        # Disable this check under sanitizers as vmem usage
        # there is artifically high
        if "PRELOAD_SANITIZER_LIB" in os.environ:
            return None

        try:
            from XMLSummaryBase import summary

            sum = summary.Summary()
            sum.parse(cwd / "summary.xml")
            for child in sum.children("usage")[0].children():
                if child.attrib()["useOf"] == "MemoryMaximum":
                    memory = child.value()
        except ImportError:
            raise ImportError("XMLSummary Summary class could not be imported.")
        except IOError:
            raise ImportError("XMLsummary missing")
        except IndexError:
            raise IndexError("XMLsummary Memory Maximum missing")

        assert memory != -1, "XMLsummary Memory Maximum missing."
        assert (memory / 1024 / 1024) < 2.0, (
            f"VMem usage too high: {memory / 1024 / 1024}, serious problem encountered."
        )
