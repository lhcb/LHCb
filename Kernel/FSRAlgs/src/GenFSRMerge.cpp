/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "FSRAlgs/IFSRNavigator.h"

#include "Event/GenFSR.h"

#include "Gaudi/Algorithm.h"
#include "GaudiAlg/FixTESPath.h"
#include "GaudiKernel/IDataProviderSvc.h"

/**
 *  Implementation file for class : GenFSRMerge
 *
 *  @author Davide Fazzini
 *  @date   2015-06-25
 */

class GenFSRMerge : public FixTESPath<Gaudi::Algorithm> {
public:
  using FixTESPath::FixTESPath;

  StatusCode execute( const EventContext& ) const override { return StatusCode::SUCCESS; };
  StatusCode finalize() override;

private:
  Gaudi::Property<std::string> m_fileRecordName{ this, "FileRecordLocation", "/FileRecords",
                                                 "TES location where FSRs are persisted" };
  Gaudi::Property<std::string> m_FSRName{ this, "FSRName", "/GenFSR", "Name of the genFSR tree" };

  ServiceHandle<IDataProviderSvc> m_fileRecordSvc{ this, "FileRecordDataSvc", "FileRecordDataSvc",
                                                   "Reference to run records data service" };
  ToolHandle<IFSRNavigator>       m_navigatorTool{ this, "FSRNavigator", "FSRNavigator", "tool to navigate FSR" };
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GenFSRMerge )

StatusCode GenFSRMerge::finalize() {
  info() << "========== Merging GenFSR ==========" << endmsg;

  // make an inventory of the FileRecord store
  std::vector<std::string> slots     = { "/PrevPrev", "/Prev", "/Next", "/NextNext", "" };
  std::vector<std::string> addresses = m_navigatorTool->navigate( m_fileRecordName, m_FSRName );

  for ( const auto& slot : slots ) {
    std::string nameFSR = slot + m_FSRName.value();
    int         nFSRs   = 0;

    for ( const auto& genRecordAddress : addresses ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "address: " << genRecordAddress << endmsg;
      if ( genRecordAddress.find( nameFSR ) != std::string::npos ) {
        DataObject* obj = nullptr;
        m_fileRecordSvc->retrieveObject( fullTESLocation( genRecordAddress, true ), obj ).ignore();
        nFSRs++;
      }
    }

    if ( nFSRs == 0 ) continue;

    LHCb::GenFSR* genFSRMerged = new LHCb::GenFSR();
    m_fileRecordSvc->registerObject( fullTESLocation( m_fileRecordName.value() + nameFSR, true ), genFSRMerged )
        .ignore();

    genFSRMerged->initializeInfos();
    for ( const auto& genRecordAddress : addresses ) {
      if ( genRecordAddress.find( nameFSR ) != std::string::npos ) {
        DataObject*   obj = nullptr;
        LHCb::GenFSR* genFSR =
            m_fileRecordSvc->retrieveObject( fullTESLocation( genRecordAddress, true ), obj ).isSuccess()
                ? dynamic_cast<LHCb::GenFSR*>( obj )
                : nullptr;
        if ( genFSR != nullptr ) {
          // Fill the informations of the new FSR
          if ( genFSRMerged->getSimulationInfo( "hardGenerator", "" ) == "" ) {
            genFSRMerged->setStringInfos( genFSR->stringInfos() );
            genFSRMerged->setIntInfos( genFSR->intInfos() );
          }
          // Update the FSR information and append to TS
          *genFSRMerged += *genFSR;
        }
      }
    }

    // clean up original FSRs
    for ( auto it = addresses.begin(); it != addresses.end(); ) {
      // get FSR as keyed object and cleanup the original ones - this only cleans genFSRs
      if ( msgLevel( MSG::DEBUG ) ) debug() << "address in list: " << *it << endmsg;
      if ( ( *it ).find( nameFSR ) != std::string::npos ) {
        DataObject*   obj    = nullptr;
        LHCb::GenFSR* genFSR = m_fileRecordSvc->retrieveObject( fullTESLocation( *it, true ), obj ).isSuccess()
                                   ? dynamic_cast<LHCb::GenFSR*>( obj )
                                   : nullptr;
        if ( !genFSR ) {
          if ( msgLevel( MSG::ERROR ) ) error() << *it << " not found" << endmsg;
        } else {
          m_fileRecordSvc->unlinkObject( *it ).ignore(); // get the FSR's directory out of TS
          it = addresses.erase( it );
        }
      } else
        ++it;
    }
  }

  // make a new inventory of the FileRecord store
  addresses = m_navigatorTool->navigate( m_fileRecordName, m_FSRName );
  if ( msgLevel( MSG::DEBUG ) )
    for ( const auto& addr : addresses ) debug() << "address: " << addr << endmsg;

  return FixTESPath::finalize();
}
