/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/EventCountFSR.h"

#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "LHCbAlgs/Consumer.h"

#include <mutex>

namespace LHCb {

  /**
   *  @author Rob Lambert
   *  @date   2009-11-11
   *
   *  EventAccounting fills an EventCountFSR.
   *         This alg should be run by default in Brunel and DaVinci
   *         The EventCountFSR holds information on how many events should be in the file
   *         and how many were read from the input files used.
   *         A flag is also set in case the number stored is known to be wrong.
   *
   */
  class EventAccounting : public extends<Algorithm::Consumer<void()>, IIncidentListener> {
  public:
    using extends::extends;

    StatusCode initialize() override;
    void       operator()() const override;
    StatusCode finalize() override;

    // IIncindentListener interface
    void handle( const Incident& ) override;

  protected:
    mutable EventCountFSR*                      m_eventFSR{};   // FSR for current file, owned by fileRecordSvc
    mutable std::map<std::string, unsigned int> m_count_files;  // a map of string to int for filenames
    mutable std::mutex                          m_eventFSRLock; // lock for protecting the previous members

    mutable Gaudi::Accumulators::Counter<> m_count_events{ this, "Number of events seen" };
    mutable Gaudi::Accumulators::Counter<> m_count_output{ this, "Number of incidents seen" };

    Gaudi::Property<std::string> m_FSRName{ this, "OutputDataContainer", EventCountFSRLocation::Default,
                                            "output location of summary data in FSR" };

    ServiceHandle<IDataProviderSvc> m_fileRecordSvc{ this, "FileRecordDataSvc", "FileRecordDataSvc",
                                                     "Reference to run records data service" };

  private:
    ServiceHandle<IIncidentSvc> m_incSvc{ this, "IncidentSvc", "IncidentSvc", "the incident service" };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( EventAccounting, "EventAccounting" )

} // namespace LHCb

StatusCode LHCb::EventAccounting::initialize() {
  return extends::initialize().andThen( [&]() {
    // prepare TDS for FSR
    m_eventFSR = new EventCountFSR{};
    m_fileRecordSvc->registerObject( fullTESLocation( m_FSRName.value(), true ), m_eventFSR ).ignore();

    // check extended file incidents are defined
#ifdef GAUDI_FILE_INCIDENTS
    m_incSvc->addListener( this, IncidentType::WroteToOutputFile );
    // if not then the counting is not reliable
#else
    m_eventFSR->setStatusFlag( EventCountFSR::UNRELIABLE );
    warn() << "cannot register with incSvc" << endmsg;
#endif // GAUDI_FILE_INCIDENTS
    return StatusCode::SUCCESS;
  } );
}

void LHCb::EventAccounting::operator()() const {
  ++m_count_events;
  std::scoped_lock l{ m_eventFSRLock };
  m_eventFSR->setInput( m_eventFSR->input() + 1 );
}

StatusCode LHCb::EventAccounting::finalize() {
  // if more than one file is written, the count is unreliable
  if ( m_count_files.size() != 1 ) m_eventFSR->setStatusFlag( EventCountFSR::StatusFlag::UNRELIABLE );
  return extends::finalize(); // must be called after all other actions
}

/// IIncindentListener interface
void LHCb::EventAccounting::handle( const Incident& incident ) {
  // check extended file incidents are defined
#ifdef GAUDI_FILE_INCIDENTS
  if ( incident.type() == IncidentType::WroteToOutputFile ) {
    ++m_count_output;
    std::scoped_lock l{ m_eventFSRLock };
    // maintain a per-file counter for now... could be used later to write many FSRs
    m_count_files[incident.source()] += 1;
    // this number will be incorrect if there is more than one file produced
    // set every time, and worry about it only in finalize
    m_eventFSR->setOutput( m_eventFSR->output() + 1 );
  }
#endif
}
