###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import DEBUG

from PyConf.Algorithms import EventAccounting, FSRCleaner, LumiMergeFSR
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    input_from_root_file,
    root_writer,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("R08S14_smallfiles")
options.output_file = "merged.dst"
options.output_type = "ROOT"
options.xml_summary_svc = "CounterSummarySvc"
options.xml_summary_file = "summary.xml"
options.root_ioalg_opts = {"StoreOldFSRs": True}
# GaudiPython is actually not used, but LumiMergeFSR expects that the TES was not cleaned up before finalize
options.gaudipython_mode = True
options.evt_max = 5
config = configure_input(options)

rawEvent = input_from_root_file(
    path="/Event/DAQ/RawEvent", forced_type="LHCb::RawEvent", options=options
)
evtAccounting = EventAccounting(name="EventAccount")
lumiMergeFSR = LumiMergeFSR(name="MergeFSR")
fsrCleaner = FSRCleaner(name="FSRCleaner", OutputLevel=DEBUG)
rootWriter = root_writer(options.output_file, [rawEvent])
cf_node = CompositeNode(
    "LumiSeq", children=[rawEvent, evtAccounting, lumiMergeFSR, fsrCleaner, rootWriter]
)
extra_config = configure(options, cf_node)

# amend the XMLSummarySvc and set OutputLevel and UpdateFreq
summarySvc = extra_config["XMLSummarySvc/CounterSummarySvc"]
summarySvc.UpdateFreq = 1
config.update(extra_config)
