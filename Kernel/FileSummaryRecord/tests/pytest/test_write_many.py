###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import os
from pathlib import Path

from LHCbTesting import LHCbExeTest

FILENAME = "test_write_many.root"
FILENAMEJSON = "test_write_many.json"


class Test(LHCbExeTest):
    command = ["gaudirun.py", f"{__file__}:config"]

    def test_files_exist(self, cwd: Path):
        for name in [FILENAME, FILENAMEJSON]:
            assert os.path.exists(cwd / name)

    def test_files_content(self, cwd: Path):
        expected_counters = {
            "EvtCounter1.count": {
                "empty": False,
                "nEntries": 5,
                "type": "counter:Counter:m",
            },
            "EvtCounter2.count": {
                "empty": False,
                "nEntries": 5,
                "type": "counter:Counter:m",
            },
        }
        expected = {
            "LHCb__FSR__Sink": ["EvtCounter1.count"],
            "SecondFSR": ["EvtCounter2.count"],
            "FileSummaryRecord": ["EvtCounter1.count", "EvtCounter2.count"],
        }

        import ROOT

        f = ROOT.TFile.Open(str(cwd / FILENAME))
        fsrs = {name: json.loads(str(getattr(f, name))) for name in expected}

        for name, counters in expected.items():
            for counter in counters:
                assert fsrs[name].get(counter) == expected_counters[counter]


def config():
    from Configurables import ApplicationMgr

    from PyConf.Algorithms import EventCountAlg, Gaudi__Examples__IntDataProducer
    from PyConf.application import (
        ApplicationOptions,
        configure,
        configure_input,
        root_writer,
    )
    from PyConf.components import setup_component
    from PyConf.control_flow import CompositeNode

    options = ApplicationOptions(_enabled=False)
    # No data from the input is used, but something should be there for the configuration
    options.input_files = ["dummy_input_file_name.dst"]
    options.input_type = "ROOT"
    options.output_file = FILENAME
    options.output_type = "ROOT"
    options.data_type = "Upgrade"
    options.dddb_tag = "upgrade/dddb-20220705"
    options.conddb_tag = "upgrade/sim-20220705-vc-mu100"
    options.geometry_version = "run3/trunk"
    options.conditions_version = "master"
    options.simulation = True
    options.evt_max = 5
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.EvtSel = "NONE"  # ignore input configuration
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                AcceptRegex=r"^EvtCounter1\.count$",
            )
        )
    )
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FSRCount2",
                RecordName="SecondFSR",
                AcceptRegex=r"^EvtCounter2\.count$",
            )
        )
    )
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^EvtCounter.\.count$",
            )
        )
    )

    producer = Gaudi__Examples__IntDataProducer()

    cf = CompositeNode(
        "test",
        [
            EventCountAlg(name="EvtCounter1"),
            EventCountAlg(
                name="EvtCounter2",
                OutputLevel=4,  # minor change to the configuration needed to please PyConf
            ),
            producer,
            root_writer(options.output_file, [producer.OutputLocation]),
        ],
    )
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in [FILENAME, FILENAMEJSON]:
        if os.path.exists(name):
            os.remove(name)
