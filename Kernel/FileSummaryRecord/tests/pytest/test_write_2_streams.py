###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import os
from pathlib import Path

from LHCbTesting import LHCbExeTest

FILENAMEFSR = "test_write_2_streams.fsr.json"
FILENAMES = [f"test_write_2_streams.{i}.root" for i in range(2)]
FILENAMEJSON = "test_write_2_streams.json"


class Test(LHCbExeTest):
    command = ["gaudirun.py", f"{__file__}:config"]

    def test_files_exist(self, cwd: Path):
        for name in FILENAMES + [FILENAMEFSR, FILENAMEJSON]:
            assert os.path.exists(cwd / name)

    def test_files_content(self, cwd: Path):
        expected = {
            "EvtCounter.count": {
                "empty": False,
                "nEntries": 5,
                "type": "counter:Counter:m",
            },
            "FilesTracker.FileEvents": [],
            "FilesTracker.OutputFileStats": {
                "test_write_2_streams.0.root": {"status": "full", "writes": 5},
                "test_write_2_streams.1.root": {"status": "full", "writes": 5},
            },
        }

        import ROOT

        fsr_dump = json.load(open(cwd / FILENAMEFSR))
        fsr_root = {}
        for name in FILENAMES:
            f = ROOT.TFile.Open(str(cwd / name))
            fsr_root[name] = json.loads(str(f.FileSummaryRecord))

        guids = {e["name"]: e["guid"] for e in fsr_dump.get("output_files", [])}
        assert guids, "Missing or invalid GUIDs in FSR dump."

        # this part is implicitly tested when comparing the content of ROOT FSRs
        del fsr_dump["output_files"]

        assert fsr_dump == expected
        for name in FILENAMES:
            expected["guid"] = guids[name]
            assert fsr_root[name] == expected


def config():
    from Configurables import ApplicationMgr

    from PyConf.Algorithms import EventCountAlg, Gaudi__Examples__IntDataProducer
    from PyConf.application import (
        ApplicationOptions,
        configure,
        configure_input,
        root_writer,
    )
    from PyConf.components import setup_component
    from PyConf.control_flow import CompositeNode

    options = ApplicationOptions(_enabled=False)
    # No data from the input is used, but something should be there for the configuration
    options.input_files = ["dummy_input_file_name.dst"]
    options.input_type = "ROOT"
    options.output_file = FILENAMES[0]
    options.output_type = "ROOT"
    options.data_type = "Upgrade"
    options.dddb_tag = "upgrade/dddb-20220705"
    options.conddb_tag = "upgrade/sim-20220705-vc-mu100"
    options.geometry_version = "run3/trunk"
    options.conditions_version = "master"
    options.simulation = True
    options.evt_max = 5
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.EvtSel = "NONE"  # ignore input configuration
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^(EvtCounter\.count|FilesTracker\..*)$",
                OutputFile=FILENAMEFSR,
            )
        )
    )
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__OpenFilesTracker",
                instance_name="FilesTracker",
            )
        )
    )

    producer = Gaudi__Examples__IntDataProducer()

    cf = CompositeNode(
        "test",
        [
            EventCountAlg(name="EvtCounter"),
            producer,
        ]
        + [root_writer(f, [producer.OutputLocation]) for f in FILENAMES],
    )
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in FILENAMES + [FILENAMEFSR, FILENAMEJSON]:
        if os.path.exists(name):
            os.remove(name)
