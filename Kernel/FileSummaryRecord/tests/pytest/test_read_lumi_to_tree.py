###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import os
from pathlib import Path

import pytest
import ROOT
from LHCbTesting import LHCbExeTest

FILENAMEFSR = "test_read_lumi_to_tree.fsr.json"
FILENAME = "test_read_lumi_to_tree.root"
FILENAMEJSON = "test_read_lumi_to_tree.json"


@pytest.mark.ctest_fixture_required("filesummaryrecord.write_lumi")
@pytest.mark.shared_cwd("FileSummaryRecord")
class Test(LHCbExeTest):
    command = ["gaudirun.py", f"{__file__}:config"]

    def test_files_exist(self, cwd: Path):
        for name in [FILENAME, FILENAMEFSR, FILENAMEJSON]:
            assert os.path.exists(cwd / name)

    def test_files_content(self, cwd: Path):
        # Load the JSON data
        json_data = json.load(open(cwd / FILENAMEFSR))

        # Open the ROOT file and get the TTree
        root_file = ROOT.TFile.Open(str(cwd / FILENAME))
        tree = root_file.Get("lumiTree")
        assert tree is not None, "Failed to get TTree from ROOT file."

        # Get the expected values from the JSON data
        expected_values = {}

        assert len(json_data["inputs"]) == 1, "Unexpected inputs in FSR."

        input_json = json_data["inputs"][0]

        for key, value in input_json["LumiCounter.eventsByRun"]["counts"].items():
            expected_values[int(key)] = value

        # Check if the TTree data matches the expected values
        for i in range(tree.GetEntries()):
            tree.GetEntry(i)
            key = tree.runNumber
            sum_value = tree.sumLumievents
            assert key in expected_values, (
                f"Key: {key} not in expected keys: {expected_values}"
            )
            assert sum_value == expected_values[key], (
                f"Sum value: {sum_value} not in expected values: {expected_values[key]}"
            )


def config():
    from Configurables import ApplicationMgr, Gaudi__MultiFileCatalog

    from PyConf.Algorithms import (
        EventCountAlg,
        Gaudi__Examples__IntDataProducer,
        ReadTES,
    )
    from PyConf.application import (
        ApplicationOptions,
        configure,
        configure_input,
        create_or_reuse_rootIOAlg,
        root_writer,
    )
    from PyConf.components import setup_component
    from PyConf.control_flow import CompositeNode

    Gaudi__MultiFileCatalog("FileCatalog").Catalogs = [
        "xmlcatalog_file:FSRTests.catalog.xml"
    ]

    options = ApplicationOptions(_enabled=False)
    options.input_files = ["test_write_lumi.root"]
    options.input_type = "ROOT"
    options.output_file = FILENAME
    options.output_type = "ROOT"
    options.data_type = "Upgrade"
    options.dddb_tag = "upgrade/dddb-20220705"
    options.conddb_tag = "upgrade/sim-20220705-vc-mu100"
    options.geometry_version = "run3/trunk"
    options.conditions_version = "master"
    options.simulation = True
    options.evt_max = 50
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 1

    config = configure_input(options)

    app = ApplicationMgr()
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^(Evt|Other)Counter\.count$",
                OutputFile=FILENAMEFSR,
            )
        )
    )
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__Tests__InputFSRSpy",
                instance_name="InputFSRSpy",
            )
        )
    )
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__LumiFSRtoTTree",
                instance_name="LumiFSRtoTTree",
            )
        )
    )

    producer = Gaudi__Examples__IntDataProducer()
    # force IOAlg as the test expects it to be ran even if none of its output is used !
    # also pretend we need some branch (or ROOT fails reading nothing from a branch...)
    ioalg = create_or_reuse_rootIOAlg(options)
    ioalg._properties["EventBranches"].append("/Event/IntDataProducer")

    cf = CompositeNode(
        "test",
        [
            ioalg,
            ReadTES(Locations=["/Event"]),
            EventCountAlg(name="EvtCounter"),
            EventCountAlg(
                name="OtherCounter",
                ErrorMax=2,  # this is needed just because PyConf does
                # not allow two identical algs apart from the name
            ),
            producer,
            root_writer(options.output_file, [producer.OutputLocation]),
        ],
    )
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in [FILENAME, FILENAMEFSR, FILENAMEJSON]:
        if os.path.exists(name):
            os.remove(name)
