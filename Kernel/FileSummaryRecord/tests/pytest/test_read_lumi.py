###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import itertools
import json
import os
from pathlib import Path

import pytest
from LHCbTesting import LHCbExeTest

FILENAMEFSR = "test_read_lumi.fsr.json"
FILENAME = "test_read_lumi.root"
FILENAMEJSON = "test_read_lumi.json"


@pytest.mark.ctest_fixture_required("filesummaryrecord.write_lumi")
@pytest.mark.shared_cwd("FileSummaryRecord")
class Test(LHCbExeTest):
    command = ["gaudirun.py", f"{__file__}:config"]

    def test_stdout(self, stdout: bytes, cwd: Path):
        assert b"got FSR from input file" in stdout, "Missing message from InputFSRSpy."

        lines = list(
            itertools.islice(
                itertools.dropwhile(
                    lambda l: "got FSR from input file" not in l,
                    stdout.decode().splitlines(),
                ),
                0,
                1,
            )
        )[0]
        lines = lines.split("INFO got FSR from input file: ")[1]

        input_fsr = json.load(open(cwd / "test_write_lumi.fsr.json"))
        assert input_fsr == json.loads(lines)

    def test_files_exist(self, cwd: Path):
        for name in [FILENAME, FILENAMEFSR, FILENAMEJSON]:
            assert os.path.exists(cwd / name)

    def test_files_content(self, cwd: Path):
        expected = {
            "EvtCounter.count": {
                "empty": False,
                "nEntries": 5,
                "type": "counter:Counter:m",
            },
            "OtherCounter.count": {
                "empty": False,
                "nEntries": 5,
                "type": "counter:Counter:m",
            },
            "inputs": [],
        }

        import ROOT

        fsr_dump = json.load(open(cwd / FILENAMEFSR))
        f = ROOT.TFile.Open(str(cwd / FILENAME))
        fsr_root = json.loads(str(f.FileSummaryRecord))

        guid = fsr_dump.get("guid")
        assert guid, "Missing or invalid GUID in FSR dump."

        expected["guid"] = guid  # GUID is random

        # let's get the FSR of the input file from the output of the upstream test
        input_fsr = json.load(open(cwd / "test_write_lumi.fsr.json"))
        expected["inputs"].append(input_fsr)

        # Check that the json contains what we wanted it to contain
        assert fsr_dump == expected

        # Check that it does so also after being written into the rootfile as a string
        assert fsr_root == expected


def config():
    from Configurables import ApplicationMgr, Gaudi__MultiFileCatalog

    from PyConf.Algorithms import (
        EventCountAlg,
        Gaudi__Examples__IntDataProducer,
        ReadTES,
    )
    from PyConf.application import (
        ApplicationOptions,
        configure,
        configure_input,
        create_or_reuse_rootIOAlg,
        root_writer,
    )
    from PyConf.components import setup_component
    from PyConf.control_flow import CompositeNode

    Gaudi__MultiFileCatalog("FileCatalog").Catalogs = [
        "xmlcatalog_file:FSRTests.catalog.xml"
    ]

    options = ApplicationOptions(_enabled=False)
    options.input_files = ["test_write_lumi.root"]
    options.input_type = "ROOT"
    options.output_file = FILENAME
    options.output_type = "ROOT"
    options.data_type = "Upgrade"
    options.dddb_tag = "upgrade/dddb-20220705"
    options.conddb_tag = "upgrade/sim-20220705-vc-mu100"
    options.geometry_version = "run3/trunk"
    options.conditions_version = "master"
    options.simulation = True
    options.evt_max = -1
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^(Evt|Other)Counter\.count$",
                OutputFile=FILENAMEFSR,
            )
        )
    )
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__Tests__InputFSRSpy",
                instance_name="InputFSRSpy",
            )
        )
    )

    producer = Gaudi__Examples__IntDataProducer(name="IntDataProducer")
    # force IOAlg as the test expects it to be ran even if none of its output is used !
    # also pretend we need some branch (or ROOT fails reading nothing from a branch...)
    ioalg = create_or_reuse_rootIOAlg(options)
    ioalg._properties["EventBranches"].append("/Event/IntDataProducer")

    cf = CompositeNode(
        "test",
        [
            ioalg,
            ReadTES(Locations=["/Event"]),
            EventCountAlg(name="EvtCounter"),
            EventCountAlg(
                name="OtherCounter",
                ErrorMax=2,  # this is needed just because PyConf does
                # not allow two identical algs apart from the name
            ),
            producer,
            root_writer(options.output_file, [producer.OutputLocation]),
        ],
    )
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in [FILENAME, FILENAMEFSR, FILENAMEJSON]:
        if os.path.exists(name):
            os.remove(name)
