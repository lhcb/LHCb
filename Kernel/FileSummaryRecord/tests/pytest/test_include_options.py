###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import os
from pathlib import Path

from LHCbTesting import LHCbExeTest

FILENAMEFSR = "test_include_options.fsr.json"
FILENAMEOPTS = "test_include_options.opts.json"
FILENAME = "test_include_options.root"
FILENAMEJSON = "test_include_options.json"


class Test(LHCbExeTest):
    command = [
        "gaudirun.py",
        f"{__file__}:config",
        "--output=test_include_options.opts.json",
    ]

    def test_files_exist(self, cwd: Path):
        for name in [FILENAME, FILENAMEFSR, FILENAMEJSON, FILENAMEOPTS]:
            assert os.path.exists(cwd / name), f"{name} gone missing."

    def test_files_content(self, cwd: Path):
        expected = {
            "EvtCounter.count": {
                "empty": False,
                "nEntries": 5,
                "type": "counter:Counter:m",
            },
        }
        expected_options = {
            "FileSummaryRecord.AcceptRegex": '"^EvtCounter\\\\.count$"',
            "FileSummaryRecord.IncludeJobOptions": "True",
            "FileSummaryRecord.OutputFile": f"'{FILENAMEFSR}'",
            "Gaudi__Examples__IntDataProducer.OutputLocation": "/Event/Gaudi__Examples__IntDataProducer/OutputLocation",
            "Gaudi__Examples__IntDataProducer.Value": "1",
        }

        import ROOT

        fsr_dump = json.load(open(cwd / FILENAMEFSR))
        f = ROOT.TFile.Open(str(cwd / FILENAME))
        fsr_root = json.loads(str(f.FileSummaryRecord))

        guid = fsr_dump.get("guid")
        assert guid, "Missing or invalid GUID in FSR dump."

        expected["guid"] = guid  # GUID is random

        jobOptions = fsr_dump.get("jobOptions")
        assert jobOptions, "Missing or invalid job options in FSR dump."

        # we do not want to check all details of the options
        expected["jobOptions"] = jobOptions

        assert fsr_dump == expected
        assert fsr_root == expected

        # check that we find in the options what we explicitly set
        found_options = jobOptions["options"]
        found_options = {
            k: found_options[k] for k in expected_options if k in found_options
        }
        assert found_options == expected_options


def config():
    from Configurables import ApplicationMgr

    from PyConf.Algorithms import EventCountAlg, Gaudi__Examples__IntDataProducer
    from PyConf.application import (
        ApplicationOptions,
        configure,
        configure_input,
        root_writer,
    )
    from PyConf.components import setup_component
    from PyConf.control_flow import CompositeNode

    options = ApplicationOptions(_enabled=False)
    # No data from the input is used, but something should be there for the configuration
    options.input_files = ["dummy_input_file_name.dst"]
    options.input_type = "ROOT"
    options.output_file = FILENAME
    options.output_type = "ROOT"
    options.data_type = "Upgrade"
    options.dddb_tag = "upgrade/dddb-20220705"
    options.conddb_tag = "upgrade/sim-20220705-vc-mu100"
    options.geometry_version = "run3/trunk"
    options.conditions_version = "master"
    options.simulation = True
    options.evt_max = 5
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.EvtSel = "NONE"  # ignore input configuration
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^EvtCounter\.count$",
                OutputFile=FILENAMEFSR,
                IncludeJobOptions=True,
            )
        )
    )

    producer = Gaudi__Examples__IntDataProducer(
        name="Gaudi__Examples__IntDataProducer", Value=1
    )

    cf = CompositeNode(
        "test",
        [
            EventCountAlg(name="EvtCounter"),
            producer,
            root_writer(options.output_file, [producer.OutputLocation]),
        ],
    )
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in [FILENAME, FILENAMEFSR, FILENAMEJSON, FILENAMEOPTS]:
        if os.path.exists(name):
            os.remove(name)
