###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import os
from pathlib import Path

import pytest
from LHCbTesting import LHCbExeTest

FILENAMEFSR = "test_write.fsr.json"
FILENAME = "test_write.root"
FILENAMEJSON = "test_write.json"
GUID = "D80398A9-70BB-4EEE-9AB8-0BEC4D976C13"


@pytest.mark.ctest_fixture_setup("filesummaryrecord.write")
@pytest.mark.shared_cwd("FileSummaryRecord")
class Test(LHCbExeTest):
    command = ["gaudirun.py", f"{__file__}:config"]

    def test_files_exist(self, cwd: Path):
        for name in [FILENAME, FILENAMEFSR, FILENAMEJSON]:
            assert os.path.exists(cwd / name)

    def test_files_content(self, cwd: Path):
        expected = {
            "EvtCounter.count": {
                "empty": False,
                "nEntries": 5,
                "type": "counter:Counter:m",
            },
            "guid": GUID,
        }

        import ROOT

        fsr_dump = json.load(open(cwd / FILENAMEFSR))
        f = ROOT.TFile.Open(str(cwd / FILENAME))
        fsr_root = json.loads(str(f.FileSummaryRecord))

        assert fsr_dump == expected
        assert fsr_root == expected


def config():
    from Configurables import ApplicationMgr, Gaudi__MultiFileCatalog

    from PyConf.Algorithms import EventCountAlg, Gaudi__Examples__IntDataProducer
    from PyConf.application import (
        ApplicationOptions,
        configure,
        configure_input,
        root_writer,
    )
    from PyConf.components import setup_component
    from PyConf.control_flow import CompositeNode

    # use a specific XML catalog
    Gaudi__MultiFileCatalog("FileCatalog").Catalogs = [
        "xmlcatalog_file:FSRTests.catalog.xml"
    ]
    with open("FSRTests.catalog.xml", "w") as f:
        f.write(f"""<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<!DOCTYPE POOLFILECATALOG SYSTEM "InMemory">
<POOLFILECATALOG>
  <File ID="{GUID}">
    <physical>
      <pfn filetype="ROOT" name="{FILENAME}"/>
    </physical>
    <logical>
      <lfn name="FSRTests-write"/>
    </logical>
  </File>
</POOLFILECATALOG>""")

    options = ApplicationOptions(_enabled=False)
    # No data from the input is used, but something should be there for the configuration
    options.input_files = ["dummy_input_file_name.dst"]
    options.input_type = "ROOT"
    options.output_file = "LFN:FSRTests-write"
    options.output_type = "ROOT"
    options.data_type = "Upgrade"
    options.dddb_tag = "upgrade/dddb-20220705"
    options.conddb_tag = "upgrade/sim-20220705-vc-mu100"
    options.geometry_version = "run3/trunk"
    options.conditions_version = "master"
    options.simulation = True
    options.evt_max = 5
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.EvtSel = "NONE"  # ignore input configuration
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^EvtCounter\.count$",
                OutputFile=FILENAMEFSR,
            )
        )
    )

    producer = Gaudi__Examples__IntDataProducer(name="IntDataProducer")

    cf = CompositeNode(
        "test",
        [
            EventCountAlg(name="EvtCounter"),
            producer,
            root_writer(options.output_file, [producer.OutputLocation]),
        ],
    )
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in [FILENAME, FILENAMEFSR, FILENAMEJSON]:
        if os.path.exists(name):
            os.remove(name)
