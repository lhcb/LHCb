/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  /** @class HCCellID HCCellID.h
   *
   * This class identifies a single PMT in the HC
   *
   * @author Victor Coco
   *
   */

  class HCCellID final {
  public:
    /// Default Constructor
    constexpr HCCellID() = default;

    /// Constructor with cellID
    constexpr explicit HCCellID( unsigned int id ) : m_cellID( id ) {}

    /// Constructor with crate and channel
    constexpr HCCellID( unsigned int crate, unsigned int channel ) : HCCellID{ ( crate << crateBits ) | channel } {}

    /// Cast
    constexpr operator unsigned int() const { return m_cellID; }

    /// Special serializer to ASCII stream
    std::ostream& fillStream( std::ostream& s ) const;

    /// Retrieve const  HC Cell ID
    [[nodiscard]] constexpr unsigned int cellID() const { return m_cellID; }

    /// Update  HC Cell ID
    constexpr HCCellID& setCellID( unsigned int value ) {
      m_cellID = value;
      return *this;
    }

    /// Retrieve channel number
    [[nodiscard]] constexpr unsigned int channel() const { return ( m_cellID & channelMask ) >> channelBits; }

    /// Update channel number
    constexpr HCCellID& setChannel( unsigned int value ) {
      m_cellID &= ~channelMask;
      m_cellID |= ( value << channelBits ) & channelMask;
      return *this;
    }

    /// Retrieve crate number
    [[nodiscard]] constexpr unsigned int crate() const { return ( m_cellID & crateMask ) >> crateBits; }

    /// Update crate number
    constexpr HCCellID& setCrate( unsigned int value ) {
      m_cellID &= ~crateMask;
      m_cellID |= ( value << crateBits ) & crateMask;
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const HCCellID& obj ) { return obj.fillStream( str ); }

  private:
    /// Offsets of bitfield cellID
    enum cellIDBits { channelBits = 0, crateBits = 6 };

    /// Bitmasks for bitfield cellID
    enum cellIDMasks { channelMask = 0x3fL, crateMask = 0x7c0L };

    unsigned int m_cellID{ 0 }; ///< HC Cell ID

  }; // class HCCellID

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------
