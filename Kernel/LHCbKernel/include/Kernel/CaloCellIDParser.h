/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Calo/CaloCellID.h"

#include "GaudiKernel/StatusCode.h"

/**
 *  Streamer& Parsing function to allow CaloCellID & related classes to be
 *  used as properties for Gaudi components
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2009-09-29
 */
namespace Gaudi::Parsers {
  /** parse cellID from the string
   *  @param result (OUPUT) the parsed cellID
   *  @param input  (INPUT) the input string
   *  @return status code
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-29
   */
  StatusCode parse( LHCb::Detector::Calo::CellID& result, const std::string& input );
} // namespace Gaudi::Parsers
