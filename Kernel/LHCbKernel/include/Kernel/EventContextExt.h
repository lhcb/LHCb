/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// LHCb::Allocators::MemoryResource
#include "Kernel/EventLocalResource.h"

#include "GAUDI_VERSION.h"
#include "GaudiKernel/EventContext.h"

#include <any>
#include <cstddef>
#include <memory>

namespace LHCb {
  /** Store an Allocators::MemoryResource* in the given EventContext.
   *  Ownership is not transferred. This could be used to save a resource
   *  pointer directly in the EventContext -- so that it can be loaded without
   *  the indirection of accessing the extension -- but at present there is no
   *  safe, relevant field in the EventContext object.
   */
  inline void setMemResource( EventContext&, Allocators::MemoryResource* ) {}

  struct EventContextExtension {
    template <typename ValueType, typename... Args>
    auto& emplaceSchedulerExtension( Args&&... args ) {
      return m_schedulerExtension.emplace<ValueType>( std::forward<Args>( args )... );
    }

    template <typename T>
    [[nodiscard]] auto& getSchedulerExtension() {
      return std::any_cast<std::decay_t<T>&>( m_schedulerExtension );
    }

    template <typename T>
    [[nodiscard]] const auto& getSchedulerExtension() const {
      return std::any_cast<std::decay_t<T> const&>( m_schedulerExtension );
    }

    const std::type_info& getSchedulerExtensionType() const { return m_schedulerExtension.type(); }

    /** Create a new memory resource that is owned by the extension.
     */
    template <typename ValueType, typename... Args>
    ValueType* emplaceMemResource( Args&&... args ) {
      m_memResource = std::make_shared<ValueType>( std::forward<Args>( args )... );
      return static_cast<ValueType*>( m_memResource.get() );
    }

    /** Get a pointer to the memory resource owned by the extension.
     */
    Allocators::MemoryResource* getMemResource() const { return m_memResource.get(); }

    /** Transfer the memory resource out of this extension
     */
    std::shared_ptr<Allocators::MemoryResource> removeMemResource() { return std::move( m_memResource ); }

    /** Remove the memory resource owned by this event context extension.
     */
    void resetMemResource() { m_memResource.reset(); }

  private:
    /** Shared memory arena that is guaranteed to have a lifespan at least
     *  slightly longer than the event store.
     */
    std::shared_ptr<Allocators::MemoryResource> m_memResource;

    /** Extra extension reserved for the scheduler implementation
     *  TODO std::any inside std::any must cause the outer one to dynamically allocate, might we win by making the inner
     *  one unique_ptr<any> so the outer one fits in the static buffer?
     */
    std::any m_schedulerExtension;
  };

  /** Retrieve an Allocators::MemoryResource* from the given EventContext.
   *  Ownership is not transferred. If LHCb::setMemResource() stored a relevant
   *  pointer directly in the EventContext then it would/might be more
   *  efficient to return that directly.
   */
  [[nodiscard]] __attribute__( ( always_inline ) ) inline Allocators::MemoryResource*
  getMemResource( EventContext const& evtContext ) {
#if GAUDI_VERSION >= CALC_GAUDI_VERSION( 33, 2 )
    auto ext = evtContext.tryGetExtension<EventContextExtension>();
    return ( ext ? ext->getMemResource() : nullptr );
#else
    return ( evtContext.hasExtension<EventContextExtension>()
                 ? evtContext.getExtension<EventContextExtension>().getMemResource()
                 : nullptr );
#endif
  }

} // namespace LHCb
