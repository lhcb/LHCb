/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/detected.h" // this provides Gaudi::cpp17

#include <type_traits>
#include <typeinfo>

// traits which indicate whether client code should use 'threeMomCovMatrix', `momPosCovMatrix` and `posCovMatrix`
// or whether to request a `trackState`
// possible values: yes (for track-like objects ) no (for neutrals, composites), maybe (particle, check at runtime, as
// calls may return invalid results -- not yet implemented)
//
namespace LHCb::Event::CanBeExtrapolatedDownstream {

  struct yes_t : std::true_type {};
  struct no_t : std::false_type {};
  struct maybe_t : no_t {
  }; // basically, if you need to know at compile time, the answer is 'no' as there is no guarantee -- but it could
     // still eg. be a Particle v1 representing a track, which in turn could be extrapolated downstream..

  inline constexpr auto yes   = yes_t{};
  inline constexpr auto no    = no_t{};
  inline constexpr auto maybe = maybe_t{};

} // namespace LHCb::Event::CanBeExtrapolatedDownstream

namespace LHCb::Event::IsBasicParticle {
  struct yes_t : std::true_type {};
  struct no_t : std::false_type {};

  inline constexpr auto yes = yes_t{};
  inline constexpr auto no  = no_t{};
} // namespace LHCb::Event::IsBasicParticle

namespace LHCb::Event::HasTrack {
  struct yes_t : std::true_type {};
  struct no_t : std::false_type {};

  inline constexpr auto yes = yes_t{};
  inline constexpr auto no  = no_t{};
} // namespace LHCb::Event::HasTrack

namespace Functors::detail {
  template <typename T>
  using has_loop_mask_ = decltype( std::declval<T>().loop_mask() );
  template <typename T>
  inline constexpr bool has_loop_mask_v = Gaudi::cpp17::is_detected_v<has_loop_mask_, T>;

  /** @fn    loop_mask
   *  @brief Extract a validity mask from an argument.
   *  @todo  Handle receiving >1 argument.
   */
  template <typename... Data>
  inline constexpr auto loop_mask( Data const&... x ) {
    if constexpr ( sizeof...( Data ) == 0 ) {
      // This is the best we can do
      return true;
    } else {
      // so far we can only handle loop masks for single input.
      // But many inputs without loop_mask -> we simply return true
      if constexpr ( ( has_loop_mask_v<Data> || ... ) ) {
        static_assert( sizeof...( Data ) == 1, "Support for multiple masked arguments is not tested yet." );
      }

      if constexpr ( ( has_loop_mask_v<Data> && ... ) ) {
        return ( x.loop_mask() && ... );
      } else {
        return true;
      }
    }
  }
} // namespace Functors::detail
