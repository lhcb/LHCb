/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SmartRef.h"
#include <cassert>
#include <type_traits>

namespace Relations {
  namespace details {
    constexpr bool is_KeyedObject( void const* ) { return false; }

    template <typename K>
    constexpr bool is_KeyedObject( KeyedObject<K> const* ) {
      return true;
    }

    template <typename T>
    constexpr bool is_KeyedObject_v = is_KeyedObject( static_cast<std::add_pointer_t<T>>( nullptr ) );
  } // namespace details

  template <typename Type_>
  struct KeyedObjectTypeTraits {
    // ========================================================================
    static_assert( details::is_KeyedObject_v<Type_> );
    using Type = Type_;
    // given that the input and output must be in the event store for a relation
    // to be usefull, any pointer to these  types should be pointer-to-const
    using Input  = const Type*;
    using Output = const Type*;
    using Inner  = SmartRef<Type>;
    struct Less {
      bool operator()( const Type& lhs, const Type& rhs ) const {
        // Relations should only be between objects in a container, hence there must _always_ be a parent.
        // And in case a table contains (merges) objects from multiple containers, then it is assumed
        // those containers are present in the TES, and thus have a registry.
        assert( lhs.parent() != nullptr );
        assert( rhs.parent() != nullptr );
        assert( lhs.parent() == rhs.parent() || ( lhs.parent()->registry() && rhs.parent()->registry() ) );
        return lhs.parent() == rhs.parent()
                   ? ( lhs.key() < rhs.key() )
                   : ( lhs.parent()->registry()->identifier() < rhs.parent()->registry()->identifier() );
      }
      bool operator()( const Type* lhs, const Type* rhs ) const {
        // there should not be any nullptrs in the 'from' side of a relation table...
        assert( lhs != nullptr );
        assert( rhs != nullptr );
        return ( *this )( *lhs, *rhs );
      }
    };
    template <typename T = void>
    using Equal = std::equal_to<T>;
    // ========================================================================
  };
} // namespace Relations
