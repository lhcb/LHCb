/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cassert>
#include <limits>
#include <ostream>

namespace LHCb::Detector::Plume {

  /** @class PlumeChannelID
   *
   * This class identifies a channel in the Plume detector
   *
   * @author Vladyslav Orlov
   *
   */

  // TODO: copy how the FTChannelID does it?
  constexpr unsigned int BitsChannelID     = 8; // we only use values 0-47
  constexpr unsigned int BitsChannelSubID  = 4; // used for distinguishing timing channel offsets
  constexpr unsigned int BitsChannelType   = 4; // should fit all possible channel types
  constexpr unsigned int ShiftChannelID    = 0;
  constexpr unsigned int ShiftChannelSubID = ShiftChannelID + BitsChannelID;
  constexpr unsigned int ShiftChannelType  = ShiftChannelSubID + BitsChannelSubID;
  constexpr unsigned int MaskChannelID     = ( ( 1u << BitsChannelID ) - 1u ) << ShiftChannelID;
  constexpr unsigned int MaskChannelSubID  = ( ( 1u << BitsChannelSubID ) - 1u ) << ShiftChannelSubID;
  constexpr unsigned int MaskChannelType   = ( ( 1u << BitsChannelType ) - 1u ) << ShiftChannelType;

  class ChannelID final {
  private:
  public:
    enum class ChannelType : unsigned int { ERR = 0, LUMI = 1, PIN = 2, MON = 3, TIME = 4, TIME_T = 5 };

    constexpr ChannelID() = default;

    constexpr ChannelID( ChannelType type, unsigned int id, unsigned int subID = 0 ) {
      assert( id < ( 1u << Plume::BitsChannelID ) );
      assert( static_cast<unsigned int>( type ) < ( 1u << Plume::BitsChannelType ) );
      m_all = ( ( id << Plume::ShiftChannelID ) & Plume::MaskChannelID ) |
              ( ( subID << Plume::ShiftChannelSubID ) & Plume::MaskChannelSubID ) |
              ( ( static_cast<unsigned int>( type ) << Plume::ShiftChannelType ) & Plume::MaskChannelType );
    }

    constexpr explicit ChannelID( unsigned int id ) { m_all = id; }

    std::string toString() const;
    /// Special serializer to ASCII stream
    std::ostream& fillStream( std::ostream& s ) const;

    /// Retrieve the underlying bit representation
    [[nodiscard]] constexpr unsigned int all() const { return m_all; }

    /// Retrieve channel type
    [[nodiscard]] constexpr ChannelType channelType() const {
      return static_cast<ChannelType>( ( m_all & Plume::MaskChannelType ) >> Plume::ShiftChannelType );
    }

    /// Retrieve the type-specific channel id
    [[nodiscard]] constexpr unsigned int channelID() const {
      return ( m_all & Plume::MaskChannelID ) >> Plume::ShiftChannelID;
    }

    /// Retrieve the type-specific channel sub id (timing offset)
    [[nodiscard]] constexpr unsigned int channelSubID() const {
      return ( m_all & Plume::MaskChannelSubID ) >> Plume::ShiftChannelSubID;
    }

    /// Update the underlying bit representation
    constexpr ChannelID& setAll( unsigned int value ) {
      m_all = value;
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const ChannelID& obj ) { return obj.fillStream( str ); }

    bool operator<( ChannelID const& other ) const { return m_all < other.m_all; }

  private:
    unsigned int m_all{ 0 };
  };

} // namespace LHCb::Detector::Plume
