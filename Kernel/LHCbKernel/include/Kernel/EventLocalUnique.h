/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Kernel/EventLocalAllocator.h"

#include <boost/compressed_pair.hpp>

#include <memory>

namespace LHCb {
  namespace Allocators {
    template <typename Alloc>
    class deleter {
      using allocator_type   = Alloc;
      using allocator_traits = std::allocator_traits<allocator_type>;

      template <typename>
      friend class deleter;

      // alloc, then sizeof object to be deleted
      boost::compressed_pair<allocator_type, std::size_t> m_data;

    public:
      template <typename U>
      deleter( deleter<U> const& other ) : m_data{ other.m_data.first(), other.m_data.second() } {}

      deleter( allocator_type alloc, std::size_t size ) : m_data{ std::move( alloc ), size } {}

      void operator()( typename allocator_traits::pointer p ) {
        allocator_traits::destroy( m_data.first(), p );
        typename allocator_traits::template rebind_alloc<std::byte> byte_alloc{ m_data.first() };
        byte_alloc.deallocate( reinterpret_cast<std::byte*>( p ), m_data.second() );
      }
    };
  } // namespace Allocators

  template <typename T, typename Alloc, typename... Args>
  auto allocate_unique( Alloc const& alloc, Args&&... args ) {
    // Get the allocator type for T
    using AllocT = typename std::allocator_traits<Alloc>::template rebind_alloc<T>;
    // Copy the allocator and rebind it
    AllocT new_alloc{ alloc };
    // Allocate memory for one T
    auto p = std::allocator_traits<AllocT>::allocate( new_alloc, 1 );
    // Construct the T object there
    std::allocator_traits<AllocT>::construct( new_alloc, p, std::forward<Args>( args )... );
    // Get the deleter type for this type + allocator
    using DeleterT = Allocators::deleter<AllocT>;
    // Put this all into a unique_ptr
    return std::unique_ptr<T, DeleterT>{ p, DeleterT{ std::move( alloc ), sizeof( T ) } };
  }

  template <typename T>
  using event_local_unique_ptr = std::unique_ptr<T, Allocators::deleter<Allocators::EventLocal<T>>>;

  template <typename T, typename... Args>
  event_local_unique_ptr<T> make_event_local_unique( LHCb::Allocators::EventLocal<T> alloc, Args&&... args ) {
    return allocate_unique<T>( std::move( alloc ), std::forward<Args>( args )... );
  }
} // namespace LHCb
