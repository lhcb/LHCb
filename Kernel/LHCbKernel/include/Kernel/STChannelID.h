/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  /** @class STChannelID STChannelID.h
   *
   * Channel ID for class for ST
   *
   * @author M Needham, J. Wang
   *
   */

  class STChannelID final {
  public:
    /// types of sub-detector channel ID
    enum detType { typeTT = 0, typeIT = 1 };

    /// Default Constructor
    constexpr STChannelID() = default;

    /// constructor with unsigned int
    constexpr explicit STChannelID( unsigned int id ) : m_channelID( id ) {}

    /// constructor with station, layer, detRegion, sector , strip,
    constexpr STChannelID( unsigned int iType, unsigned int iStation, unsigned int iLayer, unsigned int iDetRegion,
                           unsigned int iSector, unsigned int iStrip )
        : STChannelID{ ( iType << typeBits ) + ( iStation << stationBits ) + ( iLayer << layerBits ) +
                       ( iDetRegion << detRegionBits ) + ( iSector << sectorBits ) + ( iStrip << stripBits ) } {}

    /// cast
    constexpr operator unsigned int() const { return m_channelID; }

    /// Retrieve type
    [[nodiscard]] constexpr unsigned int type() const { return ( m_channelID & typeMask ) >> typeBits; }

    /// test whether TT or not
    [[nodiscard]] constexpr bool isTT() const { return type() == LHCb::STChannelID::detType::typeTT; }

    /// test whether IT or not
    [[nodiscard]] constexpr bool isIT() const { return type() == LHCb::STChannelID::detType::typeIT; }

    /// Retrieve sector
    [[nodiscard]] constexpr unsigned int sector() const { return ( m_channelID & sectorMask ) >> sectorBits; }

    /// Retrieve detRegion
    [[nodiscard]] constexpr unsigned int detRegion() const { return ( m_channelID & detRegionMask ) >> detRegionBits; }

    /// Retrieve layer
    [[nodiscard]] constexpr unsigned int layer() const { return ( m_channelID & layerMask ) >> layerBits; }

    /// Retrieve unique layer
    [[nodiscard]] constexpr unsigned int uniqueLayer() const { return ( m_channelID & uniqueLayerMask ) >> layerBits; }

    /// Retrieve unique detRegion
    [[nodiscard]] unsigned int uniqueDetRegion() const {
      return ( m_channelID & uniqueDetRegionMask ) >> detRegionBits;
    }

    /// Print this STChannelID in a human readable way
    std::ostream& fillStream( std::ostream& s ) const;

    /// Print method for python NOT NEEDED + SLOW IN C++ use fillStream
    [[nodiscard]] std::string toString() const;

    /// Retrieve const  ST Channel ID
    [[nodiscard]] constexpr unsigned int channelID() const { return m_channelID; }

    /// Update  ST Channel ID
    constexpr STChannelID& setChannelID( unsigned int value ) {
      m_channelID = value;
      return *this;
    }

    /// Retrieve strip
    [[nodiscard]] constexpr unsigned int strip() const { return ( m_channelID & stripMask ) >> stripBits; }

    /// Retrieve station
    [[nodiscard]] constexpr unsigned int station() const {
      return (unsigned int)( ( m_channelID & stationMask ) >> stationBits );
    }

    /// Retrieve unique sector
    [[nodiscard]] constexpr unsigned int uniqueSector() const {
      return ( m_channelID & uniqueSectorMask ) >> sectorBits;
    }

    friend std::ostream& operator<<( std::ostream& str, const STChannelID& obj ) { return obj.fillStream( str ); }

  private:
    /// Offsets of bitfield channelID
    enum channelIDBits {
      stripBits     = 0,
      sectorBits    = 10,
      detRegionBits = 15,
      layerBits     = 18,
      stationBits   = 21,
      typeBits      = 23
    };

    /// Bitmasks for bitfield channelID
    enum channelIDMasks {
      stripMask           = 0x3ffL,
      sectorMask          = 0x7c00L,
      detRegionMask       = 0x38000L,
      layerMask           = 0x1c0000L,
      stationMask         = 0x600000L,
      typeMask            = 0x1800000L,
      uniqueLayerMask     = layerMask + stationMask,
      uniqueDetRegionMask = detRegionMask + layerMask + stationMask,
      uniqueSectorMask    = sectorMask + detRegionMask + layerMask + stationMask
    };

    unsigned int m_channelID{ 0 }; ///< ST Channel ID

  }; // class STChannelID

  inline std::ostream& operator<<( std::ostream& s, LHCb::STChannelID::detType e ) {
    switch ( e ) {
    case LHCb::STChannelID::typeTT:
      return s << "typeTT";
    case LHCb::STChannelID::typeIT:
      return s << "typeIT";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::STChannelID::detType";
    }
  }

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------
