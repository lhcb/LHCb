/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/IInterface.h"
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/System.h"
#include <string>

/** @class IRelationBase IRelationBase.h Relations/IRelationBase.h
 *
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2004-01-14
 */
struct IRelationBase : virtual IInterface {
  /** rebuild/resort  existing relations
   *
   *  @return status code
   *
   */
  virtual StatusCode rebuild() = 0;

  /** remove ALL relations from ALL to ALL objects
   *
   *  @return status code
   */
  virtual StatusCode clear() = 0;

  /** the unique interface ID (static)
   *  @return the unique interface identifier
   */
  static const InterfaceID& interfaceID();

  // empty implementation of IInterface
  unsigned long addRef() override { return 1; }
  unsigned long release() override { return 1; }
  // empty implementation of IInterface
  void*                    i_cast( const InterfaceID& ) const override { return nullptr; }
  StatusCode               queryInterface( const InterfaceID&, void** ) override { return StatusCode::FAILURE; }
  std::vector<std::string> getInterfaceNames() const override { return {}; }
  unsigned long            refCount() const override { return 0; }
};
