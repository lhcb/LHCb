/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <iostream>
#include <memory>
#include <type_traits>
#include <vector>

// Boost
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE test_relations
#include <boost/test/unit_test.hpp>

// Relations
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"
#include "Relations/RelationsDict.h"

#define NOINLINE __attribute__( ( noinline ) )

/** @file
 *
 *  Auxillary test-file for the package Kernel/Relations
 *  to detect the obvious problems
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2009-04-09
 */

template <std::size_t N, typename REL>
void NOINLINE fill( REL& rel ) {
  bool OK = true;
  for ( std::size_t i = 0; i < N; ++i ) {
    if constexpr ( std::is_base_of_v<Relations::BaseWeightedTable, REL> ) {
      using FROM   = typename REL::From;
      using TO     = typename REL::To;
      using WEIGHT = typename REL::Weight;
      OK &= rel.relate( FROM( i ), TO( i ), WEIGHT( i ) );
    } else if constexpr ( std::is_base_of_v<Relations::BaseTable, REL> ) {
      using FROM = typename REL::From;
      using TO   = typename REL::To;
      OK &= rel.relate( FROM( i ), TO( i ) );
    } else {
      rel.reserve( N );
    }
  }
  if constexpr ( std::is_base_of_v<Relations::BaseWeightedTable, REL> ||
                 std::is_base_of_v<Relations::BaseTable, REL> ) {
    BOOST_CHECK( OK && rel.relations().size() == N );
  } else {
    BOOST_CHECK( OK && rel.capacity() == N );
  }
}

template <typename REL, std::size_t N>
auto NOINLINE create_unique() {
  auto r = std::make_unique<REL>();
  fill<N>( *r );
  DataObject* d = r.get();
  d->addRef();
  return r;
}

template <typename REL, std::size_t N>
REL NOINLINE create_instance() {
  REL r;
  fill<N>( r );
  DataObject* d = &r;
  d->addRef();
  return r;
}

constexpr std::size_t N = 5;

BOOST_AUTO_TEST_CASE( test_relations_fill ) {
  auto t1 = create_unique<LHCb::Relation1D<int, float>, N>();
  auto t2 = create_unique<LHCb::Relation2D<unsigned int, int>, N>();
  auto t3 = create_unique<LHCb::RelationWeighted1D<int, float, double>, N>();
  auto t4 = create_unique<LHCb::RelationWeighted2D<int, float, double>, N>();
}

template <typename REL>
void NOINLINE test_dataobject_delete() {
  auto        t = create_unique<REL, N>();
  DataObject* d = t.release();
  delete d;
}

BOOST_AUTO_TEST_CASE( test_relations_dataobject_delete ) {
  test_dataobject_delete<LHCb::Relation1D<int, float>>();
  test_dataobject_delete<LHCb::Relation2D<unsigned int, int>>();
  test_dataobject_delete<LHCb::RelationWeighted1D<int, float, double>>();
  test_dataobject_delete<LHCb::RelationWeighted2D<int, float, double>>();
  // note test here is if there are memory leaks.. so leak sanitizer will find them..
}

template <typename REL>
void NOINLINE test_relation_copy() {
  auto t = create_unique<REL, N>();
  auto c = *t;
  BOOST_CHECK( t.get() != &c );
  BOOST_CHECK( t->relations().size() == c.relations().size() );
}

BOOST_AUTO_TEST_CASE( test_relations_copy ) {
  test_relation_copy<LHCb::Relation1D<int, float>>();
  test_relation_copy<LHCb::Relation2D<unsigned int, int>>();
  test_relation_copy<LHCb::RelationWeighted1D<int, float, double>>();
  test_relation_copy<LHCb::RelationWeighted2D<int, float, double>>();
}

template <typename TYPE>
auto NOINLINE take_return( TYPE&& t ) {
  auto m = std::make_unique<TYPE>( std::forward<TYPE>( t ) );
  return m.release();
}

template <typename REL>
void NOINLINE test_relation_move() {
  auto m = std::unique_ptr<REL>( take_return( create_instance<REL, N>() ) );
  BOOST_CHECK( N == m->relations().size() );
}

BOOST_AUTO_TEST_CASE( test_relations_move ) {
  test_relation_move<LHCb::Relation1D<int, float>>();
  test_relation_move<LHCb::Relation2D<unsigned int, int>>();
  test_relation_move<LHCb::RelationWeighted1D<int, float, double>>();
  test_relation_move<LHCb::RelationWeighted2D<int, float, double>>();
  // secondary test here is if the leak sanitizer finds anything...
}
