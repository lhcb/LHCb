/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <cstdint>

namespace AllocationTracker {
  struct allocation_count {
    void add_allocation( std::size_t size, uint64_t ticks ) {
      m_allocated_size += size;
      m_ticks_spent_allocating += ticks;
      ++m_num_allocations;
    }
    [[nodiscard]] std::size_t num_bytes() const { return m_allocated_size; }
    [[nodiscard]] uint64_t    num_ticks() const { return m_ticks_spent_allocating; }
    [[nodiscard]] std::size_t num_allocations() const { return m_num_allocations; }
    allocation_count&         operator+=( allocation_count const& rhs ) {
      m_allocated_size += rhs.m_allocated_size;
      m_num_allocations += rhs.m_num_allocations;
      m_ticks_spent_allocating += rhs.m_ticks_spent_allocating;
      return *this;
    }
    friend allocation_count operator+( allocation_count lhs, allocation_count const& rhs ) { return lhs += rhs; }

  private:
    std::size_t m_allocated_size{ 0 };
    std::size_t m_num_allocations{ 0 };
    uint64_t    m_ticks_spent_allocating{ 0 };
  };
} // namespace AllocationTracker
