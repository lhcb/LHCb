/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/extend_interfaces.h"

/** @file  IAllocationTracker.h
 *  @brief TODO
 */

/** @class IAllocationTracker
 *  @brief TODO
 */
struct IAllocationTracker : extend_interfaces<IInterface> {
  DeclareInterfaceID( IAllocationTracker, 1, 0 );

  /** Update the count of events that have been processed while tracking is
   *  enabled, and the number of RDTSC ticks that processing them took.
   *
   *  This is used to normalise results.
   */
  virtual void incrementEventCount( std::size_t num_events, uint64_t num_ticks ) = 0;

  /** Prepare to track allocations in up to 'num_slots' different slots.
   *
   *  This method is *not* required to be thread-safe and should only be called
   *  *before* multiple threads are spawned.
   *
   *  @param num_slots Number of event slots that will be used.
   */
  virtual void reserveSlots( std::size_t num_slots ) = 0;

  /** Being recording dynamic allocation statistics.
   */
  virtual void beginTracking() = 0;

  /** Stop recording dynamic allocation statistics.
   */
  virtual void endTracking() = 0;
};
