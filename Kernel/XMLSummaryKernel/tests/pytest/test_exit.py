###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from pathlib import Path

from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    """
    # Author: rlambert
    # Purpose: Check what the XMLSummary stores when the program exits with non-zero exit code
    """

    command = ["gaudirun.py", "../../options/test-XMLSummary.py:exit"]
    environment = ["XMLSUMM_TEST_OUT=RootDst-exit.root"]
    returncode = -11

    def test_xml_content(self, cwd: Path):
        os.environ["XMLSUMM_TEST_OUT"] = "RootDst-exit.root"
        with open(
            os.getenv("XMLSUMMARYKERNELROOT") + "/options/test-XMLSummary.py", "r"
        ) as file:
            exec(file.read(), globals())
        check(
            os.environ["XMLSUMMARYKERNELROOT"] + "/tests/refs/test-XMLSummary-exit.xml",
            cwd / "summary-exit.xml",
        )
