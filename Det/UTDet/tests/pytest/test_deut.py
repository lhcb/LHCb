###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from GaudiTesting import NO_ERROR_MESSAGES
from LHCbTesting import LHCbExeTest


def is_dd4hep():
    from DDDB.CheckDD4Hep import UseDD4Hep

    return UseDD4Hep


@pytest.mark.skipif(not is_dd4hep(), reason="test only for DD4hep")
class Test(LHCbExeTest):
    command = ["gaudirun.py", "../options/test_deut.py"]
    reference = {"messages_count": NO_ERROR_MESSAGES}

    def test_stdout(self, stdout: bytes):
        assert (
            b"DeUTTester                             INFO DeUT volume: lvUT"
        ) in stdout
