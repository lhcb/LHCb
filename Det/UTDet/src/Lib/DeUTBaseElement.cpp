/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTDet/DeUTBaseElement.h"
#include "DetDesc/IGeometryInfo.h"
#include "GaudiKernel/IUpdateManagerSvc.h"

/** @file DeUTBaseElement.cpp
 *
 *  Implementation of class :  DeUTBaseElement
 *
 *    @author Andy Beiter (based on code by Matthew Needham)
 */

StatusCode DeUTBaseElement::initialize() {
  // initialize method
  return DetectorElementPlus::initialize().andThen(
      [&] { return registerCondition( this, geometry(), &DeUTBaseElement::cachePoint, true ); } );
}

StatusCode DeUTBaseElement::cachePoint() {
  m_globalCentre = toGlobal( Gaudi::XYZPoint{ 0, 0, 0 } );
  return StatusCode::SUCCESS;
}

Gaudi::XYZPoint DeUTBaseElement::globalPoint( double x, double y, double z ) const {
  return toGlobal( Gaudi::XYZPoint{ x, y, z } );
}
