###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# deduce the detectors list from the available configurables
import Configurables

AVAILABLE_DETECTORS = [
    name.replace("TestLoadDD4HEP", "")
    for name in Configurables.__all__
    if name.startswith("TestLoadDD4HEP")
]


def config():
    import PyConf
    from PyConf.Algorithms import LHCb__Tests__FakeRunNumberProducer as FRNP
    from PyConf.application import ApplicationOptions, configure, configure_input
    from PyConf.control_flow import CompositeNode

    options = ApplicationOptions(_enabled=False)
    options.data_type = "Upgrade"
    options.evt_max = 1
    options.print_freq = 1
    options.input_type = "None"
    options.auditors = ["FPEAuditor"]
    options.msg_svc_format = "% F%60W%S%7W%R%T %0W%M"
    # unused part, but requested by PyConf for now
    options.simulation = True
    options.dddb_tag = "upgrade/master"
    options.conddb_tag = "upgrade/master"
    options.geometry_version = "run3/trunk"
    options.conditions_version = "master"
    config = configure_input(options)

    # Only load the detector we want
    dd4hepsvc = config["LHCb::Det::LbDD4hep::DD4hepSvc/LHCb::Det::LbDD4hep::DD4hepSvc"]
    dd4hepsvc.DetectorList = ["/world"] + AVAILABLE_DETECTORS

    TestAlgs = [
        getattr(PyConf.Algorithms, f"TestLoadDD4HEP{detector}")(
            name=f"TestLoadDD4HEP{detector}"
        )
        for detector in AVAILABLE_DETECTORS
    ]
    algs = [FRNP(name="FakeRunNumber", Start=1000, Step=20)] + TestAlgs
    config.update(configure(options, CompositeNode("TopSeq", algs)))


def test():
    """
    Test loading of DD4Hep detector elements.
    """
    from subprocess import PIPE

    from GaudiTests import run_gaudi

    proc = run_gaudi(f"{__file__}:config", check=True, stdout=PIPE, errors="replace")
    import re

    for detector in AVAILABLE_DETECTORS:
        assert re.search(f"TestLoadDD4HEP{detector}.*DEBUG Loaded", proc.stdout)
