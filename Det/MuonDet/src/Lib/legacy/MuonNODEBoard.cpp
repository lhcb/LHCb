/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/MuonNODEBoard.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonNODEBoard
//
// 2004-01-12 : Alessia Satta
//-----------------------------------------------------------------------------

MuonNODEBoard::MuonNODEBoard() : m_TSGridX( 24, 0 ), m_TSGridY( 24, 0 ), m_TSQuadrant( 24, 0 ), m_TSName( 24, "" ) {
  for ( int i = 0; i < 4; i++ ) { m_quadrant[i] = 0; }
}

StatusCode MuonNODEBoard::initialize() {
  return Condition::initialize().andThen( [&] {
    const auto ODESN = this->param<int>( "ODESerialNumber" );
    setNODESerialNumber( ODESN );
    const auto ODEECS = this->param<std::string>( "ODEECSName" );
    setECSName( ODEECS );
    const auto RNumb = this->param<int>( "RegionNumber" );
    setRegion( RNumb );
    const auto TSX = this->param<int>( "TSLayoutX" );
    setTSLayoutX( TSX );
    const auto TSY = this->param<int>( "TSLayoutY" );
    setTSLayoutY( TSY );
    const auto TSNum = this->param<int>( "TSNumber" );
    setTSNumber( TSNum );

    const auto TSGXList       = this->paramVect<int>( "TSGridXList" );
    const auto TSGYList       = this->paramVect<int>( "TSGridYList" );
    const auto TSQuadrantList = this->paramVect<int>( "TSQuadrantList" );
    m_unique_quadrant         = TSQuadrantList[0];
    const auto TSNameList     = this->paramVect<std::string>( "TSNameList" );
    if ( TSGXList.size() != (unsigned int)getTSNumber() ) return StatusCode::FAILURE;
    if ( TSGYList.size() != (unsigned int)getTSNumber() ) return StatusCode::FAILURE;
    if ( TSQuadrantList.size() != (unsigned int)getTSNumber() ) return StatusCode::FAILURE;
    if ( TSNameList.size() != (unsigned int)getTSNumber() ) return StatusCode::FAILURE;
    if ( TSNum > (int)m_TSGridX.size() ) {
      // set the minimum size .. should never happen
      m_TSName.resize( TSNum );
      m_TSGridX.resize( TSNum );
      m_TSGridY.resize( TSNum );
      m_TSQuadrant.resize( TSNum );
    }
    for ( int ele = 0; ele < TSNum; ele++ ) {
      m_TSGridX[ele]    = TSGXList[ele];
      m_TSGridY[ele]    = TSGYList[ele];
      m_TSQuadrant[ele] = TSQuadrantList[ele];
      m_TSName[ele]     = TSNameList[ele];
    }
    return StatusCode::SUCCESS;
  } );
}

StatusCode MuonNODEBoard::update( long tslayx, long tslayy, long tsnumb, std::vector<long> gridx,
                                  std::vector<long> gridy, std::vector<long> quadrant ) {
  m_TSLayoutX  = tslayx;
  m_TSLayoutY  = tslayy;
  m_TSNumber   = tsnumb;
  m_TSGridX    = gridx;
  m_TSGridY    = gridy;
  m_TSQuadrant = quadrant;
  return StatusCode::SUCCESS;
}

StatusCode MuonNODEBoard::addTSName( std::string name ) {
  if ( (int)m_TSName.size() <= m_TSNumber ) {
    m_TSName.push_back( name );
    return StatusCode::SUCCESS;
  } else {
    return StatusCode::FAILURE;
  }
}

void MuonNODEBoard::setQuadrants() {
  for ( int i = 0; i < m_TSNumber; i++ ) {
    unsigned int a = m_TSQuadrant[i];
    if ( i == 0 ) m_unique_quadrant = a;
    m_quadrant[a] = 1;
  }
}

bool MuonNODEBoard::isQuadrantContained( long quadrant ) { return m_quadrant[quadrant] > 0; }

bool MuonNODEBoard::isTSContained( LHCb::Detector::Muon::TileID TSTile ) {
  long quadrant = (long)TSTile.quarter();
  long gridx    = (long)TSTile.nX();
  long gridy    = (long)TSTile.nY();
  for ( int i = 0; i < m_TSNumber; i++ ) {
    if ( m_TSQuadrant[i] == quadrant ) {
      if ( m_TSGridX[i] == gridx ) {
        if ( m_TSGridY[i] == gridy ) { return true; }
      }
    }
  }
  return false;
}
