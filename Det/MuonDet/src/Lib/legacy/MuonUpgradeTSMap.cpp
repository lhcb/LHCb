/***************************************************************************** \
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/MuonUpgradeTSMap.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonUpgradeTSMap
//
// 2004-01-13 : Alessia Satta
//-----------------------------------------------------------------------------

MuonUpgradeTSMap::MuonUpgradeTSMap()
    : m_OutputLayoutSequence( 48, 0 )
    , m_OutputGridXSequence( 48, 0 )
    , m_OutputGridYSequence( 48, 0 )
    , m_OutputSynchSequence( 48, 0 ) {}

StatusCode MuonUpgradeTSMap::initialize() {
  return Condition::initialize().andThen( [&] {
    const auto numLogLayout = this->param<int>( "NumberLogLayout" );
    m_NumberLogLayout       = numLogLayout;
    const auto GList        = this->paramVect<int>( "GridList" );
    if ( GList.size() != (unsigned)( 2 * m_NumberLogLayout ) ) return StatusCode::FAILURE;
    if ( m_NumberLogLayout == 1 ) {
      m_GridXLayout[0] = GList[0];
      m_GridYLayout[0] = GList[1];
    } else if ( m_NumberLogLayout == 2 ) {
      m_GridXLayout[0] = GList[0];
      m_GridYLayout[0] = GList[2];
      m_GridXLayout[1] = GList[1];
      m_GridYLayout[1] = GList[3];
    } else { // no more than 2 logical readout are non possible
      return StatusCode::FAILURE;
    }

    const auto activeOutput = this->param<int>( "ActiveOutputSignal" );
    m_OutputSignal          = activeOutput;
    const auto outputSeq    = this->paramVect<int>( "OutputSequence" );
    const auto outputGXSeq  = this->paramVect<int>( "OutputGridXSeq" );
    const auto outputGYSeq  = this->paramVect<int>( "OutputGridYSeq" );
    const auto outputLaySeq = this->paramVect<int>( "OutputLayoutSeq" );
    int        connected    = 0;

    for ( auto i = outputSeq.begin(); i < outputSeq.end(); i++ ) {
      if ( ( *i ) == 1 ) connected++;
    }
    if ( connected != activeOutput ) return StatusCode::FAILURE;
    if ( outputSeq.size() > 48 ) {
      // should never happen
      unsigned int tmp = outputSeq.size();
      m_OutputLayoutSequence.resize( tmp );
      m_OutputGridXSequence.resize( tmp );
      m_OutputGridYSequence.resize( tmp );
      m_OutputSynchSequence.resize( tmp );
    }
    m_OutputSynchSequence  = outputSeq;
    m_OutputGridXSequence  = outputGXSeq;
    m_OutputGridYSequence  = outputGYSeq;
    m_OutputLayoutSequence = outputLaySeq;

    return StatusCode::SUCCESS;
  } );
}

StatusCode MuonUpgradeTSMap::update( long output, std::vector<int> lay, std::vector<int> gridx, std::vector<int> gridy,
                                     std::vector<int> synch ) {
  m_OutputSignal         = output;
  m_OutputLayoutSequence = lay;
  m_OutputGridXSequence  = gridx;
  m_OutputGridYSequence  = gridy;
  m_OutputSynchSequence  = synch;
  return StatusCode::SUCCESS;
}
