/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP

#  include "Detector/Muon/DAQConstants.h"

#endif

// below lines should be removed when DetDesc compatibility is removed

static const unsigned int MuonUpgradeDAQHelper_maxTell40Number    = 22;
static const unsigned int MuonUpgradeDAQHelper_maxNODENumber      = 144;
static const unsigned int MuonUpgradeDAQHelper_linkNumber         = 24;
static const unsigned int MuonUpgradeDAQHelper_frameNumber        = 4;
static const unsigned int MuonUpgradeDAQHelper_maxTell40PCINumber = 2;
static const unsigned int MuonUpgradeDAQHelper_ODEFrameSize       = 48;
static const unsigned int MuonUpgradeDAQHelper_maxTell40PCINumber_linkNumber_ODEFrameSize =
    MuonUpgradeDAQHelper_maxTell40PCINumber * MuonUpgradeDAQHelper_linkNumber * MuonUpgradeDAQHelper_ODEFrameSize;
static const unsigned int MuonUpgradeDAQHelper_linkNumber_ODEFrameSize =
    MuonUpgradeDAQHelper_linkNumber * MuonUpgradeDAQHelper_ODEFrameSize;
