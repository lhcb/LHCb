/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/Condition.h"
#include "MuonDet/CLID_MuonUpgradeTSMap.h"

#include "GaudiKernel/DataObject.h"

#include <vector>

/**
 *  @author Alessia Satta
 *  @date   2004-01-05
 */
class MuonUpgradeTSMap : public Condition {
public:
  MuonUpgradeTSMap();

  /// Class ID of this class
  inline static const CLID& classID() { return CLID_MuonUpgradeTSMap; }

  StatusCode initialize() override;
  using Condition::update;
  StatusCode update( long output, std::vector<int> lay, std::vector<int> gridx, std::vector<int> gridy,
                     std::vector<int> synch );

  inline long numberOfLayout() { return m_NumberLogLayout; }
  inline long gridXLayout( int i ) { return m_GridXLayout[i]; }
  inline long gridYLayout( int i ) { return m_GridYLayout[i]; }
  inline long numberOfOutputSignal() { return m_OutputSignal; }
  inline int  layoutOutputChannel( int i ) { return m_OutputLayoutSequence[i]; }
  inline int  gridXOutputChannel( int i ) { return m_OutputGridXSequence[i]; }
  inline int  gridYOutputChannel( int i ) { return m_OutputGridYSequence[i]; }
  inline long synchChSize() { return m_OutputSynchSequence.size(); }
  inline bool synchChUsed( int i ) { return m_OutputSynchSequence[i] == 0 ? false : true; }
  inline long numberOfPad() { return m_pad; }

protected:
private:
  long             m_NumberLogLayout;
  long             m_GridXLayout[2];
  long             m_GridYLayout[2];
  long             m_OutputSignal;
  std::vector<int> m_OutputLayoutSequence;
  std::vector<int> m_OutputGridXSequence;
  std::vector<int> m_OutputGridYSequence;
  std::vector<int> m_OutputSynchSequence;
  long             m_pad;
};
