/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/Condition.h"
#include "MuonDet/CLID_MuonTell40Board.h"

#include "GaudiKernel/DataObject.h"

#include <vector>

/**
 *  @author Alessia Satta
 *  @date   2004-01-05
 */
class MuonTell40Board : public Condition {
public:
  StatusCode initialize() override;
  /// Class ID of this class
  static const CLID& classID() { return CLID_MuonTell40Board; }
  long               Tell40Number() { return m_Tell40Number; };
  void               setTell40Number( long number ) { m_Tell40Number = number; };
  void               setTell40Station( long number ) { m_station = number; };
  int                getStation() { return m_station; };

  long        numberOfPCI() { return m_numberOfPCI; };
  void        setNumberOfPCI( long pci ) { m_numberOfPCI = pci; };
  void        addPCI( std::string name );
  std::string getPCIName( int i ) { return m_PCIName[i]; };

private:
  long                     m_station;
  long                     m_numberOfPCI = 0;
  long                     m_Tell40Number;
  std::vector<std::string> m_PCIName;
};
