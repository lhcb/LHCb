/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/Condition.h"
#include "GaudiAlg/FunctionalUtilities.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "LHCbAlgs/Consumer.h"
#include "LHCbAlgs/Producer.h"
#include "Magnet/DeMagnet.h"
#include <DetDesc/GenericConditionAccessorHolder.h>

struct MagFielSvcTester final : LHCb::Algorithm::Producer<int()> {

  MagFielSvcTester( const std::string& name, ISvcLocator* svcLoc )
      : Producer( name, svcLoc, KeyValue( "OutputLocation", "/Event/MagFieldSvcTestResult" ) ) {}

  int operator()() const override {

    auto svcLocator = Gaudi::svcLocator();
    if ( !svcLocator ) {
      throw GaudiException( "ISvcLocator* points to nullptr!", "MagFielSvcTester", StatusCode::FAILURE );
    }
    auto magFieldSvc = svcLocator->service<ILHCbMagnetSvc>( "MagneticFieldSvc" );

    info() << "MagFielSvc loaded" << endmsg;

    ROOT::Math::XYZPoint  position{ 0, 0, 400 };
    ROOT::Math::XYZVector field{ 0, 0, 0 };
    magFieldSvc->fieldVector( position, field ).ignore();
    info() << "MagFielSvc:" << field << endmsg;

    return m_value;
  }

  Gaudi::Property<int> m_value{ this, "Value", 1, "Test outcome" };
};

DECLARE_COMPONENT( MagFielSvcTester )

struct DeMagnetTester
    : LHCb::Algorithm::Consumer<void( const DeMagnet& ), LHCb::Algorithm::Traits::usesConditions<DeMagnet>> {

  DeMagnetTester( const std::string& name, ISvcLocator* loc )
      : Consumer{ name, loc, { KeyValue{ "Magnet", LHCb::Det::Magnet::det_path } } } {}

  void operator()( const DeMagnet& magnet ) const override {

    ROOT::Math::XYZPoint  position{ 0.0, 0.0, 400.0 };
    ROOT::Math::XYZVector field{ 0.0, 0.0, 0.0 };

    // Now using the DeMagnet to get the same value
    ROOT::Math::XYZVector field_fromDet = magnet.fieldVector( position );
    info() << "DeMagnet:" << field_fromDet << endmsg;
  }
};

DECLARE_COMPONENT( DeMagnetTester )
