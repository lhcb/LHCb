###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "$MAGNETROOT/tests/options/test_demagnet.py"]
    reference = "../refs/test_demagnet.yaml"

    def test_stdout(self, stdout: bytes):
        expected_string = b"DeMagnet:(-7.14465e-08,-1.52186e-05,1.30232e-06)"
        assert stdout.find(expected_string) != -1, (
            f"Expected string missing: {expected_string}"
        )
