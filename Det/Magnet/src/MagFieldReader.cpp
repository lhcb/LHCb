/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <GaudiAlg/GaudiTupleAlg.h>
#include <GaudiKernel/Point3DTypes.h>
#include <GaudiKernel/RndmGenerators.h>
#include <GaudiKernel/SystemOfUnits.h>
#include <GaudiKernel/Vector3DTypes.h>
#include <Kernel/IBIntegrator.h>
#include <LHCbAlgs/Consumer.h>
#include <Magnet/DeMagnet.h>
#include <string>

/** @class MagFieldReader MagFieldReader.h
 *  @param An Algorithm to read and plot magnetic field maps
 *  @param for x and y kept constant and varying z. The x, y
 *  @param positions and the z range can be set in job options.
 *
 *  @author Edgar De Oliveira
 *  @date   08/05/2002
 */

class MagFieldReader
    : public LHCb::Algorithm::Consumer<void( const DeMagnet& ),
                                       LHCb::Algorithm::Traits::usesBaseAndConditions<GaudiTupleAlg, DeMagnet>> {
public:
  MagFieldReader( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, { KeyValue{ "Magnet", LHCb::Det::Magnet::det_path } } ) {}

  void operator()( const DeMagnet& magnet ) const override;

private:
  void TestBdl( const LHCb::Magnet::MagneticFieldGrid* field ) const;

  ToolHandle<IBIntegrator> m_bIntegrator{ this, "BIntegrator", "BIntegrator" };

  Gaudi::Property<double> m_xMin{ this, "Xmin", 0.0 * Gaudi::Units::mm, "Low x of the magnetic field range" };
  Gaudi::Property<double> m_xMax{ this, "Xmax", 4000.0 * Gaudi::Units::mm, "High x of the magnetic field range" };
  Gaudi::Property<double> m_yMin{ this, "Ymin", 0.0 * Gaudi::Units::mm, "Low y of the magnetic field range" };
  Gaudi::Property<double> m_yMax{ this, "Ymax", 4000.0 * Gaudi::Units::mm, "High y of the magnetic field range" };
  Gaudi::Property<double> m_zMin{ this, "Zmin", -500.0 * Gaudi::Units::mm, "Low z of the magnetic field range" };
  Gaudi::Property<double> m_zMax{ this, "Zmax", 14000.0 * Gaudi::Units::mm, "High z of the magnetic field range" };
  Gaudi::Property<double> m_step{ this, "Step", 100.0 * Gaudi::Units::mm, "Step of the magnetic field" };

  Gaudi::Property<bool> m_testbdl{ this, "TestFieldInt", false, "Test field integral for random \"track\" slopes" };
  Gaudi::Property<int>  m_nInt{ this, "NInt", 1000, "number of field integrals to generate" };
};

DECLARE_COMPONENT( MagFieldReader )

void MagFieldReader::operator()( const DeMagnet& magnet ) const {

  if ( m_testbdl.value() ) TestBdl( magnet.fieldGrid() );

  Tuple nt1 = nTuple( 10, "Field", CLID_ColumnWiseTuple );

  Gaudi::XYZVector B( 0.0, 0.0, 0.0 );

  for ( double z = m_zMin; z <= m_zMax; z += m_step ) {
    for ( double y = m_yMin; y <= m_yMax; y += m_step ) {
      for ( double x = m_xMin; x <= m_xMax; x += m_step ) {
        Gaudi::XYZPoint P( x, y, z );
        B = magnet.fieldVector( P );
        // fill ntuple
        nt1->column( "x", P.x() / Gaudi::Units::cm ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        nt1->column( "y", P.y() / Gaudi::Units::cm ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        nt1->column( "z", P.z() / Gaudi::Units::cm ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        nt1->column( "Bx", B.x() / Gaudi::Units::tesla ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        nt1->column( "By", B.y() / Gaudi::Units::tesla ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        nt1->column( "Bz", B.z() / Gaudi::Units::tesla ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

        nt1->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      }
    }

    Gaudi::XYZPoint P0( 0.0, 0.0, z );
    Gaudi::XYZPoint P02( 0.0, 0.0, z );
    B = magnet.fieldVector( P0 );

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Magnetic Field at (" << P0.x() << ", " << P0.y() << ", " << P0.z()
              << " ) = " << ( B.x() ) / Gaudi::Units::tesla << ", " << ( B.y() ) / Gaudi::Units::tesla << ", "
              << ( B.z() ) / Gaudi::Units::tesla << " Tesla " << endmsg;
  }
}

void MagFieldReader::TestBdl( const LHCb::Magnet::MagneticFieldGrid* field ) const {

  Tuple nt2 = nTuple( 20, "Field Integral", CLID_ColumnWiseTuple );

  double           sigtx( 0.3 );
  double           sigty( 0.25 ); // slopes at start
  double           zC;            // z centre of field returned by tool
  Gaudi::XYZVector bdl;

  // random number generation
  Rndm::Numbers gausstx( randSvc(), Rndm::Gauss( 0., sigtx / 2 ) );
  Rndm::Numbers gaussty( randSvc(), Rndm::Gauss( 0., sigty / 2 ) );

  for ( int i = 0; i < m_nInt; i++ ) {
    double tx = gausstx();
    double ty = gaussty();
    if ( fabs( tx ) < sigtx && fabs( ty ) < sigty ) {

      m_bIntegrator->calculateBdlAndCenter( field, { 0, 0, 0 }, { 0, 0, 9000 }, tx, ty, zC, bdl );

      nt2->column( "tx", tx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      nt2->column( "ty", ty ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      nt2->column( "Bdlx", bdl.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      nt2->column( "Bdly", bdl.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      nt2->column( "Bdlz", bdl.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      nt2->column( "zCenter", zC ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      nt2->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  }
}
