###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import LineSkipper, RegexpReplacer


class Test(LHCbExeTest):
    command = ["gaudirun.py"]
    reference = "../../refs/VeloAlignCondUpdatesYaml.yaml"
    preprocessor = (
        LHCbExeTest.preprocessor
        + LineSkipper(
            [
                "UpdateManagerSvc  VERBOSE Unregister object at",
                "UpdateManagerSvc  VERBOSE Trying to unregister object at",
                "UpdateManagerSvc  VERBOSE Invalidate object at",
            ]
        )
        + RegexpReplacer(
            r"using checked out files in .*test/DB",
            "using checked out files in test/DB",
        )
    )

    def options(self):
        import os

        import GaudiKernel.SystemOfUnits as unit
        from Configurables import (
            ApplicationMgr,
            CondDB,
            DDDBConf,
            UpdateManagerSvc,
        )
        from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as ReserveIOV
        from Configurables import LHCb__DetDesc__VeloMotionSystemFromYaml as YamlMS
        from Configurables import LHCb__Tests__FakeEventTimeProducer as FET
        from Configurables import LHCb__Tests__FakeRunNumberProducer as FRN
        from Gaudi.Configuration import DEBUG, VERBOSE, appendPostConfigAction

        ConditionPaths = [
            "/dd/Conditions/Online/Velo/MotionSystem",
            "/dd/Conditions/Alignment/Velo/VeloLeft",
            "/dd/Conditions/Alignment/Velo/VeloRight",
        ]

        DDDBConf()  # detector description
        CondDB().addLayer(
            os.path.join(
                os.environ.get("TEST_OVERLAY_ROOT", "Det/VeloDet/test/DB"), "updates"
            )
        )  # use local test DB

        initialTime = 10 * unit.s
        timeStep = 10 * unit.s

        # This is needed to trigger the instantiation of the update manager service
        from Configurables import DetCondTest__TestConditionAlg

        alg = DetCondTest__TestConditionAlg()
        alg.Conditions = ConditionPaths

        odin_with_time = "/Event/DummyODIN-time"
        odin_with_run = "/Event/DummyODIN-run"

        ApplicationMgr(
            TopAlg=[
                FET(
                    "FakeEventTime",
                    ODIN=odin_with_time,
                    Start=initialTime / 1e9,
                    Step=timeStep / 1e9,
                ),
                FRN("FakeRunNumber", ODIN=odin_with_run, Start=0, Step=1),
                YamlMS(
                    "MotionSystemFromYaml",
                    OutputLevel=DEBUG,
                    ODIN=odin_with_run,
                    YAMLPath="{TEST_YAML_ROOT}/Conditions/VP/Motion.yml".format(
                        **os.environ
                    ),
                    Condition=ConditionPaths[0],
                ),
                ReserveIOV("ReserveIOV", ODIN=odin_with_time),
                alg,
            ],
            EvtSel="NONE",
            EvtMax=8,
        )

        # MessageSvc(OutputLevel = 1)

        UpdateManagerSvc(
            OutputLevel=VERBOSE,
            WithoutBeginEvent=True,
        )

        # MessageSvc(setVerbose = ["VeloAlignCond"])

        @appendPostConfigAction
        def remove_MagneticFieldSvc():
            """override some settings from DDDBConf"""
            appMgr = ApplicationMgr()
            appMgr.ExtSvc = [
                svc for svc in appMgr.ExtSvc if "MagneticFieldSvc" not in str(svc)
            ]
