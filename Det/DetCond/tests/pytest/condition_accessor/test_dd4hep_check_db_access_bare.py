###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import LineSkipper


@pytest.mark.ctest_fixture_required("detcond.condition_accessor.prepare")
@pytest.mark.shared_cwd("DetCond")
class Test(LHCbExeTest):
    command = [
        "gaudirun.py",
        "../../options/dd4hep_check_db_access.py",
        "../../options/new_cond_bare_repo.py",
    ]
    reference = "../../refs/dd4hep_check_db_access_bare.yaml"
    preprocessor = LHCbExeTest.preprocessor + LineSkipper(
        regexps=[
            r"^(Compact|Conditions)Loader",
            r"^Statistics",
            r"^LHCb::Det::LbDD",
            r"^DetectorData.*INFO Using repository",
        ]
    )
