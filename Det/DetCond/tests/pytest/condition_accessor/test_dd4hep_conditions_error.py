###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "../../options/dd4hep_cond_error.py"]
    reference = "../../refs/dd4hep_conditions_error.yaml"
    returncode = 6

    def test_stderr(self, stdout: bytes):
        import re

        stdout = re.sub(
            rb"@.*FunctionalConditionAccessor.cpp:\d+",
            b"@[...]/FunctionalConditionAccessor.cpp:###",
            stdout,
        )
        expected = b"ConditionsDependencyHandler: Failed to resolve conditon:124D33D0A3526E3B @[...]/FunctionalConditionAccessor.cpp:### (input 0)"
        assert expected in stdout, f"Expected standard error missing: {expected}"
