###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import LineSkipper, RegexpReplacer


class Test(LHCbExeTest):
    command = ["gaudirun.py", "../../options/dd4hep_cond_no_prop.py"]
    reference = "../../refs/dd4hep_cond_as_yaml.yaml"
    preprocessor = (
        LHCbExeTest.preprocessor
        + LineSkipper(
            regexps=[
                r"^(Compact|Conditions)Loader",
                r"^Statistics",
                r"^DetectorData.*INFO Using repository",
            ]
        )
        + RegexpReplacer(
            r"INFO Loading DD4hep Geometry: .*/test/",
            "INFO Loading DD4hep Geometry: .../test/",
        )
        + RegexpReplacer(
            r"INFO Using conditions location: file://.*/test/",
            "INFO Using conditions location: file://.../test/",
        )
    )
