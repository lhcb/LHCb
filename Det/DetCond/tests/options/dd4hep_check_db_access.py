###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

from Configurables import DDDBConf

from PyConf.application import ApplicationOptions, configure, configure_input
from PyConf.components import setup_component
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.evt_max = 3
options.input_type = "None"
options.data_type = "Upgrade"
options.simulation = True
options.geometry_version = "DD4TESTCOND"
options.conditions_version = "v1"

config = configure_input(options)

dd4hepsvc = config["LHCb::Det::LbDD4hep::DD4hepSvc/LHCb::Det::LbDD4hep::DD4hepSvc"]
# This variable should be set by the test environment to the location of the test repository
dd4hepsvc.GeometryLocation = "${TEST_DBS_ROOT}"
dd4hepsvc.GeometryMain = "geo.xml"
dd4hepsvc.ConditionsLocation = os.environ["GIT_TEST_REPOSITORY"]
dd4hepsvc.DetectorList = ["/world"]

from Configurables import LHCb__Tests__FakeRunNumberProducer as FET

from PyConf.Algorithms import (
    LHCb__DetCond__Examples__Functional__CondAccessExample as CondAlg,
)


def fake_run_number():
    return FET("FakeRunNumberProducer", Start=42, Step=20)


cf_node = CompositeNode("TopSeq", [CondAlg(name="CondAlg")])
config.update(configure(options, cf_node, make_fake_odin=fake_run_number))
