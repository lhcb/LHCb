###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

# Prepare detector description
##############################
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
from Gaudi.Configuration import *

dd4hepSvc = DD4hepSvc(
    GeometryLocation="${TEST_DBS_ROOT}",
    GeometryVersion="DD4TESTCOND",
    GeometryMain="geo.xml",
    ConditionsLocation="file://${TEST_DBS_ROOT}/DD4TESTCOND/",
    DetectorList=["/world"],
)

condval = "{ par1 : 1001.3, par2 : 1002.1, parv : [ 123 , 456 ] }"
DD4hepSvc().ConditionsOverride = {"/world:TestConditionYML": condval}

# Main application
##################
app = ApplicationMgr(EvtSel="NONE", EvtMax=3, OutputLevel=INFO)

# Configure fake run number
###########################
# Add the ReserveIOVDD4hep which creates a fake ODIN bank from the location specified
from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
from Configurables import (
    LHCb__DetCond__Examples__Functional__CondAccessExample as CondAlg,
)
from Configurables import (
    LHCb__DetCond__Examples__Functional__CondAccessExampleYamlNoProp as CondAlgYAMLNoProp,
)
from Configurables import LHCb__Tests__FakeRunNumberProducer as FET

# make sure that we do have the condition property
alg1 = CondAlg("CondAlg1", CondPath="/world:TestConditionYML")
alg2 = CondAlgYAMLNoProp("CondAlg2")
alg3 = CondAlg("CondAlg3")

odin_path = "/Event/DummyODIN"
app.TopAlg = [
    FET("FakeRunNumber", ODIN=odin_path, Start=42, Step=20),
    IOVProducer("ReserveIOVDD4hep", ODIN=odin_path),
    alg1,
    alg2,
    alg3,
]
