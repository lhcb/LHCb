###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/DetCond
-----------
#]=======================================================================]

if(NOT USE_DD4HEP)
  set(DetDescSrcs
    src/LoadDDDB.cpp
    src/RunStampCheck.cpp
    src/TestConditionAlg.cpp
  )
  set(DetDescDeps
    LHCb::DetDescLib
  )
endif()

gaudi_add_module(DetCond
    SOURCES
        ${DetDescSrcs}
        examples/FunctionalConditionAccessor.cpp
    LINK
        Gaudi::GaudiKernel
        LHCb::LHCbAlgsLib
        ${DetDescDeps}
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        yaml-cpp
)
if(NOT USE_DD4HEP)
    gaudi_install(PYTHON)
    gaudi_generate_confuserdb()
    lhcb_add_confuser_dependencies(
        GaudiCoreSvc:GaudiCoreSvc
    )
endif()

# Prepare test Git CondDB overlay
foreach(_db IN ITEMS TESTCOND DD4TESTCOND)
    file(REMOVE_RECURSE ${CMAKE_CURRENT_BINARY_DIR}/test/${_db})
    file(COPY tests/data/${_db}/ DESTINATION test/${_db}/)
    execute_process(COMMAND git init -q ${CMAKE_CURRENT_BINARY_DIR}/test/${_db})
endforeach()

if(USE_DD4HEP)
    # DD4Hep plugin used for derived condition testing
    add_library(FakePlugins MODULE tests/src/fake_cond.cpp)
    target_link_libraries(FakePlugins Detector::DetectorLib LHCb::DetDescLib)
    install(TARGETS FakePlugins DESTINATION lib OPTIONAL)
    add_custom_command(TARGET FakePlugins POST_BUILD
        COMMAND run $<TARGET_FILE:DD4hep::listcomponents> -o ${CMAKE_CURRENT_BINARY_DIR}/FakePlugins.components $<TARGET_FILE_NAME:FakePlugins>
    )
endif()

if(NOT BUILD_TESTING)
    return()
endif()

gaudi_add_pytest(tests/pytest)

list(APPEND test_dbs_root
    DetCond.pytest.condition_accessor.test_begin_event_and_reserve
    DetCond.pytest.condition_accessor.test_begin_event_no_reserve
    DetCond.pytest.condition_accessor.test_begin_event_no_reserve_sharedcond
    DetCond.pytest.condition_accessor.test_dd4hep_check_db_access
    DetCond.pytest.condition_accessor.test_dd4hep_check_db_access_bare
    DetCond.pytest.condition_accessor.test_dd4hep_cond_as_json
    DetCond.pytest.condition_accessor.test_dd4hep_cond_as_yaml
    DetCond.pytest.condition_accessor.test_dd4hep_cond_no_prop
    DetCond.pytest.condition_accessor.test_dd4hep_conditions
    DetCond.pytest.condition_accessor.test_dd4hep_conditions_error
    DetCond.pytest.condition_accessor.test_dd4hep_override_conditions
    DetCond.pytest.condition_accessor.test_dd4hep_sharedconditions
    DetCond.pytest.condition_accessor.test_detdesc_cond_as_json
    DetCond.pytest.condition_accessor.test_detdesc_cond_as_yaml
    DetCond.pytest.condition_accessor.test_detdesc_cond_no_prop
    DetCond.pytest.condition_accessor.test_no_begin_event
)

list(APPEND git_test_repository
    DetCond.pytest.condition_accessor.test_dd4hep_check_db_access
    DetCond.pytest.condition_accessor.test_dd4hep_check_db_access_bare
    DetCond.pytest.condition_accessor.test_prepare
)

if(USE_DD4HEP)
    # disable DetDesc specific tests
    list(APPEND tests_to_disable
        DetCond.pytest.condition_accessor.test_begin_event_and_reserve
        DetCond.pytest.condition_accessor.test_begin_event_no_reserve
        DetCond.pytest.condition_accessor.test_begin_event_no_reserve_sharedcond
        DetCond.pytest.condition_accessor.test_detdesc_cond_as_json
        DetCond.pytest.condition_accessor.test_detdesc_cond_as_yaml
        DetCond.pytest.condition_accessor.test_detdesc_cond_no_prop
        DetCond.pytest.condition_accessor.test_no_begin_event
    )
else()
    # disable DD4HEP specific tests
    list(APPEND tests_to_disable
        DetCond.pytest.condition_accessor.test_dd4hep_check_db_access
        DetCond.pytest.condition_accessor.test_dd4hep_check_db_access_bare
        DetCond.pytest.condition_accessor.test_dd4hep_cond_as_json
        DetCond.pytest.condition_accessor.test_dd4hep_cond_as_yaml
        DetCond.pytest.condition_accessor.test_dd4hep_cond_no_prop
        DetCond.pytest.condition_accessor.test_dd4hep_conditions
        DetCond.pytest.condition_accessor.test_dd4hep_conditions_error
        DetCond.pytest.condition_accessor.test_dd4hep_override_conditions
        DetCond.pytest.condition_accessor.test_dd4hep_sharedconditions
    )
endif()

if(tests_to_disable)
    file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/disable_tests.cmake
        "set_tests_properties(${tests_to_disable} PROPERTIES DISABLED TRUE)"
    )
    set_property(DIRECTORY APPEND PROPERTY
        TEST_INCLUDE_FILES ${CMAKE_CURRENT_BINARY_DIR}/disable_tests.cmake)
endif()

if(git_test_repository OR test_dbs_root)
    file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/extra_env_variables.cmake
        "set_tests_properties(${git_test_repository} PROPERTIES ENVIRONMENT GIT_TEST_REPOSITORY=${CMAKE_CURRENT_BINARY_DIR}/repo)\n"
        "set_tests_properties(${test_dbs_root} PROPERTIES ENVIRONMENT TEST_DBS_ROOT=${CMAKE_CURRENT_BINARY_DIR}/test)"
    )
    set_property(DIRECTORY APPEND PROPERTY
        TEST_INCLUDE_FILES ${CMAKE_CURRENT_BINARY_DIR}/extra_env_variables.cmake)
endif()
