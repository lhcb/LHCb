###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/RichDet
-----------
#]=======================================================================]

if (USE_DD4HEP)
  set(BUILD_TESTING false) # prevent headers build test

  gaudi_add_library(RichDetLib
    SOURCES
        src/Lib/Rich1DTabFunc.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::LHCbKernel
)

else()

gaudi_add_library(RichDetLib
    SOURCES
        src/Lib/legacy/DeHorizRich.cpp
        src/Lib/legacy/DeRich.cpp
        src/Lib/legacy/DeRich1.cpp
        src/Lib/legacy/DeRich2.cpp
        src/Lib/legacy/DeRichAerogelRadiator.cpp
        src/Lib/legacy/DeRichBase.cpp
        src/Lib/legacy/DeRichBeamPipe.cpp
        src/Lib/legacy/DeRichGasRadiator.cpp
        src/Lib/legacy/DeRichHPD.cpp
        src/Lib/legacy/DeRichHPDPanel.cpp
        src/Lib/legacy/DeRichLocations.cpp
        src/Lib/legacy/DeRichMultiSolidRadiator.cpp
        src/Lib/legacy/DeRichPD.cpp
        src/Lib/legacy/DeRichPDPanel.cpp
        src/Lib/legacy/DeRichPMT.cpp
        src/Lib/legacy/DeRichPMTClassic.cpp
        src/Lib/legacy/DeRichPMTPanel.cpp
        src/Lib/legacy/DeRichPMTPanelClassic.cpp
        src/Lib/legacy/DeRichRadiator.cpp
        src/Lib/legacy/DeRichSingleSolidRadiator.cpp
        src/Lib/legacy/DeRichSphMirror.cpp
        src/Lib/legacy/DeRichSystem.cpp
        src/Lib/Rich1DTabFunc.cpp
        src/Lib/legacy/Rich1DTabProperty.cpp
        src/Lib/legacy/RichDetConfigType.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            GSL::gsl
            LHCb::DetDescLib
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::RichUtils
        PRIVATE
            VDT::vdt
            Boost::headers
)

gaudi_add_module(RichDet
    SOURCES
        src/component/XmlDeHorizRichCnv.cpp
        src/component/XmlDeRich1Cnv.cpp
        src/component/XmlDeRich2Cnv.cpp
        src/component/XmlDeRichAerogelRadiatorCnv.cpp
        src/component/XmlDeRichBeamPipeCnv.cpp
        src/component/XmlDeRichGasRadiatorCnv.cpp
        src/component/XmlDeRichHPDCnv.cpp
        src/component/XmlDeRichHPDPanelCnv.cpp
        src/component/XmlDeRichMultiSolidRadiatorCnv.cpp
        src/component/XmlDeRichPMTClassicCnv.cpp
        src/component/XmlDeRichPMTCnv.cpp
        src/component/XmlDeRichPMTPanelClassicCnv.cpp
        src/component/XmlDeRichPMTPanelCnv.cpp
        src/component/XmlDeRichSingleSolidRadiatorCnv.cpp
        src/component/XmlDeRichSphMirrorCnv.cpp
        src/component/XmlDeRichSystemCnv.cpp
    LINK
        LHCb::DetDescCnvLib
        LHCb::RichDetLib
)

gaudi_add_dictionary(RichDetDict
    HEADERFILES dict/RichDetDict.h
    SELECTION dict/RichDetDict.xml
    LINK LHCb::RichDetLib
)
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU"
   AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL 13.0)
    # Hide warnings produced by gcc 13 from dictionary:
    # Vc/common/malloc.h:99:14: warning: '<anonymous>' may be used uninitialized [-Wmaybe-uninitialized]
    target_compile_options(RichDetDict PRIVATE -Wno-maybe-uninitialized)
endif()

endif()
