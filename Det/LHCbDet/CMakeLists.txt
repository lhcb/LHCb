###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/LHCbDet
-----------
#]=======================================================================]

if(USE_DD4HEP)
  gaudi_add_header_only_library(LHCbDetLib
        LINK
            LHCb::DetDescLib
            LHCb::LbDD4hepLib
            LHCb::VPDetLib
  )
else()
  gaudi_add_header_only_library(LHCbDetLib
        LINK
            LHCb::DetDescLib
            LHCb::VPDetLib
  )
endif()

gaudi_add_module(LHCbDetExample
    SOURCES
        src/InteractionRegionExample.cpp
    LINK
        LHCbDetLib
        Gaudi::GaudiKernel
        Gaudi::GaudiAlgLib
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
)

if(BUILD_TESTING)
    gaudi_add_pytest(tests/pytest)
endif()
