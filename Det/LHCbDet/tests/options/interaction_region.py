###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import LHCb__Tests__FakeRunNumberProducer as DummyRunNumber
from DDDB.CheckDD4Hep import UseDD4Hep
from GaudiKernel.Constants import DEBUG

from PyConf.Algorithms import InteractionRegionExample
from PyConf.application import ApplicationOptions, configure, configure_input
from PyConf.control_flow import CompositeNode, NodeLogic


def runTest(options, runNumberStart=200):
    options = ApplicationOptions(_enabled=False)
    options.simulation = True
    options.data_type = "Upgrade"
    options.input_type = "NONE"
    options.evt_max = 1
    config = configure_input(options)

    if UseDD4Hep:
        dd4hepsvc = config[
            "LHCb::Det::LbDD4hep::DD4hepSvc/LHCb::Det::LbDD4hep::DD4hepSvc"
        ]
        dd4hepsvc.DetectorList = ["/world", "VP"]
    else:
        from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV

        reserveIOV("reserveIOV").PreloadGeometry = False

    def make_fake_odin():
        return DummyRunNumber(name="DummyRunNumber", Step=0, Start=runNumberStart)

    algs = [InteractionRegionExample(name="InteractionRegion", OutputLevel=DEBUG)]
    config.update(
        configure(
            options,
            CompositeNode("test_node", algs, combine_logic=NodeLogic.NONLAZY_AND),
            make_fake_odin=make_fake_odin,
        )
    )


def testCond():
    options = ApplicationOptions(_enabled=False)
    options.geometry_version = "run3/trunk"
    options.conditions_version = "master"
    options.dddb_tag = "upgrade/master"
    options.conddb_tag = "upgrade/interaction_region"
    runTest(options)


def testFallBack():
    options = ApplicationOptions(_enabled=False)
    options.geometry_version = "run3/trunk"
    # A commit on master from before the addition of the
    # InteractionRegion condition
    options.conditions_version = "74dfb641cf4116acb2b7b327b3445215506d4c4a"
    options.dddb_tag = "upgrade/dddb-20230313"
    options.conddb_tag = "upgrade/sim-20230626-vc-md100"
    # In the tag the condition is valid for run numbers 200-400
    runTest(options, 400)
