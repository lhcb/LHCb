/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <cstring> // for strlen with gcc 4.3
#include <string>

#include "DetDesc/ParamValidDataObject.h"
#include "GaudiKernel/System.h"

#include <typeindex>

//---------------------------------------------------------------------------

/// Copy constructor
ParamValidDataObject::ParamValidDataObject( const ParamValidDataObject& obj )
    : IValidity(), ValidDataObject( obj ), m_paramList( obj.m_paramList ), m_comments( obj.m_comments ) {}

ParamValidDataObject& ParamValidDataObject::operator=( ParamValidDataObject& obj ) {
  ValidDataObject::operator=( obj );
  m_paramList = obj.m_paramList;
  m_comments  = obj.m_comments;
  payload     = obj.payload;
  // make sure alternative representations are correctly regenerated when needed
  std::scoped_lock guard( m_transientReprMutex );
  m_yaml.reset();
  m_json.reset();
  return *this;
}

//----------------------------------------------------------------------------

std::ostream& ParamValidDataObject::fillStream( std::ostream& s ) const {
  ValidDataObject::fillStream( s );
  s << std::endl << printParams();
  return s;
}

//---------------------------------------------------------------------------

void ParamValidDataObject::reset() {
  m_paramList.clear();
  m_comments.clear();
  std::scoped_lock guard( m_transientReprMutex );
  m_yaml.reset();
  m_json.reset();
}

//----------------------------------------------------------------------------

void ParamValidDataObject::update( ValidDataObject& obj ) {
  // first check the class
  ParamValidDataObject* pvdo = dynamic_cast<ParamValidDataObject*>( &obj );
  if ( !pvdo ) {
    throw GaudiException( "Trying to do a deep copy between different classes", "ParamValidDataObject",
                          StatusCode::FAILURE );
  }
  // call the
  ValidDataObject::update( obj );

  if ( obj.updateMode() != OVERRIDE ) {
    // default to 'replace'
    m_paramList = pvdo->m_paramList;
    m_comments  = pvdo->m_comments;
    payload     = pvdo->payload;
  } else {
    // mode used by UpdateManagerSvc for conditions overrides
    m_paramList += pvdo->m_paramList;
    m_comments.insert( pvdo->m_comments.begin(), pvdo->m_comments.end() );
    payload = pvdo->payload;
  }
  // make sure alternative representations are correctly regenerated when needed
  std::scoped_lock guard( m_transientReprMutex );
  m_yaml.reset();
  m_json.reset();
}

//----------------------------------------------------------------------------

bool ParamValidDataObject::exists( const std::string& name ) const { return m_paramList.find( name ) != nullptr; }

//----------------------------------------------------------------------------
/// TypeId of the parameter
const std::type_info& ParamValidDataObject::type( const std::string& name ) const {
  auto i = m_paramList.find( name );
  if ( !i ) throw ParamException( name );
  return i->type();
}

//----------------------------------------------------------------------------
/// Check if the parameter is a vector.
bool ParamValidDataObject::isVector( const std::string& name ) const {
  const std::type_info& t = type( name );
  return t == typeid( std::vector<int> ) || t == typeid( std::vector<double> ) ||
         t == typeid( std::vector<std::string> );
}
//----------------------------------------------------------------------------
/// Get the comment of a parameter.
std::string ParamValidDataObject::comment( const std::string& name ) const {
  if ( !exists( name ) ) throw ParamException( name );

  auto i = m_comments.find( name );
  return i != m_comments.end() ? i->second : std::string{};
}

//----------------------------------------------------------------------------
/// Set the comment of a parameter.
void ParamValidDataObject::setComment( const std::string& name, std::string_view comm ) {
  if ( !exists( name ) ) throw ParamException( name );

  auto i = m_comments.find( name );
  if ( i != m_comments.end() ) {
    // set the comment only if is not empty (or a null pointer)
    if ( !comm.empty() ) {
      i->second = comm;
    } else {
      // if the comment is an empty string or a null pointer, remove the comment
      m_comments.erase( i );
    }
  } else {
    // do not add the comment if empty (ora a null pointer)
    if ( !comm.empty() ) m_comments.emplace( name, comm );
  }
}

//----------------------------------------------------------------------------
/// Get the value of a parameter, as a string.
const std::string& ParamValidDataObject::paramAsString( const std::string& name ) const {
  return param<std::string>( name );
}

//----------------------------------------------------------------------------
/// Get the value of a parameter, as a string (non const version).
std::string& ParamValidDataObject::paramAsString( const std::string& name ) { return param<std::string>( name ); }

//----------------------------------------------------------------------------
/// Get the value of a parameter, as an int.
int ParamValidDataObject::paramAsInt( const std::string& name ) const { return param<int>( name ); }

//----------------------------------------------------------------------------
/// Get the value of a parameter, as a double.
double ParamValidDataObject::paramAsDouble( const std::string& name ) const {
  try {
    return param<double>( name );
  } catch ( std::bad_cast& ) {
    if ( type( name ) != typeid( int ) ) throw;
    return param<int>( name );
  }
  return 0; // avoid Eclipse analyzer warning
}

//----------------------------------------------------------------------------
/// Get the value of a parameter, as a string.
const std::vector<std::string>& ParamValidDataObject::paramAsStringVect( const std::string& name ) const {
  return param<std::vector<std::string>>( name );
}

//----------------------------------------------------------------------------
/// Get the value of a parameter, as a string.
std::vector<std::string>& ParamValidDataObject::paramAsStringVect( const std::string& name ) {
  return param<std::vector<std::string>>( name );
}

//----------------------------------------------------------------------------
/// Get the value of a parameter, as an int.
const std::vector<int>& ParamValidDataObject::paramAsIntVect( const std::string& name ) const {
  return param<std::vector<int>>( name );
}

//----------------------------------------------------------------------------
/// Get the value of a parameter, as a double.
const std::vector<double>& ParamValidDataObject::paramAsDoubleVect( const std::string& name ) const {
  return param<std::vector<double>>( name );
}

//----------------------------------------------------------------------------
/// Get the value of a parameter, as an int.
std::vector<int>& ParamValidDataObject::paramAsIntVect( const std::string& name ) {
  return param<std::vector<int>>( name );
}

//----------------------------------------------------------------------------
/// Get the value of a parameter, as a double.
std::vector<double>& ParamValidDataObject::paramAsDoubleVect( const std::string& name ) {
  return param<std::vector<double>>( name );
}

//----------------------------------------------------------------------------
/// Get the list of existing parameters.
std::vector<std::string> ParamValidDataObject::paramNames() const { return m_paramList.getKeys(); }

//----------------------------------------------------------------------------
/// Print the user parameters on a string
std::string ParamValidDataObject::printParams() const {
  std::ostringstream os;
  for ( const auto& k : m_paramList.getKeys() ) {
    const auto* v = m_paramList.find( k );
    os << "(" << System::typeinfoName( v->type() ) << ") " << k;
    auto c = m_comments.find( k );
    if ( c != m_comments.end() ) os << " (" << c->second << ")";
    os << " = " << v->toStr() << "\n";
  }
  return os.str();
}

//----------------------------------------------------------------------------
/// Convert a parameter to a string (for xml representation).
std::string ParamValidDataObject::paramToString( const std::string& name, int precision ) const {
  auto i = m_paramList.find( name );
  if ( !i ) throw ParamException( name );
  return i->toXMLStr( name, comment( name ), precision );
}

const YAML::Node& ParamValidDataObject::asYAML() const {
  if ( std::type_index( payload.type() ) == std::type_index( typeid( YAML::Node ) ) )
    return std::any_cast<const YAML::Node&>( payload );
  std::scoped_lock guard( m_transientReprMutex );
  if ( m_yaml ) return *m_yaml;
  m_yaml           = std::make_unique<YAML::Node>();
  YAML::Node& node = *m_yaml;
  for ( auto& name : paramNames() ) {
    if ( is<int>( name ) ) {
      node[name] = param<int>( name );
    } else if ( is<double>( name ) ) {
      node[name] = param<double>( name );
    } else if ( is<std::string>( name ) ) {
      node[name] = param<std::string>( name );
    } else if ( is<std::vector<int>>( name ) ) {
      node[name] = param<std::vector<int>>( name );
    } else if ( is<std::vector<double>>( name ) ) {
      node[name] = param<std::vector<double>>( name );
    } else if ( is<std::vector<std::string>>( name ) ) {
      node[name] = param<std::vector<std::string>>( name );
    } else if ( is<std::map<std::string, int>>( name ) ) {
      node[name] = param<std::map<std::string, int>>( name );
    } else if ( is<std::map<std::string, double>>( name ) ) {
      node[name] = param<std::map<std::string, double>>( name );
    } else if ( is<std::map<std::string, std::string>>( name ) ) {
      node[name] = param<std::map<std::string, std::string>>( name );
    } else if ( is<std::map<int, int>>( name ) ) {
      node[name] = param<std::map<int, int>>( name );
    } else if ( is<std::map<int, double>>( name ) ) {
      node[name] = param<std::map<int, double>>( name );
    } else if ( is<std::map<int, std::string>>( name ) ) {
      node[name] = param<std::map<int, std::string>>( name );
    }
  }
  return node;
}

const nlohmann::json& ParamValidDataObject::asJSON() const {
  if ( std::type_index( payload.type() ) == std::type_index( typeid( nlohmann::json ) ) )
    return std::any_cast<const nlohmann::json&>( payload );
  std::scoped_lock guard( m_transientReprMutex );
  if ( m_json ) return *m_json;
  m_json               = std::make_unique<nlohmann::json>();
  nlohmann::json& node = *m_json;
  for ( auto& name : paramNames() ) {
    if ( is<int>( name ) ) {
      node[name] = param<int>( name );
    } else if ( is<double>( name ) ) {
      node[name] = param<double>( name );
    } else if ( is<std::string>( name ) ) {
      node[name] = param<std::string>( name );
    } else if ( is<std::vector<int>>( name ) ) {
      node[name] = param<std::vector<int>>( name );
    } else if ( is<std::vector<double>>( name ) ) {
      node[name] = param<std::vector<double>>( name );
    } else if ( is<std::vector<std::string>>( name ) ) {
      node[name] = param<std::vector<std::string>>( name );
    } else if ( is<std::map<std::string, int>>( name ) ) {
      node[name] = param<std::map<std::string, int>>( name );
    } else if ( is<std::map<std::string, double>>( name ) ) {
      node[name] = param<std::map<std::string, double>>( name );
    } else if ( is<std::map<std::string, std::string>>( name ) ) {
      node[name] = param<std::map<std::string, std::string>>( name );
    } else if ( is<std::map<int, int>>( name ) ) {
      node[name] = param<std::map<int, int>>( name );
    } else if ( is<std::map<int, double>>( name ) ) {
      node[name] = param<std::map<int, double>>( name );
    } else if ( is<std::map<int, std::string>>( name ) ) {
      node[name] = param<std::map<int, std::string>>( name );
    }
  }
  return node;
}
