/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/LinkManager.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartIF.h"

///
#include "DetDesc/DetDesc.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/DetectorElementException.h"

#include "DetDesc/Condition.h"

#include "DetDesc/AlignmentInfo.h"
#include "DetDesc/CalibrationInfo.h"
#include "DetDesc/FastControlInfo.h"
#include "DetDesc/GeoInfo.h"
#include "DetDesc/ReadOutInfo.h"
#include "DetDesc/SlowControlInfo.h"

/** @file DetectorElement.cpp
 *
 * Implementation of class DetectorElement
 *
 * @author Vanya Belyaev Ivan.Belyaev@itep.ru
 * @author Sebastien Ponce
 * @author Marco Clemencic <marco.clemencic@cern.ch>
 */
DetDesc::DetectorElementPlus::DetectorElementPlus( const std::string& /* name */ )
    : m_services{ DetDesc::services() } {}

DetDesc::DetectorElementPlus::~DetectorElementPlus() = default;

IDataProviderSvc* DetDesc::DetectorElementPlus::dataSvc() const { return m_services->detSvc(); }

IUpdateManagerSvc* DetDesc::DetectorElementPlus::updMgrSvc() const { return m_services->updMgrSvc(); }

IDetectorElementPlus* DetDesc::DetectorElementPlus::parentIDetectorElementPlus() const {
  SmartIF<IDataManagerSvc> mgr( dataSvc() );
  if ( mgr ) {
    IRegistry* pRegParent = nullptr;
    StatusCode sc         = mgr->objectParent( this, pRegParent );
    if ( sc.isSuccess() && pRegParent ) { return dynamic_cast<IDetectorElementPlus*>( pRegParent->object() ); }
  }
  return nullptr;
}

unsigned long DetDesc::DetectorElementPlus::addRef() { return ParamValidDataObject::addRef(); }

unsigned long DetDesc::DetectorElementPlus::release() { return ParamValidDataObject::release(); }

StatusCode DetDesc::DetectorElementPlus::queryInterface( const InterfaceID& ID, void** ppI ) {
  if ( !ppI ) return StatusCode::FAILURE;
  *ppI = nullptr;
  if ( IDetectorElementPlus::interfaceID() == ID ) {
    *ppI = static_cast<IDetectorElementPlus*>( this );
  } else if ( IInterface::interfaceID() == ID ) {
    *ppI = static_cast<IInterface*>( this );
  } else {
    return StatusCode::FAILURE;
  }
  /// add the reference
  addRef();
  ///
  return StatusCode::SUCCESS;
}

std::ostream& DetDesc::DetectorElementPlus::printOut( std::ostream& os ) const {
  os << "DetDesc::DetectorElementPlus::" << name();
  return !geometryPlus() ? os : ( os << "GeometryInfo::" << geometryPlus() );
}

MsgStream& DetDesc::DetectorElementPlus::printOut( MsgStream& os ) const {
  os << "DetDesc::DetectorElementPlus::" << name();
  return !geometryPlus() ? os : ( os << "GeometryInfo::" << geometryPlus() );
}
/// reset to the initial state/////
IDetectorElementPlus* DetDesc::DetectorElementPlus::reset() {
  /// reset the pointed-at geometry
  if ( geometryPlus() ) { geometryPlus()->reset(); }
  if ( m_de_childrensLoaded ) {
    auto& children = childIDetectorElements();
    std::for_each( children.begin(), children.end(), []( IDetectorElementPlus* de ) { de->reset(); } );
  }
  m_de_childrensLoaded = false;
  m_de_childrens.clear();
  return this;
}

// ----------------------------------------------------------------------
bool DetDesc::DetectorElementPlus::hasCondition( const std::string& name ) const {
  return m_de_conditions.find( name ) != m_de_conditions.end();
}

SmartRef<Condition> DetDesc::DetectorElementPlus::condition( const std::string& name ) const {
  auto cond = m_de_conditions.find( name );
  if ( cond == m_de_conditions.end() ) {
    std::ostringstream oss;
    oss << "Requested unknown condition '" << name << "' to '" << this->name() << "'";
    throw DetectorElementException( oss.str(), this, StatusCode::FAILURE );
  }
  return cond->second;
}

void DetDesc::DetectorElementPlus::createCondition( const std::string& name, const std::string& path ) {
  auto cond = m_de_conditions.find( name );
  Assert( cond == m_de_conditions.end(), "Could not add condition: " + name + " already present!" );
  long hint = linkMgr()->addLink( path, 0 );
  m_de_conditions.insert( ConditionMap::value_type( name, SmartRef<Condition>( this, hint ) ) );
}

//-- N. Gilardi; 2005.07.08 ---------------------------------------------
/// Get the list of existing conditions.
std::vector<std::string> DetDesc::DetectorElementPlus::conditionNames() const {
  std::vector<std::string> v;
  v.reserve( m_de_conditions.size() );
  std::transform( m_de_conditions.begin(), m_de_conditions.end(), std::back_inserter( v ),
                  []( ConditionMap::const_reference i ) { return i.first; } );
  return v;
}
// ----------------------------------------------------------------------

/////
const IGeometryInfo* DetDesc::DetectorElementPlus::createGeometryInfo() {
  Assert( !geometryPlus(), "Could not create GHOST: Geometry already exist!" );
  m_de_iGeometry.reset( GeoInfo::createGeometryInfo( this ) );
  return geometryPlus();
}
/////
const IGeometryInfo* DetDesc::DetectorElementPlus::createGeometryInfo( const std::string& LogVol ) {
  Assert( !geometryPlus(), "Could not create ORPHAN: Geometry already exist!" );
  m_de_iGeometry.reset( GeoInfo::createGeometryInfo( this, LogVol ) );
  return geometryPlus();
}
/////
const IGeometryInfo* DetDesc::DetectorElementPlus::createGeometryInfo( const std::string& LogVol,
                                                                       const std::string& Support,
                                                                       const std::string& NamePath ) {
  Assert( !geometryPlus(), "Could not create REGULAR(1): Geometry already exist!" );
  m_de_iGeometry.reset( GeoInfo::createGeometryInfo( this, LogVol, Support, NamePath ) );
  return geometryPlus();
}
/////
const IGeometryInfo* DetDesc::DetectorElementPlus::createGeometryInfo( const std::string& LogVol,
                                                                       const std::string& Support,
                                                                       const std::string& NamePath,
                                                                       const std::string& alignmentPath ) {
  Assert( !geometryPlus(), "Could not create REGULAR(1): Geometry already exist!" );
  m_de_iGeometry.reset( GeoInfo::createGeometryInfo( this, LogVol, Support, NamePath, alignmentPath ) );
  return geometryPlus();
}
//
const IGeometryInfo* DetDesc::DetectorElementPlus::createGeometryInfo( const std::string&           LogVol,
                                                                       const std::string&           Support,
                                                                       const ILVolume::ReplicaPath& rPath ) {
  Assert( !geometryPlus(), "Could not create REGULAR(2): Geometry already exist!" );
  m_de_iGeometry.reset( GeoInfo::createGeometryInfo( this, LogVol, Support, rPath ) );
  return geometryPlus();
}
//=============================================================================
const IGeometryInfo* DetDesc::DetectorElementPlus::createGeometryInfo( const std::string&           LogVol,
                                                                       const std::string&           Support,
                                                                       const ILVolume::ReplicaPath& rPath,
                                                                       const std::string&           alignmentPath ) {
  Assert( !geometryPlus(), "Could not create REGULAR(2): Geometry already exist!" );
  m_de_iGeometry.reset( GeoInfo::createGeometryInfo( this, LogVol, Support, rPath, alignmentPath ) );
  return geometryPlus();
}
//=============================================================================
const IAlignment* DetDesc::DetectorElementPlus::createAlignment( const std::string& condition ) {
  Assert( !alignment(), "Could not create AlignmentInfo: it already exists!" );
  if ( !m_de_iAlignment ) m_de_iAlignment = std::make_unique<AlignmentInfo>( this, condition );
  return alignment();
}

const ICalibration* DetDesc::DetectorElementPlus::createCalibration( const std::string& condition ) {
  Assert( !calibration(), "Could not create CalibrationInfo: it already exists!" );
  if ( !m_de_iCalibration ) m_de_iCalibration = std::make_unique<CalibrationInfo>( this, condition );
  return calibration();
}

const IReadOut* DetDesc::DetectorElementPlus::createReadOut( const std::string& condition ) {
  Assert( !readOut(), "Could not create ReadOutCalibrationInfo: it already exists!" );
  if ( !m_de_iReadOut ) m_de_iReadOut = std::make_unique<ReadOutInfo>( this, condition );
  return readOut();
}

const ISlowControl* DetDesc::DetectorElementPlus::createSlowControl( const std::string& condition ) {
  Assert( !slowControl(), "Could not create SlowControlInfo: it already exists!" );
  if ( !m_de_iSlowControl ) m_de_iSlowControl = std::make_unique<SlowControlInfo>( this, condition );
  return slowControl();
}

const IFastControl* DetDesc::DetectorElementPlus::createFastControl( const std::string& condition ) {
  Assert( !fastControl(), "Could not create FastControlInfo: it already exists!" );
  if ( !m_de_iFastControl ) m_de_iFastControl = std::make_unique<FastControlInfo>( this, condition );
  return fastControl();
}
//
StatusCode DetDesc::DetectorElementPlus::initialize() {
  // this is a default implementation that does nothing.
  // it is up to the user to override this in a child of DetectorElement
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------------

/// (reference to) container of pointers to child detector elements ///////////
IDetectorElementPlus::IDEContainer& DetDesc::DetectorElementPlus::childIDetectorElements() const {
  /// already loaded?
  if ( m_de_childrensLoaded ) { return m_de_childrens; }
  /// load them!
  SmartIF<IDataManagerSvc> mgr( dataSvc() );
  if ( mgr ) {
    std::vector<IRegistry*> leaves;
    StatusCode              sc = mgr->objectLeaves( this, leaves );
    if ( sc.isSuccess() ) {
      std::lock_guard guard( m_de_childrens_lock );
      if ( !m_de_childrensLoaded ) {
        for ( const auto& l : leaves ) {
          Assert( l, "DirIterator points to NULL!" );
          const std::string&                 nam = l->identifier();
          SmartDataPtr<IDetectorElementPlus> de( dataSvc(), nam );
          IDetectorElementPlus*              ide = de;
          Assert( ide, "Could not load child object=" + nam );
          m_de_childrens.push_back( ide );
        }
      }
      m_de_childrensLoaded = true;
    }
  }
  return m_de_childrens;
}

const ParamValidDataObject* DetDesc::DetectorElementPlus::params() const { return this; }
/// sensitive volume identifier ///////////////////////////////////////////////
int DetDesc::DetectorElementPlus::sensitiveVolumeID( const Gaudi::XYZPoint& globalPoint ) const {
  if ( !isInside( globalPoint ) ) return -1;
  const IDetectorElementPlus* child = childDEWithPoint( globalPoint );
  return child ? child->sensitiveVolumeID( globalPoint ) : -1;
}

bool DetDesc::DetectorElementPlus::isInside( const Gaudi::XYZPoint& globalPoint ) const {
  const IGeometryInfoPlus* gi = geometryPlus();
  return gi && gi->isInside( globalPoint );
}

const IDetectorElementPlus* DetDesc::DetectorElementPlus::childDEWithPoint( const Gaudi::XYZPoint& globalPoint ) const {
  const auto& children = childIDetectorElements();
  auto        iDE      = std::find_if( children.begin(), children.end(), [&]( const auto& i ) {
    auto pDE = dynamic_cast<DetectorElementPlus*>( i );
    return pDE && pDE->isInside( globalPoint );
  } );
  return iDE != children.end() ? *iDE : nullptr;
}
// ============================================================================
const std::string& DetDesc::DetectorElementPlus::name() const {
  static const std::string s_empty;
  IRegistry*               pReg = registry();
  return pReg ? pReg->identifier() : s_empty;
}

/// assertion
void DetDesc::DetectorElementPlus::Assert( bool assertion, const std::string& assertionName ) const {
  if ( !assertion ) { throw DetectorElementException( assertionName, this ); }
}
void DetDesc::DetectorElementPlus::Assert( bool assertion, const char* assertionName ) const {
  if ( !assertion ) { throw DetectorElementException( assertionName, this ); }
}

// ============================================================================
const std::string& DetDesc::DetectorElementPlus::lVolumeName() const { return this->geometryPlus()->lvolumeName(); }
// ============================================================================
// End
// ============================================================================
