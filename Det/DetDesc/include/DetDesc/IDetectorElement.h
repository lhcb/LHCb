/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP

#  include "Detector/LHCb/DeLHCb.h"
using IDetectorElement = LHCb::Detector::IAlignable;
namespace LHCb {
  const std::string standard_geometry_top{ "/world:DetElement-Info-IOV" };
}
#else

#  include "DetDesc/IGeometryInfo.h"

#  include "GaudiKernel/IInterface.h"
#  include "GaudiKernel/Point3DTypes.h"
#  include "GaudiKernel/SmartRef.h"

#  include <iostream>
#  include <optional>
#  include <string>

struct IAlignment;
struct ICalibration;
struct IReadOut;
struct ISlowControl;
struct IFastControl;
struct Condition;
class ParamValidDataObject;

namespace DetDesc {

  /** the unique interface identifier
   * ( unique interface identifier , major & minor versions)
   */
  static const InterfaceID IID_IDetectorElement( 156, 2, 2 );

  struct IDetectorElement : virtual IInterface {
    /// "accessor":  delegates the IGeometryInfo Interface
    virtual IGeometryInfo* geometry() = 0;
    /// "accessor":  delegates the IGeometryInfo Interface (const)
    virtual const IGeometryInfo* geometry() const = 0;

    // another way to access: "pseudo-conversion"
    // cast to         IGeometryInfo*
    virtual operator IGeometryInfo*() = 0;
    // cast to   const IGeometryInfo*
    virtual operator const IGeometryInfo*() const = 0;
    /// cast to const IGeometryInfo&
    virtual operator IGeometryInfo&() = 0;
    /// cast to const IGeometryInfo&
    virtual operator const IGeometryInfo&() const = 0;
    virtual ~IDetectorElement() {}
  };

  /** @interface IDetectorElementPlus IDetectorElementPlus.h "DetDesc/IDetectorElementPlus.h"
   *
   *  An abstract Interface accees
   *   the node in DetectorDescription tree.
   *  Just delegates all questions to right guys.
   *
   *  @author Sebastien Ponce
   *  @author Vanya Belyaev
   *  @author Marco Clemencic <marco.clemencic@cern.ch>
   */

  struct IDetectorElementPlus : virtual IDetectorElement {
    ///
    typedef std::vector<IDetectorElementPlus*> IDEContainer;
    ///

    using IDetectorElement::geometry;

    /** retrieve the unique interface identifier
     *  @return the unique interface identifier
     */
    static const InterfaceID& interfaceID() { return IID_IDetectorElement; }

    /// "accessor":  name/identifier of the Detector Element
    virtual const std::string& name() const = 0;

    virtual const std::string& lVolumeName() const = 0;

    /// Check if the condition called 'name' is in the list of conditionrefs.
    virtual bool hasCondition( const std::string& name ) const = 0;

    /// Return the SmartRef for the condition called 'name'.
    virtual SmartRef<Condition> condition( const std::string& name ) const = 0;

    ///
    /// delegations:
    ///
    /// "accessor":  delegates the IGeometryInfo Interface
    virtual IGeometryInfoPlus* geometryPlus() = 0;
    /// "accessor":  delegates the IGeometryInfo Interface (const)
    virtual const IGeometryInfoPlus* geometryPlus() const = 0;
    /// "accessor":  delegates the IAlignment Interface
    virtual IAlignment* alignment() = 0;
    /// "accessor":  delegates the IAlignment Interface (const)
    virtual const IAlignment* alignment() const = 0;
    /// "accessor":  delegates the ICalibration Interface
    virtual ICalibration* calibration() = 0;
    /// "accessor":  delegates the ICalibration Interface (const)
    virtual const ICalibration* calibration() const = 0;
    /// "accessor":  delegates the IReadOut Interface
    virtual IReadOut* readOut() = 0;
    /// "accessor":  delegates the IReadOut Interface (const)
    virtual const IReadOut* readOut() const = 0;
    /// "accessor":  delegates the ISlowControl Interface
    virtual ISlowControl* slowControl() = 0;
    /// "accessor":  delegates the ISlowControl Interface (const)
    virtual const ISlowControl* slowControl() const = 0;
    /// "accessor":  delegates the IFastControl Interface
    virtual IFastControl* fastControl() = 0;
    // "accessor":  delegates the IFastControl Interface (const)
    virtual const IFastControl* fastControl() const = 0;
    /// Another way of accessiong the information: "pseudo-conversion"
    /// cast to       IGeometryInfo*
    virtual operator IGeometryInfo*() override = 0;
    /// cast to const IGeometryInfo*
    virtual operator const IGeometryInfo*() const override = 0;
    /// cast to       IAlignment*
    virtual operator IAlignment*() = 0;
    /// cast to const IAlignment*
    virtual operator const IAlignment*() const = 0;
    /// cast to       ICalibration*
    virtual operator ICalibration*() = 0;
    /// cast to const ICalibration*
    virtual operator const ICalibration*() const = 0;
    /// cast to       IReadOut*
    virtual operator IReadOut*() = 0;
    /// cast to const IReadOut*
    virtual operator const IReadOut*() const = 0;
    /// cast to       ISlowControl*
    virtual operator ISlowControl*() = 0;
    /// cast to const ISlowControl*
    virtual operator const ISlowControl*() const = 0;
    /// cast to       IFastControl*
    virtual operator IFastControl*() = 0;
    /// cast to const IFastControl*
    virtual operator const IFastControl*() const = 0;

    /// more convenient set of conversion operators:

    /// cast to       IGeometryInfo&
    virtual operator IGeometryInfo&() override = 0;
    /// cast to const IGeometryInfo&
    virtual operator const IGeometryInfo&() const override = 0;
    /// cast to       IAlingment&
    virtual operator IAlignment&() = 0;
    /// cast to const IAlignment&
    virtual operator const IAlignment&() const = 0;
    /// cast to       ICalibration&
    virtual operator ICalibration&() = 0;
    /// cast to const ICalibration&
    virtual operator const ICalibration&() const = 0;
    /// cast to       IReadOut&
    virtual operator IReadOut&() = 0;
    /// cast to const IReadOut&
    virtual operator const IReadOut&() const = 0;
    /// cast to       ISlowControl&
    virtual operator ISlowControl&() = 0;
    /// cast to const ISlowControl&
    virtual operator const ISlowControl&() const = 0;
    /// cast to       IFastControl&
    virtual operator IFastControl&() = 0;
    /// cast to const IFastControl&
    virtual operator const IFastControl&() const = 0;

    virtual std::optional<ROOT::Math::Transform3D> motionSystemTransform() const = 0;

    /// some functions to simplify the navigation
    /// (according to feedback after release 3)
    /// pointer to parent IDetectorElementPlus (const version)
    virtual IDetectorElementPlus* parentIDetectorElementPlus() const = 0;
    /// (reference to) container of pointers to child detector elements
    virtual IDetectorElementPlus::IDEContainer& childIDetectorElements() const = 0;
    /// Lookup children containing a specific point
    virtual const IDetectorElementPlus* childDEWithPoint( const ROOT::Math::XYZPoint& globalPoint ) const = 0;
    /// iterators for manipulation of daughter elements
    /// begin iterator
    [[deprecated]] auto childBegin() const { return childIDetectorElements().begin(); }
    /// end   iterator
    [[deprecated]] auto childEnd() const { return childIDetectorElements().end(); }
    /// functions for  listing of objects, used in overloaded << operations
    virtual std::ostream& printOut( std::ostream& ) const = 0;
    /// reset to the initial state
    virtual IDetectorElementPlus* reset() = 0;

    /**
     * Method used to access the ParamValidDataObject methods from IDetectorElementPlus
     * interface.
     */
    virtual const ParamValidDataObject* params() const = 0;

    /**
     * Return a sensitive volume identifier for a given point in the
     * global reference frame.
     */

    virtual int sensitiveVolumeID( const Gaudi::XYZPoint& globalPos ) const = 0;

    /**
     * This method initializes the detector element. It should be overridden
     * and used for computation purposes. This is a kind of hook for adding
     * user code easily in the initialization of a detector element.
     */
    virtual StatusCode initialize() = 0;
  };
  ///
  inline std::ostream& operator<<( std::ostream& os, const IDetectorElementPlus& de ) { return de.printOut( os ); }
  ///
  inline std::ostream& operator<<( std::ostream& os, const IDetectorElementPlus* de ) {
    return ( ( 0 == de ) ? ( os << "IDetectorElementPlus* points to NULL" ) : ( os << *de ) );
  }
} // namespace DetDesc

using IDetectorElement = DetDesc::IDetectorElement;
namespace LHCb {
  const std::string standard_geometry_top{ "/dd/Structure/LHCb" };
}
#endif
