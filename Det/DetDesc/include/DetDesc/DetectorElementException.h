/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/DetectorElement.h"

#include "GaudiKernel/GaudiException.h"

class MsgStream;

namespace DetDesc {
  /**
   * Exception used in DetectorElement class
   * @author Vanya Belyaev
   */
  class DetectorElementException : public GaudiException {
  public:
    /// constructor
    DetectorElementException( const std::string& name, const DetectorElementPlus* DetectorElement = nullptr,
                              const StatusCode& sc = StatusCode::FAILURE );
    /// constructor from exception
    DetectorElementException( const std::string& name, const GaudiException& Exception,
                              const DetectorElementPlus* DetectorElement = nullptr,
                              const StatusCode&          sc              = StatusCode::FAILURE );

    std::ostream&   printOut( std::ostream& os = std::cerr ) const override;
    MsgStream&      printOut( MsgStream& ) const override;
    GaudiException* clone() const override;

  private:
    const DetectorElementPlus* m_dee_DetectorElement;
  };
} // namespace DetDesc

using DetectorElementException = DetDesc::DetectorElementException;
