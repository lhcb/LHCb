/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/FT/FTChannelID.h"
#include "Event/MCHit.h"
#include "FTDet/DeFTDetector.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"

/**
 *  An algorithm with solely test purposes, which calls (tests)
 *  the methods defined in the FT det. element classes.
 *
 *  @author Plamen Hopchev
 *  @date   2012-04-25
 */
struct DeFTTestAlg : LHCb::Algorithm::Consumer<void( const LHCb::MCHits&, const DeFT& ),
                                               LHCb::Algorithm::Traits::usesBaseAndConditions<GaudiTupleAlg, DeFT>> {
  DeFTTestAlg( const std::string& name, ISvcLocator* loc );
  void operator()( const LHCb::MCHits&, const DeFT& ) const override;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DeFTTestAlg )

DeFTTestAlg::DeFTTestAlg( const std::string& name, ISvcLocator* loc )
    : Consumer{ name,
                loc,
                { KeyValue{ "MCHitsLocation", LHCb::MCHitLocation::FT },
                  KeyValue{ "FTLocation", DeFTDetectorLocation::Default } } } {}

void DeFTTestAlg::operator()( const LHCb::MCHits& hitsCont, const DeFT& deFT ) const {
  if ( deFT.version() < 61 )
    throw GaudiException( "This version requires FTDet v6.1 or higher.", "DeFTTestAlg", StatusCode::FAILURE );

  // book tuple
  Tuples::Tuple tuple = GaudiTupleAlg::nTuple( "TestFTTree", "Events" );

  /// Iterate over the first few hits and test the calculateHits method
  std::cout << "MC hits: " << hitsCont.size() << std::endl;
  for ( const auto& aHit : hitsCont ) {
    Gaudi::XYZPoint pMid = aHit->midPoint();
    Gaudi::XYZPoint pIn  = aHit->entry();
    Gaudi::XYZPoint pOut = aHit->exit();

    tuple->column( "Hit_", pMid ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "HitIn_", pIn ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "HitOut_", pOut ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    // Make DeFT checks
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "\n\n\n**************************\nMC Hit " << aHit->index() << "\n"
              << "**************************" << endmsg;
      debug() << *aHit << endmsg;
    }
    std::string lVolName;

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Test of det. element geometry() methods, using "
              << "the midPoint() of the MC hit: " << pMid << endmsg;

    /// check isInside FT
    bool isInsideFT = deFT.isInside( pMid );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Global Point " << pMid << "; isInside =  " << isInsideFT << endmsg;
    tuple->column( "InFT", isInsideFT ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    /// test findStation method
    const auto pStation = deFT.findStation( pMid );
    lVolName            = ( pStation ? pStation->lVolumeName() : "" );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found Station: " << lVolName << endmsg;
    tuple->column( "StationID", ( pStation ? pStation->stationID() : -1 ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    /// test findLayer method
    const auto pLayer = deFT.findLayer( pMid );
    lVolName          = ( pLayer ? pLayer->lVolumeName() : "" );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found Layer  : " << lVolName << endmsg;
    tuple->column( "LayerID", ( pLayer ? pLayer->layerID() : -1 ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    /// test findModule method
    const auto pModule = deFT.findModule( pMid );
    lVolName           = ( pModule ? pModule->lVolumeName() : "" );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found Module  : " << lVolName << endmsg;
    tuple->column( "ModuleID", ( pModule ? to_unsigned( pModule->moduleID() ) : -1 ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    /// test findMat method
    const auto pMat = deFT.findMat( pMid );
    lVolName        = ( pMat ? pMat->lVolumeName() : "" );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found Mat  : " << lVolName << endmsg;
    tuple->column( "MatID", ( pMat ? to_unsigned( pMat->matID() ) : -1 ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    // ok, write tuple row
    StatusCode status = tuple->write();
    if ( status.isFailure() ) throw GaudiException( "Cannot fill ntuple", "DeFTTestAlg", StatusCode::FAILURE );
  } // end loop over hits
}
