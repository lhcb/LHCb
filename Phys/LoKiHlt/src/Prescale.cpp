/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// =============================================================================
// Include files
// =============================================================================
#include "LoKi/Prescale.h"
#include "GaudiKernel/ToStream.h"
#include "LHCbMath/DeterministicMixer.h"
#include "boost/integer/integer_mask.hpp"
#include "boost/integer_traits.hpp"
// =============================================================================
/** @file
 *  Implementation file for class LoKi::Odin::Prescale
 *  @see LoKi::Cuts::ODIN_PRESCALE
 *  @see DeterministicPrescaler
 *  The idea belongs to Gerhard Raven
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2010-02-12
 */
// =============================================================================
// =============================================================================
// constructor from the accept fraction and the seed
// =============================================================================
LoKi::Odin::Prescale::Prescale( const double accept, const std::string& seed )
    : LoKi::AuxFunBase( std::tie( accept, seed ) )
    , m_accept( accept )
    , m_seed( seed )
    , m_initial{ LHCb::DeterministicMixer{ seed }.state } {
  if ( 0 >= m_accept ) {
    Warning( "Non-positive 'AcceptFraction is specified" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  if ( 1 < m_accept ) {
    Warning( "'AcceptFraction exceeds 1" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
}
// =============================================================================
// MANDATORY: clone method ("virtual constructor")
// =============================================================================
LoKi::Odin::Prescale* LoKi::Odin::Prescale::clone() const { return new Prescale( *this ); }
// =============================================================================
// MANDATORY: the only essential method
// =============================================================================
bool LoKi::Odin::Prescale::operator()( const LHCb::ODIN* a ) const {
  Assert( 0 != a, "LHCb::ODIN* point to NULL!" );
  //
  uint32_t x = LHCb::DeterministicMixer{ m_initial }( a->gpsTime(), a->runNumber(), a->eventNumber() ).state;
  //
  // at this point, we assume 'x' to be uniformly
  // distributed in [0,0xffffffff]
  // (and yes, this was verified to be sufficiently
  // true on a sample of 10K MC events ;-)

  // note that an IEEE754 double has 57 bits of fraction,
  // which is enough precision
  // to cover the entire dynamic range of an uint32_t
  return double( x ) < m_accept * boost::integer_traits<uint32_t>::const_max;
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Odin::Prescale::fillStream( std::ostream& s ) const {
  s << " ODIN_PRESCALE( " << m_accept;
  if ( !m_seed.empty() ) {
    s << " , ";
    Gaudi::Utils::toStream( m_seed, s );
  }
  return s << " ) ";
}
// =============================================================================
// The END
// =============================================================================
