###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    force_location,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.input_type = "None"
options.simulation = True
options.dddb_tag = "upgrade/master"  # unused
options.conddb_tag = "upgrade/master"  # unused
options.geometry_version = "run3/trunk"  # unused
options.conditions_version = "master"  # unused
options.evt_max = 2
config = configure_input(options)

from PyConf.Algorithms import Gaudi__Examples__IntDataProducer as IntDataProducer
from PyConf.Algorithms import Gaudi__Examples__KeyedDataProducer as KeyedDataProducer
from PyConf.Algorithms import Gaudi__Examples__VectorDataProducer as VectorDataProducer
from PyConf.Algorithms import LoKi__VoidFilter as VoidFilter

intData = IntDataProducer(
    name="IntDataProducer", outputs={"OutputLocation": force_location("/Event/MyInt")}
)
vectorData = VectorDataProducer(
    name="VectorDataProducer",
    outputs={"OutputLocation": force_location("/Event/MyVector")},
)
keyedData = KeyedDataProducer(
    name="KeyedDataProducer",
    outputs={"OutputLocation": force_location("/Event/MyKeyed")},
)
keyedData_rit = KeyedDataProducer(
    name="KeyedDataProducerRootInTes",
    outputs={"OutputLocation": force_location("/Event/myrootintes/MyKeyed")},
)

filter_keyed = VoidFilter(name="FilterKeyed", Code="SIZE('MyKeyed')>1")
filter_keyed_rootintes = VoidFilter(
    name="FilterKeyedRootInTes", Code="SIZE('MyKeyed2')>1", RootInTES="myrootintes"
)
filter_int = VoidFilter(name="FilterInt", Code="SIZE('MyInt')>1")
filter_vector = VoidFilter(name="FilterVector", Code="SIZE('MyVector')>1")
filter_old_keyed = VoidFilter(name="FilterOldKeyed", Code="CONTAINS('MyKeyed')>1")

algs = [
    intData,
    vectorData,
    keyedData_rit,
    keyedData,
    filter_keyed,
    filter_old_keyed,
    filter_int,
    filter_vector,
]
config.update(configure(options, CompositeNode("TopSeq", algs)))
