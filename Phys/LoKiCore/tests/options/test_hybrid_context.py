###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import HistogramPersistencySvc, LoKiSvc
from Gaudi.Configuration import DEBUG

from PyConf.Algorithms import LoKi__VoidFilter as VoidFilter
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    force_location,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.evt_max = 1
options.input_type = "None"
options.simulation = True
options.dddb_tag = "upgrade/master"  # unused
options.conddb_tag = "upgrade/master"  # unused
options.geometry_version = "run3/trunk"  # unused
options.conditions_version = "master"  # unused
config = configure_input(options)

# Get rid of warnings
HistogramPersistencySvc().OutputLevel = 5
LoKiSvc().Welcome = False

inner = VoidFilter(
    name="VoidFilter/Inner",
    Preambulo=[
        """
# Check that CONTAINS is decorated correctly
print("Inner, CONTAINS =", CONTAINS)
assert "Inner" in repr(CONTAINS)
f1 = CONTAINS('/Event/Location') > 0
    """
    ],
    Code="f1",
    OutputLevel=DEBUG,
)

algorithm = VoidFilter(
    name="Algorithm",
    Preambulo=[
        """
# Check that CONTAINS is decorated correctly
print("Algorithm, CONTAINS =", CONTAINS)
assert "Algorithm" in repr(CONTAINS)
f1 = CONTAINS('/Event/Location') > 0

# Check that COUNTER, which inherits from CONTAINS, is decorated correctly
print("Algorithm, COUNTER =", COUNTER)
assert "Algorithm" in repr(COUNTER)
f2 = COUNTER('/Event/Location', 'counter')

from LoKiHlt.algorithms import *
# the following will initialize Inner inside of Algorithm's initialization
functor = execute('LoKi__VoidFilter/Inner')

# Check there is no leak after Inner is initialized
assert "Algorithm" in repr(CONTAINS)
"""
    ],
    Code="functor",
    OutputLevel=DEBUG,
)

algs = [algorithm, inner]
config.update(configure(options, CompositeNode("TopSeq", algs)))
