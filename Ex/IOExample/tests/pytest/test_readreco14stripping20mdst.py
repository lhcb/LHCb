###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import LineSkipper


@pytest.mark.ctest_fixture_required("ioexample.copyreco14stripping20mdsttoroot")
@pytest.mark.shared_cwd("IOExample")
class Test(LHCbExeTest):
    command = ["gaudirun.py", "../options/ReadReco14Stripping20mdst.py"]
    reference = "../refs/reco14stripping20mdst.yaml"
    preprocessor = LHCbExeTest.preprocessor + LineSkipper(
        [
            "|-input_files",
            "|                                         (default: [])",
            "|-output_file",
            "RootIOAlg",
            "FSRInputCopyStream",
            "CopyInputStream",
        ]
    )
