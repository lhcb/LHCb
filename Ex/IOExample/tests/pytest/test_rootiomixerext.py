###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTesting.preprocessors import LineSkipper
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import preprocessor


class Test(LHCbExeTest):
    command = ["gaudirun.py", "../options/ROOTIOMixer.py:test_ext"]
    reference = "../refs/ROOTIOMixerExt.yaml"
    preprocessor = LHCbExeTest.preprocessor + LineSkipper(
        ["HLTControlFlowMgr    INFO Timing"]
    )
