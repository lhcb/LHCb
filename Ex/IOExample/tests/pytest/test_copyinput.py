###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from pathlib import Path

from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "../options/CopyInput.py"]

    def test_branches(self, cwd: Path):
        refBranches = {
            "_Event_MC_DigiHeader_R.",
            "_Event_Link_Raw_FT_LiteClusters2MCHits_R.",
            "_Event_Next_Gen_Collisions_R.",
            "_Event_Next_R.",
            "_Event_Link_Raw_UT_Clusters2MCHits.",
            "_Event_Link_Raw_R.",
            "_Event_Prev_Gen_Header_R.",
            "_Event_Next_Gen_Collisions.",
            "_Event_Link_Raw_Ecal_R.",
            "_Event_MC.",
            "_Event_Gen_BeamParameters_R.",
            "_Event_Gen.",
            "_Event_Link_R.",
            "_Event_pSim_Rich_DigitSummaries.",
            "_Event_Link_Raw_UT_Clusters.",
            "_Event_Prev_Gen_Collisions_R.",
            "_Event.",
            "_Event_PrevPrev_R.",
            "_Event_Link_Raw.",
            "_Event_Link_Raw_Muon.",
            "_Event_NextNext_MC.",
            "_Event_PrevPrev_Gen_Collisions_R.",
            "_Event_R.",
            "_Event_Link_Raw_Hcal_R.",
            "_Event_Link_Raw_Hcal_Digits_R.",
            "_Event_MC_Muon_DigitsInfo_R.",
            "_Event_Link_Raw_Hcal_Digits.",
            "_Event_Prev_Gen_Header.",
            "_Event_Link_Raw_VP_Digits2MCHits.",
            "_Event_PrevPrev_MC.",
            "_Event_Link_Raw_VP.",
            "_Event_Next_Gen.",
            "_Event_pSim_MCVertices_R.",
            "_Event_pSim_R.",
            "_Event_NextNext_Gen_Header_R.",
            "_Event_MC_Muon.",
            "_Event_Next_Gen_Header.",
            "_Event_pSim.",
            "_Event_Next_Gen_Header_R.",
            "_Event_Prev_MC_R.",
            "_Event_NextNext_Gen.",
            "_Event_MC_Muon_DigitsInfo.",
            "_Event_Link_Raw_Hcal.",
            "_Event_Link_Raw_FT_LiteClusters_R.",
            "_Event_Link.",
            "_Event_MC_TrackInfo_R.",
            "_Event_DAQ.",
            "_Event_Next_MC_Header.",
            "_Event_DAQ_RawEvent.",
            "_Event_pSim_MCParticles_R.",
            "_Event_Gen_R.",
            "_Event_Link_Raw_Ecal.",
            "_Event_Link_Raw_Muon_R.",
            "_Event_Prev_Gen_Collisions.",
            "_Event_Link_Raw_UT_R.",
            "_Event_Prev_Gen_R.",
            "_Event_Gen_Header_R.",
            "_Event_pSim_Rich_DigitSummaries_R.",
            "_Event_PrevPrev_Gen_Collisions.",
            "_Event_Link_Raw_FT_LiteClusters2MCHits.",
            "_Event_DAQ_RawEvent_R.",
            "_Event_PrevPrev_Gen_Header_R.",
            "_Event_MC_Header.",
            "_Event_Link_Raw_Ecal_Digits.",
            "_Event_Link_Raw_UT_Clusters2MCHits_R.",
            "_Event_PrevPrev_Gen_R.",
            "_Event_NextNext_Gen_R.",
            "_Event_Gen_Header.",
            "_Event_Prev_Gen.",
            "_Event_NextNext_MC_R.",
            "_Event_pSim_MCVertices.",
            "_Event_PrevPrev_MC_Header.",
            "_Event_PrevPrev_MC_R.",
            "_Event_pSim_Rich.",
            "_Event_MC_DigiHeader.",
            "_Event_Link_Raw_VP_Digits_R.",
            "_Event_Prev_MC_Header_R.",
            "_Event_Link_Raw_Muon_Digits_R.",
            "_Event_Prev_MC.",
            "_Event_Prev_MC_Header.",
            "_Event_Prev_R.",
            "_Event_Link_Raw_Muon_Digits.",
            "_Event_PrevPrev.",
            "_Event_pSim_Rich_R.",
            "_Event_Gen_Collisions_R.",
            "_Event_Gen_Collisions.",
            "_Event_Next_MC_R.",
            "_Event_Next.",
            "_Event_NextNext_MC_Header_R.",
            "_Event_NextNext_MC_Header.",
            "_Event_Link_Raw_FT.",
            "_Event_MC_Muon_R.",
            "_Event_PrevPrev_MC_Header_R.",
            "_Event_Link_Raw_VP_Digits2MCHits_R.",
            "_Event_pSim_MCParticles.",
            "_Event_Link_Raw_UT.",
            "_Event_PrevPrev_Gen.",
            "_Event_Prev.",
            "_Event_Next_MC.",
            "_Event_PrevPrev_Gen_Header.",
            "_Event_NextNext.",
            "_Event_Link_Raw_FT_LiteClusters.",
            "_Event_Link_Raw_Ecal_Digits_R.",
            "_Event_Link_Raw_UT_Clusters_R.",
            "_Event_NextNext_Gen_Header.",
            "_Event_Link_Raw_VP_R.",
            "_Event_Next_Gen_R.",
            "_Event_Next_MC_Header_R.",
            "_Event_NextNext_Gen_Collisions.",
            "_Event_Link_Raw_FT_R.",
            "_Event_NextNext_R.",
            "_Event_NextNext_Gen_Collisions_R.",
            "_Event_Link_Raw_VP_Digits.",
            "_Event_MC_Header_R.",
            "_Event_MC_TrackInfo.",
            "_Event_Gen_BeamParameters.",
            "_Event_MC_R.",
            "_Event_DAQ_R.",
        }

        # check that output file has the expected branches as input
        import ROOT

        output = ROOT.TFile(str(cwd / "LHCbApp_CopyInputStream.dst"), "read")
        eventTree = output.Get("Event")
        assert eventTree, "Event tree is not present."

        outputBranches = set([b.GetName() for b in eventTree.GetListOfBranches()])

        if refBranches != outputBranches:
            log = ""
            refNoOut = refBranches.difference(outputBranches)
            if refNoOut:
                log += "Expected but not found : " + str(refNoOut) + "\n"
            outNoRef = outputBranches.difference(refBranches)
            if outNoRef:
                log += "Found but not expected : " + str(outNoRef) + "\n"

        assert refBranches == outputBranches, (
            f"Discrepancies in branches between input and output: {log}"
        )
