###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import PrintHeader
from PyConf.application import ApplicationOptions, configure, configure_input, make_odin
from PyConf.control_flow import CompositeNode


def runTest(fileDBEntry):
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb(fileDBEntry)
    configure_input(options)
    configure(
        options,
        CompositeNode(
            "Top", [PrintHeader(name="PrintHeader", ODINLocation=make_odin())]
        ),
    )


def runCorruptedMDF():
    runTest("corruptedMDF")


def runCorruptedMDF2():
    runTest("corruptedMDF2")


def runCorruptedMDF3():
    runTest("corruptedMDF3")
