###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import DEBUG

from PyConf.Algorithms import PrintHeader
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    create_or_reuse_mdfIOAlg,
    make_odin,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("mdf_file_with_large_bank")
options.print_freq = 1
options.input_type = "MDF"
config = configure_input(options)

config.update(
    configure(
        options,
        CompositeNode(
            "Top", [PrintHeader(name="PrintHeader", ODINLocation=make_odin())]
        ),
    )
)
