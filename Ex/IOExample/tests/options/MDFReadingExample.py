###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import tempfile
from subprocess import check_call

from PRConfig.TestFileDB import test_file_db

from GaudiConf.LbExec import Options
from PyConf.Algorithms import PrintHeader
from PyConf.application import configure, configure_input, make_odin
from PyConf.control_flow import CompositeNode


def main(options: Options):
    configure_input(options)
    cf = CompositeNode(
        "Top",
        [PrintHeader(name="PrintHeader", ODINLocation=make_odin(), Frequency=100)],
    )
    return configure(options, cf)


fileDbEntry = test_file_db["mdfreadingexample"]
qualifiers = fileDbEntry.qualifiers

# Download all files to be able to try local reads and remote reads
# Download a non compressed file with non compressed MDF payloads
path1 = "./file1.mdf"
if not os.path.isfile(path1):
    check_call(["xrdcp", "-s", fileDbEntry.filenames[0].replace("mdf:", ""), path1])

# Download a compressed file with non compressed MDF payloads
path1z = "./file1.zstd.mdf"
if not os.path.isfile(path1z):
    check_call(["xrdcp", "-s", fileDbEntry.filenames[1].replace("mdf:", ""), path1z])

# Download a non compressed file with compressed MDF payloads
path2 = "./file2.mdf"
if not os.path.isfile(path2):
    check_call(["xrdcp", "-s", fileDbEntry.filenames[2].replace("mdf:", ""), path2])

# Download a compressed file with compressed MDF payloads
path2z = "./file2.zstd.mdf"
if not os.path.isfile(path2z):
    check_call(["xrdcp", "-s", fileDbEntry.filenames[3].replace("mdf:", ""), path2z])

# Define the options for the application
options = {
    "data_type": "Upgrade",
    "simulation": True,
    "dddb_tag": qualifiers["DDDB"],
    "conddb_tag": qualifiers["CondDB"],
    "input_files": fileDbEntry.filenames + [path1, path1z, path2, path2z],
    "input_type": "RAW",
    "print_freq": 500,
}
