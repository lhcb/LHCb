###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from subprocess import check_call

import Configurables
from Configurables import (
    ApplicationMgr,
    CountBanks,
    HiveDataBrokerSvc,
    HiveWhiteBoard,
    HLTControlFlowMgr,
)
from Gaudi.Configuration import *
from PRConfig.TestFileDB import test_file_db

nbFiles = 4


def fileName(n, extension):
    return f"mdfIOMixerInput_{n}{extension}.mdf"


def test(mixerAlgo, extension, evtMax=10):
    # replicate input file so that we have several at hand
    fileDbEntry = test_file_db["mdfreadingexample"]
    check_call(
        [
            "xrdcp",
            "-s",
            "-f",
            fileDbEntry.filenames[0].replace("mdf:", ""),
            fileName(0, extension),
        ]
    )
    for n in range(1, nbFiles):
        check_call(
            [
                "dd",
                f"if={fileName(0, extension)}",
                f"of={fileName(n, extension)}",
                "bs=150K",
                "count=1",
                "status=none",
            ]
        )

    ioalgType = getattr(Configurables, mixerAlgo)
    ioalg = ioalgType(
        "MDFIOMixer",
        RawEventLocation="/Event/DAQ/RawEvent",
        EventBufferLocation="/Event/DAQ/RawEventBuffer",
        Input=[[fileName(n, extension)] for n in range(nbFiles)],
        BufferNbEvents=20,
        RandomSeed=1234,
    )
    countalg = CountBanks(name="CountBanks", RawEventLocation=ioalg.RawEventLocation)

    # make sure Gaudi does not close files in our back !
    IODataManager().AgeLimit = nbFiles + 1

    hiveDataBroker = HiveDataBrokerSvc("HiveDataBrokerSvc")
    hiveDataBroker.DataProducers.append(ioalg)
    hiveDataBroker.DataProducers.append(countalg)

    scheduler = HLTControlFlowMgr(
        "HLTControlFlowMgr",
        CompositeCFNodes=[
            ("testIOAlg", "LAZY_AND", ["MDFIOMixer", "CountBanks"], True)
        ],
        ThreadPoolSize=1,
    )

    app = ApplicationMgr(
        EvtSel="NONE", EvtMax=evtMax, EventLoop=scheduler, TopAlg=[ioalg, countalg]
    )
    whiteboard = HiveWhiteBoard("EventDataSvc", EventSlots=1, ForceLeaves=True)
    app.ExtSvc.insert(0, whiteboard)


def test_read():
    test("IOAlgFileReadMixerExample", "")


def test_mmap():
    test("IOAlgMemoryMapMixerExample", "mmap")


def test_read_infinite():
    test("IOAlgFileReadMixerExample", "inf", -1)


def test_mmap_infinite():
    test("IOAlgMemoryMapMixerExample", "mmapinf", -1)


def test_read_toomuch():
    test("IOAlgFileReadMixerExample", "tm", 18)


def test_mmap_toomuch():
    test("IOAlgMemoryMapMixerExample", "mmaptm", 18)
