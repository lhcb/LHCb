###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Test to ensure a Reco14-Stripping20 mdst can be copied
from IOExample.Configuration import runIOTest

from PyConf.application import ApplicationOptions

options = ApplicationOptions(_enabled=False)
options.evt_max = 5
options.set_input_and_conds_from_testfiledb("R14S20-bhadron.mdst")
options.output_file = "PFN:Reco14-Stripping20.mdst"
options.output_type = "ROOT"
options.input_stream = "Trigger"

runIOTest(options, LoadAll=True, DataContent="MDST")
