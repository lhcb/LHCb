/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "IOMixerExample.h"
#include "MDF/IOAlgFileReadMixer.h"
#include "MDF/IOAlgMemoryMapMixer.h"

#include <random>

struct IOAlgFileReadMixerExample : IOMixerExample<LHCb::MDF::IOAlgFileReadMixer> {
  using IOMixerExample::IOMixerExample;
};

struct IOAlgMemoryMapMixerExample : IOMixerExample<LHCb::MDF::IOAlgMemoryMapMixer> {
  using IOMixerExample::IOMixerExample;
};

DECLARE_COMPONENT_WITH_ID( IOAlgFileReadMixerExample, "IOAlgFileReadMixerExample" )
DECLARE_COMPONENT_WITH_ID( IOAlgMemoryMapMixerExample, "IOAlgMemoryMapMixerExample" )
