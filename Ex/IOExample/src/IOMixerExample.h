/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Gaudi/Property.h>

#include <random>

class ISvcLocator;

template <typename BaseAlgo>
struct IOMixerExample : BaseAlgo {
  using BaseAlgo::BaseAlgo;
  unsigned int chooseInput( unsigned int max ) const override {
    static thread_local std::mt19937                         generator( m_randomSeed );
    std::uniform_int_distribution<std::mt19937::result_type> distrib( 0, max - 1 );
    auto                                                     res = distrib( generator );
    this->info() << "Took event from file " << res << endmsg;
    return res;
  }
  Gaudi::Property<unsigned int> m_randomSeed{ this, "RandomSeed", std::random_device{}(),
                                              "Random seed for the random numbers" };
};
