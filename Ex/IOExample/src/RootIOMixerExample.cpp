/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "IOAlgorithms/RootIOMixer.h"
#include "IOMixerExample.h"

struct RootIOMixerExample : IOMixerExample<LHCb::IO::RootIOMixer> {
  using IOMixerExample::IOMixerExample;
};

struct RootIOMixerExtExample : IOMixerExample<LHCb::IO::RootIOMixerExt> {
  using IOMixerExample::IOMixerExample;
};

DECLARE_COMPONENT_WITH_ID( RootIOMixerExample, "RootIOMixerExample" )
DECLARE_COMPONENT_WITH_ID( RootIOMixerExtExample, "RootIOMixerExtExample" )
