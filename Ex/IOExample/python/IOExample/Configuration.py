###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configurable for IO test applications
"""

__author__ = "Marco Cattaneo <Marco.Cattaneo@cern.ch>"

from PyConf.Algorithms import CopyInputStream, PrintHeader, RecordStream
from PyConf.application import (
    configure,
    configure_input,
    create_or_reuse_rootIOAlg,
    make_odin,
)
from PyConf.control_flow import CompositeNode
from PyConf.reading import get_mc_header, get_mc_particles, get_mc_vertices


def runIOTest(options, DataContent="DST", LoadAll=False):
    """
    Arguments :
        DataContent  : Content of dataset (SIM, DIGI, RAW, DST, ...)
        LoadAll      : Load all leaves of input file with StoreExplorerAlg
        OdinLocation : Where to find ODIN banks, if any needed
    """
    options.root_ioalg_name = "RootIOAlgExt"
    config = configure_input(options)

    algs = []
    if options.output_file != "":
        writer = CopyInputStream(
            name="CopyInputStream",
            InputFileLeavesLocation=create_or_reuse_rootIOAlg(
                options
            ).InputLeavesLocation,
            Output="DATAFILE='{}' SVC='Gaudi::RootCnvSvc' OPT='RECREATE'".format(
                options.output_file
            ),
        )
        fsrWriter = RecordStream(
            name="FSRInputCopyStream",
            ItemList=["/FileRecords#999"],
            EvtDataSvc="FileRecordDataSvc",
            EvtConversionSvc="FileRecordPersistencySvc",
            Output="DATAFILE='"
            + options.output_file
            + "' SVC='FileRecordCnvSvc' OPT='REC'",
        )
        algs = [writer, fsrWriter]
    algs.append(PrintHeader(name="PrintHeader", ODINLocation=make_odin()))
    if LoadAll:
        from PyConf.Algorithms import StoreExplorerAlg, TESFingerPrint

        storeExp = StoreExplorerAlg(
            name="StoreExplorerAlg", Load=True, PrintFreq=1, ExploreRelations=1
        )
        fingerPrint = TESFingerPrint(
            name="TESFingerPrint",
            HeuristicsLevel="Medium",
            OutputLevel=1,
            InputFileLeavesLocation=create_or_reuse_rootIOAlg(
                options
            ).InputLeavesLocation,
        )
        algs += [storeExp, fingerPrint]
    if DataContent.upper() == "DST":
        from PyConf.Algorithms import DumpTracks

        algs.append(DumpTracks(OutputLevel=2))
    if DataContent.upper() == "SIM":
        from PyConf.Algorithms import DumpHepMC

        algs.append(DumpHepMC())

    cf_node = CompositeNode("MainSequence", children=algs)
    config.update(configure(options, cf_node))
