/***********************************************************************************\
* (c) Copyright 1998-2021 CERN for the benefit of the LHCb and ATLAS collaborations *
*                                                                                   *
* This software is distributed under the terms of the Apache version 2 licence,     *
* copied verbatim in the file "LICENSE".                                            *
*                                                                                   *
* In applying this licence, CERN does not waive the privileges and immunities       *
* granted to it by virtue of its status as an Intergovernmental Organization        *
* or submit itself to any jurisdiction.                                             *
\***********************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE test_CaloHistos
#include <CaloFutureUtils/CellIDHistogram.h>

#include <GaudiKernel/PropertyHolder.h>

#include <boost/test/unit_test.hpp>

namespace {
  // Mock code for the test
  struct MonitoringHub : Gaudi::Monitoring::Hub {};
  struct ServiceLocator {
    MonitoringHub& monitoringHub() { return m_monitHub; }
    MonitoringHub  m_monitHub{};
  };
  struct BaseAlgo : INamedInterface, IProperty {
    unsigned long            addRef() override { return 0; };
    unsigned long            release() override { return 0; };
    void*                    i_cast( const InterfaceID& ) const override { return nullptr; }
    std::vector<std::string> getInterfaceNames() const override { return {}; }
    unsigned long            refCount() const override { return 0; }
    StatusCode               queryInterface( const InterfaceID&, void** ) override { return StatusCode::FAILURE; };
    const std::string&       name() const override { return m_name; };
    std::string              m_name{};
  };
  struct Algo : PropertyHolder<BaseAlgo> {
    ServiceLocator*            serviceLocator() { return &m_serviceLocator; }
    ServiceLocator             m_serviceLocator{};
    void                       registerCallBack( Gaudi::StateMachine::Transition, std::function<void()> ) {}
    Gaudi::StateMachine::State FSMState() const { return Gaudi::StateMachine::CONFIGURED; }
  };
  struct HistSink : public Gaudi::Monitoring::Hub::Sink {
    virtual void registerEntity( Gaudi::Monitoring::Hub::Entity ent ) override { m_entities.push_back( ent ); }
    virtual void removeEntity( Gaudi::Monitoring::Hub::Entity const& ent ) override {
      auto it = std::find( begin( m_entities ), end( m_entities ), ent );
      if ( it != m_entities.end() ) m_entities.erase( it );
    }
    std::deque<Gaudi::Monitoring::Hub::Entity> m_entities;
  };
  // Little helper for using automatic nlohmann conversion mechanism
  template <typename T>
  nlohmann::json toJSON( T const& t ) {
    nlohmann::json j = t;
    return t;
  }
} // namespace

BOOST_AUTO_TEST_CASE( test_calo_histos, *boost::unit_test::tolerance( 1e-14 ) ) {
  Algo algo;

  {
    // testing a basic ECal static Histogram
    LHCb::Calo::StaticCellIDHistogram<LHCb::Detector::Calo::CellCode::Index::EcalCalo> ecalBasic{
        &algo, "EcalBasic", "Ecal basic cellid histo" };
    auto jsonAxis = toJSON( ecalBasic ).at( "axis" )[0];
    BOOST_TEST( jsonAxis.at( "nBins" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    BOOST_TEST( jsonAxis.at( "minValue" ) == 0 );
    BOOST_TEST( jsonAxis.at( "maxValue" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    for ( unsigned int i : { 37, 59, 137 } ) {
      auto id = LHCb::Detector::Calo::DenseIndex::details::toCellID( i );
      ++ecalBasic[id]; // fill some random ids
    }
    for ( unsigned int i : { 37, 59, 137 } ) { BOOST_TEST( toJSON( ecalBasic ).at( "bins" )[i + 1] == 1 ); }

    // test invalid id
    LHCb::Detector::Calo::CellID invalid{ 999999 };
    ++ecalBasic[invalid];
    BOOST_TEST( toJSON( ecalBasic ).at( "bins" )[0] == 1 ); // should go to underflow bin
  }

  {
    // testing a basic HCal static Histogram
    LHCb::Calo::StaticCellIDHistogram<LHCb::Detector::Calo::CellCode::Index::HcalCalo> hcalBasic{
        &algo, "HcalBasic", "Hcal basic cellid histo" };
    auto jsonAxis = toJSON( hcalBasic ).at( "axis" )[0];
    BOOST_TEST( jsonAxis.at( "nBins" ) == LHCb::Detector::Calo::Index::nbHcalCells() );
    BOOST_TEST( jsonAxis.at( "minValue" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    BOOST_TEST( jsonAxis.at( "maxValue" ) == LHCb::Detector::Calo::Index::max() );
    for ( unsigned int i : { 37, 59, 137 } ) {
      auto id = LHCb::Detector::Calo::DenseIndex::details::toCellID( i + LHCb::Detector::Calo::Index::nbEcalCells() );
      ++hcalBasic[id]; // fill some random ids
    }
    for ( unsigned int i : { 37, 59, 137 } ) { BOOST_TEST( toJSON( hcalBasic ).at( "bins" )[i + 1] == 1 ); }

    // test invalid id
    LHCb::Detector::Calo::CellID invalid{ 999999 };
    ++hcalBasic[invalid];
    BOOST_TEST( toJSON( hcalBasic ).at( "bins" )[0] == 1 ); // should go to underflow bin
  }

  {
    // testing a weighted ECal static Histogram
    LHCb::Calo::StaticWeightedCellIDHistogram<LHCb::Detector::Calo::CellCode::Index::EcalCalo> ecalWeighted{
        &algo, "EcalWeighted", "Ecal weighted cellid histo" };
    auto jsonAxis = toJSON( ecalWeighted ).at( "axis" )[0];
    BOOST_TEST( jsonAxis.at( "nBins" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    BOOST_TEST( jsonAxis.at( "minValue" ) == 0 );
    BOOST_TEST( jsonAxis.at( "maxValue" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    for ( unsigned int i : { 37, 59, 137 } ) {
      auto id = LHCb::Detector::Calo::DenseIndex::details::toCellID( i );
      ecalWeighted[id] += i / 2.0; // fill some random ids
    }
    for ( unsigned int i : { 37, 59, 137 } ) { BOOST_TEST( toJSON( ecalWeighted ).at( "bins" )[i + 1] == i / 2.0 ); }

    // test invalid id
    LHCb::Detector::Calo::CellID invalid{ 999999 };
    ecalWeighted[invalid] += 3.0;
    BOOST_TEST( toJSON( ecalWeighted ).at( "bins" )[0] == 3.0 ); // should go to underflow bin
  }

  {
    // testing a weighted HCal static Histogram
    LHCb::Calo::StaticWeightedCellIDHistogram<LHCb::Detector::Calo::CellCode::Index::HcalCalo> hcalWeighted{
        &algo, "HcalWeighted", "Hcal weighted cellid histo" };
    auto jsonAxis = toJSON( hcalWeighted ).at( "axis" )[0];
    BOOST_TEST( jsonAxis.at( "nBins" ) == LHCb::Detector::Calo::Index::nbHcalCells() );
    BOOST_TEST( jsonAxis.at( "minValue" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    BOOST_TEST( jsonAxis.at( "maxValue" ) == LHCb::Detector::Calo::Index::max() );
    for ( unsigned int i : { 37, 59, 137 } ) {
      auto id = LHCb::Detector::Calo::DenseIndex::details::toCellID( i + LHCb::Detector::Calo::Index::nbEcalCells() );
      hcalWeighted[id] += i / 2.0; // fill some random ids
    }
    for ( unsigned int i : { 37, 59, 137 } ) { BOOST_TEST( toJSON( hcalWeighted ).at( "bins" )[i + 1] == i / 2.0 ); }

    // test invalid id
    LHCb::Detector::Calo::CellID invalid{ 999999 };
    hcalWeighted[invalid] += 5.0;
    BOOST_TEST( toJSON( hcalWeighted ).at( "bins" )[0] == 5.0 ); // should go to underflow bin
  }

  {
    // testing a 2D ECal static Histogram
    LHCb::Calo::StaticCellIDHistogram<LHCb::Detector::Calo::CellCode::Index::EcalCalo, 2> ecal2D{
        &algo, "Ecal2D", "Ecal 2D cellid histo", { 100, 0, 100 } };
    auto jsonAxis = toJSON( ecal2D ).at( "axis" )[0];
    BOOST_TEST( jsonAxis.at( "nBins" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    BOOST_TEST( jsonAxis.at( "minValue" ) == 0 );
    BOOST_TEST( jsonAxis.at( "maxValue" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    jsonAxis = toJSON( ecal2D ).at( "axis" )[1];
    BOOST_TEST( jsonAxis.at( "nBins" ) == 100 );
    BOOST_TEST( jsonAxis.at( "minValue" ) == 0 );
    BOOST_TEST( jsonAxis.at( "maxValue" ) == 100 );
    for ( unsigned int i : { 37, 59, 137 } ) {
      auto id = LHCb::Detector::Calo::DenseIndex::details::toCellID( i );
      ++ecal2D[{ id, i / 2.0 }]; // fill some random ids
    }
    for ( unsigned int i : { 37, 59, 137 } ) {
      BOOST_TEST( toJSON( ecal2D ).at(
                      "bins" )[( i + 1 ) + ( LHCb::Detector::Calo::Index::nbEcalCells() + 2 ) * ( i / 2 + 1 )] == 1 );
    }

    // test invalid id
    LHCb::Detector::Calo::CellID invalid{ 999999 };
    ++ecal2D[{ invalid, 2.5 }];
    BOOST_TEST( toJSON( ecal2D ).at( "bins" )[3 * ( 2 + LHCb::Detector::Calo::Index::nbEcalCells() )] ==
                1 ); // should go to underflow bin
  }

  {
    // testing a weighted 2D HCal static Histogram
    LHCb::Calo::StaticWeightedCellIDHistogram<LHCb::Detector::Calo::CellCode::Index::HcalCalo, 2> weightedHcal2D{
        &algo, "WeightedHcal2D", "Weighted Hcal 2D cellid histo", { 100, 0, 100 } };
    auto jsonAxis = toJSON( weightedHcal2D ).at( "axis" )[0];
    BOOST_TEST( jsonAxis.at( "nBins" ) == LHCb::Detector::Calo::Index::nbHcalCells() );
    BOOST_TEST( jsonAxis.at( "minValue" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    BOOST_TEST( jsonAxis.at( "maxValue" ) == LHCb::Detector::Calo::Index::max() );
    jsonAxis = toJSON( weightedHcal2D ).at( "axis" )[1];
    BOOST_TEST( jsonAxis.at( "nBins" ) == 100 );
    BOOST_TEST( jsonAxis.at( "minValue" ) == 0 );
    BOOST_TEST( jsonAxis.at( "maxValue" ) == 100 );
    for ( unsigned int i : { 37, 59, 137 } ) {
      auto id = LHCb::Detector::Calo::DenseIndex::details::toCellID( i + LHCb::Detector::Calo::Index::nbEcalCells() );
      weightedHcal2D[{ id, i / 2.0 }] += i / 2.0; // fill some random ids
    }
    for ( unsigned int i : { 37, 59, 137 } ) {
      BOOST_TEST( toJSON( weightedHcal2D )
                      .at( "bins" )[( i + 1 ) + ( LHCb::Detector::Calo::Index::nbHcalCells() + 2 ) * ( i / 2 + 1 )] ==
                  i / 2.0 );
    }

    // test invalid id
    LHCb::Detector::Calo::CellID invalid{ 999999 };
    weightedHcal2D[{ invalid, 2.5 }] += 5.0;
    BOOST_TEST( toJSON( weightedHcal2D ).at( "bins" )[3 * ( 2 + LHCb::Detector::Calo::Index::nbHcalCells() )] ==
                5.0 ); // should go to underflow bin
  }

  {
    // testing a basic ECal non static Histogram
    LHCb::Calo::CellIDHistogram<LHCb::Detector::Calo::CellCode::Index::EcalCalo> ecalBasic{ &algo, "EcalBasic" };
    algo.setProperty( "EcalBasic_Title", "Ecal basic cellid histo" ).ignore();
    ecalBasic.createHistogram( algo );
    auto jsonAxis = toJSON( ecalBasic ).at( "axis" )[0];
    BOOST_TEST( jsonAxis.at( "nBins" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    BOOST_TEST( jsonAxis.at( "minValue" ) == 0 );
    BOOST_TEST( jsonAxis.at( "maxValue" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    for ( unsigned int i : { 37, 59, 137 } ) {
      auto id = LHCb::Detector::Calo::DenseIndex::details::toCellID( i );
      ++ecalBasic[id]; // fill some random ids
    }
    for ( unsigned int i : { 37, 59, 137 } ) { BOOST_TEST( toJSON( ecalBasic ).at( "bins" )[i + 1] == 1 ); }

    // test invalid id
    LHCb::Detector::Calo::CellID invalid{ 999999 };
    ++ecalBasic[invalid];
    BOOST_TEST( toJSON( ecalBasic ).at( "bins" )[0] == 1 ); // should go to underflow bin
  }

  {
    // testing a 2D ECal Histogram
    LHCb::Calo::CellIDHistogram<LHCb::Detector::Calo::CellCode::Index::EcalCalo, 2> ecal2D{ &algo, "Ecal2D" };
    algo.setProperty( "Ecal2D_Title", "Ecal 2D cellid histo" ).ignore();
    algo.setProperty( "Ecal2D_Axis0", "( 100, 0, 100, \"Value\" )" ).ignore();
    ecal2D.createHistogram( algo );
    auto jsonAxis = toJSON( ecal2D ).at( "axis" )[0];
    BOOST_TEST( jsonAxis.at( "nBins" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    BOOST_TEST( jsonAxis.at( "minValue" ) == 0 );
    BOOST_TEST( jsonAxis.at( "maxValue" ) == LHCb::Detector::Calo::Index::nbEcalCells() );
    jsonAxis = toJSON( ecal2D ).at( "axis" )[1];
    BOOST_TEST( jsonAxis.at( "nBins" ) == 100 );
    BOOST_TEST( jsonAxis.at( "minValue" ) == 0 );
    BOOST_TEST( jsonAxis.at( "maxValue" ) == 100 );
    for ( unsigned int i : { 37, 59, 137 } ) {
      auto id = LHCb::Detector::Calo::DenseIndex::details::toCellID( i );
      ++ecal2D[{ id, i / 2.0 }]; // fill some random ids
    }
    for ( unsigned int i : { 37, 59, 137 } ) {
      BOOST_TEST( toJSON( ecal2D ).at(
                      "bins" )[( i + 1 ) + ( LHCb::Detector::Calo::Index::nbEcalCells() + 2 ) * ( i / 2 + 1 )] == 1 );
    }

    // test invalid id
    LHCb::Detector::Calo::CellID invalid{ 999999 };
    ++ecal2D[{ invalid, 2.5 }];
    BOOST_TEST( toJSON( ecal2D ).at( "bins" )[3 * ( 2 + LHCb::Detector::Calo::Index::nbEcalCells() )] ==
                1 ); // should go to underflow bin
  }
}
