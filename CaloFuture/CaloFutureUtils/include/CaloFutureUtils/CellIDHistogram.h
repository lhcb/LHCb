/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <Detector/Calo/CaloCellID.h>

#include <Gaudi/Accumulators/AxisAsProperty.h>
#include <Gaudi/Accumulators/HistogramWrapper.h>
#include <Gaudi/Accumulators/StaticHistogram.h>

#include <tuple>

namespace LHCb::Calo {

  // forward declaration of CellIDHistogramInputTypes
  template <unsigned int NIndex, typename... Elements>
  class CellIDHistogramInputType;
  template <unsigned int NIndex, typename WArithmetic, typename... Elements>
  struct WeightedCellIDHistogramInputType;

  namespace {
    // Helper instantiating CellIDHistogramInputType with a reduced parameter pack
    // by using only the first N items among the ones given in the input Tuple
    template <unsigned int NIndex, typename ParamsTuple, typename Seq>
    struct ReducedCellIDHistogramInputType_t;
    template <unsigned int NIndex, typename ParamsTuple, size_t... I>
    struct ReducedCellIDHistogramInputType_t<NIndex, ParamsTuple, std::index_sequence<I...>> {
      using type =
          CellIDHistogramInputType<NIndex, std::decay_t<decltype( std::get<I>( std::declval<ParamsTuple>() ) )>...>;
    };
    template <unsigned int NIndex, typename ParamsTuple, unsigned int N>
    using ReducedCellIDHistogramInputType =
        typename ReducedCellIDHistogramInputType_t<NIndex, ParamsTuple, std::make_index_sequence<N>>::type;

    template <unsigned int NIndex, typename ParamsTuple>
    struct CellIDHistogramInputTypeFromTuple;
    template <unsigned int NIndex, typename... Elements>
    struct CellIDHistogramInputTypeFromTuple<NIndex, std::tuple<Elements...>>
        : CellIDHistogramInputType<NIndex, Elements...> {
      using CellIDHistogramInputType<NIndex, Elements...>::CellIDHistogramInputType;
    };

    template <unsigned int NIndex, typename WArithmetic, typename ParamsTuple>
    struct WeightedCellIDHistogramInputTypeFromTuple;
    template <unsigned int NIndex, typename WArithmetic, typename... Elements>
    struct WeightedCellIDHistogramInputTypeFromTuple<NIndex, WArithmetic, std::tuple<Elements...>>
        : WeightedCellIDHistogramInputType<NIndex, WArithmetic, Elements...> {
      using WeightedCellIDHistogramInputType<NIndex, WArithmetic, Elements...>::WeightedCellIDHistogramInputType;
    };
  } // namespace

  /**
   * InputType dedicated to CellID based histograms, that is to histograms
   * where the first dimension is based on cellID
   */
  template <unsigned int NIndex, typename... Elements>
  class CellIDHistogramInputType : public std::tuple<Detector::Calo::CellID, Elements...> {
  public:
    using Parent = std::tuple<Detector::Calo::CellID, Elements...>;
    // allow construction from set of values
    using Parent::Parent;
    using ValueType = ReducedCellIDHistogramInputType<NIndex, std::tuple<Elements...>, NIndex - 1>;
    template <class... AxisType, typename = typename std::enable_if_t<( sizeof...( AxisType ) == NIndex )>>
    unsigned int computeIndex( std::tuple<AxisType...> const& axis ) const {
      return computeIndexInternal<0, std::tuple<AxisType...>>( axis );
    }
    auto forInternalCounter() { return 1ul; }
    template <class... AxisType, typename = typename std::enable_if_t<( sizeof...( AxisType ) == NIndex )>>
    static unsigned int computeTotNBins( std::tuple<AxisType...> const& axis ) {
      return computeTotNBinsInternal<0, std::tuple<AxisType...>>( axis );
    }

  private:
    template <int N, class Tuple>
    unsigned int computeIndexInternal( Tuple const& allAxis ) const {
      // compute global index. Bins are stored in a column first manner
      auto const& axis       = std::get<N>( allAxis );
      int         localIndex = -1;
      if constexpr ( N == 0 ) {
        // special cellId dimension
        localIndex = Detector::Calo::denseIndex( std::get<N>( *this ) );
        // if invalid, will be -1 and should go to index 0 (underflow bin) else just shift
        localIndex = localIndex < 0 ? 0 : localIndex - axis.minValue() + 1;
      } else {
        localIndex = axis.index( std::get<N>( *this ) );
      }
      if constexpr ( N + 1 == NIndex ) {
        return localIndex;
      } else {
        return localIndex + ( axis.numBins() + 2 ) * computeIndexInternal<N + 1, Tuple>( allAxis );
      }
    }
    template <int N, class Tuple>
    static unsigned int computeTotNBinsInternal( Tuple const& allAxis ) {
      auto const&  axis       = std::get<N>( allAxis );
      unsigned int localNBins = axis.numBins() + 2;
      if constexpr ( N + 1 == NIndex )
        return localNBins;
      else
        return localNBins * computeTotNBinsInternal<N + 1, Tuple>( allAxis );
    }
  };

  /// specialization of CellIDHistogramInputType for ND == 1 in order to have simpler syntax
  /// that is, no tuple of one item
  template <>
  class CellIDHistogramInputType<1> {
  public:
    using ValueType = Detector::Calo::CellID;
    CellIDHistogramInputType( Detector::Calo::CellID a ) : value( a ) {}
    template <typename AxisType>
    unsigned int computeIndex( std::tuple<AxisType> const& allAxis ) const {
      auto const& axis = std::get<0>( allAxis );
      // if invalid, denseindex will be -1 and should go to index 0 (underflow bin) else just shift
      int localIndex = Detector::Calo::denseIndex( value );
      return localIndex < 0 ? 0 : localIndex - axis.minValue() + 1;
    }
    Detector::Calo::CellID& operator[]( int ) { return value; }
    auto                    forInternalCounter() { return 1ul; }
    template <typename AxisType>
    static unsigned int computeTotNBins( std::tuple<AxisType> allAxis ) {
      auto const& axis = std::get<0>( allAxis );
      return axis.nBins + 2;
    }

  private:
    Detector::Calo::CellID value;
  };

  /**
   * InputType dedicated to weighted CellID based histograms, that is to weighted histograms
   * where the first dimension is based on cellID
   */
  template <unsigned int NIndex, typename WArithmetic, typename... Elements>
  struct WeightedCellIDHistogramInputType : std::pair<CellIDHistogramInputType<NIndex, Elements...>, WArithmetic> {
    using ValueType = typename CellIDHistogramInputType<NIndex, Elements...>::ValueType;
    using std::pair<CellIDHistogramInputType<NIndex, Elements...>, WArithmetic>::pair;
    template <class... AxisType, typename = typename std::enable_if_t<( sizeof...( AxisType ) == NIndex )>>
    unsigned int computeIndex( std::tuple<AxisType...> const& axis ) const {
      return this->first.computeIndex( axis );
    }
    auto forInternalCounter() { return std::pair( this->first.forInternalCounter(), this->second ); }
    template <class... AxisType, typename = typename std::enable_if_t<( sizeof...( AxisType ) == NIndex )>>
    static unsigned int computeTotNBins( std::tuple<AxisType...> const& axis ) {
      return CellIDHistogramInputType<NIndex, Elements...>::computeTotNBins( axis );
    }
  };

  /**
   * Class implementing a regular cellid based histogram accumulator
   */
  template <Gaudi::Accumulators::atomicity Atomicity, typename Arithmetic, typename ND, typename AxisTupleType>
  using CellIDHistogramingAccumulator = Gaudi::Accumulators::HistogramingAccumulatorInternal<
      Atomicity, CellIDHistogramInputTypeFromTuple<ND::value, AxisToArithmetic_t<AxisTupleType>>, unsigned long,
      Gaudi::Accumulators::IntegralAccumulator, AxisTupleType>;

  /**
   * Class implementing a weighted cellid based histogram accumulator
   */
  template <Gaudi::Accumulators::atomicity Atomicity, typename Arithmetic, typename ND, typename AxisTupleType>
  using WeightedCellIDHistogramingAccumulator = Gaudi::Accumulators::HistogramingAccumulatorInternal<
      Atomicity, WeightedCellIDHistogramInputTypeFromTuple<ND::value, Arithmetic, AxisToArithmetic_t<AxisTupleType>>,
      Arithmetic, Gaudi::Accumulators::WeightedCountAccumulator, AxisTupleType>;

  /**
   * Class implementing a profile cellid based histogram accumulator
   */
  template <Gaudi::Accumulators::atomicity Atomicity, typename Arithmetic, typename ND, typename AxisTupleType>
  using ProfileCellIDHistogramingAccumulator = Gaudi::Accumulators::HistogramingAccumulatorInternal<
      Atomicity, CellIDHistogramInputTypeFromTuple<ND::value, ProfileAxisToArithmetic_t<Arithmetic, AxisTupleType>>,
      Arithmetic, Gaudi::Accumulators::SigmaAccumulator, AxisTupleType>;

  /**
   * Class implementing a weighted profile cellid based histogram accumulator
   */
  template <Gaudi::Accumulators::atomicity Atomicity, typename Arithmetic, typename ND, typename AxisTupleType>
  using WeightedProfileCellIDHistogramingAccumulator = Gaudi::Accumulators::HistogramingAccumulatorInternal<
      Atomicity,
      WeightedCellIDHistogramInputTypeFromTuple<ND::value, Arithmetic,
                                                ProfileAxisToArithmetic_t<Arithmetic, AxisTupleType>>,
      Arithmetic, Gaudi::Accumulators::SigmaAccumulator, AxisTupleType>;

  namespace naming {
    constexpr char cellIDHistogramString[]                = "histogram:CellIDHistogram";
    constexpr char weightedCellIDHistogramString[]        = "histogram:WeightedCellIDHistogram";
    constexpr char profileCellIDHistogramString[]         = "histogram:ProfileCellIDHistogram";
    constexpr char weightedProfileCellIDHistogramString[] = "histogram:WeightedProfileCellIDHistogram";
  } // namespace naming

  /**
   * Definition of a generic CellID based Histograms.
   * Internal implementation defining the custom constructor where the CellID specialized Axis is created
   */
  template <Detector::Calo::CellCode::Index calo, unsigned int ND, Gaudi::Accumulators::atomicity Atomicity,
            typename Arithmetic, const char* Type,
            template <Gaudi::Accumulators::atomicity, typename, typename, typename> typename Accumulator,
            typename AxisTupleType>
  struct CellIDHistogramInternal;
  template <Detector::Calo::CellCode::Index calo, unsigned int ND, Gaudi::Accumulators::atomicity Atomicity,
            typename Arithmetic, const char* Type,
            template <Gaudi::Accumulators::atomicity, typename, typename, typename> typename Accumulator,
            typename... AxisTypes>
  struct CellIDHistogramInternal<calo, ND, Atomicity, Arithmetic, Type, Accumulator, std::tuple<AxisTypes...>>
      : Gaudi::Accumulators::HistogramingCounterBase<ND, Atomicity, Arithmetic, Type, Accumulator,
                                                     std::tuple<Gaudi::Accumulators::Axis<Arithmetic>, AxisTypes...>> {
    using AxisTupleType = std::tuple<AxisTypes...>;
    // overwritten constructor to force definition of the first axis
    template <typename OWNER>
    CellIDHistogramInternal( OWNER* owner, std::string const& name, std::string const& title, AxisTypes... axis )
        : Gaudi::Accumulators::HistogramingCounterBase<ND, Atomicity, Arithmetic, Type, Accumulator,
                                                       std::tuple<Gaudi::Accumulators::Axis<Arithmetic>, AxisTypes...>>(
              owner, name, title,
              // axis for the cellID dimension
              { calo == Detector::Calo::CellCode::Index::EcalCalo ? Detector::Calo::Index::nbEcalCells()
                                                                  : Detector::Calo::Index::nbHcalCells(),
                calo == Detector::Calo::CellCode::Index::EcalCalo ? 0 : Detector::Calo::Index::nbEcalCells(),
                calo == Detector::Calo::CellCode::Index::EcalCalo ? Detector::Calo::Index::nbEcalCells()
                                                                  : Detector::Calo::Index::max() },
              // other axis, if any (that is ND > 1)
              axis... ) {}
    /// This constructor takes the axis as a tuple
    template <typename OWNER>
    CellIDHistogramInternal( OWNER* owner, std::string const& name, std::string const& title, AxisTupleType allAxis )
        : Gaudi::Accumulators::HistogramingCounterBase<ND, Atomicity, Arithmetic, Type, Accumulator,
                                                       std::tuple<Gaudi::Accumulators::Axis<Arithmetic>, AxisTypes...>>(
              owner, name, title,
              std::tuple_cat( // axis for the cellID dimension
                  std::tuple<Gaudi::Accumulators::Axis<Arithmetic>>{
                      { calo == Detector::Calo::CellCode::Index::EcalCalo ? Detector::Calo::Index::nbEcalCells()
                                                                          : Detector::Calo::Index::nbHcalCells(),
                        calo == Detector::Calo::CellCode::Index::EcalCalo ? 0 : Detector::Calo::Index::nbEcalCells(),
                        calo == Detector::Calo::CellCode::Index::EcalCalo ? Detector::Calo::Index::nbEcalCells()
                                                                          : Detector::Calo::Index::max() } },
                  // other axis, if any (that is ND > 1)
                  allAxis ) ) {}
  };

  /**
   * Definition of a CellID based Histograms
   *
   * These are histograms for the calorimeter where the first dimension is indexed by cellID.
   * Bins are indexed using the denseIndex method of cellID in order
   * to be compact despite the change of granularity between regions
   * other dimensions are free
   */
  template <Detector::Calo::CellCode::Index calo, unsigned int ND = 1,
            Gaudi::Accumulators::atomicity Atomicity = Gaudi::Accumulators::atomicity::full,
            typename Arithmetic                      = double,
            typename AxisTupleType                   = make_tuple_t<Gaudi::Accumulators::Axis<Arithmetic>, ND - 1>>
  using StaticCellIDHistogram = CellIDHistogramInternal<calo, ND, Atomicity, Arithmetic, naming::cellIDHistogramString,
                                                        CellIDHistogramingAccumulator, AxisTupleType>;

  /**
   * Definition of a weighted CellID based Histograms
   *
   * These is a weighted histogram for the calorimeter indexed by cellID.
   * Bins are indexed using the denseIndex method of cellID in order
   * to be compact despite the change of granularity between regions
   */
  template <Detector::Calo::CellCode::Index calo, unsigned int ND = 1,
            Gaudi::Accumulators::atomicity Atomicity = Gaudi::Accumulators::atomicity::full,
            typename Arithmetic                      = double,
            typename AxisTupleType                   = make_tuple_t<Gaudi::Accumulators::Axis<Arithmetic>, ND - 1>>
  using StaticWeightedCellIDHistogram =
      CellIDHistogramInternal<calo, ND, Atomicity, Arithmetic, naming::weightedCellIDHistogramString,
                              WeightedCellIDHistogramingAccumulator, AxisTupleType>;

  /**
   * Definition of a profile CellID based Histograms
   *
   * These is a profile histogram for the calorimeter indexed by cellID.
   * Bins are indexed using the denseIndex method of cellID in order
   * to be compact despite the change of granularity between regions
   */
  template <Detector::Calo::CellCode::Index calo, unsigned int ND = 1,
            Gaudi::Accumulators::atomicity Atomicity = Gaudi::Accumulators::atomicity::full,
            typename Arithmetic                      = double,
            typename AxisTupleType                   = make_tuple_t<Gaudi::Accumulators::Axis<Arithmetic>, ND - 1>>
  using StaticProfileCellIDHistogram =
      CellIDHistogramInternal<calo, ND, Atomicity, Arithmetic, naming::profileCellIDHistogramString,
                              ProfileCellIDHistogramingAccumulator, AxisTupleType>;

  /**
   * Definition of a weighted profile CellID based Histograms
   *
   * These is a profile histogram for the calorimeter indexed by cellID.
   * Bins are indexed using the denseIndex method of cellID in order
   * to be compact despite the change of granularity between regions
   */
  template <Detector::Calo::CellCode::Index calo, unsigned int ND = 1,
            Gaudi::Accumulators::atomicity Atomicity = Gaudi::Accumulators::atomicity::full,
            typename Arithmetic                      = double,
            typename AxisTupleType                   = make_tuple_t<Gaudi::Accumulators::Axis<Arithmetic>, ND - 1>>
  using StaticWeightedProfileCellIDHistogram =
      CellIDHistogramInternal<calo, ND, Atomicity, Arithmetic, naming::weightedProfileCellIDHistogramString,
                              WeightedProfileCellIDHistogramingAccumulator, AxisTupleType>;

  /**
   * Customizable versin of StaticCellIDHistogram
   *
   * properties *_Title, *_Axis<N> can be used to set the title and the axis of the histogram
   * "*" here is the name of the histogram and <N> the axis number, starting at 0 and excluding
   * the CellID based axis, as it cannot be changed
   */
  template <Detector::Calo::CellCode::Index calo, unsigned int ND = 1,
            Gaudi::Accumulators::atomicity Atomicity = Gaudi::Accumulators::atomicity::full,
            typename Arithmetic                      = double,
            typename AxisTupleType                   = make_tuple_t<Gaudi::Accumulators::Axis<Arithmetic>, ND - 1>>
  using CellIDHistogram =
      Gaudi::Accumulators::HistogramWrapper<StaticCellIDHistogram<calo, ND, Atomicity, Arithmetic, AxisTupleType>>;

  /**
   * Customizable versin of StaticWeightedCellIDHistogram
   *
   * properties *_Title, *_Axis<N> can be used to set the title and the axis of the histogram
   * "*" here is the name of the histogram and <N> the axis number, starting at 0 and excluding
   * the CellID based axis, as it cannot be changed
   */
  template <Detector::Calo::CellCode::Index calo, unsigned int ND = 1,
            Gaudi::Accumulators::atomicity Atomicity = Gaudi::Accumulators::atomicity::full,
            typename Arithmetic                      = double,
            typename AxisTupleType                   = make_tuple_t<Gaudi::Accumulators::Axis<Arithmetic>, ND - 1>>
  using WeightedCellIDHistogram = Gaudi::Accumulators::HistogramWrapper<
      StaticWeightedCellIDHistogram<calo, ND, Atomicity, Arithmetic, AxisTupleType>>;

  /**
   * Customizable versin of StaticProfileCellIDHistogram
   *
   * properties *_Title, *_Axis<N> can be used to set the title and the axis of the histogram
   * "*" here is the name of the histogram and <N> the axis number, starting at 0 and excluding
   * the CellID based axis, as it cannot be changed
   */
  template <Detector::Calo::CellCode::Index calo, unsigned int ND = 1,
            Gaudi::Accumulators::atomicity Atomicity = Gaudi::Accumulators::atomicity::full,
            typename Arithmetic                      = double,
            typename AxisTupleType                   = make_tuple_t<Gaudi::Accumulators::Axis<Arithmetic>, ND - 1>>
  using ProfileCellIDHistogram = Gaudi::Accumulators::HistogramWrapper<
      StaticProfileCellIDHistogram<calo, ND, Atomicity, Arithmetic, AxisTupleType>>;

  /**
   * Customizable versin of StaticWeightedProfileCellIDHistogram
   *
   * properties *_Title, *_Axis<N> can be used to set the title and the axis of the histogram
   * "*" here is the name of the histogram and <N> the axis number, starting at 0 and excluding
   * the CellID based axis, as it cannot be changed
   */
  template <Detector::Calo::CellCode::Index calo, unsigned int ND = 1,
            Gaudi::Accumulators::atomicity Atomicity = Gaudi::Accumulators::atomicity::full,
            typename Arithmetic                      = double,
            typename AxisTupleType                   = make_tuple_t<Gaudi::Accumulators::Axis<Arithmetic>, ND - 1>>
  using WeightedProfileCellIDHistogram = Gaudi::Accumulators::HistogramWrapper<
      StaticWeightedProfileCellIDHistogram<calo, ND, Atomicity, Arithmetic, AxisTupleType>>;

} // namespace LHCb::Calo
