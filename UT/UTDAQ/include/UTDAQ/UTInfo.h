/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/UTLiteCluster.h"
#include "UTDet/DeUTDetector.h"

#include <string>

/** Define some numbers for the UT which are detector specific
 *
 *
 *  @author Michel De Cian
 *  @date   2019-05-31
 */

namespace UTInfo {

  enum class DetectorNumbers : unsigned {
    Sectors     = 98,
    Regions     = 3,
    Layers      = 2,
    Stations    = 2,
    TotalLayers = 4,
    SubSectors  = 2,
    Modules     = 8,
    Faces       = 2,
    Staves      = 9,
    HalfLayers  = 4,
    Sides       = 2
  };

  enum class MasksBits : unsigned {
    LayerMask     = 0x180000,
    StationMask   = 0x600000,
    LayerBits     = 19,
    StationBits   = 21,
    HalfLayerMask = 0xc0000,
    HalfLayerBits = 18
  };

  enum class SectorNumbers : unsigned {
    EffectiveSectorsPerColumn = 28,
    MaxSectorsPerRegion       = 98,
    NumSectorsUTa             = 896,
    NumSectorsUTb             = 1008,
    MaxSectorsPerBoard        = 6,
    MaxBoards                 = 240 // bank v4: 240, bank v5: 216
  };

  constexpr const unsigned int MaxSectorsAllBoards = static_cast<unsigned int>( SectorNumbers::MaxSectorsPerBoard ) *
                                                     static_cast<unsigned int>( SectorNumbers::MaxBoards );
  constexpr const unsigned int MaxNumberOfSectors =
      static_cast<unsigned int>( DetectorNumbers::Sides ) * static_cast<unsigned int>( DetectorNumbers::HalfLayers ) *
      static_cast<unsigned int>( DetectorNumbers::Staves ) * static_cast<unsigned int>( DetectorNumbers::Faces ) *
      static_cast<unsigned int>( DetectorNumbers::Modules ) * static_cast<unsigned int>( DetectorNumbers::SubSectors );

  //--- Final Hit Location
  const std::string HitLocation = "UT/UTHits";

  const unsigned int upper_row          = 13;
  const unsigned int module_flipA       = 3;
  const unsigned int module_flipB       = 4;
  const unsigned int Atypestave_id      = 0;
  const unsigned int Ctypestave_startid = 2;
  const unsigned int nonAsensor_startid = 9;
  const unsigned int nonAsensor_endid   = 18;

} // namespace UTInfo
