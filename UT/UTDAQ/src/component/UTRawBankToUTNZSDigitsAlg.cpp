/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class UTRawBankToUTNZSDigitsAlg
 *
 *  Algorithm to decode UTNZSRawBank to UTDigits (TELL40 NZS decoder)
 *  Implementation file for class : UTNZSRawBankToUTDigitsAlg
 *
 *  - \b BankLocation: Location of UTRawBank
 *  - \b OutputDigitData:  Location of output UTDigit
 *
 *  @author Wojciech Krupa (based on code by Xuhao Yuan, wokrupa@cern.ch)
 *  @date   2021-04-19  / 2023-05-25 / 2024-03-13
 */

#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/UTDigit.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQBoard.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include "Kernel/UTNZSDecoder.h"
#include "LHCbAlgs/Transformer.h"
#include "UTDAQ/UTDAQHelper.h"
#include <algorithm>
#include <bitset>
#include <cstdint>
#include <iomanip>
#include <string>
#include <vector>

typedef std::vector<std::pair<unsigned int, int>> BanksTypes;

class UTRawBankToUTNZSDigitsAlg : public LHCb::Algorithm::Transformer<LHCb::UTDigits( const LHCb::RawBank::View& )> {

public:
  /// Standard constructor
  UTRawBankToUTNZSDigitsAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer{
            name, pSvcLocator, { { "UTBank", {} } }, { { "OutputDigitData", LHCb::UTDigitLocation::UTDigits } } } {}

  LHCb::UTDigits operator()( const LHCb::RawBank::View& ) const override; ///< Algorithm execution

  // Counters for decoded banks
  mutable Gaudi::Accumulators::Counter<> m_validHeaders{ this, "#Nb Valid UT banks" };
  mutable Gaudi::Accumulators::Counter<> m_validDigits{ this, "#Nb Valid UT digits" };
  mutable Gaudi::Accumulators::Counter<> m_invalidBanks{ this, "#Nb Invalid banks" };
  mutable Gaudi::Accumulators::Counter<> m_errors_insert{ this, "#Error - failure during inserting hit in TES" };

private:
  // The readout tool for handling DAQID and ChannelID
  ToolHandle<IUTReadoutTool> readoutTool{ this, "ReadoutTool", "UTReadoutTool" };
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UTRawBankToUTNZSDigitsAlg )

using namespace LHCb;

//=============================================================================
// Main execution
//=============================================================================

LHCb::UTDigits UTRawBankToUTNZSDigitsAlg::operator()( const LHCb::RawBank::View& banks ) const {

  LHCb::UTDigits digits;

  for ( const LHCb::RawBank* bank : banks ) {

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "------------------------- ------- UT RAW BANK  ------------------------------- -----------" << endmsg;
      debug() << "Bank: 0x" << std::uppercase << std::hex << bank->sourceID() << endmsg;
      debug() << "   Size: " << std::dec << bank->size() << "B" << endmsg;
      debug() << "   Type: " << bank->type() << endmsg;
      debug() << "   Version: " << bank->version() << endmsg;
      debug() << "|     |      HEADER                 | LANE5        | LANE4        | LANE3        | LANE2        "
                 "| LANE1        | LANE0       |"
              << endmsg;
      // Dump raw UT bank
      u_int8_t     iw2     = 0;
      unsigned int counter = 0;
      for ( const u_int8_t* w = bank->begin<u_int8_t>(); w != bank->end<u_int8_t>(); ++w ) {
        ++iw2;
        if ( ( iw2 - 1 ) % 32 == 0 ) {
          counter++;
          if ( counter % 34 == 0 ) {
            debug() << endmsg;
            debug() << "---------------------------------------------------------------------------------------------"
                       "-------------------------------"
                    << endmsg;
          }
          debug() << endmsg;
          debug() << "0x" << std::setfill( '0' ) << std::setw( 2 ) << std::hex << unsigned( iw2 ) << " ";
        }
        if ( ( iw2 - 1 ) % 4 == 0 ) debug() << " | ";

        debug() << std::uppercase << std::hex << std::setw( 2 ) << unsigned( *w ) << " ";
      }
      debug() << endmsg;
    }

    auto decode = [&]( UTNZSDecoder<::UTDAQ::version::v5> decoder ) {
      if ( msgLevel( MSG::DEBUG ) ) {
        std::string bxid = std::bitset<12>( decoder.getBXID() ).to_string();
        std::string flag = std::bitset<4>( decoder.getflags() ).to_string();
        debug() << "Bxid: " << bxid << " " << std::dec << decoder.getBXID() << " Flag bits: " << flag << endmsg;
        debug() << "Clusters in lanes from 0 to 5: ";
        for ( unsigned int lane = 0; lane < 6; lane++ ) { debug() << decoder.nClusters( lane ) << " "; }
        debug() << endmsg;
      }

      // Decode NZS event
      for ( const UTNZSWord& aWord : decoder.posRange() ) {
        try {

          if ( msgLevel( MSG::DEBUG ) ) {
            std::string binary = std::bitset<16>( aWord.value() ).to_string();
            debug() << "----------- DECODED DIGIT --------------------" << endmsg;
            debug() << "Binary: " << binary << " Hex: " << std::hex << std::setfill( '0' ) << std::uppercase
                    << std::setw( 4 ) << aWord.value() << endmsg;
            debug() << "Position in the bank: " << std::dec << aWord.pos() << endmsg;
            debug() << "Lane: " << std::dec << aWord.lane() << "  ASIC: " << aWord.asic_num() << endmsg;
          }
          // Decode left digit of the NZS Word
          if ( aWord.not_nzs_header() ) {
            ++m_validDigits;
            UTDAQID daqidL =
                UTDAQID( ( UTDAQID::BoardID )( LHCb::UTDAQ::boardIDfromSourceID( bank->sourceID() ) ),
                         ( UTDAQID::LaneID )( aWord.lane() ), ( UTDAQID::ChannelID )( aWord.channelIDL() & 0x1ff ) );

            Detector::UT::ChannelID channelIDL = readoutTool->daqIDToChannelID( daqidL );

            if ( msgLevel( MSG::DEBUG ) ) {
              debug() << "Channel from data bank: " << std::dec << aWord.channelIDL()
                      << " ADC from data bank: " << static_cast<int16_t>( (int8_t)aWord.adcL() ) << endmsg;
              debug() << daqidL << endmsg;
              debug() << channelIDL << endmsg;
              debug() << "Left Channel: " << aWord.channelIDL()
                      << " ADC: " << static_cast<int16_t>( (int8_t)aWord.adcL() ) << endmsg;
            }
            try {
              digits.insert( new UTDigit( channelIDL, (int8_t)aWord.adcL(), daqidL.id(), aWord.asic_num(),
                                          aWord.mcm_val(), aWord.mcm_ch() ),
                             channelIDL );
            } catch ( ... ) {
              if ( msgLevel( MSG::DEBUG ) ) debug() << "Problem with inserting" << endmsg;
              ++m_errors_insert;
              continue;
            }
          } else {
            if ( msgLevel( MSG::DEBUG ) ) debug() << "Skipping this digit" << endmsg;
          }
          // Decode right digit of the NZS Word
          if ( aWord.not_nzs_header() ) {
            ++m_validDigits;
            UTDAQID daqidR =
                UTDAQID( ( UTDAQID::BoardID )( LHCb::UTDAQ::boardIDfromSourceID( bank->sourceID() ) ),
                         ( UTDAQID::LaneID )( aWord.lane() ), ( UTDAQID::ChannelID )( aWord.channelIDR() & 0x1ff ) );

            Detector::UT::ChannelID channelIDR = readoutTool->daqIDToChannelID( daqidR );

            if ( msgLevel( MSG::DEBUG ) ) {
              std::string binary = std::bitset<16>( aWord.value() ).to_string();
              debug() << "Channel from data bank: " << std::dec << aWord.channelIDR()
                      << " ADC from data bank: " << static_cast<int16_t>( (int8_t)aWord.adcR() ) << endmsg;
              debug() << daqidR << endmsg;
              debug() << channelIDR << endmsg;
              debug() << "Right Channel: " << aWord.channelIDR()
                      << " ADC: " << static_cast<int16_t>( (int8_t)aWord.adcR() ) << endmsg;
            }
            try {
              digits.insert( new UTDigit( channelIDR, (int8_t)aWord.adcR(), daqidR.id(), aWord.asic_num(),
                                          aWord.mcm_val(), aWord.mcm_ch() ),
                             channelIDR );
            } catch ( ... ) {
              if ( msgLevel( MSG::DEBUG ) ) debug() << "Problem with inserting" << endmsg;
              ++m_errors_insert;
              continue;
            }
          } else {
            if ( msgLevel( MSG::DEBUG ) ) debug() << "Skipping this digit" << endmsg;
          }
        } catch ( const std::exception& ex ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << ex.what() << endmsg;
        } catch ( ... ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Uknown type issue for: " << bank->sourceID() << endmsg;
        }
      }
    };

    if ( ::UTDAQ::version{ bank->version() } == ::UTDAQ::version::v5 ) {
      decode( UTNZSDecoder<::UTDAQ::version::v5>{ *bank } );
      ++m_validHeaders;
    } else {
      debug() << "Wrong version of the RawBank" << endmsg;
      ++m_invalidBanks;
    }
  }
  return digits;
}

//=============================================================================
