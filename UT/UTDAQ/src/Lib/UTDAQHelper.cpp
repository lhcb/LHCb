/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTDAQ/UTDAQHelper.h"

#include "Event/RawBank.h"
#include "Kernel/UTDAQDefinitions.h"
#include "SiDAQ/SiHeaderWord.h"
#include "UTDAQ/UTHeaderWord.h"

#include <cmath>
#include <limits>

namespace LHCb::UTDAQ {

  std::optional<unsigned int> nbUTClusters( RawBank::View banks, unsigned int maxNbClusters ) {
    size_t nbClusters = 0;
    for ( const auto& bank : banks ) {
      if ( bank->size() == 0 ) continue;
      switch ( ::UTDAQ::version{ bank->version() } ) {
      case ::UTDAQ::version::v4:
        nbClusters += SiHeaderWord( bank->data()[0] ).nClusters();
        break;
      case ::UTDAQ::version::v5: {
        auto headL = UTHeaderWord( bank->data()[0] );
        auto headR = UTHeaderWord( bank->data()[1] );
        nbClusters += headR.nClustersLane0() + headR.nClustersLane1() + headR.nClustersLane2() +
                      headR.nClustersLane3() + headL.nClustersLane4() + headL.nClustersLane5();
        break;
      }
      default:
        throw std::runtime_error{ "unknown version of the RawBank" }; /* OOPS: unknown format */
      }
      // cut as soon as we have too many
      if ( nbClusters > maxNbClusters ) { return {}; }
    }
    return nbClusters;
  }

  unsigned int boardIDfromSourceID( unsigned int ut_sourceID ) {
    // FIXME the ( ut_sourceID >> 11 ) == 0 case is needed to support old MC. It should be removed at some point.
    unsigned int ut_boardID = ( ut_sourceID >> 11 ) != 0
                                  ? ( ( ( ut_sourceID & 0xFF ) - 1 ) * 2 + ( ( ut_sourceID & 0x400 ) >> 10 ) )
                                  : ut_sourceID;
    return ut_boardID;
  }

  unsigned int sourceIDfromBoardID( unsigned int ut_boardID ) {
    // ut_boardID range from 0 to 215, even number for flow 0 and odd number for flow 1
    // FIXME: partition is UTA=5 and UTC=6;
    unsigned int partition    = 5; // UTA=5, UTC=6
    auto         flow         = ut_boardID % 2;
    auto         tell40_board = ut_boardID / 2 + 1; // one board has two flows
    unsigned int ut_sourceID  = tell40_board + ( flow << 10 ) + ( partition << 11 );
    // std::cout << "boardID=" << ut_boardID << " tell40_board=" << tell40_board << " flow=" << flow << " partition=" <<
    // partition << " sourceID=" << ut_sourceID << std::endl;
    return ut_sourceID;
  }

  GeomCache::GeomCache( const DeUTDetector& utDet ) {

    for ( unsigned iLayer = 0; iLayer < static_cast<unsigned>( UTInfo::DetectorNumbers::HalfLayers ); ++iLayer ) {
      layers[iLayer] = utDet.getLayerGeom( iLayer );
    }

    sectorLUT.Station1 = populateSectorLUT<nDoubleColumnsUTa>( 0 );
    sectorLUT.Station2 = populateSectorLUT<nDoubleColumnsUTb>( 2 );

    if ( utDet.SectorsSwapped() ) {
      constexpr const int shift = static_cast<int>( UTInfo::SectorNumbers::EffectiveSectorsPerColumn );
      for ( int sect : m_sectorsToSwapStation1 )
        std::swap( sectorLUT.Station1[sect], sectorLUT.Station1[sect + shift] );
      for ( int sect : m_sectorsToSwapStation2 )
        std::swap( sectorLUT.Station2[sect], sectorLUT.Station2[sect + shift] );
    }
  }

  void GeomCache::findSectorsFullID( unsigned int layer, float x, float y, float xTol, float yTol,
                                     boost::container::small_vector_base<int>& sectors ) const {
    const LayerInfo& info   = layers[layer];
    auto             localX = x - info.dxDy * y;
    // deal with sector overlaps and geometry imprecision
    xTol += 1; // mm
    auto localXmin = localX - xTol;
    auto localXmax = localX + xTol;
    int  subcolmin = std::nearbyint( localXmin * info.invHalfSectorXSize - 0.5 ) + 2 * info.nColsPerSide;
    int  subcolmax = std::nearbyint( localXmax * info.invHalfSectorXSize - 0.5 ) + 2 * info.nColsPerSide;
    if ( subcolmax < 0 || subcolmin >= (int)( 4 * info.nColsPerSide ) ) {
      // out of acceptance, return empty result
      return;
    }
    // on the acceptance limit
    if ( subcolmax >= (int)( 4 * info.nColsPerSide ) ) subcolmax = (int)( 4 * info.nColsPerSide ) - 1;
    if ( subcolmin < 0 ) subcolmin = 0;
    // deal with sector shifts in tilted layers and overlaps in regular ones
    yTol += ( layer == 1 || layer == 2 ) ? 8 : 1; //  mm
    auto localYmin = y - yTol;
    auto localYmax = y + yTol;
    int  subrowmin = std::nearbyint( localYmin * info.invHalfSectorYSize - 0.5 ) + 2 * info.nRowsPerSide;
    int  subrowmax = std::nearbyint( localYmax * info.invHalfSectorYSize - 0.5 ) + 2 * info.nRowsPerSide;
    if ( subrowmax < 0 || subrowmin >= (int)( 4 * info.nRowsPerSide ) ) {
      // out of acceptance, return empty result
      return;
    }
    // on the acceptance limit
    if ( subrowmax >= (int)( 4 * info.nRowsPerSide ) ) subrowmax = (int)( 4 * info.nRowsPerSide ) - 1;
    if ( subrowmin < 0 ) subrowmin = 0;
    for ( int subcol = subcolmin; subcol <= subcolmax; subcol++ ) {
      for ( int subrow = subrowmin; subrow <= subrowmax; subrow++ ) {
        sectors.emplace_back( sectorFullID( layer, subcol, subrow ) );
      }
    }
  }
} // namespace LHCb::UTDAQ
