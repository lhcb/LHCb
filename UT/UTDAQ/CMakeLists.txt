###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
UT/UTDAQ
--------
#]=======================================================================]

gaudi_add_library(UTDAQLib
    SOURCES
        src/Lib/UTDAQHelper.cpp
    LINK
        PUBLIC
            Boost::container
            LHCb::DigiEvent
            LHCb::LHCbMathLib
            LHCb::UTDetLib
            LHCb::UTKernelLib
        PRIVATE
            LHCb::DAQEventLib
            LHCb::DetDescLib
            LHCb::SiDAQ
)

gaudi_add_module(UTDAQ
    SOURCES
        src/component/UTDigitsToRawBankAlg.cpp
        src/component/UTDigitsToUTTELL1Data.cpp
        src/component/UTFullDecoding.cpp
        src/component/UTPedestalDecoding.cpp
        src/component/UTRawBankMonitor.cpp
        src/component/UTReadoutTool.cpp
        src/component/UTRawBankToUTDigitsAlg.cpp
        src/component/UTRawBankToUTNZSDigitsAlg.cpp
    LINK
        LHCb::DAQEventLib
        LHCb::DAQUtilsLib
        LHCb::DetDescLib
        LHCb::DigiEvent
        LHCb::LHCbKernel
        LHCb::LHCbAlgsLib
        LHCb::RecEvent
        LHCb::UTDAQLib
        LHCb::UTDetLib
        LHCb::UTKernelLib
        LHCb::UTTELL1Event
)

gaudi_add_pytest(tests/pytest)

if(NOT USE_DD4HEP)
    set(tests_to_disable
        UTDAQ.pytest.test_ut_decoding_nzs
    )
    file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/disable_tests.cmake
        "set_tests_properties(${tests_to_disable} PROPERTIES DISABLED TRUE)"
    )
    set_property(DIRECTORY APPEND PROPERTY
        TEST_INCLUDE_FILES ${CMAKE_CURRENT_BINARY_DIR}/disable_tests.cmake)
endif()
