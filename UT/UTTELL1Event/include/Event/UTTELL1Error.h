/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/VectorMap.h"
#include <algorithm>
#include <bit>
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_UTTELL1Error = 9012;

  // Namespace for locations in TDS
  namespace UTTELL1ErrorLocation {
    inline const std::string UTError = "Raw/UT/Error";
  }

  /** @class UTTELL1Error UTTELL1Error.h
   *
   * Error info from one PP-FPGA for a TELL1 board
   *
   * @author Andy Beiter (based on code by Mathias Knecht)
   *
   */

  class UTTELL1Error final : public KeyedObject<int> {
    template <auto m>
    static constexpr unsigned int extract( unsigned int i ) {
      static_assert( std::is_enum_v<decltype( m )> );
      constexpr auto s = std::countr_zero( static_cast<unsigned int>( m ) );
      return ( i & static_cast<unsigned int>( m ) ) >> s;
    }

  public:
    /// typedef for KeyedContainer of UTTELL1Error
    using Container = KeyedContainer<UTTELL1Error, Containers::HashMap>;

    /// enumerate failure modes
    enum FailureMode {
      kNone            = 0,
      kCorruptedBank   = 1,
      kOptLinkDisabled = 2,
      kTlkLinkLoss     = 3,
      kOptLinkNoClock  = 4,
      kSyncRAMFull     = 5,
      kSyncEvtSize     = 6,
      kOptLinkNoEvent  = 7,
      kPseudoHeader    = 8,
      kWrongPCN        = 9
    };

    /// constructor fixed part of bank [5 words]
    UTTELL1Error( unsigned int word0, unsigned int word1, unsigned int word2, unsigned int word3, unsigned int word4 )
        : m_word0( word0 ), m_word1( word1 ), m_word2( word2 ), m_word3( word3 ), m_word4( word4 ) {}

    /// Default Constructor
    UTTELL1Error() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override;
    static const CLID& classID();

    /// conversion of string to enum for type FailureMode
    static LHCb::UTTELL1Error::FailureMode FailureModeToType( const std::string& aName );

    /// conversion to string for enum type FailureMode
    static const std::string& FailureModeToString( int aEnum );

    /// Add information on a link
    bool addLinkInfo( const unsigned int key, const LHCb::UTTELL1Error::FailureMode& mode );

    /// Extract the information associated with the specified key
    UTTELL1Error::FailureMode linkInfo( const unsigned int beetle, const unsigned int port,
                                        const unsigned int pcn ) const;

    /// link is bad for some reason
    bool badLink( const unsigned int beetle, const unsigned int port, const unsigned int pcn ) const;

    /// find pcn for value for this beetle
    unsigned int findPCN( const unsigned int beetle ) const;

    /// bank has optional error words
    bool hasErrorInfo() const { return ErrorBankLength() == 0x14; }

    /// bank has optional NSZ bank word
    [[nodiscard]] constexpr bool hasNZS() const { return ( BankList() & 0x8 ) != 0; }

    /// bank has optional ped bank word
    [[nodiscard]] constexpr bool hasPed() const { return ( BankList() & 0x10 ) != 0; }

    /// # words Tell1 ppx sent for this bank
    unsigned int nOptionalWords() const;

    /// # words Tell1 sent for this bank
    unsigned int sentWords() const;

    /// optional error words
    UTTELL1Error& setOptionalErrorWords( unsigned int w1, unsigned int w2, unsigned int w3, unsigned int w4,
                                         unsigned int w5 );

    /// majority vote on pcn
    unsigned int pcnVote() const;

    /// fraction of links that are OK
    double fractionOK( unsigned int pcn ) const;

    /// Print out of error information
    std::ostream& fillStream( std::ostream& s ) const override;

    friend std::ostream& operator<<( std::ostream& str, const UTTELL1Error& obj ) { return obj.fillStream( str ); }

    /// Retrieve const  Word 0 of Error block
    [[nodiscard]] constexpr unsigned int word0() const { return m_word0; }

    /// Retrieve Bunch counter
    [[nodiscard]] constexpr unsigned int bCnt() const { return extract<word0Masks::BCntMask>( m_word0 ); }

    /// Retrieve ID of the sub-detector
    [[nodiscard]] constexpr unsigned int DetectorID() const { return extract<word0Masks::DetectorIDMask>( m_word0 ); }

    /// Retrieve List of the Banks sent by the TELL1 (Internal use)
    [[nodiscard]] constexpr unsigned int BankList() const { return extract<word0Masks::BankListMask>( m_word0 ); }

    /// Retrieve For internal use
    [[nodiscard]] constexpr unsigned int EventInformation() const {
      return extract<word0Masks::EventInformationMask>( m_word0 );
    }

    /// Retrieve const  Word 1 of Error block
    [[nodiscard]] constexpr unsigned int word1() const { return m_word1; }

    /// Retrieve L0 Event ID
    [[nodiscard]] constexpr int L0EvtID() const { return (int)extract<word1Masks::L0EvtIDMask>( m_word1 ); }

    /// Retrieve const  Word 2 of Error block
    [[nodiscard]] constexpr unsigned int word2() const { return m_word2; }

    /// Retrieve ADC data section length
    [[nodiscard]] constexpr unsigned int adC1() const { return extract<word2Masks::ADC1Mask>( m_word2 ); }

    /// Retrieve Cluster data section length
    [[nodiscard]] constexpr unsigned int Clust1() const { return extract<word2Masks::Clust1Mask>( m_word2 ); }

    /// Retrieve const  Word 3 of Error block
    [[nodiscard]] constexpr unsigned int word3() const { return m_word3; }

    /// Retrieve Number of clusters
    [[nodiscard]] constexpr unsigned int NumClusters() const { return extract<word3Masks::NumClustersMask>( m_word3 ); }

    /// Retrieve PCN from Beetle 0
    [[nodiscard]] constexpr unsigned int pcn() const { return extract<word3Masks::PCNMask>( m_word3 ); }

    /// Retrieve Indicates what processing has been applied
    [[nodiscard]] constexpr unsigned int ProcessInfo() const { return extract<word3Masks::ProcessInfoMask>( m_word3 ); }

    /// Retrieve const  Word 4 of Error block
    [[nodiscard]] constexpr unsigned int word4() const { return m_word4; }

    /// Retrieve Empty value, should be 0x00
    [[nodiscard]] constexpr unsigned int EmptyVal0() const { return extract<word4Masks::EmptyVal0Mask>( m_word4 ); }

    /// Retrieve Empty value, should be 0x8E
    [[nodiscard]] constexpr unsigned int EmptyVal1() const { return extract<word4Masks::EmptyVal1Mask>( m_word4 ); }

    /// Retrieve Indicates if an error occured. Without error, value is 0x0, with an error value is 0x14
    [[nodiscard]] constexpr unsigned int ErrorBankLength() const {
      return extract<word4Masks::ErrorBankLengthMask>( m_word4 );
    }

    /// Retrieve const  Word 5 of Error block=Word 3 of Event Info block if Error Bank length is 0x14
    [[nodiscard]] constexpr unsigned int word5() const { return m_word5; }

    /// Retrieve Indicates that no data has been received, one bit per link
    [[nodiscard]] constexpr unsigned int OptLnkNoEvt() const { return extract<word5Masks::OptLnkNoEvtMask>( m_word5 ); }

    /// Retrieve Indicates if link is disabled by ECS, one bit per link
    [[nodiscard]] constexpr unsigned int OptLnkDisable() const {
      return extract<word5Masks::OptLnkDisableMask>( m_word5 );
    }

    /// Retrieve Set if more or less than 35 words are sent paer event (one bit per link)
    [[nodiscard]] constexpr unsigned int SyncEvtSizeError() const {
      return extract<word5Masks::SyncEvtSizeErrorMask>( m_word5 );
    }

    /// Retrieve Error on reception, bit set if the link is not plugged (one bit per link)
    [[nodiscard]] constexpr unsigned int tlkLnkLoss() const { return extract<word5Masks::TLKLnkLossMask>( m_word5 ); }

    /// Retrieve Overflow on input sync RAM (one bit per link)
    [[nodiscard]] constexpr unsigned int SyncRAMFull() const { return extract<word5Masks::SyncRAMFullMask>( m_word5 ); }

    /// Retrieve Address of the PP-FPGA (between 0 and 3)
    [[nodiscard]] constexpr unsigned int ChipAddr() const { return extract<word5Masks::ChipAddrMask>( m_word5 ); }

    /// Retrieve const  Word 6 of Error block=Word 4 of Event Info block if Error Bank length is 0x14
    [[nodiscard]] constexpr unsigned int word6() const { return m_word6; }

    /// Retrieve Indicates if the ADC value is between low and high thresholds, one per Analog Link
    [[nodiscard]] constexpr int HeaderPseudoError() const {
      return extract<word6Masks::HeaderPseudoErrorMask>( m_word6 );
    }

    /// Retrieve Indicates if no clock is received, one bit per link
    [[nodiscard]] constexpr unsigned int OptLnkNoClock() const {
      return extract<word6Masks::OptLnkNoClockMask>( m_word6 );
    }

    /// Retrieve Indocates if the PCN among the links are not equal
    [[nodiscard]] constexpr unsigned int pcnError() const { return extract<word6Masks::PCNErrorMask>( m_word6 ); }

    /// Retrieve Reserved bit for later use
    [[nodiscard]] constexpr unsigned int R2() const { return extract<word6Masks::R2Mask>( m_word6 ); }

    /// Retrieve const  Word 7 of Error block=Word 5 of Event Info block if Error Bank length is 0x14
    [[nodiscard]] constexpr unsigned int word7() const { return m_word7; }

    /// Retrieve PCN of the Beetle 0
    [[nodiscard]] constexpr unsigned int pcnBeetle0() const { return extract<word7Masks::PCNBeetle0Mask>( m_word7 ); }

    /// Retrieve PCN of the Beetle 1
    [[nodiscard]] constexpr unsigned int pcnBeetle1() const { return extract<word7Masks::PCNBeetle1Mask>( m_word7 ); }

    /// Retrieve PCN of the Beetle 2
    [[nodiscard]] constexpr unsigned int pcnBeetle2() const { return extract<word7Masks::PCNBeetle2Mask>( m_word7 ); }

    /// Retrieve PCN of the Beetle 3
    [[nodiscard]] constexpr unsigned int pcnBeetle3() const { return extract<word7Masks::PCNBeetle3Mask>( m_word7 ); }

    /// Retrieve const  Word 8 of Error block=Word 6 of Event Info block if Error Bank length is 0x14
    [[nodiscard]] constexpr unsigned int word8() const { return m_word8; }

    /// Retrieve PCN of the Beetle 4
    [[nodiscard]] constexpr unsigned int pcnBeetle4() const { return extract<word8Masks::PCNBeetle4Mask>( m_word8 ); }

    /// Retrieve PCN of the Beetle 5
    [[nodiscard]] constexpr unsigned int pcnBeetle5() const { return extract<word8Masks::PCNBeetle5Mask>( m_word8 ); }

    /// Retrieve Reserved bits for later use =0x8B8B
    [[nodiscard]] constexpr unsigned int R3() const { return extract<word8Masks::R3Mask>( m_word8 ); }

    /// Retrieve const  Word 9 of Error block=Word 7 of Event Info block if Error Bank length is 0x14
    [[nodiscard]] constexpr unsigned int word9() const { return m_word9; }

    /// Update  Word 9 of Error block=Word 7 of Event Info block if Error Bank length is 0x14
    constexpr UTTELL1Error& setWord9( unsigned int value ) {
      m_word9 = value;
      return *this;
    }

    /// Retrieve I Header of the Beetle 0
    [[nodiscard]] constexpr unsigned int iHeaderBeetle0() const {
      return extract<word9Masks::IHeaderBeetle0Mask>( m_word9 );
    }

    /// Retrieve I Header of the Beetle 1
    [[nodiscard]] constexpr unsigned int iHeaderBeetle1() const {
      return extract<word9Masks::IHeaderBeetle1Mask>( m_word9 );
    }

    /// Retrieve I Header of the Beetle 2
    [[nodiscard]] constexpr unsigned int iHeaderBeetle2() const {
      return extract<word9Masks::IHeaderBeetle2Mask>( m_word9 );
    }

    /// Retrieve I Header of the Beetle 3
    [[nodiscard]] constexpr unsigned int iHeaderBeetle3() const {
      return extract<word9Masks::IHeaderBeetle3Mask>( m_word9 );
    }

    /// Retrieve I Header of the Beetle 4
    [[nodiscard]] constexpr unsigned int iHeaderBeetle4() const {
      return extract<word9Masks::IHeaderBeetle4Mask>( m_word9 );
    }

    /// Retrieve I Header of the Beetle 5
    [[nodiscard]] constexpr unsigned int iHeaderBeetle5() const {
      return extract<word9Masks::IHeaderBeetle5Mask>( m_word9 );
    }

    /// Retrieve Reserved bits for later use
    [[nodiscard]] constexpr unsigned int R4() const { return extract<word9Masks::R4Mask>( m_word9 ); }

    /// Retrieve const  Word 10 of Error block
    [[nodiscard]] constexpr unsigned int word10() const { return m_word10; }

    /// Update  Word 10 of Error block
    constexpr UTTELL1Error& setWord10( unsigned int value ) {
      m_word10 = value;
      return *this;
    }

    /// Retrieve Empty value, should be 0x01
    [[nodiscard]] constexpr unsigned int EmptyVal2() const { return extract<word10Masks::EmptyVal2Mask>( m_word10 ); }

    /// Retrieve Empty value, should be 0x8E
    [[nodiscard]] constexpr unsigned int EmptyVal3() const { return extract<word10Masks::EmptyVal3Mask>( m_word10 ); }

    /// Retrieve Cluster data section length
    [[nodiscard]] constexpr unsigned int Clust2() const { return extract<word10Masks::Clust2Mask>( m_word10 ); }

    /// Retrieve const  Word 11 of Error block
    [[nodiscard]] constexpr unsigned int word11() const { return m_word11; }

    /// Update  Word 11 of Error block
    constexpr UTTELL1Error& setWord11( unsigned int value ) {
      m_word11 = value;
      return *this;
    }

    /// Retrieve Empty value, should be 0x02
    [[nodiscard]] constexpr unsigned int EmptyVal4() const { return extract<word11Masks::EmptyVal4Mask>( m_word11 ); }

    /// Retrieve Empty value, should be 0x8E
    [[nodiscard]] constexpr unsigned int EmptyVal5() const { return extract<word11Masks::EmptyVal5Mask>( m_word11 ); }

    /// Retrieve ADC data section length
    [[nodiscard]] constexpr unsigned int adC2() const { return extract<word11Masks::ADC2Mask>( m_word11 ); }

    /// Retrieve const  Word 12 of Error block
    [[nodiscard]] constexpr unsigned int word12() const { return m_word12; }

    /// Update  Word 12 of Error block
    constexpr UTTELL1Error& setWord12( unsigned int value ) {
      m_word12 = value;
      return *this;
    }

    /// Retrieve Empty value, should be 0x03
    [[nodiscard]] constexpr unsigned int EmptyVal6() const { return extract<word12Masks::EmptyVal6Mask>( m_word12 ); }

    /// Retrieve Empty value, should be 0x8E
    [[nodiscard]] constexpr unsigned int EmptyVal7() const { return extract<word12Masks::EmptyVal7Mask>( m_word12 ); }

    /// Retrieve NZS bank length
    [[nodiscard]] constexpr unsigned int nzsBankLength() const {
      return extract<word12Masks::NZSBankLengthMask>( m_word12 );
    }

    /// Retrieve const  Word 13 of Error block
    [[nodiscard]] constexpr unsigned int word13() const { return m_word13; }

    /// Update  Word 13 of Error block
    constexpr UTTELL1Error& setWord13( unsigned int value ) {
      m_word13 = value;
      return *this;
    }

    /// Retrieve Empty value, should be 0x04
    [[nodiscard]] constexpr unsigned int EmptyVal8() const { return extract<word13Masks::EmptyVal8Mask>( m_word13 ); }

    /// Retrieve Empty value, should be 0x8E
    [[nodiscard]] constexpr unsigned int EmptyVal9() const { return extract<word13Masks::EmptyVal9Mask>( m_word13 ); }

    /// Retrieve Pedestal bank length
    [[nodiscard]] constexpr unsigned int PedBankLength() const {
      return extract<word13Masks::PedBankLengthMask>( m_word13 );
    }

  private:
    /// Bitmasks for bitfield word0
    enum class word0Masks : unsigned int {
      BCntMask             = 0xfffL,
      DetectorIDMask       = 0xf000L,
      BankListMask         = 0xff0000L,
      EventInformationMask = 0xff000000L
    };

    /// Bitmasks for bitfield word1
    enum class word1Masks : unsigned int { L0EvtIDMask = 0xffffffffL };

    /// Bitmasks for bitfield word2
    enum class word2Masks : unsigned int { ADC1Mask = 0xffffL, Clust1Mask = 0xffff0000L };

    /// Bitmasks for bitfield word3
    enum class word3Masks : unsigned int {
      NumClustersMask = 0xffffL,
      PCNMask         = 0xff0000L,
      ProcessInfoMask = 0xff000000L
    };

    /// Bitmasks for bitfield word4
    enum class word4Masks : unsigned int {
      EmptyVal0Mask       = 0xffL,
      EmptyVal1Mask       = 0xff00L,
      ErrorBankLengthMask = 0xffff0000L
    };

    /// Bitmasks for bitfield word5
    enum class word5Masks : unsigned int {
      OptLnkNoEvtMask      = 0x3fL,
      OptLnkDisableMask    = 0xfc0L,
      SyncEvtSizeErrorMask = 0x3f000L,
      TLKLnkLossMask       = 0xfc0000L,
      SyncRAMFullMask      = 0x3f000000L,
      ChipAddrMask         = 0xc0000000L
    };

    /// Bitmasks for bitfield word6
    enum class word6Masks : unsigned int {
      HeaderPseudoErrorMask = 0xffffffL,
      OptLnkNoClockMask     = 0x3f000000L,
      PCNErrorMask          = 0x40000000L,
      R2Mask                = 0x80000000L
    };

    /// Bitmasks for bitfield word7
    enum class word7Masks : unsigned int {
      PCNBeetle0Mask = 0xffL,
      PCNBeetle1Mask = 0xff00L,
      PCNBeetle2Mask = 0xff0000L,
      PCNBeetle3Mask = 0xff000000L
    };

    /// Bitmasks for bitfield word8
    enum class word8Masks : unsigned int { PCNBeetle4Mask = 0xffL, PCNBeetle5Mask = 0xff00L, R3Mask = 0xffff0000L };

    /// Bitmasks for bitfield word9
    enum class word9Masks : unsigned int {
      IHeaderBeetle0Mask = 0xfL,
      IHeaderBeetle1Mask = 0xf0L,
      IHeaderBeetle2Mask = 0xf00L,
      IHeaderBeetle3Mask = 0xf000L,
      IHeaderBeetle4Mask = 0xf0000L,
      IHeaderBeetle5Mask = 0xf00000L,
      R4Mask             = 0xff000000L
    };

    /// Bitmasks for bitfield word10
    enum class word10Masks : unsigned int { EmptyVal2Mask = 0xffL, EmptyVal3Mask = 0xff00L, Clust2Mask = 0xffff0000L };

    /// Bitmasks for bitfield word11
    enum class word11Masks : unsigned int { EmptyVal4Mask = 0xffL, EmptyVal5Mask = 0xff00L, ADC2Mask = 0xffff0000L };

    /// Bitmasks for bitfield word12
    enum class word12Masks : unsigned int {
      EmptyVal6Mask     = 0xffL,
      EmptyVal7Mask     = 0xff00L,
      NZSBankLengthMask = 0xffff0000L
    };

    /// Bitmasks for bitfield word13
    enum class word13Masks : unsigned int {
      EmptyVal8Mask     = 0xffL,
      EmptyVal9Mask     = 0xff00L,
      PedBankLengthMask = 0xffff0000L
    };

    unsigned int m_word0  = 0; ///< Word 0 of Error block
    unsigned int m_word1  = 0; ///< Word 1 of Error block
    unsigned int m_word2  = 0; ///< Word 2 of Error block
    unsigned int m_word3  = 0; ///< Word 3 of Error block
    unsigned int m_word4  = 0; ///< Word 4 of Error block
    unsigned int m_word5  = 0; ///< Word 5 of Error block=Word 3 of Event Info block if Error Bank length is 0x14
    unsigned int m_word6  = 0; ///< Word 6 of Error block=Word 4 of Event Info block if Error Bank length is 0x14
    unsigned int m_word7  = 0; ///< Word 7 of Error block=Word 5 of Event Info block if Error Bank length is 0x14
    unsigned int m_word8  = 0; ///< Word 8 of Error block=Word 6 of Event Info block if Error Bank length is 0x14
    unsigned int m_word9  = 0; ///< Word 9 of Error block=Word 7 of Event Info block if Error Bank length is 0x14
    unsigned int m_word10 = 0; ///< Word 10 of Error block
    unsigned int m_word11 = 0; ///< Word 11 of Error block
    unsigned int m_word12 = 0; ///< Word 12 of Error block
    unsigned int m_word13 = 0; ///< Word 13 of Error block

  private:
    /// Vector of arx [ each 32 channels] with failure
    using FailureInfo = GaudiUtils::VectorMap<unsigned int, LHCb::UTTELL1Error::FailureMode>;

    /// few useful constants
    enum Tell1Const : unsigned int { nPort = 4, nBeetle = 6, totalNumberOfPorts = nPort * nBeetle, nMagic = 0x8E };

    /// link identifier 0 - 23
    [[nodiscard]] constexpr unsigned int linkID( const unsigned int beetle, const unsigned int port ) const {
      return ( 4 * beetle + port );
    }

    /// flag bad links
    UTTELL1Error& flagBadLinks( unsigned int link, const LHCb::UTTELL1Error::FailureMode& mode ) {
      const unsigned int start = linkID( link, 0u );
      for ( unsigned int i = start; i < start + 4; ++i ) { addLinkInfo( i, mode ); }
      return *this;
    }

    /// fill error information map
    void fillErrorInfo();

    /// checks the magic patterns in error bank
    bool correctPatterns() const;

    FailureInfo m_badLinks; ///< list of links with failures

    static const GaudiUtils::VectorMap<std::string, FailureMode>& s_FailureModeTypMap();

  }; // class UTTELL1Error

  /// Definition of Keyed Container for UTTELL1Error
  typedef KeyedContainer<UTTELL1Error, Containers::HashMap> UTTELL1Errors;

  inline std::ostream& operator<<( std::ostream& s, LHCb::UTTELL1Error::FailureMode e ) {
    switch ( e ) {
    case LHCb::UTTELL1Error::kNone:
      return s << "kNone";
    case LHCb::UTTELL1Error::kCorruptedBank:
      return s << "kCorruptedBank";
    case LHCb::UTTELL1Error::kOptLinkDisabled:
      return s << "kOptLinkDisabled";
    case LHCb::UTTELL1Error::kTlkLinkLoss:
      return s << "kTlkLinkLoss";
    case LHCb::UTTELL1Error::kOptLinkNoClock:
      return s << "kOptLinkNoClock";
    case LHCb::UTTELL1Error::kSyncRAMFull:
      return s << "kSyncRAMFull";
    case LHCb::UTTELL1Error::kSyncEvtSize:
      return s << "kSyncEvtSize";
    case LHCb::UTTELL1Error::kOptLinkNoEvent:
      return s << "kOptLinkNoEvent";
    case LHCb::UTTELL1Error::kPseudoHeader:
      return s << "kPseudoHeader";
    case LHCb::UTTELL1Error::kWrongPCN:
      return s << "kWrongPCN";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::UTTELL1Error::FailureMode";
    }
  }

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline const CLID& LHCb::UTTELL1Error::clID() const { return LHCb::UTTELL1Error::classID(); }

inline const CLID& LHCb::UTTELL1Error::classID() { return CLID_UTTELL1Error; }

inline const GaudiUtils::VectorMap<std::string, LHCb::UTTELL1Error::FailureMode>&
LHCb::UTTELL1Error::s_FailureModeTypMap() {
  static const GaudiUtils::VectorMap<std::string, FailureMode> m = { { "kNone", kNone },
                                                                     { "kCorruptedBank", kCorruptedBank },
                                                                     { "kOptLinkDisabled", kOptLinkDisabled },
                                                                     { "kTlkLinkLoss", kTlkLinkLoss },
                                                                     { "kOptLinkNoClock", kOptLinkNoClock },
                                                                     { "kSyncRAMFull", kSyncRAMFull },
                                                                     { "kSyncEvtSize", kSyncEvtSize },
                                                                     { "kOptLinkNoEvent", kOptLinkNoEvent },
                                                                     { "kPseudoHeader", kPseudoHeader },
                                                                     { "kWrongPCN", kWrongPCN } };
  return m;
}

inline LHCb::UTTELL1Error::FailureMode LHCb::UTTELL1Error::FailureModeToType( const std::string& aName ) {
  auto iter = s_FailureModeTypMap().find( aName );
  return iter != s_FailureModeTypMap().end() ? iter->second : kNone;
}

inline const std::string& LHCb::UTTELL1Error::FailureModeToString( int aEnum ) {
  static const std::string s_kNone = "kNone";
  auto                     iter    = std::find_if( s_FailureModeTypMap().begin(), s_FailureModeTypMap().end(),
                                                   [&]( const std::pair<const std::string, FailureMode>& i ) { return i.second == aEnum; } );
  return iter != s_FailureModeTypMap().end() ? iter->first : s_kNone;
}

inline unsigned int LHCb::UTTELL1Error::nOptionalWords() const {
  unsigned int wCount = 0;
  if ( hasErrorInfo() ) wCount += 5;
  if ( hasPed() ) ++wCount;
  if ( hasNZS() ) ++wCount;
  return wCount;
}

inline unsigned int LHCb::UTTELL1Error::sentWords() const {
  unsigned int wCount = 7 + nOptionalWords();
  return wCount;
}

inline LHCb::UTTELL1Error& LHCb::UTTELL1Error::setOptionalErrorWords( unsigned int w1, unsigned int w2, unsigned int w3,
                                                                      unsigned int w4, unsigned int w5 ) {
  m_word5 = w1;
  m_word6 = w2;
  m_word7 = w3;
  m_word8 = w4;
  m_word9 = w5;
  fillErrorInfo(); // fill the map
  return *this;
}
