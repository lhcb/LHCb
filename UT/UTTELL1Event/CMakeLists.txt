###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
UT/UTTELL1Event
---------------
#]=======================================================================]

gaudi_add_library(UTTELL1Event
    SOURCES
        src/UTTELL1BoardErrorBank.cpp
        src/UTTELL1Data.cpp
        src/UTTELL1Error.cpp
        src/UTTELL1EventInfo.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
        PRIVATE
            LHCb::UTKernelLib
)

gaudi_add_dictionary(UTTELL1EventDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK LHCb::UTTELL1Event
)
