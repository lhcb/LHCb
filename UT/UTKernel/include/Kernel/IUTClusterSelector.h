/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "UTDet/DeUTDetector.h"

#include "GaudiKernel/IAlgTool.h"
#include <functional>

// forward declaration
namespace LHCb {
  class UTCluster;
}

/**
 *  An abstract interface for cluster "Selector"/"Preselector"
 *  tools, which allow a fast and efficient selection/preselection
 *  of clusters
 *
 *  @author A Beiter
 *  @date   2018-09-04
 */
struct IUTClusterSelector : extend_interfaces<IAlgTool> {

  /** interface identification
   *  @see IInterface
   *  @see IID_IUTClusterSelector
   *  @return the unique interface identifier
   */
  DeclareInterfaceID( IUTClusterSelector, 2, 0 );

  /** "select"/"preselect" method
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  virtual bool select( LHCb::UTCluster const& cluster, DeUTDetector const& ) const = 0;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  virtual bool operator()( LHCb::UTCluster const& cluster, DeUTDetector const& ) const = 0;
};
