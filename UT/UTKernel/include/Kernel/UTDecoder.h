/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class
 *  Implementation of UTRawBank decoder
 *
 *  @author Xuhao Yuan
 *  @author Wojciech Krupa (based on code by Xuhao Yuan, wokrupa@cern.ch)
 *  @date   @date   2020-06-17 / 2023-06-08 / 2024-04-10
 */

#pragma once
#include "Event/RawBank.h"
#include "Kernel/STLExtensions.h"
#include "SiDAQ/SiRawBankDecoder.h"
#include "SiDAQ/SiRawBufferWord.h"
#include "UTClusterWord.h"
#include "UTDAQ/UTADCWord.h"
#include "UTDAQ/UTHeaderWord.h"
#include "UTDAQDefinitions.h"
#include <cassert>
#include <cmath>
#include <map>
#include <stdexcept>

template <UTDAQ::version>
class UTDecoder;

template <>
class UTDecoder<UTDAQ::version::v4> : public SiRawBankDecoder<UTClusterWord> {
public:
  explicit UTDecoder( const LHCb::RawBank& bank ) : SiRawBankDecoder<UTClusterWord>{ bank.data() } {
    assert( UTDAQ::version{ bank.version() } == UTDAQ::version::v4 );
  }
};

template <>
class UTDecoder<UTDAQ::version::v5> final {
  enum class banksize { left_bit = 10, center_bit = 16, right_bit = 15 };

  // Method which allow moving within lane by jump betweens 256b TELL40 lines. It returns 1, 16 (jump to next line), 17,
  // 32, ... for N (numb of hits) = 1, 2, 3, ...
  constexpr static unsigned int nskip( unsigned int digLane ) {
    return static_cast<unsigned int>( banksize::center_bit ) * ( ( digLane + 1 ) / 2 - 1 ) + ( 1 - ( digLane % 2 ) );
  }

  // Method for reading number of hits in each line for 64b (2x32b) event header.
  constexpr static UTDAQ::digiVec make_digiVec( LHCb::span<const uint32_t, 2> header ) {
    UTHeaderWord headerL{ header[0] }, headerR{ header[1] };
    return { headerR.nClustersLane0(), headerR.nClustersLane1(), headerR.nClustersLane2(),
             headerR.nClustersLane3(), headerL.nClustersLane4(), headerL.nClustersLane5() };
  }
  constexpr static UTDAQ::headerVec make_headerVec( LHCb::span<const uint32_t, 2> header ) {
    UTHeaderWord headerL{ header[0] }, headerR{ header[1] };
    return { headerL.getFlags(), headerL.getBXID() };
  }

public:
  enum class PositionMethod { MaxAdc = 0, AdcWeighting, GeoWeighting };
  explicit UTDecoder( const LHCb::RawBank& bank )
      : UTDecoder{ bank.range<uint16_t>().subspan( 4 ), bank.range<uint32_t>().first<2>() } {
    assert( UTDAQ::version{ bank.version() } == UTDAQ::version::v5 );
    // bank.size() / sizeof( unsigned int ) > 32 prevent throwing ex. when UTError with minimal size (less then 128b)
    if ( bank.size() / sizeof( unsigned int ) > 32 &&
         bank.size() / sizeof( unsigned int ) != ( ( nClusters() + 1 ) / 2 ) * 8 && nClusters() != 0 ) {
      // TODO: add a dedicated UT DAQ StatusCode category, and throw a GaudiException with a value inside that category
      std::string str_msg = "Error: unexpected UT RawBank size, expected: ";
      str_msg.append( std::to_string( ( ( nClusters() + 1 ) / 2 ) * 8 ) );
      str_msg.append( " received: " + std::to_string( bank.size() / sizeof( unsigned int ) ) );
      const char* cc_msge = str_msg.c_str();
      throw std::runtime_error{ cc_msge };
    }
  }

  UTDecoder( LHCb::span<const uint16_t> body, LHCb::span<const uint32_t, 2> header )
      : m_bank( body ), m_nDigits{ make_digiVec( header ) }, m_header{ make_headerVec( header ) } {}

  class pos_range final { // NON clustering iterator.

  private:
    const LHCb::span<const uint16_t> m_bank;  // Data Bank 16b in lane (max 2 hits)
    UTDAQ::digiVec                   m_Digit; // Number of hits in each lane
    struct Sentinel                  final {};

    class Iterator final {
      const LHCb::span<const uint16_t> m_bank;
      UTDAQ::digiVec                   m_iterDigit;
      unsigned int                     m_lane   = UTDAQ::max_nlane;
      unsigned int                     m_pos    = 0; // Current position in lane
      unsigned int                     m_maxpos = 0; // Max position in lane (determined by number of hits in lane)

    public:
      Iterator( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec m_Digit )
          : m_bank{ bank }, m_iterDigit{ m_Digit } {
        auto PosLane = std::find_if( m_iterDigit.begin(), m_iterDigit.end(),
                                     []( unsigned int& element ) { return element != 0; } );
        if ( PosLane != m_iterDigit.end() ) {
          m_lane   = std::distance( m_iterDigit.begin(), PosLane );                // first non-zero lane
          m_pos    = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane; // initial pos (10, 8, 6, 4, 2, 0)
          m_maxpos = m_pos + nskip( m_iterDigit[m_lane] ); // max posiotion (initial + distance distance depends on
        }                                                  // number of hits )
      }

      // dereferencing
      [[nodiscard]] constexpr UTADCWord operator*() const { return UTADCWord{ m_bank[m_pos], m_lane }; }

      constexpr Iterator& operator++() {
        m_pos += ( ( m_pos % 2 ) ? static_cast<unsigned int>( banksize::right_bit )
                                 : 1u ); // if we read odd word we moving to the second in the line (1), otherwise jump
        if ( m_pos > m_maxpos ) {        // to next line
          ++m_lane;                      // if we reach last hit, we jump to the next line
          while ( m_lane < UTDAQ::max_nlane && m_iterDigit[m_lane] == 0 ) {
            ++m_lane;
          }                                  // it there is no hits in lane, we jump to next one
          if ( m_lane < UTDAQ::max_nlane ) { // if we didn't reach the end of lanes
            m_pos = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane; // changing initial and max position
            m_maxpos = m_pos + nskip( m_iterDigit[m_lane] );
          }
        }
        return *this;
      }

      constexpr bool operator!=( Sentinel ) const { return m_lane < UTDAQ::max_nlane; }
    };

  public:
    constexpr pos_range( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec& ClusterVec )
        : m_bank{ std::move( bank ) }, m_Digit{ ClusterVec } {}
    [[nodiscard]] auto           begin() const { return Iterator{ m_bank, m_Digit }; }
    [[nodiscard]] constexpr auto end() const { return Sentinel{}; }
  };

  class posadc_range final { // Clustering iterator.

  private:
    const LHCb::span<const uint16_t> m_bank;
    UTDAQ::digiVec                   m_Digit;
    struct Sentinel                  final {};
    PositionMethod                   m_positionMethod;
    unsigned int                     m_stripMax; // strip with highest number of ADC

    class Iterator final {
      const LHCb::span<const uint16_t> m_bank;
      UTDAQ::digiVec                   m_iterDigit;
      unsigned int                     m_lane          = UTDAQ::max_nlane;
      unsigned int                     m_pos           = 0;
      unsigned int                     m_maxpos        = 0;
      unsigned int                     m_pos_nextBegin = 0;
      float                            m_fracStrip     = 0.;
      PositionMethod                   m_positionMethod;
      unsigned int                     m_stripMax;
      unsigned int                     m_clusterSize   = 0;
      unsigned int                     m_clusterCharge = 0;
      enum class adcbits { adc = 0, strip = 5 };
      enum class adcmask { adc = 0x1F, strip = 0XFFE0 };
      constexpr unsigned int adcValue( unsigned int pos ) const {
        return ( m_bank[pos] & static_cast<unsigned int>( adcmask::adc ) ) >> static_cast<unsigned int>( adcbits::adc );
      }
      constexpr unsigned int strip( unsigned int pos ) const {
        return ( m_bank[pos] & static_cast<unsigned int>( adcmask::strip ) ) >>
               static_cast<unsigned int>( adcbits::strip );
      }
      constexpr static unsigned int nextPos( unsigned int pos ) {
        return pos + ( ( pos % 2 ) ? static_cast<unsigned int>( banksize::right_bit ) : 1u );
      }
      bool keepClustering() {
        unsigned int Optpos = m_pos, maxAdc = adcValue( m_pos ), cstrip = strip( m_pos );
        unsigned int adccount( maxAdc ), numstrip( 1u );
        unsigned int Optpos_cal = ( m_positionMethod == PositionMethod::GeoWeighting ? cstrip : ( cstrip * adccount ) );
        std::map<unsigned int, unsigned int> posvector;
        posvector.emplace( cstrip, m_pos );
        while ( true ) {
          m_pos = nextPos( m_pos );
          if ( m_pos > m_maxpos ) break; // finish if end of hits in lane
          auto stripPrev = std::exchange( cstrip, strip( m_pos ) );
          if ( stripPrev + 1 != cstrip ) break; // check if strips are adjcent
          numstrip++;                           // counting number of strips in cluster
          unsigned int adcvalue = adcValue( m_pos );
          if ( adcvalue > maxAdc ) { // check if maximal
            maxAdc = adcvalue;
            Optpos = m_pos;
          }
          if ( m_positionMethod != PositionMethod::MaxAdc ) posvector.emplace( cstrip, m_pos );
          Optpos_cal += ( m_positionMethod == PositionMethod::AdcWeighting ? cstrip * adcvalue : cstrip );
          adccount += adcvalue;
        }
        if ( numstrip <= m_stripMax && adccount > 0 ) {
          m_fracStrip = 0; // strip center
          if ( numstrip > 1 ) {
            if ( m_positionMethod >= PositionMethod::GeoWeighting ) {
              unsigned int istrip = ( Optpos_cal * 2 + numstrip ) / 2 / numstrip; // get weighted strip position
              m_fracStrip         = static_cast<float>( Optpos_cal ) / static_cast<float>( numstrip ) - istrip;
              auto iter           = posvector.find( istrip );
              if ( iter != posvector.end() ) Optpos = iter->second;
            } else if ( m_positionMethod == PositionMethod::AdcWeighting ) {
              unsigned int istrip = ( Optpos_cal * 2 + adccount ) / 2 / adccount; // get weighted strip position
              m_fracStrip         = static_cast<float>( Optpos_cal ) / static_cast<float>( adccount ) - istrip;
              auto iter           = posvector.find( istrip );
              if ( iter != posvector.end() ) Optpos = iter->second;
            }
          }
          m_pos_nextBegin = std::exchange( m_pos, Optpos ); // if size of the cluster is below maximal value -> accept
          m_clusterSize   = numstrip;
          m_clusterCharge = adccount;
          return true;
        } else {
          m_pos_nextBegin = m_pos;
          return false; // can't make the cluster, even the bank is not empty.
        }
      }

    public:
      Iterator( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec m_Digit, PositionMethod positionMethod,
                const unsigned int stripMax )
          : m_bank{ bank }, m_iterDigit{ m_Digit }, m_positionMethod{ positionMethod }, m_stripMax{ stripMax } {
        auto PosLane =
            std::find_if( m_iterDigit.begin(), m_iterDigit.end(), []( unsigned int element ) { return element != 0; } );
        if ( PosLane != m_iterDigit.end() ) {
          m_lane            = std::distance( m_iterDigit.begin(), PosLane ); // first non-zero lane
          m_pos             = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
          m_maxpos          = m_pos + nskip( m_iterDigit[m_lane] );
          bool clusterFound = false;
          while ( !clusterFound ) {
            if ( m_pos <= m_maxpos ) { clusterFound = keepClustering(); }
            if ( m_pos > m_maxpos ) {
              ++m_lane;
              while ( m_lane < UTDAQ::max_nlane && m_iterDigit[m_lane] == 0 ) ++m_lane;
              if ( m_lane < UTDAQ::max_nlane ) {
                m_pos_nextBegin = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
                m_maxpos        = m_pos_nextBegin + nskip( m_iterDigit[m_lane] );
                m_pos           = m_pos_nextBegin;
                clusterFound    = keepClustering();
              } else {
                break;
              }
            }
          }
        }
      }

      // dereferencing
      [[nodiscard]] constexpr UTADCWord operator*() const {
        return { m_bank[m_pos], m_lane, m_fracStrip, m_clusterCharge, m_clusterSize };
      }

      Iterator& operator++() {
        bool clusterFound = false;
        while ( !clusterFound ) {
          m_pos = m_pos_nextBegin;
          // if m_pos is still in range there could be more hits so we call keepClustering
          if ( m_pos <= m_maxpos ) { clusterFound = keepClustering(); }
          // if keepClustering found a cluster m_pos will point to the maxiumum
          // adc value now and will still be smaller than m_maxpos. if it is
          // greater that means there wasn't a new cluster found and we are ready
          // to jump to the next lane
          if ( m_pos > m_maxpos ) {
            ++m_lane;
            while ( m_lane < UTDAQ::max_nlane && m_iterDigit[m_lane] == 0 ) ++m_lane;
            if ( m_lane < UTDAQ::max_nlane ) {
              m_pos_nextBegin = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
              m_maxpos        = m_pos_nextBegin + nskip( m_iterDigit[m_lane] );
              m_pos           = m_pos_nextBegin;
              clusterFound    = keepClustering();
            } else {
              break;
            }
          }
        }
        return *this;
      }

      [[nodiscard]] constexpr bool operator!=( Sentinel ) const { return m_lane < UTDAQ::max_nlane; }
    };

  public:
    constexpr posadc_range( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec& ClusterVec,
                            PositionMethod positionMethod, unsigned int stripMax )
        : m_bank{ std::move( bank ) }
        , m_Digit{ ClusterVec }
        , m_positionMethod{ positionMethod }
        , m_stripMax{ stripMax } {}
    [[nodiscard]] auto           begin() const { return Iterator{ m_bank, m_Digit, m_positionMethod, m_stripMax }; }
    [[nodiscard]] constexpr auto end() const { return Sentinel{}; }
  };
  [[nodiscard]] unsigned int word_size() const {
    constexpr unsigned int row_size    = 16;
    constexpr unsigned int header_size = 4;
    unsigned int n_rows_longest_lane   = ( nClusters() + 1 ) / 2; // nClusters() indicate number of hits in longest lane
                                                                  // Each row can contain 2 hits,
    //  pad with 1 to account for last row containing 1 hit
    unsigned int size_of_raw_bank =
        std::max( n_rows_longest_lane * row_size, header_size ); // For 0 hit, there is always a size 4 header
    unsigned int ut_hits_word_size = size_of_raw_bank - header_size;
    return ut_hits_word_size;
  }

  [[nodiscard]] constexpr unsigned int nClusters() const {
    static_assert( UTDAQ::max_nlane == 6 );
    return std::max( { m_nDigits[0], m_nDigits[1], m_nDigits[2], m_nDigits[3], m_nDigits[4], m_nDigits[5] } );
  }
  [[nodiscard]] constexpr unsigned int nClusters( unsigned int laneID ) const { return m_nDigits[laneID]; }
  [[nodiscard]] constexpr unsigned int getflags() const { return m_header[0]; }
  [[nodiscard]] constexpr unsigned int getBXID() const { return m_header[1]; }
  [[nodiscard]] auto                   posRange() const {
    //  last ADC word in UTTell40 ZS RawBank, it is organized in 16 words long rows (2 ADC words per LANE). First 4
    //  words in the row are header, thus word_size() is protection against nCluster() = 0 and m_bank.first( - 4 )
    return pos_range{ m_bank.first( word_size() ), m_nDigits };
  }
  [[nodiscard]] auto posAdcRange( PositionMethod positionMethod = PositionMethod::GeoWeighting,
                                  unsigned int   stripMax       = 252 ) const {
    return posadc_range{ m_bank.first( word_size() ), m_nDigits, positionMethod, stripMax };
  }

  friend std::string toString( PositionMethod e ) {
    switch ( e ) {
    case PositionMethod::MaxAdc:
      return "MaxAdc";
    case PositionMethod::AdcWeighting:
      return "AdcWeighting";
    case PositionMethod::GeoWeighting:
      return "GeoWeighting";
    }
    throw std::invalid_argument( "toString( UTDecoder::PositionMethod ): invalid value for PositionMethod" );
  }
  friend std::ostream& toStream( PositionMethod e, std::ostream& os ) {
    return os << std::quoted( toString( e ), '\'' );
  }
  friend std::ostream& operator<<( std::ostream& s, PositionMethod e ) { return toStream( e, s ); }
  friend StatusCode    parse( PositionMethod& result, std::string_view input ) {
    if ( ( input.front() == '"' || input.back() == '\'' ) && input.front() == input.back() ) {
      input.remove_prefix( 1 );
      input.remove_suffix( 1 );
    }
    for ( PositionMethod m : { PositionMethod::MaxAdc, PositionMethod::AdcWeighting, PositionMethod::GeoWeighting } ) {
      if ( input == toString( m ) ) {
        result = m;
        return StatusCode::SUCCESS;
      }
    }

    return StatusCode::FAILURE;
  }

private:
  LHCb::span<const uint16_t> m_bank;
  UTDAQ::digiVec             m_nDigits;
  UTDAQ::headerVec           m_header;
};

template <>
class UTDecoder<UTDAQ::version::v2> final {
  using PositionMethod = UTDecoder<UTDAQ::version::v5>::PositionMethod;

public:
  explicit UTDecoder( const LHCb::RawBank& bank ) {
    assert( UTDAQ::version{ bank.version() } == UTDAQ::version::v2 );
    auto data = bank.range<uint32_t>();
    m_data.reserve( data.size() );
    std::transform( data.begin(), data.end(), std::back_inserter( m_data ),
                    []( uint32_t in ) { return __builtin_bswap32( in ); } );
    // get the body and the header words
    m_v5decoder.emplace(
        LHCb::span( reinterpret_cast<std::uint16_t const*>( &m_data.front() ), m_data.size() * 2 ).subspan( 4 ),
        LHCb::span( m_data ).first<2>() );
  }
  [[nodiscard]] constexpr unsigned int nClusters() const { return m_v5decoder->nClusters(); }
  [[nodiscard]] constexpr unsigned int nClusters( unsigned int laneID ) const {
    return m_v5decoder->nClusters( laneID );
  }
  [[nodiscard]] auto posRange() const { return m_v5decoder->posRange(); }
  [[nodiscard]] auto posAdcRange( PositionMethod positionMethod, unsigned int stripMax = 252 ) const {
    return m_v5decoder->posAdcRange( positionMethod, stripMax );
  }
  [[nodiscard]] constexpr unsigned int getflags() const { return m_v5decoder->getflags(); }
  [[nodiscard]] constexpr unsigned int getBXID() const { return m_v5decoder->getBXID(); }

private:
  std::optional<UTDecoder<UTDAQ::version::v5>> m_v5decoder; // optional to delay construction until we're ready for it
  std::vector<std::uint32_t>                   m_data;
};
