/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class
 *  Implementation of UTRawBank decoder
 *
 *  @author Wojciech Krupa (based on code by Xuhao Yuan, wokrupa@cern.ch)
 *  @date   2020-06-17 / 2023-06-08
 */

#pragma once
#include "Event/RawBank.h"
#include "Kernel/STLExtensions.h"
#include "SiDAQ/SiRawBankDecoder.h"
#include "SiDAQ/SiRawBufferWord.h"
#include "UTClusterWord.h"
#include "UTDAQ/UTHeaderWord.h"
#include "UTDAQ/UTNZSWord.h"
#include "UTDAQDefinitions.h"
#include <cassert>

template <UTDAQ::version>
class UTNZSDecoder;

template <>
class UTNZSDecoder<UTDAQ::version::v4> : public SiRawBankDecoder<UTClusterWord> {
public:
  explicit UTNZSDecoder( const LHCb::RawBank& bank ) : SiRawBankDecoder<UTClusterWord>{ bank.data() } {
    assert( UTDAQ::version{ bank.version() } == UTDAQ::version::v4 );
  }
};

template <>
class UTNZSDecoder<UTDAQ::version::v5> final {
  enum class banksize { left_bit = 10, center_bit = 16, right_bit = 15 };

  // Method which allow moving within lane by jump betweens 256b TELL40 lines. It returns 1, 16 (jump to next line), 17,
  // 32, ... for N (numb of hits) = 1, 2, 3, ...
  constexpr static unsigned int nskip( unsigned int digLane ) {
    return static_cast<unsigned int>( banksize::center_bit ) * ( ( digLane + 1 ) / 2 - 1 ) + ( 1 - ( digLane % 2 ) );
  }

  constexpr static unsigned int get_digits( unsigned int input ) {
    if ( input == 0x08 ) return 264;
    if ( input == 0xC6 ) return 198;
    if ( input == 0x84 ) return 132;
    if ( input == 0x42 )
      return 66;
    else
      return 0;
  }

  // Method for reading number of hits in each line for 64b (2x32b) event header.
  constexpr static UTDAQ::digiVec make_digiVec( LHCb::span<const uint32_t, 2> header ) {
    UTHeaderWord headerL{ header[0] }, headerR{ header[1] };
    return { get_digits( headerR.nClustersLane0() ), get_digits( headerR.nClustersLane1() ),
             get_digits( headerR.nClustersLane2() ), get_digits( headerR.nClustersLane3() ),
             get_digits( headerL.nClustersLane4() ), get_digits( headerL.nClustersLane5() ) };
  }

  constexpr static UTDAQ::headerVec make_headerVec( LHCb::span<const uint32_t, 2> header ) {
    UTHeaderWord headerL{ header[0] }, headerR{ header[1] };
    return { headerL.getFlags(), headerL.getBXID() };
  }

public:
  explicit UTNZSDecoder( const LHCb::RawBank& bank )
      : UTNZSDecoder{ bank.range<uint16_t>().subspan( 4 ), bank.range<uint32_t>().first<2>() } {
    assert( UTDAQ::version{ bank.version() } == UTDAQ::version::v5 );
    if ( bank.type() == LHCb::RawBank::UT &&
         ( bank.size() / sizeof( unsigned int ) != ( ( nClusters() + 1 ) / 2 ) * 8 &&
           nClusters() != 0 ) ) { // TODO: add a dedicated UT DAQ StatusCode category, and
                                  // throw a GaudiException with a value inside that
                                  // category
      std::string str_msg = "Error: unexpected UT RawBank size, expected: ";
      str_msg.append( std::to_string( ( ( nClusters() + 1 ) / 2 ) * 8 ) );
      str_msg.append( " received: " + std::to_string( bank.size() / sizeof( unsigned int ) ) );
      const char* cc_msge = str_msg.c_str();
      throw std::runtime_error{ cc_msge };
    }
  }

  UTNZSDecoder( LHCb::span<const uint16_t> body, LHCb::span<const uint32_t, 2> header )
      : m_bank( body ), m_nDigits{ make_digiVec( header ) }, m_header{ make_headerVec( header ) } {}

  class pos_range final { // NON clustering iterator.

  private:
    const LHCb::span<const uint16_t> m_bank;  // Data Bank 16b in lane (max 2 hits)
    UTDAQ::digiVec                   m_Digit; // Number of hits in each lane
    struct Sentinel                  final {};

    class Iterator final {
      const LHCb::span<const uint16_t> m_bank;
      UTDAQ::digiVec                   m_iterDigit;
      unsigned int                     m_lane   = UTDAQ::max_nlane;
      unsigned int                     m_pos    = 0; // Current position in lane
      unsigned int                     m_maxpos = 0; // Max position in lane (determined by number of hits in lane)
      unsigned int                     m_asic_num;   // ASIC_num + 8b of NZS header
      unsigned int                     m_nzs_header; // Remaining 16b of NZS header
      unsigned int                     m_inlane_counter = 0; //

    public:
      Iterator( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec m_Digit )
          : m_bank{ bank }, m_iterDigit{ m_Digit } {
        auto PosLane = std::find_if( m_iterDigit.begin(), m_iterDigit.end(),
                                     []( unsigned int& element ) { return element != 0; } );
        if ( PosLane != m_iterDigit.end() ) {
          m_lane   = std::distance( m_iterDigit.begin(), PosLane );                // first non-zero lane
          m_pos    = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane; // initial pos (10, 8, 6, 4, 2, 0)
          m_maxpos = m_pos + nskip( m_iterDigit[m_lane] ); // max posiotion (initial + distance distance depends on
        }                                                  // number of hits )
      }

      // dereferencing
      [[nodiscard]] constexpr UTNZSWord operator*() const {
        return UTNZSWord{ m_bank[m_pos], m_lane, m_asic_num, m_nzs_header, m_inlane_counter };
      }

      constexpr Iterator& operator++() {

        if ( m_inlane_counter % 66 == 0 ) m_nzs_header = m_bank[m_pos];
        if ( m_inlane_counter % 66 == 1 ) m_asic_num = m_bank[m_pos];
        m_inlane_counter++;
        m_pos += ( ( m_pos % 2 ) ? static_cast<unsigned int>( banksize::right_bit )
                                 : 1u ); // if we read odd word we moving to the second in the line (1), otherwise jump

        if ( m_pos > m_maxpos ) {
          m_inlane_counter = 0; // to next line
          ++m_lane;             // if we reach last hit, we jump to the next line
          while ( m_lane < UTDAQ::max_nlane && m_iterDigit[m_lane] == 0 ) {
            ++m_lane;
          }                                  // it there is no hits in lane, we jump to next one
          if ( m_lane < UTDAQ::max_nlane ) { // if we didn't reach the end of lanes
            m_pos = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane; // changing initial and max position
            m_maxpos = m_pos + nskip( m_iterDigit[m_lane] );
          }
        }

        return *this;
      }

      constexpr bool operator!=( Sentinel ) const { return m_lane < UTDAQ::max_nlane; }
    };

  public:
    constexpr pos_range( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec& ClusterVec )
        : m_bank{ std::move( bank ) }, m_Digit{ ClusterVec } {}
    [[nodiscard]] auto           begin() const { return Iterator{ m_bank, m_Digit }; }
    [[nodiscard]] constexpr auto end() const { return Sentinel{}; }
  };

  [[nodiscard]] constexpr unsigned int nClusters() const {
    static_assert( UTDAQ::max_nlane == 6 );
    return std::max( { m_nDigits[0], m_nDigits[1], m_nDigits[2], m_nDigits[3], m_nDigits[4], m_nDigits[5] } );
  }
  [[nodiscard]] constexpr unsigned int nClusters( unsigned int laneID ) const { return m_nDigits[laneID]; }
  [[nodiscard]] constexpr unsigned int getflags() const { return m_header[0]; }
  [[nodiscard]] constexpr unsigned int getBXID() const { return m_header[1]; }
  [[nodiscard]] auto                   posRange() const {
    return pos_range{ m_bank.first( ( ( nClusters() + 1 ) / 2 ) * 16 - 4 ), m_nDigits };
  }

private:
  LHCb::span<const uint16_t> m_bank;
  UTDAQ::digiVec             m_nDigits;
  UTDAQ::headerVec           m_header;
};
