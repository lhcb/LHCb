/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/UTDAQBoard.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"

using namespace LHCb;

UTDAQ::Board::Board( const UTDAQID::BoardID daqid ) : m_boardID( daqid ) {}

UTDAQID UTDAQ::Board::addSector( LHCb::Detector::UT::ChannelID sector, unsigned int orientation ) {
  UTDAQID::LaneID thislane = static_cast<UTDAQID::LaneID>( m_sectorsVector.size() );
  m_sectorsVector.push_back( sector );
  m_orientationVector.push_back( orientation );
  return UTDAQID( m_boardID.board(), thislane );
}
