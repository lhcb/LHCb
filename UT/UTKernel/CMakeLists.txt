###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
UT/UTKernel
-----------
#]=======================================================================]

gaudi_add_library(UTKernelLib
    SOURCES
	  src/UTIDMapping.cpp
        src/UTDAQBoard.cpp
        src/UTDetectorPlot.cpp
        src/UTFun.cpp
        src/UTTell1Board.cpp
        src/UTXMLUtils.cpp
    LINK
        PUBLIC
            Boost::container
            Boost::headers
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::DAQEventLib
            LHCb::DigiEvent
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::SiDAQ
            LHCb::UTDetLib
        PRIVATE
            Boost::date_time
)

gaudi_add_dictionary(UTKernelDict
    HEADERFILES dict/UTKernelDict.h
    SELECTION dict/UTKernelDict.xml
    LINK LHCb::UTKernelLib
)
