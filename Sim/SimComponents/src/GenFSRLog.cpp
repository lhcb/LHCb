/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/CrossSectionsFSR.h"
#include "Event/GenCountersFSR.h"
#include "Event/GenFSR.h"
#include "FSRAlgs/IFSRNavigator.h"
#include "Gaudi/Algorithm.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : GenFSRLog
//
// 2015-06-23 : Davide Fazzini
//-----------------------------------------------------------------------------

namespace {
  LHCb::CrossSectionsFSR::CrossSectionKey to_xkey( LHCb::GenCountersFSR::CounterKey key ) {
    return static_cast<LHCb::CrossSectionsFSR::CrossSectionKey>( key );
  }

} // namespace

/** @class GenFSRLog GenFSRLog.h
 *
 *
 *  @author Davide Fazzini
 *  @date   2015-06-23
 */

class GenFSRLog : public Gaudi::Algorithm {
public:
  // Standard constructor
  using Gaudi::Algorithm::Algorithm;

  StatusCode initialize() override;                         // Algorithm initialization
  StatusCode execute( const EventContext& ) const override; // Algorithm execution
  StatusCode finalize() override;                           // Algorithm finalization

private:
  void printFSR(); // Print the GenFSR in a file .xml

  Gaudi::Property<std::string> m_fileRecordName{ this, "FileRecordLocation", "/FileRecords",
                                                 "TES location where FSRs are persisted" };
  Gaudi::Property<std::string> m_FSRName{ this, "FSRName", "/GenFSR", "Name of the genFSR tree" };
  Gaudi::Property<std::string> m_xmlOutputLocation{ this, "XmlOutputLocation", "",
                                                    "Path where to save the .xml output" };
  Gaudi::Property<std::string> m_xmlOutputName{ this, "xmlOutputName", "GeneratorLogFSR.xml",
                                                "Name of the .xml output" };

  SmartIF<IDataProviderSvc>       m_fileRecordSvc;
  PublicToolHandle<IFSRNavigator> m_navigatorTool{ this, "FSRNavigator",
                                                   "FSRNavigator/FSRNavigator" }; // tool to navigate FSR
};

namespace {
  //=============================================================================
  //  Add generator level counters in the xml file
  //=============================================================================

  void addGenCounters( LHCb::GenFSR const& genFSR, std::ostream& xmlOutput ) {

    const auto& mapCounter = LHCb::GenCountersFSR::getFullNames();

    for ( auto const& counter : genFSR.genCounters() ) {
      auto key = counter.first;

      if ( key > LHCb::GenCountersFSR::CounterKey::bAndcAcc ) continue;
      if ( key == LHCb::GenCountersFSR::CounterKey::BeforeFullEvt ||
           key == LHCb::GenCountersFSR::CounterKey::AfterFullEvt )
        continue;

      longlong value = counter.second.second;

      xmlOutput << "  <counter name = \"" << mapCounter.at( key ) << "\">\n"
                << "    <value> " << value << " </value>\n"
                << "  </counter>\n";
    }
  }

  //=============================================================================
  //  Add generator level cut efficiencies in the xml file
  //=============================================================================

  void addCutEfficiencies( LHCb::GenFSR const& genFSR, std::ofstream& xmlOutput ) {
    const auto& mapCross = LHCb::GenCountersFSR::getFullNames();

    for ( auto const& counter : genFSR.genCounters() ) {
      auto key = counter.first;

      if ( key == LHCb::GenCountersFSR::CounterKey::EvtInverted ) {
        longlong value = counter.second.second;

        xmlOutput << "  <counter name = \"" << mapCross.at( key ) << "\">\n"
                  << "    <value> " << value << " </value>\n"
                  << "  </counter>\n";
      }

      if ( key != LHCb::GenCountersFSR::CounterKey::AfterFullEvt &&
           key != LHCb::GenCountersFSR::CounterKey::AfterLevelCut &&
           key != LHCb::GenCountersFSR::CounterKey::AfterPCut &&
           key != LHCb::GenCountersFSR::CounterKey::AfterantiPCut )
        continue;

      longlong before = genFSR.getDenominator( key );
      if ( before == 0 ) continue;

      longlong after    = counter.second.second;
      double   fraction = genFSR.getEfficiency( after, before );
      double   error    = genFSR.getEfficiencyError( after, before );

      xmlOutput << "  <efficiency name = \"" << mapCross.at( key ) << "\">\n"
                << "    <after> " << after << " </after>\n"
                << "    <before> " << before << " </before>\n"
                << "    <value> " << fraction << " </value>\n"
                << "    <error> " << error << " </error>\n"
                << "  </efficiency>\n";
    }
  }

  //=============================================================================
  //  Add generator level fractions in the xml file
  //=============================================================================

  void addGenFractions( LHCb::GenFSR const& genFSR, std::ostream& xmlOutput ) {
    const auto& mapCross = LHCb::GenCountersFSR::getFullNames();

    for ( auto const& counter : genFSR.genCounters() ) {
      auto key = counter.first;
      if ( key < LHCb::GenCountersFSR::CounterKey::B0Gen || key >= LHCb::GenCountersFSR::CounterKey::BeforePCut )
        continue;

      longlong before = genFSR.getDenominator( key );
      if ( before == 0 ) continue;

      longlong after    = counter.second.second;
      double   fraction = genFSR.getEfficiency( after, before );
      double   error    = genFSR.getEfficiencyError( after, before );

      xmlOutput << "  <fraction name = \"" << mapCross.at( key ) << "\">\n"
                << "    <number> " << after << " </number>\n"
                << "    <value> " << fraction << " </value>\n"
                << "    <error> " << error << " </error>\n"
                << "  </fraction>\n";
    }
  }

  //=============================================================================
  //  Add generator level cross-sections in the xml file
  //=============================================================================

  void addGenCrossSections( LHCb::GenFSR const& genFSR, std::ostream& xmlOutput ) {

    const auto& mapCounter = LHCb::GenCountersFSR::getFullNames();

    for ( auto const& counter : genFSR.genCounters() ) {
      auto key = counter.first;

      if ( key > LHCb::GenCountersFSR::CounterKey::bAndcAcc ) continue;
      if ( key == LHCb::GenCountersFSR::CounterKey::BeforeFullEvt ||
           key == LHCb::GenCountersFSR::CounterKey::AfterFullEvt )
        continue;

      longlong before = genFSR.getDenominator( key );
      if ( before == 0 ) continue;

      double C = key >= LHCb::GenCountersFSR::CounterKey::OnebGen
                     ? genFSR.getCrossSectionInfo( LHCb::CrossSectionsFSR::CrossSectionKey::MBCrossSection ).second
                     : 1;

      longlong after = key == 2
                           ? genFSR.getGenCounterInfo( static_cast<LHCb::GenCountersFSR::CounterKey>( key + 1 ) ).second
                           : counter.second.second;
      if ( after == 0 ) continue;

      double fraction = genFSR.getEfficiency( after, before, C );

      bool flag = ( to_xkey( key ) != LHCb::CrossSectionsFSR::CrossSectionKey::MeanNoZeroPUInt &&
                    to_xkey( key ) != LHCb::CrossSectionsFSR::CrossSectionKey::MeanPUInt &&
                    to_xkey( key ) != LHCb::CrossSectionsFSR::CrossSectionKey::MeanPUIntAcc );

      double error = genFSR.getEfficiencyError( after, before, C, flag );

      xmlOutput << "  <crosssection name = \"" << mapCounter.at( key ) << "\">\n"
                << "    <generated> " << after << " </generated>\n"
                << "    <value> " << fraction << " </value>\n"
                << "    <error> " << error << " </error>\n"
                << "  </crosssection>\n";
    }
  }

  //=============================================================================
  //  Add pythia cross-sections in the xml file
  //=============================================================================

  void addGeneratorCrossSections( LHCb::GenFSR const& genFSR, std::ostream& xmlOutput ) {
    for ( auto const& cross : genFSR.crossSections() ) {
      auto        key  = cross.first;
      std::string name = cross.second.first;
      name.erase( name.size() - 2 );

      longlong number = genFSR.getGenCounterInfo( static_cast<LHCb::GenCountersFSR::CounterKey>( key + 100 ) ).second;
      double   value  = cross.second.second;

      xmlOutput << "  <crosssection id = \"" << static_cast<int>( key ) << "\">\n"
                << "    <description> \"" << name << "\" </description>\n"
                << "    <generated> " << number << " </generated>\n"
                << "    <value> " << value << " </value>\n"
                << "  </crosssection>\n";
    }
  }
} // namespace

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GenFSRLog )

//=============================================================================
// Initialization
//=============================================================================
StatusCode GenFSRLog::initialize() {
  return Algorithm::initialize().andThen( [&] {
    // get the File Records service
    m_fileRecordSvc = Gaudi::svcLocator()->service( "FileRecordDataSvc" );
  } );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode GenFSRLog::execute( const EventContext& ) const { return StatusCode::SUCCESS; }

//=============================================================================
//  Finalize
//=============================================================================
StatusCode GenFSRLog::finalize() {
  printFSR();
  return Algorithm::finalize(); // must be called after all other actions
}

//=============================================================================

//=============================================================================
//  Print the GenFSR in a file xml
//=============================================================================

void GenFSRLog::printFSR() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "write to file: " + m_xmlOutputName << endmsg;

  // make an inventory of the FileRecord store
  auto addresses = m_navigatorTool->navigate( m_fileRecordName, m_FSRName );

  if ( !addresses.empty() ) {
    std::ofstream xmlOutput( m_xmlOutputName, std::fstream::out );

    if ( xmlOutput.is_open() ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << " Xml Output: " + m_xmlOutputName + " - opened" << endmsg;

      xmlOutput << "<?xml version=\"1.0\"?>\n"
                << "<generatorCounters>\n"
                << "  <version>1.1</version>\n";

      for ( const auto& genRecordAddress : addresses ) {
        DataObject* obj = nullptr;
        StatusCode  sc  = m_fileRecordSvc->retrieveObject( genRecordAddress, obj );

        if ( !sc.isSuccess() ) {
          error() << "Unable to retrieve object '" << genRecordAddress << "'" << endmsg;
          continue;
        }

        LHCb::GenFSR const* genFSR = dynamic_cast<LHCb::GenFSR*>( obj );

        if ( genFSR == nullptr ) {
          warning() << "genFSR record was not found" << endmsg;
          if ( msgLevel( MSG::DEBUG ) ) debug() << genRecordAddress << " not found" << endmsg;
        } else {
          int         evtType = genFSR->getSimulationInfo( "evtType", 0 );
          std::string genName = genFSR->getSimulationInfo( "hardGenerator", "" );
          std::string method  = genFSR->getSimulationInfo( "generationMethod", "" );

          xmlOutput << "  <eventType> " << evtType << " </eventType>\n";

          // Add generator level counters
          ::addGenCounters( *genFSR, xmlOutput );
          // Add generator level cut efficiencies
          ::addCutEfficiencies( *genFSR, xmlOutput );
          // Add generator level fractions
          ::addGenFractions( *genFSR, xmlOutput );
          // Add generator level cross-sections
          ::addGenCrossSections( *genFSR, xmlOutput );

          xmlOutput << "  <method> " << method << " </method>\n";
          xmlOutput << "  <generator> " << genName << " </generator>\n";

          // Add pythia cross-sections
          ::addGeneratorCrossSections( *genFSR, xmlOutput );
        }
      }

      xmlOutput << "</generatorCounters>\n";
      xmlOutput.close();
    } else if ( msgLevel( MSG::DEBUG ) )
      debug() << "The file was not opened correctly" << endmsg;
  } else if ( msgLevel( MSG::DEBUG ) )
    debug() << "There are no FSRs to write" << endmsg;
}
