###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Sim/SimComponents
-----------------
#]=======================================================================]

gaudi_add_module(SimComponents
    SOURCES
        src/BooleInit.cpp
        src/CheckMCEventTool.cpp
        src/DumpHepMC.cpp
        src/DumpHepMCDecay.cpp
        src/DumpHepMCTree.cpp
        src/DumpMCEventAlg.cpp
        src/EvtTypeChecker.cpp
        src/EvtTypeSvc.cpp
        src/FlagSignalChain.cpp
        src/ForcedBDecayTool.cpp
        src/GenFSRJson.cpp
        src/GenFSRLog.cpp
        src/GenFSRRead.cpp
        src/GenFSRStat.cpp
        src/MCDecayFinder.cpp
        src/MCEventTypeFinder.cpp
        src/MCHitMonitor.cpp
        src/MCParticleSelector.cpp
        src/MCReconstructible.cpp
        src/PGPrimaryVertex.cpp
        src/PrintMCDecayTreeAlg.cpp
        src/PrintMCDecayTreeTool.cpp
        src/PrintMCTree.cpp
        src/VisPrimVertTool.cpp
    LINK
        AIDA::aida
        Boost::headers
        Gaudi::GaudiKernel
        HepMC::HepMC
        LHCb::CaloDetLib
        LHCb::DAQEventLib
        LHCb::EventBase
        LHCb::FSRAlgsLib
        LHCb::GenEvent
        LHCb::LHCbKernel
        LHCb::LHCbAlgsLib
        LHCb::LHCbMathLib
        LHCb::MCEvent
        LHCb::MCInterfaces
        LHCb::PartPropLib
        LHCb::RecEvent
        LHCb::TrackEvent
        ROOT::Hist
        ROOT::RIO
)

gaudi_install(PYTHON)
