###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
DAQ/DAQUtils
------------
#]=======================================================================]

gaudi_add_header_only_library(DAQUtilsLib
    LINK
        Gaudi::GaudiAlgLib
        LHCb::LHCbAlgsLib
        LHCb::DAQEventLib
)

gaudi_add_module(DAQUtils
    SOURCES
        src/ByteStreamTests.cpp
        src/DummyRawEventCreator.cpp
        src/FileIdBankWriter.cpp
        src/ODINTimeFilter.cpp
        src/OdinBCIDFilter.cpp
        src/OdinTypesFilter.cpp
        src/RawBankReadoutStatusConverter.cpp
        src/RawBankReadoutStatusFilter.cpp
        src/RawEventCombiner.cpp
        src/RawEventDump.cpp
        src/RawEventMapCombiner.cpp
        src/RawEventSelectiveCopy.cpp
        src/RawEventSimpleCombiner.cpp
        src/RawEventSimpleMover.cpp
        src/UnpackRawEvent.cpp
        src/UnpackRawEvents.cpp
        src/RawBankSizeFilter.cpp
        src/bankKiller.cpp
        src/CombineRawBankViewsToRawEvent.cpp
        src/BackwardsCompatibleMergeViewIntoRawEvent.cpp
        src/SelectiveCombineRawBankViewsToPersist.cpp
    LINK
        Gaudi::GaudiKernel
        LHCb::LHCbAlgsLib
        LHCb::DAQEventLib
        LHCb::DAQUtilsLib
        LHCb::HltEvent
        LHCb::HltInterfaces
        LHCb::LoKiHltLib
)

gaudi_add_pytest(tests/pytest)
