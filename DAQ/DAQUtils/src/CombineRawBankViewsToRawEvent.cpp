/*****************************************************************************\
* (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "LHCbAlgs/MergingTransformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CombineRawBankViewsToRawEvent
//
// 2021-11-04 : Nicole Skidmore
//-----------------------------------------------------------------------------

/**
 *  Combines vector of RawBank::View and returns *new* RawEvent
 *  Note there is currently no check for if 2 RawBanks of the same type are being added to the new RawEvent. For the
 *  current implementation this is justified but should be revised if usage of this extends
 *
 *  @author Nicole Skidmore
 *  @date   2021-11-04
 *
 *  Update: 2023-12-30
 *  Rewritten to use same code as SelectiveCombineRawBankViewsToRawEvent as much as possible
 *  Additional protection against having inconsistent types within a view and saving banks with the same sourceID
 */

struct LessRawBankPtr final {
  bool operator()( const LHCb::RawBank* lhs, const LHCb::RawBank* rhs ) const {
    assert( lhs != nullptr && rhs != nullptr );
    return std::pair{ lhs->type(), lhs->sourceID() } < std::pair{ rhs->type(), rhs->sourceID() };
  }
};

template <typename T>
using VOC = Gaudi::Functional::vector_of_const_<T>;

class CombineRawBankViewsToRawEvent final
    : public LHCb::Algorithm::MergingTransformer<LHCb::RawEvent( VOC<LHCb::RawBank::View> const& )> {

  Gaudi::Property<bool> m_allow_same_sourceID{ this, "AllowSameSourceID", false };

public:
  CombineRawBankViewsToRawEvent( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::MergingTransformer<LHCb::RawEvent( VOC<LHCb::RawBank::View> const& )>{
            name, pSvcLocator,
            // Inputs
            KeyValues{ "RawBankViews", {} },
            // Output
            KeyValue{ "RawEvent", "/Event/DAQ/MergedEvent" } } {}

  LHCb::RawEvent operator()( VOC<LHCb::RawBank::View> const& views ) const override {

    LHCb::RawEvent rawEvent;

    unsigned int eventsize   = 0;
    unsigned int dstdatasize = 0;

    std::set<LHCb::RawBank const*, LessRawBankPtr> outputmap;
    for ( const LHCb::RawBank::View& view : views ) {
      if ( view.empty() ) {
        ++m_emptyview;
        this->debug() << "requested RawBank::View at " << inputLocation( 0 ) << " is empty" << endmsg;
        continue;
      }
      auto type = view.front()->type();
      for ( auto bank : view ) {
        if ( !bank ) {
          throw GaudiException( "RawBank::View contains a nullptr???", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }
        if ( bank->type() != type ) { // verify all banks in this view have the same type
          throw GaudiException( "Banks with different types found inside current view.", __PRETTY_FUNCTION__,
                                StatusCode::FAILURE );
        }
        auto r = outputmap.emplace( bank );
        if ( r.second ) continue; // regular case, move to the next iteration

        // entry already exists -- compare to what is already observed... and decide whether this is an error or
        // not. This fix exists due to a data taken in 2023 where some banks were saved twice, should not be allowed in
        // other cases.
        if ( m_allow_same_sourceID ) {
          if ( *r.first == bank ) {
            if ( msgLevel( MSG::DEBUG ) ) { debug() << "Two equal banks found, skipping 2nd copy" << endmsg; }
            continue;
          } else if ( std::equal( ( *r.first )->begin<std::byte>(), ( *r.first )->end<std::byte>(),
                                  bank->begin<std::byte>(), bank->end<std::byte>() ) ) {
            if ( msgLevel( MSG::DEBUG ) ) {
              debug() << "Distinct banks with same SourceID and equal content found. Skipping 2nd copy" << endmsg;
            }
            continue;
          }
        }
        throw GaudiException( name() + "Different RawBanks with the same SourceID found for type " +
                                  toString( bank->type() ),
                              __PRETTY_FUNCTION__, StatusCode::FAILURE );
      }
    }

    for ( auto bank : outputmap ) {

      rawEvent.addBank( bank->sourceID(), bank->type(), bank->version(), bank->range<std::byte>() );

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Copied RawBank: type =" << bank->type() << ", version = " << bank->version()
                << ", sourceID = " << bank->sourceID() << ", size (bytes) = " << bank->size() << endmsg;
      }
      eventsize += bank->size();
      if ( bank->type() == LHCb::RawBank::BankType::DstData ) dstdatasize += bank->size();
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << "Persisted event of size (bytes):" << eventsize << endmsg;

    m_evtsize += eventsize;
    m_dstdatasize += dstdatasize;
    return rawEvent;
  };

  mutable Gaudi::Accumulators::StatCounter<> m_dstdatasize{ this, "DstData bank size (bytes)" };
  mutable Gaudi::Accumulators::StatCounter<> m_evtsize{ this, "Event size (bytes)" };
  mutable Gaudi::Accumulators::StatCounter<> m_emptyview{ this, "Empty RawBank::View" };
  mutable Gaudi::Accumulators::StatCounter<> m_line_absent{ this, "Specified line not known" };
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CombineRawBankViewsToRawEvent )
