/*****************************************************************************\
* (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Gaudi/Parsers/Factory.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <map>
#include <numeric>
#include <string>
#include <type_traits>

//-----------------------------------------------------------------------------
// Implementation file for class : SelectiveCombineRawBankViewsToRawEvent
// Selects and combines RawBanks into a new RawEvent
// based on the list of requested RawBanks to be persisted
// 01/2023 Initial version allowing SelectivePersistency of detector RawBanks
// 12/2023 Added protection against saving banks of the same type with the same sourceID
//-----------------------------------------------------------------------------
namespace Gaudi::Parsers {
  StatusCode parse( std::map<std::string, DataObjIDColl>& result, const std::string& s ) {
    std::map<std::string, std::vector<std::string>> t;
    return parse( t, s ).andThen( [&] {
      result.clear();
      for ( const auto& [k, v] : t ) {
        DataObjIDColl col;
        std::transform( v.begin(), v.end(), std::inserter( col, col.end() ),
                        []( const auto& i ) { return DataObjID{ i }; } );
        result.emplace( k, col );
      }
    } );
  }
} // namespace Gaudi::Parsers

namespace {
  template <typename Inputs>
  class MaskedInputRange {
    boost::dynamic_bitset<>  m_mask;
    std::map<DataObjID, int> m_map;
    Inputs const*            m_inputs;

    struct Iterator {
      MaskedInputRange const*            parent;
      boost::dynamic_bitset<>::size_type i;

      Iterator& operator++() {
        if ( i != boost::dynamic_bitset<>::npos ) i = parent->m_mask.find_next( i );
        return *this;
      }
      friend bool operator!=( Iterator const& lhs, Iterator const& rhs ) {
        assert( lhs.parent == rhs.parent );
        return lhs.i != rhs.i;
      }
      auto const& operator*() const {
        assert( i != boost::dynamic_bitset<>::npos );
        return ( *parent->m_inputs )[i];
      }

      auto const* operator->() const {
        assert( i != boost::dynamic_bitset<>::npos );
        return &( ( *parent->m_inputs )[i] );
      }

      auto const& location() const {
        auto const& map = parent->m_map;
        auto        k =
            std::find_if( map.begin(), map.end(), [&]( const auto& j ) { return j.second == static_cast<int>( i ); } );
        assert( k != map.end() );
        return k->first;
      }
    };

  public:
    template <typename Parent>
    MaskedInputRange( Parent* p, Inputs const& inputs ) : m_mask{ p->inputLocationSize() }, m_inputs{ &inputs } {
      for ( size_t j = 0; j != p->inputLocationSize(); ++j ) {
        // verify that there are no duplicate input lcoations
        auto r = m_map.emplace( p->inputLocation( j ), j );
        if ( !r.second ) {
          throw GaudiException( "The input location " + p->inputLocation( j ) +
                                    " is specified more than once -- please fix your configuration",
                                __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }
      }
    }

    MaskedInputRange& request( DataObjIDColl const& ids ) {
      for ( const auto& id : ids ) {
        auto i = m_map.find( id );
        if ( i == m_map.end() ) {
          throw GaudiException( "The requested input location " + id.key() +
                                    " is not amongst the available input locations -- please fix your configuration",
                                __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }
        m_mask.set( i->second );
      }
      return *this;
    }

    Iterator begin() const { return { this, m_mask.find_first() }; }
    Iterator end() const { return { this, boost::dynamic_bitset<>::npos }; }
  };

  struct LessRawBankPtr final {
    bool operator()( const LHCb::RawBank* lhs, const LHCb::RawBank* rhs ) const {
      assert( lhs != nullptr && rhs != nullptr );
      return std::pair{ lhs->type(), lhs->sourceID() } < std::pair{ rhs->type(), rhs->sourceID() };
    }
  };

} // namespace

template <typename T>
using VOC = Gaudi::Functional::vector_of_const_<T>;

class SelectiveCombineRawBankViewsToRawEvent final
    : public LHCb::Algorithm::MergingTransformer<LHCb::RawEvent( VOC<LHCb::RawBank::View> const& )> {

  DataObjectReadHandle<LHCb::HltDecReports>             m_decrep{ this, "DecReports", "" };
  Gaudi::Property<DataObjIDColl>                        m_require{ this, "AlwaysCopy", {} };
  Gaudi::Property<std::map<std::string, DataObjIDColl>> m_map{ this, "MapLinesRawBanks", {} };
  Gaudi::Property<bool>                                 m_allow_same_sourceID{ this, "AllowSameSourceID", false };

public:
  SelectiveCombineRawBankViewsToRawEvent( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::MergingTransformer<LHCb::RawEvent( VOC<LHCb::RawBank::View> const& )>{
            name, pSvcLocator,
            // Inputs
            KeyValues{ "RawBankViews", {} },
            // Output
            KeyValue{ "RawEvent", "/Event/DAQ/MergedEvent" } } {}

  LHCb::RawEvent operator()( VOC<LHCb::RawBank::View> const& views ) const override {

    LHCb::RawEvent rawEvent;

    // Collecting and saving banks requested using SP functionality
    const auto& decReports = m_decrep.get();

    // figure out the subset of input views to actually persist
    auto inputs = MaskedInputRange{ this, views }.request( m_require.value() );
    for ( auto const& [k, rb] : m_map ) {
      if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "line " << k << endmsg;
      const LHCb::HltDecReport* decReport = decReports->decReport( k );
      if ( !decReport ) {
        ++m_line_absent;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "specified line " << k << " not present in decreport" << endmsg;
        continue;
      }
      if ( !decReport->decision() ) continue;
      if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "got positive decision for line  " << k << endmsg;
      inputs.request( rb );
    }

    unsigned int eventsize   = 0;
    unsigned int dstdatasize = 0;

    std::set<LHCb::RawBank const*, LessRawBankPtr> outputmap;
    for ( auto view = inputs.begin(); view != inputs.end(); ++view ) {
      if ( view->empty() ) {
        ++m_emptyview;
        this->debug() << "requested RawBank::View at " << view.location() << " is empty" << endmsg;
        continue;
      }
      auto type = view->front()->type();
      for ( auto bank : *view ) {
        if ( !bank ) {
          throw GaudiException( "RawBank::View contains a nullptr???", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }
        if ( bank->type() != type ) { // verify all banks in this view have the same type
          throw GaudiException( "Banks with different types found inside current view.", __PRETTY_FUNCTION__,
                                StatusCode::FAILURE );
        }
        auto r = outputmap.emplace( bank );
        if ( r.second ) continue; // regular case, move to the next iteration

        // entry already exists -- compare to what is already observed... and decide whether this is an error or
        // not. This fix exists due to a data taken in 2023 where some banks were saved twice, should not be allowed in
        // other cases.
        if ( m_allow_same_sourceID ) {
          if ( *r.first == bank ) {
            if ( msgLevel( MSG::DEBUG ) ) { debug() << "Two equal banks found, skipping 2nd copy" << endmsg; }
            continue;
          } else if ( std::equal( ( *r.first )->begin<std::byte>(), ( *r.first )->end<std::byte>(),
                                  bank->begin<std::byte>(), bank->end<std::byte>() ) ) {
            if ( msgLevel( MSG::DEBUG ) ) {
              debug() << "Distinct banks with same SourceID and equal content found. Skipping 2nd copy" << endmsg;
            }
            continue;
          }
        }
        throw GaudiException( name() + "Different RawBanks with the same SourceID found for type " +
                                  toString( bank->type() ),
                              __PRETTY_FUNCTION__, StatusCode::FAILURE );
      }
    }

    for ( auto bank : outputmap ) {

      rawEvent.addBank( bank->sourceID(), bank->type(), bank->version(), bank->range<std::byte>() );

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Copied RawBank: type =" << bank->type() << ", version = " << bank->version()
                << ", sourceID = " << bank->sourceID() << ", size (bytes) = " << bank->size() << endmsg;
      }
      eventsize += bank->size();
      if ( bank->type() == LHCb::RawBank::BankType::DstData ) dstdatasize += bank->size();
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << "Persisted event of size (bytes):" << eventsize << endmsg;

    m_evtsize += eventsize;
    m_dstdatasize += dstdatasize;
    return rawEvent;
  };

  mutable Gaudi::Accumulators::StatCounter<> m_dstdatasize{ this, "DstData bank size (bytes)" };
  mutable Gaudi::Accumulators::StatCounter<> m_evtsize{ this, "Event size (bytes)" };
  mutable Gaudi::Accumulators::StatCounter<> m_emptyview{ this, "Empty RawBank::View" };
  mutable Gaudi::Accumulators::StatCounter<> m_line_absent{ this, "Specified line not known" };
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SelectiveCombineRawBankViewsToRawEvent )
