/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiKernel/ISvcLocator.h"
#include "LHCbAlgs/Consumer.h"
#include <string>

struct RawEventSimpleMover final : LHCb::Algorithm::Consumer<void( LHCb::RawEvent const& )> {

  Gaudi::Property<std::string> m_outputLocation{ this, "RawEventOutput", LHCb::RawEventLocation::Default };

  RawEventSimpleMover( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, { KeyValue( "RawEventInput", LHCb::RawEventLocation::Default ) } ) {}

  void operator()( LHCb::RawEvent const& rawEvent ) const override {

    DataObject* ptr = nullptr;
    auto        sc  = this->evtSvc()->retrieveObject( m_outputLocation, ptr );
    if ( sc.isSuccess() && ptr ) {
      // output location already has something in it... let's move it elsewhere
      sc = this->evtSvc()
               ->unregisterObject( ptr )
               .andThen( [&] { return this->evtSvc()->registerObject( m_outputLocation + "_moved_aside", ptr ); } )
               .orElse( [&] {
                 throw GaudiException( "Failed to unregister " + m_outputLocation, __PRETTY_FUNCTION__,
                                       StatusCode::FAILURE );
               } );
    }

    auto rawEventCopy = std::make_unique<LHCb::RawEvent>();

    for ( auto i : LHCb::RawBank::types() ) {

      for ( const LHCb::RawBank* b : rawEvent.banks( i ) ) {
        if ( !b ) continue;

        rawEventCopy->addBank( b->sourceID(), b->type(), b->version(), b->range<std::byte>() );
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << " Copied RawBank type=" << b->type() << " version= " << b->version()
                  << " sourceID= " << b->sourceID() << " size (bytes) = " << b->size() << endmsg;
        }
      }
    }
    sc = this->evtSvc()->registerObject( m_outputLocation, rawEventCopy.release() );
    if ( !sc.isSuccess() ) {
      throw GaudiException( "Failed to register " + m_outputLocation, __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }
  }
};
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RawEventSimpleMover )
