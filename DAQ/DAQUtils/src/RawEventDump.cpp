/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawEvent.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <algorithm>

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : RawEventDump
//
// 2005-10-13 : Markus Frank
//-----------------------------------------------------------------------------

/** @class RawEventDump RawEventDump.h tests/RawEventDump.h
 *  Dump a RawEvent
 *
 *  @author Markus Frank
 *  @date   2005-10-13
 */
using RawEvents = Gaudi::Functional::vector_of_const_<RawEvent const*>;

class RawEventDump : public LHCb::Algorithm::MergingConsumer<void( RawEvents const& )> {
public:
  RawEventDump( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::MergingConsumer<void( RawEvents const& )>{
            name,
            pSvcLocator,
            { "RawEventLocations",
              { RawEventLocation::Default, RawEventLocation::Trigger, RawEventLocation::Rich, RawEventLocation::Calo,
                RawEventLocation::Muon, RawEventLocation::Other, RawEventLocation::Copied,
                RawEventLocation::Emulated } } } {}

  void operator()( RawEvents const& rawEvents ) const override {
    for ( auto&& [idx, raw] : range::enumerate( rawEvents ) ) {
      if ( !raw ) continue;
      for ( RawBank::BankType i : RawBank::types() ) {
        if ( !acceptBank( i ) ) continue;
        const auto& b = raw->banks( i );
        if ( b.empty() ) continue;
        info() << "banks of type " << static_cast<int>( i ) << "(" << toString( i ) << ") discovered in "
               << inputLocation( idx ) << endmsg;
        info() << b.size() << " banks of type " << static_cast<int>( i ) << ": [size, source, version, magic]";
        for ( const auto [k, r] : range::enumerate( b ) ) {
          if ( ( k % 4 ) == 0 ) info() << endmsg << "  ";
          info() << "[" << int( r->size() ) << ", " << int( r->sourceID() ) << ", " << int( r->version() ) << ", "
                 << std::hex << r->magic() << std::dec << "] ";
          if ( m_dump ) {
            info() << "Data follows..." << std::hex;
            for ( const auto& [cnt, p] : range::enumerate( r->range<int>() ) ) {
              if ( ( cnt % 10 ) == 0 ) info() << endmsg << "   ... ";
              info() << "[" << p << "] ";
            }
            info() << std::dec << endmsg << "  ";
          }
        }
        info() << endmsg;
      }
    }
  }

private:
  bool acceptBank( RawBank::BankType i ) const {
    return m_banks.empty() || std::any_of( m_banks.begin(), m_banks.end(), [i]( auto j ) { return i == j; } );
  }
  Gaudi::Property<bool>                           m_dump{ this, "DumpData",
                                false }; ///< Property "DumpData". If true, full bank contents are dumped
  Gaudi::Property<std::vector<RawBank::BankType>> m_banks{ this, "RawBanks" }; // RawBanks to be dumped  (default
                                                                               // ALL banks)
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RawEventDump )
