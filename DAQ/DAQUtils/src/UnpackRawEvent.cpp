/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "LHCbAlgs/SplittingTransformer.h"
#include <algorithm>
#include <vector>

namespace LHCb {

  using RawBankVector = std::vector<RawBank const*, Allocators::EventLocal<RawBank const*>>;

  class UnpackRawEvent
      : public Algorithm::SplittingTransformer<std::vector<RawBankVector>( const EventContext&, const RawEvent& ),
                                               Algorithm::Traits::writeViewFor<RawBankVector, RawBank::View>> {
    Gaudi::Property<std::vector<RawBank::BankType>> m_types{ this, "BankTypes" };

  public:
    UnpackRawEvent( const std::string& name, ISvcLocator* locator )
        : SplittingTransformer( name, locator, { "RawEventLocation", LHCb::RawEventLocation::Default },
                                { "RawBankLocations", {} } ) {}
    std::vector<RawBankVector> operator()( const EventContext& ctx, const RawEvent& evt ) const override {
      std::vector<RawBankVector> banks;
      banks.reserve( m_types.size() );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << " start unpacking raw events " << endmsg;
        debug() << " looking for bank types of " << m_types << endmsg;
      }
      // TODO: move the mapping done in 'RawEvent::banks' to this code, and
      //      remove it from 'RawEvent' itself, which allows 'RawEvent' to
      //      be const without mutable
      std::transform( m_types.begin(), m_types.end(), std::back_inserter( banks ),
                      [&evt, allocator = getMemResource( ctx )]( RawBank::BankType t ) {
                        const auto& bnks = evt.banks( t );
                        return RawBankVector{ bnks.begin(), bnks.end(), allocator };
                      } );
      if ( msgLevel( MSG::DEBUG ) ) {
        for ( auto b : banks ) debug() << " found " << b.size() << " banks " << endmsg;
      }

      return banks;
    }
  };

  DECLARE_COMPONENT( UnpackRawEvent )
} // namespace LHCb
