/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <cstring>
#include <vector>

/** @class LHCb::BankWriter BankWriter.h Event/BankWriter.h
 *  Helper class for writing banks.
 *  It holds a std::vector<unsigned int> and takes care of writing the
 *  data to the correct place, adding more elements if the initial space
 *  runs out and trimming if there is too much space
 *
 *  @author Matt Needham
 *  @date   2005-10-13
 */
namespace LHCb {
  class BankWriter final {
  public:
    /**
     *  Constructor
     * @param blockSize size in 32 bit words
     */
    BankWriter( size_t blockSize );

    /** get bank data
     *  @return data
     **/
    const std::vector<unsigned int>& dataBank();

    /** templated streamer */
    template <class TYPE>
    BankWriter& operator<<( TYPE value );

    /** size in bytes */
    std::size_t byteSize() const { return m_pos; }

  private:
    std::vector<unsigned int> m_dataBank;
    size_t                    m_pos = 0;
  };

  inline BankWriter::BankWriter( size_t blockSize ) {
    // constructor
    m_dataBank.resize( std::max( size_t( 1 ), blockSize ) );
  }

  inline const std::vector<unsigned int>& BankWriter::dataBank() {
    size_t used = ( m_pos + sizeof( unsigned int ) - 1 ) / sizeof( unsigned int );
    if ( used < m_dataBank.size() ) m_dataBank.resize( used );
    return m_dataBank;
  }

  template <class TYPE>
  BankWriter& BankWriter::operator<<( const TYPE value ) {
    static_assert( std::is_trivially_copyable_v<TYPE> );

    size_t startPos = m_pos;
    m_pos += sizeof( TYPE ) / sizeof( char );

    // extend the bank if necessary
    if ( m_pos > sizeof( unsigned int ) * m_dataBank.size() ) {
      m_dataBank.resize( ( m_pos + sizeof( unsigned int ) - 1 ) / sizeof( unsigned int ) );
    }

    unsigned char* cPtr = reinterpret_cast<unsigned char*>( m_dataBank.data() );
    std::memcpy( cPtr + startPos, &value, sizeof( TYPE ) );
    return *this;
  }

} // namespace LHCb
