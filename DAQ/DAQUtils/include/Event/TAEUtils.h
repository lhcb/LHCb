/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/ODIN.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "LHCbAlgs/MergingTransformer.h" // Gaudi::Functional::vector_of_const_

#include <algorithm>
#include <map>
#include <vector>

namespace LHCb::TAE {
  const auto defaultAxis = Gaudi::Accumulators::Axis<double>(
      9, -4.5, 4.5, "TAE offset",
      { "Prev4", "Prev3", "Prev2", "Prev1", "Central", "Next1", "Next2", "Next3", "Next4" } );

  /** @brief Helper class to combine ODIN and data objects for TAE.
   *
   *  The main method `arrangeTAE` takes three parameters:
   *  - a vector of const ODIN objects
   *  - a vector of const data objects
   *  - `nSide`
   *  The return value is a map with the TAE offset as key (Central = 0, Prev1 = -1, Next2 = 2, etc.)
   *  and a pair (ODIN,DataType) as value.
   *  The parameter `nSide` defines how many events to require on each side.
   *  If the request cannot be satisfied (e.g. nSide is more than the half window),
   *  an error will be logged and nothing will be returned.
   *  If `nSide` is negative, the half window is automatically determined and all
   *  TAE events are returned.
   */

  class Handler final {
  public:
    template <typename Owner>
    Handler( Owner* owner )
        : m_inconsistentSize{ owner, "inconsistent number of ODIN and data objects" }
        , m_noCentralEvent{ owner, "no central event" }
        , m_noFirstEvent{ owner, "no first event" }
        , m_notEnoughEvents{ owner, "not enough events" }
        , m_holeInBCIDs{ owner, "hole in BCIDs" }
        , m_missingData{ owner, "data missing for some event(s)" } {}

    template <typename T>
    auto arrangeTAE( Gaudi::Functional::vector_of_const_<LHCb::ODIN const*> const& odins,
                     Gaudi::Functional::vector_of_const_<T const*> const& data, int nSide = -1 ) const;

  private:
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_inconsistentSize;
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_noCentralEvent;
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_noFirstEvent;
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_notEnoughEvents;
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_holeInBCIDs;
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_missingData;
    mutable Gaudi::Accumulators::Counter<>              m_goodTAE;
  };

  template <typename T>
  auto Handler::arrangeTAE( Gaudi::Functional::vector_of_const_<LHCb::ODIN const*> const& odins,
                            Gaudi::Functional::vector_of_const_<T const*> const& data, int nSide ) const {
    // std::map<int, std::tuple<LHCb::ODIN const &, T const &>> result;
    std::map<int, std::pair<LHCb::ODIN const&, T const&>> result;

    std::vector<unsigned int> indices;
    indices.reserve( odins.size() );

    if ( odins.size() != data.size() ) {
      // inconsistent number of ODIN and data objects
      ++m_inconsistentSize;
      return result;
    }

    // remove missing events (can happen if the TAE half window was reduced but the python options were not adapted)
    for ( auto const& [i, p] : LHCb::range::enumerate( odins ) ) {
      if ( p ) indices.push_back( i );
    }

    // Find the central event.
    auto itCentral =
        std::find_if( indices.begin(), indices.end(), [&]( auto i ) { return odins[i]->timeAlignmentEventCentral(); } );
    if ( itCentral == indices.end() ) {
      // No central event -- this is a problem
      ++m_noCentralEvent;
      return result;
    }
    auto const* central = odins[*itCentral];

    // Do we have enough events (with consecutive BCIDs) on either side of the central?
    if ( nSide < 0 ) {
      // We're requested to get the complete TAE, so find out the half window
      auto itFirst = itCentral;
      while ( itFirst >= indices.begin() && !odins[*itFirst]->timeAlignmentEventFirst() ) { --itFirst; }
      if ( itFirst < indices.begin() ) {
        // No first event
        ++m_noFirstEvent;
        return result;
      }
      nSide = itCentral - itFirst;
    }

    if ( itCentral - nSide < indices.begin() || itCentral + nSide >= indices.end() ) {
      // Not enough events
      ++m_notEnoughEvents;
      return result;
    }

    // Do final checks and assemble the output map
    for ( int j = -nSide; j <= nSide; ++j ) {
      auto i    = *( itCentral + j );
      auto dist = int( odins[i]->bunchId() ) - int( central->bunchId() ) +
                  3564 * ( int( odins[i]->orbitNumber() ) - int( central->orbitNumber() ) );
      if ( dist != j ) {
        // There is a hole in the BCIDs...
        ++m_holeInBCIDs;
        return result;
      }
      if ( !data[i] ) {
        // Some data is missing. This should never happen for a sane job control flow.
        ++m_missingData;
        return result;
      }
      result.emplace( j, std::make_pair( std::cref( *odins[i] ), std::cref( *data[i] ) ) );
      // result.emplace(j, std::make_tuple(std::cref(*odins[i]), std::cref(*data[i])));
    }
    ++m_goodTAE;
    return result;
  }

} // namespace LHCb::TAE
