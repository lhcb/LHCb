/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//====================================================================

#pragma once

// Include files
#include "GaudiKernel/IEvtSelector.h"
#include "GaudiKernel/Service.h"
#include "GaudiUtils/IIODataManager.h"
#include "MDF/StreamDescriptor.h"
#include <memory>
#include <utility>

// Forward declarations
namespace Gaudi {
  class IIODataManager;
} // namespace Gaudi

/*
 *  LHCb namespace declaration
 */
namespace LHCb {

  /** @class RawDataSelector RawDataSelector.h MDF/RawDataSelector.h
   * Component for reading MDF (Master Data File) files.
   *
   * @author M.Frank
   * @date   12/12/2005
   */
  class RawDataSelector : public extends<Service, IEvtSelector> {
  public:
    /** @class LoopContext
     *
     *  @author  M.Frank
     *  @version 1.0
     */
    class LoopContext : public IEvtSelector::Context {
    protected:
      /// Owning event selector
      const RawDataSelector* m_sel = nullptr;
      /// Connection specs of current file
      std::string m_conSpec;
      /// Data holder
      mutable std::pair<char*, int> m_data{ nullptr, 0 };
      /// Current file offset
      long long m_fileOffset{ 0 };
      /// Pointer to file manager service
      Gaudi::IIODataManager* m_ioMgr = nullptr;
      /// Pointer to file connection
      std::unique_ptr<Gaudi::IDataConnection> m_connection;
      /// Cached pointer to trigger mask of the event selector
      const std::vector<unsigned int>* m_trgMask = nullptr;
      /// Cached pointer to veto mask of the event selector
      const std::vector<unsigned int>* m_vetoMask = nullptr;

    public:
      /// Standard constructor
      LoopContext( const RawDataSelector* pSelector );
      /// Standard destructor
      virtual ~LoopContext() { close(); }
      /// IEvtSelector::Context overload; context identifier
      void* identifier() const override { return (void*)m_sel; }
      /// Connection specification
      const std::string& specs() const { return m_conSpec; }
      /// Access to file offset(if possible)
      virtual long long offset() const { return m_fileOffset; }
      /// Raw data buffer (if it exists)
      virtual std::pair<char*, int> data() const { return m_data; }
      /// Release data buffer and give ownership to caller
      virtual std::pair<char*, int> releaseData() const { return std::exchange( m_data, { nullptr, 0 } ); }
      /// Receive event and update communication structure
      virtual StatusCode receiveData( IMessageSvc* msg ) = 0;
      /// Skip N events
      virtual StatusCode skipEvents( IMessageSvc* msg, int numEvt ) = 0;
      /// Set connection
      virtual StatusCode connect( const std::string& specs );
      /// close connection
      virtual void close();
    };

    /// IService implementation: initialize the service
    StatusCode initialize() override;

    /** Create a new event loop context
     * @param[in,out] refpCtxt  Reference to pointer to store the context
     *
     * @return StatusCode indicating success or failure
     */
    StatusCode createContext( Context*& refpCtxt ) const override = 0;

    /** Access last item in the iteration
     * @param[in,out] refContext Reference to the Context object.
     */
    StatusCode last( Context& /* refContext */ ) const override { return StatusCode::FAILURE; }

    /** Get next iteration item from the event loop context
     * @param[in,out] refCtxt   Reference to the context
     *
     * @return StatusCode indicating success or failure
     */
    StatusCode next( Context& refCtxt ) const override;

    /** Get next iteration item from the event loop context, but skip jump elements
     * @param[in,out] refCtxt   Reference to the context
     * @param[in]     jump      Number of events to be skipped
     *
     * @return StatusCode indicating success or failure
     */
    StatusCode next( Context& refCtxt, int jump ) const override;

    /** Get previous iteration item from the event loop context
     * @param[in,out] refCtxt   Reference to the context
     *
     * @return StatusCode indicating success or failure
     */
    StatusCode previous( Context& refCtxt ) const override;

    /** Get previous iteration item from the event loop context, but skip jump elements
     * @param[in,out] refCtxt   Reference to the context
     * @param[in]     jump      Number of events to be skipped
     *
     * @return StatusCode indicating success or failure
     */
    StatusCode previous( Context& refCtxt, int jump ) const override;

    /** Rewind the dataset
     * @param[in,out] refCtxt   Reference to the context
     *
     * @return StatusCode indicating success or failure
     */
    StatusCode rewind( Context& /* refCtxt */ ) const override { return StatusCode::FAILURE; }

    /** Create new Opaque address corresponding to the current record
     * @param[in,out] refCtxt   Reference to the context
     *
     * @return StatusCode indicating success or failure
     */
    StatusCode createAddress( const Context& refCtxt, IOpaqueAddress*& ) const override;

    /** Release existing event iteration context
     * @param[in,out] refCtxt   Reference to the context
     *
     * @return StatusCode indicating success or failure
     */
    StatusCode releaseContext( Context*& refCtxt ) const override;

    /** Will set a new criteria for the selection of the next list of events and will change
        the state of the context in a way to point to the new list.

        @param cr The new criteria string.
        @param c  Reference pointer to the Context object.
    */
    StatusCode resetCriteria( const std::string& cr, Context& c ) const override;

    /// Access to the file manager
    Gaudi::IIODataManager* fileMgr() const { return &*m_ioMgr; }

    /// Additional dataspace in buffer [BYTES]
    int additionalSpace() const { return m_addSpace * 1024; }

    /// Access to required trigger mask
    const std::vector<unsigned int>& triggerMask() const { return m_trgMask; }

    /// Access to required veto mask
    const std::vector<unsigned int>& vetoMask() const { return m_vetoMask; }

    using extends::extends;

  protected:
    using Mask = std::vector<unsigned int>;

    /// ROOT class ID in TES
    CLID m_rootCLID = CLID_NULL;

    ServiceHandle<Gaudi::IIODataManager> m_ioMgr{ this, "DataManager", "Gaudi::IODataManager/IODataManager" };
    Gaudi::Property<std::string>         m_input{ this, "Input", "", "input dataset name" };
    Gaudi::Property<int>                 m_skipEvents{ this, "NSkip", 0, "First event to process" };
    Gaudi::Property<int>                 m_printFreq{ this, "PrintFreq", -1, "printout frequency" };
    Gaudi::Property<int>                 m_addSpace{ this, "AddSpace", 1,
                                     "additional dataspace to be used to add data [KBYTES]. Default=1" };
    Gaudi::Property<Mask>                m_trgMask{
        this, "TriggerMask", {}, "required trigger mask from MDF header (only 128 bits significant)" };
    Gaudi::Property<Mask> m_vetoMask{ this, "VetoMask", {}, "veto mask from MDF header (only 128 bits significant)" };

    /// Event record count
    mutable int m_evtCount = 0;
  };
} // namespace LHCb
