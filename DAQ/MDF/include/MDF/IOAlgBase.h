/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LHCbAlgs/Transformer.h"

#include <memory>
#include <optional>
#include <string>
#include <tuple>

namespace LHCb::IO {

  /**
   * A base class for any algorithm intending to read data from files.
   *
   * Essentially an algorithm wrapper around the underlying (templated) IOHandler
   * This IOHandler has to provide 4 methods :
   *    - IOHandler(Owner& owner, std::vector<std::string>& inputFileNames)
   *      where owner will be an instance of this class
   *    - void initialize(Owner&, unsigned int& bufferNbEvents, unsigned int& nbSkippedEvents, ...);
   *      where '...' means any other parameters which will be forwarded by our initIOHandler method
   *    - void finalize()
   *    - ReturnType next( Owner&, EventContext const& )
   *      should return the next piece of data as a tuple. The precise content is defined
   *      by the IOHandler itself
   *
   * IOAlg base essentially declares 3 Properties to help configuring an I/O algorithm :
   *    - BufferNbEvents : the number of events to be prefetch in one single buffer
   *      Only an estimate, usually taken as a maximum by the IOHandler
   *    - NSkip : the number of events to skip from the input
   *    - Input : the list of input files
   *
   * When subclassing IOAlgBase, remember :
   *    - that you have to call initIOHandler in your initialize method.
   *      You can sneak in any number of parameters, of any type, they will be forwarded
   *      to the initialize method of your IOHandler
   *    - that you have to call the IOAlgBase constructor with the right number of KeyValues
   *      for the Data types returned by the get method of your IOHandler.
   *      In case you're using the IOHandler class, this is one KeyValue per type returned
   *      by your Buffer plus an extra one at the end for the buffer itself
   *
   * FIXME : when C++20 is available, use concepts here
   */
  template <typename IOHandler, typename InputType = std::vector<std::string>>
  class IOAlgBase : public LHCb::Algorithm::MultiTransformer<typename IOHandler::TESEventType( EventContext const& )> {

    using BaseType = LHCb::Algorithm::MultiTransformer<typename IOHandler::TESEventType( EventContext const& )>;

  public:
    IOAlgBase( const std::string& name, ISvcLocator* pSvcLocator,
               const typename IOHandler::ReturnKeyValuesType& outputKeys )
        : BaseType( name, pSvcLocator, outputKeys ) {}

    template <typename... InputHandlerArgs>
    void initIOHandler( InputHandlerArgs... args ) {
      m_ioHandler.emplace( *this, m_input.value() );
      m_ioHandler->initialize( *this, &*m_incidentSvc, m_bufferNbEvents.value(), m_nbSkippedEvents.value(), args... );
    }
    StatusCode finalize() override {
      m_ioHandler->finalize();
      return StatusCode::SUCCESS;
    };

  private:
    Gaudi::Property<unsigned int> m_bufferNbEvents{
        this, "BufferNbEvents", 20000,
        "approximate size of the buffer used to prefetch rawbanks in terms of number of events. Default is 20000" };
    Gaudi::Property<InputType>    m_input{ this, "Input", {}, "List of inputs" };
    Gaudi::Property<unsigned int> m_nbSkippedEvents{ this, "NSkip", 0, "First event to process" };

  protected:
    // Note that IOHandler::next is thread safe, so the usage of mutable is safe
    mutable std::optional<IOHandler> m_ioHandler;

    // incident service to be used. FIXME : drop whenever FSRSink does not need this anymore
    ServiceHandle<IIncidentSvc> m_incidentSvc{ this, "IncidentSvc", "IncidentSvc", "the incident service" };
  };
} // namespace LHCb::IO
