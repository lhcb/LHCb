/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MDF/IOAlgFileReadBase.h"
#include "MDF/IOHandlerFileRead.h"

namespace LHCb::MDF {

  template <typename IOHANDLER>
  struct IOAlgFileRead : IOAlgFileReadBase<IOHANDLER> {
    using IOAlgFileReadBase<IOHANDLER>::IOAlgFileReadBase;

    StatusCode initialize() override {
      try {
        return LHCb::IO::IOAlgBase<IOHANDLER>::initialize().andThen( [&]() { this->initIOHandler(); } );
      } catch ( IO::EndOfInput const& ) {
        // empty or non existent input files. Considered as OK for backward compatibility with old code
        return StatusCode::SUCCESS;
      }
    }
  };

} // namespace LHCb::MDF

DECLARE_COMPONENT_WITH_ID( LHCb::MDF::IOAlgFileRead<LHCb::MDF::IOHandlerFileRead>, "IOAlgFileRead" )
