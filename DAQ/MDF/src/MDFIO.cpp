/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//  ====================================================================
//  MDFIO.cpp
//  --------------------------------------------------------------------
//
//  Author    : Markus Frank
//
//  ====================================================================
#include "MDF/MDFIO.h"
#include "Event/ODIN.h"
#include "Event/RawEvent.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "MDF/MDFHeader.h"
#include "MDF/RawDataAddress.h"
#include "MDF/RawEventPrintout.h"

#include <cstring> // For memcpy, memmove with gcc 4.3

#include "RZip.h"
#include <netinet/in.h>

using namespace LHCb;

namespace {

  typedef std::pair<char*, int> MDFDescriptor;

  /// one-at-time hash function
  unsigned int hash32Checksum( const void* ptr, size_t len ) {
    unsigned int       hash = 0;
    const signed char* k    = (const signed char*)ptr;
    for ( size_t i = 0; i < len; ++i, ++k ) {
      hash += *k;
      hash += ( hash << 10 );
      hash ^= ( hash >> 6 );
    }
    hash += ( hash << 3 );
    hash ^= ( hash >> 11 );
    hash += ( hash << 15 );
    return hash;
  }

  /* ========================================================================= */
  unsigned int adler32Checksum( unsigned int adler, const char* buf, size_t len ) {
#define DO1( buf, i )                                                                                                  \
  {                                                                                                                    \
    s1 += (unsigned char)buf[i];                                                                                       \
    s2 += s1;                                                                                                          \
  }
#define DO2( buf, i )                                                                                                  \
  DO1( buf, i );                                                                                                       \
  DO1( buf, i + 1 );
#define DO4( buf, i )                                                                                                  \
  DO2( buf, i );                                                                                                       \
  DO2( buf, i + 2 );
#define DO8( buf, i )                                                                                                  \
  DO4( buf, i );                                                                                                       \
  DO4( buf, i + 4 );
#define DO16( buf )                                                                                                    \
  DO8( buf, 0 );                                                                                                       \
  DO8( buf, 8 );

    static const unsigned int BASE = 65521; /* largest prime smaller than 65536 */
    /* NMAX is the largest n such that 255n(n+1)/2 + (n+1)(BASE-1) <= 2^32-1 */
    static const unsigned int NMAX = 5550;
    unsigned int              s1   = adler & 0xffff;
    unsigned int              s2   = ( adler >> 16 ) & 0xffff;
    int                       k;

    if ( buf == NULL ) return 1;

    while ( len > 0 ) {
      k = len < NMAX ? (int)len : NMAX;
      len -= k;
      while ( k >= 16 ) {
        DO16( buf );
        buf += 16;
        k -= 16;
      }
      if ( k != 0 ) do {
          s1 += (unsigned char)*buf++;
          s2 += s1;
        } while ( --k );
      s1 %= BASE;
      s2 %= BASE;
    }
    unsigned int result = ( s2 << 16 ) | s1;
    return result;
  }
  /* ========================================================================= */

  static unsigned int xorChecksum( const int* ptr, size_t len ) {
    unsigned int checksum = 0;
    len                   = len / sizeof( int ) + ( len % sizeof( int ) ? 1 : 0 );
    for ( const int *p = ptr, *end = p + len; p < end; ++p ) { checksum ^= *p; }
    return checksum;
  }

#define QUOTIENT 0x04c11db7
  class CRC32Table {
  public:
    unsigned int m_data[256];
    CRC32Table() {
      unsigned int crc;
      for ( int i = 0; i < 256; i++ ) {
        crc = i << 24;
        for ( int j = 0; j < 8; j++ ) {
          if ( crc & 0x80000000 )
            crc = ( crc << 1 ) ^ QUOTIENT;
          else
            crc = crc << 1;
        }
        m_data[i] = htonl( crc );
      }
    }
    const unsigned int* data() const { return m_data; }
  };

  // Only works for word aligned data and assumes that the data is an exact number of words
  // Copyright  1993 Richard Black. All rights are reserved.
  static unsigned int crc32Checksum( const char* data, size_t len ) {
    static CRC32Table   table;
    const unsigned int* crctab = table.data();
    const unsigned int* p      = (const unsigned int*)data;
    const unsigned int* e      = (const unsigned int*)( data + len );
    if ( len < 4 || ( size_t( data ) % sizeof( unsigned int ) ) != 0 ) return ~0x0;
    unsigned int result = ~*p++;
    while ( p < e ) {
#if defined( LITTLE_ENDIAN )
      result = crctab[result & 0xff] ^ result >> 8;
      result = crctab[result & 0xff] ^ result >> 8;
      result = crctab[result & 0xff] ^ result >> 8;
      result = crctab[result & 0xff] ^ result >> 8;
      result ^= *p++;
#else
      result = crctab[result >> 24] ^ result << 8;
      result = crctab[result >> 24] ^ result << 8;
      result = crctab[result >> 24] ^ result << 8;
      result = crctab[result >> 24] ^ result << 8;
      result ^= *p++;
#endif
    }

    return ~result;
  }

  static unsigned short crc16Checksum( const char* data, size_t len ) {
    static const unsigned short wCRCTable[] = {
        0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241, 0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1,
        0XC481, 0X0440, 0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40, 0X0A00, 0XCAC1, 0XCB81, 0X0B40,
        0XC901, 0X09C0, 0X0880, 0XC841, 0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40, 0X1E00, 0XDEC1,
        0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41, 0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
        0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040, 0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1,
        0XF281, 0X3240, 0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441, 0X3C00, 0XFCC1, 0XFD81, 0X3D40,
        0XFF01, 0X3FC0, 0X3E80, 0XFE41, 0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840, 0X2800, 0XE8C1,
        0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41, 0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
        0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640, 0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0,
        0X2080, 0XE041, 0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240, 0X6600, 0XA6C1, 0XA781, 0X6740,
        0XA501, 0X65C0, 0X6480, 0XA441, 0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41, 0XAA01, 0X6AC0,
        0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840, 0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
        0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40, 0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1,
        0XB681, 0X7640, 0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041, 0X5000, 0X90C1, 0X9181, 0X5140,
        0X9301, 0X53C0, 0X5280, 0X9241, 0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440, 0X9C01, 0X5CC0,
        0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40, 0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
        0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40, 0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0,
        0X4C80, 0X8C41, 0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641, 0X8201, 0X42C0, 0X4380, 0X8341,
        0X4100, 0X81C1, 0X8081, 0X4040 };
    unsigned int   nTemp;
    unsigned short wCRCWord = 0xFFFF;
    while ( len-- ) {
      nTemp = *data++ ^ wCRCWord;
      wCRCWord >>= 8;
      wCRCWord = (unsigned short)( wCRCWord ^ wCRCTable[nTemp] );
    }
    return wCRCWord;
  }

  static char crc8Checksum( const char* data, int len ) {
    static unsigned char crc8_table[] = {
        0,   94,  188, 226, 97,  63,  221, 131, 194, 156, 126, 32,  163, 253, 31,  65,  157, 195, 33,  127, 252, 162,
        64,  30,  95,  1,   227, 189, 62,  96,  130, 220, 35,  125, 159, 193, 66,  28,  254, 160, 225, 191, 93,  3,
        128, 222, 60,  98,  190, 224, 2,   92,  223, 129, 99,  61,  124, 34,  192, 158, 29,  67,  161, 255, 70,  24,
        250, 164, 39,  121, 155, 197, 132, 218, 56,  102, 229, 187, 89,  7,   219, 133, 103, 57,  186, 228, 6,   88,
        25,  71,  165, 251, 120, 38,  196, 154, 101, 59,  217, 135, 4,   90,  184, 230, 167, 249, 27,  69,  198, 152,
        122, 36,  248, 166, 68,  26,  153, 199, 37,  123, 58,  100, 134, 216, 91,  5,   231, 185, 140, 210, 48,  110,
        237, 179, 81,  15,  78,  16,  242, 172, 47,  113, 147, 205, 17,  79,  173, 243, 112, 46,  204, 146, 211, 141,
        111, 49,  178, 236, 14,  80,  175, 241, 19,  77,  206, 144, 114, 44,  109, 51,  209, 143, 12,  82,  176, 238,
        50,  108, 142, 208, 83,  13,  239, 177, 240, 174, 76,  18,  145, 207, 45,  115, 202, 148, 118, 40,  171, 245,
        23,  73,  8,   86,  180, 234, 105, 55,  213, 139, 87,  9,   235, 181, 54,  104, 138, 212, 149, 203, 41,  119,
        244, 170, 72,  22,  233, 183, 85,  11,  136, 214, 52,  106, 43,  117, 151, 201, 74,  20,  246, 168, 116, 42,
        200, 150, 21,  75,  169, 247, 182, 232, 10,  84,  215, 137, 107, 53 };
    const char* s = data;
    char        c = 0;
    while ( len-- ) c = crc8_table[c ^ *s++];
    return c;
  }

  /// Generate XOR Checksum
  unsigned int genChecksum( int flag, const void* ptr, size_t len ) {
    switch ( flag ) {
    case 0:
      return xorChecksum( (const int*)ptr, len );
    case 1:
      return hash32Checksum( ptr, len );
    case 2:
      len = ( len / sizeof( int ) ) * sizeof( int );
      return crc32Checksum( (const char*)ptr, len );
    case 3:
      len = ( len / sizeof( short ) ) * sizeof( short );
      return crc16Checksum( (const char*)ptr, len );
    case 4:
      return crc8Checksum( (const char*)ptr, len );
    case 5:
      len = ( len / sizeof( int ) ) * sizeof( int );
      return adler32Checksum( 1, (const char*)ptr, len );
    case 22: // Old CRC32 (fixed by now)
      return crc32Checksum( (const char*)ptr, len );
    default:
      return ~0x0;
    }
  }

  /// Compress opaque data buffer
  /*
    The algorithm applied is the ROOT compression mechanism.
    Option "algtype" is used to specify the compression level:
    compress = 0 objects written to this file will not be compressed.
    compress = 1 minimal compression level but fast.
    ....
    compress = 9 maximal compression level but slow.
  */
  StatusCode compressBuffer( std::string algname, int alglevel, char* tar, size_t tar_len, char* src, size_t src_len,
                             size_t& new_len ) {
    int in_len, out_len, res_len = 0;
    switch ( alglevel ) {
    case 0:
      if ( tar == src ) {
        new_len = src_len;
        return StatusCode::SUCCESS;
      } else if ( tar != src && tar_len >= src_len ) {
        ::memcpy( tar, src, src_len );
        new_len = src_len;
        return StatusCode::SUCCESS;
      }
      new_len = 0;
      return StatusCode::FAILURE;
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
      in_len  = src_len;
      out_len = tar_len;
      ROOT::RCompressionSetting::EAlgorithm::EValues alg;
      if ( algname == "ZLIB" ) {
        alg = ROOT::RCompressionSetting::EAlgorithm::kZLIB;
      } else if ( algname == "ZSTD" ) {
        alg = ROOT::RCompressionSetting::EAlgorithm::kZSTD;
      } else if ( algname == "LZMA" ) {
        alg = ROOT::RCompressionSetting::EAlgorithm::kLZMA;
      } else if ( algname == "LZ4" ) {
        alg = ROOT::RCompressionSetting::EAlgorithm::kLZ4;
      } else {
        return StatusCode::FAILURE;
      }
      ::R__zipMultipleAlgorithm( alglevel, &in_len, src, &out_len, tar, &res_len, alg );
      if ( res_len == 0 || size_t( res_len ) >= src_len ) {
        // this happens when the buffer cannot be compressed
        res_len = 0;
        return StatusCode::FAILURE;
      }
      new_len = res_len;
      return StatusCode::SUCCESS;
    default:
      break;
    }
    return StatusCode::FAILURE;
  }

  /// Decompress opaque data buffer
  StatusCode decompressBuffer( int algtype, char* tar, size_t tar_len, const char* src, size_t src_len,
                               size_t& new_len ) {
    int in_len, out_len, res_len = 0;
    switch ( algtype ) {
    case 0:
      if ( tar != src && tar_len >= src_len ) {
        new_len = src_len;
        ::memcpy( tar, src, src_len );
        return StatusCode::SUCCESS;
      }
      break;
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
      in_len  = src_len;
      out_len = tar_len;
      ::R__unzip( &in_len, reinterpret_cast<unsigned char*>( const_cast<char*>( src ) ), &out_len,
                  reinterpret_cast<unsigned char*>( tar ), &res_len );
      if ( res_len > 0 ) {
        new_len = res_len;
        return StatusCode::SUCCESS;
      }
      break;
    default:
      break;
    }
    return StatusCode::FAILURE;
  }

  /// Determine length of the sequential buffer from RawEvent object
  size_t rawEventLength( RawBank::View banks ) {
    return std::accumulate( banks.begin(), banks.end(), size_t{ 0 },
                            []( size_t l, const RawBank* bank ) { return l + bank->totalSize(); } );
  }

  /// Determine length of the sequential buffer from RawEvent object
  size_t rawEventLength( const RawEvent* evt ) {
    size_t len = 0;
    for ( auto i : RawBank::types() ) { len += rawEventLength( evt->banks( i ) ); }
    return len;
  }

  /// Determine length of the sequential buffer from RawEvent object
  size_t rawEventLengthTAE( RawBank::View banks ) {
    return std::accumulate( banks.begin(), banks.end(), size_t{ 0 }, []( size_t l, const RawBank* bank ) {
      return !( bank->type() == RawBank::DAQ && bank->version() == DAQ_STATUS_BANK ) ? l + bank->totalSize() : l;
    } );
  }

  /// Determine length of the sequential buffer from RawEvent object
  size_t rawEventLengthTAE( const RawEvent* evt ) {
    auto tr = RawBank::types();
    return std::accumulate( tr.begin(), tr.end(), size_t{ 0 }, [&]( size_t len, RawBank::BankType bt ) {
      return len + rawEventLengthTAE( evt->banks( bt ) );
    } );
  }

  /// Returns the prefix on TES according to bx number, - is previous, + is next
  std::string rootFromBxOffset( int bxOffset ) {
    if ( 0 == bxOffset ) return "/Event";
    if ( 0 < bxOffset ) return std::string( "/Event/Next" ) + char( '0' + bxOffset );
    return std::string( "/Event/Prev" ) + char( '0' - bxOffset );
  }

  /// Copy RawEvent data from bank vectors to sequential buffer
  StatusCode encodeRawBanks( LHCb::RawBank::View banks, char* const data, size_t size, bool skip_hdr_bank,
                             size_t* length ) {
    const RawBank* fid = nullptr;
    size_t         len = 0, s;
    for ( const auto* b : banks ) {
      if ( skip_hdr_bank ) {
        if ( b->type() == RawBank::DAQ ) {
          if ( b->version() == DAQ_STATUS_BANK ) { continue; }
        }
      }
      if ( b->type() == RawBank::DAQ ) {
        if ( b->version() == DAQ_FILEID_BANK ) {
          fid = b;
          continue;
        }
      }
      s = b->totalSize();
      if ( size >= ( len + s ) ) {
        ::memcpy( data + len, b, s );
        len += s;
        continue;
      }
      return StatusCode::FAILURE;
    }
    // ALWAYS attach FID bank at the very end!
    if ( fid ) {
      s = fid->totalSize();
      if ( size >= ( len + s ) ) {
        ::memcpy( data + len, fid, s );
        len += s;
      }
    }
    if ( length ) *length = len;
    return StatusCode::SUCCESS;
  }

  /// Copy RawEvent data from the object to sequential buffer
  StatusCode encodeRawBanks( const RawEvent* evt, char* const data, size_t size, bool skip_hdr_bank ) {
    if ( data ) {
      size_t total = 0, len = 0;
      for ( auto i : RawBank::types() ) {
        if ( i != RawBank::DAQ ) {
          const auto& b = evt->banks( i );
          if ( encodeRawBanks( b, data + total, size - total, skip_hdr_bank, &len ).isSuccess() ) {
            total += len;
            continue;
          }
          return StatusCode::FAILURE;
        }
      }
      return encodeRawBanks( evt->banks( RawBank::DAQ ), data + total, size - total, skip_hdr_bank, &len );
    }
    return StatusCode::FAILURE;
  }

  /// Check if a given RawEvent structure belongs to a TAE event
  bool isTAERawEvent( RawEvent* raw ) {
    if ( !raw ) return false;
    //== Check ODIN event type to see if this is TAE
    const auto& oBnks = raw->banks( RawBank::ODIN );
    return std::any_of( oBnks.begin(), oBnks.end(), []( const RawBank* bank ) {
      const LHCb::ODIN odin{ bank->range<std::uint32_t>() };
      return odin.isTAE();
    } );
  }

} // namespace

/// Transform file name in presence of catalogs
std::string LHCb::MDFIO::getConnection( const std::string& org_conn ) { return org_conn; }

MDFHeader* LHCb::MDFIO::getHeader() {
  switch ( m_dataType ) {
  case MDF_NONE: // Pure RawEvent structure with MDF Header encoded as bank
  {
    SmartDataPtr<RawEvent> raw( m_evtSvc, "/Event/DAQ/RawEvent" );
    if ( raw ) {
      for ( const auto* b : raw->banks( RawBank::DAQ ) ) {
        if ( b->version() == DAQ_STATUS_BANK ) { return (MDFHeader*)b->data(); }
      }
    }
  } break;
  case MDF_RECORDS: // Ready to write MDF records...
    return (MDFHeader*)getDataFromAddress().first;
  case MDF_BANKS: // Ready to write banks structure with first bank containing MDF header...
  {
    RawBank* b = (RawBank*)getDataFromAddress().first;
    return (MDFHeader*)( b ? b->data() : 0 );
  } break;
  }
  return 0;
}

std::pair<const char*, int> LHCb::MDFIO::getDataFromAddress() {
  SmartDataPtr<DataObject> evt( m_evtSvc, "/Event" );
  if ( evt ) {
    IRegistry* reg = evt->registry();
    if ( reg ) {
      IOpaqueAddress* padd = reg->address();
      RawDataAddress* pA   = dynamic_cast<RawDataAddress*>( padd );
      if ( pA ) { return pA->data(); }
    }
    MsgStream log1( m_msgSvc, m_parent );
    log1 << MSG::ERROR << "[Direct I/O] operations can only be performed on valid raw buffers!" << endmsg;
    return std::pair<const char*, int>( 0, 0 );
  }
  MsgStream log2( m_msgSvc, m_parent );
  log2 << MSG::ERROR << "[Direct I/O] failed to retrieve event object: /Event" << endmsg;
  return std::pair<const char*, int>( 0, 0 );
}

StatusCode LHCb::MDFIO::commitRawBanks( RawEvent* raw, const RawBank* hdr_bank, std::string compAlg, int compTyp,
                                        int chksumTyp, void* const ioDesc ) {
  try {
    size_t        len     = rawEventLength( raw );
    size_t        hdrSize = hdr_bank->totalSize();
    MDFDescriptor space   = getDataSpace( ioDesc, len );
    if ( space.first ) {
      m_spaceActions++;
      StatusCode sc = encodeRawBanks( raw, space.first + hdrSize, len, true );
      if ( sc.isSuccess() ) {
        sc = writeDataSpace( compAlg, compTyp, chksumTyp, ioDesc, hdr_bank, space.first, len );
        if ( sc.isSuccess() ) {
          m_writeActions++;
          return sc;
        }
        MsgStream err( m_msgSvc, m_parent );
        err << MSG::ERROR << "Failed write data to output device." << endmsg;
        m_writeErrors++;
        return sc;
      }
      MsgStream err( m_msgSvc, m_parent );
      err << MSG::ERROR << "Failed to encode output banks." << endmsg;
      m_writeErrors++;
      return sc;
    }
    if ( !m_silent ) {
      MsgStream log0( m_msgSvc, m_parent );
      log0 << MSG::ERROR << "Failed allocate output space." << endmsg;
    }
    m_spaceErrors++;
    return StatusCode::FAILURE;
  } catch ( std::exception& e ) {
    MsgStream log( m_msgSvc, m_parent );
    log << MSG::ERROR << "Got exception when writing data:" << e.what() << endmsg;
  } catch ( ... ) {
    MsgStream log( m_msgSvc, m_parent );
    log << MSG::ERROR << "Got unknown exception when writing data." << endmsg;
  }
  m_writeErrors++;
  return StatusCode::FAILURE;
}

StatusCode LHCb::MDFIO::commitRawBanks( std::string compAlg, int compTyp, int chksumTyp, void* const ioDesc,
                                        const std::string& location ) {
  SmartDataPtr<RawEvent> raw( m_evtSvc, location );
  if ( raw ) {
    size_t len;
    bool   isTAE = !m_ignoreTAE && ( m_forceTAE || isTAERawEvent( raw ) ); // false in principle...unless TAE
    if ( !isTAE ) {
      for ( const auto* b : raw->banks( RawBank::DAQ ) ) {
        if ( b->version() == DAQ_STATUS_BANK ) { return commitRawBanks( raw, b, compAlg, compTyp, chksumTyp, ioDesc ); }
      }

      len                 = rawEventLength( raw );
      const auto& odin    = raw->banks( RawBank::ODIN );
      RawBank*    hdrBank = createDummyMDFHeader( raw, len );
      if ( odin.empty() ) {
        MsgStream log1( m_msgSvc, m_parent );
        log1 << MSG::INFO << "Adding dummy MDF/DAQ[DAQ_STATUS_BANK] information." << endmsg;
      } else {
        unsigned int         trMask[4] = { ~0u, ~0u, ~0u, ~0u };
        const LHCb::ODIN     odin_info{ odin[0]->range<std::uint32_t>() };
        MDFHeader::SubHeader inf = hdrBank->begin<MDFHeader>()->subHeader();
        MsgStream            log1( m_msgSvc, m_parent );
        if ( log1.level() <= MSG::DEBUG )
          log1 << MSG::DEBUG << "Creating MDF/DAQ[DAQ_STATUS_BANK] with ODIN information." << endmsg;
        inf.H1->setTriggerMask( trMask );
        inf.H1->setRunNumber( odin_info.runNumber() );
        inf.H1->setOrbitNumber( odin_info.orbitNumber() );
        inf.H1->setBunchID( odin_info.bunchId() );
      }
      raw->adoptBank( hdrBank, true );
      return commitRawBanks( raw, hdrBank, compAlg, compTyp, chksumTyp, ioDesc );
    }
    //== TAE event. Start scanning the whole TES for previous and next events.
    MsgStream              msg( m_msgSvc, m_parent );
    RawEvent               privateBank;  // This holds the two temporary banks
    std::vector<RawEvent*> theRawEvents; // Keep pointer on found events
    if ( msg.level() <= MSG::DEBUG ) msg << MSG::DEBUG << "Found a TAE event. scan for various BXs" << endmsg;

    // prepare the bank containing information for each BX
    // Maximum +-7 crossings, due to hardware limitation of derandomisers = 15 consecutive BX
    int              sizeCtrlBlock = 3 * 15;
    std::vector<int> ctrlData;
    ctrlData.reserve( sizeCtrlBlock );
    size_t         offset    = 0;
    const RawBank* centerMDF = nullptr;
    for ( int n = -7; 7 >= n; ++n ) {
      std::string            loc = rootFromBxOffset( n ) + "/" + location;
      SmartDataPtr<RawEvent> rawEvt( m_evtSvc, loc );
      if ( rawEvt ) {
        theRawEvents.push_back( rawEvt );
        ctrlData.push_back( n );      // BX offset
        ctrlData.push_back( offset ); // offset in buffer, after end of this bank
        if ( !centerMDF || n == 0 ) {
          const auto& bnks = raw->banks( RawBank::DAQ );
          auto        i    = std::find_if( bnks.begin(), bnks.end(),
                                           []( const LHCb::RawBank* b ) { return b->version() == DAQ_STATUS_BANK; } );
          if ( i != bnks.end() ) centerMDF = *i;
        }
        size_t l = rawEventLengthTAE( rawEvt );
        ctrlData.push_back( l ); // size of this BX information
        offset += l;
        if ( msg.level() <= MSG::DEBUG ) msg << MSG::DEBUG << "Found RawEvent in " << loc << ", size =" << l << endmsg;
      }
    }
    len               = ctrlData.size();
    RawBank* ctrlBank = privateBank.createBank( 0, RawBank::TAEHeader, 0, sizeof( int ) * len, &ctrlData[0] );

    // Create now the complete event header (MDF header) with complete length
    len              = offset + ctrlBank->totalSize();
    RawBank* hdrBank = createDummyMDFHeader( &privateBank, len );
    if ( centerMDF ) {
      MDFHeader::SubHeader inf    = hdrBank->begin<MDFHeader>()->subHeader();
      MDFHeader::SubHeader center = const_cast<RawBank*>( centerMDF )->begin<MDFHeader>()->subHeader();
      inf.H1->setTriggerMask( center.H1->triggerMask() );
      inf.H1->setRunNumber( center.H1->runNumber() );
      inf.H1->setOrbitNumber( center.H1->orbitNumber() );
      inf.H1->setBunchID( center.H1->bunchID() );
    }
    len += hdrBank->totalSize();

    // Prepare the input to add these two banks to an output buffer
    std::vector<const RawBank*> banks;
    banks.push_back( hdrBank );
    banks.push_back( ctrlBank );

    size_t        total  = len;
    size_t        length = 0;
    MDFDescriptor space  = getDataSpace( ioDesc, total );
    if ( space.first ) {
      char* dest = space.first;
      m_spaceActions++;
      if ( encodeRawBanks( banks, dest, len, false, &length ).isSuccess() ) {
        dest += length;
        len -= length;
      }
      for ( unsigned int kk = 0; theRawEvents.size() != kk; ++kk ) {
        size_t     myLength = ctrlData[3 * kk + 2]; // 2 words header, third element for each buffer
        StatusCode sc       = encodeRawBanks( theRawEvents[kk], dest, myLength, true );
        if ( !sc.isSuccess() ) {
          msg << MSG::ERROR << "Failed to encode output raw data banks." << endmsg;
          m_spaceErrors++;
          privateBank.removeBank( ctrlBank );
          privateBank.removeBank( hdrBank );
          return sc;
        }
        dest += myLength;
        len -= myLength;
      }
      //== here, len should be 0...
      if ( 0 != len ) { msg << MSG::ERROR << "Expect length=0, found " << len << " starting from " << total << endmsg; }

      StatusCode sc = writeDataSpace( compAlg, compTyp, chksumTyp, ioDesc, hdrBank, space.first, total );
      if ( !sc.isSuccess() ) { msg << MSG::ERROR << "Failed write data to output device." << endmsg; }
      if ( msg.level() <= MSG::DEBUG )
        msg << MSG::DEBUG << "Wrote TAE event of length:" << total << " bytes from " << theRawEvents.size() << " Bxs"
            << endmsg;
      privateBank.removeBank( ctrlBank );
      privateBank.removeBank( hdrBank );
      return sc;
    }
    m_spaceErrors++;
    msg << MSG::ERROR << "Failed to get space, size = " << len << endmsg;
    privateBank.removeBank( ctrlBank );
    privateBank.removeBank( hdrBank );
    return StatusCode::FAILURE;
  }
  if ( !m_silent ) {
    MsgStream log2( m_msgSvc, m_parent );
    log2 << MSG::ERROR << "Failed to retrieve raw event object at " << location << endmsg;
  }
  return StatusCode::FAILURE;
}

RawBank* LHCb::MDFIO::createDummyMDFHeader( RawEvent* raw, size_t len ) {
  unsigned int trMask[] = { ~0u, ~0u, ~0u, ~0u };
  RawBank*     hdrBank =
      raw->createBank( 0, RawBank::DAQ, DAQ_STATUS_BANK, sizeof( MDFHeader ) + sizeof( MDFHeader::Header1 ), 0 );
  MDFHeader* hdr = (MDFHeader*)hdrBank->data();
  hdr->setChecksum( 0 );
  hdr->setCompression( 0 );
  hdr->setHeaderVersion( 3 );
  hdr->setSpare( 0 );
  hdr->setDataType( MDFHeader::BODY_TYPE_BANKS );
  hdr->setSubheaderLength( sizeof( MDFHeader::Header1 ) );
  hdr->setSize( len );
  MDFHeader::SubHeader h = hdr->subHeader();
  h.H1->setTriggerMask( trMask );
  h.H1->setRunNumber( ~0x0 );
  h.H1->setOrbitNumber( ~0x0 );
  h.H1->setBunchID( ~0x0 );
  return hdrBank;
}

/// Direct I/O with valid existing raw buffers
StatusCode LHCb::MDFIO::commitRawBuffer( const void* data, size_t len, int type, int /* compTyp */, int /* chksumTyp */,
                                         void* const ioDesc ) {
  StatusCode  sc  = StatusCode::FAILURE;
  const char* ptr = (const char*)data;
  switch ( type ) {
  case MDF_BANKS: // data buffer with bank structure
  {
    const RawBank* hdr     = (const RawBank*)ptr;
    MDFHeader*     h       = (MDFHeader*)hdr->data();
    int            hdrSize = sizeof( MDFHeader ) + h->subheaderLength();
    int            bnkSize = hdr->totalSize();
    writeBuffer( ioDesc, h, hdrSize ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    // Need two write due to RAW bank alignment
    sc = writeBuffer( ioDesc, ptr + bnkSize, len - bnkSize );
    sc.isSuccess() ? ++m_writeActions : ++m_writeErrors;
    return sc;
  }
  case MDF_RECORDS: // Already ready to write MDF record
    sc = writeBuffer( ioDesc, ptr, len );
    sc.isSuccess() ? ++m_writeActions : ++m_writeErrors;
    return sc;
  default:
    m_writeErrors++;
    break;
  }
  return sc;
}

/// Direct I/O with valid existing raw buffers
StatusCode LHCb::MDFIO::commitRawBuffer( int type, int compTyp, int chksumTyp, void* const ioDesc ) {
  std::pair<const char*, int> data = getDataFromAddress();
  if ( data.first ) { return commitRawBuffer( data.first, data.second, type, compTyp, chksumTyp, ioDesc ); }
  return StatusCode::FAILURE;
}

/// Write MDF record from serialization buffer
StatusCode LHCb::MDFIO::writeDataSpace( std::string compAlg, int compTyp, int chksumTyp, void* const ioDesc,
                                        const RawBank* hdr, char* const data, size_t len ) {
  MDFHeader* h       = (MDFHeader*)hdr->data();
  int        bnkSize = hdr->totalSize();
  size_t     newlen  = len - bnkSize;
  int        hdrSize = sizeof( MDFHeader ) + h->subheaderLength();
  char*      ptr     = data + bnkSize;
  if ( compTyp ) {
    m_tmp.reserve( len + bnkSize );
    if ( compressBuffer( compAlg, compTyp, m_tmp.data() + bnkSize, newlen, ptr, newlen, newlen ).isSuccess() ) {
      ptr     = m_tmp.data() + bnkSize;
      int cmp = ( len - bnkSize ) / newlen - 1;
      compTyp = ( compTyp & 0xF ) + ( ( cmp > 0xF ? 0xF : cmp ) << 4 );
    } else {
      // Bad compression; file uncompressed buffer
      newlen  = len - bnkSize;
      compTyp = 0;
      ptr     = data + bnkSize;
    }
  }
  int chkSize = newlen + hdrSize - 4 * sizeof( int );
  if ( m_dataType == MDF_RECORDS ) {
    ptr -= hdrSize;
    ::memmove( ptr, h, hdrSize );
    h = (MDFHeader*)ptr;
    h->setSize( newlen );
    newlen += hdrSize;
  } else {
    ptr -= bnkSize;
    // Careful: if the MDFHeaders are not 32 bit aligned,
    //          the checksum later will be wrong!
    ::memmove( ptr, hdr, bnkSize );
    h = (MDFHeader*)( (RawBank*)ptr )->data();
    h->setSize( newlen );
    newlen += bnkSize;
  }
  const char* chk_begin = ( (char*)h ) + 4 * sizeof( int );
  h->setCompression( compTyp );
  h->setChecksum( chksumTyp > 0 ? genChecksum( chksumTyp, chk_begin, chkSize ) : 0 );
  StatusCode sc = writeBuffer( ioDesc, ptr, newlen );
  sc.isSuccess() ? ++m_writeActions : ++m_writeErrors;
  return sc;
}

bool LHCb::MDFIO::checkSumOk( int checksum, const char* src, int datSize, bool prt ) {
  if ( checksum ) {
    int chk = genChecksum( 1, src, datSize );
    if ( chk != checksum ) { // Try to fix with old checksum calculation
      int chk2 = genChecksum( 22, src, datSize );
      if ( chk2 != checksum ) {
        if ( prt ) {
          MsgStream log( m_msgSvc, m_parent );
          log << MSG::ERROR << "Data corruption. [Invalid checksum] expected:" << std::hex << checksum
              << " got:" << std::hex << chk << endmsg;
        }
        return false;
      }
    }
  }
  return true;
}

MDFDescriptor LHCb::MDFIO::readLegacyBanks( const MDFHeader& h, void* const ioDesc, bool dbg ) {
  int  rawSize    = sizeof( MDFHeader );
  int  checksum   = h.checkSum();
  int  compress   = h.compression() & 0xF;
  int  expand     = ( h.compression() >> 4 ) + 1;
  bool velo_patch = h.subheaderLength() == sizeof( int );
  int  datSize    = h.size() + h.sizeOf( velo_patch ? 0 : 1 );
  int  hdrSize    = velo_patch ? sizeof( MDFHeader::Header0 ) - 2 : h.subheaderLength() - 2;
  int  readSize   = hdrSize + datSize;
  int  alloc_len  = rawSize + readSize + ( compress ? expand * readSize : 0 );
  if ( dbg ) {
    MsgStream log( m_msgSvc, m_parent );
    if ( velo_patch )
      log << MSG::INFO << "Velo testbeam data - Compression:" << compress << " Checksum:" << ( checksum != 0 )
          << endmsg;
    else if ( h.headerVersion() == 1 )
      log << MSG::INFO << "Legacy format[1] - Compression:" << compress << " Checksum:" << ( checksum != 0 ) << endmsg;
  }
  // accomodate for potential padding of MDF header bank!
  MDFDescriptor space = getDataSpace( ioDesc, alloc_len + sizeof( int ) + sizeof( RawBank ) );
  char*         data  = space.first;
  if ( !data ) {
    m_spaceErrors++;
  } else {
    m_spaceActions++;
    RawBank* b = (RawBank*)data;
    b->setMagic();
    b->setType( RawBank::DAQ );
    b->setSize( rawSize + hdrSize );
    b->setVersion( DAQ_STATUS_BANK );
    b->setSourceID( 0 );
    int bnkSize = b->totalSize();
    ::memcpy( b->data(), &h, rawSize );
    char*      bptr = (char*)b->data();
    MDFHeader* hdr  = (MDFHeader*)bptr;
    if ( compress != 0 ) {
      m_tmp.reserve( readSize );
      size_t     new_len = 0;
      StatusCode sc      = readBuffer( ioDesc, m_tmp.data(), readSize );
      if ( sc.isSuccess() ) {
        const char* src = m_tmp.data();
        hdr->setHeaderVersion( 3 );
        hdr->setDataType( MDFHeader::BODY_TYPE_BANKS );
        hdr->setSubheaderLength( sizeof( MDFHeader::Header1 ) );
        hdr->setSize( datSize );
        src += hdrSize;
        char*  ptr        = ( (char*)data ) + bnkSize;
        size_t space_size = space.second - bnkSize;
        if ( m_ignoreChecksum ) {
          hdr->setChecksum( 0 );
        } else if ( !checkSumOk( checksum, src, datSize, true ) ) {
          return MDFDescriptor( 0, -1 );
        }
        if ( decompressBuffer( compress, ptr, space_size, src, datSize, new_len ).isSuccess() ) {
          hdr->setHeaderVersion( 3 );
          hdr->setSize( new_len );
          hdr->setCompression( 0 );
          hdr->setChecksum( 0 );
          if ( h.headerVersion() == 1 ) {
            hdr->setDataType( 0 );
            hdr->setSpare( 0 );
          }
          return std::pair<char*, int>( data, bnkSize + new_len );
        }
        MsgStream log0( m_msgSvc, m_parent );
        log0 << MSG::ERROR << "Cannot allocate sufficient space for decompression." << endmsg;
        return MDFDescriptor( 0, -1 );
      } else if ( sc.isFailure() ) {
        MsgStream log( m_msgSvc, m_parent );
        log << MSG::INFO << "Cannot read more data (Compressed record). End-of-File reached." << endmsg;
        return MDFDescriptor( 0, -1 );
      }

      MsgStream log1( m_msgSvc, m_parent );
      log1 << MSG::ERROR << "Cannot read " << readSize << " bytes of compressed data." << endmsg;
      return MDFDescriptor( 0, -1 );
    }
    // Read uncompressed data file...
    int        off = bnkSize - hdrSize;
    StatusCode sc  = readBuffer( ioDesc, data + off, readSize );
    if ( sc.isSuccess() ) {
      if ( m_ignoreChecksum ) {
        hdr->setChecksum( 0 );
      } else if ( !checkSumOk( checksum, data + off + hdrSize, datSize, true ) ) {
        return MDFDescriptor( 0, -1 );
      }
      off -= rawSize + b->hdrSize();
      if ( off != 0 ) { ::memmove( bptr + rawSize, bptr + rawSize + off, hdrSize ); }
      if ( velo_patch ) {
        // Fix for intermediate VELO data
        MDFHeader::Header0* h0 = hdr->subHeader0().H0;
        if ( h0->triggerMask()[0] != 0x103 ) {
          MsgStream log( m_msgSvc, m_parent );
          log << MSG::ERROR << "Data corruption. [Velo_Patch]....expect trouble!!!" << endmsg;
        }
        h0->setEventType( hdr->hdr() );
        hdr->setSubheaderLength( sizeof( MDFHeader::Header0 ) );
        hdr->setHeaderVersion( 0 );
      } else if ( h.headerVersion() == 1 ) {
        hdr->setHeaderVersion( 3 );
        hdr->setDataType( 0 );
        hdr->setSpare( 0 );
      }
      return std::pair<char*, int>( data, bnkSize + datSize );
    } else if ( sc.isFailure() ) {
      MsgStream log( m_msgSvc, m_parent );
      log << MSG::INFO << "Cannot read more data (Uncompressed record). End-of-File reached." << endmsg;
      return MDFDescriptor( 0, -1 );
    }

    MsgStream log2( m_msgSvc, m_parent );
    log2 << MSG::ERROR << "Cannot allocate buffer to read:" << readSize << " bytes " << endmsg;
    return MDFDescriptor( 0, -1 );
  }
  MsgStream log3( m_msgSvc, m_parent );
  log3 << MSG::ERROR << "Cannot read " << readSize << " bytes of uncompressed data." << endmsg;
  return MDFDescriptor( 0, -1 );
}

MDFDescriptor LHCb::MDFIO::readBanks( const MDFHeader& h, void* const ioDesc, bool dbg ) {
  int          rawSize   = sizeof( MDFHeader );
  unsigned int checksum  = h.checkSum();
  int          compress  = h.compression() & 0xF;
  int          expand    = ( h.compression() >> 4 ) + 1;
  int          hdrSize   = h.subheaderLength();
  int          readSize  = h.recordSize() - rawSize;
  int          chkSize   = h.recordSize() - 4 * sizeof( int );
  int          alloc_len = rawSize + readSize + sizeof( MDFHeader ) + sizeof( RawBank ) + sizeof( int ) +
                  ( compress ? expand * readSize : 0 );
  if ( dbg ) {
    MsgStream log( m_msgSvc, m_parent );
    log << MSG::INFO << "Compression:" << compress << " Checksum:" << ( checksum != 0 ) << endmsg;
  }
  // accomodate for potential padding of MDF header bank!
  MDFDescriptor space = getDataSpace( ioDesc, alloc_len + sizeof( int ) + sizeof( RawBank ) );
  char*         data  = space.first;
  if ( !data ) {
    m_spaceErrors++;
  } else {
    m_spaceActions++;
    RawBank* b = (RawBank*)data;
    b->setMagic();
    b->setType( RawBank::DAQ );
    b->setSize( rawSize + hdrSize );
    b->setVersion( DAQ_STATUS_BANK );
    b->setSourceID( 0 );
    int bnkSize = b->totalSize();
    ::memcpy( b->data(), &h, rawSize );
    char*      bptr = (char*)b->data();
    MDFHeader* hdr  = (MDFHeader*)bptr;
    if ( compress != 0 ) {
      m_tmp.reserve( readSize + rawSize );
      ::memcpy( m_tmp.data(), &h, rawSize ); // Need to copy header to get checksum right
      StatusCode sc = readBuffer( ioDesc, m_tmp.data() + rawSize, readSize );
      if ( sc.isSuccess() ) {
        int space_retry = 0;
        while ( space_retry++ < 5 ) {
          if ( space_retry > 1 ) {
            MsgStream log( m_msgSvc, m_parent );
            alloc_len *= 2;
            log << MSG::INFO << "Retry with increased buffer space of " << alloc_len << " bytes." << endmsg;
            space = getDataSpace( ioDesc, alloc_len + sizeof( int ) + sizeof( RawBank ) );
            data  = space.first;
            if ( !data ) {
              m_spaceErrors++;
              goto NoSpace;
            }
            m_spaceActions++;
            b = (RawBank*)data;
            b->setMagic();
            b->setType( RawBank::DAQ );
            b->setSize( rawSize + hdrSize );
            b->setVersion( DAQ_STATUS_BANK );
            b->setSourceID( 0 );
            bnkSize = b->totalSize();
            ::memcpy( b->data(), &h, rawSize );
            bptr = (char*)b->data();
            hdr  = (MDFHeader*)bptr;
          }
          ::memcpy( bptr + rawSize, m_tmp.data() + rawSize, hdrSize );
          if ( m_ignoreChecksum ) {
            hdr->setChecksum( 0 );
          } else if ( checksum && checksum != genChecksum( 1, m_tmp.data() + 4 * sizeof( int ), chkSize ) ) {
            return MDFDescriptor( 0, -1 );
          }
          // Checksum is correct...from all we know data integrity is proven
          size_t      new_len    = 0;
          const char* src        = m_tmp.data() + rawSize;
          char*       ptr        = ( (char*)data ) + bnkSize;
          size_t      space_size = space.second - bnkSize;
          if ( decompressBuffer( compress, ptr, space_size, src + hdrSize, h.size(), new_len ).isSuccess() ) {
            hdr->setSize( new_len );
            hdr->setCompression( 0 );
            hdr->setChecksum( 0 );
            return std::pair<char*, int>( data, bnkSize + new_len );
          }
          ++space_retry;
        }
      NoSpace:
        MsgStream log0( m_msgSvc, m_parent );
        log0 << MSG::ERROR << "Cannot allocate sufficient space for decompression." << endmsg;
        return MDFDescriptor( 0, -1 );
      } else if ( sc.isFailure() ) {
        MsgStream log( m_msgSvc, m_parent );
        log << MSG::INFO << "Cannot read more data (Compressed record). End-of-File reached." << endmsg;
        return MDFDescriptor( 0, -1 );
      }
      MsgStream log1( m_msgSvc, m_parent );
      log1 << MSG::ERROR << "Cannot read " << readSize << " bytes of compressed data." << endmsg;
      return MDFDescriptor( 0, -1 );
    }
    // Read uncompressed data file...
    StatusCode sc = readBuffer( ioDesc, bptr + rawSize, readSize );
    if ( sc.isSuccess() ) {
      if ( m_ignoreChecksum ) {
        hdr->setChecksum( 0 );
      } else if ( checksum && checksum != genChecksum( 1, bptr + 4 * sizeof( int ), chkSize ) ) {
        return MDFDescriptor( 0, -1 );
      }
      return std::pair<char*, int>( data, bnkSize + h.size() );
    } else if ( sc.isFailure() ) {
      MsgStream log( m_msgSvc, m_parent );
      log << MSG::INFO << "Cannot read more data (Uncompressed record). End-of-File reached." << endmsg;
      return MDFDescriptor( 0, -1 );
    }
    MsgStream log2( m_msgSvc, m_parent );
    log2 << MSG::ERROR << "Cannot allocate buffer to read:" << readSize << " bytes " << endmsg;
    return MDFDescriptor( 0, -1 );
  }
  MsgStream log3( m_msgSvc, m_parent );
  log3 << MSG::ERROR << "Cannot read " << readSize << " bytes of uncompressed data." << endmsg;
  return MDFDescriptor( 0, -1 );
}

MDFDescriptor LHCb::MDFIO::readBanks( void* const ioDesc, bool dbg ) {
  MDFHeader  h;
  int        rawSize = sizeof( MDFHeader );
  StatusCode sc      = readBuffer( ioDesc, &h, rawSize );
  if ( sc.isSuccess() ) {
    bool velo_patch = h.subheaderLength() == sizeof( int );
    bool vsn1_hdr   = !velo_patch && h.headerVersion() == 1;
    if ( velo_patch || vsn1_hdr ) { return readLegacyBanks( h, ioDesc, dbg ); }
    return readBanks( h, ioDesc, dbg );
  } else if ( sc.isFailure() ) {
    MsgStream log( m_msgSvc, m_parent );
    log << MSG::INFO << "Cannot read more data  (Header). End-of-File reached." << endmsg;
    return MDFDescriptor( 0, -1 );
  }
  MsgStream log( m_msgSvc, m_parent );
  if ( log.level() <= MSG::DEBUG ) log << MSG::DEBUG << "Cannot read " << rawSize << " bytes of header data." << endmsg;
  return MDFDescriptor( 0, -1 );
}

/// Read raw char buffer from input stream
StatusCode LHCb::MDFIO::readBuffer( void* const /* ioDesc */, void* const /* data */, size_t /* len */ ) {
  throw std::runtime_error( "LHCb::MDFIO::readBuffer: "
                            "This is a default implementation which should never be called!" );
}

StatusCode LHCb::MDFIO::writeBuffer( void* const /* ioDesc */, const void* /* data */, size_t /* len */ ) {
  throw std::runtime_error( "LHCb::MDFIO::writeBuffer: "
                            "This is a default implementation which should never be called!" );
}

// Pass raw banks to RawEvent object
StatusCode LHCb::MDFIO::adoptBanks( RawEvent* evt, const std::vector<RawBank*>& bnks, bool copy_banks ) {
  if ( evt ) {
    // MsgStream log(m_msgSvc, m_parent);
    for ( std::vector<RawBank*>::const_iterator k = bnks.begin(); k != bnks.end(); ++k ) {
      // log << MSG::DEBUG << "Adopt bank:" << RawEventPrintout::bankHeader(*k) << endmsg;
      evt->adoptBank( *k, copy_banks );
    }
    return StatusCode::SUCCESS;
  }
  return StatusCode::FAILURE;
}
