###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest


@pytest.mark.ctest_fixture_setup("mdf.dst_from_mdf")
@pytest.mark.shared_cwd("MDF")
class Test(LHCbExeTest):
    command = ["gaudirun.py", "$MDFROOT/tests/options/dst_from_mdf.py"]
    # Expected warning:
    #  HiveDataBrokerSvc  WARNING non-reentrant algorithm: OutputStream/OutputStream
    reference = {"messages_count": {"FATAL": 0, "ERROR": 0, "WARNING": 1}}
