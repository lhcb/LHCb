###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["python", "../options/existing_mdf.py"]
    # Expected warning :
    # HiveDataBrokerSvc  WARNING non-reentrant algorithm: LHCb::MDFWriter/LHCb__MDFWriter
    reference = {"messages_count": {"FATAL": 0, "ERROR": 0, "WARNING": 1}}
