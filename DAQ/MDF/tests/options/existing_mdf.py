###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

from PyConf.Algorithms import LHCb__MDFWriter as MDFWriter
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    create_or_reuse_mdfIOAlg,
    mdf_writer,
)
from PyConf.control_flow import CompositeNode

FILENAME = "existing.mdf"
EVTMAX = 100
EXISTING_SIZE = EVTMAX * 200 * 1024

# Write a dummy file that should be larged than the MDFWriter output
with open(FILENAME, "wb") as f:
    f.write(bytearray([0xFF] * EXISTING_SIZE))

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("HltDAQ-routingbits_full")
options.evt_max = EVTMAX
options.output_file = FILENAME
options.output_type = "MDF"

config = configure_input(options)

ioalg = create_or_reuse_mdfIOAlg("/Event/DAQ/RawEvent", options)
writer = mdf_writer(options.output_file, "/Event/DAQ/RawEvent")
cf_node = CompositeNode("Seq", children=[ioalg, writer])
config.update(configure(options, cf_node))

from Gaudi.Main import gaudimain

c = gaudimain()
c.run(False)

size = os.stat(FILENAME).st_size
print("Existing file size:", EXISTING_SIZE)
print("Current file size:", size)
assert size < EXISTING_SIZE, "Existing file was not truncated"
