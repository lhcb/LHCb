###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Write a DST (ROOT file) from an MDF."""

from PyConf.Algorithms import OutputStream
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    create_or_reuse_mdfIOAlg,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.evt_max = 1000
options.set_input_and_conds_from_testfiledb("MiniBrunel_2018_MinBias_FTv4_MDF")
options.output_file = "output.dst"
options.output_type = "ROOT"

config = configure_input(options)

ioalg = create_or_reuse_mdfIOAlg("/Event/DAQ/RawEvent", options)

writer = OutputStream(
    ItemList=["/Event/DAQ/RawEvent#1"],
    Output="DATAFILE='{}' SVC='Gaudi::RootCnvSvc' OPT='RECREATE'".format(
        options.output_file
    ),
)

cf_node = CompositeNode("Seq", children=[ioalg, writer])
config.update(configure(options, cf_node))
