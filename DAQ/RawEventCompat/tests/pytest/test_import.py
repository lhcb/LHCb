###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest


@pytest.mark.ctest_fixture_setup("raweventcompat.import")
@pytest.mark.shared_cwd("RawEventCompat")
class Test(LHCbExeTest):
    """
    # Author: rlambert
    # Purpose: Unit test, Check I can import the DBase package this requires
    # Common failure modes:
    #   . MAJOR: ImportError, and AttributeError are thrown initially if the database cannot be loaded.
    #       Then forceLoad() is called. If anything fails it indicates a big problem with
    #       the database or with the way DBASE is picked up.
    """

    command = ["python", "../options/test-import.py"]

    def test_if_passed(self, stdout: bytes):
        expected_string = b"Pass"
        assert stdout.find(expected_string) != -1, (
            "'Pass' missing in the standard output."
        )
