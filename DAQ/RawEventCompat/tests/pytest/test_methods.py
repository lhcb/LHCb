###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest


@pytest.mark.ctest_fixture_required("raweventcompat.import")
@pytest.mark.shared_cwd("RawEventCompat")
class Test(LHCbExeTest):
    """
    # Author: rlambert
    # Purpose: Unit test of the methods and functions in RawEventCompat
    # Common failure modes, severities and cures:
    #        . MAJOR: Any failure of this test indicates a major problem
    #           in the methods or functions of RawEventCompat.
    #           check ../options/test methods.py and ameliorate the code in the python methods.
    """

    command = ["python", "../options/test-methods.py"]

    def test_if_passed(self, stdout: bytes):
        expected_string = b"Pass"
        assert stdout.find(expected_string) != -1, (
            "'Pass' missing in the standard output."
        )
