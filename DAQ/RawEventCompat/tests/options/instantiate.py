from __future__ import print_function

import sys
import traceback

import Configurables

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# ============ Auto-generated testing python file ======
from Gaudi.Configuration import *
from GaudiKernel.ConfigurableDb import cfgDb, loadConfigurableDb

##### load all configurables of this package #####
loadConfigurableDb()
for name, conf in cfgDb.items():
    if conf["package"] == "RawEventCompat":
        if hasattr(Configurables, name):
            try:
                aConf = getattr(Configurables, name)
                aConf()
            except Exception as e:
                print(
                    "ERROR, cannot import/instantiate configurable",
                    name,
                    "\n-------\n",
                    e.__class__,
                    "\n-------",
                    file=sys.stderr,
                )
                traceback.print_exc()

# =========== Auto-generated, import all python modules ====
import glob
import os
import sys
import traceback

modules = glob.glob("../../python/RawEventCompat/*.py")
for mod in modules:
    try:
        module_name = mod.split(os.sep)[-1][:-3]
        if module_name != "__init__":
            amod = __import__("RawEventCompat." + module_name)
    except Exception as e:
        print(
            "ERROR, cannot import module",
            mod,
            "\n-------\n",
            e.__class__,
            "\n-------",
            end=" ",
            file=sys.stderr,
        )
        traceback.print_exc()
