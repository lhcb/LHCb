###############################################################################
# (c) Copyright 2020-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Protect from multiple include (global scope, because
# everything defined in this file is globally visible)
include_guard(GLOBAL)

# locate script to extract QMTest metadata
message(STATUS ${CMAKE_CURRENT_LIST_DIR} ${CMAKE_MODULE_PATH})
find_file(extract_qmtest_metadata extract_qmtest_metadata.py
    PATHS ${CMAKE_CURRENT_LIST_DIR} ${CMAKE_MODULE_PATH}
    NO_DEFAULT_PATH)

function(gaudi_add_qmtest)
    if(NOT TARGET Python::Interpreter)
        message(FATAL_ERROR "No python interpreter was found, call find_package(Python REQUIRED Interpreter) first.")
    endif()

    # Naming convention for tests: PackageName.TestName
    get_filename_component(package_name ${CMAKE_CURRENT_SOURCE_DIR} NAME)

    # Custom test_directory
    set(test_directory)
    if(ARGC GREATER 0)
        set(test_directory "${ARGV0}")
    endif()

    if(test_directory)
        set(qmtest_root_dir "${test_directory}")
    else()
        set(qmtest_root_dir "${CMAKE_CURRENT_SOURCE_DIR}/tests/qmtest")
    endif()
    file(GLOB_RECURSE qmt_files RELATIVE "${qmtest_root_dir}" "${qmtest_root_dir}/*.qmt") # ordered lexicographically
    string(TOLOWER "${package_name}" subdir_name_lower)
    file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/qmtest_tmp")
    # Add a test for each qmt files
    foreach(qmt_file IN LISTS qmt_files)
        string(REPLACE ".qmt" "" qmt_name "${qmt_file}")
        string(REPLACE ".qms/" "." qmt_name "${qmt_name}")
        string(REGEX REPLACE "^${subdir_name_lower}\\." "" qmt_name "${qmt_name}")
        add_test(NAME ${package_name}.${qmt_name}
            COMMAND run $<TARGET_FILE:Python::Interpreter> -m GaudiConf.QMTest.Run
                --skip-return-code 77
                --report ctest
                --common-tmpdir ${CMAKE_CURRENT_BINARY_DIR}/qmtest_tmp
                --workdir ${qmtest_root_dir}
                ${qmtest_root_dir}/${qmt_file}
            WORKING_DIRECTORY "${qmtest_root_dir}")
        set_tests_properties(${package_name}.${qmt_name}
            PROPERTIES LABELS "${PROJECT_NAME};${package_name};QMTest"
            SKIP_RETURN_CODE 77
            TIMEOUT 0)
    endforeach()
    # Extract dependencies to a cmake file
    if(NOT extract_qmtest_metadata)
        message(FATAL_ERROR "Cannot find extract_qmtest_metadata.py")
    endif()
    mark_as_advanced(extract_qmtest_metadata)
    # Note: we rely on the Python instalation available a configure time
    execute_process(
        COMMAND python3 ${extract_qmtest_metadata} ${package_name} ${qmtest_root_dir}
        OUTPUT_FILE ${CMAKE_CURRENT_BINARY_DIR}/qmt_deps.cmake
        RESULT_VARIABLE qmt_deps_retcode)
    if(NOT qmt_deps_retcode EQUAL "0")
        message(FATAL_ERROR "Failed to compute dependencies of QMTest tests.")
        return()
    endif()
    # Include the generated file with the QMTest dependencies
    include(${CMAKE_CURRENT_BINARY_DIR}/qmt_deps.cmake)
endfunction()

function(gaudi_add_tests type)
    if(NOT BUILD_TESTING)
        return()
    endif()

    # Naming convention for tests: PackageName.TestName
    get_filename_component(package_name ${CMAKE_CURRENT_SOURCE_DIR} NAME)

    # Custom test_directory
    set(test_directory)
    if(ARGC GREATER 1)
        set(test_directory "${ARGV1}")
    endif()

    # Test framework used
    if(type STREQUAL "QMTest")
        gaudi_add_qmtest(${test_directory})

    elseif(type STREQUAL "pytest")
        if(NOT test_directory)
            set(test_directory "tests/pytest")
        endif()
        gaudi_add_pytest(${test_directory} OPTIONS -v --doctest-modules)

    else()
        message(FATAL_ERROR "${type} is not a valid test framework.")
    endif()
endfunction()
