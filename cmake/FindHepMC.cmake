###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Locate HepMC library
# Defines:
#  Variables:
#   HepMC_FOUND
#   HepMC_INCLUDE_DIR
#   HepMC_INCLUDE_DIRS (not cached)
#   HepMC_<component>_LIBRARY
#   HepMC_LIBRARIES (not cached)
#   HepMC_LIBRARY_DIRS (not cached)
#  Targets (optional components):
#   HepMC::HepMC (default)
#   HepMC::fio

find_path(HepMC_INCLUDE_DIR HepMC/HepMCDefs.h)
mark_as_advanced(HepMC_INCLUDE_DIR)
set(HepMC_INCLUDE_DIRS ${HepMC_INCLUDE_DIR})
if(NOT DEFINED HepMC_VERSION)
  file(READ ${HepMC_INCLUDE_DIR}/HepMC/HepMCDefs.h _hepmc_defs)
  if(_hepmc_defs MATCHES "#define +HEPMC_VERSION +\"([0-9.]+)\"")
    set(HepMC_VERSION ${CMAKE_MATCH_1})
  endif()
endif()

if(NOT HepMC_FIND_COMPONENTS)
  set(HepMC_FIND_COMPONENTS HepMC)
endif()
foreach(component IN LISTS HepMC_FIND_COMPONENTS)
  if(component STREQUAL "HepMC")
    set(libname HepMC)
  else()
    set(libname HepMC${component})
  endif()

  find_library(HepMC_${component}_LIBRARY NAMES ${libname})
  mark_as_advanced(HepMC_${component}_LIBRARY)
  if(HepMC_${component}_LIBRARY)
    set(HepMC_${component}_FOUND ${HepMC_${component}_LIBRARY})
    if(NOT TARGET HepMC::${component})
      add_library(HepMC::${component} UNKNOWN IMPORTED)
      set_target_properties(HepMC::${component} PROPERTIES IMPORTED_LOCATION ${HepMC_${component}_LIBRARY})
      target_include_directories(HepMC::${component} INTERFACE "${HepMC_INCLUDE_DIR}")
    endif()
    list(APPEND HepMC_LIBRARIES ${HepMC_${component}_LIBRARY})
    get_filename_component(_comp_dir ${HepMC_${component}_LIBRARY} DIRECTORY)
    list(APPEND HepMC_LIBRARY_DIRS ${_comp_dir})
  endif()

endforeach()

if(HepMC_LIBRARY_DIRS)
  list(REMOVE_DUPLICATES HepMC_LIBRARY_DIRS)
endif()

# handle the QUIETLY and REQUIRED arguments and set HepMC_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
find_package_handle_standard_args(HepMC
  HANDLE_COMPONENTS
  VERSION_VAR HepMC_VERSION
  REQUIRED_VARS HepMC_INCLUDE_DIR HepMC_LIBRARIES
)
