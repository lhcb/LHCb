###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re

from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v"]

    def options(self):
        from Configurables import AuditorTestAlg, TimingAuditor
        from Configurables import GaudiExamples__LoggingAuditor as LoggingAuditor
        from Gaudi.Configuration import ApplicationMgr, AuditorSvc, MessageSvc, ToolSvc

        AuditorSvc().Auditors += [
            TimingAuditor("TIMER"),
            LoggingAuditor("LoggingAuditor"),
        ]

        app = ApplicationMgr(TopAlg=[AuditorTestAlg()], EvtSel="NONE", EvtMax=5)

        app.ExtSvc += [ToolSvc(), AuditorSvc()]
        app.AuditAlgorithms = True

        MessageSvc().setDebug.append("EventLoopMgr")

    def test_timing_report(self, stdout: bytes):
        assert re.search(b"TIMER.TIMER *INFO AuditorTestAlg:loop", stdout), (
            "Missing timing report for timer 'AuditorTestAlg:loop'"
        )
