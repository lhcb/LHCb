###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from pathlib import Path

from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py"]

    def options(self):
        from Configurables import (
            ApplicationMgr,
            AuditorSvc,
            ChronoStatSvc,
            GaudiExamplesCommonConf,
            TimingAuditor,
        )
        from Gaudi.Configuration import DEBUG

        GaudiExamplesCommonConf()

        logfile = "timing_data.log"

        # ensure that we start from a new file
        import os

        if os.path.exists(logfile):
            os.remove(logfile)

        ChronoStatSvc(PerEventFile=logfile, OutputLevel=DEBUG)

        auditSvc = AuditorSvc()
        auditSvc.Auditors.append(TimingAuditor("TIMER"))

        app = ApplicationMgr()
        app.TopAlg = ["GaudiExamples::TimingAlg/Timing"]
        app.EvtSel = "NONE"  # do not use any event input
        app.EvtMax = 400
        app.ExtSvc.extend(["ToolSvc", auditSvc])
        app.AuditAlgorithms = True

    def test_regexp_match(self, stdout: bytes):
        import re

        expected = (
            rb"Timing\s+SUCCESS\s+The timing is \(in us\)\s*\n"
            + rb"\|\s*\|\s*#\s*\|\s*Total\s*\|\s*Mean\+-RMS\s*\|\s*Min/Max\s*\|\s*\n"
            + rb"\|\s*\(1U\)\s*\|\s*\d*\s*\|(?:[-+.\deE /]+\|){3}\s*\n"
            + rb"\|\s*\(2U\)\s*\|\s*\d*\s*\|(?:[-+.\deE /]+\|){3}\s*\n"
            + rb"\|\s*\(3U\)\s*\|\s*\d*\s*\|(?:[-+.\deE /]+\|){3}\s*\n"
        )
        assert re.search(expected, stdout), "Expected regexp match not found."

    def test_timing_log(self, cwd: Path):
        assert os.path.exists(cwd / "timing_data.log"), "Expected timing log missing."

        lines = open(cwd / "timing_data.log").read().splitlines()

        assert len(lines) == 1, "Timing long contect not as expected: len(lines) != 1"

        entries = lines[0].split()
        assert len(entries) == 401, (
            "Timing log contect not as expected: # of lines not 401."
        )
        assert entries[0] == "Timing", (
            "Timing log contect not as expected: missing 'Timing' at line 0."
        )
