###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest


@pytest.mark.ctest_fixture_setup("gaudialg.evtcolsex.prepare")
@pytest.mark.shared_cwd("GaudiAlg")
class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v", "../../options/EvtColsEx/Prepare.py"]
    reference = "../refs/EvtColsEx/Prepare.yaml"
    environment = ["GAUDIAPPNAME=", "GAUDIAPPVERSION="]
