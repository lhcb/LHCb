###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.Configurable import Configurable
from LHCbTesting import LHCbExeTest

from GaudiConfUtils import (
    configurableExists,
    filterConfigurables,
    isConfigurable,
    isIterable,
)


class Dummy(object):
    pass


class DummyConfigurable(Configurable):
    def getGaudiType(self):
        pass

    def getDlls(self):
        pass

    def getHandle(self):
        pass


class Test(LHCbExeTest):
    command = ["echo", ""]

    def test_isConfigurable(self):
        assert isConfigurable(5.0) is False
        assert isConfigurable("bob") is False
        assert isConfigurable(Dummy()) is False
        assert isConfigurable(DummyConfigurable("TestConf")) is True

    def test_configurableExists(self):
        assert configurableExists("bob") is False
        _ = DummyConfigurable("Bob")
        assert configurableExists("Bob") is True

    def test_isIterable(self):
        assert isIterable([1, 2, 3, 4, 5]) is True
        assert isIterable("Hello World") is False
        assert isIterable((1, 2, 3, 4, 5, 6)) is True
        assert isIterable({"a": 1, "b": 2, "c": 3}) is True
        assert isIterable(1) is False
        assert isIterable(1.0) is False
        assert isIterable(Dummy()) is False
        conf = DummyConfigurable("TestConf")
        assert isIterable(conf) is False

    def test_filterConfigurables(self):
        a = 1
        b = 2
        c = 3
        d = "hello"
        e = [1, 2, 3, 4, 5]
        f = 1.2345
        ca = DummyConfigurable("a")
        cb = DummyConfigurable("b")
        cc = DummyConfigurable("c")
        symbols = [a, ca, b, c, cb, d, e, cc, f]
        assert filterConfigurables(symbols) == [ca, cb, cc]
