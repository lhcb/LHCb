from __future__ import print_function

import os

from Configurables import ApplicationMgr, TCKANNSvc
from Gaudi.Configuration import DEBUG

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import (
    CombineRawBankViewsToRawEvent,
    HltDecReportsDecoder,
    HltDecReportsWriter,
    HltSelReportsDecoder,
    HltSelReportsWriter,
    RawEventDump,
    bankKiller,
)
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    default_raw_banks,
    mdf_writer,
)
from PyConf.control_flow import CompositeNode

if os.path.exists("rewritten.mdf"):
    os.system("rm rewritten.mdf")

options = ApplicationOptions(_enabled=False)
options.evt_max = 10
options.set_input_from_testfiledb("2015_raw_full")
options.data_type = "2015"
options.simulation = False
options.dddb_tag = "dddb-20150724"
options.conddb_tag = "cond-20150828"
config = configure_input(options)

selReports = default_raw_banks("HltSelReports")
decReports = default_raw_banks("HltDecReports")

selDecoder1 = HltSelReportsDecoder(
    name="Hlt1SelReportsDecoder",
    SourceID="Hlt1",
    RawBanks=selReports,
    DecReports=decReports,
    DecoderMapping="TCKANNAvc",
    OutputLevel=DEBUG,
)
decDecoder1 = HltDecReportsDecoder(
    name="Hlt1DecReportsDecoder",
    SourceID="Hlt1",
    RawBanks=decReports,
    DecoderMapping="TCKANNAvc",
    OutputLevel=DEBUG,
)
selDecoder2 = HltSelReportsDecoder(
    name="Hlt2SelReportsDecoder",
    SourceID="Hlt2",
    RawBanks=selReports,
    DecReports=decReports,
    DecoderMapping="TCKANNAvc",
    OutputLevel=DEBUG,
)
decDecoder2 = HltDecReportsDecoder(
    name="Hlt2DecReportsDecoder",
    SourceID="Hlt2",
    RawBanks=decReports,
    DecoderMapping="TCKANNAvc",
    OutputLevel=DEBUG,
)

selWriter1 = HltSelReportsWriter(
    name="Hlt1SelReportsWriter",
    SourceID="Hlt1",
    SelReports=selDecoder1.OutputHltSelReportsLocation,
    DecReports=decDecoder1.OutputHltDecReportsLocation,
    ObjectSummaries=selDecoder1.OutputHltObjectSummariesLocation,
    ANNSvc="TCKANNAvc",
    EncodingKey=0x010600A2,
    OutputLevel=DEBUG,
)

decWriter1 = HltDecReportsWriter(
    name="Hlt1DecReportsWriter",
    SourceID="Hlt1",
    InputHltDecReportsLocation=decDecoder1.OutputHltDecReportsLocation,
    EncodingKey=0x010600A2,
    DecoderMapping="TCKANNAvc",
    OutputLevel=DEBUG,
)

selWriter2 = HltSelReportsWriter(
    name="Hlt2SelReportsWriter",
    SourceID="Hlt2",
    SelReports=selDecoder2.OutputHltSelReportsLocation,
    DecReports=decDecoder2.OutputHltDecReportsLocation,
    ObjectSummaries=selDecoder2.OutputHltObjectSummariesLocation,
    ANNSvc="TCKANNAvc",
    EncodingKey=0x010600A2,
    OutputLevel=DEBUG,
)

decWriter2 = HltDecReportsWriter(
    name="Hlt2DecReportsWriter",
    SourceID="Hlt2",
    InputHltDecReportsLocation=decDecoder2.OutputHltDecReportsLocation,
    EncodingKey=0x010600A2,
    DecoderMapping="TCKANNAvc",
    OutputLevel=DEBUG,
)

##Temporary for backwards compatibility - merge the HLT1 and HLT2 RawBank::Views back into the default RawEvent
views_to_raw_event = CombineRawBankViewsToRawEvent(
    RawBankViews=[
        selWriter1.OutputView,
        decWriter1.OutputView,
        selWriter2.OutputView,
        decWriter2.OutputView,
    ]
)

dump = RawEventDump(
    RawBanks=["HltSelReports", "HltDecReports"],
    RawEventLocations=["/Event/DAQ/RawEvent"],
    DumpData=True,
)
killer2 = bankKiller(
    name="Killer2",
    BankTypes=["HltSelReports", "HltDecReports", "ODIN"],
    DefaultIsKill=True,
)

hltann = TCKANNSvc("TCKANNAvc")
appmgr = ApplicationMgr()
appmgr.ExtSvc += [hltann]

writer = mdf_writer("rewritten.mdf", "/Event/DAQ/RawEvent")

dumpSeq = CompositeNode(
    "Dump", children=[dump]
)  # needed to force dump to run before kill
node = CompositeNode("Seq", children=[dumpSeq, views_to_raw_event, killer2, writer])
config.update(configure(options, node))
