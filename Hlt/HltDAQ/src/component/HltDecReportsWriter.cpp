/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "Event/RawEvent.h"
#include "HltSourceID.h"
#include "Kernel/IIndexedANNSvc.h"
#include "LHCbAlgs/Transformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltDecReportsWriter
//
// 2008-07-26 : Tomasz Skwarnicki
//-----------------------------------------------------------------------------

/** @class DecReportsWriter HltDecReportsWriter.h
 *
 *
 *  @author Tomasz Skwarnicki
 *  @date   2008-07-26
 *
 *  Algorithm to convert HltDecReports container on TES to *new* RawEvent and Raw Bank View
 *
 */

namespace LHCb::Hlt::DAQ {
  namespace {
    bool isNonTrivialDecReport( unsigned int r ) noexcept {
      HltDecReport dr{ r };
      return dr != HltDecReport{}.setIntDecisionID( dr.intDecisionID() );
    }
    constexpr bool alwaysTrue( unsigned int ) noexcept { return true; }

    template <typename Container, typename Predicate>
    auto back_inserter_if( Container& c, Predicate predicate ) {
      struct Inserter {
        Container* container;
        Predicate  predicate;
        Inserter&  operator=( const typename Container::value_type& val ) {
          if ( std::invoke( predicate, val ) ) container->push_back( val );
          return *this;
        }
        Inserter& operator*() { return *this; }
        Inserter& operator++() { return *this; }
        Inserter& operator++( int ) { return *this; }
      };
      return Inserter{ &c, std::move( predicate ) };
    }
    // C++23: use std::to_underlying...
    template <class Enum>
    constexpr std::underlying_type_t<Enum> to_underlying( Enum e ) noexcept {
      return static_cast<std::underlying_type_t<Enum>>( e );
    }
    bool validate_table( HltDecReports const& reports, IIndexedANNSvc::map_t const& table ) {
      // check that the entries in the table are an exact match (and not e.g. a superset) for the reports
      // if so, we can omit the entries with a 'default' decReport in the packed data
      if ( reports.size() != table.size() ) return false;
      // decreports are sorted by _name_, not by decision ID -- so must do that here...
      std::vector<unsigned int> rep_ids;
      rep_ids.reserve( reports.size() );
      std::transform( reports.begin(), reports.end(), std::back_inserter( rep_ids ),
                      []( const auto& r ) { return r.second.intDecisionID(); } );
      std::sort( rep_ids.begin(), rep_ids.end() );
      // map_t is (should be!) sorted by the key, i.e. the numerical id
      if ( !std::is_sorted( table.begin(), table.end(),
                            []( const auto& lhs, const auto& rhs ) { return lhs.first < rhs.first; } ) ) {
        throw GaudiException( "IIndexedANNSvc::map_t is not sorted???", __PRETTY_FUNCTION__, StatusCode::FAILURE );
      }
      auto r = std::mismatch( table.begin(), table.end(), rep_ids.begin(), rep_ids.end(),
                              []( const auto& tbl, const auto& id ) { return tbl.first == id; } );
      return r.first == table.end() && r.second == rep_ids.end();
    }
  } // namespace

  class DecReportsWriter
      : public Algorithm::MultiTransformer<std::tuple<RawEvent, RawBank::View>( HltDecReports const& ),
                                           Algorithm::Traits::writeOnly<RawEvent>> {

    /// SourceID to insert in the bank header
    Gaudi::Property<bool>     m_zerosuppression{ this, "EnableZeroSuppression", true };
    Gaudi::Property<SourceID> m_sourceID{ this, "SourceID", SourceID::Dummy };
    Gaudi::Property<bool>     m_verify_decoder_mapping{ this, "VerifyDecoderMapping", true };
    Gaudi::Property<unsigned> m_encodingKey{ this, "EncodingKey", 0,
                                             "if non-zero, ignore TCK in decreport, and use this value" };

    ServiceHandle<IIndexedANNSvc> m_svc{ this, "DecoderMapping", "HltANNSvc" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_unable_to_suppress{
        this, "Requested to zerosuppress, but unable to do so -- copying all decisions instead..." };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_missing_mapping{
        this, "No string key found for trigger decision", 50 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_wrong_mapping{
        this, "Wrong string key found for trigger decision", 50 };

  public:
    enum struct Version : int { non_zero_suppressed = 3, zero_suppressed = 4 };

    DecReportsWriter( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer{ name,
                            pSvcLocator,
                            // Input
                            KeyValue{ "InputHltDecReportsLocation", HltDecReportsLocation::Default },
                            // Outputs
                            { KeyValue{ "OutputRawEvent", "/Event/DAQ/HltDecEvent" },
                              KeyValue{ "OutputView", "/Event/DAQ/HltDec/View" } } } {};

    std::tuple<RawEvent, RawBank::View> operator()( HltDecReports const& inputSummary ) const override {

      // Create *new* RawEvent
      RawEvent outputrawevent;

      assert( m_sourceID == SourceID::Hlt1 || m_sourceID == SourceID::Hlt2 || m_sourceID == SourceID::Spruce );
      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << " Input: ";
        for ( const auto& rep : inputSummary ) {
          auto decRep = HltDecReport{ rep.second.decReport() };
          verbose() << decRep.intDecisionID() << "-" << decRep.decision() << " ";
        }
        verbose() << endmsg;
      }
      // verify the string <-> int mapping that will be done by the decoding later...
      if ( m_verify_decoder_mapping ) {
        const auto& tbl = m_svc->i2s( m_encodingKey, selectionID_for( m_sourceID.value() ) );
        for ( const auto& [name, dec] : inputSummary ) {
          auto i = tbl.find( dec.intDecisionID() );
          if ( i == tbl.end() ) {
            ++m_missing_mapping;
          } else if ( i->second != name ) {
            ++m_wrong_mapping;
          }
        }
      }

      // compose the bank body
      std::vector<unsigned int> bankBody;
      constexpr size_t          prefixSize = 3;
      bankBody.reserve( inputSummary.size() + prefixSize );
      bankBody.push_back( m_encodingKey.value() ); // new in version 3
      bankBody.push_back( inputSummary.configuredTCK() );
      bankBody.push_back( inputSummary.taskID() );
      // decide to copy all, or only copy 'non-trivial' decisions...
      auto version =
          ( ( m_zerosuppression && validate_table( inputSummary, m_svc->i2s( m_encodingKey.value(),
                                                                             selectionID_for( m_sourceID.value() ) ) ) )
                ? Version::zero_suppressed
                : Version::non_zero_suppressed );

      if ( m_zerosuppression && version != Version::zero_suppressed ) ++m_unable_to_suppress;

      assert( bankBody.size() == prefixSize );
      std::transform(
          std::begin( inputSummary ), std::end( inputSummary ),
          back_inserter_if( bankBody, version == Version::zero_suppressed ? isNonTrivialDecReport : alwaysTrue ),
          []( const auto& r ) { return r.second.decReport(); } );

      // order according to the values, essentially orders by intDecisionID
      // this is important since it will put "*Global" reports at the beginning of the bank
      // NOTE: we must skip the first three words (encoding key, configuredTCK, taskID)
      std::sort( std::next( std::begin( bankBody ), prefixSize ), std::end( bankBody ) );

      // shift bits in sourceID for the same convention as in HltSelReports
      uint16_t sourceIDCommon =
          shift<SourceIDMasks::OnlineReserved>( subSystemBits ) | shift<SourceIDMasks::Process>( m_sourceID.value() );
      outputrawevent.addBank( sourceIDCommon, RawBank::HltDecReports, to_underlying( version ), bankBody );

      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << " Output:  ";
        verbose() << " VersionNumber= " << to_underlying( version );
        verbose() << " SourceID= " << m_sourceID.value();
        auto i = std::begin( bankBody );
        verbose() << " key: " << *i++ << " ";
        verbose() << " configuredTCK = " << *i++ << " ";
        verbose() << " taskID = " << *i++ << " ";
        while ( i != std::end( bankBody ) ) {
          auto rep = HltDecReport{ *i++ };
          verbose() << rep.intDecisionID() << "-" << rep.decision() << " ";
        }
        verbose() << endmsg;
      }

      // make sure view creation happens
      //    a) after the RawEvent is fully populated
      //    b) prior to moving RawEvent
      // -- note: cannot use the generic `writeViewFor` mechanism here as that requires
      // the view to be constructible solely from the source, which it isn't here as it
      // somehow needs to know which banktype, i.e. RawBank::HltDecReports in this case,
      // to view...
      auto view = outputrawevent.banks( RawBank::HltDecReports );
      // without std::move here the RawEvent gets copied which would invalidate the view
      return { std::move( outputrawevent ), std::move( view ) };
    };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( DecReportsWriter, "HltDecReportsWriter" )

} // namespace LHCb::Hlt::DAQ
