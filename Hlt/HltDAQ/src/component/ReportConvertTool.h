/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef REPORTCONVERTTOOL_H
#define REPORTCONVERTTOOL_H 1

// Include files
// from STL
#include <string>
#include <unordered_map>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// From PhysEvent
#include "Event/CaloCluster.h"
#include "Event/MuonPID.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"
#include "Event/RichPID.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Event/Vertex.h"

#include "Event/HltDecReports.h"
#include "Event/HltSelReports.h"
#include "Event/HltVertexReports.h"

#include "Event/HltObjectSummary.h"
#include "HltSelRepRBStdInfo.h"
#include "IReportConvert.h"

using GaudiUtils::VectorMap;
using LHCb::HltObjectSummary;
using LHCb::HltSelRepRBStdInfo;
using std::pair;
using std::string;
using std::unordered_map;

/** @class ReportConvertTool ReportConvertTool.h
 *
 *  Tool to check if version number of the HltSelReports
 *
 *  @author Sean Benson
 *  @date   02/10/2014
 */
class ReportConvertTool : public extends<GaudiTool, IReportConvert> {
public:
  /// Standard constructor
  ReportConvertTool( const std::string& type, const std::string& name, const IInterface* parent );

  int getSizeSelRepParticleLatest() const override;

  //===========================================================================
  /// Convert the sub bank to a HltObjectSummary.
  void SummaryFromRaw( HltObjectSummary::Info*, HltSelRepRBStdInfo::StdInfo*, int, int ) const override;

  /// Put the information in to the HltObjectSummary
  void ParticleObject2Summary( HltObjectSummary::Info*, const LHCb::Particle*, bool, int ) const override;
  void ProtoParticleObject2Summary( HltObjectSummary::Info*, const LHCb::ProtoParticle*, bool, int ) const override;
  void TrackObject2Summary( HltObjectSummary::Info*, const LHCb::Track*, bool ) const override;
  void RichPIDObject2Summary( HltObjectSummary::Info*, const LHCb::RichPID*, bool, int ) const override;
  void MuonPIDObject2Summary( HltObjectSummary::Info*, const LHCb::MuonPID*, bool, int ) const override;
  void CaloClusterObject2Summary( HltObjectSummary::Info*, const LHCb::CaloCluster*, bool, int ) const override;
  void CaloHypoObject2Summary( HltObjectSummary::Info*, const LHCb::CaloHypo*, bool, int ) const override;
  void RecVertexObject2Summary( HltObjectSummary::Info*, const LHCb::RecVertex*, bool ) const override;
  void VertexObject2Summary( HltObjectSummary::Info*, const LHCb::Vertex*, bool, int ) const override;
  void RecSummaryObject2Summary( HltObjectSummary::Info*, const LHCb::RecSummary*, int ) const override;
  void GenericMapObject2Summary( HltObjectSummary::Info*, const GaudiUtils::VectorMap<short, float>* ) const override;
  //
  /// Put the information in the summary back in the object
  void ParticleObjectFromSummary( const HltObjectSummary::Info*, LHCb::Particle*, bool, int ) const override;
  void ProtoParticleObjectFromSummary( const HltObjectSummary::Info*, LHCb::ProtoParticle*, bool, int ) const override;
  void TrackObjectFromSummary( const HltObjectSummary::Info*, LHCb::Track*, bool ) const override;
  void RichPIDObjectFromSummary( const HltObjectSummary::Info*, LHCb::RichPID*, bool, int ) const override;
  void MuonPIDObjectFromSummary( const HltObjectSummary::Info*, LHCb::MuonPID*, bool, int ) const override;
  void CaloClusterObjectFromSummary( const HltObjectSummary::Info*, LHCb::CaloCluster*, bool, int ) const override;
  void CaloHypoObjectFromSummary( const HltObjectSummary::Info*, LHCb::CaloHypo*, bool, int ) const override;
  void RecVertexObjectFromSummary( const HltObjectSummary::Info*, LHCb::RecVertex*, bool ) const override;
  void VertexObjectFromSummary( const HltObjectSummary::Info*, LHCb::Vertex*, bool, int ) const override;
  void RecSummaryObjectFromSummary( const HltObjectSummary::Info*, LHCb::RecSummary*, int ) const override;
  void GenericMapObjectFromSummary( const HltObjectSummary::Info*,
                                    GaudiUtils::VectorMap<short, float>* ) const override;

  int findBestPrevious( const unordered_map<int, unordered_map<string, pair<int, int>>>&, int ) const;

private:
  int m_LatestVersion = 1;

}; // End of class header.

#endif // REPORTCONVERTTOOL_H
