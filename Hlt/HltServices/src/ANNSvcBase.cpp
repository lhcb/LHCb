/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ANNSvcBase.h"
#include <Gaudi/Parsers/Factory.h>
#include <fmt/format.h>

namespace ANNSvcBase_details {

  int index_major( const Gaudi::StringKey& major ) {
    auto i = std::find( s_majors.begin(), s_majors.end(), major );
    return i == s_majors.end() ? -1 : std::distance( s_majors.begin(), i );
  }

  auto reverse( IIndexedANNSvc::map_t const& m ) {
    IIndexedANNSvc::inv_map_t imap;
    for ( auto const& [k, v] : m ) {
      auto [_, ok] = imap.insert( v, k );
      if ( !ok ) throw GaudiException( "unable to invert map", __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }
    return imap;
  }

} // namespace ANNSvcBase_details

StatusCode ANNSvcBase::initialize() {
  // use updateHandler to make sure we are registered in case that is requested
  return extends::initialize().andThen( [&] { updateRegistration(); } );
}

ANNSvcBase::~ANNSvcBase() {
  if ( m_monitoringHub ) m_monitoringHub->removeEntity( *this );
}

IIndexedANNSvc::map_t const& ANNSvcBase::i2s( unsigned int index, const Gaudi::StringKey& major ) const {
  auto i = ANNSvcBase_details::index_major( major );
  if ( i < 0 ) throw GaudiException{ "bad major key " + major, __PRETTY_FUNCTION__, StatusCode::FAILURE };
  auto ptr = m_maps[i].with_lock( [&]( std::map<unsigned int, map_t>& map ) -> map_t const* {
    auto j = map.find( index );
    if ( j != map.end() ) return &j->second;
    auto [k, ok] = map.emplace( index, fetch( index, major ) );
    return ok ? &k->second : nullptr;
  } );
  if ( !ptr ) throw GaudiException{ "Failed to add to map", __PRETTY_FUNCTION__, StatusCode::FAILURE };
  return *ptr;
}

IIndexedANNSvc::inv_map_t const& ANNSvcBase::s2i( unsigned int index, const Gaudi::StringKey& major ) const {
  auto i = ANNSvcBase_details::index_major( major );
  if ( i < 0 ) throw GaudiException{ "bad major key " + major, __PRETTY_FUNCTION__, StatusCode::FAILURE };
  auto ptr = m_inv_maps[i].with_lock( [&]( std::map<unsigned int, inv_map_t>& imap ) -> inv_map_t const* {
    auto j = imap.find( index );
    if ( j != imap.end() ) return &j->second;
    auto [k, ok] = imap.emplace( index, ANNSvcBase_details::reverse( this->i2s( index, major ) ) );
    return ok ? &k->second : nullptr;
  } );
  if ( !ptr ) throw GaudiException{ "Failed to add to map", __PRETTY_FUNCTION__, StatusCode::FAILURE };
  return *ptr;
}

ANNSvcBase::lumi_schema_t const& ANNSvcBase::lumiCounters( unsigned key, unsigned version ) const {
  auto ptr = m_lumi.with_lock( [&]( std::map<std::pair<unsigned, unsigned>, lumi_schema_t>& lumi ) {
    auto j = lumi.find( { key, version } );
    if ( j != lumi.end() ) return &j->second;
    auto [k, ok] = lumi.emplace( std::pair{ key, version }, fetch_lumi( key, version ) );
    return ok ? &k->second : nullptr;
  } );
  if ( !ptr ) throw GaudiException{ "Failed to add to lumi schema map", __PRETTY_FUNCTION__, StatusCode::FAILURE };
  return *ptr;
}

// implement methods to support use as Entity
void to_json( nlohmann::json& json, ANNSvcBase const& that ) {
  // start with the explicitly requested keys
  std::set<unsigned int> keys = that.m_default_publish.value();
  // now add all the keys needed at runtime in this job ...
  for ( const auto& map : that.m_maps ) {
    map.with_lock( [&keys]( const auto& m ) {
      for ( const auto& [k, _] : m ) keys.insert( k );
    } );
  }
  // TODO: will this ever add anything, given that the inverse maps are
  //       (always?) generated from the non-inverse ones? if so anything here
  //       has already been added above
  for ( const auto& map : that.m_inv_maps ) {
    map.with_lock( [&keys]( const auto& m ) {
      for ( const auto& [k, _] : m ) keys.insert( k );
    } );
  }
  // TODO: should this be a dictionary, or should it be a plain text string which
  // happens to be a JSON representation of a dictionary,
  // so that the checksum of the string is the key???
  // Best to first implement the read-back side, and then see what
  // requires the least amount of code...
  for ( unsigned int k : keys ) { json.emplace( fmt::format( "0x{:08x}", k ), that.generate_json( k ) ); }
  if ( that.msgLevel( MSG::DEBUG ) ) {
    that.debug() << "publishing the following JSON to monitoring hub: " << json << endmsg;
  }
}

void ANNSvcBase::updateRegistration() {
  if ( m_register_with_monitoring_hub && !m_monitoringHub ) {
    m_monitoringHub = &Gaudi::svcLocator()->monitoringHub();
    m_monitoringHub->registerEntity( name(), "DecodingKeys", "DecodingKeys", *this );
  } else if ( !m_register_with_monitoring_hub && m_monitoringHub ) {
    m_monitoringHub->removeEntity( *this );
    m_monitoringHub = nullptr;
  }
}
