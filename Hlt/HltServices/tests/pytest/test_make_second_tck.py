###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest


@pytest.mark.ctest_fixture_required("hltservices.assign_first_tck")
@pytest.mark.ctest_fixture_setup("hltservices.make_second_tck")
@pytest.mark.shared_cwd("HltServices")
class Test(LHCbExeTest):
    command = ["python", "../options/make_tck.py", "0.5"]
    reference = {"messages_count": {"FATAL": 0, "ERROR": 0, "WARNING": 2}}

    # Author: Roel Aaij
    # Purpose: Make the TCK needed to test the closing of the config.cdb file

    def test_config_created(self, stdout):
        assert b"HltGenConfig         INFO Generating config for TestSequence" in stdout

    def test_stdout(self, stdout: bytes):
        assert b"PASSED" in stdout
