###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest


@pytest.mark.ctest_fixture_required("hltservices.make_first_tck")
@pytest.mark.ctest_fixture_setup("hltservices.assign_first_tck")
@pytest.mark.shared_cwd("HltServices")
class Test(LHCbExeTest):
    command = ["python", "../options/assign_tck.py", "0x11291600"]
    reference = {"messages_count": {"FATAL": 0, "ERROR": 0, "WARNING": 0}}

    # Author: Roel Aaij

    def test_config_created(self, stdout):
        assert (
            b"ConfigCDBAccessSvc   INFO  opening TCKData/config.cdb in mode ReadWrite\n"
            b"ConfigCDBAccessSvc   INFO  created AL/TCK/0x11291600"
        ) in stdout

    def test_stdout(self, stdout: bytes):
        assert b"PASSED" in stdout
