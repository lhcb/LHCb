/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CFNodePropertiesParse.h"
#include "GaudiKernel/SerializeSTL.h"
namespace std {
  ostream& operator<<( ostream& s, nodeType m ) { return s << toString( m ); }

  ostream& operator<<( ostream& s, NodeDefinition const& m ) {
    s << "[" << m.name << ", " << m.type << ", ";
    GaudiUtils::details::ostream_joiner( s << "[", m.children, ", " ) << "], ";
    return s << m.ordered << "]";
  }
} // namespace std
