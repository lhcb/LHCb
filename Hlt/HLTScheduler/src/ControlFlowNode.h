/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// @author Niklas Nolte, Katya Govorkova
#pragma once
#include "CFNodeType.h"
#include "GaudiAlg/FunctionalDetails.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/AppReturnCode.h"
#include "GaudiKernel/FunctionalFilterDecision.h"
#include "GaudiKernel/MsgStream.h"
#include "Kernel/Chrono.h"
#include "Kernel/IOHandler.h"
#include "Kernel/ISchedulerConfiguration.h"
#include "Kernel/STLExtensions.h"
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <gsl/pointers>
#include <iomanip>
#include <iostream>
#include <set>
#include <sstream>
#include <stdlib.h>
#include <thread>
#include <time.h>
#include <variant>
#include <vector>

using NodeState = LHCb::Interfaces::ISchedulerConfiguration::State::NodeState;
using AlgState  = LHCb::Interfaces::ISchedulerConfiguration::State::AlgState;

struct AlgWrapper {
  using Algorithm = Gaudi::Algorithm;
  Algorithm*            m_alg{ nullptr };
  uint16_t              m_executedIndex{};
  bool                  m_callSysExecute{};
  static constexpr bool s_trace{ false };

  AlgWrapper( gsl::not_null<Algorithm*> algo, uint16_t index, bool callSysExecute )
      : m_alg( algo ), m_executedIndex( index ), m_callSysExecute( callSysExecute ) {}

  bool isExecuted( LHCb::span<AlgState const> AlgoStates ) const { return AlgoStates[m_executedIndex].isExecuted; }

  bool getFilterPassed( LHCb::span<AlgState const> AlgoStates ) const {
    return AlgoStates[m_executedIndex].filterPassed;
  }

  void execute( EventContext& evtCtx, LHCb::span<AlgState> AlgoStates ) const {
    m_alg->whiteboard()->selectStore( evtCtx.valid() ? evtCtx.slot() : 0 ).ignore();

    if ( s_trace ) m_alg->always() << ">> AlgWrapper: executing " << m_alg->name() << endmsg;
    auto ret = m_callSysExecute ? m_alg->sysExecute( evtCtx ) : m_alg->execute( evtCtx );
    if ( s_trace ) m_alg->always() << "<< AlgWrapper: executed " << m_alg->name() << endmsg;
    // Special case of end of input, throw a dedicated exception
    if ( ret == LHCb::IO::TaskOutput::ENDOFINPUT ) throw LHCb::IO::EndOfInput{};

    bool filterpassed = [this, ret, &evtCtx] {
      if ( ret == Gaudi::Functional::FilterDecision::PASSED ) return true;
      if ( ret == Gaudi::Functional::FilterDecision::FAILED ) return false;
      if ( ret == StatusCode::SUCCESS ) return m_alg->execState( evtCtx ).filterPassed();
      throw GaudiException( "Error in algorithm execute", m_alg->name(), ret );
    }();

    AlgoStates[m_executedIndex] = { true, filterpassed };
  }

  std::string const& name() const { return m_alg->name(); }
};

class VNode;
bool requested( LHCb::span<gsl::not_null<VNode*> const> nodes, LHCb::span<NodeState const> nodeStates );
void notify( int id, LHCb::span<gsl::not_null<VNode*> const> nodes, LHCb::span<NodeState> nodeStates );

// This is the BasicNode implementation, which is a wrapper around
// Gaudi::Functional::Algorithm's that shall be scheduled. It contains a pointer
// to its Gaudi::Functional::Algorithm as member, and moreover implements the
// functionality to be scheduled correctly
class BasicNode final {
  std::string m_name;        // should be the same as the Gaudi::Algorithm name
  int         m_NodeID = -1; // this is the m_NodeID of the node in a list of all nodes that will be created,
                             // to correctly access a state vector
  std::vector<gsl::not_null<VNode*>> m_parents;
  std::vector<AlgWrapper>            m_RequiredAlgs; // the last element in the vector is the TopAlg itself

public:
  BasicNode( std::string const& name ) : m_name( name ) {}

  int        id() const { return m_NodeID; }
  BasicNode& setId( int id ) {
    m_NodeID = id;
    return *this;
  }

  auto const& name() const { return m_name; }

  auto&       parents() { return m_parents; }
  auto const& parents() const { return m_parents; }

  auto&       requiredAlgs() { return m_RequiredAlgs; }
  auto const& requiredAlgs() const { return m_RequiredAlgs; }

  [[nodiscard]] bool
  execute( LHCb::span<NodeState> NodeStates, LHCb::span<AlgState> AlgStates,
           std::deque<Gaudi::Accumulators::AveragingCounter<LHCb::chrono::fast_clock::duration>>& TimingCounters,
           EventContext& evtCtx, IAlgExecStateSvc* aess ) const {
    assert( aess != nullptr );

    // first, execute the required algorithms
    try {
      for ( AlgWrapper const& requiredAlg : m_RequiredAlgs ) {
        if ( !requiredAlg.isExecuted( AlgStates ) ) {
          // if one can guarantee, that every TopAlg is a data consumer, we could omit
          // the isExecuted call for the last element of m_RequiredAlgs
          const auto start = LHCb::chrono::fast_clock::now();
          requiredAlg.execute( evtCtx, AlgStates );
          TimingCounters[requiredAlg.m_executedIndex] += LHCb::chrono::fast_clock::now() - start;
        }
      }
    } catch ( LHCb::IO::EndOfInput const& ) {
      // make sur EndOfInput goes out to the main loop
      throw;
    } catch ( std::exception const& ) {
      aess->updateEventStatus( true, evtCtx );
      return false;
    } catch ( ... ) {
      aess->updateEventStatus( true, evtCtx );
      return false;
    }

    // the last of m_requiredAlgs is our own Algorithm, depending on which we want to set
    // executionCtr and passed flag of this node
    NodeStates[id()].executionCtr--;
    NodeStates[id()].passed = m_RequiredAlgs.back().getFilterPassed( AlgStates );
    return true;
  } // end of execute

}; // end of BasicNode

// This is the implementation of CompositeNodes, like the HLT Line. This gets
// BasicNodes and other CompositeNodes as children and defines a control flow
// for the children. Currently implemented are Nodes for LAZY_AND, executing
// every child until one returns FALSE, and NONLAZY_OR, which sets its state to
// TRUE when one child returns TRUE, but still executes every child.

class CompositeNode final {
  std::string                        m_name;
  int                                m_NodeID = -1;
  std::vector<gsl::not_null<VNode*>> m_parents;
  nodeType                           m_type;
  std::vector<std::string>           m_childrenNames;
  std::vector<gsl::not_null<VNode*>> m_children;
  bool                               m_ordered; // do you care about the order of execution?

public:
  CompositeNode( nodeType nType, std::string name, std::vector<std::string> childrenNames, bool ordered = false )
      : m_name{ std::move( name ) }
      , m_type{ nType }
      , m_childrenNames{ std::move( childrenNames ) }
      , m_ordered{ ordered } {
    assert( !m_childrenNames.empty() );
    // assert( m_type != nodeType::NOT || childrenNames.size() == 1 );
    constexpr auto known =
        std::array{ nodeType::NOT, nodeType::LAZY_AND, nodeType::LAZY_OR, nodeType::NONLAZY_OR, nodeType::NONLAZY_AND };
    if ( std::find( known.begin(), known.end(), nType ) == known.end() ) {
      throw GaudiException( std::string{ "nodeType " }.append( toString( nType ) ).append( " does not exist." ),
                            __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }
  }

  int            id() const { return m_NodeID; }
  CompositeNode& setId( int id ) {
    m_NodeID = id;
    return *this;
  }

  auto&       parents() { return m_parents; }
  auto const& parents() const { return m_parents; }

  auto&       children() { return m_children; }
  auto const& children() const { return m_children; }

  auto const& childrenNames() const { return m_childrenNames; }

  nodeType type() const { return m_type; }

  const auto& name() const { return m_name; }

  // returns all edges, meaning control-flow dependencies of the
  // ControlFlowNode. This is needed to schedule execution in the right order...
  std::vector<std::pair<gsl::not_null<VNode*>, gsl::not_null<VNode*>>> Edges() const {
    if ( m_children.empty() || !m_ordered ) {
      return {};
    } else {
      std::vector<std::pair<gsl::not_null<VNode*>, gsl::not_null<VNode*>>> edges;
      edges.reserve( m_children.size() - 1 );
      std::transform( std::next( std::begin( m_children ) ), std::end( m_children ), std::begin( m_children ),
                      std::back_inserter( edges ), []( const auto& second, const auto& first ) {
                        return std::pair{ first, second };
                      } );
      return edges;
    }
  }

  // this should update the passed and executionCtr flags after each loop
  // if the return value is true, the state has changed and the parent should
  // be notified
  bool updateState( nodeType type, int senderNodeID, LHCb::span<NodeState> NodeStates ) const;

}; // end of class CompositeNode

class VNode {
  std::variant<BasicNode, CompositeNode> m_data;

  // define the overloaded struct to be able to properly use std::visit to get the
  // right function depending on the input type see cppreference of std::visit
  template <class... Ts>
  struct overload : Ts... {
    using Ts::operator()...;
  };
  template <class... Ts>
  overload( Ts... ) -> overload<Ts...>;

public:
  template <typename... Args>
  VNode( Args&&... args ) : m_data{ std::forward<Args>( args )... } {}

  template <typename... F>
  auto visit( F&&... f ) const {
    return std::visit( overload{ std::forward<F>( f )... }, m_data );
  }

  template <typename... F>
  auto visit( F&&... f ) {
    return std::visit( overload{ std::forward<F>( f )... }, m_data );
  }

  bool passed( LHCb::span<const NodeState> NodeStates ) const {
    return std::visit( [&]( auto const& node ) { return NodeStates[node.id()].passed; }, m_data );
  }

  bool isActive( LHCb::span<const NodeState> NodeStates ) const {
    return visit(
        [&]( CompositeNode const& node ) {
          return NodeStates[node.id()].executionCtr != 0 && requested( node.parents(), NodeStates );
        },
        []( BasicNode const& ) { return false; } );
  }

  const std::string& name() const {
    return std::visit( []( auto const& node ) -> std::string const& { return node.name(); }, m_data );
  }

  template <typename F, typename = std::enable_if_t<std::is_invocable_v<F, VNode&>>>
  void walk_node_and_children( F f ) {
    std::invoke( f, *this );
    visit(
        [&]( CompositeNode& cn ) {
          for ( gsl::not_null<VNode*> child : cn.children() ) child->walk_node_and_children( f );
        },
        []( BasicNode& ) {} );
  }

  template <typename F, typename = std::enable_if_t<std::is_invocable_v<F, VNode const&>>>
  void walk_node_and_children( F f ) const {
    std::invoke( f, *this );
    visit(
        [&]( CompositeNode const& cn ) {
          for ( gsl::not_null<VNode const*> child : cn.children() ) child->walk_node_and_children( f );
        },
        []( BasicNode const& ) {} );
  }
};

// requesting recursion: implemented in requested(), isActive()
// checks whether the ControlFlowNode that calls it is requested. To do that,
// it checks whether any parent is active (parent->isActive()). The parents
// themselves go and check again, whether they are requested themselves if
// they are not already executed. This goes recursively until we reach reach
// the highest ControlFlowNode (returning true) or until we reach some not
// requested or already executed ControlFlowNode (returns false). If we went
// all the way up to the highest ControlFlowNode, we ask: is this node active?
// If yes, requested returns true for the second highest ControlFlowNode(s)
// and we continue to resolve the recursion, asking each composite
// ControlFlowNode if it is active. If any of the parents of the basic
// ControlFlowNode, it will be executed.
inline bool requested( LHCb::span<gsl::not_null<VNode*> const> nodes, LHCb::span<NodeState const> nodeStates ) {
  return nodes.empty() ||
         std::any_of( begin( nodes ), end( nodes ), [&]( VNode const* n ) { return n->isActive( nodeStates ); } );
}

// this calls updateState for all parents of the ControlFlowNode that called notify.
// If an updateState invocation updated the state, then recursively call notify on the parents
inline void notify( int senderID, LHCb::span<gsl::not_null<VNode*> const> nodes, LHCb::span<NodeState> nodeStates ) {
  for ( gsl::not_null<VNode*> parent : nodes ) {
    parent->visit(
        [&]( CompositeNode& node ) {
          if ( nodeStates[node.id()].executionCtr != 0 && node.updateState( node.type(), senderID, nodeStates ) ) {
            notify( node.id(), node.parents(), nodeStates );
          }
        },
        []( BasicNode& ) {} );
  }
}

// returns all basic nodes reachable from vnode
std::set<gsl::not_null<VNode*>> reachableBasics( gsl::not_null<VNode*> vnode );
// returns all composite nodes reachable from vnode
std::set<gsl::not_null<VNode*>> reachableComposites( gsl::not_null<VNode*> vnode );
/// Returns all BasicNode prerequisites for each BasicNode reachable from vnode
std::map<gsl::not_null<VNode*>, std::set<gsl::not_null<VNode*>>>
findAllEdges( gsl::not_null<VNode const*> vnode, std::set<std::array<gsl::not_null<VNode*>, 2>> const& custom_edges );
// converts the children given as strings to a node into pointers to the right instances
void childrenNamesToPointers( std::map<std::string, VNode>& allNodes );
// checks whether all control flow dependencies are met for `nodeToCheck`
bool CFDependenciesMet( gsl::not_null<VNode*>                                                   nodeToCheck,
                        std::map<gsl::not_null<VNode*>, std::set<gsl::not_null<VNode*>>> const& nodePrerequisites,
                        std::set<gsl::not_null<VNode*>> const&                                  alreadyOrdered );
/// Return a sequence of basic nodes respecting the edge constraints
std::vector<gsl::not_null<BasicNode*>>
resolveDependencies( std::set<gsl::not_null<VNode*>>&                                        unordered,
                     std::map<gsl::not_null<VNode*>, std::set<gsl::not_null<VNode*>>> const& nodePrerequisites );
// based on the child-pointers in a composite node, supply the children with parent pointers
void addParentsToAllNodes( std::set<gsl::not_null<VNode*>> const& composites );
