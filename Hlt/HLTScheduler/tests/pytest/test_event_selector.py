###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import SortGroupOfLines


@pytest.mark.ctest_fixture_required("hltscheduler.write")
@pytest.mark.shared_cwd("HLTScheduler")
class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v", "../options/event_selector.py"]
    reference = "../refs/event_selector.yaml"
    environment = ["GAUDIAPPNAME=", "GAUDIAPPVERSION="]

    preprocessor = LHCbExeTest.preprocessor + SortGroupOfLines(
        r".*INFO Event \d+ Collision number \d+"
    )
