###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v", "../options/testAlgFailure.py"]
    returncode = 3

    def test_event_failed_prompt(self, stdout: bytes):
        assert b"FATAL Event failed in Node consumer1" in stdout, (
            "missing fatal message from consumer1."
        )

    def test_event_failed_prompt(self, stdout: bytes):
        assert b"FATAL Event failed in Node consumer2" not in stdout, (
            "consumer2 should not execute."
        )
