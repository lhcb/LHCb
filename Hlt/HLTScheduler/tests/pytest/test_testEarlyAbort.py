###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest

# from LHCbTesting.pytest.fixtures import counters


class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v", "../options/testEarlyAbort.py"]
    returncode = 3

    def test_number_of_processed_events(self, counters: dict):
        assert counters["HLTControlFlowMgr"]["Processed events"]["count"] <= 6, (
            "Early abort did not work correctly"
        )
