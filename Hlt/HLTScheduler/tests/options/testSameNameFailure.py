###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# with this file we test that having the same name for a compositecf node fails
# in the initialization part of the scheduler
from Configurables import (
    ConfigurableDummy,
    HiveDataBrokerSvc,
    HiveWhiteBoard,
    HLTControlFlowMgr,
)
from Gaudi.Configuration import *

a1 = ConfigurableDummy("A1")
a1.outKeys = ["/Event/a1"]
a1.CFD = 1

HLTControlFlowMgr().CompositeCFNodes = [
    ("moore", "NONLAZY_AND", ["line1"], False),
    ("line1", "LAZY_OR", ["A1"], True),
    ("line1", "LAZY_AND", ["A1"], True),
]

HLTControlFlowMgr().OutputLevel = VERBOSE
HiveDataBrokerSvc().OutputLevel = DEBUG

whiteboard = HiveWhiteBoard("EventDataSvc", EventSlots=1)

app = ApplicationMgr(
    EvtMax=-1,
    EvtSel="NONE",
    ExtSvc=[whiteboard],
    EventLoop=HLTControlFlowMgr(),
    TopAlg=[a1],
)

HiveDataBrokerSvc().DataProducers = app.TopAlg
