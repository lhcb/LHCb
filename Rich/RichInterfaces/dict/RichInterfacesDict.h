/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DICT_RICHKERNELDICT_H
#define DICT_RICHKERNELDICT_H 1

// Interfaces
#include "RichInterfaces/IRichParticleProperties.h"
#include "RichInterfaces/IRichSmartIDTool.h"
#include "RichInterfaces/IRichToolRegistry.h"

#endif // DICT_RICHKERNELDICT_H
