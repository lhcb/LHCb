/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichFutureMCUtils/RichMCHitUtils.h"

using namespace Rich::Future::MC::Relations;

MCHitUtils::MCHitUtils( const LHCb::MCRichHits& mchits ) {

  // Loop over MC hits
  for ( const auto hit : mchits ) {
    if ( hit ) {
      // Build the mappings to MCHits

      // Get the pixel level ID
      const auto pixelID = hit->sensDetID().pixelID().channelDataOnly();
      // append hit to list for this ID
      m_smartIDsToHits[pixelID].emplace_back( hit );

      // Add another entry for the PD ID
      const auto hpdID = pixelID.pdID();
      m_smartIDsToHits[hpdID].emplace_back( hit );

      // Get the MCP for this hit and add to reverse mapping
      const auto mcP = hit->mcParticle();
      if ( mcP ) { m_MCPsToHits[mcP].emplace_back( hit ); }
    }
  }
}
