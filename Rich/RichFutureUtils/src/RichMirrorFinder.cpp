/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichFutureUtils/RichMirrorFinder.h"

using namespace Rich::Utils;

namespace {

  // Returns the side for a given mirror and Rich Detector
  template <typename MIRROR>
  inline Rich::Side side( const MIRROR*            mirror, //
                          const Rich::DetectorType rich ) noexcept {
    return ( Rich::Rich1 == rich                ? mirror->mirrorCentre().y() > 0.0 ? Rich::top : Rich::bottom
             : mirror->mirrorCentre().x() > 0.0 ? Rich::left
                                                : Rich::right );
  }

} // namespace

// Constructor from dependent detector elements
MirrorFinder::MirrorFinder( const Detector::Rich1& rich1, //
                            const Detector::Rich2& rich2 )
    : m_sphMirrFinder( std::make_unique<PrimFinder>() ) //
    , m_secMirrFinder( std::make_unique<SecFinder>() ) {

  // add the mirrors to each finder
  auto addMirrors = [&]( auto& derich ) {
    // primary
    for ( auto& m : derich.primaryMirrors() ) {
      m_sphMirrFinder->addMirror( derich.rich(), side( m.get(), derich.rich() ), m.get() );
      m_ownedMirrors.emplace_back( m );
    }
    // secondary
    for ( auto& m : derich.secondaryMirrors() ) {
      m_secMirrFinder->addMirror( derich.rich(), side( m.get(), derich.rich() ), m.get() );
      m_ownedMirrors.emplace_back( m );
    }
  };
  addMirrors( rich1 );
  addMirrors( rich2 );

  // initialise the finders
  m_sphMirrFinder->init();
  m_secMirrFinder->init();
}
