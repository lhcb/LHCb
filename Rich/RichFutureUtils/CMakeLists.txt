###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichFutureUtils
--------------------
#]=======================================================================]

gaudi_add_library(RichFutureUtils
    SOURCES
        src/RichGeomPhoton.cpp
        src/RichMirrorFinder.cpp
        src/RichRayTracing.cpp
        src/RichSIMDGeomPhoton.cpp
        src/RichSIMDMirrorData.cpp
        src/RichSmartIDs.cpp
        src/RichTabulatedRefIndex.cpp
        src/RichDecodedData.cpp
    LINK
        PUBLIC
            Boost::headers
            Gaudi::GaudiKernel
            LHCb::DetDescLib
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::RichDetectorsLib
            LHCb::RichDetLib
            LHCb::RichUtils
            LHCb::RichInterfaces
            Vc::Vc
)

gaudi_add_dictionary(RichFutureUtilsDict
    HEADERFILES dict/RichFutureUtilsDict.h
    SELECTION dict/RichFutureUtilsDict.xml
    LINK LHCb::RichFutureUtils
    OPTIONS ${LHCB_DICT_GEN_DEFAULT_OPTS}
)
