
/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "GaudiKernel/Kernel.h"
#include <utility>

namespace {
  template <typename OS, typename Arg>
  inline void rich_message( OS& os, Arg&& arg ) {
    os << arg;
  }
  template <typename OS, typename Arg, typename... Args>
  inline void rich_message( OS& os, Arg&& arg, Args&&... args ) {
    os << arg;
    rich_message( os, std::forward<Args>( args )... );
  }
} // namespace

// Some defines for debug/verbose messages...
// Note macros below assume code using them defines the 'messenger()' method that returns a
// pointer to the entity that implements the Gaudi based messaging API.
#ifndef _ri_debug
#  define _ri_debug                                                                                                    \
    if ( messenger()->msgLevel( MSG::DEBUG ) ) messenger()->debug()
#endif
#ifndef _ri_info
#  define _ri_info                                                                                                     \
    if ( messenger()->msgLevel( MSG::INFO ) ) messenger()->info()
#endif
#ifndef _ri_verbo
#  define _ri_verbo                                                                                                    \
    if ( messenger()->msgLevel( MSG::VERBOSE ) ) messenger()->verbose()
#endif
#ifndef _ri_warning
#  define _ri_warning messenger()->warning()
#endif
#ifndef _ri_error
#  define _ri_error messenger()->error()
#endif
#ifndef ri_message
#  define ri_message( level, ... )                                                                                     \
    if ( messenger()->msgLevel( level ) ) { rich_message( messenger()->msgStream( level ), __VA_ARGS__ ); }
#  define ri_info( ... ) ri_message( MSG::INFO, __VA_ARGS__ )
#  define ri_debug( ... ) ri_message( MSG::DEBUG, __VA_ARGS__ )
#  define ri_verbo( ... ) ri_message( MSG::VERBOSE, __VA_ARGS__ )
#  define ri_warning( ... ) ri_message( MSG::WARNING, __VA_ARGS__ )
#  define ri_error( ... ) ri_message( MSG::ERROR, __VA_ARGS__ )
#  ifndef NDEBUG
#    define ri_debug_dbgonly( ... ) ri_debug( __VA_ARGS__ )
#    define ri_verbo_dbgonly( ... ) ri_verbo( __VA_ARGS__ )
#  else
#    define ri_debug_dbgonly( ... ) ;
#    define ri_verbo_dbgonly( ... ) ;
#  endif
#endif
