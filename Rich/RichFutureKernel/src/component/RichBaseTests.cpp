/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiKernel/RndmGenerators.h"
#include "LHCbAlgs/Consumer.h"
#include "RichFutureKernel/RichHistoAlgBase.h"
#include <cmath>
#include <cstdint>
#include <type_traits>

namespace Rich::Future::Tests {

  using namespace Rich::Future::Hist;

  template <typename HIST>
  class HistoTestBase final
      : public LHCb::Algorithm::Consumer<void(), Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {
  private:
    // variable type from hist
    using T = param_t<HIST>;
    /// Type for floating point quantities
    using FT = std::conditional_t<std::is_floating_point_v<T>, T, double>;

    /// Function type enum
    enum class Function { Undefined, Normal, Exponential, FlatTop };

  private:
    // Min/Max range. Set according to function type.
    T m_minVar{ 0 };
    T m_maxVar{ 1 };

    /// Function type property
    Gaudi::Property<std::string> m_funcType{ this, "Function", "Normal" };

    /// Function to use
    Function m_func{ Function::Undefined };

    /// test array of (optional) hists.
    /// Note last entry is intentionally uninitialised to test handling this scenario.
    mutable Hist::Array<HIST, 3> m_h;

    /// Random generators
    Rndm::Numbers m_randFunc{};
    Rndm::Numbers m_randCoord{};
    Rndm::Numbers m_randWeight{};

  private:
    // Function specific parameters
    inline FT lambda() const noexcept { return 0.075; }
    inline FT sigma() const noexcept { return 15; }
    inline FT a() const noexcept { return m_minVar + T{ 2 }; }
    inline FT b() const noexcept { return m_maxVar - T{ 2 }; }

  private:
    // Function statistical quantities
    inline FT mean() const noexcept {
      FT m{};
      if ( Function::Normal == m_func ) {
        m = 0.5 * ( m_maxVar + m_minVar );
      } else if ( Function::Exponential == m_func ) {
        m = 1.0 / lambda();
      } else if ( Function::FlatTop == m_func ) {
        m = 0.5 * ( a() + b() );
      }
      return m;
    }
    inline FT skewness() const noexcept { return ( Function::Exponential == m_func ? 2.0 : 0.0 ); }
    inline FT kurtosis() const noexcept {
      FT k = 0.0;
      if ( Function::Exponential == m_func ) {
        k = 6.0;
      } else if ( Function::FlatTop == m_func ) {
        k = -6.0 / 5.0;
      }
      return k;
    }
    inline FT StdDev() const noexcept {
      FT r = 0.0;
      if ( Function::Normal == m_func ) {
        r = sigma();
      } else if ( Function::Exponential == m_func ) {
        r = 1.0 / lambda();
      } else if ( Function::FlatTop == m_func ) {
        r = ( b() - a() ) / std::sqrt( 12.0 );
      }
      return r;
    }

  private:
    // Generators for function arguments and values
    inline T coordinate() const {
      T res{ 0 };
      if constexpr ( is_profile_v<HIST> ) {
        // random in min max range
        if constexpr ( std::is_floating_point_v<T> ) {
          res = m_randCoord.shoot();
        } else {
          res = std::round( m_randCoord.shoot() );
        }
      } else {
        // sample from the given function
        if constexpr ( std::is_floating_point_v<T> ) {
          res = m_randFunc.shoot();
        } else {
          res = std::round( m_randFunc.shoot() );
        }
      }
      debug() << m_funcType.value() << " sample = " << res << endmsg;
      return res;
    }
    inline T func( const T x ) const {
      FT res{ 0 };
      if ( Function::Normal == m_func ) {
        res = std::exp( -0.5 * std::pow( ( x - mean() ) / sigma(), 2 ) );
      } else if ( Function::Exponential == m_func ) {
        res = ( x >= 0 ? ( lambda() * std::exp( -1.0 * lambda() * x ) ) : 0.0 );
      } else if ( Function::FlatTop == m_func ) {
        res = ( x >= a() && x <= b() ? 1.0 : 0.0 );
      }
      T ret{ 0 };
      if constexpr ( std::is_floating_point_v<T> ) {
        ret = res;
      } else {
        ret = T{ 10000000 } * res;
      }
      debug() << m_funcType.value() << "( " << x << ") = " << ret << endmsg;
      return ret;
    }
    inline T weight() const {
      T w{ 0 };
      if constexpr ( std::is_floating_point_v<T> ) {
        w = m_randWeight.shoot();
      } else {
        w = T{ 10 } * m_randWeight.shoot();
      }
      debug() << "Weight = " << w << endmsg;
      return w;
    }

  public:
    HistoTestBase( const std::string& name, ISvcLocator* pSvcLocator ) : Consumer( name, pSvcLocator ) {
      setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins1DHistos", 200 ).ignore();
      setProperty( "NBins2DHistos", 50 ).ignore();
    }

    void operator()() const override {
      std::size_t iFill = 0;
      auto        h_b   = m_h.buffer(); // test buffers
      while ( ++iFill <= 100 ) {
        const auto x  = coordinate();
        const auto y  = ( is_2d_v<HIST> ? coordinate() : 0 );
        const auto fx = ( is_profile_v<HIST> ? func( x ) : 0 );
        const auto fy = ( is_2d_v<HIST> && is_profile_v<HIST> ? func( y ) : 0 );
        const auto wx = ( is_weighted_v<HIST> ? weight() : 0 );
        const auto wy = ( is_2d_v<HIST> && is_weighted_v<HIST> ? weight() : 0 );
        for ( const std::size_t i : { 0, 1 } ) {
          if constexpr ( is_1d_v<HIST> ) {
            if constexpr ( is_weighted_v<HIST> ) {
              if constexpr ( is_profile_v<HIST> ) {
                debug() << "Filling W1DP " << x << " " << fx << " " << wx << endmsg;
                if ( 0 == i ) {
                  m_h[i][x] += { fx, wx };
                } else {
                  h_b[i][x] += { fx, wx };
                }
              } else if constexpr ( is_hist_v<HIST> ) {
                debug() << "Filling W1DH " << x << " " << wx << endmsg;
                if ( 0 == i ) {
                  m_h[i][x] += wx;
                } else {
                  h_b[i][x] += wx;
                }
              }
            } else if constexpr ( is_not_weighted_v<HIST> ) {
              if constexpr ( is_profile_v<HIST> ) {
                debug() << "Filling 1DP " << x << " " << fx << endmsg;
                if ( 0 == i ) {
                  m_h[i][x] += fx;
                } else {
                  h_b[i][x] += fx;
                }
              } else if constexpr ( is_hist_v<HIST> ) {
                debug() << "Filling 1D " << x << endmsg;
                if ( 0 == i ) {
                  ++m_h[i][x];
                } else {
                  ++h_b[i][x];
                }
              }
            }
          } else if constexpr ( is_2d_v<HIST> ) {
            if constexpr ( is_weighted_v<HIST> ) {
              if constexpr ( is_profile_v<HIST> ) {
                debug() << "Filling W2DP " << x << " " << y << " " << fx * fy << " " << wx * wy << endmsg;
                if ( 0 == i ) {
                  m_h[i][{ x, y }] += { fx * fy, wx * wy };
                } else {
                  h_b[i][{ x, y }] += { fx * fy, wx * wy };
                }
              } else if constexpr ( is_hist_v<HIST> ) {
                debug() << "Filling W2DH " << x << " " << y << " " << wx * wy << endmsg;
                if ( 0 == i ) {
                  m_h[i][{ x, y }] += wx * wy;
                } else {
                  h_b[i][{ x, y }] += wx * wy;
                }
              }
            } else if constexpr ( is_not_weighted_v<HIST> ) {
              if constexpr ( is_profile_v<HIST> ) {
                debug() << "Filling 2DP " << x << " " << y << " " << fx * fy << endmsg;
                if ( 0 == i ) {
                  m_h[i][{ x, y }] += fx * fy;
                } else {
                  h_b[i][{ x, y }] += fx * fy;
                }
              } else if constexpr ( is_hist_v<HIST> ) {
                debug() << "Filling 2DH " << x << " " << y << endmsg;
                if ( 0 == i ) {
                  ++m_h[i][{ x, y }];
                } else {
                  ++h_b[i][{ x, y }];
                }
              }
            }
          }
        }
      }
    }

  protected:
    StatusCode prebookHistograms() override {

      bool ok = true;

      if ( m_funcType.value() == "Normal" ) {
        m_func     = Function::Normal;
        m_minVar   = -50;
        m_maxVar   = 100;
        m_randFunc = Rndm::Numbers( randSvc(), Rndm::Gauss( mean(), sigma() ) );
      } else if ( m_funcType.value() == "Exponential" ) {
        m_func     = Function::Exponential;
        m_minVar   = 0;
        m_maxVar   = 200;
        m_randFunc = Rndm::Numbers( randSvc(), Rndm::Exponential( 1.0 / lambda() ) );
      } else if ( m_funcType.value() == "FlatTop" ) {
        m_func     = Function::FlatTop;
        m_minVar   = 0;
        m_maxVar   = 100;
        m_randFunc = Rndm::Numbers( randSvc(), Rndm::Flat( a(), b() ) );
      } else {
        error() << "Unknown function '" << m_funcType.value() << "'" << endmsg;
        return StatusCode::FAILURE;
      }

      if constexpr ( std::is_integral_v<T> ) {
        ok &= ( setProperty( "NBins1DHistos", m_maxVar - m_minVar ).isSuccess() &&
                setProperty( "NBins2DHistos", m_maxVar - m_minVar ).isSuccess() );
      }

      m_randCoord  = Rndm::Numbers( randSvc(), Rndm::Flat( m_minVar, m_maxVar ) );
      m_randWeight = Rndm::Numbers( randSvc(), Rndm::Flat( 0.9, 1.1 ) );

      info() << "1D=" << is_1d_v<HIST> << " 2D=" << is_2d_v<HIST>       //
             << " H=" << is_hist_v<HIST> << " P=" << is_profile_v<HIST> //
             << " W=" << is_weighted_v<HIST> << " NW=" << is_not_weighted_v<HIST> << endmsg;

      info() << "Generating " << m_funcType.value() << " Mean=" << mean() << " StdDev=" << StdDev()
             << " Skewness=" << skewness() << " Kurtosis=" << kurtosis() << endmsg;

      const auto hMin  = m_minVar;
      const auto hMax  = m_maxVar;
      const auto nBins = ( is_1d_v<HIST> ? nBins1D() : nBins2D() );
      info() << "Hist Min/Max/Bins " << hMin << "/" << hMax << "/" << nBins << endmsg;

      if constexpr ( is_1d_v<HIST> ) {
        ok &= initHist( m_h[0ul], HID( "testH1D0" ), m_funcType, //
                        hMin, hMax, nBins, "X", "Entries" );
        ok &= initHist( m_h[1ul], HID( "testH1D1" ), m_funcType, //
                        hMin, hMax, nBins, "X", "Entries" );
      }
      if constexpr ( is_2d_v<HIST> ) {
        ok &= initHist( m_h[0ul], HID( "testH2D0" ), m_funcType, //
                        hMin, hMax, nBins, hMin, hMax, nBins,    //
                        "X", "Y", "Entries" );
        ok &= initHist( m_h[1ul], HID( "testH2D1" ), m_funcType, //
                        hMin, hMax, nBins, hMin, hMax, nBins,    //
                        "X", "Y", "Entries" );
      }
      return StatusCode{ ok };
    }
  };

  DECLARE_COMPONENT_WITH_ID( HistoTestBase<H1D<>>, "RichTestH1DD" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WH1D<>>, "RichTestWH1DD" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<P1D<>>, "RichTestP1DD" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WP1D<>>, "RichTestWP1DD" )

  DECLARE_COMPONENT_WITH_ID( HistoTestBase<H2D<>>, "RichTestH2DD" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WH2D<>>, "RichTestWH2DD" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<P2D<>>, "RichTestP2DD" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WP2D<>>, "RichTestWP2DD" )

  DECLARE_COMPONENT_WITH_ID( HistoTestBase<H1D<float>>, "RichTestH1DF" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WH1D<float>>, "RichTestWH1DF" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<P1D<float>>, "RichTestP1DF" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WP1D<float>>, "RichTestWP1DF" )

  DECLARE_COMPONENT_WITH_ID( HistoTestBase<H2D<float>>, "RichTestH2DF" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WH2D<float>>, "RichTestWH2DF" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<P2D<float>>, "RichTestP2DF" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WP2D<float>>, "RichTestWP2DF" )

  DECLARE_COMPONENT_WITH_ID( HistoTestBase<H1D<std::int64_t>>, "RichTestH1DI" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WH1D<std::int64_t>>, "RichTestWH1DI" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<P1D<std::int64_t>>, "RichTestP1DI" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WP1D<std::int64_t>>, "RichTestWP1DI" )

  DECLARE_COMPONENT_WITH_ID( HistoTestBase<H2D<std::int64_t>>, "RichTestH2DI" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WH2D<std::int64_t>>, "RichTestWH2DI" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<P2D<std::int64_t>>, "RichTestP2DI" )
  DECLARE_COMPONENT_WITH_ID( HistoTestBase<WP2D<std::int64_t>>, "RichTestWP2DI" )

} // namespace Rich::Future::Tests
