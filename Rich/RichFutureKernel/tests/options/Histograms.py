#####################################################################################
# (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations #
#                                                                                   #
# This software is distributed under the terms of the Apache version 2 licence,     #
# copied verbatim in the file "LICENSE".                                            #
#                                                                                   #
# In applying this licence, CERN does not waive the privileges and immunities       #
# granted to it by virtue of its status as an Intergovernmental Organization        #
# or submit itself to any jurisdiction.                                             #
#####################################################################################

from Configurables import (
    FPEAuditor,
    RichTestH1DD,
    RichTestH1DF,
    RichTestH1DI,
    RichTestH2DD,
    RichTestH2DF,
    RichTestH2DI,
    RichTestP1DD,
    RichTestP1DF,
    RichTestP1DI,
    RichTestP2DD,
    RichTestP2DF,
    RichTestP2DI,
    RichTestWH1DD,
    RichTestWH1DF,
    RichTestWH1DI,
    RichTestWH2DD,
    RichTestWH2DF,
    RichTestWH2DI,
    RichTestWP1DD,
    RichTestWP1DF,
    RichTestWP1DI,
    RichTestWP2DD,
    RichTestWP2DF,
    RichTestWP2DI,
)
from Configurables import Gaudi__Histograming__Sink__Root as RootHistoSink
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Gaudi.Configuration import *

# Histo printout should be disabled if either of these is WARNING or above
outLevel = INFO
# outLevel = DEBUG
MessageSvc().OutputLevel = INFO

algs = []

for func in ["Normal", "Exponential", "FlatTop"]:
    algs += [
        # Double
        RichTestH1DD(func + "_H1DD", OutputLevel=outLevel, Function=func),
        RichTestWH1DD(func + "_WH1DD", OutputLevel=outLevel, Function=func),
        RichTestP1DD(func + "_P1DD", OutputLevel=outLevel, Function=func),
        RichTestWP1DD(func + "_WP1DD", OutputLevel=outLevel, Function=func),
        RichTestH2DD(func + "_H2DD", OutputLevel=outLevel, Function=func),
        RichTestWH2DD(func + "_WH2DD", OutputLevel=outLevel, Function=func),
        RichTestP2DD(func + "_P2DD", OutputLevel=outLevel, Function=func),
        RichTestWP2DD(func + "_WP2DD", OutputLevel=outLevel, Function=func),
        # Float
        RichTestH1DF(func + "_H1DF", OutputLevel=outLevel, Function=func),
        RichTestWH1DF(func + "_WH1DF", OutputLevel=outLevel, Function=func),
        RichTestP1DF(func + "_P1DF", OutputLevel=outLevel, Function=func),
        RichTestWP1DF(func + "_WP1DF", OutputLevel=outLevel, Function=func),
        RichTestH2DF(func + "_H2DF", OutputLevel=outLevel, Function=func),
        RichTestWH2DF(func + "_WH2DF", OutputLevel=outLevel, Function=func),
        RichTestP2DF(func + "_P2DF", OutputLevel=outLevel, Function=func),
        RichTestWP2DF(func + "_WP2DF", OutputLevel=outLevel, Function=func),
        # Ints
        RichTestH1DI(func + "_H1DI", OutputLevel=outLevel, Function=func),
        RichTestWH1DI(func + "_WH1DI", OutputLevel=outLevel, Function=func),
        RichTestP1DI(func + "_P1DI", OutputLevel=outLevel, Function=func),
        RichTestWP1DI(func + "_WP1DI", OutputLevel=outLevel, Function=func),
        RichTestH2DI(func + "_H2DI", OutputLevel=outLevel, Function=func),
        RichTestWH2DI(func + "_WH2DI", OutputLevel=outLevel, Function=func),
        RichTestP2DI(func + "_P2DI", OutputLevel=outLevel, Function=func),
        RichTestWP2DI(func + "_WP2DI", OutputLevel=outLevel, Function=func),
    ]

AuditorSvc().Auditors += ["FPEAuditor"]
FPEAuditor().EnableGlobal = True

app = ApplicationMgr(
    EvtMax=10000,
    EvtSel="NONE",
    HistogramPersistency="ROOT",
    TopAlg=algs,
    ExtSvc=[AuditorSvc(), MessageSvcSink(), RootHistoSink()],
    AuditAlgorithms=True,
)
