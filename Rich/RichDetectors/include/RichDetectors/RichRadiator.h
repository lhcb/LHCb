/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SystemOfUnits.h"

// Detector description
#ifdef USE_DD4HEP
#  include "Detector/Rich/DeRichRadiator.h"
#else
#  include "RichDet/DeRichRadiator.h"
#endif

// LHCbMath
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/SIMDTypes.h"

// Utils
#include "RichUtils/RichSIMDTypes.h"

// local
#include "RichDetectors/Utilities.h"

// eventually should be moved elsewhere
#include "RichDet/Rich1DTabProperty.h"

// STL
#include <array>
#include <cassert>
#include <memory>
#include <string>

namespace Rich::Detector {

  namespace details {

    /// base class for all radiators
    template <typename DETELEM>
    class alignas( LHCb::SIMD::VectorAlignment ) Radiator {

    public:
      // types

      // expose underlying detector element type
      using DetElem = DETELEM;

    public:
      // constructors

#ifdef USE_DD4HEP
      /// Constructor from DD4HEP
      template <typename DET>
      Radiator( const DET& rad )
          : m_radiatorID( rad.radiatorID() ) //
          , m_rich( rad.rich() )             //
          , m_radiator( rad.access() )
          , m_refIndex( std::make_shared<const Rich::TabulatedFunction1D>( rad.GasRefIndex() ) )
          , m_refScaleF( rad.CurrentRefIndexScaleFactor() ) {}
#else
      /// Constructor from DetDesc
      template <typename DET>
      Radiator( const DET& rad )
          : m_radiatorID( rad.radiatorID() ) //
          , m_rich( rad.rich() )             //
          , m_radiator( &rad )               //
          , m_refIndex( rad.refIndex() )     //
          , m_refScaleF( rad.CurrentRefIndexScaleFactor() ) {}
#endif

    private:
      /// Get access to the underlying object
      inline auto get() const noexcept { return m_radiator; }

    public:
      // accessors

      /// The radiator ID
      inline auto radiatorID() const noexcept { return m_radiatorID; }

      /// The RICH type
      inline auto rich() const noexcept { return m_rich; }

      /// refractive index interpolator
      inline auto refIndex() const noexcept { return m_refIndex.get(); }

      /// refractive index for given photon energy
      template <typename TYPE>
      inline auto refractiveIndex( const TYPE energy ) const noexcept {
        assert( refIndex() );
        return refIndex()->value( energy * Gaudi::Units::eV );
      }

      /// Current scale factor
      inline auto currentRefIndexScaleFactor() const noexcept { return m_refScaleF; }

    public:
      // methods forwarded to the underlying Det Elem

      FORWARD_TO_DET_OBJ( intersectionPoints )
      FORWARD_TO_DET_OBJ( isInside )

    protected:
      /// messaging
      void fillStream( std::ostream& s ) const {
        s << "[ " << rich() << " " << radiatorID()           //
          << " refIndex=" << *refIndex()                     //
          << " scaleFactor=" << currentRefIndexScaleFactor() //
          << " ]";
      }

    private:
      // data

      /// The Radiator ID
      Rich::RadiatorType m_radiatorID = Rich::InvalidRadiator;

      /// The Rich detector of this radiator
      Rich::DetectorType m_rich = Rich::InvalidDetector;

      /// Detector panel object
      const DETELEM* m_radiator = nullptr;

      /// Refractive index
      std::shared_ptr<const Rich::TabulatedFunction1D> m_refIndex;

      /// Current refractive index scale factor
      double m_refScaleF{ 1.0 };
    };

    /// templated class for each top level Rich Gas object
    template <Rich::RadiatorType GAS,
              // typename RICH,
              typename DETELEM, typename BASEDETELEM>
    class RichXGas : public Radiator<BASEDETELEM> {

    public:
      // types

      // expose base class types
      using DetElem     = DETELEM;
      using BaseDetElem = BASEDETELEM;
      using Base        = Radiator<BASEDETELEM>;

    public:
      // constructors

      /// Constructor from Det Elem
      template <typename RAD>
      RichXGas( const RAD& rad ) : Base( rad ) {}

    private:
      // messaging

      /// name string
      static constexpr auto MyName() noexcept {
        return ( GAS == Rich::Rich1Gas ? "Rich::Detector::Rich1Gas" : //
                     GAS == Rich::Rich2Gas ? "Rich::Detector::Rich2Gas"
                                           : "UNDEFINED" );
      }

    public:
      // messaging

      /// Overload ostream operator
      friend inline std::ostream& operator<<( std::ostream&                                        s,
                                              const RichXGas<GAS, /*RICH,*/ DETELEM, BASEDETELEM>& r ) {
        s << MyName() << " ";
        r.fillStream( s );
        return s;
      }
    };

  } // namespace details

#ifdef USE_DD4HEP
  using Radiator = details::Radiator<LHCb::Detector::detail::DeRichRadiatorObject>;
#else
  using Radiator = details::Radiator<DeRichRadiator>;
#endif

} // namespace Rich::Detector
