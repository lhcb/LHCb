###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import FPEAuditor
from DDDB.CheckDD4Hep import UseDD4Hep

from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    default_raw_banks,
)
from PyConf.control_flow import CompositeNode


def _setupTest(options, evt_max=1, extraAuditors=[]):
    options.evt_max = evt_max
    options.auditors = [FPEAuditor(ActivateAt=["Execute"])] + extraAuditors
    config = configure_input(options)
    appMgr = config["ApplicationMgr/ApplicationMgr"]
    appMgr.AuditAlgorithms = True
    if UseDD4Hep:
        dd4hepSvc = config[
            "LHCb::Det::LbDD4hep::DD4hepSvc/LHCb::Det::LbDD4hep::DD4hepSvc"
        ]
        dd4hepSvc.DetectorList = ["/world", "Rich1", "Rich2"]
    return config


def _runDataTest(options):
    from PyConf.Algorithms import Rich__Future__RawBankDecoder as RichDecoder
    from PyConf.Algorithms import Rich__Future__TestDecodeAndIDs as TestDecodeAndIDs

    config = _setupTest(options)
    rawbanks = default_raw_banks("Rich")
    richDecoded = RichDecoder(
        name="RichFutureDecode", RawBanks=rawbanks
    ).DecodedDataLocation
    testDecode = TestDecodeAndIDs(
        name="TestDecodeAndIDs", DecodedDataLocation=richDecoded
    )
    config.update(configure(options, CompositeNode("All", children=[testDecode])))


def _runInstanciateTest(options):
    from PyConf.Algorithms import Rich__Future__TestConds as TestConds
    from PyConf.Algorithms import (
        Rich__Future__TestDerivedDetObjects as TestDerivedDetObjects,
    )

    config = _setupTest(options)
    testDeriv = TestDerivedDetObjects(name="TestDerivedDets", OutputLevel=1)
    testCond = TestConds(name="TestConds", OutputLevel=1)
    config.update(
        configure(options, CompositeNode("All", children=[testDeriv, testCond]))
    )


def _runRadInterTest(options):
    from PyConf.Algorithms import (
        Rich__Future__TestRadiatorIntersections as TestRadInters,
    )

    config = _setupTest(options)
    testRad = TestRadInters(name="TestRadIntersects", OutputLevel=1)
    config.update(configure(options, CompositeNode("All", children=[testRad])))


def _runMultipleRunsTest(options):
    from PyConf.Algorithms import (
        Rich__Future__TestDerivedDetObjects as TestDerivedDetObjects,
    )
    from PyConf.Algorithms import Rich__Future__TestDerivedElem as TestDerivedElem

    options.msg_svc_format = "% F%40W%S%7W%R%T %0W%M"
    config = _setupTest(options, evt_max=-1)
    testDerivElem = TestDerivedElem(name="TestDerivedElem")
    testDerivObj = TestDerivedDetObjects(name="TestDerivedDetObjects")
    nconfig = configure(
        options, CompositeNode("All", children=[testDerivElem, testDerivObj])
    )
    msgSvc = nconfig["MessageSvc/MessageSvc"]
    msgSvc.setDebug += [
        "Rich::Detector::Rich1",
        "Rich::Detector::Rich2",
        "Rich::Detector::PDInfo",
        "Rich::Utils::MirrorFinder",
    ]
    config.update(nconfig)


def run2022Data():
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb("rich-decode-2022")
    if UseDD4Hep:
        options.force_odin = True
    _runDataTest(options)


def run2023Data():
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb("rich-decode-2023")
    if UseDD4Hep:
        options.force_odin = True
    _runDataTest(options)


def run2024Data():
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb("rich-decode-2024")
    if UseDD4Hep:
        options.force_odin = True
    _runDataTest(options)


def run2024DataTrunkGeom():
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb("rich-decode-2024")
    options.geometry_version = "run3/trunk"
    if UseDD4Hep:
        options.force_odin = True
    _runDataTest(options)


def runDetDescCompat():
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb("rich-decode-detdesc-compat-old-rich1")
    options.input_stream = "Rich"
    options.xml_file_catalog = "out.xml"
    _runDataTest(options)


def runCompatOld():
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb("rich-decode-detdesc-compat-old-rich1")
    options.input_stream = "Rich"
    options.xml_file_catalog = "out.xml"
    _runInstanciateTest(options)


def runCompatTrunk():
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb("rich-decode-2024")
    # overwrite condition db tag
    options.geometry_version = "run3/trunk"
    if UseDD4Hep:
        options.force_odin = True
    _runInstanciateTest(options)


def runCompat2024():
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb("rich-decode-2024")
    # overwrite condition db tag
    options.geometry_version = "run3/2024.Q1.2-v00.00"
    if UseDD4Hep:
        options.force_odin = True
    _runInstanciateTest(options)


def runRadInter():
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb("rich-decode-detdesc-compat-old-rich1")
    options.input_stream = "Rich"
    options.xml_file_catalog = "out.xml"
    _runRadInterTest(options)


def runMultipleRuns():
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb("rich-multiple-runs")
    _runMultipleRunsTest(options)
