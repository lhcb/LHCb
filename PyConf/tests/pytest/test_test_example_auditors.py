###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re

from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = [
        "gaudirun.py",
        "../../options/example_auditors.py",
        "--output=example_auditors.opts.py",
        "--all-opts",
    ]
    reference = {"messages_count": {"FATAL": 0, "ERROR": 0, "WARNING": 0}}

    def test_stdout(self, stdout: bytes):
        auditor_enter = re.search(
            rb"NameAuditor\s+INFO About to Enter .* with auditor trigger", stdout
        )
        auditor_exit = re.search(
            rb"NameAuditor\s+INFO Just Exited .* with auditor trigger", stdout
        )
        assert auditor_enter and auditor_exit, "NameAuditor output not found"
