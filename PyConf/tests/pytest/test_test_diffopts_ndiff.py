###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import LineSkipper


class Test(LHCbExeTest):
    command = [
        "diffopts.py",
        "--ndiff",
        "../pytest/data/brunel.opts.py",
        "../pytest/data/moore.opts.py",
    ]
    reference = "../refs/diffopts_ndiff.yaml"
    returncode = 1

    preprocessor = LHCbExeTest.preprocessor + LineSkipper(
        regexps=[r"^(---|\+\+\+).*(brunel|moore)\.opts\.py$"]
    )
