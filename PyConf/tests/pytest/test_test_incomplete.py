###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "../options/incomplete.py"]
    returncode = 1

    def test_stderr(self, stderr: bytes):
        from DDDB.CheckDD4Hep import UseDD4Hep

        if not UseDD4Hep:
            expected = (
                b"Required options not set: ['input_type', 'dddb_tag', 'conddb_tag']"
            )
        else:
            expected = b"Required options not set: ['input_type', 'geometry_version', 'conditions_version']"
        assert expected in stderr, "ConfigurationError not raised"
