###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re

import pytest

from PyConf import ConfigurationError
from PyConf.Algorithms import (
    Gaudi__Examples__IntDataProducer as IntDataProducer,
)
from PyConf.Algorithms import (
    Gaudi__Examples__IntToFloatData as IntToFloatData,
)
from PyConf.Tools import Gaudi__Examples__FloatTool as FloatTool
from PyConf.Tools import MyTool


def test_init():
    # Fail on nonexistent properties
    with pytest.raises(ConfigurationError) as e:
        MyTool(NotAProp=1)
    assert re.search(r"NotAProp.*not propert.* of.*MyTool", str(e))

    with pytest.raises(ConfigurationError) as e:
        FloatTool()
    assert re.match(r".*provide all inputs.*Input.*", str(e))

    data = IntToFloatData(InputLocation=IntDataProducer())
    t = FloatTool(Input=data)
    assert len(t.inputs) == 1
    assert t.private

    with pytest.raises(ConfigurationError):
        # immutable after instantiation
        t.OutputLevel = 2

    # initializing a private tool twice should **NOT** give the same instance
    t2 = FloatTool(Input=data)
    assert t is not t2, "tool instantiation should not return cached object"

    tp = FloatTool(public=True, Input=data)
    assert tp.public
    assert tp != t, "public and private tool instance must be different"

    tp2 = FloatTool(public=True, Input=data)
    assert tp is tp2, "identical public tool instantiation should return cached object"

    # 2 public tools with same name aren't allowed to have different properties
    with pytest.raises(ConfigurationError) as e:
        a = MyTool(public=True)  # noqa: F841
        b = MyTool(public=True, Int=123)  # noqa: F841

    assert "already configured with different properties" in str(e)
