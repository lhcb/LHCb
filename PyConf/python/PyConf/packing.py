##############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Helpers for configuring packing and unpacking."""

from PyConf import configurable
from PyConf.components import force_location
from PyConf.location_prefix import prefix
from PyConf.persistency_locations import (
    default_persistreco_version as _default_persistreco_version,
)
from PyConf.persistency_locations import pp2mcp_locations as _pp2mcp_locations
from PyConf.persistency_locations import reco_locations as _reco_locations

_default_persistable_locations = {
    v: {k: cfg[0] for k, cfg in locs.items()} for v, locs in _reco_locations.items()
}

_default_persistable_pp2mcp_locations = {k: v[0] for k, v in _pp2mcp_locations.items()}


def available_persistreco_versions():
    return list(_reco_locations.keys())


def default_persistreco_version():
    return _default_persistreco_version


@configurable
def persistreco_version(version=default_persistreco_version()):
    """
    returns version of persistreco for reading
    """
    return version


@configurable
def persistreco_writing_version(version=default_persistreco_version()):
    """
    returns version of persistreco for writing
    """
    return version


@configurable
def reco_locations(stream="/Event/HLT2", version=persistreco_version):
    return {
        k: (prefix(v[0], stream), v[1]) for k, v in _reco_locations[version()].items()
    }


@configurable
def pp2mcp_locations(stream="/Event/HLT2"):
    return {k: (prefix(v[0], stream), v[1]) for k, v in _pp2mcp_locations.items()}


@configurable
def persistable_locations(
    locations=_default_persistable_locations,
    stream="/Event",
    version=persistreco_writing_version,
):
    return {k: prefix(v, stream) for k, v in locations[version()].items()}


def default_persistable_locations(**kwargs):
    kwargs["version"] = default_persistreco_version
    return persistable_locations(**kwargs)


@configurable
def persistable_location(k, force=True, locations=persistable_locations):
    return force_location(locations()[k]) if force else None


def default_persistable_location(k, **kwargs):
    kwargs["locations"] = default_persistable_locations
    return persistable_location(k, **kwargs)


def packers_map():
    from PyConf.Algorithms import (
        SOACaloClusterPacker,
        SOACaloHypoPacker,
        SOAPVPacker,
        SOATrackPacker,
    )

    return {
        "Tracks_v2": SOATrackPacker,
        "CaloClusters_v2": SOACaloClusterPacker,
        "CaloHypos_v2": SOACaloHypoPacker,
        "ExtendedPVs": SOAPVPacker,
    }
