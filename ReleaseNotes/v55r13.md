2024-09-29 LHCb v55r13
===

This version uses
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r36](../../../../Detector/-/tags/v1r36) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.0.

This version is released on the `2024-patches` branch.
Built relative to LHCb [v55r12](/../../tags/v55r12), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Using Gaudi/Parsers/Factory.h instead of GaudiKernel/ParsersFactory.h, !4675 (@msaur)
- Tidy up UT sim conditions, !4608 (@hawu)
- ~RICH | RichTrackSegment: General Cleanup, !4666 (@jonrob)
- ~Calo | CaloDataFunctor: Ensure structs are default initialised, !4665 (@jonrob)


### Documentation ~Documentation


### Other
