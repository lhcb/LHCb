2024-06-13 LHCb v55r10
===

This version uses
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r33](../../../../Detector/-/tags/v1r33) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to LHCb [v55r9](/../../tags/v55r9), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- Fix to allow for UThit clusters > 512, !4599 (@ldufour)
- Improve handling of empty DstData raw bank, !4565 (@graven)


### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration ~Persistency | Resolve "Follow-up from "Bug fix in unpacking of GlobalChargedPIDs"", !4576 (@mveghel) [#359]


### Documentation ~Documentation


### Other

- ~Tracking ~PID | Fix sigmoid finite answer and flattening SIMD MLPs, !4570 (@mveghel)
- Support huge cluster containers for VP / UT / FT, !4592 (@ldufour)
- Add sklearn standard scaler for NN preprocessing, !4557 (@jzhuo)
- ChargedProtoParticleAddCaloInfo from track ancestor, Give RelationTable1D a version of add that checks entry is not already added, !4385 (@isanders)
