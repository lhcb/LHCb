2021-10-04 LHCb v53r3
===

This version uses
Gaudi [v36r1](../../../../Gaudi/-/tags/v36r1) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to LHCb [v53r2](/../../tags/v53r2), with the following changes:

### New features ~"new feature"

- ~Conditions | Add support for TH3 reading from conditions DB, !3208 (@chasse)
- RefBot going live!, !3272 (@chasse)


### Enhancements ~enhancement

- LHCbAlgs: add missing MultiTransformerFilter in LHCb::Algorithms namespace, !3266 (@graven)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Persistency | Move Packing related algorithms to Event/EventPacker, !3269 (@sesen)
- ~Persistency | Spruce reports to RawBanks, further cleanup of HltDAQ, !3260 (@graven)
- Migrate to LHCb::Algorithm, !3267 (@graven)
