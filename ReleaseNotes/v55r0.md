2024-01-26 LHCb v55r0
===

This version uses
Gaudi [v37r2](../../../../Gaudi/-/tags/v37r2),
Detector [v1r25](../../../../Detector/-/tags/v1r25) and
LCG [104](http://lcginfo.cern.ch/release/104/) with ROOT 6.28.04.

This version is released on the `master` branch.
Built relative to LHCb [v54r21](/../../tags/v54r21), with the following changes:

### New features ~"new feature"

- ~Persistency | Add zero-suppressed HltDecReport rawbank format, !4367 (@graven)


### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Decoding ~Calo | Small cleanups that speed up the Calo decoding, !4339 (@rmatev)
- ~Tracking | Drop FittedForward track type and use FitHistory instead, !4419 (@gunther) [#339]
- ~Tracking | Define order of hitzones in PrSciFiHits header, !4406 (@gunther)
- ~RICH | RichPID: Small clean up and modernisation, !4288 (@jonrob)
- ~Persistency | Small speedup of unpacking, !4340 (@graven) [#334]
- ~Persistency | Remove pointer assumption from packer's check function, !4323 (@ldufour)
- ~Build | Include a standalone compilation of ODIN in DAQEvent, !4412 (@raaij)
- ~Build | Reintroduced MDFLib, with StreamDescriptor inside, as it's needed by MooreOnline, !4400 (@sponce)
- ~Build | Use gaudi_add_pytest instead of gaudi_add_tests(pytest/nosetest), !4059 (@clemenci)
- Clean up original GOD generated code in PhysEvent and MCEvent, !4418 (@graven)
- Do not rely on dynamic TES location alternate resolution, !4415 (@graven)
- Streamline HltDecReport{,s} and HltDecReportsWriter, !4398 (@graven)
- Remove mention of deleted IBTaggingTool, !4391 (@cprouve)
- Remove PixelModule and HCRawBankDecoder, !4382 (@ldufour)
- Reduce code duplication and simplify ControlFlowNode and HLTControlFlowMgr, !4379 (@graven)
- Cleanup state location and track type usage in track v3 and converters, !4374 (@gunther) [#335]
- Do not build DetDesc dictionary, !4363 (@clemenci)
- Replace std::random_shuffle with std::shuffle, !4335 (@clemenci)
- Remove duplicate configuration of MCParticle/MCVertex unpacking, !4327 (@graven)


### Documentation ~Documentation


### Other

- ~Configuration | Add `--with-defaults` command-line option to LbExec, !4319 (@admorris)
- ~Muon ~PID ~"Event model" | Cleanup of ProtoParticle of duplicate muon info, !4416 (@mveghel)
- ~Calo ~Persistency | Add NeutralPID object to ProtoParticle, !4097 (@tnanut)
- ~RICH | DecodedDataFromMCRichHits: Improvements to hit time handling, !4371 (@jonrob)
- ~RICH ~PID ~"Event model" | Remove obsolete RICH additionalInfo in ProtoParticle, !4320 (@mveghel)
- ~RICH ~"MC checking" | Misc. improvements to support RICH 4D Reco, !4337 (@jonrob)
- ~Persistency | Add a raw event mover algorithm, !4393 (@sesen)
- ~Persistency | Check of unique rawbank sourceID in SelectiveCombineRawBankViewsToPersist, !4259 (@msaur) [Moore#614]
- ~Persistency | Add support for packing T::Selection, for classes T which are packable,  if T::Selection is defined, !4193 (@graven)
- Prepare for new GaudiPartProp in Gaudi v38, !4420 (@clemenci)
- Fixes cast method of MatVec so that it works with non explicit casting operator, !4414 (@sponce)
- Fixed typo in platform info for clang16 platform, !4410 (@sponce)
- Various cleanup in LHCb, !4402 (@sponce)
- Adapted Phoenix Sink to changes in BaseSink interface, !4328 (@sponce)
- Add heavy flavour track related info and functionality for VP, !4208 (@mveghel)
- More paranoid FetchDataFromFile and more generic unpacking, !4403 (@graven)
- Fixed missing include breaking the build on gcc13, !4401 (@sponce)
- Remove DAQKernel package (and migrate its users, eg. `bankKiller` and `RawEventDump`), !4399 (@graven) [#338]
- Reduced precision of TestVDTMath to avoid fluctuations with platforms and compilers, !4397 (@sponce)
- Added new platforms to PlatformInfo, !4396 (@sponce)
- Minor tweak to PyConf JSON encoder to support set properties, !4381 (@clemenci)
- Add a copy of GaudiAlg (imported from Gaudi v37r2), !4373 (@clemenci)
- Backported floating point comparison fixes from Run2Patches, !4304 (@sponce)
- Update References for: LHCb!4379 based on lhcb-master-mr/9834, !4394 (@lhcbsoft)
- Various fixes for C++20, backported from Run2Support, !4392 (@sponce)
- Unpack MCParticles when requesting MCHeader, !4387 (@amathad)
- Cleaned up code around MDFWriter, !4380 (@sponce)
- Support setting compression algorithm with mdf_writer, !4282 (@cburr)
- Consistently use RawBank::View as input to algorithms instead of RawEvent, !4010 (@graven)
- Make FindVDT.cmake compatible with ROOT's FindVdt.cmake, !4345 (@clemenci)
