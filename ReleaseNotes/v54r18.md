2023-09-25 LHCb v54r18
===

This version uses
Detector [v1r21](../../../../Detector/-/tags/v1r21),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to LHCb [v54r17](/../../tags/v54r17), with the following changes:

### New features ~"new feature"

- ~Conditions | Condition derivation for InteractionRegion information, !4173 (@raaij)


### Fixes ~"bug fix" ~workaround

- ~RICH | Fix point write access, !4296 (@jonrob)
- ~Persistency | (more) deterministic weighted relations ordering, !4275 (@graven)


### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~"PV finding" | Make sure PVTracks::prvelocontainers is initialized, !4289 (@clemenci)
- ~UT | Convert UT warnings and errors to accumulators., !4274 (@ganowak) [Lbcom#11]
- ~"Event model" | Remove Velo(Lite) clusters from DigiEvent, !4294 (@ldufour)
- ~Build | Fixes for a clean compilation on ARM, !4207 (@clemenci)
- Fix standalone headers builds, !4211 (@clemenci)
- Fixes for LCG 104, !4143 (@clemenci) [LHCBPS-1912]
- Adapted to changes in BaseSink and Entities, !4022 (@sponce) [#294]


### Documentation ~Documentation


### Other

- ~Tracking | PrSciFiHits add method to view x values, !4266 (@gunther)
- Remove operator== for floats in simdwrapper, !4264 (@ahennequ)
- Explicit cast added for ARM, !4200 (@obuon)
