2023-07-16 LHCb v54r12
===

This version uses
Detector [v1r16](../../../../Detector/-/tags/v1r16),
Gaudi [v36r14](../../../../Gaudi/-/tags/v36r14) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to LHCb [v54r11](/../../tags/v54r11), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- ~Persistency | Revert revert of !4179, !4189 (@graven)
- ~Persistency | Explicitly pack empty KeyedContainers and Relations Tables, !4179 (@graven)
- PyConf: fix __eq__ for algorithm components, !4191 (@graven) [Moore#605,Moore#612]


### Enhancements ~enhancement

- ~Tracking | Loose sector search scheme for UT, !4180 (@decianm)
- ~FT | FT_geo_supporting_functions, !4163 (@zexu)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Decoding ~UT | Automatic derivation of sensor lookup table in UT, !4135 (@decianm)
- ~Build | Suppress suspected GCC 12 false positive warning, !4194 (@rmatev) [DaVinci#127]
- Correction for ARM, !4190 (@obuon)
- Remove non-thread-safe unused code, !4171 (@cmarinbe)
- Remove old, unused 'mapper' code to go from packed -> transient representations, the corresponding unpackers, and corresponding tests, !4170 (@graven)
- Dropped unused .opts file, !4178 (@sponce)


### Documentation ~Documentation


### Other

- ~FT | Function for hit efficiency, !4057 (@zexu)
- Revert "Merge branch 'pack-empty-keyedcontainers' into 'master'", !4188 (@rmatev)
- Dropped unused code around time decoding, !4186 (@sponce)
- Warn in case of problematic DetElem path for derived condition, !4184 (@sponce)
