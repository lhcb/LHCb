2023-03-03 LHCb v54r5
===

This version uses
Gaudi [v36r11](../../../../Gaudi/-/tags/v36r11),
Detector [v1r9](../../../../Detector/-/tags/v1r9) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to LHCb [v54r4](/../../tags/v54r4), with the following changes:

### New features ~"new feature"

- ~FT ~Conditions | SciFi mat contraction calibration conditions, !3591 (@isanders)
- ~"Event model" | Add relation tables to FutureNeutralProtoPAlg, !3956 (@tnanut)
- ~Build | Option to isolate tests with a per-test working directory, !3975 (@rmatev)


### Fixes ~"bug fix" ~workaround

- ~Build | Disable python hash randomization, !3971 (@rmatev)


### Enhancements ~enhancement

- ~Configuration | Generalize profiling algorithms configuration with HLTControlFlowMgr, !3919 (@rmatev)
- ~Decoding | A filter for empty raw banks, !3990 (@sesen) [Moore#523]
- ~Tracking | Change hitpattern such that it counts hits per layer, !3891 (@wouter)
- ~Persistency | Unify relation unpackers, !3970 (@sesen)
- ~Luminosity | Add routing bits filter to control flow, !3952 (@nskidmor)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Decoding ~FT | Two small improvements to FTRawBankDecoder, !3954 (@graven)
- ~Decoding ~Calo | Fix noBanksCounter in Calo decoding, !3950 (@cmarinbe)
- ~Decoding ~Luminosity | Add DetDesc specific ref for HltDAQ.lumi_decoding test, !3964 (@jonrob)
- ~Build | Declare dependency of RichKernel ConfigurableUser on LHCbKenrel, !3991 (@clemenci)
- ~Build | Fix detdesc tests in LHCb and Rec, !3962 (@rmatev)
- ~Build | Fix PROJECT_SOURCE_DIR replacement pattern in LHCbConfigUtils.cmake, !3959 (@mimazure)
- Avoid copy in structured binding, !3989 (@clemenci)
- Dropping unused code, !3985 (@sponce)


### Documentation ~Documentation


### Other

- ~Decoding ~UT ~Conditions | Fix UT readout map condition path in UTReadoutTool for dd4hep, !3951 (@jonrob)
- ~Calo ~"Event model" | Brem functors / wrappers, !3894 (@mveghel)
- ~"Event model" | Add ECal and HCal total energy deposit to RecSummary, !3980 (@sbelin)
- ~Persistency | Selective packing of relations, !3963 (@graven) [DaVinci#96]
- ~Luminosity | Run3 File Summary Record, !3796 (@clemenci) [LBCOMP-58]
- ~Core | Add some centos7/gcc12 platforms, !3979 (@jonrob)
- Workaround external SyntaxWarning due to LCG 103, see #292, !3993 (@rmatev) [#292]
- Fix stand-alone compilation of public headers, !3992 (@clemenci)
- Expose more options via the compression option, !3984 (@cburr)
- Improve error reports for invalid geometry options, !3982 (@clemenci)
- Ref updates needed for gaudi/Gaudi!1426, !3978 (@clemenci)
- Fix typo in !3980, !3983 (@rmatev)
- Improve LinksByKey implementation, !3972 (@graven)
- Update PlatformInfo with a number of new platforms, !3976 (@clemenci)
- Revert !3964, !3965 (@rmatev)
- Do not throw in HltRoutingBitsCombiner the case HLT2 data are processed with HLT2., !3961 (@sstahl)
- Make calo ADC and Digit packing compatible with other packed classes, !3943 (@sesen)
- Update References for: LHCb!3953, Moore!2053, DaVinci!809 based on lhcb-master-mr/6955, !3957 (@lhcbsoft)
- Removed usage of HistogramPersistencySvc and put all Histos into same output file, !3953 (@sponce)
- Fix compatibility with Boost 1.81, !3948 (@clemenci) [gaudi/Gaudi#255]
- Correctly handle geometry and conditions versions from TestFileDB, !3946 (@clemenci)
- Enable DD4hep by default, !3903 (@clemenci)
- FT cluster pseudo width update in Decoder, !3811 (@zexu)
- Add type to pyconf.algorithms importable algs/tools, !3698 (@nnolte)
- Fix reference (follow !3919), !3945 (@rmatev)
- Introduce Track::hasMuon(), !3926 (@ausachov)
