2020-02-18 LHCb v51r0
===

This version uses Gaudi v33r0 and LCG_96b with ROOT 6.18.04.

This version is released on `master` branch.
Built relative to LHCb v50r6, with the following changes:

### New features ~"new feature"

- ~Decoding | Add ODIN emulator and encoder, !2196 (@rmatev)
- ~Decoding ~Persistency | Add fibre tracker bank types, !2374 (@sesen)
- ~Tracking ~"Event model" | First commit of PrUTHits, the SOA hit class for the UT, !2204 (@decianm) :star:
- ~Muon ~"Event model" | Add new Muon PIDs object in SOA format, !2111 (@sstahl, @olupton) :star:
- ~RICH ~Conditions ~Simulation | Provide access to channel properties in RICH PMT detector elements., !2353 (@bmalecki)
- ~"Event model" ~Utilities | Add small matrix/vector library working with simdwrappers + small additions to zipping, !2309 (@nnolte)
- ~Persistency | Add new routing bits writer, !2215 (@sstahl)
- ~Persistency ~Simulation | Add setting for configuring output of PGUN simulation (@adavis), !2385 (@cattanem)
- ~Core | Custom allocator/memory pool in HLT1, !2270 (@olupton) :star:
- ~Core | Implement asynchronous scheduler, !1939 (@nnolte) :star:
- ~Utilities | Add {stableP,p}artition{Position,ByMask}, and bind_front, !2312 (@graven)
- ~Utilities | Add ArenaAllocator and two generic Container  types, !2197 (@graven)
- ~Utilities | Soa accessor prototype with boost::hana, !2141 (@nnolte)
- ~Simulation | PGPrimaryVertex Configuration for all years (@adavis), !2384 (@cattanem) [LHCBGAUSS-1924] :star:


### Fixes ~"bug fix" ~workaround

- ~Decoding ~Accelerators | Modernize VPDAQ, !2302 (@graven)
- ~Tracking | Sort and remove duplicate LHCb IDs in Pr::Fitted::Forward::Tracks, !2221 (@apearce)
- ~Calo | Add CaloIndex::Undefined enum value, !2273 (@jonrob)
- ~Calo | Fix remaining gcc9 warnings, !2218 (@graven)
- ~Calo | Fix unnecessary memory copy in DeCalorimeter::Cell_, !2184 (@conrad)
- ~RICH ~Conditions | Unify convention to transform global->detPlane frame, !2162 (@bmalecki)
- ~Functors | LoKi::ToCpp - Explicitly instanciate array<string,4> to work around clang/python/cling/ROOT issue, !2258 (@jonrob)
- ~"Event model" | Define comparator decorator for LHCb::ParticleProperty objects., !2237 (@apearce) [lhcb/LHCb#66]
- ~Core | Workaround for ROOT/Cling problem with Boost small_vector, !2388 (@rmatev) [lhcb/LHCb#75]
- ~Core | HLTControlFlowMgr - Prevent NaN in timing summary, !2261 (@jonrob)
- ~Core | Workaround for Gaudi::Application not popping the output queue, !2247 (@nnolte)
- ~Core | Remove copy of scheduler states in executionreportswriter, !2222 (@nnolte)
- ~Conditions | Improve diagnostic for ConditionAccessor in case of type mismatch, !2262 (@graven)
- ~Conditions | Modernize DetDesc, !2225 (@graven)
- ~Conditions | Weaken requirements for ConditionUpdateContext from ParamValidDataObject to ValidDataObject, !2210 (@graven)
- ~Utilities | Fix SIMDWrapper::scalar::types::loop_mask for out of range, !2378 (@pseyfert)
- ~Utilities | Fix hmax functions in SIMDWrapper.h, !2211 (@nnolte)
- ~Simulation | Add context methods to LoKi::Hybrid::GenEngine, !2362 (@apearce) [LHCBGAUSS-1833] :star:
- ~Build | Fixed counter checking for counters with '|' in the name, !2334 (@sponce)
- ~Build | Fixed retrieval of sensibility in counter checking, !2324 (@sponce)
- ~Build | Fix GroupMessages preprocessor, !2314 (@rmatev)
- ~Build | Always write out new ref if different from old, !2308 (@rmatev) [gaudi/Gaudi#108]
- ~Build | Exclude histogram headers in LHCb ref preprocessor, !2292 (@rmatev)
- ~Build | Fix check for non-x86 architectures, !2244 (@rmatev)
- ~Build | Fix dependency order between TrackEvent and SOAExtensions, !2188 (@graven)


### Enhancements ~enhancement

- ~Decoding ~Tracking | Simplify pos_iterator and posAdc_iterator, !2306 (@ahennequ)
- ~Decoding ~Muon | Use event-local memory pool in LHCb::Pr::Muon::PIDs and muon decoding, !2342 (@olupton)
- ~Decoding ~Muon | Improve MuonRawToHits, !2177 (@rvazquez) :star:
- ~Decoding ~Muon ~"Event model" | Improve MuonHit data structures and decoding, !2203 (@ahennequ)
- ~Tracking | Make classes relevant to the SciFi/forward tracking event-local memory pool aware, !2341 (@olupton)
- ~Tracking | Enable event-local memory pool for LHCb::Pr::{UT::Hits,Upstream::Tracks}, !2340 (@olupton)
- ~Tracking | Enable event-local memory pool for LHCb::Pr::Velo::{Hits,Tracks}, !2339 (@olupton)
- ~Tracking | Masked compilation warnings coming from VectorClass for avx512, !2304 (@sponce)
- ~Tracking | Make max_tracks publicly available for Rec!1744, !2178 (@gunther) [lhcb/Rec#89]
- ~Muon ~"Event model" ~"MC checking" | Modify CommonMuonHit to support MC matching, !2153 (@olupton) [lhcb/Rec#86]
- ~Muon ~"Event model" ~Simulation | Make MuonDigit::TimeStamp const correct + misc changes, !2223 (@graven)
- ~Calo | Adjust ICaloFutureElectron to make CaloFutureElectron tool thread safe, !2239 (@aszabels) [lhcb/Rec#106]
- ~Calo ~PID | Update of NeutralIDTool for upgrade conditions, !2354 (@calvom)
- ~RICH | CPUDispatch.h - Limit CPU dispatching to only levels supported by compilation level, !2265 (@jonrob)
- ~RICH | RichRayTracing - Update SIMD API to accept r- or l-value reference to starting directions vector, !2264 (@jonrob)
- ~RICH ~Conditions | RichFutureUtils - Add derived condition factory methods utilising inputLocation<TYPE>, !2255 (@jonrob)
- ~RICH ~Conditions | RICH - Convert to functional conditions access, !2226 (@jonrob) :star:
- ~Filters | Set DeterministicPrescaler seed with an explicit property, !2383 (@apearce) [lhcb/Moore#74]
- ~Functors | Make zips agnostic about argument order and add .get<T>() accessor, !2198 (@olupton)
- ~"Event model" | Enable event-local memory pool for vertex and selection types, !2343 (@olupton)
- ~"Event model" | Dummy producers and track proxy tweaks, !2209 (@olupton)
- ~"Event model" | Support for new functors, !2199 (@samarian)
- ~"Event model" | Streamline proxy definition, add registry of headers for functors, Pr::Zip improvements, !2173 (@olupton)
- ~Persistency | Suffix line names with Decision in DecReports writer, !2373 (@rmatev) [lhcb/Moore#130]
- ~Persistency | Add counter for the number of raw banks and make the reserved number configurable, !2278 (@olupton)
- ~Persistency | Take DecReport IDs from the HltANNSvc in execution reports writer, !2259 (@apearce) [lhcb/LHCb#68]
- ~Core | HLTControlFlowMgr - Resize name column in timing table to accommodate larger names, !2217 (@jonrob)
- ~Core | HLTControlFlowMgr introduce optional calling of sysExecute or execute and implement a timing table, !2171 (@chasse)
- ~Conditions | Further simplify addConditionDerivation, !2254 (@graven)
- ~Conditions | Add default transform to LHCb::DetDesc::addConditionDerivation -- if signature is explicitly given, !2236 (@graven)
- ~Conditions | Add support to IConditionDerivationMgr for chained derived conditions, !2232 (@jonrob)
- ~Conditions | Allow 'just signature' to be specified in addConditionDerivation, !2216 (@graven)
- ~Conditions | Update default LHCBCOND global global tags, !2167 (@cattanem)
- ~Utilities | Add operators to scalar mask_v, !2377 (@pseyfert)
- ~Utilities | Add max and min functions for int_v, !2256 (@decianm)
- ~Utilities | Additions to SIMDWrapper, !2185 (@ahennequ)
- ~Build | Add skylake_avx512+vecwid256-centos7-gcc9-* platforms to the list of known ones, !2326 (@olupton)
- ~Build | Replace run time with compile time dispatch LHCbMath similarity, !2307 (@jonrob) [lhcb/LHCb#71]
- ~Build | Minimize new refs produced by validateWithRef, !2260 (@rmatev) :star:
- ~Build | OTDet for non intel architectures, !2231 (@roiser)
- ~Build | Remove dependencies on vectorclass in Kernel packages for non intel architectures, !2228 (@roiser)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Decoding | Cleanup SiRawBankDecoder, !2311 (@graven)
- ~Decoding | Modernize OTRawBankEncoder, !2294 (@graven)
- ~Decoding | Replace print statements with message logging, !2275 (@cattanem)
- ~Decoding | Port FTRawBankDecoder to Gaudi::Algorithm, !2201 (@graven)
- ~Decoding ~Muon | Modernize MuonDAQ, !2291 (@graven)
- ~Tracking | Limited use of vectorclass to required places in MagneticFieldGrid, !2227 (@sponce)
- ~Tracking ~"Event model" | Event/TrackEvent: run clang-tidy, !2192 (@graven)
- ~Tracking ~"Event model" | PrIterableFittedForwardTracks.h consistent between data types, !2169 (@cprouve)
- ~Tracking ~Conditions | Change `UTDAQ::computeGeometry` to make it usable for `addConditionsDerivation`, !2208 (@graven)
- ~Tracking ~Conditions | Det/UTDet: run clang-tidy & tweak DeUTDetector, !2191 (@graven)
- ~Tracking ~Conditions | UTDet: minor technical cleanup, !2176 (@graven)
- ~Tracking ~Utilities | Dropped unused SOA utilities, !2369 (@sponce)
- ~Muon | Use an error message counter in MuonRawToHits, !2251 (@apearce) [lhcb/LHCb#69]
- ~Muon | Modernize MuonRawToHits, !2229 (@graven)
- ~Calo | Make chi2(match,match) a friend function of match, !2206 (@graven)
- ~Calo | Det/CaloDet: run clang-tidy, !2194 (@graven)
- ~Calo | CaloFutureUtils: run clang-tidy, !2190 (@graven)
- ~Calo | Cleanup ICaloFutureHypo2CaloFuture, !2172 (@graven)
- ~RICH | Cleanup of API of RichFutureUtils, !2252 (@jonrob)
- ~RICH | RICH - Migrate tools to derived condition objects, !2230 (@jonrob)
- ~RICH | RichFutureKernel - Streamline and make Gaudi::Algorithm default base class, !2200 (@jonrob)
- ~RICH | Rich/RichFutureUtils: run clang-tidy, !2195 (@graven)
- ~RICH ~"MC checking" | TrackToMCParticleRelations - Remove empty relations warning, !2321 (@jonrob)
- ~Functors | Phys/LoKiCore: Run clang-tidy, !2189 (@graven)
- ~"Event model" | Modernize MCEvent, !2300 (@graven)
- ~"Event model" | Move methods from proxy_type to PROXY_METHODS, !2205 (@olupton) [lhcb/LHCb#59]
- ~"Event model" | Event/RecEvent: run  clang-tidy, !2186 (@graven)
- ~"Event model" | Modernize Relations, !2182 (@graven)
- ~"Event model" ~Simulation | Event/DigiEvent: run clang-tidy..., !2187 (@graven)
- ~"Event model" ~Build | Resolve LHCBPS-1845 "Decommission GaudiObjDesc", !2158 (@clemenci) [LHCBPS-1845]
- ~Persistency | Make IOExample tests more agnostic about the architecture of the machine running the tests, !2327 (@olupton)
- ~Persistency | Modernize EventPacker/Compare*, !2297 (@graven)
- ~Persistency | Modernize RecreatePIDTools, !2296 (@graven)
- ~Persistency | Fix HltLinePersistenceSvc test, !2245 (@apearce)
- ~Persistency | Move IOAlg to Gaudi::Algorithm, !2233 (@graven)
- ~Persistency | Migrate ExecutionReportsWriter to Gaudi::Functional::Transformer, !2224 (@graven)
- ~"MC checking" | Remove thread unsafe, incident-dependent variable in MCReconstructible, !2319 (@rjhunter)
- ~Conditions | Cleanups and changes in DetDesc to streamline the code and prepare DD4hep introduction, !2246 (@sponce)
- ~Conditions | Move MuonRawToHits to condition derivation, !2212 (@graven)
- ~Utilities | Remove LHCbMath Similarity tests, !2317 (@jonrob)
- ~Utilities | Kernel/LHCbKernel: run clang-tidy, !2193 (@graven)
- ~Utilities | Remove explicit assignment operator in SIMDWrapper to suppress gcc9 warning., !2161 (@olupton)
- ~Build | Add workaround for finding StringIO module with Python 3, !2379 (@cattanem)
- ~Build | Fix range v3 deprecation warnings, !2268 (@cattanem)
- ~Build | Switch from deprecated GaudiKernel/Counters.h to Gaudi/Accumulators.h, !2266 (@jonrob)
- ~Build | Add 'expected' OMP stderr messages to test exclusions, !2263 (@jonrob) [lhcb/LHCb#96]
- ~Build | RichUtils - Remove relic dependency on VectorClass, !2220 (@jonrob)
- ~Build | Work towards make test_public_headers_build pass, !1184 (@pseyfert)
- Ignore the timing table, !2207 (@apearce)


### Documentation ~Documentation

- Update CONTRIBUTING.md, !2310 (@cattanem)
