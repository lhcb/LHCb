2023-03-22 LHCb v54r6
===

This version uses
Detector [v1r10](../../../../Detector/-/tags/v1r10),
Gaudi [v36r12](../../../../Gaudi/-/tags/v36r12) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to LHCb [v54r5](/../../tags/v54r5), with the following changes:

### New features ~"new feature"

- ~"Event model" | SOACollection particlev2, !3907 (@ahennequ)
- Add counter class for luminosity counters, !4009 (@clemenci)
- Add option to mdf writer to skip special TAE treatment, !4008 (@sstahl)
- Determinstic comparisons of  Particle in RelationsTables, !4003 (@graven)
- (re-)introduce transparant lookup in GenFSR, !3999 (@graven)


### Fixes ~"bug fix" ~workaround

- More deterministic comparison for ProtoParticle Relations, !3998 (@graven) [Moore#538]
- Make name of algorithms independent of instantiation order, !3872 (@sponce) [#267]
- ~Configuration | Fix trivial typo in GaudiConf/python/GaudiConf/LbExec/cli_utils.py, !3997 (@erodrigu)
- Fix https://gitlab.cern.ch/lhcb/DaVinci/-/issues/108, !4019 (@pkoppenb)
- Adapt XMLSummaryKernel tests to change output file creation policy, !4023 (@clemenci)
- Fix use of Boost::unit_test_framework library, !4033 (@clemencic)
- Make sure Git resources are released before git_libgit2_shutdown, !4032 (@clemenci)


### Enhancements ~enhancement

- ~Persistency | Make pp2mcp relations unpacking consistent across different functions, !3986 (@sesen)
- Improved counter reporting :
   + Fixed test after improvement of counter reports, !4021 (@sponce)
   + Improved error for algo renaming in counters, !4020 (@sponce)
   + Fixes to counter comparison script, !4006 (@sponce)
   + Improvements in counter diff reporting, !4005 (@sponce)

### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Remove LinkerTable and LinkerTool, augment LinkedTo, LinkedFrom, !3988 (@graven)


### Documentation ~Documentation


### Other
