2021-09-24 LHCb v53r2
===

This version uses
Gaudi [v36r1](../../../../Gaudi/-/tags/v36r1) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to LHCb [v53r1](/../../tags/v53r1), with the following changes:


### Fixes ~"bug fix" ~workaround

- ~Decoding | Revert FTRawBankEncoder to produce raw event only, !3262 (@sesen) [#156] :star:
- ~Tracking | Remove implicit ordering assumption of LHCbIDs when converting to a v3::Track, !3249 (@decianm)
- ~Calo | Fix CaloHypotheses::erase (#105), !3243 (@graven) [#105,(#105]
- ~Calo | Follow up to !3221: fix DD4Hep case, !3242 (@graven)
- ~Persistency | Align `PackedDataChecksum::process` code path with `save` code path, !3248 (@graven)
- ~Utilities | Fixes for gcc11, !3228 (@graven) [#150]
- ~Build | Minor CMake fixes, !3250 (@clemenci)


### Enhancements ~enhancement

- ~Configuration | Make DISABLE_TOOL to allow arguments, !3246 (@peilian)
- ~Configuration | Make GaudiConf.reading compatible with PyConf, !3222 (@dfazzini) [#149]
- ~Calo ~PID | Update of neutral PID tools, !3166 (@calvom)
- ~Core | Add gcc11 and clang12 platforms to PlatformInfo, !3253 (@cattanem)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Fix LHCb__UnpackRawEvent output level, !3233 (@lmeyerga)
- ~Decoding | RawbankView for FT/UT decoders, !3225 (@sesen)
- ~Decoding ~Persistency | Modernize & Tweak HltDAQ, !3257 (@graven)
- ~Tracking ~"Event model" | Changed the name of trackFT to trackSeed and added a proxy, !3212 (@lohenry)
- ~"Event model" | Rationalize names, !3255 (@graven)
- ~Persistency ~Simulation | Modernize GenCountersFSR and GenFSR, !3121 (@graven)
- ~Utilities | Remove workaround for gcc9, !3261 (@cattanem)
- ~Build | Fix: disable DD4Hep tests in cmake, !3239 (@chasse)
- ~Build | Disable writing of new refs for passed tests, !3234 (@chasse)
- ~Build | Add missing header to get boost::hash in Boost 1.77, !3227 (@clemenci)
- Streamline LHCb::Algorithm namespace, !3221 (@graven)
- Fixes for DD4hep, !3119 (@clemenci)
