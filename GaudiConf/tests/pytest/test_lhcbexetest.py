###############################################################################
# (c) Copyright 2000-2025 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest

counters_fragment = """
HLTControlFlowMgr                      INFO Number of counters : 1
 |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |
 | "Processed events"                              |      1000 |
 """

counters_fragment_dict = {
    "HLTControlFlowMgr": {
        "set": "HLTControlFlowMgr",
        "total_counters": 1,
        "Processed events": {
            "binomial": False,
            "name": "Processed events",
            "count": 1000,
        },
    }
}


class Test(LHCbExeTest):
    """
    Isolate test whether LHCb (and downstream) specific fixtures work as intended.
    """

    command = ["echo", counters_fragment]

    def test_counters(self, counters: dict):
        assert counters == counters_fragment_dict

    def test_stderr(self):
        pytest.skip()

    def test_returncode(self):
        pytest.skip()

    def test_record_options(self):
        pytest.skip()

    def test_fixture_setup(self):
        pytest.skip()
