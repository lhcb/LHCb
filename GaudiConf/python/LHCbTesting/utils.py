###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re

# Signature of the print-out of Counters
c_count_re = re.compile(r"^(.*)INFO\s*Number\s*of\s*counters\s*:\s*(\d+)")


def _parse_counters_summary(lines, pos):
    """
    Parse the Counters summary table starting from pos.
    Returns a tuple with the dictionary with the digested information.
    """
    header_match = re.match(
        r"\s*([\w\.]+)\s+(\w+)\s+Number of counters\s*:\s*(\d+)", lines[pos]
    )
    if not header_match:
        raise ValueError(f"Invalid header format: {lines[pos]}")

    total_counters = int(header_match.group(3))
    title = header_match.group(1)
    counter_info = {
        header_match.group(1): {
            "set": title,
            "total_counters": total_counters,
        }
    }
    # Parse counter data
    for line in lines[pos + 2 : pos + 2 + total_counters]:
        if not line.startswith(" |"):
            raise AssertionError("Declared incorrect number of counters")
        parts = re.split(r"\|", line.strip())
        counter = {}
        counter["binomial"] = bool(parts[1].startswith("*"))
        counter["name"] = parts[1].strip().strip('"*')
        counter["count"] = int(parts[2].strip())
        if len(parts) > 4:
            counter["sum"] = float(parts[3].strip())
        if len(parts) > 5:
            counter["mean/eff^*"] = parts[4].strip()
            if not counter["binomial"]:
                counter["mean/eff^*"] = float(counter["mean/eff^*"])
        if len(parts) > 6:
            counter["rms/eff^*"] = parts[5].strip()
            if not counter["binomial"]:
                counter["rms/eff^*"] = float(counter["rms/eff^*"])
        if len(parts) > 7:
            counter["min"] = float(parts[6].strip())
        if len(parts) > 8:
            counter["max"] = float(parts[7].strip())
        counter_info[title][counter["name"]] = counter
    return counter_info


def find_counters_summaries(stdout):
    """
    Scan stdout to find counters summaries and digest them.
    """
    stdout = stdout.decode() if isinstance(stdout, bytes) else stdout
    outlines = stdout.splitlines() if hasattr(stdout, "splitlines") else stdout
    if outlines is None:
        return {}
    nlines = len(outlines) - 1
    counters = {}
    global c_count_re
    pos = 0
    while pos < nlines:
        summ = {}
        # find first line of block:
        match = c_count_re.search(outlines[pos])
        while pos < nlines and not match:
            pos += 1
            match = c_count_re.search(outlines[pos])
        if match:
            summ = _parse_counters_summary(outlines, pos)
            pos += 1
        counters.update(summ)
    return counters


# signature of the print-out of the histograms
h_count_re = re.compile(
    r"^(.*)(?:SUCCESS|INFO)\s+Booked (\d+) Histogram\(s\) :\s+([\s\w=-]*)"
)


def _parse_histos_summary(lines, pos):
    """
    Temporary.
    Note similar functions exist in Gaudi.
    This one was adjusted to not raise RuntimeError when histogram is not displayed.
    Extract the histograms infos from the lines starting at pos.
    Returns the position of the first line after the summary block.
    """
    global h_count_re
    h_table_head = re.compile(
        r'(?:SUCCESS|INFO)\s+(1D|2D|3D|1D profile|2D profile) histograms in directory\s+"(\w*)"'
    )
    h_short_summ = re.compile(r"ID=([^\"]+)\s+\"([^\"]*)\"\s+(.*)")

    nlines = len(lines)

    # decode header
    m = h_count_re.search(lines[pos])
    name = m.group(1).strip()
    total = int(m.group(2))
    header = {}
    for k, v in [x.split("=") for x in m.group(3).split()]:
        header[k] = int(v)
    pos += 1
    header["Total"] = total

    summ = {}
    while pos < nlines:
        m = h_table_head.search(lines[pos])
        if m:
            t, d = m.groups(1)  # type and directory
            t = t.replace(" profile", "Prof")
            pos += 1
            if pos < nlines:
                l = lines[pos]
            else:
                l = ""
            cont = {}
            if l.startswith(" | ID"):
                # table format
                titles = [x.strip() for x in l.split("|")][1:]
                pos += 1
                while pos < nlines and lines[pos].startswith(" |"):
                    l = lines[pos]
                    values = [x.strip() for x in l.split("|")][1:]
                    hcont = {}
                    for i in range(len(titles)):
                        hcont[titles[i]] = values[i]
                    cont[hcont["ID"]] = hcont
                    pos += 1
            elif l.startswith(" ID="):
                while pos < nlines and lines[pos].startswith(" ID="):
                    values = [
                        x.strip() for x in h_short_summ.search(lines[pos]).groups()
                    ]
                    cont[values[0]] = values
                    pos += 1
            else:  # not interpreted
                pass
            if d not in summ:
                summ[d] = {}
            summ[d][t] = cont
            summ[d]["header"] = header
        else:
            break
    if not summ:
        # If the full table is not present, we use only the header
        summ[name] = {"header": header}
    return summ, pos


def find_histos_summaries(stdout):
    """
    Scan stdout to find ROOT Histogram summaries and digest them.
    """
    outlines = stdout.splitlines() if hasattr(stdout, "splitlines") else stdout
    nlines = len(outlines) - 1
    summaries = {}
    global h_count_re

    pos = 0
    while pos < nlines:
        summ = {}
        # find first line of block:
        match = h_count_re.search(outlines[pos])
        while pos < nlines and not match:
            pos += 1
            match = h_count_re.search(outlines[pos])
        if match:
            summ, pos = _parse_histos_summary(outlines, pos)
        summaries.update(summ)
    return summaries
