###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import subprocess
from typing import Generator

import pytest
from LHCbTesting.utils import find_counters_summaries

# Extra plugin for LHCb applications

# The fixtures include:
# - counters: Captures counters in standard output


@pytest.fixture(scope="class")
def counters(
    completed_process: subprocess.CompletedProcess,
) -> Generator[dict, None, None]:
    stdout = completed_process.stdout if completed_process else None
    counters = find_counters_summaries(stdout)

    yield counters
