###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiConf.LbExec import Options
from PyConf.application import configure, configure_input, mdf_writer
from PyConf.control_flow import CompositeNode


def mdf(options: Options):
    """
    Merges output of HLT2 reprocessing jobs in production
    """
    writer = mdf_writer(
        options.output_file,
        "/Event/DAQ/RawEvent",
        compression=options.compression.level,
        compression_alg=options.compression.algorithm.value,
    )
    node = CompositeNode("MDFMerger", [writer])
    config = configure_input(options)
    config.update(configure(options, node))
    return config
