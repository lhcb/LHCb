###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import os
import sys
from itertools import chain

import yaml


def do_export(format, opts) -> bytes:
    known_formats = {
        ".opts": lambda opts: b"\n".join(
            map(lambda x: f"{x[0]} = {x[1]};".encode(), opts.items())
        )
        + b"\n",
        ".json": lambda opts: json.dumps(opts, indent=4).encode(),
        ".yaml": lambda opts: yaml.safe_dump(opts).encode(),
    }
    known_formats[".yml"] = known_formats[".yaml"]

    if format not in known_formats:
        raise NotImplementedError(
            f"Unrecognised format {format!r}, known formats are {set(known_formats)}"
        )

    return known_formats[format](opts)


def config2opts(configurables, with_defaults=False):
    if not configurables.values():
        raise RuntimeError(f"Expected configurables.values()")
    configurables = configurables.values()

    from GaudiKernel.Proxy.Configurable import Configurable, applyConfigurableUsers

    applyConfigurableUsers()

    dict_opts_old = configurables2dict(
        list(Configurable.allConfigurables.values()), with_defaults
    )
    dict_opts = configurables2dict(configurables, with_defaults)

    conflicts = [
        n
        for n in set(dict_opts).intersection(dict_opts_old)
        if dict_opts[n] != dict_opts_old[n]
    ]
    if conflicts:
        conflicts.sort()
        print(
            "ERROR: Some properties are set in old and new style configuration",
            file=sys.stderr,
        )
        print("WARNING: name: old -> new", file=sys.stderr)
        for n in conflicts:
            print(f"WARNING {n}: {dict_opts_old[n]} -> {dict_opts[n]}", file=sys.stderr)
        return 10

    opts = {**dict_opts_old, **dict_opts}
    opts["ApplicationMgr.AppName"] = f'"{os.environ["GAUDIAPPNAME"]}"'
    opts["ApplicationMgr.AppVersion"] = (
        f'"{os.environ.get("GAUDIAPPVERSION", "Unknown")}"'
    )
    return opts


def configurables2dict(configurables, with_defaults):
    """Convert a list of configurables into a dictionary for Gaudi

    Args:
       - configurables: list of configurables to be passed to Gaudi.
       - with_defaults: flag to set the default values.

    Returns:
       - opts: dictionary containing all the configurables.
    """
    from GaudiKernel.Proxy.Configurable import Configurable

    opts = {}
    for c in configurables:
        if hasattr(c, "__opt_properties__"):
            opts.update(c.__opt_properties__(with_defaults))
            continue

        to_iterate = [c.getValuedProperties().items()]
        if with_defaults:
            to_iterate.insert(0, c.getDefaultProperties().items())

        for p, v in chain(*to_iterate):
            if hasattr(Configurable, "PropertyReference") and isinstance(
                v, Configurable.PropertyReference
            ):
                v = v.__resolve__()
            if isinstance(v, str):
                v = '"%s"' % v.replace('"', '\\"')
            elif hasattr(v, "__opt_value__"):
                v = v.__opt_value__()

            opts[f"{c.name()}.{p}"] = str(v)
    return opts
