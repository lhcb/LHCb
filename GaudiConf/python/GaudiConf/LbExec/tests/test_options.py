###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest

from GaudiConf.LbExec import Options
from GaudiConf.LbExec.options import _expand_braces

DEFAULT_KWARGS = dict(
    data_type="Upgrade",
    simulation=True,
    output_file="example.root",
)


def test_multiprocessing_defaults():
    options = Options(**DEFAULT_KWARGS, evt_max=100)
    assert options.n_threads == 1
    assert options.n_event_slots == 1

    options = Options(**DEFAULT_KWARGS, evt_max=100, n_threads=4)
    assert options.n_threads == 4
    assert options.n_event_slots == 5

    options = Options(**DEFAULT_KWARGS, evt_max=100, n_event_slots=7)
    assert options.n_threads == 1
    assert options.n_event_slots == 7

    options = Options(**DEFAULT_KWARGS, evt_max=100, n_threads=4, n_event_slots=2)
    assert options.n_threads == 4
    assert options.n_event_slots == 2


def test_input_data_brace_expansion(tmp_path):
    for i in range(100):
        (tmp_path / f"{i}.MDST").write_text("")

    options = Options(
        **DEFAULT_KWARGS,
        input_type="ROOT",
        input_files=str(tmp_path / "{0,1,2,3}.MDST"),
    )
    assert options.input_files == [str(tmp_path / f"{i}.MDST") for i in range(4)]

    options = Options(
        **DEFAULT_KWARGS,
        input_type="ROOT",
        input_files=str(tmp_path / "{{1,4},9}5.MDST"),
    )
    assert options.input_files == [str(tmp_path / f"{i}.MDST") for i in [15, 95, 45]]


def test_input_data_brace_expansion_sequence(tmp_path):
    for i in range(100):
        (tmp_path / f"{i}.MDST").write_text("")

    options = Options(
        **DEFAULT_KWARGS, input_type="ROOT", input_files=str(tmp_path / "{0..10}.MDST")
    )
    assert options.input_files == [str(tmp_path / f"{i}.MDST") for i in range(11)]


def test_input_data_brace_expansion_nested():
    prefix = "root://example.invalid//lhcb/"
    pattern = prefix + "{{0..00003}{0..9},{3..00009}{0..9}}"
    pattern += ".{Charm{,CompleteEvent},EW}.{DST,ROOT}"
    options = Options(**DEFAULT_KWARGS, input_type="ROOT", input_files=pattern)
    reference = [f"{prefix}{i:06d}.Charm.DST" for i in range(100)]
    reference += [f"{prefix}{i:06d}.Charm.ROOT" for i in range(100)]
    reference += [f"{prefix}{i:06d}.CharmCompleteEvent.DST" for i in range(100)]
    reference += [f"{prefix}{i:06d}.CharmCompleteEvent.ROOT" for i in range(100)]
    reference += [f"{prefix}{i:06d}.EW.DST" for i in range(100)]
    reference += [f"{prefix}{i:06d}.EW.ROOT" for i in range(100)]
    assert set(options.input_files) == set(reference)


def test_input_data_glob(tmp_path):
    for i in range(100):
        (tmp_path / f"{i}.DST").write_text("")

    options = Options(
        **DEFAULT_KWARGS,
        input_type="ROOT",
        input_files=str(tmp_path / "*.DST"),
    )
    assert set(options.input_files) == {str(tmp_path / f"{i}.DST") for i in range(100)}

    options = Options(
        **DEFAULT_KWARGS,
        input_type="ROOT",
        input_files=str(tmp_path / "*0.DST"),
    )
    assert set(options.input_files) == {
        str(tmp_path / f"{i}.DST") for i in range(0, 100, 10)
    }


def test_input_data_xrootd():
    options = Options(
        **DEFAULT_KWARGS,
        input_type="ROOT",
        input_files="root://eos.invalid//lhcb/{1,2,3}.root",
    )
    assert options.input_files == [
        "root://eos.invalid//lhcb/1.root",
        "root://eos.invalid//lhcb/2.root",
        "root://eos.invalid//lhcb/3.root",
    ]

    with pytest.raises(NotImplementedError):
        Options(
            **DEFAULT_KWARGS,
            input_type="ROOT",
            input_files="root://eos.invalid//lhcb/*.root",
        )


def test_input_data_brace_expansion_and_glob(tmp_path):
    for i in range(1000):
        (tmp_path / f"{i}.MDST").write_text("")

    options = Options(
        **DEFAULT_KWARGS,
        input_type="ROOT",
        input_files=str(tmp_path / "{0,1,2,3}*.MDST"),
    )
    assert set(options.input_files) == {
        str(tmp_path / f"{i}.MDST")
        for i in list(range(0, 4)) + list(range(10, 40)) + list(range(100, 400))
    }

    options = Options(
        **DEFAULT_KWARGS,
        input_type="ROOT",
        input_files=str(tmp_path / "*{{1,4},9}5.MDST"),
    )
    assert set(options.input_files) == {
        str(tmp_path / f"{j or ''}{i}.MDST") for i in [15, 45, 95] for j in range(10)
    }

    options = Options(
        **DEFAULT_KWARGS,
        input_type="ROOT",
        input_files=str(tmp_path / "{{1,*},9}5.MDST"),
    )
    assert set(options.input_files) == {
        str(tmp_path / f"{j or ''}{i}.MDST") for i in [15, 95] for j in range(10)
    } | {str(tmp_path / f"{i}.MDST") for i in range(1000) if str(i)[-1] == "5"}


def test_missing_input_data(tmp_path):
    for i in range(11):
        (tmp_path / f"{i}.MDST").write_text("")

    err_match = r"No input files found matching.*/100\.\*DST"
    with pytest.raises(ValueError, match=err_match):
        Options(
            **DEFAULT_KWARGS,
            input_type="ROOT",
            input_files=str(tmp_path / "{1,10,100}.*DST"),
        )


def test_evt_max_not_set():
    with pytest.raises(ValueError, match="'evt_max' must be >=0"):
        Options(**DEFAULT_KWARGS)


def test_no_input_data():
    with pytest.raises(ValueError, match="'input_files' is required"):
        Options(**DEFAULT_KWARGS, input_type="ROOT")


def test_expand_braces():
    text_to_expand = "{{pine,}apples,oranges} are {tasty,disgusting} to m{,}e }{"
    assert list(_expand_braces(text_to_expand)) == [
        "pineapples are tasty to me }{",
        "oranges are tasty to me }{",
        "apples are tasty to me }{",
        "pineapples are disgusting to me }{",
        "oranges are disgusting to me }{",
        "apples are disgusting to me }{",
    ]


@pytest.mark.parametrize(
    "text_to_expand,expected",
    [
        ["{0}", ["{0}"]],
        ["{0..0}", ["0"]],
        ["{0..0..0}", ["0"]],
        ["{0..0..0..0}", ["{0..0..0..0}"]],
        ["{1..6}", ["1", "2", "3", "4", "5", "6"]],
        ["{01..6..2}", ["01", "03", "05"]],
        ["{1..6..2}", ["1", "3", "5"]],
        ["{0..6..2}", ["0", "2", "4", "6"]],
        ["{0..06..2}", ["00", "02", "04", "06"]],
        ["{6..0..2}", ["6", "4", "2", "0"]],
        ["{6..0..-2}", ["6", "4", "2", "0"]],
        ["{6..1..2}", ["6", "4", "2"]],
        ["{6..1..-2}", ["6", "4", "2"]],
        ["{10..0..4}", ["10", "6", "2"]],
        ["{10..00..4}", ["10", "06", "02"]],
        ["{10..00..2}", ["10", "08", "06", "04", "02", "00"]],
        ["{10..0..02}", ["10", "8", "6", "4", "2", "0"]],
        ["{10..8..1}", ["10", "9", "8"]],
        ["{10..08..1}", ["10", "09", "08"]],
        ["{10..-08..5}", ["010", "005", "000", "-05"]],
        ["{10..+08..1}", ["10", "9", "8"]],
        ["{10..08..1..5}", ["{10..08..1..5}"]],
        ["{10..a..1..5}", ["{10..a..1..5}"]],
        ["{10..08..1..5,}", ["10..08..1..5"]],
        ["{10..a..1..5,}", ["10..a..1..5"]],
        ["{50..-2..9}", ["50", "41", "32", "23", "14", "5"]],
        ["{050..-2..9}", ["050", "041", "032", "023", "014", "005"]],
        ["{050..02..9}", ["050", "041", "032", "023", "014", "005"]],
        ["{50..02..9}", ["50", "41", "32", "23", "14", "05"]],
        ["{50..0-2..9}", ["{50..0-2..9}"]],
        ["{6..1..4}{10..1..3}", ["610", "67", "64", "61", "210", "27", "24", "21"]],
        ["{b,{6..1..2},a}", ["b", "6", "4", "2", "a"]],
        ["{10..1..2,10..1..2}", ["10..1..2", "10..1..2"]],
        ["{a..d}", ["a", "b", "c", "d"]],
        ["{z..x}", ["z", "y", "x"]],
        ["{a..x..3}", ["a", "d", "g", "j", "m", "p", "s", "v"]],
        ["{x..a..9}", ["x", "o", "f"]],
        ["{z..9..3}", ["{z..9..3}"]],
        ["{a..aa}", ["{a..aa}"]],
        ["{a..aa..3}", ["{a..aa..3}"]],
        ["0{..,}", ["0..", "0"]],
        ["{{0..1}..2,}", ["0..2", "1..2"]],
        ["{0..0{0..A},}", ["0..0{0..A}"]],
        ["{2..{0..1},}", ["2..0", "2..1"]],
        ["{{,0..0}0..0}", ["{0..0}", "{0..00..0}"]],
    ],
)
def test_expand_braces_sequences(text_to_expand, expected):
    result = set(filter(None, _expand_braces(text_to_expand)))
    assert result == set(expected), text_to_expand
