###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import VPRetinaClusterDecoder
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    default_raw_banks,
)
from PyConf.control_flow import CompositeNode, NodeLogic

options = ApplicationOptions(_enabled=False)
config = configure_input(options)

decoder = VPRetinaClusterDecoder(RawBanks=default_raw_banks("VPRetinaCluster"))

cf_node = CompositeNode(
    "retina_decoding", [decoder], combine_logic=NodeLogic.LAZY_AND, force_order=True
)

config.update(configure(options, cf_node))
