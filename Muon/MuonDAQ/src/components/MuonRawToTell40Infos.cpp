/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/Condition.h"
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/ODIN.h"
#include "Event/PrHits.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Transformer.h"
#include "MuonDAQ/MuonDAQDefinitions.h"
#include "MuonDAQ/MuonTell40PciInfoContainer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonDet/MuonTell40LinksMap.h"

#include <boost/numeric/conversion/cast.hpp>

#include <array>
#include <bitset>
#include <functional>
#include <optional>
#include <string>
#include <vector>

/**
 *  This is the muon reconstruction algorithm
 *  This just crosses the logical strips back into pads
 */

using namespace Muon::DAQ;
using namespace LHCb::Detector;
namespace LHCb::MuonUpgrade::DAQ {

  //-----------------------------------------------------------------------------
  // Implementation file for class : RawToHits
  //-----------------------------------------------------------------------------
  class RawToTell40Infos final
      : public Algorithm::Transformer<MuonTell40PciInfos( const RawBank::View&, const RawBank::View&,
                                                          const RawBank::View&, const DeMuonDetector&,
                                                          const Tell40LinksMap&, const LHCb::ODIN& odin ),
                                      Algorithm::Traits::usesConditions<DeMuonDetector, Tell40LinksMap>> {
  public:
    RawToTell40Infos( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;
    StatusCode finalize() override;

    MuonTell40PciInfos operator()( const RawBank::View&, const RawBank::View&, const RawBank::View&,
                                   const DeMuonDetector&, const Tell40LinksMap&,
                                   const LHCb::ODIN& odin ) const override;

  private:
    std::array<unsigned int, 24> ReadMapFromData( unsigned int active_links, ByteType data_fiber[3],
                                                  unsigned int& number_of_readout_fibers ) const;
    mutable unsigned int         local_bank_type[44][10] = {};
    // mutable unsigned int                                local_hits_counters[44][16]       = {};
    // mutable unsigned int                                local_wrong_hits_counters[44][16] = {};
    // mutable unsigned int                                local_acquired_frame[44][16]      = {};
    // mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_invalid_add{this, "invalid add"};
    // mutable Gaudi::Accumulators::Counter<>              m_Tell40Size[44]{
    //    {this, "Tell_001_0 size"}, {this, "Tell_001_1 size"}, {this, "Tell_002_0 size"}, {this, "Tell_002_1 size"},
    //    {this, "Tell_003_0 size"}, {this, "Tell_003_1 size"}, {this, "Tell_004_0 size"}, {this, "Tell_004_1 size"},
    //    {this, "Tell_005_0 size"}, {this, "Tell_005_1 size"}, {this, "Tell_006_0 size"}, {this, "Tell_006_1 size"},
    //    {this, "Tell_007_0 size"}, {this, "Tell_007_1 size"}, {this, "Tell_008_0 size"}, {this, "Tell_008_1 size"},
    //    {this, "Tell_009_0 size"}, {this, "Tell_009_1 size"}, {this, "Tell_010_0 size"}, {this, "Tell_010_1 size"},
    //    {this, "Tell_011_0 size"}, {this, "Tell_011_1 size"}, {this, "Tell_012_0 size"}, {this, "Tell_012_1 size"},
    //    {this, "Tell_013_0 size"}, {this, "Tell_013_1 size"}, {this, "Tell_014_0 size"}, {this, "Tell_014_1 size"},
    //    {this, "Tell_015_0 size"}, {this, "Tell_015_1 size"}, {this, "Tell_016_0 size"}, {this, "Tell_016_1 size"},
    //    {this, "Tell_017_0 size"}, {this, "Tell_017_1 size"}, {this, "Tell_018_0 size"}, {this, "Tell_018_1 size"},
    //    {this, "Tell_019_0 size"}, {this, "Tell_019_1 size"}, {this, "Tell_020_0 size"}, {this, "Tell_020_1 size"},
    //    {this, "Tell_021_0 size"}, {this, "Tell_021_1 size"}, {this, "Tell_022_0 size"}, {this, "Tell_022_1 size"}};
    // mutable unsigned int                               local_edac_sec[44][16]={};
    // mutable unsigned int                               local_edac_ded[44][16]={};
    mutable unsigned int local_frame_in_error[44][16] = {};

    Gaudi::Property<bool> m_print_stat{ this, "PrintStat", true };
    Gaudi::Property<bool> m_print_details{ this, "PrintDetails", true };
  };

  DECLARE_COMPONENT_WITH_ID( RawToTell40Infos, "MuonRawToTell40Infos" )

  //=============================================================================
  // Standard constructor
  //=============================================================================
  RawToTell40Infos::RawToTell40Infos( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue{ "RawBanks", "DAQ/RawBanks/Muon" },
                       KeyValue{ "SpecialRawBanks", "DAQ/RawBanks/MuonError" },
                       KeyValue{ "IDLERawBanks", "DAQ/RawBanks/DaqErrorIdleBXIDCorrupted" },
                       KeyValue{ "MuonDetectorLocation", DeMuonLocation::Default },
                       KeyValue{ "Tell40LinksMapByRun", "AlgorithmSpecific-" + name + "-Tell40LinksMapByRun" },
                       KeyValue{ "ODINLocation", LHCb::ODINLocation::Default } },
                     KeyValue{ "Tell40InfoContainer", MuonTell40PciInfoContainerLocation::Default } ) {}

  //=============================================================================
  // Initialisation
  //=============================================================================
  StatusCode RawToTell40Infos::initialize() {

    auto sc = Transformer::initialize();
    if ( !sc ) return sc;

    addConditionDerivation<Tell40LinksMap( const DeMuonDetector&, const DeLHCb& )>(
        { DeMuonLocation::Default, LHCb::standard_geometry_top }, inputLocation<Tell40LinksMap>() );
    return sc;
  }

  StatusCode RawToTell40Infos::finalize() {
    return Transformer::finalize().andThen( [&]() {
      if ( m_print_stat ) {

        info() << " Number of corrupted bank per type " << endmsg;
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 " << std::setw( 2 ) << i / 2 + 1 << " PCI number " << i % 2 << "-------"; // endmsg;
          for ( int j = 0; j < 10; j++ ) { info() << "  " << std::setw( 8 ) << local_bank_type[i][j] << "  "; }
          info() << endmsg;
        }

        info() << " Number of corrupted link per bank " << endmsg;
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 " << std::setw( 2 ) << i / 2 + 1 << " PCI number " << i % 2 << "-------"; // endmsg;
          info() << " list of problematic links ";
          for ( int j = 0; j < 16; j++ ) {
            if ( local_frame_in_error[i][j] > 0 ) info() << j << " " << local_frame_in_error[i][j] << " ";
          }
          info() << endmsg;
        }
      }
    } );
  }

  //=============================================================================
  // Main execution
  //=============================================================================
  // MuonTell40PciInfos RawToTell40Infos::operator()( const EventContext& evtCtx, const RawBank::View& muon_banks,
  MuonTell40PciInfos RawToTell40Infos::operator()( const RawBank::View& muon_banks, const RawBank::View& muonErrorBanks,
                                                   const RawBank::View& allIDLEBanks, const DeMuonDetector& det,
                                                   const Tell40LinksMap& linksMap, const LHCb::ODIN& odin ) const {

    if ( msgLevel( MSG::DEBUG ) ) { debug() << "==> Execute the decoding" << endmsg; }
    assert( det.stations() <= 4 );

    std::map<int, LHCb::span<RawBank const* const>> allBanks;
    allBanks[0] = muon_banks;
    allBanks[1] = muonErrorBanks;
    allBanks[2] = allIDLEBanks;

    if ( m_print_details )
      debug() << " odin info " << odin.bunchId() << " " << odin.eventNumber() << " " << odin.orbitNumber() << " "
              << odin.gpsTime() << " " << endmsg;

    auto infos = MuonTell40PciInfos();
    // first decode special muon error bank
    bool m_link_in_error[44][16] = { { false } };
    for ( int bt = 0; bt < 2; bt++ ) {
      if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "type size " << allBanks[bt].size() << endmsg; }
      for ( const auto& raw_bank : allBanks[bt] ) {
        if ( RawBank::MagicPattern != raw_bank->magic() ) {
          //++m_bad_magic;
          continue;
        }
        bool              bank_corrupted = false;
        RawBank::BankType type           = raw_bank->type();
        unsigned int      tell40PCI      = raw_bank->sourceID() & 0x00FF;
        unsigned int      TNumber        = tell40PCI / 2 + 1;
        unsigned int      PCINumber      = tell40PCI % 2;
        // unsigned int      stationOfTell40 = det.getUpgradeDAQInfo()->whichstationTell40( TNumber - 1 );
        unsigned int active_links = det.getUpgradeDAQInfo()->getNumberOfActiveLinkInTell40PCI( TNumber, PCINumber );
        if ( (unsigned int)raw_bank->size() < 1 ) {
          //++m_bank_too_short;
          bank_corrupted = true;
          if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "corrupted bank size " << bank_corrupted << endmsg; }
          continue;
        }
        auto         range8             = raw_bank->range<ByteType>();
        auto         range_data         = range8.subspan( 1 );
        unsigned int link_start_pointer = 0;
        // unsigned int regionOfLink       = 99;
        // unsigned int quarterOfLink      = 99;
        unsigned int ZS_applied   = ( range8[0] & 0x05 );
        unsigned int EDAC_applied = ( range8[0] & 0x08 ) >> 3;
        unsigned int synch_evt    = ( range8[0] & 0x10 ) >> 4;
        unsigned int align_info   = ( range8[0] & 0x20 ) >> 5;
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << " Tell40 " << TNumber << " " << PCINumber << endmsg;
          debug() << "header of bank reports :" << endmsg;
          debug() << " ZS type " << ZS_applied << " "
                  << " EDAC enabled " << EDAC_applied << " " << endmsg;
          debug() << " Synch event " << synch_evt << " aligned fiber info stored " << align_info << endmsg;
        }
        if ( synch_evt ) continue;
        if ( (unsigned int)raw_bank->size() < 1 + 3 * align_info ) {
          //++m_bank_too_short;
          bank_corrupted = true;
          continue;
        }

        unsigned int                 number_of_readout_fibers = 0;
        std::array<unsigned int, 24> map_connected_fibers     = { 0 };

        // need to determine the true enabled links, possible only or using online condition or data itself if
        // align_info is true

        bool         skipmap = false;
        unsigned int enabled = 0;
        if ( !skipmap && linksMap.getInitStatus( TNumber, PCINumber ) == Tell40LinksMap::InitStatus::OK ) {
          connectionMap test       = linksMap.getAllConnection( TNumber, PCINumber );
          number_of_readout_fibers = test.connectedLinks;
          map_connected_fibers     = test.map;
          enabled                  = linksMap.getEnabled( TNumber, PCINumber );
        } else if ( align_info ) {

          auto     range_fiber   = range8.subspan( 1, 3 );
          ByteType align_data[3] = { range_fiber[0], range_fiber[1], range_fiber[2] };
          map_connected_fibers   = ReadMapFromData( active_links, align_data, number_of_readout_fibers );
          if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "tot fibers " << number_of_readout_fibers << " " << endmsg; }
          enabled = ( range_fiber[0] << 16 ) | ( range_fiber[1] << 8 ) | range_fiber[2];
          for ( int io = 0; io < 24; io++ ) { verbose() << "map from raw bank " << map_connected_fibers[io] << endmsg; }
        } else if ( !align_info ) {
          for ( unsigned int i = 0; i < active_links; i++ ) {
            map_connected_fibers[i] = i;
            enabled                 = enabled | ( 0x1 << i );
          }
          number_of_readout_fibers = active_links;
        }

        link_start_pointer = ( align_info == 1 ) ? link_start_pointer + 3 : link_start_pointer;

        unsigned int dec = det.getUpgradeDAQInfo()->getDeclaredLinks( TNumber, PCINumber );
        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << " PCI Info " << TNumber << " " << PCINumber << " " << bt << " " << dec << " " << enabled
                    << endmsg;
        }

        if ( bt == 0 ) {
          unsigned int      LinkInError = 0;
          MuonTell40PciInfo pci_info( TNumber, PCINumber, active_links, dec, enabled, type, LinkInError );
          infos.emplace_back( pci_info );
        } else if ( bt == 1 ) {
          unsigned current_pointer = link_start_pointer;

          auto         range_errorLink = range_data.subspan( current_pointer, 3 );
          unsigned int LinkInError     = 0;

          if ( msgLevel( MSG::DEBUG ) )
            debug() << "link in error  " << current_pointer << " " << std::bitset<8>( range_errorLink[0] ) << " "
                    << std::bitset<8>( range_errorLink[1] ) << " " << std::bitset<8>( range_errorLink[2] ) << endmsg;
          for ( int i = 0; i < 8; i++ ) {
            if ( ( range_errorLink[0] >> i ) & 0x1 ) {
              m_link_in_error[( TNumber - 1 ) * 2 + PCINumber][16 + i] = true;
              if ( msgLevel( MSG::DEBUG ) )
                debug() << "found desync link " << ( TNumber - 1 ) * 2 + PCINumber << " " << 16 + i << endmsg;
            }
            if ( ( range_errorLink[1] >> i ) & 0x1 ) {
              m_link_in_error[( TNumber - 1 ) * 2 + PCINumber][8 + i] = true;
              if ( msgLevel( MSG::DEBUG ) )
                debug() << "found desync link " << ( TNumber - 1 ) * 2 + PCINumber << " " << 8 + i << endmsg;
            }
            if ( ( range_errorLink[2] >> i ) & 0x1 ) {
              m_link_in_error[( TNumber - 1 ) * 2 + PCINumber][i] = true;
              if ( msgLevel( MSG::DEBUG ) )
                debug() << "found desync link " << ( TNumber - 1 ) * 2 + PCINumber << " " << i << endmsg;
            }
          }
          LinkInError = ( range_errorLink[1] << 8 ) | ( range_errorLink[2] );
          // info()<<" error links "<<curr_byte1<<" "<<curr_byte2<<endmsg;
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << "summary " << TNumber << " " << PCINumber << " " << type << " " << LinkInError << endmsg;
            for ( int i = 0; i < 24; i++ ) {
              if ( m_link_in_error[( TNumber - 1 ) * 2 + PCINumber][i] )
                debug() << " error detected " << m_link_in_error[( TNumber - 1 ) * 2 + PCINumber][i] << endmsg;
            }
          }
          MuonTell40PciInfo pci_info( TNumber, PCINumber, active_links, dec, enabled, type, LinkInError );
          infos.emplace_back( pci_info );
        }
      }
    }

    // now the rest of problematic banks
    for ( int bt = 2; bt < 3; bt++ ) {
      for ( const auto& raw_bank : allBanks[bt] ) {

        // need to look for muon Tell40 sourceID
        unsigned int sourceID = raw_bank->sourceID();
        if ( sourceID < 0x6800 || sourceID > 0x70FF ) continue;

        RawBank::BankType type = raw_bank->type();
        if ( msgLevel( MSG::DEBUG ) ) { debug() << " found a muon error bank of type " << type << endmsg; }
        unsigned int tell40PCI = raw_bank->sourceID() & 0x00FF;
        unsigned int TNumber   = tell40PCI / 2 + 1;
        unsigned int PCINumber = tell40PCI % 2;
        // unsigned int stationOfTell40 = det.getUpgradeDAQInfo()->whichstationTell40( TNumber - 1 );
        unsigned int active_links = det.getUpgradeDAQInfo()->getNumberOfActiveLinkInTell40PCI( TNumber, PCINumber );

        unsigned int LinkInError = 0;

        local_bank_type[tell40PCI][(unsigned int)type - (unsigned int)RawBank::DaqErrorFragmentThrottled]++;
        // print error info
        if ( m_print_details )
          info() << " bank sourceID " << MSG::hex << sourceID << MSG::dec << " Tell Number  " << TNumber
                 << " PCINumber " << PCINumber << " type " << type << " size" << (unsigned int)raw_bank->size()
                 << endmsg;
        if ( m_print_details )
          info() << " odin info " << odin.bunchId() << " " << odin.eventNumber() << " " << odin.orbitNumber() << " "
                 << odin.gpsTime() << " " << endmsg;
        continue;
        auto range8 = raw_bank->range<ByteType>();

        auto range_data = range8.subspan( 0, raw_bank->size() );

        for ( auto r = range_data.begin(); r < range_data.end(); r++ ) {
          // count_byte++;
          // ByteType     data_copy = *r;
          unsigned int test = *r;
          if ( m_print_details )
            info() << " " << MSG::hex << std::uppercase << std::setfill( '0' ) << std::setw( 2 ) << test;
        }
        if ( m_print_details ) info() << " " << MSG::dec << endmsg;
        unsigned int count_byte = 0;
        unsigned int count_link = 0;

        for ( auto r = range_data.rbegin(); r < range_data.rend(); r++ ) {
          // count_byte++;
          ByteType data_copy = *r;

          unsigned int test = *r;
          if ( count_link > 15 ) break;
          for ( auto bit : single_bits<ByteType>{} ) {
            if ( data_copy & bit ) {
              local_frame_in_error[tell40PCI][count_link]++;

              // fill info
            }

            count_link++;
          }
          // unsigned int d=(data_copy);
          // info
          LinkInError = ( LinkInError | ( test << ( count_byte * 8 ) ) );
          count_byte++;
        }
        //	  if(m_print_details)info()<<" "<<MSG::hex<<std::uppercase <<std::setfill('0') << std::setw(2)<< test;
        MuonTell40PciInfo pci_info( TNumber, PCINumber, active_links, 0xffff, 0xffff, type, LinkInError );
        infos.emplace_back( pci_info );
        // yMuonTell40PciInfo(TNumber,PCINumber,active_links,0xffff,0xffff,type,LinkInError);
        // info()<<"alessia "<<pci_info.Tell40Number()<<" "<<pci_info.PCINumber()<<" "<<pci_info.isInError(0)<<"
        // "<<pci_info.isInError(1)<<" "<<pci_info.isInError(2)<<endmsg;
      } //  if(m_print_details)info()<<" "<<MSG::dec<<endmsg;
    }
    if ( msgLevel( MSG::VERBOSE ) ) { verbose() << " at the end " << infos.size() << endmsg; }
    return infos;
  }

  std::array<unsigned int, 24> RawToTell40Infos::ReadMapFromData( unsigned int active_links, ByteType data_fiber[3],
                                                                  unsigned int& number_of_readout_fibers ) const {

    std::array<unsigned int, 24> map_connected_fibers = { 0 };
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "connected fibers " << std::bitset<8>( data_fiber[0] ) << " " << std::bitset<8>( data_fiber[1] )
                << " " << std::bitset<8>( data_fiber[2] ) << endmsg;
    bool         align_vector[24] = {};
    unsigned int readout_fibers   = 0;
    for ( int i = 0; i < 8; i++ ) {
      if ( ( data_fiber[0] >> i ) & 0x1 ) {
        align_vector[16 + i] = true;
        readout_fibers++;
      }
      if ( ( data_fiber[1] >> i ) & 0x1 ) {
        align_vector[8 + i] = true;
        readout_fibers++;
      }
      if ( ( data_fiber[2] >> i ) & 0x1 ) {
        align_vector[i] = true;
        readout_fibers++;
      }
    }
    if ( msgLevel( MSG::VERBOSE ) ) {
      for ( int i = 0; i < 24; i++ ) { verbose() << i << " " << align_vector[i] << endmsg; }
    }
    unsigned int fib_counter = 0;
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "how many active link " << active_links << endmsg;
    for ( unsigned int i = 0; i < active_links; i++ ) {
      if ( align_vector[i] ) {
        map_connected_fibers[fib_counter] = i;
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << "filling " << fib_counter << " " << map_connected_fibers[fib_counter] << endmsg;
        fib_counter++;
      }
    }
    if ( msgLevel( MSG::VERBOSE ) ) {
      for ( int i = 0; i < 24; i++ ) { verbose() << i << " " << align_vector[i] << endmsg; }
    }
    number_of_readout_fibers = readout_fibers;
    return map_connected_fibers;
  }

} // namespace LHCb::MuonUpgrade::DAQ
