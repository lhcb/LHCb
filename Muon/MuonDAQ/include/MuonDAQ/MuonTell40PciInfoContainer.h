/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <array>
#include <string>
#include <vector>

#include "MuonDAQ/MuonTell40PciInfo.h"
#include "MuonDet/DeMuonDetector.h"

namespace MuonTell40PciInfoContainerLocation {
  inline const std::string Default = "Muon/MuonTell40PciInfos";
}

class MuonTell40PciInfoContainer final {
public:
  MuonTell40PciInfoContainer( std::array<MuonTell40PciInfos, 44> tell40Infos )
      : m_tell40Infos( std::move( tell40Infos ) ) {}

  MuonTell40PciInfosRange infos( unsigned int id ) const {
    return { m_tell40Infos[id].begin(), m_tell40Infos[id].end() };
  }

  const MuonTell40PciInfos& tell40pci( unsigned int id ) const { return m_tell40Infos[id]; };

private:
  // data object stored in TES and to get from algorithms using muon coords
  std::array<MuonTell40PciInfos, 44> m_tell40Infos;
};
