/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <array>
#include <climits>
#include <cstdint>

/*
 *  @author Alessia Satta
 *  @date   2020-05-23
 */
namespace Muon::DAQ {

  using LongType                        = std::uint64_t;
  using ShortType                       = std::uint32_t;
  using ByteType                        = std::uint8_t;
  constexpr unsigned int MaxHitsInFrame = 48;
  constexpr unsigned int MaxTDCsInFrame = 12;
  constexpr unsigned int MaxHitBytes    = MaxHitsInFrame / 8;
  constexpr unsigned int MaxTDCBytes    = MaxTDCsInFrame / 2;

  // let's you iterate through powers of two covered by T
  // this is useful e.g. for bit masking 0001,0010,0100,...
  template <typename T>
  struct single_bits {
    static_assert( std::is_unsigned_v<T> );
    struct Iterator {
      std::size_t         i = 0;
      constexpr T         operator*() const { return T{ 1 } << i; }
      constexpr Iterator& operator++() {
        ++i;
        return *this;
      }
      constexpr bool operator!=( Iterator const& rhs ) const { return i != rhs.i; }
    };
    constexpr Iterator begin() const { return { 0 }; }
    constexpr Iterator end() const { return { sizeof( T ) * CHAR_BIT }; }
  };

} // namespace Muon::DAQ
