/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "FTDAQ/FTDAQHelper.h"
#include "Event/FTLiteCluster.h"
#include "Event/RawBank.h"
#include <Detector/FT/FTSourceID.h>
#include <numeric> // std::accumulate

unsigned int LHCb::FTDAQ::nbFTClusters( LHCb::RawBank::View banks ) {

  // First check whether we actually have banks
  // This is needed as we later check the version of the first bank...
  if ( banks.empty() ) { return 0u; }

  // Simple loop over the banks to roughly estimate total number of clusters
  unsigned int nbClusters = std::accumulate( banks.begin(), banks.end(), 0u,
                                             []( unsigned int sum, auto& bank ) { return sum + bank->size(); } );

  // Note that we check only the version of the first bank, supposing they
  // all have a consistent version
  int bankversion = ( *banks.begin() )->version();

  if ( bankversion == 2 || bankversion == 3 ) {
    // Bank size is roughly half the number of clusters and includes headers
    nbClusters *= 0.4;
  } else {
    // Bank size is given in bytes. There are 2 bytes per cluster.
    // 4 bytes are removed for the header.
    // Note that this overestimates slightly the number of clusters
    // due to bank padding in 32b. For v5, it further overestimates the
    // number of clusters due to the merging of clusters.
    nbClusters = nbClusters / 2 - 2u;
  }

  return nbClusters;
}

unsigned int LHCb::FTDAQ::nbFTDigits( LHCb::RawBank::View banks ) {
  // Simple loop over the banks to roughly estimate total number of digits
  unsigned int nbDigits = std::accumulate( banks.begin(), banks.end(), 0u,
                                           []( unsigned int sum, auto& bank ) { return sum + bank->size(); } );
  return nbDigits * 128 * 16;
}

template <typename Iter>
void LHCb::FTDAQ::reverse_each_module( Iter first, Iter last ) {
  constexpr auto is_in_module = []( LHCb::Detector::FTChannelID::ModuleID mod ) {
    return [mod]( const LHCb::FTLiteCluster cluster ) { return cluster.channelID().module() == mod; };
  };
  for ( unsigned int iMod = 0; iMod < 5; ++iMod ) {
    auto finish = std::partition_point( first, last, is_in_module( LHCb::Detector::FTChannelID::ModuleID{ iMod } ) );
    std::reverse( first, finish ); // swap clusters in module
    first = finish;
  }
  assert( std::all_of( first, last, is_in_module( LHCb::Detector::FTChannelID::ModuleID{ 5u } ) ) &&
          "Remaining partition should all be in module 5..." );
  std::reverse( first, last );
}

template <typename Container, typename Fun>
void LHCb::FTDAQ::for_each_quadrant( Container& c, Fun&& f ) {
  for ( uint16_t iQuarter = 0; iQuarter < c.nSubDetectors(); ++iQuarter ) {
    auto range = c.range_( iQuarter );
    f( range.first, range.second, iQuarter );
  }
}

void LHCb::FTDAQ::orderInX( FTLiteClusters& clus ) {
  for_each_quadrant( clus, []( auto first, auto last, auto quaIdx ) {
    if ( first == last ) return;
    // Swap clusters within modules: now modules are beam pipe -> edge
    if ( LHCb::Detector::FTSourceID::isReversedInDecoding( quaIdx ) ) reverse_each_module( first, last );
    // Swap clusters within full quadrant
    if ( LHCb::Detector::FTChannelID::is_right_from_qidx( quaIdx ) ) { // quadrant==0 or 2
      //      if ((quaIdx & 1) == 0 ) {    // quadrant==0 or 2
      std::reverse( first, last ); // swap clusters in quadrant
    }
  } );
}

//---LoH: unused function, but shows another way to sort stuff.
//---     It is typically more useful in Allen
template <bool ascendingX>
bool sortingPredicate( const LHCb::FTLiteCluster& a, const LHCb::FTLiteCluster& b ) {
  // AscendingX is true if going from centre to edge of the quarter is going towards larger X values.
  // It is equivalent to isLeft().
  // localChannelIdx_quarter allows to sort cluster references by ascending 'pseudoChannel', i.e.
  // making sure that the vector is now properly sorted in the direction of ascending modules.
  if constexpr ( ascendingX ) {
    return a.channelID().localChannelIdx_quarter() < b.channelID().localChannelIdx_quarter();
  }
  return a.channelID().localChannelIdx_quarter() > b.channelID().localChannelIdx_quarter();
}
