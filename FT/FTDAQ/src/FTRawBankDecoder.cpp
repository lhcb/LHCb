/***************************************************************************** \
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <range/v3/view/subrange.hpp>
#include <range/v3/view/transform.hpp>

#include "Gaudi/Accumulators/Histogram.h"
#include <Gaudi/Property.h>

#include "Event/ODIN.h"
#include <DetDesc/Condition.h>
#include <Detector/FT/FTSourceID.h>
#include <Event/FTLiteCluster.h>
#include <Event/RawBank.h>
#include <LHCbAlgs/Transformer.h>

#include <FTDAQ/FTDAQHelper.h>
#include <FTDAQ/FTReadoutMap.h>

#include <algorithm>
#include <netinet/in.h>

#include "GaudiKernel/IDataProviderSvc.h"
#include "Kernel/STLExtensions.h"
#include <GaudiKernel/StdArrayAsProperty.h>

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;
namespace FTRawBank  = LHCb::Detector::FT::RawBank;

/** @class FTRawBankDecoder FTRawBankDecoder.h
 *  Decode the FT raw bank into FTLiteClusters
 *
 *  @author Olivier Callot
 *  @date   2012-05-11
 */
class FTRawBankDecoder
    : public LHCb::Algorithm::Transformer<FTLiteClusters( const EventContext&, const LHCb::ODIN&,
                                                          const LHCb::RawBank::View&, const FTReadoutMap& ),
                                          LHCb::Algorithm::Traits::usesConditions<FTReadoutMap>> {
public:
  /// Standard constructor
  FTRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  FTLiteClusters operator()( const EventContext& evtCtx, const LHCb::ODIN& odin, const LHCb::RawBank::View& banks,
                             const FTReadoutMap& readoutMap ) const override;

private:
  // Force decoding of v5 as v4 for testing of v5 encoding; expert use only
  Gaudi::Property<bool> m_decodeV5AsV4{ this, "DecodeV5AsV4", false, "Decode v5 as v4" };
  // Force not doing anything with large clusters to study saturation effects
  Gaudi::Property<bool> m_ignoreLarge{ this, "IgnoreLarge", false, "IgnoreLarge" };

  Gaudi::Property<std::array<std::vector<double>, LHCb::Detector::FT::nQuartersTotal>> m_ChannelsToSkip{
      this, "ChannelsToSkip", { {} }, "a list of PseudoChannels in Quarters to skip" };

  template <unsigned int version>
  FTLiteClusters decode_v23( const EventContext& evtCtx, LHCb::RawBank::View, unsigned int nClusters ) const;
  template <unsigned int version>
  FTLiteClusters decode_v456( const EventContext& evtCtx, LHCb::RawBank::View, const FTReadoutMap& readoutMap,
                              unsigned int nClusters ) const;
  template <unsigned int version>
  FTLiteClusters decode( const EventContext& evtCtx, LHCb::RawBank::View, const FTReadoutMap& readoutMap,
                         unsigned int nClusters ) const;

  ServiceHandle<IDataProviderSvc> m_dataSvc{ this, "DataService", "DetectorDataSvc", "The detector data service" };

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_misordered_channel{ this, "Misordered channel", 1 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_misordered_link{ this, "Misordered links", 1 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noEndFragment{
      this, "No end fragment found. Ignoring the cluster.", 1 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noBegFragment{
      this, "No begin fragment found. Ignoring the cluster.", 1 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_oddBank{ this, "Odd bank.", 1 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_emptyBank{ this, "Totally empty bank.", 1 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_absurdBank{
      this, "Bank containing more clusters than physically possible.", 1 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_wrongLocalLink{
      this, "Local link larger than physical limit. Bank may be corrupted.", 1 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_wrongLink{
      this, "Received data from a link that should be disabled. Fix the DB.", 1 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_nonExistingModule{
      this, "Skipping cluster(s) for non-existing module.", 1 };
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_oddBank_hist{
      this,
      "odd_bank",
      "Odd bank error;iBank",
      { FTRawBank::BankProperties::NbBanksMax, -0.5,
        static_cast<float>( FTRawBank::BankProperties::NbBanksMax ) - 0.5 } };
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_emptyBank_hist{
      this,
      "empty_bank",
      "Empty bank error;iBank",
      { FTRawBank::BankProperties::NbBanksMax, -0.5,
        static_cast<float>( FTRawBank::BankProperties::NbBanksMax ) - 0.5 } };
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_absurdBank_hist{
      this,
      "absurd_bank",
      "Absurd bank error;iBank",
      { FTRawBank::BankProperties::NbBanksMax, -0.5,
        static_cast<float>( FTRawBank::BankProperties::NbBanksMax ) - 0.5 } };
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_wrongLocalLink_hist{
      this,
      "wrong_local_link",
      "Wrong local link error;iBank",
      { FTRawBank::BankProperties::NbBanksMax, -0.5,
        static_cast<float>( FTRawBank::BankProperties::NbBanksMax ) - 0.5 } };
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_wrongLink_hist{
      this,
      "wrong_link",
      "Wrong link error;iLink",
      { FTRawBank::BankProperties::NbLinksMax, -0.5,
        static_cast<float>( FTRawBank::BankProperties::NbLinksMax ) - 0.5 } };
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_misordered_channel_hist{
      this,
      "misordered_channel",
      "Misordered channel;iLink",
      { FTRawBank::BankProperties::NbLinksMax, -0.5,
        static_cast<float>( FTRawBank::BankProperties::NbLinksMax ) - 0.5 } };
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_misordered_link_hist{
      this,
      "misordered_link",
      "Misordered link index;iLink",
      { FTRawBank::BankProperties::NbLinksMax, -0.5,
        static_cast<float>( FTRawBank::BankProperties::NbLinksMax ) - 0.5 } };
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_noEndFragment_hist{
      this,
      "no_end_fragment",
      "No end fragment found;iLink",
      { FTRawBank::BankProperties::NbLinksMax, -0.5,
        static_cast<float>( FTRawBank::BankProperties::NbLinksMax ) - 0.5 } };
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_noBegFragment_hist{
      this,
      "no_beg_fragment",
      "No begin fragment found;iLink",
      { FTRawBank::BankProperties::NbLinksMax, -0.5,
        static_cast<float>( FTRawBank::BankProperties::NbLinksMax ) - 0.5 } };
};

//-----------------------------------------------------------------------------
// Implementation file for class : FTRawBankDecoder
//
// 2012-05-11 : Olivier Callot
//-----------------------------------------------------------------------------

namespace {
  struct sourceID_t {
    auto operator()( LHCb::RawBank const* b ) const { return b->sourceID() & 0x7FF; }
  };

  constexpr auto by_sourceID = sourceID_t{};

  template <typename Projection>
  auto sorted( LHCb::RawBank::View banks, Projection proj ) {
    auto orderedBanks = std::vector<const LHCb::RawBank*>( banks.begin(), banks.end() );
    std::sort( orderedBanks.begin(), orderedBanks.end(),
               [proj]( const LHCb::RawBank* lhs, const LHCb::RawBank* rhs ) { return proj( lhs ) < proj( rhs ); } );
    return orderedBanks;
  }

} // namespace

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTRawBankDecoder )

FTRawBankDecoder::FTRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   { KeyValue{ "Odin", LHCb::ODINLocation::Default }, KeyValue{ "RawBanks", "DAQ/RawBanks/FTCluster" },
                     KeyValue{ "ReadoutMapStorage", "AlgorithmSpecific-" + name + "-ReadoutMap" } },
                   KeyValue{ "OutputLocation", LHCb::FTLiteClusterLocation::Default } ) {}

StatusCode FTRawBankDecoder::initialize() {
  return Transformer::initialize().andThen(
      [&] { FTReadoutMap::addConditionDerivation( this, &*m_dataSvc, inputLocation<FTReadoutMap>() ); } );
}

template <>
FTLiteClusters FTRawBankDecoder::decode<0>( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                            const FTReadoutMap& readoutMap, unsigned int nClusters ) const {
  FTLiteClusters clus{ nClusters, LHCb::getMemResource( evtCtx ) };
  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks

    LHCb::Detector::FTChannelID offset  = readoutMap.channelIDShift( bank->sourceID() );
    auto                        quarter = offset.globalQuarterIdx();

    auto make_lite_cluster = [&clus, &quarter]( LHCb::Detector::FTChannelID chan, int fracBit, int size ) {
      clus.addHit( std::forward_as_tuple( chan, fracBit, size ), quarter );
    };

    // Loop over clusters
    auto range = bank->range<FTRawBank::FTClusterData>();
    range      = range.subspan( 2 * FTRawBank::nClustersForHeader ); // skip first 64b of header;
    if ( !range.empty() && FTRawBank::isNullCluster( range[range.size() - 1] ) )
      range = range.first( range.size() - 1 ); // remove padding at end
    for ( FTRawBank::FTClusterData c : range ) {
      c            = FTRawBank::FTClusterData{ htons( static_cast<uint16_t>( c ) ) };
      auto channel = LHCb::Detector::FTChannelID{ offset + channelInBank( c ) };
      make_lite_cluster( channel, FTRawBank::fracBit( c ), FTRawBank::sizeBit( c ) );
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "quarter " << quarter << " channel  " << channel << " size " << FTRawBank::sizeBit( c )
                << " fraction " << FTRawBank::fracBit( c ) << endmsg;
    }
  }
  return clus;
}

// Obsolete decoding versions, still present in some MC.
// These do not use any link map at all, since every cluster
// contains the global address of the SiPM.
template <unsigned int vrsn>
FTLiteClusters FTRawBankDecoder::decode_v23( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                             unsigned int nClusters ) const {
  if constexpr ( vrsn == 3u || vrsn == 2u ) {

    FTLiteClusters clus{ nClusters, LHCb::getMemResource( evtCtx ) };
    for ( const LHCb::RawBank* bank : banks ) {
      auto source  = static_cast<unsigned>( bank->sourceID() );
      auto station = LHCb::Detector::FTChannelID::StationID{ source / 16 + 1u };
      auto layer   = LHCb::Detector::FTChannelID::LayerID{ ( source & 12 ) / 4 };
      auto quarter = LHCb::Detector::FTChannelID::QuarterID{ source & 3 };

      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "source " << source << " station " << to_unsigned( station ) << " layer " << to_unsigned( layer )
                  << " quarter " << to_unsigned( quarter ) << " size " << bank->size() << endmsg;

      auto first = bank->begin<FTRawBank::FTClusterData>();
      auto last  = bank->end<FTRawBank::FTClusterData>();

      while ( first != last ) {
        auto sipmHeader = static_cast<uint16_t>( *first++ );
        if ( first == last && sipmHeader == 0 ) continue; // padding at the end...
        unsigned modulesipm = sipmHeader >> 4;
        auto     module     = LHCb::Detector::FTChannelID::ModuleID{ modulesipm >> 4 };
        auto     mat        = LHCb::Detector::FTChannelID::MatID{ ( modulesipm & 15 ) >> 2 };
        unsigned sipm       = modulesipm & 3;
        int      nClus      = sipmHeader & 15;

        if ( msgLevel( MSG::VERBOSE ) && nClus > 0 )
          verbose() << " Module " << to_unsigned( module ) << " mat " << to_unsigned( mat ) << " SiPM " << sipm
                    << " nClusters " << nClus << endmsg;

        if ( nClus > std::distance( first, last ) ) {
          warning() << "Inconsistent size of rawbank. #clusters in header=" << nClus
                    << ", #clusters in bank=" << std::distance( first, last ) << endmsg;

          throw GaudiException( "Inconsistent size of rawbank", "FTRawBankDecoder", StatusCode::FAILURE );
        }

        if ( to_unsigned( module ) > 5 ) {
          ++m_nonExistingModule;
          first += nClus;
          continue;
        }

        if constexpr ( vrsn == 3u ) {

          for ( auto it = first; it < first + nClus; ++it ) {
            auto     c       = static_cast<uint16_t>( *it );
            unsigned channel = c & 127;
            int      fracBit = ( c >> 7 ) & 1;
            bool     sizeBit = ( c >> 8 ) & 1;

            // not the last cluster
            if ( !sizeBit && it < first + nClus - 1 ) {
              auto c2       = static_cast<uint16_t>( *( it + 1 ) );
              bool sizeBit2 = ( c2 >> 8 ) & 1;

              if ( !sizeBit2 ) { // next cluster is not last fragment
                clus.addHit( std::forward_as_tuple(
                                 LHCb::Detector::FTChannelID{ station, layer, quarter, module, mat, sipm, channel },
                                 fracBit, FTRawBank::maxClusterWidth ),
                             bank->sourceID() );

                if ( msgLevel( MSG::VERBOSE ) ) {
                  verbose() << format( "size<=4  channel %4d frac %3d size %3d code %4.4x", channel, fracBit, sizeBit,
                                       c )
                            << endmsg;
                }
              } else { // fragmented cluster, last edge found
                unsigned channel2 = c2 & 127;
                int      fracBit2 = ( c2 >> 7 ) & 1;

                unsigned int diff = ( channel2 - channel );

                if ( diff > 128 ) {
                  error() << "something went terribly wrong here first fragment: " << channel
                          << " second fragment: " << channel2 << endmsg;
                  throw GaudiException( "There is an inconsistency between Encoder and Decoder!", "FTRawBankDecoder",
                                        StatusCode::FAILURE );
                }
                // fragemted clusters, size > 2*max size
                // only edges were saved, add middles now
                if ( diff > FTRawBank::maxClusterWidth ) {

                  // add the first edge cluster
                  clus.addHit( std::forward_as_tuple(
                                   LHCb::Detector::FTChannelID{ station, layer, quarter, module, mat, sipm, channel },
                                   fracBit, 0 ),
                               bank->sourceID() ); // pseudoSize=0

                  if ( msgLevel( MSG::VERBOSE ) ) {
                    verbose() << format( "first edge cluster %4d frac %3d size %3d code %4.4x", channel, fracBit,
                                         sizeBit, c )
                              << endmsg;
                  }

                  for ( unsigned int i = FTRawBank::maxClusterWidth; i < diff; i += FTRawBank::maxClusterWidth ) {
                    // all middle clusters will have same size as the first cluster,
                    // so use same fraction
                    clus.addHit( std::forward_as_tuple( LHCb::Detector::FTChannelID{ station, layer, quarter, module,
                                                                                     mat, sipm, channel + i },
                                                        fracBit, 0 ),
                                 bank->sourceID() );

                    if ( msgLevel( MSG::VERBOSE ) ) {
                      verbose() << format( "middle cluster %4d frac %3d size %3d code %4.4x", channel + i, fracBit,
                                           sizeBit, c )
                                << " added " << diff << endmsg;
                    }
                  }

                  // add the last edge
                  clus.addHit( std::forward_as_tuple(
                                   LHCb::Detector::FTChannelID{ station, layer, quarter, module, mat, sipm, channel2 },
                                   fracBit2, 0 ),
                               bank->sourceID() );

                  if ( msgLevel( MSG::VERBOSE ) ) {
                    verbose() << format( "last edge cluster %4d frac %3d size %3d code %4.4x", channel2, fracBit2,
                                         sizeBit2, c2 )
                              << endmsg;
                  }
                } else { // big cluster size upto size 8
                  unsigned int firstChan = channel - int( ( FTRawBank::maxClusterWidth - 1 ) / 2 );
                  unsigned int widthClus = 2 * diff - 1 + fracBit2;

                  unsigned int clusChanPosition = firstChan + ( widthClus - 1 ) / 2;
                  int          frac             = ( widthClus - 1 ) % 2;

                  // add the new cluster = cluster1+cluster2
                  clus.addHit( std::forward_as_tuple( LHCb::Detector::FTChannelID{ station, layer, quarter, module, mat,
                                                                                   sipm, clusChanPosition },
                                                      frac, widthClus ),
                               bank->sourceID() );

                  if ( msgLevel( MSG::VERBOSE ) ) {
                    verbose() << format( "combined cluster %4d frac %3d size %3d code %4.4x", channel, fracBit, sizeBit,
                                         c )
                              << endmsg;
                  }
                } // end if adjacent clusters
                ++it;
              }    // last edge foud
            }      // not the last cluster
            else { // last cluster, so nothing we can do
              clus.addHit( std::forward_as_tuple(
                               LHCb::Detector::FTChannelID{ station, layer, quarter, module, mat, sipm, channel },
                               fracBit, FTRawBank::maxClusterWidth ),
                           bank->sourceID() );

              if ( msgLevel( MSG::VERBOSE ) ) {
                verbose() << format( "size<=4  channel %4d frac %3d size %3d code %4.4x", channel, fracBit, sizeBit, c )
                          << endmsg;
              }
            }    // last cluster added
          }      // end loop over clusters in one sipm
        } else { // bank vrsn == 2
          static_assert( vrsn == 2 );
          // normal clustering without any modification to clusters, should work for encoder=2
          for ( auto it = first; it < first + nClus; ++it ) {
            auto     c       = static_cast<uint16_t>( *it );
            unsigned channel = ( c >> 0 ) & 127;
            int      fracBit = ( c >> 7 ) & 1;
            int      sizeBit = ( c >> 8 ) & 1;
            clus.addHit( std::forward_as_tuple(
                             LHCb::Detector::FTChannelID{ station, layer, quarter, module, mat, sipm, channel },
                             fracBit, ( sizeBit ? 0 : FTRawBank::maxClusterWidth ) ),
                         bank->sourceID() );
          }
        }
        first += nClus;
      } // end loop over sipms
    }   // end loop over rawbanks
    clus.setOffsets();
    return clus;
  }
}

// Obsolete decoding versions, still present in some MC
// These do not contain a link map, and so always assume that SiPMs
// within a bank are contiguous and you can always guess the SiPM ID
// from the link index within the bank.
template <unsigned int vrsn>
FTLiteClusters FTRawBankDecoder::decode_v456( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                              const FTReadoutMap& readoutMap, unsigned int nClusters ) const {

  FTLiteClusters clus{ nClusters, LHCb::getMemResource( evtCtx ) };
  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks
    LHCb::Detector::FTChannelID offset   = readoutMap.channelIDShift( bank->sourceID() );
    auto                        clusters = bank->range<FTRawBank::FTClusterData>();
    clusters = clusters.subspan( FTRawBank::nClustersForHeader ); // skip first 32b of header
    if ( !clusters.empty() && FTRawBank::isNullCluster( clusters.back() ) )
      clusters = clusters.first( clusters.size() - 1 ); // Remove padding at the end
    if constexpr ( vrsn == 4 ) {
      auto r = clusters | ranges::views::transform( [&offset]( FTRawBank::FTClusterData c ) -> LHCb::FTLiteCluster {
                 return { LHCb::Detector::FTChannelID{ offset + channelInBank( c ) }, FTRawBank::fracBit( c ),
                          ( FTRawBank::sizeBit( c ) ? 0 : FTRawBank::maxClusterWidth ) };
               } );
      clus.insert( r.begin(), r.end(), offset.globalQuarterIdx() );
    }
    if constexpr ( vrsn == 5 || vrsn == 6 ) {
      auto quarter = offset.globalQuarterIdx();
      // Define Lambda functions to be used in loop
      auto make_lite_cluster = [&clus, &quarter]( int chan, int fracBit, int size ) {
        clus.addHit(
            std::forward_as_tuple( LHCb::Detector::FTChannelID{ static_cast<unsigned int>( chan ) }, fracBit, size ),
            quarter );
      };

      // Make clusters between two channels
      auto make_lite_clusters = [&]( int firstChannel, FTRawBank::FTClusterData c, FTRawBank::FTClusterData c2 ) {
        short int widthClus = FTRawBank::channelInSiPM( c2 ) - FTRawBank::channelInSiPM( c ) + 2;
        // fragmented clusters, size > 2*max size
        // only edges were saved, add middles now
        if ( widthClus > 2 * FTRawBank::maxClusterWidth ) {
          // add the first edge cluster, and then the middle clusters
          short int i = 0;
          for ( ; i < widthClus - FTRawBank::maxClusterWidth; i += FTRawBank::maxClusterWidth ) {
            // all middle clusters will have same size as the first cluster,
            // so re-use the fraction
            make_lite_cluster( firstChannel + i, FTRawBank::fracBit( c ), 0 );
          }
          // add the last edge
          int lastChannel = firstChannel + i + std::floor( ( widthClus - i - 1 ) / 2 ) - 1;
          make_lite_cluster( lastChannel, ( widthClus - 1 ) % 2, 0 );
        } else { // big cluster size upto size 8
          make_lite_cluster( firstChannel + ( widthClus - 1 ) / 2 - 1, ( widthClus - 1 ) % 2, widthClus );
        } // end if adjacent clusters
      };  // End lambda make_lite_clusters

      // loop over clusters
      while ( !clusters.empty() ) { // loop over the clusters
        auto channel = LHCb::Detector::FTChannelID{ offset + channelInBank( clusters[0] ) };

        if ( !FTRawBank::sizeBit( clusters[0] ) ) // Not flagged as large
          make_lite_cluster( channel, FTRawBank::fracBit( clusters[0] ), FTRawBank::maxClusterWidth );
        else { // Flagged as a large cluster
          if ( clusters.size() == 1 || FTRawBank::linkIdx( clusters[0] ) != FTRawBank::linkIdx( clusters[1] ) ) {
            // Last cluster of its SiPM or bank
            make_lite_cluster( channel, FTRawBank::fracBit( clusters[0] ), 0 );
          } else {
            if ( begCluster<vrsn>( clusters[0] ) ) {
              if ( endCluster<vrsn>( clusters[1] ) ) {
                make_lite_clusters( channel, clusters[0], clusters[1] );
                clusters = clusters.subspan( 1 );
              } else { // this should never happen,
                ++m_noEndFragment;
              }
            }
          }
        }
        clusters = clusters.subspan( 1 );
      }
    }
  }
  clus.setOffsets();
  return clus;
}
// This is the only version of the decoding that runs as of 2024.
template <unsigned int vrsn>
FTLiteClusters FTRawBankDecoder::decode( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                         const FTReadoutMap& readoutMap, unsigned int nClusters ) const {
  static_assert( vrsn == 7 || vrsn == 8 );
  std::array<boost::container::small_vector<LHCb::FTLiteCluster,
                                            FTRawBank::nbClusMaximum * FTRawBank::BankProperties::NbBanksPerQuarter>,
             LHCb::Detector::FT::nQuartersTotal>
                 liteClustersByQuarter;
  FTLiteClusters sortedLiteClus{ nClusters, LHCb::getMemResource( evtCtx ) };
  // Sort the banks now by x
  for ( const LHCb::RawBank* bank : sorted( banks, by_sourceID ) ) { // Iterates over the banks, sorted by SourceID
    auto sourceID = bank->sourceID();
    auto iRow     = readoutMap.iSource( sourceID );
    if ( iRow >= readoutMap.nBanks() ) {
      error() << "Could not find the sourceID " << iRow << " "
              << toString( LHCb::Detector::FTSourceID( bank->sourceID() ) ) << " in the readout map" << endmsg;
      continue;
    }

    auto clusters = bank->range<FTRawBank::FTClusterData>();
    // Since banks are made of 16-bit clusters, we must have
    // an even number of bytes. Lots of things need to go
    // wrong to trigger this error. As of July 2024, it has
    // never been seen.
    if ( bank->range<std::byte>().size() % 2 ) {
      ++m_oddBank;
      ++m_oddBank_hist[iRow];
      continue;
    }

    // Banks should at least contain their headers. As of
    // July 2024, this error has never been seen.
    if ( ( clusters.empty() ) ) {
      ++m_emptyBank;
      ++m_emptyBank_hist[iRow];
      continue;
    }

    // Check that the bank does not contain more cluster fragments than physically possible
    // As of July 2024, this has never been seen.
    if ( clusters.size() > FTRawBank::BankProperties::NbLinksPerBank * FTRawBank::nbClusMaximum ) {
      ++m_absurdBank;
      ++m_absurdBank_hist[iRow];
      continue;
    }

    // Now, prepare the cluster container by removing headers & padding
    clusters = clusters.subspan( FTRawBank::nClustersForHeader );
    // Remove padding at the end (clusters are transmitted in 32-bit words but are 16-bits long)
    if ( !clusters.empty() && FTRawBank::isNullCluster( clusters.back() ) )
      clusters = clusters.first( clusters.size() - 1 );

    // Vector to temporarily store clusters
    std::array<boost::container::small_vector<LHCb::FTLiteCluster, FTRawBank::nbClusMaximum>,
               FTRawBank::BankProperties::NbLinksPerBank>
                                liteClustersInBankPerLinkID;
    unsigned int                localLinkIndex = 0;
    LHCb::Detector::FTChannelID globalSiPMID;

    // Define Lambda functions to be used in loop
    // Creates a lite cluster in the proper container, using an FTClusterData information
    auto make_lite_cluster = [&globalSiPMID, &liteClustersInBankPerLinkID, &localLinkIndex]( unsigned chan, int fracBit,
                                                                                             int size ) {
      liteClustersInBankPerLinkID[localLinkIndex].emplace_back( LHCb::Detector::FTChannelID( globalSiPMID + chan ),
                                                                fracBit, size );
    };

    // Make clusters between two channels, or fuse back the two cluster fragments forming
    // a large cluster of size 5-8
    std::function<void( FTRawBank::FTClusterData, FTRawBank::FTClusterData )> make_lite_clusters;
    if ( m_ignoreLarge ) {
      make_lite_clusters = [&]( FTRawBank::FTClusterData begClus, FTRawBank::FTClusterData endClus ) {
        unsigned int widthClus =
            ( FTRawBank::channelInSiPM( endClus ) - FTRawBank::channelInSiPM( begClus ) + 2 ); // lastCh-firstCh+4/2
        unsigned int size = ( widthClus > 2 * FTRawBank::maxClusterWidth ) ? 0 : widthClus;
        make_lite_cluster( FTRawBank::channelInSiPM( begClus ), 1, size );
        make_lite_cluster( FTRawBank::channelInSiPM( endClus ), 1, size );
      }; // End lambda make_lite_clusters
    } else {
      make_lite_clusters = [&]( FTRawBank::FTClusterData begClus, FTRawBank::FTClusterData endClus ) {
        short int firstChannel = FTRawBank::channelInSiPM( begClus );
        short int widthClus    = ( FTRawBank::channelInSiPM( endClus ) - firstChannel + 2 ); // lastCh-firstCh+4/2
        // fragmented clusters, size > 2*max size
        // only edges were saved, add middles now
        // these are marked with pseudosize 0
        if ( widthClus > 2 * FTRawBank::maxClusterWidth ) {
          // add the first edge cluster, and then the middle clusters
          short int i = 0;
          for ( ; i < widthClus - FTRawBank::maxClusterWidth; i += FTRawBank::maxClusterWidth ) {
            // all middle clusters will have same size as the first cluster,
            // for max size 4, fractions is always 1
            make_lite_cluster( firstChannel + i, 1, 0 );
          }

          // add the last edge
          short int lastChannel = firstChannel + i + std::floor( ( widthClus - i - 1 ) / 2 ) - 1;
          make_lite_cluster( lastChannel, ( widthClus - 1 ) % 2, 0 );
        } else { // big cluster size upto size 8
          make_lite_cluster( firstChannel + ( widthClus - 1 ) / 2 - 1, ( widthClus - 1 ) % 2, widthClus );
        } // end if adjacent clusters
      };  // End lambda make_lite_clusters
    }
    short int lastChannel( -1 );
    unsigned  lastLink( 0 );

    // Loop over the clusters
    while ( !clusters.empty() ) {
      const auto& thisClus          = clusters[0];
      uint16_t    thisChannelInSiPM = FTRawBank::channelInSiPM( thisClus );
      localLinkIndex                = FTRawBank::linkIdx( thisClus );
      auto globalLinkIndex_hist     = iRow * FTRawBank::BankProperties::NbLinksPerBank + localLinkIndex;
      // Check that the local link index is physically possible (24 links per bank)
      if ( localLinkIndex >= FTRawBank::BankProperties::NbLinksPerBank ) {
        ++m_wrongLocalLink;
        ++m_wrongLocalLink_hist[iRow];
        clusters = clusters.subspan( 1 );
        continue;
      }
      globalSiPMID = readoutMap.getGlobalSiPMIDFromIndex( iRow, localLinkIndex );
      // Check that the local link does correspond to an SiPM in the map.
      if ( globalSiPMID == LHCb::Detector::FTChannelID::kInvalidChannel() ) {
        ++m_wrongLink;
        ++m_wrongLink_hist[globalLinkIndex_hist];
        clusters = clusters.subspan( 1 );
        continue;
      }
      if ( localLinkIndex < lastLink ) {
        // LoH: this has happened in April 2024 because of bit flips. Allen!1546
        ++m_misordered_link;
        ++m_misordered_link_hist[globalLinkIndex_hist];
        clusters = clusters.subspan( 1 );
        continue;
      }
      if ( localLinkIndex == lastLink && thisChannelInSiPM <= lastChannel ) {
        ++m_misordered_channel;
        ++m_misordered_channel_hist[globalLinkIndex_hist];
        // Skip the cluster
        clusters = clusters.subspan( 1 );
        continue;
      }
      lastChannel = thisChannelInSiPM;
      lastLink    = localLinkIndex;
      if ( !FTRawBank::sizeBit( thisClus ) ) {
        // Not flagged as large
        // Small clusters have a size of 4
        make_lite_cluster( thisChannelInSiPM, FTRawBank::fracBit( thisClus ), FTRawBank::maxClusterWidth );
      } else {
        // Large cluster
        //  Check if it is the last cluster in the bank or the link
        if ( clusters.size() == 1 || FTRawBank::linkIdx( thisClus ) != FTRawBank::linkIdx( clusters[1] ) ) {
          make_lite_cluster( thisChannelInSiPM, FTRawBank::fracBit( thisClus ), 0 );
        } else {
          const auto& nextClus = clusters[1];
          if ( FTRawBank::begCluster<vrsn>( thisClus ) ) {
            if ( !FTRawBank::endCluster<vrsn>( nextClus ) ) {
              ++m_noEndFragment;
              ++m_noEndFragment_hist[globalLinkIndex_hist];
              if constexpr ( vrsn == 8 ) { clusters = clusters.subspan( 1 ); }
            } else {
              if ( FTRawBank::channelInSiPM( nextClus ) <= thisChannelInSiPM ) {
                // Clusters should come in order. This is behaviour puts into question the whole concept of large
                // clusters
                //(imagine we got two large clusters and their beginning/end get scrambled)
                // For the time being, we will assume this is a simple local swap and keep the clusters
                ++m_misordered_channel;
                ++m_misordered_channel_hist[globalLinkIndex_hist];
              } else { // this should always be true
                make_lite_clusters( thisClus, nextClus );
                lastChannel = FTRawBank::channelInSiPM( nextClus );
              }
              clusters = clusters.subspan( 1 );
            }
            // In all cases, skip the end fragment
          } else {
            //---LoH: This is actually problematic but since we want the exact same behaviour
            //        with Detector!442, we port it as is.
            if constexpr ( vrsn == 7 ) {
              ++m_noBegFragment;
              ++m_noBegFragment_hist[globalLinkIndex_hist];
            }
          }
        }
      }
      // Go to next cluster
      clusters = clusters.subspan( 1 );
    }

    // Sort clusters within bank using the SiPMID->linkID index array from FTReadoutMap
    auto SiPMindices = readoutMap.getSiPMRemappingFromIndex( iRow );
    for ( auto index : SiPMindices ) {
      if ( index == -1 ) break; // stop when padding is reached
      // Now loop over clusters within link (ordered by definition) and add to object
      auto liteClusterVector = liteClustersInBankPerLinkID[index];
      for ( auto liteClusterData : liteClusterVector ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << liteClusterData.channelID() << endmsg;

        unsigned int globalQuarterIdx = liteClusterData.channelID().globalQuarterIdx();
        int          pseudoChannelIdx = liteClusterData.channelID().localChannelIdx_module();
        if ( !m_ChannelsToSkip.empty() ) {
          if ( !m_ChannelsToSkip[globalQuarterIdx].empty() ) {
            if ( std::binary_search( m_ChannelsToSkip[globalQuarterIdx].begin(),
                                     m_ChannelsToSkip[globalQuarterIdx].end(), pseudoChannelIdx ) ) {
              continue;
            }
          }
        }

        liteClustersByQuarter[liteClusterData.channelID().globalQuarterIdx()].emplace_back(
            liteClusterData.channelID(), liteClusterData.fractionBit(), liteClusterData.pseudoSize() );
      }
    }
  } // end loop over rawbanks

  // Sort containers per channelID
  for ( auto& c : liteClustersByQuarter ) {
    std::sort( c.begin(), c.end(), []( const LHCb::FTLiteCluster& lhs, const LHCb::FTLiteCluster& rhs ) {
      return lhs.channelID() < rhs.channelID();
    } );
  }

  for ( const auto& [iQuarter, liteClusters] : LHCb::range::enumerate( liteClustersByQuarter ) ) {
    for ( auto liteClus : liteClusters ) sortedLiteClus.addHit( std::forward_as_tuple( liteClus ), iQuarter );
  }

  sortedLiteClus.setOffsets();
  return sortedLiteClus;
}

//=============================================================================
// Main execution
//=============================================================================
FTLiteClusters FTRawBankDecoder::operator()( const EventContext& evtCtx, const LHCb::ODIN& odin,
                                             const LHCb::RawBank::View& banks, const FTReadoutMap& readoutMap ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of raw banks " << banks.size() << endmsg;
  if ( banks.empty() ) return {};

  // Testing the bank version
  unsigned int vrsn = banks[0]->version();
  // Fix due to early 2023 data where the hardcoded bank version was still 7 whereas
  // the logic of large clusters was already version 8.
  bool       decodeV7AsV8 = ( vrsn == 7 && odin.runNumber() >= 267665 && odin.runNumber() < 273505 );
  auto const version      = ( vrsn == 5 && m_decodeV5AsV4.value() ) ? 4 : ( decodeV7AsV8 ) ? 8 : vrsn;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Bank version=v" << vrsn << " with decoding version=v" << version << endmsg;
  // Check if decoding version corresponds with readout version.
  readoutMap.compatible( version );

  // Estimate total number of clusters from bank sizes
  auto clus = [&]( unsigned int nClusters ) {
    switch ( version ) {
    case 0:
      return decode<0>( evtCtx, banks, readoutMap, nClusters );
    case 2:
      return decode_v23<2>( evtCtx, banks, nClusters );
    case 3:
      return decode_v23<3>( evtCtx, banks, nClusters );
    case 4:
      return decode_v456<4>( evtCtx, banks, readoutMap, nClusters );
    case 5:
      return decode_v456<5>( evtCtx, banks, readoutMap, nClusters );
    case 6:
      return decode_v456<6>( evtCtx, banks, readoutMap, nClusters );
    case 7:
      return decode<7>( evtCtx, banks, readoutMap, nClusters );
    case 8:
      return decode<8>( evtCtx, banks, readoutMap, nClusters );
    default:
      throw GaudiException( "Unknown bank version: " + std::to_string( version ), __FILE__, StatusCode::FAILURE );
    };
  }( LHCb::FTDAQ::nbFTClusters( banks ) );

  if ( msgLevel( MSG::VERBOSE ) ) {
    for ( const auto& c : clus.range() )
      verbose() << format( " channel %4u frac %3f size %3u ", c.channelID(), c.fraction(), c.pseudoSize() ) << endmsg;
  }

  assert( std::is_sorted( clus.range().begin(), clus.range().end(),
                          []( const LHCb::FTLiteCluster& lhs, const LHCb::FTLiteCluster& rhs ) {
                            return lhs.channelID() < rhs.channelID();
                          } ) &&
          "Clusters from the RawBanks not sorted. Should be sorted by construction." );
  LHCb::FTDAQ::orderInX( clus );
  return clus;
}
