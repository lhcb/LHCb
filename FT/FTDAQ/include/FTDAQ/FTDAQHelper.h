/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/RawBank.h"
#include <Event/FTLiteCluster.h>

#include "Detector/FT/FTConstants.h"

#include "Kernel/STLExtensions.h"

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;
namespace FTRawBank  = LHCb::Detector::FT::RawBank;

namespace LHCb {

  namespace FTDAQ {

    /**
     * counts number of FT clusters or FTDigits in the given raw banks
     */
    unsigned int nbFTClusters( LHCb::RawBank::View banks );
    unsigned int nbFTDigits( LHCb::RawBank::View banks );

    // Ordering functions
    template <typename Iter>
    void reverse_each_module( Iter first, Iter last );
    template <typename Container, typename Fun>
    void for_each_quadrant( Container& c, Fun&& f );
    void orderInX( FTLiteClusters& clus );

  } // namespace FTDAQ

} // namespace LHCb
