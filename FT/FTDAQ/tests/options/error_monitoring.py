###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Gaudi__Monitoring__JSONSink as JSONSink
from DDDB.CheckDD4Hep import UseDD4Hep
from Gaudi.Configuration import ApplicationMgr

from PyConf.Algorithms import FTErrorBankDecoder, LHCb__UnpackRawEvent
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    default_raw_banks,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("FRErrorMonitoring")
options.evt_max = 100

config = configure_input(options)

ftErrorDecoder = FTErrorBankDecoder(
    name="FTErrorBankDecoder", ErrorRawBanks=default_raw_banks("DaqErrorBXIDCorrupted")
)

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    DD4hepSvc(DetectorList=["/world", "FT"])

JSONSink(FileName="error_monitoring.json").NamesToSave = [
    ".*errorsPerBankLocation",
    ".*ErrorsPerErrorBankType",
]
ApplicationMgr().ExtSvc.append(
    JSONSink(
        FileName="error_monitoring.json",
        NamesToSave=[".*errorsPerBankLocation", ".*ErrorsPerErrorBankType"],
    )
)

node = CompositeNode("FTErrorDecoding", children=[ftErrorDecoder])
config.update(configure(options, node))
